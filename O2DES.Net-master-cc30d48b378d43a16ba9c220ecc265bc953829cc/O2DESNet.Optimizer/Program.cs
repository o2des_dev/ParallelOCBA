﻿using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.Statistics;
using O2DESNet.Optimizer.Samplings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O2DESNet.Optimizer
{
    class Program
    {
        static void Main(string[] args)
        {
            Test_ForOCBARearranged();
        }

        static void Test_ForOCBARearranged()
        {
            var ocba1 = new OCBA(); //Original algorithm
            var ocba2 = new POCBA(); //POCBA refers to ROCBA

            var rns1 = new Benchmarks.RnS_MonotoneDecreasingMeans(nDesigns: 16, vPower: 2, seed: 10);
            var rns2 = new Benchmarks.RnS_MonotoneDecreasingMeans(nDesigns: 16, vPower: 2, seed: 10);
 //           var rns1 = new Benchmarks.RnS_SlippageConfiguration(nDesigns: 16, rho: 0.5, seed: 10);
//            var rns2 = new Benchmarks.RnS_SlippageConfiguration(nDesigns: 16, rho: 0.5, seed: 10);

            for (int i = 0; i < 200; i++)
            {
                var alloc1 = ocba1.Alloc(10, rns1.Solutions, i);
                foreach (var a in alloc1) rns1.Evaluate((int)a.Key[0], a.Value);

                var alloc2 = ocba2.Alloc(10, rns2.Solutions, i);
                foreach (var a in alloc2) rns2.Evaluate((int)a.Key[0], a.Value);

                Console.WriteLine("{0:F4}\t{1:F4}\t{2:F4}\t{3:F4}", rns1.PCS, rns2.PCS, rns1.Variance, rns2.Variance);
            }
        }
    }
}
