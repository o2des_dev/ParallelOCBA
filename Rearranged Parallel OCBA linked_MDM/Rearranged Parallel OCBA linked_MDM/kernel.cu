#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <cuda.h>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include <fstream>
#include <random>
#include <time.h>

/* please put the following definitions at the head of your
program */
/*Define parameters for simulation*/
/*the number of designs. It should be larger than 2.*/
#define GRIDDIMENSION 1
#define BLOCKDIMENSION 32
#define TYPE 1
/*maximization TYPE = -1, minimization TYPE = 1*/
/*prototypes of functions and subroutines included in this
file*/

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type);
int best(float t_s_mean[], int nd);
int second_best(float t_s_mean[], int nd, int b);

#ifdef _WIN32
#include <Windows.h>
double get_wall_time(){
	LARGE_INTEGER time, freq;
	if (!QueryPerformanceFrequency(&freq)){
		//  Handle error
		return 0;
	}
	if (!QueryPerformanceCounter(&time)){
		//  Handle error
		return 0;
	}
	return (double)time.QuadPart / freq.QuadPart;
}

double get_cpu_time(){
	FILETIME a, b, c, d;
	if (GetProcessTimes(GetCurrentProcess(), &a, &b, &c, &d) != 0){
		//  Returns total user time.
		return
			(double)(d.dwLowDateTime |
			((unsigned long long)d.dwHighDateTime << 32)) * 0.0000001;
	}
	else{
		//  Handle error
		return 0;
	}
}
#endif


__device__ inline void atomicFloatSub(float *address, float val) {
	int i_val = __float_as_int(val);
	int tmp0 = 0;
	int tmp1;

	while ((tmp1 = atomicCAS((int *)address, tmp0, i_val)) != tmp0)	{
		tmp0 = tmp1;
		i_val = __float_as_int(__int_as_float(tmp1) - val);
	}
}

__global__ void parallelOCBA3(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	float temp = 0;
	unsigned int i = 0;

	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//use previous alpha of best
		if (lamda[index] == 0) {
			lamda[index] = *alphaB;
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//use previous rho
		*rho = *rhoPrevious;
		d_t[1] = d_t[0];
		//	__syncthreads();

		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];
	}
}

__global__ void parallelOCBA_1(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 2: find maximum
		*best = -FLT_MAX;
		float temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		unsigned int i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		*alphaB = *best;

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rhoPrevious = d_t[0] / *summation;
	}
}

__global__ void parallelOCBA2(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	float temp = 0;
	unsigned int i = 0;

	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}
		
		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//use previous alpha of best
		if (lamda[index] == 0) {
			lamda[index] = *alphaB;
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//use previous rho
		*rho = *rhoPrevious;
		d_t[1] = d_t[0];
		//	__syncthreads();

		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];

		//step 2: find maximum
		*best = -FLT_MAX;
		temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rho = d_t[0] / *summation;
	}
}

__global__ void parallelOCBA1(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 2: find maximum
		*best = -FLT_MAX;
		float temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		unsigned int i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rho = d_t[0] / *summation;
		d_t[1] = d_t[0];
		//	__syncthreads();


		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];
	}
}

void simulatorEmulator(int an[], int nd, double xToAdd[], double xSquareToAdd[]) {
	//test case: i=0 is best design
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		for (int j = 0; j < an[i]; j++) {
			std::default_random_engine generator;
			std::normal_distribution<double> distribution(-900 + (100 * i), 50);
			temp = distribution(generator);
			xToAdd[i] += temp;
			xSquareToAdd[i] += (temp*temp);
		}
	}
	return;
}

void updateMeanVariance(int nd, int an[], int n[], float mean[], float var[], double xCurrent[], double xSquareCurrent[], double xToAdd[], double xSquareToAdd[]) {
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		mean[i] = (n[i] * mean[i] + xCurrent[i]) / (n[i] + an[i]);
		xCurrent[i] += xToAdd[i];
		xSquareCurrent[i] += xSquareToAdd[i];
		var[i] = (xSquareCurrent[i] - (xCurrent[i] * xCurrent[i] / (n[i] + an[i]))) / (n[i] + an[i] - 1);
	}
	return;
}



void main()
{
	srand(time(NULL));
	std::ofstream outputFile;
	outputFile.open("results.csv");
	int budget[1] = { 1000 };
	int candidates[10] = { 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192 };
	//int budget[6] = { 10, 100, 1000, 10000, 100000, 1000000 };
	for (int k = 0; k < 1; k++) {
		int ADD_BUDGET = budget[k]; /*the additional simulation
									budget. It should be positive integer.*/
		outputFile << std::endl << "Budget" << "," << budget[k] << std::endl;
		for (int j = 0; j < 50; j++) {
			for (int l = 0; l < 10; l++) {
				outputFile << std::endl << "Candidate" << "," << candidates[l] << std::endl;
				for (int k = 0; k < 4; k++) {
					outputFile << std::endl << "Config" << "," << k << std::endl;

					int i;

					if (l == 0) {
						float s_mean[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
						float s_var[16] = {1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2 };
						int n[16] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[16];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 1) {
						float s_mean[32] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 };
						float s_var[32] = {1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2, 2.03054, 2.05977, 2.0878, 2.11474, 2.1407, 2.16574, 2.18994, 2.21336, 2.23607, 2.2581, 2.27951, 2.30033, 2.3206, 2.34035, 2.35961, 2.37841 };
						int n[32] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[32];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 2) {
						float s_mean[64] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64};
						float s_var[64] = { 1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2, 2.03054, 2.05977, 2.0878, 2.11474, 2.1407, 2.16574, 2.18994, 2.21336, 2.23607, 2.2581, 2.27951, 2.30033, 2.3206, 2.34035, 2.35961, 2.37841, 2.39678, 2.41474, 2.4323, 2.44949, 2.46633, 2.48282, 2.499, 2.51487, 2.53044, 2.54573, 2.56075, 2.57551, 2.59002, 2.60429, 2.61833, 2.63215, 2.64575, 2.65915, 2.67235, 2.68535, 2.69817, 2.71081, 2.72327, 2.73556, 2.7477, 2.75967, 2.77149, 2.78316, 2.79468, 2.80607, 2.81731, 2.82843};
						int n[64] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[64];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 3) {
						float s_mean[128] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128};
						float s_var[128] = { 1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2, 2.03054, 2.05977, 2.0878, 2.11474, 2.1407, 2.16574, 2.18994, 2.21336, 2.23607, 2.2581, 2.27951, 2.30033, 2.3206, 2.34035, 2.35961, 2.37841, 2.39678, 2.41474, 2.4323, 2.44949, 2.46633, 2.48282, 2.499, 2.51487, 2.53044, 2.54573, 2.56075, 2.57551, 2.59002, 2.60429, 2.61833, 2.63215, 2.64575, 2.65915, 2.67235, 2.68535, 2.69817, 2.71081, 2.72327, 2.73556, 2.7477, 2.75967, 2.77149, 2.78316, 2.79468, 2.80607, 2.81731, 2.82843, 2.83941, 2.85027, 2.86101, 2.87162, 2.88212, 2.89251, 2.90278, 2.91295, 2.92301, 2.93297, 2.94283, 2.95259, 2.96226, 2.97183, 2.98131, 2.9907, 3, 3.00922, 3.01835, 3.0274, 3.03637, 3.04526, 3.05408, 3.06281, 3.07148, 3.08007, 3.08859, 3.09704, 3.10542, 3.11374, 3.12199, 3.13017, 3.13829, 3.14635, 3.15434, 3.16228, 3.17015, 3.17797, 3.18573, 3.19344, 3.20109, 3.20868, 3.21622, 3.22371, 3.23115, 3.23853, 3.24587, 3.25315, 3.26039, 3.26758, 3.27472, 3.28182, 3.28887, 3.29587, 3.30283, 3.30975, 3.31662, 3.32346, 3.33025, 3.33699, 3.3437, 3.35037, 3.357, 3.36359};
						int n[128] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[128];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 4) {
						float s_mean[256] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256};
						float s_var[256] = {1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2, 2.03054, 2.05977, 2.0878, 2.11474, 2.1407, 2.16574, 2.18994, 2.21336, 2.23607, 2.2581, 2.27951, 2.30033, 2.3206, 2.34035, 2.35961, 2.37841, 2.39678, 2.41474, 2.4323, 2.44949, 2.46633, 2.48282, 2.499, 2.51487, 2.53044, 2.54573, 2.56075, 2.57551, 2.59002, 2.60429, 2.61833, 2.63215, 2.64575, 2.65915, 2.67235, 2.68535, 2.69817, 2.71081, 2.72327, 2.73556, 2.7477, 2.75967, 2.77149, 2.78316, 2.79468, 2.80607, 2.81731, 2.82843, 2.83941, 2.85027, 2.86101, 2.87162, 2.88212, 2.89251, 2.90278, 2.91295, 2.92301, 2.93297, 2.94283, 2.95259, 2.96226, 2.97183, 2.98131, 2.9907, 3, 3.00922, 3.01835, 3.0274, 3.03637, 3.04526, 3.05408, 3.06281, 3.07148, 3.08007, 3.08859, 3.09704, 3.10542, 3.11374, 3.12199, 3.13017, 3.13829, 3.14635, 3.15434, 3.16228, 3.17015, 3.17797, 3.18573, 3.19344, 3.20109, 3.20868, 3.21622, 3.22371, 3.23115, 3.23853, 3.24587, 3.25315, 3.26039, 3.26758, 3.27472, 3.28182, 3.28887, 3.29587, 3.30283, 3.30975, 3.31662, 3.32346, 3.33025, 3.33699, 3.3437, 3.35037, 3.357, 3.36359, 3.37014, 3.37665, 3.38312, 3.38956, 3.39596, 3.40233, 3.40866, 3.41495, 3.42121, 3.42744, 3.43363, 3.43979, 3.44592, 3.45201, 3.45807, 3.4641, 3.4701, 3.47607, 3.482, 3.48791, 3.49379, 3.49964, 3.50545, 3.51124, 3.517, 3.52274, 3.52844, 3.53412, 3.53977, 3.54539, 3.55099, 3.55656, 3.5621, 3.56762, 3.57311, 3.57858, 3.58402, 3.58944, 3.59484, 3.60021, 3.60555, 3.61087, 3.61617, 3.62145, 3.6267, 3.63193, 3.63714, 3.64232, 3.64748, 3.65262, 3.65774, 3.66284, 3.66792, 3.67297, 3.67801, 3.68302, 3.68802, 3.69299, 3.69794, 3.70288, 3.70779, 3.71269, 3.71756, 3.72242, 3.72726, 3.73208, 3.73688, 3.74166, 3.74642, 3.75117, 3.75589, 3.7606, 3.7653, 3.76997, 3.77463, 3.77927, 3.78389, 3.7885, 3.79309, 3.79766, 3.80221, 3.80675, 3.81128, 3.81579, 3.82028, 3.82475, 3.82921, 3.83366, 3.83809, 3.8425, 3.8469, 3.85129, 3.85565, 3.86001, 3.86435, 3.86867, 3.87298, 3.87728, 3.88156, 3.88583, 3.89008, 3.89432, 3.89855, 3.90276, 3.90696, 3.91115, 3.91532, 3.91948, 3.92362, 3.92775, 3.93187, 3.93598, 3.94007, 3.94415, 3.94822, 3.95228, 3.95632, 3.96035, 3.96437, 3.96838, 3.97237, 3.97635, 3.98032, 3.98428, 3.98823, 3.99216, 3.99609, 4};
						int n[256] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[256];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 5) {
						float s_mean[512] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512};
						float s_var[512] = {1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2, 2.03054, 2.05977, 2.0878, 2.11474, 2.1407, 2.16574, 2.18994, 2.21336, 2.23607, 2.2581, 2.27951, 2.30033, 2.3206, 2.34035, 2.35961, 2.37841, 2.39678, 2.41474, 2.4323, 2.44949, 2.46633, 2.48282, 2.499, 2.51487, 2.53044, 2.54573, 2.56075, 2.57551, 2.59002, 2.60429, 2.61833, 2.63215, 2.64575, 2.65915, 2.67235, 2.68535, 2.69817, 2.71081, 2.72327, 2.73556, 2.7477, 2.75967, 2.77149, 2.78316, 2.79468, 2.80607, 2.81731, 2.82843, 2.83941, 2.85027, 2.86101, 2.87162, 2.88212, 2.89251, 2.90278, 2.91295, 2.92301, 2.93297, 2.94283, 2.95259, 2.96226, 2.97183, 2.98131, 2.9907, 3, 3.00922, 3.01835, 3.0274, 3.03637, 3.04526, 3.05408, 3.06281, 3.07148, 3.08007, 3.08859, 3.09704, 3.10542, 3.11374, 3.12199, 3.13017, 3.13829, 3.14635, 3.15434, 3.16228, 3.17015, 3.17797, 3.18573, 3.19344, 3.20109, 3.20868, 3.21622, 3.22371, 3.23115, 3.23853, 3.24587, 3.25315, 3.26039, 3.26758, 3.27472, 3.28182, 3.28887, 3.29587, 3.30283, 3.30975, 3.31662, 3.32346, 3.33025, 3.33699, 3.3437, 3.35037, 3.357, 3.36359, 3.37014, 3.37665, 3.38312, 3.38956, 3.39596, 3.40233, 3.40866, 3.41495, 3.42121, 3.42744, 3.43363, 3.43979, 3.44592, 3.45201, 3.45807, 3.4641, 3.4701, 3.47607, 3.482, 3.48791, 3.49379, 3.49964, 3.50545, 3.51124, 3.517, 3.52274, 3.52844, 3.53412, 3.53977, 3.54539, 3.55099, 3.55656, 3.5621, 3.56762, 3.57311, 3.57858, 3.58402, 3.58944, 3.59484, 3.60021, 3.60555, 3.61087, 3.61617, 3.62145, 3.6267, 3.63193, 3.63714, 3.64232, 3.64748, 3.65262, 3.65774, 3.66284, 3.66792, 3.67297, 3.67801, 3.68302, 3.68802, 3.69299, 3.69794, 3.70288, 3.70779, 3.71269, 3.71756, 3.72242, 3.72726, 3.73208, 3.73688, 3.74166, 3.74642, 3.75117, 3.75589, 3.7606, 3.7653, 3.76997, 3.77463, 3.77927, 3.78389, 3.7885, 3.79309, 3.79766, 3.80221, 3.80675, 3.81128, 3.81579, 3.82028, 3.82475, 3.82921, 3.83366, 3.83809, 3.8425, 3.8469, 3.85129, 3.85565, 3.86001, 3.86435, 3.86867, 3.87298, 3.87728, 3.88156, 3.88583, 3.89008, 3.89432, 3.89855, 3.90276, 3.90696, 3.91115, 3.91532, 3.91948, 3.92362, 3.92775, 3.93187, 3.93598, 3.94007, 3.94415, 3.94822, 3.95228, 3.95632, 3.96035, 3.96437, 3.96838, 3.97237, 3.97635, 3.98032, 3.98428, 3.98823, 3.99216, 3.99609, 4, 4.0039, 4.00779, 4.01167, 4.01553, 4.01939, 4.02323, 4.02707, 4.03089, 4.0347, 4.0385, 4.04229, 4.04607, 4.04984, 4.0536, 4.05735, 4.06109, 4.06481, 4.06853, 4.07224, 4.07594, 4.07962, 4.0833, 4.08697, 4.09062, 4.09427, 4.09791, 4.10154, 4.10516, 4.10876, 4.11236, 4.11595, 4.11953, 4.12311, 4.12667, 4.13022, 4.13376, 4.1373, 4.14082, 4.14434, 4.14785, 4.15135, 4.15484, 4.15832, 4.16179, 4.16526, 4.16871, 4.17216, 4.1756, 4.17902, 4.18245, 4.18586, 4.18926, 4.19266, 4.19605, 4.19943, 4.2028, 4.20616, 4.20952, 4.21287, 4.21621, 4.21954, 4.22286, 4.22618, 4.22949, 4.23279, 4.23608, 4.23936, 4.24264, 4.24591, 4.24917, 4.25243, 4.25568, 4.25892, 4.26215, 4.26537, 4.26859, 4.2718, 4.275, 4.2782, 4.28139, 4.28457, 4.28775, 4.29092, 4.29408, 4.29723, 4.30038, 4.30352, 4.30665, 4.30978, 4.3129, 4.31601, 4.31912, 4.32221, 4.32531, 4.32839, 4.33147, 4.33455, 4.33761, 4.34067, 4.34373, 4.34677, 4.34981, 4.35285, 4.35588, 4.3589, 4.36191, 4.36492, 4.36793, 4.37092, 4.37391, 4.3769, 4.37988, 4.38285, 4.38582, 4.38878, 4.39173, 4.39468, 4.39762, 4.40056, 4.40349, 4.40641, 4.40933, 4.41225, 4.41515, 4.41806, 4.42095, 4.42384, 4.42673, 4.42961, 4.43248, 4.43535, 4.43821, 4.44107, 4.44392, 4.44677, 4.44961, 4.45244, 4.45527, 4.45809, 4.46091, 4.46373, 4.46654, 4.46934, 4.47214, 4.47493, 4.47772, 4.4805, 4.48327, 4.48605, 4.48881, 4.49157, 4.49433, 4.49708, 4.49983, 4.50257, 4.50531, 4.50804, 4.51076, 4.51349, 4.5162, 4.51891, 4.52162, 4.52432, 4.52702, 4.52971, 4.5324, 4.53508, 4.53776, 4.54043, 4.5431, 4.54576, 4.54842, 4.55108, 4.55373, 4.55637, 4.55901, 4.56165, 4.56428, 4.56691, 4.56953, 4.57215, 4.57476, 4.57737, 4.57998, 4.58258, 4.58517, 4.58776, 4.59035, 4.59293, 4.59551, 4.59808, 4.60065, 4.60322, 4.60578, 4.60834, 4.61089, 4.61344, 4.61598, 4.61852, 4.62106, 4.62359, 4.62611, 4.62864, 4.63116, 4.63367, 4.63618, 4.63869, 4.64119, 4.64369, 4.64618, 4.64868, 4.65116, 4.65364, 4.65612, 4.6586, 4.66107, 4.66354, 4.666, 4.66846, 4.67091, 4.67336, 4.67581, 4.67825, 4.68069, 4.68313, 4.68556, 4.68799, 4.69042, 4.69284, 4.69525, 4.69767, 4.70008, 4.70248, 4.70489, 4.70728, 4.70968, 4.71207, 4.71446, 4.71684, 4.71922, 4.7216, 4.72397, 4.72634, 4.72871, 4.73107, 4.73343, 4.73579, 4.73814, 4.74049, 4.74283, 4.74517, 4.74751, 4.74985, 4.75218, 4.7545, 4.75683};
						int n[512] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[512];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 6) {
						float s_mean[1024] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024};
						float s_var[1024] = {1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2, 2.03054, 2.05977, 2.0878, 2.11474, 2.1407, 2.16574, 2.18994, 2.21336, 2.23607, 2.2581, 2.27951, 2.30033, 2.3206, 2.34035, 2.35961, 2.37841, 2.39678, 2.41474, 2.4323, 2.44949, 2.46633, 2.48282, 2.499, 2.51487, 2.53044, 2.54573, 2.56075, 2.57551, 2.59002, 2.60429, 2.61833, 2.63215, 2.64575, 2.65915, 2.67235, 2.68535, 2.69817, 2.71081, 2.72327, 2.73556, 2.7477, 2.75967, 2.77149, 2.78316, 2.79468, 2.80607, 2.81731, 2.82843, 2.83941, 2.85027, 2.86101, 2.87162, 2.88212, 2.89251, 2.90278, 2.91295, 2.92301, 2.93297, 2.94283, 2.95259, 2.96226, 2.97183, 2.98131, 2.9907, 3, 3.00922, 3.01835, 3.0274, 3.03637, 3.04526, 3.05408, 3.06281, 3.07148, 3.08007, 3.08859, 3.09704, 3.10542, 3.11374, 3.12199, 3.13017, 3.13829, 3.14635, 3.15434, 3.16228, 3.17015, 3.17797, 3.18573, 3.19344, 3.20109, 3.20868, 3.21622, 3.22371, 3.23115, 3.23853, 3.24587, 3.25315, 3.26039, 3.26758, 3.27472, 3.28182, 3.28887, 3.29587, 3.30283, 3.30975, 3.31662, 3.32346, 3.33025, 3.33699, 3.3437, 3.35037, 3.357, 3.36359, 3.37014, 3.37665, 3.38312, 3.38956, 3.39596, 3.40233, 3.40866, 3.41495, 3.42121, 3.42744, 3.43363, 3.43979, 3.44592, 3.45201, 3.45807, 3.4641, 3.4701, 3.47607, 3.482, 3.48791, 3.49379, 3.49964, 3.50545, 3.51124, 3.517, 3.52274, 3.52844, 3.53412, 3.53977, 3.54539, 3.55099, 3.55656, 3.5621, 3.56762, 3.57311, 3.57858, 3.58402, 3.58944, 3.59484, 3.60021, 3.60555, 3.61087, 3.61617, 3.62145, 3.6267, 3.63193, 3.63714, 3.64232, 3.64748, 3.65262, 3.65774, 3.66284, 3.66792, 3.67297, 3.67801, 3.68302, 3.68802, 3.69299, 3.69794, 3.70288, 3.70779, 3.71269, 3.71756, 3.72242, 3.72726, 3.73208, 3.73688, 3.74166, 3.74642, 3.75117, 3.75589, 3.7606, 3.7653, 3.76997, 3.77463, 3.77927, 3.78389, 3.7885, 3.79309, 3.79766, 3.80221, 3.80675, 3.81128, 3.81579, 3.82028, 3.82475, 3.82921, 3.83366, 3.83809, 3.8425, 3.8469, 3.85129, 3.85565, 3.86001, 3.86435, 3.86867, 3.87298, 3.87728, 3.88156, 3.88583, 3.89008, 3.89432, 3.89855, 3.90276, 3.90696, 3.91115, 3.91532, 3.91948, 3.92362, 3.92775, 3.93187, 3.93598, 3.94007, 3.94415, 3.94822, 3.95228, 3.95632, 3.96035, 3.96437, 3.96838, 3.97237, 3.97635, 3.98032, 3.98428, 3.98823, 3.99216, 3.99609, 4, 4.0039, 4.00779, 4.01167, 4.01553, 4.01939, 4.02323, 4.02707, 4.03089, 4.0347, 4.0385, 4.04229, 4.04607, 4.04984, 4.0536, 4.05735, 4.06109, 4.06481, 4.06853, 4.07224, 4.07594, 4.07962, 4.0833, 4.08697, 4.09062, 4.09427, 4.09791, 4.10154, 4.10516, 4.10876, 4.11236, 4.11595, 4.11953, 4.12311, 4.12667, 4.13022, 4.13376, 4.1373, 4.14082, 4.14434, 4.14785, 4.15135, 4.15484, 4.15832, 4.16179, 4.16526, 4.16871, 4.17216, 4.1756, 4.17902, 4.18245, 4.18586, 4.18926, 4.19266, 4.19605, 4.19943, 4.2028, 4.20616, 4.20952, 4.21287, 4.21621, 4.21954, 4.22286, 4.22618, 4.22949, 4.23279, 4.23608, 4.23936, 4.24264, 4.24591, 4.24917, 4.25243, 4.25568, 4.25892, 4.26215, 4.26537, 4.26859, 4.2718, 4.275, 4.2782, 4.28139, 4.28457, 4.28775, 4.29092, 4.29408, 4.29723, 4.30038, 4.30352, 4.30665, 4.30978, 4.3129, 4.31601, 4.31912, 4.32221, 4.32531, 4.32839, 4.33147, 4.33455, 4.33761, 4.34067, 4.34373, 4.34677, 4.34981, 4.35285, 4.35588, 4.3589, 4.36191, 4.36492, 4.36793, 4.37092, 4.37391, 4.3769, 4.37988, 4.38285, 4.38582, 4.38878, 4.39173, 4.39468, 4.39762, 4.40056, 4.40349, 4.40641, 4.40933, 4.41225, 4.41515, 4.41806, 4.42095, 4.42384, 4.42673, 4.42961, 4.43248, 4.43535, 4.43821, 4.44107, 4.44392, 4.44677, 4.44961, 4.45244, 4.45527, 4.45809, 4.46091, 4.46373, 4.46654, 4.46934, 4.47214, 4.47493, 4.47772, 4.4805, 4.48327, 4.48605, 4.48881, 4.49157, 4.49433, 4.49708, 4.49983, 4.50257, 4.50531, 4.50804, 4.51076, 4.51349, 4.5162, 4.51891, 4.52162, 4.52432, 4.52702, 4.52971, 4.5324, 4.53508, 4.53776, 4.54043, 4.5431, 4.54576, 4.54842, 4.55108, 4.55373, 4.55637, 4.55901, 4.56165, 4.56428, 4.56691, 4.56953, 4.57215, 4.57476, 4.57737, 4.57998, 4.58258, 4.58517, 4.58776, 4.59035, 4.59293, 4.59551, 4.59808, 4.60065, 4.60322, 4.60578, 4.60834, 4.61089, 4.61344, 4.61598, 4.61852, 4.62106, 4.62359, 4.62611, 4.62864, 4.63116, 4.63367, 4.63618, 4.63869, 4.64119, 4.64369, 4.64618, 4.64868, 4.65116, 4.65364, 4.65612, 4.6586, 4.66107, 4.66354, 4.666, 4.66846, 4.67091, 4.67336, 4.67581, 4.67825, 4.68069, 4.68313, 4.68556, 4.68799, 4.69042, 4.69284, 4.69525, 4.69767, 4.70008, 4.70248, 4.70489, 4.70728, 4.70968, 4.71207, 4.71446, 4.71684, 4.71922, 4.7216, 4.72397, 4.72634, 4.72871, 4.73107, 4.73343, 4.73579, 4.73814, 4.74049, 4.74283, 4.74517, 4.74751, 4.74985, 4.75218, 4.7545, 4.75683, 4.75915, 4.76147, 4.76378, 4.76609, 4.7684, 4.7707, 4.773, 4.7753, 4.7776, 4.77989, 4.78217, 4.78446, 4.78674, 4.78902, 4.79129, 4.79356, 4.79583, 4.7981, 4.80036, 4.80262, 4.80487, 4.80712, 4.80937, 4.81162, 4.81386, 4.8161, 4.81834, 4.82057, 4.8228, 4.82503, 4.82725, 4.82947, 4.83169, 4.83391, 4.83612, 4.83833, 4.84053, 4.84273, 4.84493, 4.84713, 4.84932, 4.85152, 4.8537, 4.85589, 4.85807, 4.86025, 4.86243, 4.8646, 4.86677, 4.86894, 4.8711, 4.87326, 4.87542, 4.87758, 4.87973, 4.88188, 4.88403, 4.88617, 4.88831, 4.89045, 4.89259, 4.89472, 4.89685, 4.89898, 4.9011, 4.90323, 4.90535, 4.90746, 4.90958, 4.91169, 4.9138, 4.9159, 4.91801, 4.92011, 4.9222, 4.9243, 4.92639, 4.92848, 4.93057, 4.93265, 4.93473, 4.93681, 4.93889, 4.94096, 4.94303, 4.9451, 4.94717, 4.94923, 4.95129, 4.95335, 4.95541, 4.95746, 4.95951, 4.96156, 4.9636, 4.96565, 4.96769, 4.96973, 4.97176, 4.97379, 4.97583, 4.97785, 4.97988, 4.9819, 4.98392, 4.98594, 4.98796, 4.98997, 4.99198, 4.99399, 4.996, 4.998, 5, 5.002, 5.004, 5.00599, 5.00798, 5.00997, 5.01196, 5.01394, 5.01592, 5.0179, 5.01988, 5.02186, 5.02383, 5.0258, 5.02777, 5.02973, 5.0317, 5.03366, 5.03562, 5.03757, 5.03953, 5.04148, 5.04343, 5.04538, 5.04732, 5.04927, 5.05121, 5.05315, 5.05508, 5.05702, 5.05895, 5.06088, 5.06281, 5.06473, 5.06666, 5.06858, 5.0705, 5.07241, 5.07433, 5.07624, 5.07815, 5.08006, 5.08196, 5.08387, 5.08577, 5.08767, 5.08956, 5.09146, 5.09335, 5.09524, 5.09713, 5.09902, 5.1009, 5.10279, 5.10467, 5.10655, 5.10842, 5.1103, 5.11217, 5.11404, 5.11591, 5.11777, 5.11964, 5.1215, 5.12336, 5.12522, 5.12707, 5.12893, 5.13078, 5.13263, 5.13448, 5.13632, 5.13817, 5.14001, 5.14185, 5.14369, 5.14552, 5.14736, 5.14919, 5.15102, 5.15285, 5.15467, 5.1565, 5.15832, 5.16014, 5.16196, 5.16378, 5.16559, 5.1674, 5.16921, 5.17102, 5.17283, 5.17464, 5.17644, 5.17824, 5.18004, 5.18184, 5.18363, 5.18543, 5.18722, 5.18901, 5.1908, 5.19258, 5.19437, 5.19615, 5.19793, 5.19971, 5.20149, 5.20327, 5.20504, 5.20681, 5.20858, 5.21035, 5.21212, 5.21388, 5.21564, 5.21741, 5.21916, 5.22092, 5.22268, 5.22443, 5.22618, 5.22793, 5.22968, 5.23143, 5.23318, 5.23492, 5.23666, 5.2384, 5.24014, 5.24188, 5.24361, 5.24534, 5.24708, 5.24881, 5.25053, 5.25226, 5.25398, 5.25571, 5.25743, 5.25915, 5.26087, 5.26258, 5.2643, 5.26601, 5.26772, 5.26943, 5.27114, 5.27284, 5.27455, 5.27625, 5.27795, 5.27965, 5.28135, 5.28305, 5.28474, 5.28643, 5.28812, 5.28981, 5.2915, 5.29319, 5.29487, 5.29656, 5.29824, 5.29992, 5.3016, 5.30327, 5.30495, 5.30662, 5.3083, 5.30997, 5.31164, 5.3133, 5.31497, 5.31663, 5.3183, 5.31996, 5.32162, 5.32327, 5.32493, 5.32659, 5.32824, 5.32989, 5.33154, 5.33319, 5.33484, 5.33648, 5.33813, 5.33977, 5.34141, 5.34305, 5.34469, 5.34633, 5.34796, 5.3496, 5.35123, 5.35286, 5.35449, 5.35612, 5.35774, 5.35937, 5.36099, 5.36261, 5.36423, 5.36585, 5.36747, 5.36908, 5.3707, 5.37231, 5.37392, 5.37553, 5.37714, 5.37875, 5.38036, 5.38196, 5.38356, 5.38516, 5.38676, 5.38836, 5.38996, 5.39156, 5.39315, 5.39474, 5.39634, 5.39793, 5.39951, 5.4011, 5.40269, 5.40427, 5.40586, 5.40744, 5.40902, 5.4106, 5.41217, 5.41375, 5.41533, 5.4169, 5.41847, 5.42004, 5.42161, 5.42318, 5.42475, 5.42631, 5.42788, 5.42944, 5.431, 5.43256, 5.43412, 5.43568, 5.43723, 5.43879, 5.44034, 5.44189, 5.44344, 5.44499, 5.44654, 5.44809, 5.44963, 5.45118, 5.45272, 5.45426, 5.4558, 5.45734, 5.45888, 5.46041, 5.46195, 5.46348, 5.46501, 5.46654, 5.46807, 5.4696, 5.47113, 5.47266, 5.47418, 5.4757, 5.47723, 5.47875, 5.48027, 5.48178, 5.4833, 5.48482, 5.48633, 5.48784, 5.48936, 5.49087, 5.49238, 5.49389, 5.49539, 5.4969, 5.4984, 5.49991, 5.50141, 5.50291, 5.50441, 5.50591, 5.5074, 5.5089, 5.51039, 5.51189, 5.51338, 5.51487, 5.51636, 5.51785, 5.51934, 5.52082, 5.52231, 5.52379, 5.52528, 5.52676, 5.52824, 5.52972, 5.53119, 5.53267, 5.53415, 5.53562, 5.53709, 5.53857, 5.54004, 5.54151, 5.54298, 5.54444, 5.54591, 5.54737, 5.54884, 5.5503, 5.55176, 5.55322, 5.55468, 5.55614, 5.5576, 5.55905, 5.56051, 5.56196, 5.56341, 5.56487, 5.56632, 5.56776, 5.56921, 5.57066, 5.5721, 5.57355, 5.57499, 5.57643, 5.57788, 5.57932, 5.58075, 5.58219, 5.58363, 5.58506, 5.5865, 5.58793, 5.58936, 5.5908, 5.59223, 5.59365, 5.59508, 5.59651, 5.59794, 5.59936, 5.60078, 5.60221, 5.60363, 5.60505, 5.60647, 5.60788, 5.6093, 5.61072, 5.61213, 5.61355, 5.61496, 5.61637, 5.61778, 5.61919, 5.6206, 5.62201, 5.62341, 5.62482, 5.62622, 5.62763, 5.62903, 5.63043, 5.63183, 5.63323, 5.63463, 5.63602, 5.63742, 5.63881, 5.64021, 5.6416, 5.64299, 5.64438, 5.64577, 5.64716, 5.64855, 5.64994, 5.65132, 5.65271, 5.65409, 5.65547, 5.65685};
						int n[1024] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[1024];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 7) {
						float s_mean[2048] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 1132, 1133, 1134, 1135, 1136, 1137, 1138, 1139, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149, 1150, 1151, 1152, 1153, 1154, 1155, 1156, 1157, 1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187, 1188, 1189, 1190, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249, 1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259, 1260, 1261, 1262, 1263, 1264, 1265, 1266, 1267, 1268, 1269, 1270, 1271, 1272, 1273, 1274, 1275, 1276, 1277, 1278, 1279, 1280, 1281, 1282, 1283, 1284, 1285, 1286, 1287, 1288, 1289, 1290, 1291, 1292, 1293, 1294, 1295, 1296, 1297, 1298, 1299, 1300, 1301, 1302, 1303, 1304, 1305, 1306, 1307, 1308, 1309, 1310, 1311, 1312, 1313, 1314, 1315, 1316, 1317, 1318, 1319, 1320, 1321, 1322, 1323, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1332, 1333, 1334, 1335, 1336, 1337, 1338, 1339, 1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1371, 1372, 1373, 1374, 1375, 1376, 1377, 1378, 1379, 1380, 1381, 1382, 1383, 1384, 1385, 1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396, 1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405, 1406, 1407, 1408, 1409, 1410, 1411, 1412, 1413, 1414, 1415, 1416, 1417, 1418, 1419, 1420, 1421, 1422, 1423, 1424, 1425, 1426, 1427, 1428, 1429, 1430, 1431, 1432, 1433, 1434, 1435, 1436, 1437, 1438, 1439, 1440, 1441, 1442, 1443, 1444, 1445, 1446, 1447, 1448, 1449, 1450, 1451, 1452, 1453, 1454, 1455, 1456, 1457, 1458, 1459, 1460, 1461, 1462, 1463, 1464, 1465, 1466, 1467, 1468, 1469, 1470, 1471, 1472, 1473, 1474, 1475, 1476, 1477, 1478, 1479, 1480, 1481, 1482, 1483, 1484, 1485, 1486, 1487, 1488, 1489, 1490, 1491, 1492, 1493, 1494, 1495, 1496, 1497, 1498, 1499, 1500, 1501, 1502, 1503, 1504, 1505, 1506, 1507, 1508, 1509, 1510, 1511, 1512, 1513, 1514, 1515, 1516, 1517, 1518, 1519, 1520, 1521, 1522, 1523, 1524, 1525, 1526, 1527, 1528, 1529, 1530, 1531, 1532, 1533, 1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545, 1546, 1547, 1548, 1549, 1550, 1551, 1552, 1553, 1554, 1555, 1556, 1557, 1558, 1559, 1560, 1561, 1562, 1563, 1564, 1565, 1566, 1567, 1568, 1569, 1570, 1571, 1572, 1573, 1574, 1575, 1576, 1577, 1578, 1579, 1580, 1581, 1582, 1583, 1584, 1585, 1586, 1587, 1588, 1589, 1590, 1591, 1592, 1593, 1594, 1595, 1596, 1597, 1598, 1599, 1600, 1601, 1602, 1603, 1604, 1605, 1606, 1607, 1608, 1609, 1610, 1611, 1612, 1613, 1614, 1615, 1616, 1617, 1618, 1619, 1620, 1621, 1622, 1623, 1624, 1625, 1626, 1627, 1628, 1629, 1630, 1631, 1632, 1633, 1634, 1635, 1636, 1637, 1638, 1639, 1640, 1641, 1642, 1643, 1644, 1645, 1646, 1647, 1648, 1649, 1650, 1651, 1652, 1653, 1654, 1655, 1656, 1657, 1658, 1659, 1660, 1661, 1662, 1663, 1664, 1665, 1666, 1667, 1668, 1669, 1670, 1671, 1672, 1673, 1674, 1675, 1676, 1677, 1678, 1679, 1680, 1681, 1682, 1683, 1684, 1685, 1686, 1687, 1688, 1689, 1690, 1691, 1692, 1693, 1694, 1695, 1696, 1697, 1698, 1699, 1700, 1701, 1702, 1703, 1704, 1705, 1706, 1707, 1708, 1709, 1710, 1711, 1712, 1713, 1714, 1715, 1716, 1717, 1718, 1719, 1720, 1721, 1722, 1723, 1724, 1725, 1726, 1727, 1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736, 1737, 1738, 1739, 1740, 1741, 1742, 1743, 1744, 1745, 1746, 1747, 1748, 1749, 1750, 1751, 1752, 1753, 1754, 1755, 1756, 1757, 1758, 1759, 1760, 1761, 1762, 1763, 1764, 1765, 1766, 1767, 1768, 1769, 1770, 1771, 1772, 1773, 1774, 1775, 1776, 1777, 1778, 1779, 1780, 1781, 1782, 1783, 1784, 1785, 1786, 1787, 1788, 1789, 1790, 1791, 1792, 1793, 1794, 1795, 1796, 1797, 1798, 1799, 1800, 1801, 1802, 1803, 1804, 1805, 1806, 1807, 1808, 1809, 1810, 1811, 1812, 1813, 1814, 1815, 1816, 1817, 1818, 1819, 1820, 1821, 1822, 1823, 1824, 1825, 1826, 1827, 1828, 1829, 1830, 1831, 1832, 1833, 1834, 1835, 1836, 1837, 1838, 1839, 1840, 1841, 1842, 1843, 1844, 1845, 1846, 1847, 1848, 1849, 1850, 1851, 1852, 1853, 1854, 1855, 1856, 1857, 1858, 1859, 1860, 1861, 1862, 1863, 1864, 1865, 1866, 1867, 1868, 1869, 1870, 1871, 1872, 1873, 1874, 1875, 1876, 1877, 1878, 1879, 1880, 1881, 1882, 1883, 1884, 1885, 1886, 1887, 1888, 1889, 1890, 1891, 1892, 1893, 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909, 1910, 1911, 1912, 1913, 1914, 1915, 1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048};
						float s_var[2048] = { 1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2, 2.03054, 2.05977, 2.0878, 2.11474, 2.1407, 2.16574, 2.18994, 2.21336, 2.23607, 2.2581, 2.27951, 2.30033, 2.3206, 2.34035, 2.35961, 2.37841, 2.39678, 2.41474, 2.4323, 2.44949, 2.46633, 2.48282, 2.499, 2.51487, 2.53044, 2.54573, 2.56075, 2.57551, 2.59002, 2.60429, 2.61833, 2.63215, 2.64575, 2.65915, 2.67235, 2.68535, 2.69817, 2.71081, 2.72327, 2.73556, 2.7477, 2.75967, 2.77149, 2.78316, 2.79468, 2.80607, 2.81731, 2.82843, 2.83941, 2.85027, 2.86101, 2.87162, 2.88212, 2.89251, 2.90278, 2.91295, 2.92301, 2.93297, 2.94283, 2.95259, 2.96226, 2.97183, 2.98131, 2.9907, 3, 3.00922, 3.01835, 3.0274, 3.03637, 3.04526, 3.05408, 3.06281, 3.07148, 3.08007, 3.08859, 3.09704, 3.10542, 3.11374, 3.12199, 3.13017, 3.13829, 3.14635, 3.15434, 3.16228, 3.17015, 3.17797, 3.18573, 3.19344, 3.20109, 3.20868, 3.21622, 3.22371, 3.23115, 3.23853, 3.24587, 3.25315, 3.26039, 3.26758, 3.27472, 3.28182, 3.28887, 3.29587, 3.30283, 3.30975, 3.31662, 3.32346, 3.33025, 3.33699, 3.3437, 3.35037, 3.357, 3.36359, 3.37014, 3.37665, 3.38312, 3.38956, 3.39596, 3.40233, 3.40866, 3.41495, 3.42121, 3.42744, 3.43363, 3.43979, 3.44592, 3.45201, 3.45807, 3.4641, 3.4701, 3.47607, 3.482, 3.48791, 3.49379, 3.49964, 3.50545, 3.51124, 3.517, 3.52274, 3.52844, 3.53412, 3.53977, 3.54539, 3.55099, 3.55656, 3.5621, 3.56762, 3.57311, 3.57858, 3.58402, 3.58944, 3.59484, 3.60021, 3.60555, 3.61087, 3.61617, 3.62145, 3.6267, 3.63193, 3.63714, 3.64232, 3.64748, 3.65262, 3.65774, 3.66284, 3.66792, 3.67297, 3.67801, 3.68302, 3.68802, 3.69299, 3.69794, 3.70288, 3.70779, 3.71269, 3.71756, 3.72242, 3.72726, 3.73208, 3.73688, 3.74166, 3.74642, 3.75117, 3.75589, 3.7606, 3.7653, 3.76997, 3.77463, 3.77927, 3.78389, 3.7885, 3.79309, 3.79766, 3.80221, 3.80675, 3.81128, 3.81579, 3.82028, 3.82475, 3.82921, 3.83366, 3.83809, 3.8425, 3.8469, 3.85129, 3.85565, 3.86001, 3.86435, 3.86867, 3.87298, 3.87728, 3.88156, 3.88583, 3.89008, 3.89432, 3.89855, 3.90276, 3.90696, 3.91115, 3.91532, 3.91948, 3.92362, 3.92775, 3.93187, 3.93598, 3.94007, 3.94415, 3.94822, 3.95228, 3.95632, 3.96035, 3.96437, 3.96838, 3.97237, 3.97635, 3.98032, 3.98428, 3.98823, 3.99216, 3.99609, 4, 4.0039, 4.00779, 4.01167, 4.01553, 4.01939, 4.02323, 4.02707, 4.03089, 4.0347, 4.0385, 4.04229, 4.04607, 4.04984, 4.0536, 4.05735, 4.06109, 4.06481, 4.06853, 4.07224, 4.07594, 4.07962, 4.0833, 4.08697, 4.09062, 4.09427, 4.09791, 4.10154, 4.10516, 4.10876, 4.11236, 4.11595, 4.11953, 4.12311, 4.12667, 4.13022, 4.13376, 4.1373, 4.14082, 4.14434, 4.14785, 4.15135, 4.15484, 4.15832, 4.16179, 4.16526, 4.16871, 4.17216, 4.1756, 4.17902, 4.18245, 4.18586, 4.18926, 4.19266, 4.19605, 4.19943, 4.2028, 4.20616, 4.20952, 4.21287, 4.21621, 4.21954, 4.22286, 4.22618, 4.22949, 4.23279, 4.23608, 4.23936, 4.24264, 4.24591, 4.24917, 4.25243, 4.25568, 4.25892, 4.26215, 4.26537, 4.26859, 4.2718, 4.275, 4.2782, 4.28139, 4.28457, 4.28775, 4.29092, 4.29408, 4.29723, 4.30038, 4.30352, 4.30665, 4.30978, 4.3129, 4.31601, 4.31912, 4.32221, 4.32531, 4.32839, 4.33147, 4.33455, 4.33761, 4.34067, 4.34373, 4.34677, 4.34981, 4.35285, 4.35588, 4.3589, 4.36191, 4.36492, 4.36793, 4.37092, 4.37391, 4.3769, 4.37988, 4.38285, 4.38582, 4.38878, 4.39173, 4.39468, 4.39762, 4.40056, 4.40349, 4.40641, 4.40933, 4.41225, 4.41515, 4.41806, 4.42095, 4.42384, 4.42673, 4.42961, 4.43248, 4.43535, 4.43821, 4.44107, 4.44392, 4.44677, 4.44961, 4.45244, 4.45527, 4.45809, 4.46091, 4.46373, 4.46654, 4.46934, 4.47214, 4.47493, 4.47772, 4.4805, 4.48327, 4.48605, 4.48881, 4.49157, 4.49433, 4.49708, 4.49983, 4.50257, 4.50531, 4.50804, 4.51076, 4.51349, 4.5162, 4.51891, 4.52162, 4.52432, 4.52702, 4.52971, 4.5324, 4.53508, 4.53776, 4.54043, 4.5431, 4.54576, 4.54842, 4.55108, 4.55373, 4.55637, 4.55901, 4.56165, 4.56428, 4.56691, 4.56953, 4.57215, 4.57476, 4.57737, 4.57998, 4.58258, 4.58517, 4.58776, 4.59035, 4.59293, 4.59551, 4.59808, 4.60065, 4.60322, 4.60578, 4.60834, 4.61089, 4.61344, 4.61598, 4.61852, 4.62106, 4.62359, 4.62611, 4.62864, 4.63116, 4.63367, 4.63618, 4.63869, 4.64119, 4.64369, 4.64618, 4.64868, 4.65116, 4.65364, 4.65612, 4.6586, 4.66107, 4.66354, 4.666, 4.66846, 4.67091, 4.67336, 4.67581, 4.67825, 4.68069, 4.68313, 4.68556, 4.68799, 4.69042, 4.69284, 4.69525, 4.69767, 4.70008, 4.70248, 4.70489, 4.70728, 4.70968, 4.71207, 4.71446, 4.71684, 4.71922, 4.7216, 4.72397, 4.72634, 4.72871, 4.73107, 4.73343, 4.73579, 4.73814, 4.74049, 4.74283, 4.74517, 4.74751, 4.74985, 4.75218, 4.7545, 4.75683, 4.75915, 4.76147, 4.76378, 4.76609, 4.7684, 4.7707, 4.773, 4.7753, 4.7776, 4.77989, 4.78217, 4.78446, 4.78674, 4.78902, 4.79129, 4.79356, 4.79583, 4.7981, 4.80036, 4.80262, 4.80487, 4.80712, 4.80937, 4.81162, 4.81386, 4.8161, 4.81834, 4.82057, 4.8228, 4.82503, 4.82725, 4.82947, 4.83169, 4.83391, 4.83612, 4.83833, 4.84053, 4.84273, 4.84493, 4.84713, 4.84932, 4.85152, 4.8537, 4.85589, 4.85807, 4.86025, 4.86243, 4.8646, 4.86677, 4.86894, 4.8711, 4.87326, 4.87542, 4.87758, 4.87973, 4.88188, 4.88403, 4.88617, 4.88831, 4.89045, 4.89259, 4.89472, 4.89685, 4.89898, 4.9011, 4.90323, 4.90535, 4.90746, 4.90958, 4.91169, 4.9138, 4.9159, 4.91801, 4.92011, 4.9222, 4.9243, 4.92639, 4.92848, 4.93057, 4.93265, 4.93473, 4.93681, 4.93889, 4.94096, 4.94303, 4.9451, 4.94717, 4.94923, 4.95129, 4.95335, 4.95541, 4.95746, 4.95951, 4.96156, 4.9636, 4.96565, 4.96769, 4.96973, 4.97176, 4.97379, 4.97583, 4.97785, 4.97988, 4.9819, 4.98392, 4.98594, 4.98796, 4.98997, 4.99198, 4.99399, 4.996, 4.998, 5, 5.002, 5.004, 5.00599, 5.00798, 5.00997, 5.01196, 5.01394, 5.01592, 5.0179, 5.01988, 5.02186, 5.02383, 5.0258, 5.02777, 5.02973, 5.0317, 5.03366, 5.03562, 5.03757, 5.03953, 5.04148, 5.04343, 5.04538, 5.04732, 5.04927, 5.05121, 5.05315, 5.05508, 5.05702, 5.05895, 5.06088, 5.06281, 5.06473, 5.06666, 5.06858, 5.0705, 5.07241, 5.07433, 5.07624, 5.07815, 5.08006, 5.08196, 5.08387, 5.08577, 5.08767, 5.08956, 5.09146, 5.09335, 5.09524, 5.09713, 5.09902, 5.1009, 5.10279, 5.10467, 5.10655, 5.10842, 5.1103, 5.11217, 5.11404, 5.11591, 5.11777, 5.11964, 5.1215, 5.12336, 5.12522, 5.12707, 5.12893, 5.13078, 5.13263, 5.13448, 5.13632, 5.13817, 5.14001, 5.14185, 5.14369, 5.14552, 5.14736, 5.14919, 5.15102, 5.15285, 5.15467, 5.1565, 5.15832, 5.16014, 5.16196, 5.16378, 5.16559, 5.1674, 5.16921, 5.17102, 5.17283, 5.17464, 5.17644, 5.17824, 5.18004, 5.18184, 5.18363, 5.18543, 5.18722, 5.18901, 5.1908, 5.19258, 5.19437, 5.19615, 5.19793, 5.19971, 5.20149, 5.20327, 5.20504, 5.20681, 5.20858, 5.21035, 5.21212, 5.21388, 5.21564, 5.21741, 5.21916, 5.22092, 5.22268, 5.22443, 5.22618, 5.22793, 5.22968, 5.23143, 5.23318, 5.23492, 5.23666, 5.2384, 5.24014, 5.24188, 5.24361, 5.24534, 5.24708, 5.24881, 5.25053, 5.25226, 5.25398, 5.25571, 5.25743, 5.25915, 5.26087, 5.26258, 5.2643, 5.26601, 5.26772, 5.26943, 5.27114, 5.27284, 5.27455, 5.27625, 5.27795, 5.27965, 5.28135, 5.28305, 5.28474, 5.28643, 5.28812, 5.28981, 5.2915, 5.29319, 5.29487, 5.29656, 5.29824, 5.29992, 5.3016, 5.30327, 5.30495, 5.30662, 5.3083, 5.30997, 5.31164, 5.3133, 5.31497, 5.31663, 5.3183, 5.31996, 5.32162, 5.32327, 5.32493, 5.32659, 5.32824, 5.32989, 5.33154, 5.33319, 5.33484, 5.33648, 5.33813, 5.33977, 5.34141, 5.34305, 5.34469, 5.34633, 5.34796, 5.3496, 5.35123, 5.35286, 5.35449, 5.35612, 5.35774, 5.35937, 5.36099, 5.36261, 5.36423, 5.36585, 5.36747, 5.36908, 5.3707, 5.37231, 5.37392, 5.37553, 5.37714, 5.37875, 5.38036, 5.38196, 5.38356, 5.38516, 5.38676, 5.38836, 5.38996, 5.39156, 5.39315, 5.39474, 5.39634, 5.39793, 5.39951, 5.4011, 5.40269, 5.40427, 5.40586, 5.40744, 5.40902, 5.4106, 5.41217, 5.41375, 5.41533, 5.4169, 5.41847, 5.42004, 5.42161, 5.42318, 5.42475, 5.42631, 5.42788, 5.42944, 5.431, 5.43256, 5.43412, 5.43568, 5.43723, 5.43879, 5.44034, 5.44189, 5.44344, 5.44499, 5.44654, 5.44809, 5.44963, 5.45118, 5.45272, 5.45426, 5.4558, 5.45734, 5.45888, 5.46041, 5.46195, 5.46348, 5.46501, 5.46654, 5.46807, 5.4696, 5.47113, 5.47266, 5.47418, 5.4757, 5.47723, 5.47875, 5.48027, 5.48178, 5.4833, 5.48482, 5.48633, 5.48784, 5.48936, 5.49087, 5.49238, 5.49389, 5.49539, 5.4969, 5.4984, 5.49991, 5.50141, 5.50291, 5.50441, 5.50591, 5.5074, 5.5089, 5.51039, 5.51189, 5.51338, 5.51487, 5.51636, 5.51785, 5.51934, 5.52082, 5.52231, 5.52379, 5.52528, 5.52676, 5.52824, 5.52972, 5.53119, 5.53267, 5.53415, 5.53562, 5.53709, 5.53857, 5.54004, 5.54151, 5.54298, 5.54444, 5.54591, 5.54737, 5.54884, 5.5503, 5.55176, 5.55322, 5.55468, 5.55614, 5.5576, 5.55905, 5.56051, 5.56196, 5.56341, 5.56487, 5.56632, 5.56776, 5.56921, 5.57066, 5.5721, 5.57355, 5.57499, 5.57643, 5.57788, 5.57932, 5.58075, 5.58219, 5.58363, 5.58506, 5.5865, 5.58793, 5.58936, 5.5908, 5.59223, 5.59365, 5.59508, 5.59651, 5.59794, 5.59936, 5.60078, 5.60221, 5.60363, 5.60505, 5.60647, 5.60788, 5.6093, 5.61072, 5.61213, 5.61355, 5.61496, 5.61637, 5.61778, 5.61919, 5.6206, 5.62201, 5.62341, 5.62482, 5.62622, 5.62763, 5.62903, 5.63043, 5.63183, 5.63323, 5.63463, 5.63602, 5.63742, 5.63881, 5.64021, 5.6416, 5.64299, 5.64438, 5.64577, 5.64716, 5.64855, 5.64994, 5.65132, 5.65271, 5.65409, 5.65547, 5.65685, 5.65823, 5.65961, 5.66099, 5.66237, 5.66375, 5.66512, 5.6665, 5.66787, 5.66924, 5.67061, 5.67199, 5.67335, 5.67472, 5.67609, 5.67746, 5.67882, 5.68019, 5.68155, 5.68291, 5.68428, 5.68564, 5.687, 5.68835, 5.68971, 5.69107, 5.69243, 5.69378, 5.69513, 5.69649, 5.69784, 5.69919, 5.70054, 5.70189, 5.70324, 5.70458, 5.70593, 5.70728, 5.70862, 5.70996, 5.71131, 5.71265, 5.71399, 5.71533, 5.71667, 5.718, 5.71934, 5.72068, 5.72201, 5.72335, 5.72468, 5.72601, 5.72734, 5.72867, 5.73, 5.73133, 5.73266, 5.73398, 5.73531, 5.73663, 5.73796, 5.73928, 5.7406, 5.74192, 5.74324, 5.74456, 5.74588, 5.7472, 5.74851, 5.74983, 5.75115, 5.75246, 5.75377, 5.75508, 5.75639, 5.75771, 5.75901, 5.76032, 5.76163, 5.76294, 5.76424, 5.76555, 5.76685, 5.76815, 5.76946, 5.77076, 5.77206, 5.77336, 5.77466, 5.77595, 5.77725, 5.77855, 5.77984, 5.78114, 5.78243, 5.78372, 5.78502, 5.78631, 5.7876, 5.78889, 5.79017, 5.79146, 5.79275, 5.79403, 5.79532, 5.7966, 5.79789, 5.79917, 5.80045, 5.80173, 5.80301, 5.80429, 5.80557, 5.80684, 5.80812, 5.8094, 5.81067, 5.81194, 5.81322, 5.81449, 5.81576, 5.81703, 5.8183, 5.81957, 5.82084, 5.8221, 5.82337, 5.82464, 5.8259, 5.82717, 5.82843, 5.82969, 5.83095, 5.83221, 5.83347, 5.83473, 5.83599, 5.83725, 5.8385, 5.83976, 5.84101, 5.84227, 5.84352, 5.84477, 5.84603, 5.84728, 5.84853, 5.84978, 5.85102, 5.85227, 5.85352, 5.85476, 5.85601, 5.85725, 5.8585, 5.85974, 5.86098, 5.86222, 5.86347, 5.86471, 5.86594, 5.86718, 5.86842, 5.86966, 5.87089, 5.87213, 5.87336, 5.8746, 5.87583, 5.87706, 5.87829, 5.87952, 5.88075, 5.88198, 5.88321, 5.88444, 5.88566, 5.88689, 5.88811, 5.88934, 5.89056, 5.89178, 5.89301, 5.89423, 5.89545, 5.89667, 5.89789, 5.8991, 5.90032, 5.90154, 5.90275, 5.90397, 5.90518, 5.9064, 5.90761, 5.90882, 5.91003, 5.91124, 5.91245, 5.91366, 5.91487, 5.91608, 5.91729, 5.91849, 5.9197, 5.9209, 5.92211, 5.92331, 5.92451, 5.92572, 5.92692, 5.92812, 5.92932, 5.93052, 5.93171, 5.93291, 5.93411, 5.9353, 5.9365, 5.93769, 5.93889, 5.94008, 5.94127, 5.94246, 5.94366, 5.94485, 5.94604, 5.94722, 5.94841, 5.9496, 5.95079, 5.95197, 5.95316, 5.95434, 5.95553, 5.95671, 5.95789, 5.95907, 5.96025, 5.96144, 5.96262, 5.96379, 5.96497, 5.96615, 5.96733, 5.9685, 5.96968, 5.97085, 5.97203, 5.9732, 5.97437, 5.97555, 5.97672, 5.97789, 5.97906, 5.98023, 5.9814, 5.98256, 5.98373, 5.9849, 5.98606, 5.98723, 5.98839, 5.98956, 5.99072, 5.99188, 5.99304, 5.9942, 5.99537, 5.99652, 5.99768, 5.99884, 6, 6.00116, 6.00231, 6.00347, 6.00462, 6.00578, 6.00693, 6.00809, 6.00924, 6.01039, 6.01154, 6.01269, 6.01384, 6.01499, 6.01614, 6.01729, 6.01843, 6.01958, 6.02073, 6.02187, 6.02302, 6.02416, 6.0253, 6.02645, 6.02759, 6.02873, 6.02987, 6.03101, 6.03215, 6.03329, 6.03442, 6.03556, 6.0367, 6.03784, 6.03897, 6.04011, 6.04124, 6.04237, 6.04351, 6.04464, 6.04577, 6.0469, 6.04803, 6.04916, 6.05029, 6.05142, 6.05255, 6.05367, 6.0548, 6.05593, 6.05705, 6.05818, 6.0593, 6.06042, 6.06155, 6.06267, 6.06379, 6.06491, 6.06603, 6.06715, 6.06827, 6.06939, 6.07051, 6.07162, 6.07274, 6.07386, 6.07497, 6.07609, 6.0772, 6.07831, 6.07943, 6.08054, 6.08165, 6.08276, 6.08387, 6.08498, 6.08609, 6.0872, 6.08831, 6.08942, 6.09052, 6.09163, 6.09274, 6.09384, 6.09494, 6.09605, 6.09715, 6.09825, 6.09936, 6.10046, 6.10156, 6.10266, 6.10376, 6.10486, 6.10596, 6.10705, 6.10815, 6.10925, 6.11034, 6.11144, 6.11253, 6.11363, 6.11472, 6.11582, 6.11691, 6.118, 6.11909, 6.12018, 6.12127, 6.12236, 6.12345, 6.12454, 6.12563, 6.12672, 6.1278, 6.12889, 6.12997, 6.13106, 6.13214, 6.13323, 6.13431, 6.13539, 6.13648, 6.13756, 6.13864, 6.13972, 6.1408, 6.14188, 6.14296, 6.14404, 6.14511, 6.14619, 6.14727, 6.14834, 6.14942, 6.15049, 6.15157, 6.15264, 6.15371, 6.15479, 6.15586, 6.15693, 6.158, 6.15907, 6.16014, 6.16121, 6.16228, 6.16335, 6.16441, 6.16548, 6.16655, 6.16761, 6.16868, 6.16974, 6.17081, 6.17187, 6.17293, 6.174, 6.17506, 6.17612, 6.17718, 6.17824, 6.1793, 6.18036, 6.18142, 6.18248, 6.18354, 6.18459, 6.18565, 6.1867, 6.18776, 6.18882, 6.18987, 6.19092, 6.19198, 6.19303, 6.19408, 6.19513, 6.19618, 6.19724, 6.19829, 6.19934, 6.20038, 6.20143, 6.20248, 6.20353, 6.20458, 6.20562, 6.20667, 6.20771, 6.20876, 6.2098, 6.21085, 6.21189, 6.21293, 6.21397, 6.21502, 6.21606, 6.2171, 6.21814, 6.21918, 6.22022, 6.22125, 6.22229, 6.22333, 6.22437, 6.2254, 6.22644, 6.22747, 6.22851, 6.22954, 6.23058, 6.23161, 6.23264, 6.23368, 6.23471, 6.23574, 6.23677, 6.2378, 6.23883, 6.23986, 6.24089, 6.24192, 6.24294, 6.24397, 6.245, 6.24602, 6.24705, 6.24808, 6.2491, 6.25012, 6.25115, 6.25217, 6.25319, 6.25422, 6.25524, 6.25626, 6.25728, 6.2583, 6.25932, 6.26034, 6.26136, 6.26238, 6.26339, 6.26441, 6.26543, 6.26644, 6.26746, 6.26847, 6.26949, 6.2705, 6.27152, 6.27253, 6.27354, 6.27455, 6.27557, 6.27658, 6.27759, 6.2786, 6.27961, 6.28062, 6.28163, 6.28264, 6.28364, 6.28465, 6.28566, 6.28666, 6.28767, 6.28868, 6.28968, 6.29069, 6.29169, 6.29269, 6.2937, 6.2947, 6.2957, 6.2967, 6.2977, 6.2987, 6.2997, 6.3007, 6.3017, 6.3027, 6.3037, 6.3047, 6.3057, 6.30669, 6.30769, 6.30868, 6.30968, 6.31067, 6.31167, 6.31266, 6.31366, 6.31465, 6.31564, 6.31663, 6.31763, 6.31862, 6.31961, 6.3206, 6.32159, 6.32258, 6.32357, 6.32456, 6.32554, 6.32653, 6.32752, 6.3285, 6.32949, 6.33048, 6.33146, 6.33245, 6.33343, 6.33441, 6.3354, 6.33638, 6.33736, 6.33835, 6.33933, 6.34031, 6.34129, 6.34227, 6.34325, 6.34423, 6.34521, 6.34618, 6.34716, 6.34814, 6.34912, 6.35009, 6.35107, 6.35205, 6.35302, 6.354, 6.35497, 6.35594, 6.35692, 6.35789, 6.35886, 6.35983, 6.36081, 6.36178, 6.36275, 6.36372, 6.36469, 6.36566, 6.36663, 6.3676, 6.36856, 6.36953, 6.3705, 6.37147, 6.37243, 6.3734, 6.37436, 6.37533, 6.37629, 6.37726, 6.37822, 6.37918, 6.38015, 6.38111, 6.38207, 6.38303, 6.38399, 6.38495, 6.38591, 6.38687, 6.38783, 6.38879, 6.38975, 6.39071, 6.39167, 6.39262, 6.39358, 6.39454, 6.39549, 6.39645, 6.3974, 6.39836, 6.39931, 6.40027, 6.40122, 6.40217, 6.40312, 6.40408, 6.40503, 6.40598, 6.40693, 6.40788, 6.40883, 6.40978, 6.41073, 6.41168, 6.41263, 6.41357, 6.41452, 6.41547, 6.41641, 6.41736, 6.41831, 6.41925, 6.4202, 6.42114, 6.42209, 6.42303, 6.42397, 6.42492, 6.42586, 6.4268, 6.42774, 6.42868, 6.42962, 6.43056, 6.4315, 6.43244, 6.43338, 6.43432, 6.43526, 6.4362, 6.43713, 6.43807, 6.43901, 6.43994, 6.44088, 6.44182, 6.44275, 6.44369, 6.44462, 6.44555, 6.44649, 6.44742, 6.44835, 6.44928, 6.45022, 6.45115, 6.45208, 6.45301, 6.45394, 6.45487, 6.4558, 6.45673, 6.45766, 6.45858, 6.45951, 6.46044, 6.46137, 6.46229, 6.46322, 6.46414, 6.46507, 6.46599, 6.46692, 6.46784, 6.46877, 6.46969, 6.47061, 6.47154, 6.47246, 6.47338, 6.4743, 6.47522, 6.47614, 6.47706, 6.47798, 6.4789, 6.47982, 6.48074, 6.48166, 6.48258, 6.48349, 6.48441, 6.48533, 6.48624, 6.48716, 6.48808, 6.48899, 6.48991, 6.49082, 6.49173, 6.49265, 6.49356, 6.49447, 6.49539, 6.4963, 6.49721, 6.49812, 6.49903, 6.49994, 6.50085, 6.50176, 6.50267, 6.50358, 6.50449, 6.5054, 6.50631, 6.50721, 6.50812, 6.50903, 6.50993, 6.51084, 6.51175, 6.51265, 6.51356, 6.51446, 6.51536, 6.51627, 6.51717, 6.51807, 6.51898, 6.51988, 6.52078, 6.52168, 6.52258, 6.52348, 6.52438, 6.52528, 6.52618, 6.52708, 6.52798, 6.52888, 6.52978, 6.53068, 6.53157, 6.53247, 6.53337, 6.53426, 6.53516, 6.53606, 6.53695, 6.53785, 6.53874, 6.53963, 6.54053, 6.54142, 6.54231, 6.54321, 6.5441, 6.54499, 6.54588, 6.54677, 6.54766, 6.54855, 6.54944, 6.55033, 6.55122, 6.55211, 6.553, 6.55389, 6.55478, 6.55566, 6.55655, 6.55744, 6.55832, 6.55921, 6.5601, 6.56098, 6.56187, 6.56275, 6.56364, 6.56452, 6.5654, 6.56629, 6.56717, 6.56805, 6.56893, 6.56982, 6.5707, 6.57158, 6.57246, 6.57334, 6.57422, 6.5751, 6.57598, 6.57686, 6.57774, 6.57861, 6.57949, 6.58037, 6.58125, 6.58212, 6.583, 6.58388, 6.58475, 6.58563, 6.5865, 6.58738, 6.58825, 6.58913, 6.59, 6.59087, 6.59175, 6.59262, 6.59349, 6.59436, 6.59524, 6.59611, 6.59698, 6.59785, 6.59872, 6.59959, 6.60046, 6.60133, 6.6022, 6.60306, 6.60393, 6.6048, 6.60567, 6.60654, 6.6074, 6.60827, 6.60913, 6.61, 6.61087, 6.61173, 6.6126, 6.61346, 6.61432, 6.61519, 6.61605, 6.61691, 6.61778, 6.61864, 6.6195, 6.62036, 6.62122, 6.62209, 6.62295, 6.62381, 6.62467, 6.62553, 6.62639, 6.62725, 6.6281, 6.62896, 6.62982, 6.63068, 6.63154, 6.63239, 6.63325, 6.63411, 6.63496, 6.63582, 6.63667, 6.63753, 6.63838, 6.63924, 6.64009, 6.64095, 6.6418, 6.64265, 6.6435, 6.64436, 6.64521, 6.64606, 6.64691, 6.64776, 6.64861, 6.64946, 6.65031, 6.65116, 6.65201, 6.65286, 6.65371, 6.65456, 6.65541, 6.65626, 6.6571, 6.65795, 6.6588, 6.65965, 6.66049, 6.66134, 6.66218, 6.66303, 6.66387, 6.66472, 6.66556, 6.66641, 6.66725, 6.66809, 6.66894, 6.66978, 6.67062, 6.67146, 6.67231, 6.67315, 6.67399, 6.67483, 6.67567, 6.67651, 6.67735, 6.67819, 6.67903, 6.67987, 6.68071, 6.68154, 6.68238, 6.68322, 6.68406, 6.68489, 6.68573, 6.68657, 6.6874, 6.68824, 6.68907, 6.68991, 6.69074, 6.69158, 6.69241, 6.69325, 6.69408, 6.69491, 6.69575, 6.69658, 6.69741, 6.69824, 6.69908, 6.69991, 6.70074, 6.70157, 6.7024, 6.70323, 6.70406, 6.70489, 6.70572, 6.70655, 6.70738, 6.7082, 6.70903, 6.70986, 6.71069, 6.71151, 6.71234, 6.71317, 6.71399, 6.71482, 6.71565, 6.71647, 6.7173, 6.71812, 6.71894, 6.71977, 6.72059, 6.72142, 6.72224, 6.72306, 6.72388, 6.72471, 6.72553, 6.72635, 6.72717};
						int n[2048] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8 };
						int an[2048];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 8) {
						float s_mean[4096] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 1132, 1133, 1134, 1135, 1136, 1137, 1138, 1139, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149, 1150, 1151, 1152, 1153, 1154, 1155, 1156, 1157, 1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187, 1188, 1189, 1190, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249, 1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259, 1260, 1261, 1262, 1263, 1264, 1265, 1266, 1267, 1268, 1269, 1270, 1271, 1272, 1273, 1274, 1275, 1276, 1277, 1278, 1279, 1280, 1281, 1282, 1283, 1284, 1285, 1286, 1287, 1288, 1289, 1290, 1291, 1292, 1293, 1294, 1295, 1296, 1297, 1298, 1299, 1300, 1301, 1302, 1303, 1304, 1305, 1306, 1307, 1308, 1309, 1310, 1311, 1312, 1313, 1314, 1315, 1316, 1317, 1318, 1319, 1320, 1321, 1322, 1323, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1332, 1333, 1334, 1335, 1336, 1337, 1338, 1339, 1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1371, 1372, 1373, 1374, 1375, 1376, 1377, 1378, 1379, 1380, 1381, 1382, 1383, 1384, 1385, 1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396, 1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405, 1406, 1407, 1408, 1409, 1410, 1411, 1412, 1413, 1414, 1415, 1416, 1417, 1418, 1419, 1420, 1421, 1422, 1423, 1424, 1425, 1426, 1427, 1428, 1429, 1430, 1431, 1432, 1433, 1434, 1435, 1436, 1437, 1438, 1439, 1440, 1441, 1442, 1443, 1444, 1445, 1446, 1447, 1448, 1449, 1450, 1451, 1452, 1453, 1454, 1455, 1456, 1457, 1458, 1459, 1460, 1461, 1462, 1463, 1464, 1465, 1466, 1467, 1468, 1469, 1470, 1471, 1472, 1473, 1474, 1475, 1476, 1477, 1478, 1479, 1480, 1481, 1482, 1483, 1484, 1485, 1486, 1487, 1488, 1489, 1490, 1491, 1492, 1493, 1494, 1495, 1496, 1497, 1498, 1499, 1500, 1501, 1502, 1503, 1504, 1505, 1506, 1507, 1508, 1509, 1510, 1511, 1512, 1513, 1514, 1515, 1516, 1517, 1518, 1519, 1520, 1521, 1522, 1523, 1524, 1525, 1526, 1527, 1528, 1529, 1530, 1531, 1532, 1533, 1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545, 1546, 1547, 1548, 1549, 1550, 1551, 1552, 1553, 1554, 1555, 1556, 1557, 1558, 1559, 1560, 1561, 1562, 1563, 1564, 1565, 1566, 1567, 1568, 1569, 1570, 1571, 1572, 1573, 1574, 1575, 1576, 1577, 1578, 1579, 1580, 1581, 1582, 1583, 1584, 1585, 1586, 1587, 1588, 1589, 1590, 1591, 1592, 1593, 1594, 1595, 1596, 1597, 1598, 1599, 1600, 1601, 1602, 1603, 1604, 1605, 1606, 1607, 1608, 1609, 1610, 1611, 1612, 1613, 1614, 1615, 1616, 1617, 1618, 1619, 1620, 1621, 1622, 1623, 1624, 1625, 1626, 1627, 1628, 1629, 1630, 1631, 1632, 1633, 1634, 1635, 1636, 1637, 1638, 1639, 1640, 1641, 1642, 1643, 1644, 1645, 1646, 1647, 1648, 1649, 1650, 1651, 1652, 1653, 1654, 1655, 1656, 1657, 1658, 1659, 1660, 1661, 1662, 1663, 1664, 1665, 1666, 1667, 1668, 1669, 1670, 1671, 1672, 1673, 1674, 1675, 1676, 1677, 1678, 1679, 1680, 1681, 1682, 1683, 1684, 1685, 1686, 1687, 1688, 1689, 1690, 1691, 1692, 1693, 1694, 1695, 1696, 1697, 1698, 1699, 1700, 1701, 1702, 1703, 1704, 1705, 1706, 1707, 1708, 1709, 1710, 1711, 1712, 1713, 1714, 1715, 1716, 1717, 1718, 1719, 1720, 1721, 1722, 1723, 1724, 1725, 1726, 1727, 1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736, 1737, 1738, 1739, 1740, 1741, 1742, 1743, 1744, 1745, 1746, 1747, 1748, 1749, 1750, 1751, 1752, 1753, 1754, 1755, 1756, 1757, 1758, 1759, 1760, 1761, 1762, 1763, 1764, 1765, 1766, 1767, 1768, 1769, 1770, 1771, 1772, 1773, 1774, 1775, 1776, 1777, 1778, 1779, 1780, 1781, 1782, 1783, 1784, 1785, 1786, 1787, 1788, 1789, 1790, 1791, 1792, 1793, 1794, 1795, 1796, 1797, 1798, 1799, 1800, 1801, 1802, 1803, 1804, 1805, 1806, 1807, 1808, 1809, 1810, 1811, 1812, 1813, 1814, 1815, 1816, 1817, 1818, 1819, 1820, 1821, 1822, 1823, 1824, 1825, 1826, 1827, 1828, 1829, 1830, 1831, 1832, 1833, 1834, 1835, 1836, 1837, 1838, 1839, 1840, 1841, 1842, 1843, 1844, 1845, 1846, 1847, 1848, 1849, 1850, 1851, 1852, 1853, 1854, 1855, 1856, 1857, 1858, 1859, 1860, 1861, 1862, 1863, 1864, 1865, 1866, 1867, 1868, 1869, 1870, 1871, 1872, 1873, 1874, 1875, 1876, 1877, 1878, 1879, 1880, 1881, 1882, 1883, 1884, 1885, 1886, 1887, 1888, 1889, 1890, 1891, 1892, 1893, 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909, 1910, 1911, 1912, 1913, 1914, 1915, 1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049, 2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2059, 2060, 2061, 2062, 2063, 2064, 2065, 2066, 2067, 2068, 2069, 2070, 2071, 2072, 2073, 2074, 2075, 2076, 2077, 2078, 2079, 2080, 2081, 2082, 2083, 2084, 2085, 2086, 2087, 2088, 2089, 2090, 2091, 2092, 2093, 2094, 2095, 2096, 2097, 2098, 2099, 2100, 2101, 2102, 2103, 2104, 2105, 2106, 2107, 2108, 2109, 2110, 2111, 2112, 2113, 2114, 2115, 2116, 2117, 2118, 2119, 2120, 2121, 2122, 2123, 2124, 2125, 2126, 2127, 2128, 2129, 2130, 2131, 2132, 2133, 2134, 2135, 2136, 2137, 2138, 2139, 2140, 2141, 2142, 2143, 2144, 2145, 2146, 2147, 2148, 2149, 2150, 2151, 2152, 2153, 2154, 2155, 2156, 2157, 2158, 2159, 2160, 2161, 2162, 2163, 2164, 2165, 2166, 2167, 2168, 2169, 2170, 2171, 2172, 2173, 2174, 2175, 2176, 2177, 2178, 2179, 2180, 2181, 2182, 2183, 2184, 2185, 2186, 2187, 2188, 2189, 2190, 2191, 2192, 2193, 2194, 2195, 2196, 2197, 2198, 2199, 2200, 2201, 2202, 2203, 2204, 2205, 2206, 2207, 2208, 2209, 2210, 2211, 2212, 2213, 2214, 2215, 2216, 2217, 2218, 2219, 2220, 2221, 2222, 2223, 2224, 2225, 2226, 2227, 2228, 2229, 2230, 2231, 2232, 2233, 2234, 2235, 2236, 2237, 2238, 2239, 2240, 2241, 2242, 2243, 2244, 2245, 2246, 2247, 2248, 2249, 2250, 2251, 2252, 2253, 2254, 2255, 2256, 2257, 2258, 2259, 2260, 2261, 2262, 2263, 2264, 2265, 2266, 2267, 2268, 2269, 2270, 2271, 2272, 2273, 2274, 2275, 2276, 2277, 2278, 2279, 2280, 2281, 2282, 2283, 2284, 2285, 2286, 2287, 2288, 2289, 2290, 2291, 2292, 2293, 2294, 2295, 2296, 2297, 2298, 2299, 2300, 2301, 2302, 2303, 2304, 2305, 2306, 2307, 2308, 2309, 2310, 2311, 2312, 2313, 2314, 2315, 2316, 2317, 2318, 2319, 2320, 2321, 2322, 2323, 2324, 2325, 2326, 2327, 2328, 2329, 2330, 2331, 2332, 2333, 2334, 2335, 2336, 2337, 2338, 2339, 2340, 2341, 2342, 2343, 2344, 2345, 2346, 2347, 2348, 2349, 2350, 2351, 2352, 2353, 2354, 2355, 2356, 2357, 2358, 2359, 2360, 2361, 2362, 2363, 2364, 2365, 2366, 2367, 2368, 2369, 2370, 2371, 2372, 2373, 2374, 2375, 2376, 2377, 2378, 2379, 2380, 2381, 2382, 2383, 2384, 2385, 2386, 2387, 2388, 2389, 2390, 2391, 2392, 2393, 2394, 2395, 2396, 2397, 2398, 2399, 2400, 2401, 2402, 2403, 2404, 2405, 2406, 2407, 2408, 2409, 2410, 2411, 2412, 2413, 2414, 2415, 2416, 2417, 2418, 2419, 2420, 2421, 2422, 2423, 2424, 2425, 2426, 2427, 2428, 2429, 2430, 2431, 2432, 2433, 2434, 2435, 2436, 2437, 2438, 2439, 2440, 2441, 2442, 2443, 2444, 2445, 2446, 2447, 2448, 2449, 2450, 2451, 2452, 2453, 2454, 2455, 2456, 2457, 2458, 2459, 2460, 2461, 2462, 2463, 2464, 2465, 2466, 2467, 2468, 2469, 2470, 2471, 2472, 2473, 2474, 2475, 2476, 2477, 2478, 2479, 2480, 2481, 2482, 2483, 2484, 2485, 2486, 2487, 2488, 2489, 2490, 2491, 2492, 2493, 2494, 2495, 2496, 2497, 2498, 2499, 2500, 2501, 2502, 2503, 2504, 2505, 2506, 2507, 2508, 2509, 2510, 2511, 2512, 2513, 2514, 2515, 2516, 2517, 2518, 2519, 2520, 2521, 2522, 2523, 2524, 2525, 2526, 2527, 2528, 2529, 2530, 2531, 2532, 2533, 2534, 2535, 2536, 2537, 2538, 2539, 2540, 2541, 2542, 2543, 2544, 2545, 2546, 2547, 2548, 2549, 2550, 2551, 2552, 2553, 2554, 2555, 2556, 2557, 2558, 2559, 2560, 2561, 2562, 2563, 2564, 2565, 2566, 2567, 2568, 2569, 2570, 2571, 2572, 2573, 2574, 2575, 2576, 2577, 2578, 2579, 2580, 2581, 2582, 2583, 2584, 2585, 2586, 2587, 2588, 2589, 2590, 2591, 2592, 2593, 2594, 2595, 2596, 2597, 2598, 2599, 2600, 2601, 2602, 2603, 2604, 2605, 2606, 2607, 2608, 2609, 2610, 2611, 2612, 2613, 2614, 2615, 2616, 2617, 2618, 2619, 2620, 2621, 2622, 2623, 2624, 2625, 2626, 2627, 2628, 2629, 2630, 2631, 2632, 2633, 2634, 2635, 2636, 2637, 2638, 2639, 2640, 2641, 2642, 2643, 2644, 2645, 2646, 2647, 2648, 2649, 2650, 2651, 2652, 2653, 2654, 2655, 2656, 2657, 2658, 2659, 2660, 2661, 2662, 2663, 2664, 2665, 2666, 2667, 2668, 2669, 2670, 2671, 2672, 2673, 2674, 2675, 2676, 2677, 2678, 2679, 2680, 2681, 2682, 2683, 2684, 2685, 2686, 2687, 2688, 2689, 2690, 2691, 2692, 2693, 2694, 2695, 2696, 2697, 2698, 2699, 2700, 2701, 2702, 2703, 2704, 2705, 2706, 2707, 2708, 2709, 2710, 2711, 2712, 2713, 2714, 2715, 2716, 2717, 2718, 2719, 2720, 2721, 2722, 2723, 2724, 2725, 2726, 2727, 2728, 2729, 2730, 2731, 2732, 2733, 2734, 2735, 2736, 2737, 2738, 2739, 2740, 2741, 2742, 2743, 2744, 2745, 2746, 2747, 2748, 2749, 2750, 2751, 2752, 2753, 2754, 2755, 2756, 2757, 2758, 2759, 2760, 2761, 2762, 2763, 2764, 2765, 2766, 2767, 2768, 2769, 2770, 2771, 2772, 2773, 2774, 2775, 2776, 2777, 2778, 2779, 2780, 2781, 2782, 2783, 2784, 2785, 2786, 2787, 2788, 2789, 2790, 2791, 2792, 2793, 2794, 2795, 2796, 2797, 2798, 2799, 2800, 2801, 2802, 2803, 2804, 2805, 2806, 2807, 2808, 2809, 2810, 2811, 2812, 2813, 2814, 2815, 2816, 2817, 2818, 2819, 2820, 2821, 2822, 2823, 2824, 2825, 2826, 2827, 2828, 2829, 2830, 2831, 2832, 2833, 2834, 2835, 2836, 2837, 2838, 2839, 2840, 2841, 2842, 2843, 2844, 2845, 2846, 2847, 2848, 2849, 2850, 2851, 2852, 2853, 2854, 2855, 2856, 2857, 2858, 2859, 2860, 2861, 2862, 2863, 2864, 2865, 2866, 2867, 2868, 2869, 2870, 2871, 2872, 2873, 2874, 2875, 2876, 2877, 2878, 2879, 2880, 2881, 2882, 2883, 2884, 2885, 2886, 2887, 2888, 2889, 2890, 2891, 2892, 2893, 2894, 2895, 2896, 2897, 2898, 2899, 2900, 2901, 2902, 2903, 2904, 2905, 2906, 2907, 2908, 2909, 2910, 2911, 2912, 2913, 2914, 2915, 2916, 2917, 2918, 2919, 2920, 2921, 2922, 2923, 2924, 2925, 2926, 2927, 2928, 2929, 2930, 2931, 2932, 2933, 2934, 2935, 2936, 2937, 2938, 2939, 2940, 2941, 2942, 2943, 2944, 2945, 2946, 2947, 2948, 2949, 2950, 2951, 2952, 2953, 2954, 2955, 2956, 2957, 2958, 2959, 2960, 2961, 2962, 2963, 2964, 2965, 2966, 2967, 2968, 2969, 2970, 2971, 2972, 2973, 2974, 2975, 2976, 2977, 2978, 2979, 2980, 2981, 2982, 2983, 2984, 2985, 2986, 2987, 2988, 2989, 2990, 2991, 2992, 2993, 2994, 2995, 2996, 2997, 2998, 2999, 3000, 3001, 3002, 3003, 3004, 3005, 3006, 3007, 3008, 3009, 3010, 3011, 3012, 3013, 3014, 3015, 3016, 3017, 3018, 3019, 3020, 3021, 3022, 3023, 3024, 3025, 3026, 3027, 3028, 3029, 3030, 3031, 3032, 3033, 3034, 3035, 3036, 3037, 3038, 3039, 3040, 3041, 3042, 3043, 3044, 3045, 3046, 3047, 3048, 3049, 3050, 3051, 3052, 3053, 3054, 3055, 3056, 3057, 3058, 3059, 3060, 3061, 3062, 3063, 3064, 3065, 3066, 3067, 3068, 3069, 3070, 3071, 3072, 3073, 3074, 3075, 3076, 3077, 3078, 3079, 3080, 3081, 3082, 3083, 3084, 3085, 3086, 3087, 3088, 3089, 3090, 3091, 3092, 3093, 3094, 3095, 3096, 3097, 3098, 3099, 3100, 3101, 3102, 3103, 3104, 3105, 3106, 3107, 3108, 3109, 3110, 3111, 3112, 3113, 3114, 3115, 3116, 3117, 3118, 3119, 3120, 3121, 3122, 3123, 3124, 3125, 3126, 3127, 3128, 3129, 3130, 3131, 3132, 3133, 3134, 3135, 3136, 3137, 3138, 3139, 3140, 3141, 3142, 3143, 3144, 3145, 3146, 3147, 3148, 3149, 3150, 3151, 3152, 3153, 3154, 3155, 3156, 3157, 3158, 3159, 3160, 3161, 3162, 3163, 3164, 3165, 3166, 3167, 3168, 3169, 3170, 3171, 3172, 3173, 3174, 3175, 3176, 3177, 3178, 3179, 3180, 3181, 3182, 3183, 3184, 3185, 3186, 3187, 3188, 3189, 3190, 3191, 3192, 3193, 3194, 3195, 3196, 3197, 3198, 3199, 3200, 3201, 3202, 3203, 3204, 3205, 3206, 3207, 3208, 3209, 3210, 3211, 3212, 3213, 3214, 3215, 3216, 3217, 3218, 3219, 3220, 3221, 3222, 3223, 3224, 3225, 3226, 3227, 3228, 3229, 3230, 3231, 3232, 3233, 3234, 3235, 3236, 3237, 3238, 3239, 3240, 3241, 3242, 3243, 3244, 3245, 3246, 3247, 3248, 3249, 3250, 3251, 3252, 3253, 3254, 3255, 3256, 3257, 3258, 3259, 3260, 3261, 3262, 3263, 3264, 3265, 3266, 3267, 3268, 3269, 3270, 3271, 3272, 3273, 3274, 3275, 3276, 3277, 3278, 3279, 3280, 3281, 3282, 3283, 3284, 3285, 3286, 3287, 3288, 3289, 3290, 3291, 3292, 3293, 3294, 3295, 3296, 3297, 3298, 3299, 3300, 3301, 3302, 3303, 3304, 3305, 3306, 3307, 3308, 3309, 3310, 3311, 3312, 3313, 3314, 3315, 3316, 3317, 3318, 3319, 3320, 3321, 3322, 3323, 3324, 3325, 3326, 3327, 3328, 3329, 3330, 3331, 3332, 3333, 3334, 3335, 3336, 3337, 3338, 3339, 3340, 3341, 3342, 3343, 3344, 3345, 3346, 3347, 3348, 3349, 3350, 3351, 3352, 3353, 3354, 3355, 3356, 3357, 3358, 3359, 3360, 3361, 3362, 3363, 3364, 3365, 3366, 3367, 3368, 3369, 3370, 3371, 3372, 3373, 3374, 3375, 3376, 3377, 3378, 3379, 3380, 3381, 3382, 3383, 3384, 3385, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396, 3397, 3398, 3399, 3400, 3401, 3402, 3403, 3404, 3405, 3406, 3407, 3408, 3409, 3410, 3411, 3412, 3413, 3414, 3415, 3416, 3417, 3418, 3419, 3420, 3421, 3422, 3423, 3424, 3425, 3426, 3427, 3428, 3429, 3430, 3431, 3432, 3433, 3434, 3435, 3436, 3437, 3438, 3439, 3440, 3441, 3442, 3443, 3444, 3445, 3446, 3447, 3448, 3449, 3450, 3451, 3452, 3453, 3454, 3455, 3456, 3457, 3458, 3459, 3460, 3461, 3462, 3463, 3464, 3465, 3466, 3467, 3468, 3469, 3470, 3471, 3472, 3473, 3474, 3475, 3476, 3477, 3478, 3479, 3480, 3481, 3482, 3483, 3484, 3485, 3486, 3487, 3488, 3489, 3490, 3491, 3492, 3493, 3494, 3495, 3496, 3497, 3498, 3499, 3500, 3501, 3502, 3503, 3504, 3505, 3506, 3507, 3508, 3509, 3510, 3511, 3512, 3513, 3514, 3515, 3516, 3517, 3518, 3519, 3520, 3521, 3522, 3523, 3524, 3525, 3526, 3527, 3528, 3529, 3530, 3531, 3532, 3533, 3534, 3535, 3536, 3537, 3538, 3539, 3540, 3541, 3542, 3543, 3544, 3545, 3546, 3547, 3548, 3549, 3550, 3551, 3552, 3553, 3554, 3555, 3556, 3557, 3558, 3559, 3560, 3561, 3562, 3563, 3564, 3565, 3566, 3567, 3568, 3569, 3570, 3571, 3572, 3573, 3574, 3575, 3576, 3577, 3578, 3579, 3580, 3581, 3582, 3583, 3584, 3585, 3586, 3587, 3588, 3589, 3590, 3591, 3592, 3593, 3594, 3595, 3596, 3597, 3598, 3599, 3600, 3601, 3602, 3603, 3604, 3605, 3606, 3607, 3608, 3609, 3610, 3611, 3612, 3613, 3614, 3615, 3616, 3617, 3618, 3619, 3620, 3621, 3622, 3623, 3624, 3625, 3626, 3627, 3628, 3629, 3630, 3631, 3632, 3633, 3634, 3635, 3636, 3637, 3638, 3639, 3640, 3641, 3642, 3643, 3644, 3645, 3646, 3647, 3648, 3649, 3650, 3651, 3652, 3653, 3654, 3655, 3656, 3657, 3658, 3659, 3660, 3661, 3662, 3663, 3664, 3665, 3666, 3667, 3668, 3669, 3670, 3671, 3672, 3673, 3674, 3675, 3676, 3677, 3678, 3679, 3680, 3681, 3682, 3683, 3684, 3685, 3686, 3687, 3688, 3689, 3690, 3691, 3692, 3693, 3694, 3695, 3696, 3697, 3698, 3699, 3700, 3701, 3702, 3703, 3704, 3705, 3706, 3707, 3708, 3709, 3710, 3711, 3712, 3713, 3714, 3715, 3716, 3717, 3718, 3719, 3720, 3721, 3722, 3723, 3724, 3725, 3726, 3727, 3728, 3729, 3730, 3731, 3732, 3733, 3734, 3735, 3736, 3737, 3738, 3739, 3740, 3741, 3742, 3743, 3744, 3745, 3746, 3747, 3748, 3749, 3750, 3751, 3752, 3753, 3754, 3755, 3756, 3757, 3758, 3759, 3760, 3761, 3762, 3763, 3764, 3765, 3766, 3767, 3768, 3769, 3770, 3771, 3772, 3773, 3774, 3775, 3776, 3777, 3778, 3779, 3780, 3781, 3782, 3783, 3784, 3785, 3786, 3787, 3788, 3789, 3790, 3791, 3792, 3793, 3794, 3795, 3796, 3797, 3798, 3799, 3800, 3801, 3802, 3803, 3804, 3805, 3806, 3807, 3808, 3809, 3810, 3811, 3812, 3813, 3814, 3815, 3816, 3817, 3818, 3819, 3820, 3821, 3822, 3823, 3824, 3825, 3826, 3827, 3828, 3829, 3830, 3831, 3832, 3833, 3834, 3835, 3836, 3837, 3838, 3839, 3840, 3841, 3842, 3843, 3844, 3845, 3846, 3847, 3848, 3849, 3850, 3851, 3852, 3853, 3854, 3855, 3856, 3857, 3858, 3859, 3860, 3861, 3862, 3863, 3864, 3865, 3866, 3867, 3868, 3869, 3870, 3871, 3872, 3873, 3874, 3875, 3876, 3877, 3878, 3879, 3880, 3881, 3882, 3883, 3884, 3885, 3886, 3887, 3888, 3889, 3890, 3891, 3892, 3893, 3894, 3895, 3896, 3897, 3898, 3899, 3900, 3901, 3902, 3903, 3904, 3905, 3906, 3907, 3908, 3909, 3910, 3911, 3912, 3913, 3914, 3915, 3916, 3917, 3918, 3919, 3920, 3921, 3922, 3923, 3924, 3925, 3926, 3927, 3928, 3929, 3930, 3931, 3932, 3933, 3934, 3935, 3936, 3937, 3938, 3939, 3940, 3941, 3942, 3943, 3944, 3945, 3946, 3947, 3948, 3949, 3950, 3951, 3952, 3953, 3954, 3955, 3956, 3957, 3958, 3959, 3960, 3961, 3962, 3963, 3964, 3965, 3966, 3967, 3968, 3969, 3970, 3971, 3972, 3973, 3974, 3975, 3976, 3977, 3978, 3979, 3980, 3981, 3982, 3983, 3984, 3985, 3986, 3987, 3988, 3989, 3990, 3991, 3992, 3993, 3994, 3995, 3996, 3997, 3998, 3999, 4000, 4001, 4002, 4003, 4004, 4005, 4006, 4007, 4008, 4009, 4010, 4011, 4012, 4013, 4014, 4015, 4016, 4017, 4018, 4019, 4020, 4021, 4022, 4023, 4024, 4025, 4026, 4027, 4028, 4029, 4030, 4031, 4032, 4033, 4034, 4035, 4036, 4037, 4038, 4039, 4040, 4041, 4042, 4043, 4044, 4045, 4046, 4047, 4048, 4049, 4050, 4051, 4052, 4053, 4054, 4055, 4056, 4057, 4058, 4059, 4060, 4061, 4062, 4063, 4064, 4065, 4066, 4067, 4068, 4069, 4070, 4071, 4072, 4073, 4074, 4075, 4076, 4077, 4078, 4079, 4080, 4081, 4082, 4083, 4084, 4085, 4086, 4087, 4088, 4089, 4090, 4091, 4092, 4093, 4094, 4095, 4096};
						float s_var[4096] = { 1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2, 2.03054, 2.05977, 2.0878, 2.11474, 2.1407, 2.16574, 2.18994, 2.21336, 2.23607, 2.2581, 2.27951, 2.30033, 2.3206, 2.34035, 2.35961, 2.37841, 2.39678, 2.41474, 2.4323, 2.44949, 2.46633, 2.48282, 2.499, 2.51487, 2.53044, 2.54573, 2.56075, 2.57551, 2.59002, 2.60429, 2.61833, 2.63215, 2.64575, 2.65915, 2.67235, 2.68535, 2.69817, 2.71081, 2.72327, 2.73556, 2.7477, 2.75967, 2.77149, 2.78316, 2.79468, 2.80607, 2.81731, 2.82843, 2.83941, 2.85027, 2.86101, 2.87162, 2.88212, 2.89251, 2.90278, 2.91295, 2.92301, 2.93297, 2.94283, 2.95259, 2.96226, 2.97183, 2.98131, 2.9907, 3, 3.00922, 3.01835, 3.0274, 3.03637, 3.04526, 3.05408, 3.06281, 3.07148, 3.08007, 3.08859, 3.09704, 3.10542, 3.11374, 3.12199, 3.13017, 3.13829, 3.14635, 3.15434, 3.16228, 3.17015, 3.17797, 3.18573, 3.19344, 3.20109, 3.20868, 3.21622, 3.22371, 3.23115, 3.23853, 3.24587, 3.25315, 3.26039, 3.26758, 3.27472, 3.28182, 3.28887, 3.29587, 3.30283, 3.30975, 3.31662, 3.32346, 3.33025, 3.33699, 3.3437, 3.35037, 3.357, 3.36359, 3.37014, 3.37665, 3.38312, 3.38956, 3.39596, 3.40233, 3.40866, 3.41495, 3.42121, 3.42744, 3.43363, 3.43979, 3.44592, 3.45201, 3.45807, 3.4641, 3.4701, 3.47607, 3.482, 3.48791, 3.49379, 3.49964, 3.50545, 3.51124, 3.517, 3.52274, 3.52844, 3.53412, 3.53977, 3.54539, 3.55099, 3.55656, 3.5621, 3.56762, 3.57311, 3.57858, 3.58402, 3.58944, 3.59484, 3.60021, 3.60555, 3.61087, 3.61617, 3.62145, 3.6267, 3.63193, 3.63714, 3.64232, 3.64748, 3.65262, 3.65774, 3.66284, 3.66792, 3.67297, 3.67801, 3.68302, 3.68802, 3.69299, 3.69794, 3.70288, 3.70779, 3.71269, 3.71756, 3.72242, 3.72726, 3.73208, 3.73688, 3.74166, 3.74642, 3.75117, 3.75589, 3.7606, 3.7653, 3.76997, 3.77463, 3.77927, 3.78389, 3.7885, 3.79309, 3.79766, 3.80221, 3.80675, 3.81128, 3.81579, 3.82028, 3.82475, 3.82921, 3.83366, 3.83809, 3.8425, 3.8469, 3.85129, 3.85565, 3.86001, 3.86435, 3.86867, 3.87298, 3.87728, 3.88156, 3.88583, 3.89008, 3.89432, 3.89855, 3.90276, 3.90696, 3.91115, 3.91532, 3.91948, 3.92362, 3.92775, 3.93187, 3.93598, 3.94007, 3.94415, 3.94822, 3.95228, 3.95632, 3.96035, 3.96437, 3.96838, 3.97237, 3.97635, 3.98032, 3.98428, 3.98823, 3.99216, 3.99609, 4, 4.0039, 4.00779, 4.01167, 4.01553, 4.01939, 4.02323, 4.02707, 4.03089, 4.0347, 4.0385, 4.04229, 4.04607, 4.04984, 4.0536, 4.05735, 4.06109, 4.06481, 4.06853, 4.07224, 4.07594, 4.07962, 4.0833, 4.08697, 4.09062, 4.09427, 4.09791, 4.10154, 4.10516, 4.10876, 4.11236, 4.11595, 4.11953, 4.12311, 4.12667, 4.13022, 4.13376, 4.1373, 4.14082, 4.14434, 4.14785, 4.15135, 4.15484, 4.15832, 4.16179, 4.16526, 4.16871, 4.17216, 4.1756, 4.17902, 4.18245, 4.18586, 4.18926, 4.19266, 4.19605, 4.19943, 4.2028, 4.20616, 4.20952, 4.21287, 4.21621, 4.21954, 4.22286, 4.22618, 4.22949, 4.23279, 4.23608, 4.23936, 4.24264, 4.24591, 4.24917, 4.25243, 4.25568, 4.25892, 4.26215, 4.26537, 4.26859, 4.2718, 4.275, 4.2782, 4.28139, 4.28457, 4.28775, 4.29092, 4.29408, 4.29723, 4.30038, 4.30352, 4.30665, 4.30978, 4.3129, 4.31601, 4.31912, 4.32221, 4.32531, 4.32839, 4.33147, 4.33455, 4.33761, 4.34067, 4.34373, 4.34677, 4.34981, 4.35285, 4.35588, 4.3589, 4.36191, 4.36492, 4.36793, 4.37092, 4.37391, 4.3769, 4.37988, 4.38285, 4.38582, 4.38878, 4.39173, 4.39468, 4.39762, 4.40056, 4.40349, 4.40641, 4.40933, 4.41225, 4.41515, 4.41806, 4.42095, 4.42384, 4.42673, 4.42961, 4.43248, 4.43535, 4.43821, 4.44107, 4.44392, 4.44677, 4.44961, 4.45244, 4.45527, 4.45809, 4.46091, 4.46373, 4.46654, 4.46934, 4.47214, 4.47493, 4.47772, 4.4805, 4.48327, 4.48605, 4.48881, 4.49157, 4.49433, 4.49708, 4.49983, 4.50257, 4.50531, 4.50804, 4.51076, 4.51349, 4.5162, 4.51891, 4.52162, 4.52432, 4.52702, 4.52971, 4.5324, 4.53508, 4.53776, 4.54043, 4.5431, 4.54576, 4.54842, 4.55108, 4.55373, 4.55637, 4.55901, 4.56165, 4.56428, 4.56691, 4.56953, 4.57215, 4.57476, 4.57737, 4.57998, 4.58258, 4.58517, 4.58776, 4.59035, 4.59293, 4.59551, 4.59808, 4.60065, 4.60322, 4.60578, 4.60834, 4.61089, 4.61344, 4.61598, 4.61852, 4.62106, 4.62359, 4.62611, 4.62864, 4.63116, 4.63367, 4.63618, 4.63869, 4.64119, 4.64369, 4.64618, 4.64868, 4.65116, 4.65364, 4.65612, 4.6586, 4.66107, 4.66354, 4.666, 4.66846, 4.67091, 4.67336, 4.67581, 4.67825, 4.68069, 4.68313, 4.68556, 4.68799, 4.69042, 4.69284, 4.69525, 4.69767, 4.70008, 4.70248, 4.70489, 4.70728, 4.70968, 4.71207, 4.71446, 4.71684, 4.71922, 4.7216, 4.72397, 4.72634, 4.72871, 4.73107, 4.73343, 4.73579, 4.73814, 4.74049, 4.74283, 4.74517, 4.74751, 4.74985, 4.75218, 4.7545, 4.75683, 4.75915, 4.76147, 4.76378, 4.76609, 4.7684, 4.7707, 4.773, 4.7753, 4.7776, 4.77989, 4.78217, 4.78446, 4.78674, 4.78902, 4.79129, 4.79356, 4.79583, 4.7981, 4.80036, 4.80262, 4.80487, 4.80712, 4.80937, 4.81162, 4.81386, 4.8161, 4.81834, 4.82057, 4.8228, 4.82503, 4.82725, 4.82947, 4.83169, 4.83391, 4.83612, 4.83833, 4.84053, 4.84273, 4.84493, 4.84713, 4.84932, 4.85152, 4.8537, 4.85589, 4.85807, 4.86025, 4.86243, 4.8646, 4.86677, 4.86894, 4.8711, 4.87326, 4.87542, 4.87758, 4.87973, 4.88188, 4.88403, 4.88617, 4.88831, 4.89045, 4.89259, 4.89472, 4.89685, 4.89898, 4.9011, 4.90323, 4.90535, 4.90746, 4.90958, 4.91169, 4.9138, 4.9159, 4.91801, 4.92011, 4.9222, 4.9243, 4.92639, 4.92848, 4.93057, 4.93265, 4.93473, 4.93681, 4.93889, 4.94096, 4.94303, 4.9451, 4.94717, 4.94923, 4.95129, 4.95335, 4.95541, 4.95746, 4.95951, 4.96156, 4.9636, 4.96565, 4.96769, 4.96973, 4.97176, 4.97379, 4.97583, 4.97785, 4.97988, 4.9819, 4.98392, 4.98594, 4.98796, 4.98997, 4.99198, 4.99399, 4.996, 4.998, 5, 5.002, 5.004, 5.00599, 5.00798, 5.00997, 5.01196, 5.01394, 5.01592, 5.0179, 5.01988, 5.02186, 5.02383, 5.0258, 5.02777, 5.02973, 5.0317, 5.03366, 5.03562, 5.03757, 5.03953, 5.04148, 5.04343, 5.04538, 5.04732, 5.04927, 5.05121, 5.05315, 5.05508, 5.05702, 5.05895, 5.06088, 5.06281, 5.06473, 5.06666, 5.06858, 5.0705, 5.07241, 5.07433, 5.07624, 5.07815, 5.08006, 5.08196, 5.08387, 5.08577, 5.08767, 5.08956, 5.09146, 5.09335, 5.09524, 5.09713, 5.09902, 5.1009, 5.10279, 5.10467, 5.10655, 5.10842, 5.1103, 5.11217, 5.11404, 5.11591, 5.11777, 5.11964, 5.1215, 5.12336, 5.12522, 5.12707, 5.12893, 5.13078, 5.13263, 5.13448, 5.13632, 5.13817, 5.14001, 5.14185, 5.14369, 5.14552, 5.14736, 5.14919, 5.15102, 5.15285, 5.15467, 5.1565, 5.15832, 5.16014, 5.16196, 5.16378, 5.16559, 5.1674, 5.16921, 5.17102, 5.17283, 5.17464, 5.17644, 5.17824, 5.18004, 5.18184, 5.18363, 5.18543, 5.18722, 5.18901, 5.1908, 5.19258, 5.19437, 5.19615, 5.19793, 5.19971, 5.20149, 5.20327, 5.20504, 5.20681, 5.20858, 5.21035, 5.21212, 5.21388, 5.21564, 5.21741, 5.21916, 5.22092, 5.22268, 5.22443, 5.22618, 5.22793, 5.22968, 5.23143, 5.23318, 5.23492, 5.23666, 5.2384, 5.24014, 5.24188, 5.24361, 5.24534, 5.24708, 5.24881, 5.25053, 5.25226, 5.25398, 5.25571, 5.25743, 5.25915, 5.26087, 5.26258, 5.2643, 5.26601, 5.26772, 5.26943, 5.27114, 5.27284, 5.27455, 5.27625, 5.27795, 5.27965, 5.28135, 5.28305, 5.28474, 5.28643, 5.28812, 5.28981, 5.2915, 5.29319, 5.29487, 5.29656, 5.29824, 5.29992, 5.3016, 5.30327, 5.30495, 5.30662, 5.3083, 5.30997, 5.31164, 5.3133, 5.31497, 5.31663, 5.3183, 5.31996, 5.32162, 5.32327, 5.32493, 5.32659, 5.32824, 5.32989, 5.33154, 5.33319, 5.33484, 5.33648, 5.33813, 5.33977, 5.34141, 5.34305, 5.34469, 5.34633, 5.34796, 5.3496, 5.35123, 5.35286, 5.35449, 5.35612, 5.35774, 5.35937, 5.36099, 5.36261, 5.36423, 5.36585, 5.36747, 5.36908, 5.3707, 5.37231, 5.37392, 5.37553, 5.37714, 5.37875, 5.38036, 5.38196, 5.38356, 5.38516, 5.38676, 5.38836, 5.38996, 5.39156, 5.39315, 5.39474, 5.39634, 5.39793, 5.39951, 5.4011, 5.40269, 5.40427, 5.40586, 5.40744, 5.40902, 5.4106, 5.41217, 5.41375, 5.41533, 5.4169, 5.41847, 5.42004, 5.42161, 5.42318, 5.42475, 5.42631, 5.42788, 5.42944, 5.431, 5.43256, 5.43412, 5.43568, 5.43723, 5.43879, 5.44034, 5.44189, 5.44344, 5.44499, 5.44654, 5.44809, 5.44963, 5.45118, 5.45272, 5.45426, 5.4558, 5.45734, 5.45888, 5.46041, 5.46195, 5.46348, 5.46501, 5.46654, 5.46807, 5.4696, 5.47113, 5.47266, 5.47418, 5.4757, 5.47723, 5.47875, 5.48027, 5.48178, 5.4833, 5.48482, 5.48633, 5.48784, 5.48936, 5.49087, 5.49238, 5.49389, 5.49539, 5.4969, 5.4984, 5.49991, 5.50141, 5.50291, 5.50441, 5.50591, 5.5074, 5.5089, 5.51039, 5.51189, 5.51338, 5.51487, 5.51636, 5.51785, 5.51934, 5.52082, 5.52231, 5.52379, 5.52528, 5.52676, 5.52824, 5.52972, 5.53119, 5.53267, 5.53415, 5.53562, 5.53709, 5.53857, 5.54004, 5.54151, 5.54298, 5.54444, 5.54591, 5.54737, 5.54884, 5.5503, 5.55176, 5.55322, 5.55468, 5.55614, 5.5576, 5.55905, 5.56051, 5.56196, 5.56341, 5.56487, 5.56632, 5.56776, 5.56921, 5.57066, 5.5721, 5.57355, 5.57499, 5.57643, 5.57788, 5.57932, 5.58075, 5.58219, 5.58363, 5.58506, 5.5865, 5.58793, 5.58936, 5.5908, 5.59223, 5.59365, 5.59508, 5.59651, 5.59794, 5.59936, 5.60078, 5.60221, 5.60363, 5.60505, 5.60647, 5.60788, 5.6093, 5.61072, 5.61213, 5.61355, 5.61496, 5.61637, 5.61778, 5.61919, 5.6206, 5.62201, 5.62341, 5.62482, 5.62622, 5.62763, 5.62903, 5.63043, 5.63183, 5.63323, 5.63463, 5.63602, 5.63742, 5.63881, 5.64021, 5.6416, 5.64299, 5.64438, 5.64577, 5.64716, 5.64855, 5.64994, 5.65132, 5.65271, 5.65409, 5.65547, 5.65685, 5.65823, 5.65961, 5.66099, 5.66237, 5.66375, 5.66512, 5.6665, 5.66787, 5.66924, 5.67061, 5.67199, 5.67335, 5.67472, 5.67609, 5.67746, 5.67882, 5.68019, 5.68155, 5.68291, 5.68428, 5.68564, 5.687, 5.68835, 5.68971, 5.69107, 5.69243, 5.69378, 5.69513, 5.69649, 5.69784, 5.69919, 5.70054, 5.70189, 5.70324, 5.70458, 5.70593, 5.70728, 5.70862, 5.70996, 5.71131, 5.71265, 5.71399, 5.71533, 5.71667, 5.718, 5.71934, 5.72068, 5.72201, 5.72335, 5.72468, 5.72601, 5.72734, 5.72867, 5.73, 5.73133, 5.73266, 5.73398, 5.73531, 5.73663, 5.73796, 5.73928, 5.7406, 5.74192, 5.74324, 5.74456, 5.74588, 5.7472, 5.74851, 5.74983, 5.75115, 5.75246, 5.75377, 5.75508, 5.75639, 5.75771, 5.75901, 5.76032, 5.76163, 5.76294, 5.76424, 5.76555, 5.76685, 5.76815, 5.76946, 5.77076, 5.77206, 5.77336, 5.77466, 5.77595, 5.77725, 5.77855, 5.77984, 5.78114, 5.78243, 5.78372, 5.78502, 5.78631, 5.7876, 5.78889, 5.79017, 5.79146, 5.79275, 5.79403, 5.79532, 5.7966, 5.79789, 5.79917, 5.80045, 5.80173, 5.80301, 5.80429, 5.80557, 5.80684, 5.80812, 5.8094, 5.81067, 5.81194, 5.81322, 5.81449, 5.81576, 5.81703, 5.8183, 5.81957, 5.82084, 5.8221, 5.82337, 5.82464, 5.8259, 5.82717, 5.82843, 5.82969, 5.83095, 5.83221, 5.83347, 5.83473, 5.83599, 5.83725, 5.8385, 5.83976, 5.84101, 5.84227, 5.84352, 5.84477, 5.84603, 5.84728, 5.84853, 5.84978, 5.85102, 5.85227, 5.85352, 5.85476, 5.85601, 5.85725, 5.8585, 5.85974, 5.86098, 5.86222, 5.86347, 5.86471, 5.86594, 5.86718, 5.86842, 5.86966, 5.87089, 5.87213, 5.87336, 5.8746, 5.87583, 5.87706, 5.87829, 5.87952, 5.88075, 5.88198, 5.88321, 5.88444, 5.88566, 5.88689, 5.88811, 5.88934, 5.89056, 5.89178, 5.89301, 5.89423, 5.89545, 5.89667, 5.89789, 5.8991, 5.90032, 5.90154, 5.90275, 5.90397, 5.90518, 5.9064, 5.90761, 5.90882, 5.91003, 5.91124, 5.91245, 5.91366, 5.91487, 5.91608, 5.91729, 5.91849, 5.9197, 5.9209, 5.92211, 5.92331, 5.92451, 5.92572, 5.92692, 5.92812, 5.92932, 5.93052, 5.93171, 5.93291, 5.93411, 5.9353, 5.9365, 5.93769, 5.93889, 5.94008, 5.94127, 5.94246, 5.94366, 5.94485, 5.94604, 5.94722, 5.94841, 5.9496, 5.95079, 5.95197, 5.95316, 5.95434, 5.95553, 5.95671, 5.95789, 5.95907, 5.96025, 5.96144, 5.96262, 5.96379, 5.96497, 5.96615, 5.96733, 5.9685, 5.96968, 5.97085, 5.97203, 5.9732, 5.97437, 5.97555, 5.97672, 5.97789, 5.97906, 5.98023, 5.9814, 5.98256, 5.98373, 5.9849, 5.98606, 5.98723, 5.98839, 5.98956, 5.99072, 5.99188, 5.99304, 5.9942, 5.99537, 5.99652, 5.99768, 5.99884, 6, 6.00116, 6.00231, 6.00347, 6.00462, 6.00578, 6.00693, 6.00809, 6.00924, 6.01039, 6.01154, 6.01269, 6.01384, 6.01499, 6.01614, 6.01729, 6.01843, 6.01958, 6.02073, 6.02187, 6.02302, 6.02416, 6.0253, 6.02645, 6.02759, 6.02873, 6.02987, 6.03101, 6.03215, 6.03329, 6.03442, 6.03556, 6.0367, 6.03784, 6.03897, 6.04011, 6.04124, 6.04237, 6.04351, 6.04464, 6.04577, 6.0469, 6.04803, 6.04916, 6.05029, 6.05142, 6.05255, 6.05367, 6.0548, 6.05593, 6.05705, 6.05818, 6.0593, 6.06042, 6.06155, 6.06267, 6.06379, 6.06491, 6.06603, 6.06715, 6.06827, 6.06939, 6.07051, 6.07162, 6.07274, 6.07386, 6.07497, 6.07609, 6.0772, 6.07831, 6.07943, 6.08054, 6.08165, 6.08276, 6.08387, 6.08498, 6.08609, 6.0872, 6.08831, 6.08942, 6.09052, 6.09163, 6.09274, 6.09384, 6.09494, 6.09605, 6.09715, 6.09825, 6.09936, 6.10046, 6.10156, 6.10266, 6.10376, 6.10486, 6.10596, 6.10705, 6.10815, 6.10925, 6.11034, 6.11144, 6.11253, 6.11363, 6.11472, 6.11582, 6.11691, 6.118, 6.11909, 6.12018, 6.12127, 6.12236, 6.12345, 6.12454, 6.12563, 6.12672, 6.1278, 6.12889, 6.12997, 6.13106, 6.13214, 6.13323, 6.13431, 6.13539, 6.13648, 6.13756, 6.13864, 6.13972, 6.1408, 6.14188, 6.14296, 6.14404, 6.14511, 6.14619, 6.14727, 6.14834, 6.14942, 6.15049, 6.15157, 6.15264, 6.15371, 6.15479, 6.15586, 6.15693, 6.158, 6.15907, 6.16014, 6.16121, 6.16228, 6.16335, 6.16441, 6.16548, 6.16655, 6.16761, 6.16868, 6.16974, 6.17081, 6.17187, 6.17293, 6.174, 6.17506, 6.17612, 6.17718, 6.17824, 6.1793, 6.18036, 6.18142, 6.18248, 6.18354, 6.18459, 6.18565, 6.1867, 6.18776, 6.18882, 6.18987, 6.19092, 6.19198, 6.19303, 6.19408, 6.19513, 6.19618, 6.19724, 6.19829, 6.19934, 6.20038, 6.20143, 6.20248, 6.20353, 6.20458, 6.20562, 6.20667, 6.20771, 6.20876, 6.2098, 6.21085, 6.21189, 6.21293, 6.21397, 6.21502, 6.21606, 6.2171, 6.21814, 6.21918, 6.22022, 6.22125, 6.22229, 6.22333, 6.22437, 6.2254, 6.22644, 6.22747, 6.22851, 6.22954, 6.23058, 6.23161, 6.23264, 6.23368, 6.23471, 6.23574, 6.23677, 6.2378, 6.23883, 6.23986, 6.24089, 6.24192, 6.24294, 6.24397, 6.245, 6.24602, 6.24705, 6.24808, 6.2491, 6.25012, 6.25115, 6.25217, 6.25319, 6.25422, 6.25524, 6.25626, 6.25728, 6.2583, 6.25932, 6.26034, 6.26136, 6.26238, 6.26339, 6.26441, 6.26543, 6.26644, 6.26746, 6.26847, 6.26949, 6.2705, 6.27152, 6.27253, 6.27354, 6.27455, 6.27557, 6.27658, 6.27759, 6.2786, 6.27961, 6.28062, 6.28163, 6.28264, 6.28364, 6.28465, 6.28566, 6.28666, 6.28767, 6.28868, 6.28968, 6.29069, 6.29169, 6.29269, 6.2937, 6.2947, 6.2957, 6.2967, 6.2977, 6.2987, 6.2997, 6.3007, 6.3017, 6.3027, 6.3037, 6.3047, 6.3057, 6.30669, 6.30769, 6.30868, 6.30968, 6.31067, 6.31167, 6.31266, 6.31366, 6.31465, 6.31564, 6.31663, 6.31763, 6.31862, 6.31961, 6.3206, 6.32159, 6.32258, 6.32357, 6.32456, 6.32554, 6.32653, 6.32752, 6.3285, 6.32949, 6.33048, 6.33146, 6.33245, 6.33343, 6.33441, 6.3354, 6.33638, 6.33736, 6.33835, 6.33933, 6.34031, 6.34129, 6.34227, 6.34325, 6.34423, 6.34521, 6.34618, 6.34716, 6.34814, 6.34912, 6.35009, 6.35107, 6.35205, 6.35302, 6.354, 6.35497, 6.35594, 6.35692, 6.35789, 6.35886, 6.35983, 6.36081, 6.36178, 6.36275, 6.36372, 6.36469, 6.36566, 6.36663, 6.3676, 6.36856, 6.36953, 6.3705, 6.37147, 6.37243, 6.3734, 6.37436, 6.37533, 6.37629, 6.37726, 6.37822, 6.37918, 6.38015, 6.38111, 6.38207, 6.38303, 6.38399, 6.38495, 6.38591, 6.38687, 6.38783, 6.38879, 6.38975, 6.39071, 6.39167, 6.39262, 6.39358, 6.39454, 6.39549, 6.39645, 6.3974, 6.39836, 6.39931, 6.40027, 6.40122, 6.40217, 6.40312, 6.40408, 6.40503, 6.40598, 6.40693, 6.40788, 6.40883, 6.40978, 6.41073, 6.41168, 6.41263, 6.41357, 6.41452, 6.41547, 6.41641, 6.41736, 6.41831, 6.41925, 6.4202, 6.42114, 6.42209, 6.42303, 6.42397, 6.42492, 6.42586, 6.4268, 6.42774, 6.42868, 6.42962, 6.43056, 6.4315, 6.43244, 6.43338, 6.43432, 6.43526, 6.4362, 6.43713, 6.43807, 6.43901, 6.43994, 6.44088, 6.44182, 6.44275, 6.44369, 6.44462, 6.44555, 6.44649, 6.44742, 6.44835, 6.44928, 6.45022, 6.45115, 6.45208, 6.45301, 6.45394, 6.45487, 6.4558, 6.45673, 6.45766, 6.45858, 6.45951, 6.46044, 6.46137, 6.46229, 6.46322, 6.46414, 6.46507, 6.46599, 6.46692, 6.46784, 6.46877, 6.46969, 6.47061, 6.47154, 6.47246, 6.47338, 6.4743, 6.47522, 6.47614, 6.47706, 6.47798, 6.4789, 6.47982, 6.48074, 6.48166, 6.48258, 6.48349, 6.48441, 6.48533, 6.48624, 6.48716, 6.48808, 6.48899, 6.48991, 6.49082, 6.49173, 6.49265, 6.49356, 6.49447, 6.49539, 6.4963, 6.49721, 6.49812, 6.49903, 6.49994, 6.50085, 6.50176, 6.50267, 6.50358, 6.50449, 6.5054, 6.50631, 6.50721, 6.50812, 6.50903, 6.50993, 6.51084, 6.51175, 6.51265, 6.51356, 6.51446, 6.51536, 6.51627, 6.51717, 6.51807, 6.51898, 6.51988, 6.52078, 6.52168, 6.52258, 6.52348, 6.52438, 6.52528, 6.52618, 6.52708, 6.52798, 6.52888, 6.52978, 6.53068, 6.53157, 6.53247, 6.53337, 6.53426, 6.53516, 6.53606, 6.53695, 6.53785, 6.53874, 6.53963, 6.54053, 6.54142, 6.54231, 6.54321, 6.5441, 6.54499, 6.54588, 6.54677, 6.54766, 6.54855, 6.54944, 6.55033, 6.55122, 6.55211, 6.553, 6.55389, 6.55478, 6.55566, 6.55655, 6.55744, 6.55832, 6.55921, 6.5601, 6.56098, 6.56187, 6.56275, 6.56364, 6.56452, 6.5654, 6.56629, 6.56717, 6.56805, 6.56893, 6.56982, 6.5707, 6.57158, 6.57246, 6.57334, 6.57422, 6.5751, 6.57598, 6.57686, 6.57774, 6.57861, 6.57949, 6.58037, 6.58125, 6.58212, 6.583, 6.58388, 6.58475, 6.58563, 6.5865, 6.58738, 6.58825, 6.58913, 6.59, 6.59087, 6.59175, 6.59262, 6.59349, 6.59436, 6.59524, 6.59611, 6.59698, 6.59785, 6.59872, 6.59959, 6.60046, 6.60133, 6.6022, 6.60306, 6.60393, 6.6048, 6.60567, 6.60654, 6.6074, 6.60827, 6.60913, 6.61, 6.61087, 6.61173, 6.6126, 6.61346, 6.61432, 6.61519, 6.61605, 6.61691, 6.61778, 6.61864, 6.6195, 6.62036, 6.62122, 6.62209, 6.62295, 6.62381, 6.62467, 6.62553, 6.62639, 6.62725, 6.6281, 6.62896, 6.62982, 6.63068, 6.63154, 6.63239, 6.63325, 6.63411, 6.63496, 6.63582, 6.63667, 6.63753, 6.63838, 6.63924, 6.64009, 6.64095, 6.6418, 6.64265, 6.6435, 6.64436, 6.64521, 6.64606, 6.64691, 6.64776, 6.64861, 6.64946, 6.65031, 6.65116, 6.65201, 6.65286, 6.65371, 6.65456, 6.65541, 6.65626, 6.6571, 6.65795, 6.6588, 6.65965, 6.66049, 6.66134, 6.66218, 6.66303, 6.66387, 6.66472, 6.66556, 6.66641, 6.66725, 6.66809, 6.66894, 6.66978, 6.67062, 6.67146, 6.67231, 6.67315, 6.67399, 6.67483, 6.67567, 6.67651, 6.67735, 6.67819, 6.67903, 6.67987, 6.68071, 6.68154, 6.68238, 6.68322, 6.68406, 6.68489, 6.68573, 6.68657, 6.6874, 6.68824, 6.68907, 6.68991, 6.69074, 6.69158, 6.69241, 6.69325, 6.69408, 6.69491, 6.69575, 6.69658, 6.69741, 6.69824, 6.69908, 6.69991, 6.70074, 6.70157, 6.7024, 6.70323, 6.70406, 6.70489, 6.70572, 6.70655, 6.70738, 6.7082, 6.70903, 6.70986, 6.71069, 6.71151, 6.71234, 6.71317, 6.71399, 6.71482, 6.71565, 6.71647, 6.7173, 6.71812, 6.71894, 6.71977, 6.72059, 6.72142, 6.72224, 6.72306, 6.72388, 6.72471, 6.72553, 6.72635, 6.72717, 6.72799, 6.72881, 6.72963, 6.73045, 6.73127, 6.73209, 6.73291, 6.73373, 6.73455, 6.73537, 6.73619, 6.737, 6.73782, 6.73864, 6.73946, 6.74027, 6.74109, 6.7419, 6.74272, 6.74354, 6.74435, 6.74517, 6.74598, 6.74679, 6.74761, 6.74842, 6.74923, 6.75005, 6.75086, 6.75167, 6.75248, 6.7533, 6.75411, 6.75492, 6.75573, 6.75654, 6.75735, 6.75816, 6.75897, 6.75978, 6.76059, 6.7614, 6.76221, 6.76302, 6.76382, 6.76463, 6.76544, 6.76625, 6.76705, 6.76786, 6.76867, 6.76947, 6.77028, 6.77108, 6.77189, 6.77269, 6.7735, 6.7743, 6.77511, 6.77591, 6.77671, 6.77752, 6.77832, 6.77912, 6.77992, 6.78073, 6.78153, 6.78233, 6.78313, 6.78393, 6.78473, 6.78553, 6.78633, 6.78713, 6.78793, 6.78873, 6.78953, 6.79033, 6.79113, 6.79193, 6.79272, 6.79352, 6.79432, 6.79511, 6.79591, 6.79671, 6.7975, 6.7983, 6.7991, 6.79989, 6.80069, 6.80148, 6.80227, 6.80307, 6.80386, 6.80466, 6.80545, 6.80624, 6.80704, 6.80783, 6.80862, 6.80941, 6.8102, 6.811, 6.81179, 6.81258, 6.81337, 6.81416, 6.81495, 6.81574, 6.81653, 6.81732, 6.81811, 6.81889, 6.81968, 6.82047, 6.82126, 6.82205, 6.82283, 6.82362, 6.82441, 6.82519, 6.82598, 6.82677, 6.82755, 6.82834, 6.82912, 6.82991, 6.83069, 6.83147, 6.83226, 6.83304, 6.83383, 6.83461, 6.83539, 6.83617, 6.83696, 6.83774, 6.83852, 6.8393, 6.84008, 6.84087, 6.84165, 6.84243, 6.84321, 6.84399, 6.84477, 6.84555, 6.84633, 6.8471, 6.84788, 6.84866, 6.84944, 6.85022, 6.85099, 6.85177, 6.85255, 6.85333, 6.8541, 6.85488, 6.85565, 6.85643, 6.85721, 6.85798, 6.85876, 6.85953, 6.86031, 6.86108, 6.86185, 6.86263, 6.8634, 6.86417, 6.86495, 6.86572, 6.86649, 6.86726, 6.86804, 6.86881, 6.86958, 6.87035, 6.87112, 6.87189, 6.87266, 6.87343, 6.8742, 6.87497, 6.87574, 6.87651, 6.87728, 6.87805, 6.87881, 6.87958, 6.88035, 6.88112, 6.88188, 6.88265, 6.88342, 6.88418, 6.88495, 6.88572, 6.88648, 6.88725, 6.88801, 6.88878, 6.88954, 6.89031, 6.89107, 6.89183, 6.8926, 6.89336, 6.89412, 6.89489, 6.89565, 6.89641, 6.89717, 6.89794, 6.8987, 6.89946, 6.90022, 6.90098, 6.90174, 6.9025, 6.90326, 6.90402, 6.90478, 6.90554, 6.9063, 6.90706, 6.90782, 6.90857, 6.90933, 6.91009, 6.91085, 6.9116, 6.91236, 6.91312, 6.91388, 6.91463, 6.91539, 6.91614, 6.9169, 6.91765, 6.91841, 6.91916, 6.91992, 6.92067, 6.92143, 6.92218, 6.92293, 6.92369, 6.92444, 6.92519, 6.92595, 6.9267, 6.92745, 6.9282, 6.92895, 6.92971, 6.93046, 6.93121, 6.93196, 6.93271, 6.93346, 6.93421, 6.93496, 6.93571, 6.93646, 6.93721, 6.93796, 6.9387, 6.93945, 6.9402, 6.94095, 6.9417, 6.94244, 6.94319, 6.94394, 6.94468, 6.94543, 6.94618, 6.94692, 6.94767, 6.94841, 6.94916, 6.9499, 6.95065, 6.95139, 6.95214, 6.95288, 6.95362, 6.95437, 6.95511, 6.95585, 6.9566, 6.95734, 6.95808, 6.95882, 6.95956, 6.96031, 6.96105, 6.96179, 6.96253, 6.96327, 6.96401, 6.96475, 6.96549, 6.96623, 6.96697, 6.96771, 6.96845, 6.96918, 6.96992, 6.97066, 6.9714, 6.97214, 6.97287, 6.97361, 6.97435, 6.97509, 6.97582, 6.97656, 6.9773, 6.97803, 6.97877, 6.9795, 6.98024, 6.98097, 6.98171, 6.98244, 6.98318, 6.98391, 6.98464, 6.98538, 6.98611, 6.98684, 6.98758, 6.98831, 6.98904, 6.98977, 6.99051, 6.99124, 6.99197, 6.9927, 6.99343, 6.99416, 6.99489, 6.99562, 6.99635, 6.99708, 6.99781, 6.99854, 6.99927, 7, 7.00073, 7.00146, 7.00219, 7.00291, 7.00364, 7.00437, 7.0051, 7.00582, 7.00655, 7.00728, 7.008, 7.00873, 7.00946, 7.01018, 7.01091, 7.01163, 7.01236, 7.01308, 7.01381, 7.01453, 7.01526, 7.01598, 7.0167, 7.01743, 7.01815, 7.01887, 7.0196, 7.02032, 7.02104, 7.02176, 7.02249, 7.02321, 7.02393, 7.02465, 7.02537, 7.02609, 7.02681, 7.02753, 7.02825, 7.02897, 7.02969, 7.03041, 7.03113, 7.03185, 7.03257, 7.03329, 7.03401, 7.03473, 7.03544, 7.03616, 7.03688, 7.0376, 7.03831, 7.03903, 7.03975, 7.04046, 7.04118, 7.0419, 7.04261, 7.04333, 7.04404, 7.04476, 7.04547, 7.04619, 7.0469, 7.04762, 7.04833, 7.04904, 7.04976, 7.05047, 7.05119, 7.0519, 7.05261, 7.05332, 7.05404, 7.05475, 7.05546, 7.05617, 7.05688, 7.05759, 7.05831, 7.05902, 7.05973, 7.06044, 7.06115, 7.06186, 7.06257, 7.06328, 7.06399, 7.0647, 7.0654, 7.06611, 7.06682, 7.06753, 7.06824, 7.06895, 7.06965, 7.07036, 7.07107, 7.07177, 7.07248, 7.07319, 7.07389, 7.0746, 7.07531, 7.07601, 7.07672, 7.07742, 7.07813, 7.07883, 7.07954, 7.08024, 7.08095, 7.08165, 7.08235, 7.08306, 7.08376, 7.08446, 7.08517, 7.08587, 7.08657, 7.08728, 7.08798, 7.08868, 7.08938, 7.09008, 7.09078, 7.09149, 7.09219, 7.09289, 7.09359, 7.09429, 7.09499, 7.09569, 7.09639, 7.09709, 7.09779, 7.09849, 7.09918, 7.09988, 7.10058, 7.10128, 7.10198, 7.10268, 7.10337, 7.10407, 7.10477, 7.10546, 7.10616, 7.10686, 7.10755, 7.10825, 7.10895, 7.10964, 7.11034, 7.11103, 7.11173, 7.11242, 7.11312, 7.11381, 7.11451, 7.1152, 7.11589, 7.11659, 7.11728, 7.11798, 7.11867, 7.11936, 7.12005, 7.12075, 7.12144, 7.12213, 7.12282, 7.12351, 7.12421, 7.1249, 7.12559, 7.12628, 7.12697, 7.12766, 7.12835, 7.12904, 7.12973, 7.13042, 7.13111, 7.1318, 7.13249, 7.13318, 7.13387, 7.13455, 7.13524, 7.13593, 7.13662, 7.13731, 7.13799, 7.13868, 7.13937, 7.14006, 7.14074, 7.14143, 7.14211, 7.1428, 7.14349, 7.14417, 7.14486, 7.14554, 7.14623, 7.14691, 7.1476, 7.14828, 7.14897, 7.14965, 7.15034, 7.15102, 7.1517, 7.15239, 7.15307, 7.15375, 7.15443, 7.15512, 7.1558, 7.15648, 7.15716, 7.15785, 7.15853, 7.15921, 7.15989, 7.16057, 7.16125, 7.16193, 7.16261, 7.16329, 7.16397, 7.16465, 7.16533, 7.16601, 7.16669, 7.16737, 7.16805, 7.16873, 7.16941, 7.17008, 7.17076, 7.17144, 7.17212, 7.1728, 7.17347, 7.17415, 7.17483, 7.1755, 7.17618, 7.17686, 7.17753, 7.17821, 7.17889, 7.17956, 7.18024, 7.18091, 7.18159, 7.18226, 7.18294, 7.18361, 7.18429, 7.18496, 7.18563, 7.18631, 7.18698, 7.18765, 7.18833, 7.189, 7.18967, 7.19035, 7.19102, 7.19169, 7.19236, 7.19303, 7.19371, 7.19438, 7.19505, 7.19572, 7.19639, 7.19706, 7.19773, 7.1984, 7.19907, 7.19974, 7.20041, 7.20108, 7.20175, 7.20242, 7.20309, 7.20376, 7.20443, 7.20509, 7.20576, 7.20643, 7.2071, 7.20777, 7.20843, 7.2091, 7.20977, 7.21044, 7.2111, 7.21177, 7.21244, 7.2131, 7.21377, 7.21443, 7.2151, 7.21576, 7.21643, 7.2171, 7.21776, 7.21843, 7.21909, 7.21975, 7.22042, 7.22108, 7.22175, 7.22241, 7.22307, 7.22374, 7.2244, 7.22506, 7.22573, 7.22639, 7.22705, 7.22771, 7.22837, 7.22904, 7.2297, 7.23036, 7.23102, 7.23168, 7.23234, 7.233, 7.23366, 7.23432, 7.23499, 7.23565, 7.2363, 7.23696, 7.23762, 7.23828, 7.23894, 7.2396, 7.24026, 7.24092, 7.24158, 7.24224, 7.24289, 7.24355, 7.24421, 7.24487, 7.24552, 7.24618, 7.24684, 7.2475, 7.24815, 7.24881, 7.24946, 7.25012, 7.25078, 7.25143, 7.25209, 7.25274, 7.2534, 7.25405, 7.25471, 7.25536, 7.25602, 7.25667, 7.25733, 7.25798, 7.25863, 7.25929, 7.25994, 7.26059, 7.26125, 7.2619, 7.26255, 7.2632, 7.26386, 7.26451, 7.26516, 7.26581, 7.26647, 7.26712, 7.26777, 7.26842, 7.26907, 7.26972, 7.27037, 7.27102, 7.27167, 7.27232, 7.27297, 7.27362, 7.27427, 7.27492, 7.27557, 7.27622, 7.27687, 7.27752, 7.27817, 7.27881, 7.27946, 7.28011, 7.28076, 7.28141, 7.28205, 7.2827, 7.28335, 7.28399, 7.28464, 7.28529, 7.28593, 7.28658, 7.28723, 7.28787, 7.28852, 7.28916, 7.28981, 7.29045, 7.2911, 7.29174, 7.29239, 7.29303, 7.29368, 7.29432, 7.29497, 7.29561, 7.29625, 7.2969, 7.29754, 7.29818, 7.29883, 7.29947, 7.30011, 7.30076, 7.3014, 7.30204, 7.30268, 7.30332, 7.30397, 7.30461, 7.30525, 7.30589, 7.30653, 7.30717, 7.30781, 7.30845, 7.30909, 7.30973, 7.31037, 7.31101, 7.31165, 7.31229, 7.31293, 7.31357, 7.31421, 7.31485, 7.31549, 7.31613, 7.31676, 7.3174, 7.31804, 7.31868, 7.31932, 7.31995, 7.32059, 7.32123, 7.32186, 7.3225, 7.32314, 7.32377, 7.32441, 7.32505, 7.32568, 7.32632, 7.32695, 7.32759, 7.32823, 7.32886, 7.3295, 7.33013, 7.33076, 7.3314, 7.33203, 7.33267, 7.3333, 7.33394, 7.33457, 7.3352, 7.33584, 7.33647, 7.3371, 7.33774, 7.33837, 7.339, 7.33963, 7.34027, 7.3409, 7.34153, 7.34216, 7.34279, 7.34342, 7.34406, 7.34469, 7.34532, 7.34595, 7.34658, 7.34721, 7.34784, 7.34847, 7.3491, 7.34973, 7.35036, 7.35099, 7.35162, 7.35225, 7.35288, 7.3535, 7.35413, 7.35476, 7.35539, 7.35602, 7.35665, 7.35727, 7.3579, 7.35853, 7.35916, 7.35978, 7.36041, 7.36104, 7.36166, 7.36229, 7.36292, 7.36354, 7.36417, 7.3648, 7.36542, 7.36605, 7.36667, 7.3673, 7.36792, 7.36855, 7.36917, 7.3698, 7.37042, 7.37105, 7.37167, 7.37229, 7.37292, 7.37354, 7.37416, 7.37479, 7.37541, 7.37603, 7.37666, 7.37728, 7.3779, 7.37852, 7.37915, 7.37977, 7.38039, 7.38101, 7.38163, 7.38226, 7.38288, 7.3835, 7.38412, 7.38474, 7.38536, 7.38598, 7.3866, 7.38722, 7.38784, 7.38846, 7.38908, 7.3897, 7.39032, 7.39094, 7.39156, 7.39218, 7.3928, 7.39342, 7.39403, 7.39465, 7.39527, 7.39589, 7.39651, 7.39712, 7.39774, 7.39836, 7.39898, 7.39959, 7.40021, 7.40083, 7.40144, 7.40206, 7.40268, 7.40329, 7.40391, 7.40453, 7.40514, 7.40576, 7.40637, 7.40699, 7.4076, 7.40822, 7.40883, 7.40945, 7.41006, 7.41068, 7.41129, 7.4119, 7.41252, 7.41313, 7.41375, 7.41436, 7.41497, 7.41559, 7.4162, 7.41681, 7.41742, 7.41804, 7.41865, 7.41926, 7.41987, 7.42049, 7.4211, 7.42171, 7.42232, 7.42293, 7.42354, 7.42415, 7.42476, 7.42538, 7.42599, 7.4266, 7.42721, 7.42782, 7.42843, 7.42904, 7.42965, 7.43026, 7.43086, 7.43147, 7.43208, 7.43269, 7.4333, 7.43391, 7.43452, 7.43513, 7.43573, 7.43634, 7.43695, 7.43756, 7.43817, 7.43877, 7.43938, 7.43999, 7.44059, 7.4412, 7.44181, 7.44241, 7.44302, 7.44363, 7.44423, 7.44484, 7.44544, 7.44605, 7.44666, 7.44726, 7.44787, 7.44847, 7.44908, 7.44968, 7.45029, 7.45089, 7.45149, 7.4521, 7.4527, 7.45331, 7.45391, 7.45451, 7.45512, 7.45572, 7.45632, 7.45693, 7.45753, 7.45813, 7.45873, 7.45934, 7.45994, 7.46054, 7.46114, 7.46175, 7.46235, 7.46295, 7.46355, 7.46415, 7.46475, 7.46535, 7.46595, 7.46655, 7.46716, 7.46776, 7.46836, 7.46896, 7.46956, 7.47016, 7.47076, 7.47135, 7.47195, 7.47255, 7.47315, 7.47375, 7.47435, 7.47495, 7.47555, 7.47615, 7.47674, 7.47734, 7.47794, 7.47854, 7.47914, 7.47973, 7.48033, 7.48093, 7.48152, 7.48212, 7.48272, 7.48331, 7.48391, 7.48451, 7.4851, 7.4857, 7.4863, 7.48689, 7.48749, 7.48808, 7.48868, 7.48927, 7.48987, 7.49046, 7.49106, 7.49165, 7.49225, 7.49284, 7.49344, 7.49403, 7.49462, 7.49522, 7.49581, 7.4964, 7.497, 7.49759, 7.49818, 7.49878, 7.49937, 7.49996, 7.50056, 7.50115, 7.50174, 7.50233, 7.50292, 7.50352, 7.50411, 7.5047, 7.50529, 7.50588, 7.50647, 7.50706, 7.50765, 7.50825, 7.50884, 7.50943, 7.51002, 7.51061, 7.5112, 7.51179, 7.51238, 7.51297, 7.51356, 7.51415, 7.51473, 7.51532, 7.51591, 7.5165, 7.51709, 7.51768, 7.51827, 7.51885, 7.51944, 7.52003, 7.52062, 7.52121, 7.52179, 7.52238, 7.52297, 7.52356, 7.52414, 7.52473, 7.52532, 7.5259, 7.52649, 7.52708, 7.52766, 7.52825, 7.52883, 7.52942, 7.53, 7.53059, 7.53118, 7.53176, 7.53235, 7.53293, 7.53352, 7.5341, 7.53468, 7.53527, 7.53585, 7.53644, 7.53702, 7.53761, 7.53819, 7.53877, 7.53936, 7.53994, 7.54052, 7.54111, 7.54169, 7.54227, 7.54285, 7.54344, 7.54402, 7.5446, 7.54518, 7.54576, 7.54635, 7.54693, 7.54751, 7.54809, 7.54867, 7.54925, 7.54983, 7.55042, 7.551, 7.55158, 7.55216, 7.55274, 7.55332, 7.5539, 7.55448, 7.55506, 7.55564, 7.55622, 7.5568, 7.55738, 7.55795, 7.55853, 7.55911, 7.55969, 7.56027, 7.56085, 7.56143, 7.562, 7.56258, 7.56316, 7.56374, 7.56432, 7.56489, 7.56547, 7.56605, 7.56663, 7.5672, 7.56778, 7.56836, 7.56893, 7.56951, 7.57009, 7.57066, 7.57124, 7.57181, 7.57239, 7.57297, 7.57354, 7.57412, 7.57469, 7.57527, 7.57584, 7.57642, 7.57699, 7.57757, 7.57814, 7.57872, 7.57929, 7.57986, 7.58044, 7.58101, 7.58159, 7.58216, 7.58273, 7.58331, 7.58388, 7.58445, 7.58502, 7.5856, 7.58617, 7.58674, 7.58732, 7.58789, 7.58846, 7.58903, 7.5896, 7.59018, 7.59075, 7.59132, 7.59189, 7.59246, 7.59303, 7.5936, 7.59417, 7.59475, 7.59532, 7.59589, 7.59646, 7.59703, 7.5976, 7.59817, 7.59874, 7.59931, 7.59988, 7.60045, 7.60101, 7.60158, 7.60215, 7.60272, 7.60329, 7.60386, 7.60443, 7.605, 7.60557, 7.60613, 7.6067, 7.60727, 7.60784, 7.6084, 7.60897, 7.60954, 7.61011, 7.61067, 7.61124, 7.61181, 7.61237, 7.61294, 7.61351, 7.61407, 7.61464, 7.61521, 7.61577, 7.61634, 7.6169, 7.61747, 7.61804, 7.6186, 7.61917, 7.61973, 7.6203, 7.62086, 7.62143, 7.62199, 7.62256, 7.62312, 7.62368, 7.62425, 7.62481, 7.62538, 7.62594, 7.6265, 7.62707, 7.62763, 7.62819, 7.62876, 7.62932, 7.62988, 7.63045, 7.63101, 7.63157, 7.63213, 7.6327, 7.63326, 7.63382, 7.63438, 7.63494, 7.63551, 7.63607, 7.63663, 7.63719, 7.63775, 7.63831, 7.63887, 7.63943, 7.63999, 7.64055, 7.64112, 7.64168, 7.64224, 7.6428, 7.64336, 7.64392, 7.64448, 7.64503, 7.64559, 7.64615, 7.64671, 7.64727, 7.64783, 7.64839, 7.64895, 7.64951, 7.65007, 7.65062, 7.65118, 7.65174, 7.6523, 7.65286, 7.65341, 7.65397, 7.65453, 7.65509, 7.65564, 7.6562, 7.65676, 7.65731, 7.65787, 7.65843, 7.65898, 7.65954, 7.6601, 7.66065, 7.66121, 7.66176, 7.66232, 7.66288, 7.66343, 7.66399, 7.66454, 7.6651, 7.66565, 7.66621, 7.66676, 7.66732, 7.66787, 7.66843, 7.66898, 7.66953, 7.67009, 7.67064, 7.6712, 7.67175, 7.6723, 7.67286, 7.67341, 7.67396, 7.67452, 7.67507, 7.67562, 7.67618, 7.67673, 7.67728, 7.67783, 7.67839, 7.67894, 7.67949, 7.68004, 7.68059, 7.68115, 7.6817, 7.68225, 7.6828, 7.68335, 7.6839, 7.68445, 7.685, 7.68556, 7.68611, 7.68666, 7.68721, 7.68776, 7.68831, 7.68886, 7.68941, 7.68996, 7.69051, 7.69106, 7.69161, 7.69216, 7.6927, 7.69325, 7.6938, 7.69435, 7.6949, 7.69545, 7.696, 7.69655, 7.69709, 7.69764, 7.69819, 7.69874, 7.69929, 7.69983, 7.70038, 7.70093, 7.70148, 7.70202, 7.70257, 7.70312, 7.70366, 7.70421, 7.70476, 7.7053, 7.70585, 7.7064, 7.70694, 7.70749, 7.70803, 7.70858, 7.70913, 7.70967, 7.71022, 7.71076, 7.71131, 7.71185, 7.7124, 7.71294, 7.71349, 7.71403, 7.71458, 7.71512, 7.71567, 7.71621, 7.71675, 7.7173, 7.71784, 7.71839, 7.71893, 7.71947, 7.72002, 7.72056, 7.7211, 7.72165, 7.72219, 7.72273, 7.72327, 7.72382, 7.72436, 7.7249, 7.72544, 7.72599, 7.72653, 7.72707, 7.72761, 7.72815, 7.7287, 7.72924, 7.72978, 7.73032, 7.73086, 7.7314, 7.73194, 7.73248, 7.73302, 7.73356, 7.73411, 7.73465, 7.73519, 7.73573, 7.73627, 7.73681, 7.73735, 7.73789, 7.73842, 7.73896, 7.7395, 7.74004, 7.74058, 7.74112, 7.74166, 7.7422, 7.74274, 7.74328, 7.74381, 7.74435, 7.74489, 7.74543, 7.74597, 7.7465, 7.74704, 7.74758, 7.74812, 7.74865, 7.74919, 7.74973, 7.75027, 7.7508, 7.75134, 7.75188, 7.75241, 7.75295, 7.75349, 7.75402, 7.75456, 7.7551, 7.75563, 7.75617, 7.7567, 7.75724, 7.75777, 7.75831, 7.75884, 7.75938, 7.75991, 7.76045, 7.76098, 7.76152, 7.76205, 7.76259, 7.76312, 7.76366, 7.76419, 7.76473, 7.76526, 7.76579, 7.76633, 7.76686, 7.76739, 7.76793, 7.76846, 7.76899, 7.76953, 7.77006, 7.77059, 7.77113, 7.77166, 7.77219, 7.77272, 7.77326, 7.77379, 7.77432, 7.77485, 7.77538, 7.77592, 7.77645, 7.77698, 7.77751, 7.77804, 7.77857, 7.7791, 7.77964, 7.78017, 7.7807, 7.78123, 7.78176, 7.78229, 7.78282, 7.78335, 7.78388, 7.78441, 7.78494, 7.78547, 7.786, 7.78653, 7.78706, 7.78759, 7.78812, 7.78865, 7.78917, 7.7897, 7.79023, 7.79076, 7.79129, 7.79182, 7.79235, 7.79288, 7.7934, 7.79393, 7.79446, 7.79499, 7.79552, 7.79604, 7.79657, 7.7971, 7.79763, 7.79815, 7.79868, 7.79921, 7.79973, 7.80026, 7.80079, 7.80131, 7.80184, 7.80237, 7.80289, 7.80342, 7.80395, 7.80447, 7.805, 7.80552, 7.80605, 7.80657, 7.8071, 7.80762, 7.80815, 7.80867, 7.8092, 7.80972, 7.81025, 7.81077, 7.8113, 7.81182, 7.81235, 7.81287, 7.8134, 7.81392, 7.81444, 7.81497, 7.81549, 7.81602, 7.81654, 7.81706, 7.81759, 7.81811, 7.81863, 7.81916, 7.81968, 7.8202, 7.82072, 7.82125, 7.82177, 7.82229, 7.82281, 7.82334, 7.82386, 7.82438, 7.8249, 7.82542, 7.82594, 7.82647, 7.82699, 7.82751, 7.82803, 7.82855, 7.82907, 7.82959, 7.83011, 7.83063, 7.83116, 7.83168, 7.8322, 7.83272, 7.83324, 7.83376, 7.83428, 7.8348, 7.83532, 7.83584, 7.83636, 7.83688, 7.83739, 7.83791, 7.83843, 7.83895, 7.83947, 7.83999, 7.84051, 7.84103, 7.84155, 7.84206, 7.84258, 7.8431, 7.84362, 7.84414, 7.84465, 7.84517, 7.84569, 7.84621, 7.84673, 7.84724, 7.84776, 7.84828, 7.84879, 7.84931, 7.84983, 7.85034, 7.85086, 7.85138, 7.85189, 7.85241, 7.85293, 7.85344, 7.85396, 7.85448, 7.85499, 7.85551, 7.85602, 7.85654, 7.85705, 7.85757, 7.85808, 7.8586, 7.85911, 7.85963, 7.86014, 7.86066, 7.86117, 7.86169, 7.8622, 7.86272, 7.86323, 7.86375, 7.86426, 7.86477, 7.86529, 7.8658, 7.86632, 7.86683, 7.86734, 7.86786, 7.86837, 7.86888, 7.86939, 7.86991, 7.87042, 7.87093, 7.87145, 7.87196, 7.87247, 7.87298, 7.8735, 7.87401, 7.87452, 7.87503, 7.87554, 7.87606, 7.87657, 7.87708, 7.87759, 7.8781, 7.87861, 7.87912, 7.87963, 7.88015, 7.88066, 7.88117, 7.88168, 7.88219, 7.8827, 7.88321, 7.88372, 7.88423, 7.88474, 7.88525, 7.88576, 7.88627, 7.88678, 7.88729, 7.8878, 7.88831, 7.88882, 7.88933, 7.88984, 7.89034, 7.89085, 7.89136, 7.89187, 7.89238, 7.89289, 7.8934, 7.8939, 7.89441, 7.89492, 7.89543, 7.89594, 7.89644, 7.89695, 7.89746, 7.89797, 7.89847, 7.89898, 7.89949, 7.9, 7.9005, 7.90101, 7.90152, 7.90202, 7.90253, 7.90304, 7.90354, 7.90405, 7.90456, 7.90506, 7.90557, 7.90607, 7.90658, 7.90709, 7.90759, 7.9081, 7.9086, 7.90911, 7.90961, 7.91012, 7.91062, 7.91113, 7.91163, 7.91214, 7.91264, 7.91315, 7.91365, 7.91416, 7.91466, 7.91516, 7.91567, 7.91617, 7.91668, 7.91718, 7.91768, 7.91819, 7.91869, 7.91919, 7.9197, 7.9202, 7.9207, 7.92121, 7.92171, 7.92221, 7.92272, 7.92322, 7.92372, 7.92422, 7.92473, 7.92523, 7.92573, 7.92623, 7.92673, 7.92724, 7.92774, 7.92824, 7.92874, 7.92924, 7.92974, 7.93025, 7.93075, 7.93125, 7.93175, 7.93225, 7.93275, 7.93325, 7.93375, 7.93425, 7.93475, 7.93525, 7.93575, 7.93625, 7.93675, 7.93725, 7.93775, 7.93825, 7.93875, 7.93925, 7.93975, 7.94025, 7.94075, 7.94125, 7.94175, 7.94225, 7.94275, 7.94325, 7.94375, 7.94424, 7.94474, 7.94524, 7.94574, 7.94624, 7.94674, 7.94723, 7.94773, 7.94823, 7.94873, 7.94923, 7.94972, 7.95022, 7.95072, 7.95122, 7.95171, 7.95221, 7.95271, 7.9532, 7.9537, 7.9542, 7.95469, 7.95519, 7.95569, 7.95618, 7.95668, 7.95718, 7.95767, 7.95817, 7.95867, 7.95916, 7.95966, 7.96015, 7.96065, 7.96114, 7.96164, 7.96213, 7.96263, 7.96312, 7.96362, 7.96411, 7.96461, 7.9651, 7.9656, 7.96609, 7.96659, 7.96708, 7.96758, 7.96807, 7.96857, 7.96906, 7.96955, 7.97005, 7.97054, 7.97103, 7.97153, 7.97202, 7.97251, 7.97301, 7.9735, 7.97399, 7.97449, 7.97498, 7.97547, 7.97597, 7.97646, 7.97695, 7.97744, 7.97794, 7.97843, 7.97892, 7.97941, 7.9799, 7.9804, 7.98089, 7.98138, 7.98187, 7.98236, 7.98286, 7.98335, 7.98384, 7.98433, 7.98482, 7.98531, 7.9858, 7.98629, 7.98678, 7.98727, 7.98776, 7.98826, 7.98875, 7.98924, 7.98973, 7.99022, 7.99071, 7.9912, 7.99169, 7.99218, 7.99267, 7.99316, 7.99364, 7.99413, 7.99462, 7.99511, 7.9956, 7.99609, 7.99658, 7.99707, 7.99756, 7.99805, 7.99853, 7.99902, 7.99951, 8};
						int n[4096] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8, 6, 9, 2, 4, 2, 11, 10, 2, 5, 5, 4, 3, 10, 9, 7, 8, 9, 3, 9, 11, 6, 11, 4, 8, 9, 4, 7, 9, 2, 7, 3, 8, 11, 3, 10, 6, 5, 9, 3, 7, 6, 7, 4, 7, 5, 3, 6, 11, 10, 7, 2, 4, 2, 8, 8, 6, 3, 10, 6, 8, 4, 2, 4, 11, 5, 10, 2, 9, 5, 7, 3, 2, 9, 4, 8, 4, 11, 2, 6, 5, 11, 10, 2, 7, 7, 11, 3, 8, 4, 5, 4, 8, 5, 5, 10, 3, 4, 6, 9, 2, 6, 3, 9, 7, 2, 9, 2, 6, 7, 2, 7, 4, 11, 2, 6, 7, 9, 5, 7, 5, 10, 10, 5, 7, 6, 2, 4, 7, 3, 6, 5, 3, 3, 8, 2, 7, 11, 3, 2, 5, 7, 9, 8, 8, 2, 4, 6, 3, 4, 6, 11, 5, 6, 6, 8, 11, 5, 4, 6, 2, 8, 7, 8, 2, 9, 6, 7, 4, 3, 8, 8, 5, 3, 7, 2, 4, 5, 6, 5, 6, 11, 9, 9, 4, 10, 5, 3, 2, 5, 6, 7, 3, 5, 2, 9, 10, 3, 8, 10, 5, 6, 5, 11, 2, 8, 2, 2, 6, 10, 6, 10, 9, 6, 8, 5, 4, 11, 5, 5, 11, 3, 6, 2, 11, 11, 3, 10, 11, 6, 6, 4, 7, 8, 11, 6, 3, 4, 5, 10, 5, 10, 2, 10, 5, 9, 4, 10, 2, 5, 6, 8, 11, 3, 7, 7, 7, 2, 5, 8, 9, 8, 9, 8, 9, 6, 5, 2, 7, 7, 10, 5, 7, 5, 10, 9, 11, 7, 2, 5, 3, 2, 5, 4, 7, 4, 5, 4, 3, 5, 6, 10, 10, 2, 6, 11, 11, 9, 4, 10, 5, 8, 6, 11, 6, 10, 7, 11, 2, 2, 9, 5, 8, 9, 8, 3, 4, 8, 2, 3, 4, 8, 9, 10, 3, 7, 4, 8, 7, 7, 6, 2, 8, 3, 5, 4, 2, 5, 7, 4, 8, 8, 10, 5, 7, 5, 3, 2, 11, 7, 11, 2, 6, 8, 2, 11, 6, 8, 3, 5, 8, 7, 5, 5, 10, 5, 6, 6, 11, 4, 5, 8, 9, 10, 10, 9, 11, 11, 4, 6, 7, 3, 2, 8, 4, 4, 3, 10, 2, 4, 6, 11, 11, 10, 5, 8, 6, 10, 8, 5, 6, 11, 2, 9, 6, 3, 9, 7, 4, 5, 4, 8, 2, 7, 8, 3, 10, 4, 11, 7, 11, 9, 10, 7, 2, 4, 10, 4, 3, 2, 6, 11, 7, 8, 11, 10, 4, 11, 4, 7, 2, 6, 7, 11, 9, 5, 5, 10, 2, 6, 2, 5, 9, 5, 8, 9, 11, 10, 3, 6, 9, 7, 10, 8, 10, 9, 4, 3, 6, 11, 11, 5, 10, 7, 5, 9, 3, 6, 6, 7, 5, 10, 11, 10, 11, 7, 6, 4, 4, 8, 6, 3, 4, 10, 3, 10, 9, 9, 10, 9, 9, 6, 5, 6, 8, 5, 9, 4, 9, 6, 4, 3, 9, 4, 3, 10, 4, 6, 2, 3, 8, 9, 8, 5, 9, 8, 11, 7, 2, 2, 9, 3, 11, 6, 2, 10, 3, 3, 9, 6, 3, 7, 10, 11, 2, 9, 2, 7, 7, 7, 2, 7, 11, 4, 11, 5, 7, 5, 6, 9, 5, 3, 8, 3, 2, 4, 4, 8, 10, 2, 6, 9, 5, 5, 6, 5, 8, 3, 11, 4, 11, 3, 3, 5, 11, 4, 2, 9, 2, 11, 4, 2, 11, 6, 6, 8, 2, 8, 4, 7, 9, 2, 6, 3, 5, 2, 9, 5, 8, 8, 2, 8, 6, 9, 7, 3, 11, 10, 10, 6, 11, 9, 3, 10, 9, 7, 9, 3, 6, 5, 3, 2, 5, 6, 7, 5, 4, 4, 2, 9, 9, 4, 4, 9, 8, 9, 11, 5, 2, 4, 11, 7, 11, 10, 7, 6, 5, 3, 11, 3, 6, 2, 3, 6, 5, 6, 6, 11, 6, 4, 9, 5, 6, 2, 10, 10, 9, 10, 4, 3, 11, 9, 2, 7, 11, 8, 4, 11, 10, 7, 9, 8, 8, 6, 2, 10, 6, 3, 9, 5, 7, 10, 10, 5, 9, 3, 7, 5, 9, 5, 11, 7, 11, 6, 7, 4, 3, 5, 4, 11, 3, 8, 4, 8, 10, 7, 11, 3, 6, 11, 10, 6, 2, 5, 8, 11, 4, 8, 3, 4, 3, 10, 10, 4, 11, 10, 11, 7, 8, 4, 5, 4, 11, 2, 10, 9, 11, 11, 6, 10, 8, 2, 3, 5, 9, 6, 2, 4, 5, 9, 7, 6, 3, 11, 4, 10, 9, 2, 7, 9, 9, 8, 4, 11, 3, 5, 9, 8, 2, 8, 5, 7, 11, 10, 10, 8, 9, 9, 2, 11, 3, 8, 3, 6, 9, 5, 9, 8, 7, 6, 8, 6, 4, 3, 6, 5, 10, 5, 2, 4, 4, 5, 2, 5, 3, 9, 3, 11, 2, 10, 5, 6, 4, 8, 8, 9, 4, 7, 5, 8, 5, 6, 4, 6, 8, 10, 9, 5, 5, 10, 7, 2, 10, 3, 5, 5, 5, 11, 3, 3, 11, 6, 11, 4, 2, 11, 6, 7, 3, 6, 7, 10, 8, 6, 5, 11, 10, 6, 5, 7, 11, 7, 5, 6, 7, 10, 6, 3, 8, 2, 11, 9, 10, 5, 2, 4, 5, 10, 10, 10, 2, 6, 5, 10, 11, 11, 3, 5, 10, 3, 9, 7, 6, 4, 3, 6, 11, 8, 9, 5, 7, 5, 5, 2, 6, 2, 8, 7, 11, 9, 3, 3, 7, 8, 9, 3, 2, 4, 4, 4, 10, 9, 11, 8, 4, 10, 8, 3, 10, 4, 6, 5, 5, 6, 2, 2, 6, 7, 10, 7, 3, 11, 7, 5, 9, 6, 8, 10, 6, 11, 7, 9, 4, 7, 6, 8, 6, 4, 7, 10, 4, 2, 9, 4, 2, 10, 11, 10, 8, 6, 4, 10, 10, 11, 11, 7, 8, 2, 11, 2, 7, 8, 8, 3, 5, 4, 2, 8, 11, 4, 4, 5, 5, 9, 6, 11, 11, 3, 7, 9, 4, 10, 11, 7, 8, 2, 6, 10, 6, 11, 5, 7, 3, 9, 2, 9, 10, 5, 10, 3, 2, 11, 3, 8, 5, 9, 10, 2, 7, 9, 6, 7, 5, 4, 4, 9, 10, 9, 10, 3, 6, 4, 7, 10, 10, 11, 6, 8, 8, 8, 7, 6, 11, 4, 9, 2, 9, 11, 6, 2, 8, 5, 2, 3, 5, 10, 2, 3, 4, 7, 11, 3, 5, 7, 11, 5, 10, 10, 2, 9, 7, 6, 3, 10, 5, 3, 2, 11, 4, 9, 5, 4, 8, 8, 8, 10, 10, 6, 6, 6, 8, 11, 7, 3, 8, 4, 5, 2, 11, 2, 3, 7, 8, 11, 2, 3, 8, 8, 6, 5, 6, 2, 9, 7, 11, 4, 7, 2, 6, 2, 5, 9, 3, 7, 3, 7, 6, 10, 8, 2, 10, 5, 2, 7, 5, 5, 4, 4, 11, 11, 9, 4, 5, 6, 9, 10, 4, 9, 10, 8, 2, 6, 7, 5, 7, 11, 3, 4, 6, 8, 6, 11, 6, 5, 8, 11, 11, 10, 10, 7, 10, 11, 2, 2, 2, 10, 6, 8, 3, 3, 8, 9, 7, 2, 8, 3, 7, 8, 3, 2, 2, 7, 9, 4, 10, 10, 4, 9, 3, 10, 7, 5, 11, 11, 4, 5, 9, 10, 5, 5, 9, 11, 10, 8, 4, 6, 5, 9, 4, 5, 5, 5, 6, 8, 10, 3, 5, 8, 7, 7, 7, 6, 5, 10, 2, 2, 11, 5, 11, 7, 8, 3, 11, 3, 5, 2, 4, 2, 2, 8, 8, 10, 2, 10, 3, 10, 6, 9, 3, 7, 5, 3, 2, 4, 9, 8, 11, 6, 2, 11, 2, 6, 9, 11, 2, 5, 11, 9, 3, 10, 6, 3, 2, 8, 4, 5, 10, 8, 8, 9, 3, 5, 7, 8, 9, 6, 9, 8, 11, 5, 10, 11, 10, 8, 7, 5, 6, 8, 4, 3, 8, 2, 6, 3, 10, 7, 2, 11, 3, 9, 3, 11, 9, 11, 8, 4, 9, 7, 6, 3, 2, 2, 2, 2, 3, 2, 3, 11, 8, 9, 11, 9, 5, 7, 8, 7, 8, 6, 5, 3, 3, 9, 10, 6, 11, 4, 4, 9, 7, 10, 7, 4, 11, 9, 11, 4, 2, 7, 5, 11, 5, 8, 6, 2, 4, 2, 11, 11, 3, 2, 3, 8, 10, 7, 2, 8, 9, 4, 9, 2, 7, 3, 4, 3, 10, 10, 10, 2, 8, 3, 3, 6, 6, 5, 4, 6, 6, 4, 7, 8, 4, 4, 11, 7, 8, 8, 7, 6, 5, 9, 6, 3, 6, 6, 11, 6, 11, 8, 2, 11, 4, 11, 3, 5, 10, 8, 7, 2, 3, 11, 4, 10, 11, 9, 8, 11, 9, 2, 7, 6, 11, 11, 9, 9, 2, 10, 7, 9, 8, 7, 7, 11, 6, 9, 2, 2, 10, 9, 7, 6, 10, 11, 3, 6, 8, 8, 8, 6, 10, 5, 7, 4, 5, 8, 9, 7, 5, 5, 6, 4, 4, 2, 5, 3, 10, 11, 3, 3, 7, 8, 6, 9, 11, 11, 8, 4, 6, 8, 6, 2, 8, 7, 8, 3, 9, 3, 7, 7, 6, 2, 5, 4, 9, 6, 6, 4, 3, 4, 4, 9, 9, 6, 4, 11, 5, 2, 9, 7, 2, 11, 6, 8, 2, 5, 4, 3, 6, 5, 2, 2, 9, 3, 4, 7, 9, 4, 8, 9, 6, 4, 6, 10, 8, 3, 10, 8, 9, 10, 10, 11, 11, 6, 11, 10, 11, 10, 2, 2, 11, 7, 3, 9, 7, 8, 9, 10, 7, 2, 8, 11, 10, 8, 3, 2, 7, 11, 3, 6, 5, 7, 7, 3, 4, 2, 2, 4, 9, 9, 5, 8, 9, 9, 9, 5, 7, 11, 10, 7, 10, 4, 3, 11, 7, 2, 4, 2, 11, 2, 10, 6, 3, 11, 11, 4, 7, 11, 2, 9, 7, 4, 8, 4, 9, 3, 5, 4, 7, 2, 10, 10, 6, 7, 8, 7, 11, 5, 4, 11, 4, 7, 7, 10, 8, 2, 10, 9, 5, 4, 8, 9, 3, 3, 2, 2, 10, 2, 9, 8, 5, 6, 5, 10, 6, 6, 8, 6, 8, 3, 8, 2, 6, 9, 10, 6, 4, 10, 10, 3, 8, 11, 2, 8, 11, 9, 2, 2, 2, 8, 5, 6, 2, 3, 9, 4, 8, 8, 6, 2, 6, 5, 9, 2, 4, 2, 4, 11, 7, 7, 8, 6, 3, 4, 10, 5, 10, 8, 5, 11, 8, 4, 7, 8, 11, 9, 6, 9, 9, 10, 5, 7, 3, 7, 2, 8, 11, 6, 2, 7, 6, 11, 9, 9, 4, 4, 10, 11, 10, 6, 4, 4, 3, 3, 9, 6, 5, 4, 6, 7, 9, 7, 7, 11, 11, 11, 5, 5, 7, 7, 8, 8, 11, 7, 6, 10, 11, 5, 8, 9, 2, 10, 5, 6, 10, 8, 2, 7, 9, 6, 3, 10, 9, 8, 2, 2, 10, 3, 4, 3, 7, 7, 2, 2, 2, 2, 4, 3, 10, 8, 10, 6, 10, 4, 9, 6, 9, 3, 9, 8, 2, 5, 5, 8, 11, 10, 11, 3, 6, 5, 4, 4, 5, 10, 6, 7, 3, 11, 3, 11, 5, 5, 9, 11, 6, 9, 10, 4, 6, 2, 9, 10, 11, 9, 2, 7, 8, 11, 2, 2, 2, 2, 8, 5, 11, 10, 9, 8, 7, 4, 8, 6, 6, 11, 10, 6, 8, 7, 8, 6, 4, 9, 3, 9, 6, 9, 3, 2, 9, 9, 3, 5, 5, 9, 11, 11, 10, 3, 11, 9, 11, 6, 6, 6, 9, 8, 5, 7, 2, 10, 4, 6, 11, 10, 8, 2, 9, 4, 5, 6, 7, 3, 2, 5, 5, 11, 4, 6, 5, 2, 10, 9, 11, 6, 8, 8, 7, 10, 6, 11, 9, 3, 6, 7, 5, 9, 11, 6, 4, 6, 3, 9, 4, 11, 3, 5, 10, 3, 9, 10, 4, 4, 2, 9, 8, 11, 2, 6, 10, 10, 2, 9, 8, 5, 4, 10, 6, 11, 11, 7, 5, 3, 6, 9, 8, 2, 4, 3, 3, 3, 6, 4, 5, 2, 5, 3, 11, 10, 6, 2, 3};
						int an[4096];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 9) {
						static float s_mean[8192] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 1132, 1133, 1134, 1135, 1136, 1137, 1138, 1139, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149, 1150, 1151, 1152, 1153, 1154, 1155, 1156, 1157, 1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187, 1188, 1189, 1190, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249, 1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259, 1260, 1261, 1262, 1263, 1264, 1265, 1266, 1267, 1268, 1269, 1270, 1271, 1272, 1273, 1274, 1275, 1276, 1277, 1278, 1279, 1280, 1281, 1282, 1283, 1284, 1285, 1286, 1287, 1288, 1289, 1290, 1291, 1292, 1293, 1294, 1295, 1296, 1297, 1298, 1299, 1300, 1301, 1302, 1303, 1304, 1305, 1306, 1307, 1308, 1309, 1310, 1311, 1312, 1313, 1314, 1315, 1316, 1317, 1318, 1319, 1320, 1321, 1322, 1323, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1332, 1333, 1334, 1335, 1336, 1337, 1338, 1339, 1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1371, 1372, 1373, 1374, 1375, 1376, 1377, 1378, 1379, 1380, 1381, 1382, 1383, 1384, 1385, 1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396, 1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405, 1406, 1407, 1408, 1409, 1410, 1411, 1412, 1413, 1414, 1415, 1416, 1417, 1418, 1419, 1420, 1421, 1422, 1423, 1424, 1425, 1426, 1427, 1428, 1429, 1430, 1431, 1432, 1433, 1434, 1435, 1436, 1437, 1438, 1439, 1440, 1441, 1442, 1443, 1444, 1445, 1446, 1447, 1448, 1449, 1450, 1451, 1452, 1453, 1454, 1455, 1456, 1457, 1458, 1459, 1460, 1461, 1462, 1463, 1464, 1465, 1466, 1467, 1468, 1469, 1470, 1471, 1472, 1473, 1474, 1475, 1476, 1477, 1478, 1479, 1480, 1481, 1482, 1483, 1484, 1485, 1486, 1487, 1488, 1489, 1490, 1491, 1492, 1493, 1494, 1495, 1496, 1497, 1498, 1499, 1500, 1501, 1502, 1503, 1504, 1505, 1506, 1507, 1508, 1509, 1510, 1511, 1512, 1513, 1514, 1515, 1516, 1517, 1518, 1519, 1520, 1521, 1522, 1523, 1524, 1525, 1526, 1527, 1528, 1529, 1530, 1531, 1532, 1533, 1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545, 1546, 1547, 1548, 1549, 1550, 1551, 1552, 1553, 1554, 1555, 1556, 1557, 1558, 1559, 1560, 1561, 1562, 1563, 1564, 1565, 1566, 1567, 1568, 1569, 1570, 1571, 1572, 1573, 1574, 1575, 1576, 1577, 1578, 1579, 1580, 1581, 1582, 1583, 1584, 1585, 1586, 1587, 1588, 1589, 1590, 1591, 1592, 1593, 1594, 1595, 1596, 1597, 1598, 1599, 1600, 1601, 1602, 1603, 1604, 1605, 1606, 1607, 1608, 1609, 1610, 1611, 1612, 1613, 1614, 1615, 1616, 1617, 1618, 1619, 1620, 1621, 1622, 1623, 1624, 1625, 1626, 1627, 1628, 1629, 1630, 1631, 1632, 1633, 1634, 1635, 1636, 1637, 1638, 1639, 1640, 1641, 1642, 1643, 1644, 1645, 1646, 1647, 1648, 1649, 1650, 1651, 1652, 1653, 1654, 1655, 1656, 1657, 1658, 1659, 1660, 1661, 1662, 1663, 1664, 1665, 1666, 1667, 1668, 1669, 1670, 1671, 1672, 1673, 1674, 1675, 1676, 1677, 1678, 1679, 1680, 1681, 1682, 1683, 1684, 1685, 1686, 1687, 1688, 1689, 1690, 1691, 1692, 1693, 1694, 1695, 1696, 1697, 1698, 1699, 1700, 1701, 1702, 1703, 1704, 1705, 1706, 1707, 1708, 1709, 1710, 1711, 1712, 1713, 1714, 1715, 1716, 1717, 1718, 1719, 1720, 1721, 1722, 1723, 1724, 1725, 1726, 1727, 1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736, 1737, 1738, 1739, 1740, 1741, 1742, 1743, 1744, 1745, 1746, 1747, 1748, 1749, 1750, 1751, 1752, 1753, 1754, 1755, 1756, 1757, 1758, 1759, 1760, 1761, 1762, 1763, 1764, 1765, 1766, 1767, 1768, 1769, 1770, 1771, 1772, 1773, 1774, 1775, 1776, 1777, 1778, 1779, 1780, 1781, 1782, 1783, 1784, 1785, 1786, 1787, 1788, 1789, 1790, 1791, 1792, 1793, 1794, 1795, 1796, 1797, 1798, 1799, 1800, 1801, 1802, 1803, 1804, 1805, 1806, 1807, 1808, 1809, 1810, 1811, 1812, 1813, 1814, 1815, 1816, 1817, 1818, 1819, 1820, 1821, 1822, 1823, 1824, 1825, 1826, 1827, 1828, 1829, 1830, 1831, 1832, 1833, 1834, 1835, 1836, 1837, 1838, 1839, 1840, 1841, 1842, 1843, 1844, 1845, 1846, 1847, 1848, 1849, 1850, 1851, 1852, 1853, 1854, 1855, 1856, 1857, 1858, 1859, 1860, 1861, 1862, 1863, 1864, 1865, 1866, 1867, 1868, 1869, 1870, 1871, 1872, 1873, 1874, 1875, 1876, 1877, 1878, 1879, 1880, 1881, 1882, 1883, 1884, 1885, 1886, 1887, 1888, 1889, 1890, 1891, 1892, 1893, 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909, 1910, 1911, 1912, 1913, 1914, 1915, 1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049, 2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2059, 2060, 2061, 2062, 2063, 2064, 2065, 2066, 2067, 2068, 2069, 2070, 2071, 2072, 2073, 2074, 2075, 2076, 2077, 2078, 2079, 2080, 2081, 2082, 2083, 2084, 2085, 2086, 2087, 2088, 2089, 2090, 2091, 2092, 2093, 2094, 2095, 2096, 2097, 2098, 2099, 2100, 2101, 2102, 2103, 2104, 2105, 2106, 2107, 2108, 2109, 2110, 2111, 2112, 2113, 2114, 2115, 2116, 2117, 2118, 2119, 2120, 2121, 2122, 2123, 2124, 2125, 2126, 2127, 2128, 2129, 2130, 2131, 2132, 2133, 2134, 2135, 2136, 2137, 2138, 2139, 2140, 2141, 2142, 2143, 2144, 2145, 2146, 2147, 2148, 2149, 2150, 2151, 2152, 2153, 2154, 2155, 2156, 2157, 2158, 2159, 2160, 2161, 2162, 2163, 2164, 2165, 2166, 2167, 2168, 2169, 2170, 2171, 2172, 2173, 2174, 2175, 2176, 2177, 2178, 2179, 2180, 2181, 2182, 2183, 2184, 2185, 2186, 2187, 2188, 2189, 2190, 2191, 2192, 2193, 2194, 2195, 2196, 2197, 2198, 2199, 2200, 2201, 2202, 2203, 2204, 2205, 2206, 2207, 2208, 2209, 2210, 2211, 2212, 2213, 2214, 2215, 2216, 2217, 2218, 2219, 2220, 2221, 2222, 2223, 2224, 2225, 2226, 2227, 2228, 2229, 2230, 2231, 2232, 2233, 2234, 2235, 2236, 2237, 2238, 2239, 2240, 2241, 2242, 2243, 2244, 2245, 2246, 2247, 2248, 2249, 2250, 2251, 2252, 2253, 2254, 2255, 2256, 2257, 2258, 2259, 2260, 2261, 2262, 2263, 2264, 2265, 2266, 2267, 2268, 2269, 2270, 2271, 2272, 2273, 2274, 2275, 2276, 2277, 2278, 2279, 2280, 2281, 2282, 2283, 2284, 2285, 2286, 2287, 2288, 2289, 2290, 2291, 2292, 2293, 2294, 2295, 2296, 2297, 2298, 2299, 2300, 2301, 2302, 2303, 2304, 2305, 2306, 2307, 2308, 2309, 2310, 2311, 2312, 2313, 2314, 2315, 2316, 2317, 2318, 2319, 2320, 2321, 2322, 2323, 2324, 2325, 2326, 2327, 2328, 2329, 2330, 2331, 2332, 2333, 2334, 2335, 2336, 2337, 2338, 2339, 2340, 2341, 2342, 2343, 2344, 2345, 2346, 2347, 2348, 2349, 2350, 2351, 2352, 2353, 2354, 2355, 2356, 2357, 2358, 2359, 2360, 2361, 2362, 2363, 2364, 2365, 2366, 2367, 2368, 2369, 2370, 2371, 2372, 2373, 2374, 2375, 2376, 2377, 2378, 2379, 2380, 2381, 2382, 2383, 2384, 2385, 2386, 2387, 2388, 2389, 2390, 2391, 2392, 2393, 2394, 2395, 2396, 2397, 2398, 2399, 2400, 2401, 2402, 2403, 2404, 2405, 2406, 2407, 2408, 2409, 2410, 2411, 2412, 2413, 2414, 2415, 2416, 2417, 2418, 2419, 2420, 2421, 2422, 2423, 2424, 2425, 2426, 2427, 2428, 2429, 2430, 2431, 2432, 2433, 2434, 2435, 2436, 2437, 2438, 2439, 2440, 2441, 2442, 2443, 2444, 2445, 2446, 2447, 2448, 2449, 2450, 2451, 2452, 2453, 2454, 2455, 2456, 2457, 2458, 2459, 2460, 2461, 2462, 2463, 2464, 2465, 2466, 2467, 2468, 2469, 2470, 2471, 2472, 2473, 2474, 2475, 2476, 2477, 2478, 2479, 2480, 2481, 2482, 2483, 2484, 2485, 2486, 2487, 2488, 2489, 2490, 2491, 2492, 2493, 2494, 2495, 2496, 2497, 2498, 2499, 2500, 2501, 2502, 2503, 2504, 2505, 2506, 2507, 2508, 2509, 2510, 2511, 2512, 2513, 2514, 2515, 2516, 2517, 2518, 2519, 2520, 2521, 2522, 2523, 2524, 2525, 2526, 2527, 2528, 2529, 2530, 2531, 2532, 2533, 2534, 2535, 2536, 2537, 2538, 2539, 2540, 2541, 2542, 2543, 2544, 2545, 2546, 2547, 2548, 2549, 2550, 2551, 2552, 2553, 2554, 2555, 2556, 2557, 2558, 2559, 2560, 2561, 2562, 2563, 2564, 2565, 2566, 2567, 2568, 2569, 2570, 2571, 2572, 2573, 2574, 2575, 2576, 2577, 2578, 2579, 2580, 2581, 2582, 2583, 2584, 2585, 2586, 2587, 2588, 2589, 2590, 2591, 2592, 2593, 2594, 2595, 2596, 2597, 2598, 2599, 2600, 2601, 2602, 2603, 2604, 2605, 2606, 2607, 2608, 2609, 2610, 2611, 2612, 2613, 2614, 2615, 2616, 2617, 2618, 2619, 2620, 2621, 2622, 2623, 2624, 2625, 2626, 2627, 2628, 2629, 2630, 2631, 2632, 2633, 2634, 2635, 2636, 2637, 2638, 2639, 2640, 2641, 2642, 2643, 2644, 2645, 2646, 2647, 2648, 2649, 2650, 2651, 2652, 2653, 2654, 2655, 2656, 2657, 2658, 2659, 2660, 2661, 2662, 2663, 2664, 2665, 2666, 2667, 2668, 2669, 2670, 2671, 2672, 2673, 2674, 2675, 2676, 2677, 2678, 2679, 2680, 2681, 2682, 2683, 2684, 2685, 2686, 2687, 2688, 2689, 2690, 2691, 2692, 2693, 2694, 2695, 2696, 2697, 2698, 2699, 2700, 2701, 2702, 2703, 2704, 2705, 2706, 2707, 2708, 2709, 2710, 2711, 2712, 2713, 2714, 2715, 2716, 2717, 2718, 2719, 2720, 2721, 2722, 2723, 2724, 2725, 2726, 2727, 2728, 2729, 2730, 2731, 2732, 2733, 2734, 2735, 2736, 2737, 2738, 2739, 2740, 2741, 2742, 2743, 2744, 2745, 2746, 2747, 2748, 2749, 2750, 2751, 2752, 2753, 2754, 2755, 2756, 2757, 2758, 2759, 2760, 2761, 2762, 2763, 2764, 2765, 2766, 2767, 2768, 2769, 2770, 2771, 2772, 2773, 2774, 2775, 2776, 2777, 2778, 2779, 2780, 2781, 2782, 2783, 2784, 2785, 2786, 2787, 2788, 2789, 2790, 2791, 2792, 2793, 2794, 2795, 2796, 2797, 2798, 2799, 2800, 2801, 2802, 2803, 2804, 2805, 2806, 2807, 2808, 2809, 2810, 2811, 2812, 2813, 2814, 2815, 2816, 2817, 2818, 2819, 2820, 2821, 2822, 2823, 2824, 2825, 2826, 2827, 2828, 2829, 2830, 2831, 2832, 2833, 2834, 2835, 2836, 2837, 2838, 2839, 2840, 2841, 2842, 2843, 2844, 2845, 2846, 2847, 2848, 2849, 2850, 2851, 2852, 2853, 2854, 2855, 2856, 2857, 2858, 2859, 2860, 2861, 2862, 2863, 2864, 2865, 2866, 2867, 2868, 2869, 2870, 2871, 2872, 2873, 2874, 2875, 2876, 2877, 2878, 2879, 2880, 2881, 2882, 2883, 2884, 2885, 2886, 2887, 2888, 2889, 2890, 2891, 2892, 2893, 2894, 2895, 2896, 2897, 2898, 2899, 2900, 2901, 2902, 2903, 2904, 2905, 2906, 2907, 2908, 2909, 2910, 2911, 2912, 2913, 2914, 2915, 2916, 2917, 2918, 2919, 2920, 2921, 2922, 2923, 2924, 2925, 2926, 2927, 2928, 2929, 2930, 2931, 2932, 2933, 2934, 2935, 2936, 2937, 2938, 2939, 2940, 2941, 2942, 2943, 2944, 2945, 2946, 2947, 2948, 2949, 2950, 2951, 2952, 2953, 2954, 2955, 2956, 2957, 2958, 2959, 2960, 2961, 2962, 2963, 2964, 2965, 2966, 2967, 2968, 2969, 2970, 2971, 2972, 2973, 2974, 2975, 2976, 2977, 2978, 2979, 2980, 2981, 2982, 2983, 2984, 2985, 2986, 2987, 2988, 2989, 2990, 2991, 2992, 2993, 2994, 2995, 2996, 2997, 2998, 2999, 3000, 3001, 3002, 3003, 3004, 3005, 3006, 3007, 3008, 3009, 3010, 3011, 3012, 3013, 3014, 3015, 3016, 3017, 3018, 3019, 3020, 3021, 3022, 3023, 3024, 3025, 3026, 3027, 3028, 3029, 3030, 3031, 3032, 3033, 3034, 3035, 3036, 3037, 3038, 3039, 3040, 3041, 3042, 3043, 3044, 3045, 3046, 3047, 3048, 3049, 3050, 3051, 3052, 3053, 3054, 3055, 3056, 3057, 3058, 3059, 3060, 3061, 3062, 3063, 3064, 3065, 3066, 3067, 3068, 3069, 3070, 3071, 3072, 3073, 3074, 3075, 3076, 3077, 3078, 3079, 3080, 3081, 3082, 3083, 3084, 3085, 3086, 3087, 3088, 3089, 3090, 3091, 3092, 3093, 3094, 3095, 3096, 3097, 3098, 3099, 3100, 3101, 3102, 3103, 3104, 3105, 3106, 3107, 3108, 3109, 3110, 3111, 3112, 3113, 3114, 3115, 3116, 3117, 3118, 3119, 3120, 3121, 3122, 3123, 3124, 3125, 3126, 3127, 3128, 3129, 3130, 3131, 3132, 3133, 3134, 3135, 3136, 3137, 3138, 3139, 3140, 3141, 3142, 3143, 3144, 3145, 3146, 3147, 3148, 3149, 3150, 3151, 3152, 3153, 3154, 3155, 3156, 3157, 3158, 3159, 3160, 3161, 3162, 3163, 3164, 3165, 3166, 3167, 3168, 3169, 3170, 3171, 3172, 3173, 3174, 3175, 3176, 3177, 3178, 3179, 3180, 3181, 3182, 3183, 3184, 3185, 3186, 3187, 3188, 3189, 3190, 3191, 3192, 3193, 3194, 3195, 3196, 3197, 3198, 3199, 3200, 3201, 3202, 3203, 3204, 3205, 3206, 3207, 3208, 3209, 3210, 3211, 3212, 3213, 3214, 3215, 3216, 3217, 3218, 3219, 3220, 3221, 3222, 3223, 3224, 3225, 3226, 3227, 3228, 3229, 3230, 3231, 3232, 3233, 3234, 3235, 3236, 3237, 3238, 3239, 3240, 3241, 3242, 3243, 3244, 3245, 3246, 3247, 3248, 3249, 3250, 3251, 3252, 3253, 3254, 3255, 3256, 3257, 3258, 3259, 3260, 3261, 3262, 3263, 3264, 3265, 3266, 3267, 3268, 3269, 3270, 3271, 3272, 3273, 3274, 3275, 3276, 3277, 3278, 3279, 3280, 3281, 3282, 3283, 3284, 3285, 3286, 3287, 3288, 3289, 3290, 3291, 3292, 3293, 3294, 3295, 3296, 3297, 3298, 3299, 3300, 3301, 3302, 3303, 3304, 3305, 3306, 3307, 3308, 3309, 3310, 3311, 3312, 3313, 3314, 3315, 3316, 3317, 3318, 3319, 3320, 3321, 3322, 3323, 3324, 3325, 3326, 3327, 3328, 3329, 3330, 3331, 3332, 3333, 3334, 3335, 3336, 3337, 3338, 3339, 3340, 3341, 3342, 3343, 3344, 3345, 3346, 3347, 3348, 3349, 3350, 3351, 3352, 3353, 3354, 3355, 3356, 3357, 3358, 3359, 3360, 3361, 3362, 3363, 3364, 3365, 3366, 3367, 3368, 3369, 3370, 3371, 3372, 3373, 3374, 3375, 3376, 3377, 3378, 3379, 3380, 3381, 3382, 3383, 3384, 3385, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396, 3397, 3398, 3399, 3400, 3401, 3402, 3403, 3404, 3405, 3406, 3407, 3408, 3409, 3410, 3411, 3412, 3413, 3414, 3415, 3416, 3417, 3418, 3419, 3420, 3421, 3422, 3423, 3424, 3425, 3426, 3427, 3428, 3429, 3430, 3431, 3432, 3433, 3434, 3435, 3436, 3437, 3438, 3439, 3440, 3441, 3442, 3443, 3444, 3445, 3446, 3447, 3448, 3449, 3450, 3451, 3452, 3453, 3454, 3455, 3456, 3457, 3458, 3459, 3460, 3461, 3462, 3463, 3464, 3465, 3466, 3467, 3468, 3469, 3470, 3471, 3472, 3473, 3474, 3475, 3476, 3477, 3478, 3479, 3480, 3481, 3482, 3483, 3484, 3485, 3486, 3487, 3488, 3489, 3490, 3491, 3492, 3493, 3494, 3495, 3496, 3497, 3498, 3499, 3500, 3501, 3502, 3503, 3504, 3505, 3506, 3507, 3508, 3509, 3510, 3511, 3512, 3513, 3514, 3515, 3516, 3517, 3518, 3519, 3520, 3521, 3522, 3523, 3524, 3525, 3526, 3527, 3528, 3529, 3530, 3531, 3532, 3533, 3534, 3535, 3536, 3537, 3538, 3539, 3540, 3541, 3542, 3543, 3544, 3545, 3546, 3547, 3548, 3549, 3550, 3551, 3552, 3553, 3554, 3555, 3556, 3557, 3558, 3559, 3560, 3561, 3562, 3563, 3564, 3565, 3566, 3567, 3568, 3569, 3570, 3571, 3572, 3573, 3574, 3575, 3576, 3577, 3578, 3579, 3580, 3581, 3582, 3583, 3584, 3585, 3586, 3587, 3588, 3589, 3590, 3591, 3592, 3593, 3594, 3595, 3596, 3597, 3598, 3599, 3600, 3601, 3602, 3603, 3604, 3605, 3606, 3607, 3608, 3609, 3610, 3611, 3612, 3613, 3614, 3615, 3616, 3617, 3618, 3619, 3620, 3621, 3622, 3623, 3624, 3625, 3626, 3627, 3628, 3629, 3630, 3631, 3632, 3633, 3634, 3635, 3636, 3637, 3638, 3639, 3640, 3641, 3642, 3643, 3644, 3645, 3646, 3647, 3648, 3649, 3650, 3651, 3652, 3653, 3654, 3655, 3656, 3657, 3658, 3659, 3660, 3661, 3662, 3663, 3664, 3665, 3666, 3667, 3668, 3669, 3670, 3671, 3672, 3673, 3674, 3675, 3676, 3677, 3678, 3679, 3680, 3681, 3682, 3683, 3684, 3685, 3686, 3687, 3688, 3689, 3690, 3691, 3692, 3693, 3694, 3695, 3696, 3697, 3698, 3699, 3700, 3701, 3702, 3703, 3704, 3705, 3706, 3707, 3708, 3709, 3710, 3711, 3712, 3713, 3714, 3715, 3716, 3717, 3718, 3719, 3720, 3721, 3722, 3723, 3724, 3725, 3726, 3727, 3728, 3729, 3730, 3731, 3732, 3733, 3734, 3735, 3736, 3737, 3738, 3739, 3740, 3741, 3742, 3743, 3744, 3745, 3746, 3747, 3748, 3749, 3750, 3751, 3752, 3753, 3754, 3755, 3756, 3757, 3758, 3759, 3760, 3761, 3762, 3763, 3764, 3765, 3766, 3767, 3768, 3769, 3770, 3771, 3772, 3773, 3774, 3775, 3776, 3777, 3778, 3779, 3780, 3781, 3782, 3783, 3784, 3785, 3786, 3787, 3788, 3789, 3790, 3791, 3792, 3793, 3794, 3795, 3796, 3797, 3798, 3799, 3800, 3801, 3802, 3803, 3804, 3805, 3806, 3807, 3808, 3809, 3810, 3811, 3812, 3813, 3814, 3815, 3816, 3817, 3818, 3819, 3820, 3821, 3822, 3823, 3824, 3825, 3826, 3827, 3828, 3829, 3830, 3831, 3832, 3833, 3834, 3835, 3836, 3837, 3838, 3839, 3840, 3841, 3842, 3843, 3844, 3845, 3846, 3847, 3848, 3849, 3850, 3851, 3852, 3853, 3854, 3855, 3856, 3857, 3858, 3859, 3860, 3861, 3862, 3863, 3864, 3865, 3866, 3867, 3868, 3869, 3870, 3871, 3872, 3873, 3874, 3875, 3876, 3877, 3878, 3879, 3880, 3881, 3882, 3883, 3884, 3885, 3886, 3887, 3888, 3889, 3890, 3891, 3892, 3893, 3894, 3895, 3896, 3897, 3898, 3899, 3900, 3901, 3902, 3903, 3904, 3905, 3906, 3907, 3908, 3909, 3910, 3911, 3912, 3913, 3914, 3915, 3916, 3917, 3918, 3919, 3920, 3921, 3922, 3923, 3924, 3925, 3926, 3927, 3928, 3929, 3930, 3931, 3932, 3933, 3934, 3935, 3936, 3937, 3938, 3939, 3940, 3941, 3942, 3943, 3944, 3945, 3946, 3947, 3948, 3949, 3950, 3951, 3952, 3953, 3954, 3955, 3956, 3957, 3958, 3959, 3960, 3961, 3962, 3963, 3964, 3965, 3966, 3967, 3968, 3969, 3970, 3971, 3972, 3973, 3974, 3975, 3976, 3977, 3978, 3979, 3980, 3981, 3982, 3983, 3984, 3985, 3986, 3987, 3988, 3989, 3990, 3991, 3992, 3993, 3994, 3995, 3996, 3997, 3998, 3999, 4000, 4001, 4002, 4003, 4004, 4005, 4006, 4007, 4008, 4009, 4010, 4011, 4012, 4013, 4014, 4015, 4016, 4017, 4018, 4019, 4020, 4021, 4022, 4023, 4024, 4025, 4026, 4027, 4028, 4029, 4030, 4031, 4032, 4033, 4034, 4035, 4036, 4037, 4038, 4039, 4040, 4041, 4042, 4043, 4044, 4045, 4046, 4047, 4048, 4049, 4050, 4051, 4052, 4053, 4054, 4055, 4056, 4057, 4058, 4059, 4060, 4061, 4062, 4063, 4064, 4065, 4066, 4067, 4068, 4069, 4070, 4071, 4072, 4073, 4074, 4075, 4076, 4077, 4078, 4079, 4080, 4081, 4082, 4083, 4084, 4085, 4086, 4087, 4088, 4089, 4090, 4091, 4092, 4093, 4094, 4095, 4096, 4097, 4098, 4099, 4100, 4101, 4102, 4103, 4104, 4105, 4106, 4107, 4108, 4109, 4110, 4111, 4112, 4113, 4114, 4115, 4116, 4117, 4118, 4119, 4120, 4121, 4122, 4123, 4124, 4125, 4126, 4127, 4128, 4129, 4130, 4131, 4132, 4133, 4134, 4135, 4136, 4137, 4138, 4139, 4140, 4141, 4142, 4143, 4144, 4145, 4146, 4147, 4148, 4149, 4150, 4151, 4152, 4153, 4154, 4155, 4156, 4157, 4158, 4159, 4160, 4161, 4162, 4163, 4164, 4165, 4166, 4167, 4168, 4169, 4170, 4171, 4172, 4173, 4174, 4175, 4176, 4177, 4178, 4179, 4180, 4181, 4182, 4183, 4184, 4185, 4186, 4187, 4188, 4189, 4190, 4191, 4192, 4193, 4194, 4195, 4196, 4197, 4198, 4199, 4200, 4201, 4202, 4203, 4204, 4205, 4206, 4207, 4208, 4209, 4210, 4211, 4212, 4213, 4214, 4215, 4216, 4217, 4218, 4219, 4220, 4221, 4222, 4223, 4224, 4225, 4226, 4227, 4228, 4229, 4230, 4231, 4232, 4233, 4234, 4235, 4236, 4237, 4238, 4239, 4240, 4241, 4242, 4243, 4244, 4245, 4246, 4247, 4248, 4249, 4250, 4251, 4252, 4253, 4254, 4255, 4256, 4257, 4258, 4259, 4260, 4261, 4262, 4263, 4264, 4265, 4266, 4267, 4268, 4269, 4270, 4271, 4272, 4273, 4274, 4275, 4276, 4277, 4278, 4279, 4280, 4281, 4282, 4283, 4284, 4285, 4286, 4287, 4288, 4289, 4290, 4291, 4292, 4293, 4294, 4295, 4296, 4297, 4298, 4299, 4300, 4301, 4302, 4303, 4304, 4305, 4306, 4307, 4308, 4309, 4310, 4311, 4312, 4313, 4314, 4315, 4316, 4317, 4318, 4319, 4320, 4321, 4322, 4323, 4324, 4325, 4326, 4327, 4328, 4329, 4330, 4331, 4332, 4333, 4334, 4335, 4336, 4337, 4338, 4339, 4340, 4341, 4342, 4343, 4344, 4345, 4346, 4347, 4348, 4349, 4350, 4351, 4352, 4353, 4354, 4355, 4356, 4357, 4358, 4359, 4360, 4361, 4362, 4363, 4364, 4365, 4366, 4367, 4368, 4369, 4370, 4371, 4372, 4373, 4374, 4375, 4376, 4377, 4378, 4379, 4380, 4381, 4382, 4383, 4384, 4385, 4386, 4387, 4388, 4389, 4390, 4391, 4392, 4393, 4394, 4395, 4396, 4397, 4398, 4399, 4400, 4401, 4402, 4403, 4404, 4405, 4406, 4407, 4408, 4409, 4410, 4411, 4412, 4413, 4414, 4415, 4416, 4417, 4418, 4419, 4420, 4421, 4422, 4423, 4424, 4425, 4426, 4427, 4428, 4429, 4430, 4431, 4432, 4433, 4434, 4435, 4436, 4437, 4438, 4439, 4440, 4441, 4442, 4443, 4444, 4445, 4446, 4447, 4448, 4449, 4450, 4451, 4452, 4453, 4454, 4455, 4456, 4457, 4458, 4459, 4460, 4461, 4462, 4463, 4464, 4465, 4466, 4467, 4468, 4469, 4470, 4471, 4472, 4473, 4474, 4475, 4476, 4477, 4478, 4479, 4480, 4481, 4482, 4483, 4484, 4485, 4486, 4487, 4488, 4489, 4490, 4491, 4492, 4493, 4494, 4495, 4496, 4497, 4498, 4499, 4500, 4501, 4502, 4503, 4504, 4505, 4506, 4507, 4508, 4509, 4510, 4511, 4512, 4513, 4514, 4515, 4516, 4517, 4518, 4519, 4520, 4521, 4522, 4523, 4524, 4525, 4526, 4527, 4528, 4529, 4530, 4531, 4532, 4533, 4534, 4535, 4536, 4537, 4538, 4539, 4540, 4541, 4542, 4543, 4544, 4545, 4546, 4547, 4548, 4549, 4550, 4551, 4552, 4553, 4554, 4555, 4556, 4557, 4558, 4559, 4560, 4561, 4562, 4563, 4564, 4565, 4566, 4567, 4568, 4569, 4570, 4571, 4572, 4573, 4574, 4575, 4576, 4577, 4578, 4579, 4580, 4581, 4582, 4583, 4584, 4585, 4586, 4587, 4588, 4589, 4590, 4591, 4592, 4593, 4594, 4595, 4596, 4597, 4598, 4599, 4600, 4601, 4602, 4603, 4604, 4605, 4606, 4607, 4608, 4609, 4610, 4611, 4612, 4613, 4614, 4615, 4616, 4617, 4618, 4619, 4620, 4621, 4622, 4623, 4624, 4625, 4626, 4627, 4628, 4629, 4630, 4631, 4632, 4633, 4634, 4635, 4636, 4637, 4638, 4639, 4640, 4641, 4642, 4643, 4644, 4645, 4646, 4647, 4648, 4649, 4650, 4651, 4652, 4653, 4654, 4655, 4656, 4657, 4658, 4659, 4660, 4661, 4662, 4663, 4664, 4665, 4666, 4667, 4668, 4669, 4670, 4671, 4672, 4673, 4674, 4675, 4676, 4677, 4678, 4679, 4680, 4681, 4682, 4683, 4684, 4685, 4686, 4687, 4688, 4689, 4690, 4691, 4692, 4693, 4694, 4695, 4696, 4697, 4698, 4699, 4700, 4701, 4702, 4703, 4704, 4705, 4706, 4707, 4708, 4709, 4710, 4711, 4712, 4713, 4714, 4715, 4716, 4717, 4718, 4719, 4720, 4721, 4722, 4723, 4724, 4725, 4726, 4727, 4728, 4729, 4730, 4731, 4732, 4733, 4734, 4735, 4736, 4737, 4738, 4739, 4740, 4741, 4742, 4743, 4744, 4745, 4746, 4747, 4748, 4749, 4750, 4751, 4752, 4753, 4754, 4755, 4756, 4757, 4758, 4759, 4760, 4761, 4762, 4763, 4764, 4765, 4766, 4767, 4768, 4769, 4770, 4771, 4772, 4773, 4774, 4775, 4776, 4777, 4778, 4779, 4780, 4781, 4782, 4783, 4784, 4785, 4786, 4787, 4788, 4789, 4790, 4791, 4792, 4793, 4794, 4795, 4796, 4797, 4798, 4799, 4800, 4801, 4802, 4803, 4804, 4805, 4806, 4807, 4808, 4809, 4810, 4811, 4812, 4813, 4814, 4815, 4816, 4817, 4818, 4819, 4820, 4821, 4822, 4823, 4824, 4825, 4826, 4827, 4828, 4829, 4830, 4831, 4832, 4833, 4834, 4835, 4836, 4837, 4838, 4839, 4840, 4841, 4842, 4843, 4844, 4845, 4846, 4847, 4848, 4849, 4850, 4851, 4852, 4853, 4854, 4855, 4856, 4857, 4858, 4859, 4860, 4861, 4862, 4863, 4864, 4865, 4866, 4867, 4868, 4869, 4870, 4871, 4872, 4873, 4874, 4875, 4876, 4877, 4878, 4879, 4880, 4881, 4882, 4883, 4884, 4885, 4886, 4887, 4888, 4889, 4890, 4891, 4892, 4893, 4894, 4895, 4896, 4897, 4898, 4899, 4900, 4901, 4902, 4903, 4904, 4905, 4906, 4907, 4908, 4909, 4910, 4911, 4912, 4913, 4914, 4915, 4916, 4917, 4918, 4919, 4920, 4921, 4922, 4923, 4924, 4925, 4926, 4927, 4928, 4929, 4930, 4931, 4932, 4933, 4934, 4935, 4936, 4937, 4938, 4939, 4940, 4941, 4942, 4943, 4944, 4945, 4946, 4947, 4948, 4949, 4950, 4951, 4952, 4953, 4954, 4955, 4956, 4957, 4958, 4959, 4960, 4961, 4962, 4963, 4964, 4965, 4966, 4967, 4968, 4969, 4970, 4971, 4972, 4973, 4974, 4975, 4976, 4977, 4978, 4979, 4980, 4981, 4982, 4983, 4984, 4985, 4986, 4987, 4988, 4989, 4990, 4991, 4992, 4993, 4994, 4995, 4996, 4997, 4998, 4999, 5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007, 5008, 5009, 5010, 5011, 5012, 5013, 5014, 5015, 5016, 5017, 5018, 5019, 5020, 5021, 5022, 5023, 5024, 5025, 5026, 5027, 5028, 5029, 5030, 5031, 5032, 5033, 5034, 5035, 5036, 5037, 5038, 5039, 5040, 5041, 5042, 5043, 5044, 5045, 5046, 5047, 5048, 5049, 5050, 5051, 5052, 5053, 5054, 5055, 5056, 5057, 5058, 5059, 5060, 5061, 5062, 5063, 5064, 5065, 5066, 5067, 5068, 5069, 5070, 5071, 5072, 5073, 5074, 5075, 5076, 5077, 5078, 5079, 5080, 5081, 5082, 5083, 5084, 5085, 5086, 5087, 5088, 5089, 5090, 5091, 5092, 5093, 5094, 5095, 5096, 5097, 5098, 5099, 5100, 5101, 5102, 5103, 5104, 5105, 5106, 5107, 5108, 5109, 5110, 5111, 5112, 5113, 5114, 5115, 5116, 5117, 5118, 5119, 5120, 5121, 5122, 5123, 5124, 5125, 5126, 5127, 5128, 5129, 5130, 5131, 5132, 5133, 5134, 5135, 5136, 5137, 5138, 5139, 5140, 5141, 5142, 5143, 5144, 5145, 5146, 5147, 5148, 5149, 5150, 5151, 5152, 5153, 5154, 5155, 5156, 5157, 5158, 5159, 5160, 5161, 5162, 5163, 5164, 5165, 5166, 5167, 5168, 5169, 5170, 5171, 5172, 5173, 5174, 5175, 5176, 5177, 5178, 5179, 5180, 5181, 5182, 5183, 5184, 5185, 5186, 5187, 5188, 5189, 5190, 5191, 5192, 5193, 5194, 5195, 5196, 5197, 5198, 5199, 5200, 5201, 5202, 5203, 5204, 5205, 5206, 5207, 5208, 5209, 5210, 5211, 5212, 5213, 5214, 5215, 5216, 5217, 5218, 5219, 5220, 5221, 5222, 5223, 5224, 5225, 5226, 5227, 5228, 5229, 5230, 5231, 5232, 5233, 5234, 5235, 5236, 5237, 5238, 5239, 5240, 5241, 5242, 5243, 5244, 5245, 5246, 5247, 5248, 5249, 5250, 5251, 5252, 5253, 5254, 5255, 5256, 5257, 5258, 5259, 5260, 5261, 5262, 5263, 5264, 5265, 5266, 5267, 5268, 5269, 5270, 5271, 5272, 5273, 5274, 5275, 5276, 5277, 5278, 5279, 5280, 5281, 5282, 5283, 5284, 5285, 5286, 5287, 5288, 5289, 5290, 5291, 5292, 5293, 5294, 5295, 5296, 5297, 5298, 5299, 5300, 5301, 5302, 5303, 5304, 5305, 5306, 5307, 5308, 5309, 5310, 5311, 5312, 5313, 5314, 5315, 5316, 5317, 5318, 5319, 5320, 5321, 5322, 5323, 5324, 5325, 5326, 5327, 5328, 5329, 5330, 5331, 5332, 5333, 5334, 5335, 5336, 5337, 5338, 5339, 5340, 5341, 5342, 5343, 5344, 5345, 5346, 5347, 5348, 5349, 5350, 5351, 5352, 5353, 5354, 5355, 5356, 5357, 5358, 5359, 5360, 5361, 5362, 5363, 5364, 5365, 5366, 5367, 5368, 5369, 5370, 5371, 5372, 5373, 5374, 5375, 5376, 5377, 5378, 5379, 5380, 5381, 5382, 5383, 5384, 5385, 5386, 5387, 5388, 5389, 5390, 5391, 5392, 5393, 5394, 5395, 5396, 5397, 5398, 5399, 5400, 5401, 5402, 5403, 5404, 5405, 5406, 5407, 5408, 5409, 5410, 5411, 5412, 5413, 5414, 5415, 5416, 5417, 5418, 5419, 5420, 5421, 5422, 5423, 5424, 5425, 5426, 5427, 5428, 5429, 5430, 5431, 5432, 5433, 5434, 5435, 5436, 5437, 5438, 5439, 5440, 5441, 5442, 5443, 5444, 5445, 5446, 5447, 5448, 5449, 5450, 5451, 5452, 5453, 5454, 5455, 5456, 5457, 5458, 5459, 5460, 5461, 5462, 5463, 5464, 5465, 5466, 5467, 5468, 5469, 5470, 5471, 5472, 5473, 5474, 5475, 5476, 5477, 5478, 5479, 5480, 5481, 5482, 5483, 5484, 5485, 5486, 5487, 5488, 5489, 5490, 5491, 5492, 5493, 5494, 5495, 5496, 5497, 5498, 5499, 5500, 5501, 5502, 5503, 5504, 5505, 5506, 5507, 5508, 5509, 5510, 5511, 5512, 5513, 5514, 5515, 5516, 5517, 5518, 5519, 5520, 5521, 5522, 5523, 5524, 5525, 5526, 5527, 5528, 5529, 5530, 5531, 5532, 5533, 5534, 5535, 5536, 5537, 5538, 5539, 5540, 5541, 5542, 5543, 5544, 5545, 5546, 5547, 5548, 5549, 5550, 5551, 5552, 5553, 5554, 5555, 5556, 5557, 5558, 5559, 5560, 5561, 5562, 5563, 5564, 5565, 5566, 5567, 5568, 5569, 5570, 5571, 5572, 5573, 5574, 5575, 5576, 5577, 5578, 5579, 5580, 5581, 5582, 5583, 5584, 5585, 5586, 5587, 5588, 5589, 5590, 5591, 5592, 5593, 5594, 5595, 5596, 5597, 5598, 5599, 5600, 5601, 5602, 5603, 5604, 5605, 5606, 5607, 5608, 5609, 5610, 5611, 5612, 5613, 5614, 5615, 5616, 5617, 5618, 5619, 5620, 5621, 5622, 5623, 5624, 5625, 5626, 5627, 5628, 5629, 5630, 5631, 5632, 5633, 5634, 5635, 5636, 5637, 5638, 5639, 5640, 5641, 5642, 5643, 5644, 5645, 5646, 5647, 5648, 5649, 5650, 5651, 5652, 5653, 5654, 5655, 5656, 5657, 5658, 5659, 5660, 5661, 5662, 5663, 5664, 5665, 5666, 5667, 5668, 5669, 5670, 5671, 5672, 5673, 5674, 5675, 5676, 5677, 5678, 5679, 5680, 5681, 5682, 5683, 5684, 5685, 5686, 5687, 5688, 5689, 5690, 5691, 5692, 5693, 5694, 5695, 5696, 5697, 5698, 5699, 5700, 5701, 5702, 5703, 5704, 5705, 5706, 5707, 5708, 5709, 5710, 5711, 5712, 5713, 5714, 5715, 5716, 5717, 5718, 5719, 5720, 5721, 5722, 5723, 5724, 5725, 5726, 5727, 5728, 5729, 5730, 5731, 5732, 5733, 5734, 5735, 5736, 5737, 5738, 5739, 5740, 5741, 5742, 5743, 5744, 5745, 5746, 5747, 5748, 5749, 5750, 5751, 5752, 5753, 5754, 5755, 5756, 5757, 5758, 5759, 5760, 5761, 5762, 5763, 5764, 5765, 5766, 5767, 5768, 5769, 5770, 5771, 5772, 5773, 5774, 5775, 5776, 5777, 5778, 5779, 5780, 5781, 5782, 5783, 5784, 5785, 5786, 5787, 5788, 5789, 5790, 5791, 5792, 5793, 5794, 5795, 5796, 5797, 5798, 5799, 5800, 5801, 5802, 5803, 5804, 5805, 5806, 5807, 5808, 5809, 5810, 5811, 5812, 5813, 5814, 5815, 5816, 5817, 5818, 5819, 5820, 5821, 5822, 5823, 5824, 5825, 5826, 5827, 5828, 5829, 5830, 5831, 5832, 5833, 5834, 5835, 5836, 5837, 5838, 5839, 5840, 5841, 5842, 5843, 5844, 5845, 5846, 5847, 5848, 5849, 5850, 5851, 5852, 5853, 5854, 5855, 5856, 5857, 5858, 5859, 5860, 5861, 5862, 5863, 5864, 5865, 5866, 5867, 5868, 5869, 5870, 5871, 5872, 5873, 5874, 5875, 5876, 5877, 5878, 5879, 5880, 5881, 5882, 5883, 5884, 5885, 5886, 5887, 5888, 5889, 5890, 5891, 5892, 5893, 5894, 5895, 5896, 5897, 5898, 5899, 5900, 5901, 5902, 5903, 5904, 5905, 5906, 5907, 5908, 5909, 5910, 5911, 5912, 5913, 5914, 5915, 5916, 5917, 5918, 5919, 5920, 5921, 5922, 5923, 5924, 5925, 5926, 5927, 5928, 5929, 5930, 5931, 5932, 5933, 5934, 5935, 5936, 5937, 5938, 5939, 5940, 5941, 5942, 5943, 5944, 5945, 5946, 5947, 5948, 5949, 5950, 5951, 5952, 5953, 5954, 5955, 5956, 5957, 5958, 5959, 5960, 5961, 5962, 5963, 5964, 5965, 5966, 5967, 5968, 5969, 5970, 5971, 5972, 5973, 5974, 5975, 5976, 5977, 5978, 5979, 5980, 5981, 5982, 5983, 5984, 5985, 5986, 5987, 5988, 5989, 5990, 5991, 5992, 5993, 5994, 5995, 5996, 5997, 5998, 5999, 6000, 6001, 6002, 6003, 6004, 6005, 6006, 6007, 6008, 6009, 6010, 6011, 6012, 6013, 6014, 6015, 6016, 6017, 6018, 6019, 6020, 6021, 6022, 6023, 6024, 6025, 6026, 6027, 6028, 6029, 6030, 6031, 6032, 6033, 6034, 6035, 6036, 6037, 6038, 6039, 6040, 6041, 6042, 6043, 6044, 6045, 6046, 6047, 6048, 6049, 6050, 6051, 6052, 6053, 6054, 6055, 6056, 6057, 6058, 6059, 6060, 6061, 6062, 6063, 6064, 6065, 6066, 6067, 6068, 6069, 6070, 6071, 6072, 6073, 6074, 6075, 6076, 6077, 6078, 6079, 6080, 6081, 6082, 6083, 6084, 6085, 6086, 6087, 6088, 6089, 6090, 6091, 6092, 6093, 6094, 6095, 6096, 6097, 6098, 6099, 6100, 6101, 6102, 6103, 6104, 6105, 6106, 6107, 6108, 6109, 6110, 6111, 6112, 6113, 6114, 6115, 6116, 6117, 6118, 6119, 6120, 6121, 6122, 6123, 6124, 6125, 6126, 6127, 6128, 6129, 6130, 6131, 6132, 6133, 6134, 6135, 6136, 6137, 6138, 6139, 6140, 6141, 6142, 6143, 6144, 6145, 6146, 6147, 6148, 6149, 6150, 6151, 6152, 6153, 6154, 6155, 6156, 6157, 6158, 6159, 6160, 6161, 6162, 6163, 6164, 6165, 6166, 6167, 6168, 6169, 6170, 6171, 6172, 6173, 6174, 6175, 6176, 6177, 6178, 6179, 6180, 6181, 6182, 6183, 6184, 6185, 6186, 6187, 6188, 6189, 6190, 6191, 6192, 6193, 6194, 6195, 6196, 6197, 6198, 6199, 6200, 6201, 6202, 6203, 6204, 6205, 6206, 6207, 6208, 6209, 6210, 6211, 6212, 6213, 6214, 6215, 6216, 6217, 6218, 6219, 6220, 6221, 6222, 6223, 6224, 6225, 6226, 6227, 6228, 6229, 6230, 6231, 6232, 6233, 6234, 6235, 6236, 6237, 6238, 6239, 6240, 6241, 6242, 6243, 6244, 6245, 6246, 6247, 6248, 6249, 6250, 6251, 6252, 6253, 6254, 6255, 6256, 6257, 6258, 6259, 6260, 6261, 6262, 6263, 6264, 6265, 6266, 6267, 6268, 6269, 6270, 6271, 6272, 6273, 6274, 6275, 6276, 6277, 6278, 6279, 6280, 6281, 6282, 6283, 6284, 6285, 6286, 6287, 6288, 6289, 6290, 6291, 6292, 6293, 6294, 6295, 6296, 6297, 6298, 6299, 6300, 6301, 6302, 6303, 6304, 6305, 6306, 6307, 6308, 6309, 6310, 6311, 6312, 6313, 6314, 6315, 6316, 6317, 6318, 6319, 6320, 6321, 6322, 6323, 6324, 6325, 6326, 6327, 6328, 6329, 6330, 6331, 6332, 6333, 6334, 6335, 6336, 6337, 6338, 6339, 6340, 6341, 6342, 6343, 6344, 6345, 6346, 6347, 6348, 6349, 6350, 6351, 6352, 6353, 6354, 6355, 6356, 6357, 6358, 6359, 6360, 6361, 6362, 6363, 6364, 6365, 6366, 6367, 6368, 6369, 6370, 6371, 6372, 6373, 6374, 6375, 6376, 6377, 6378, 6379, 6380, 6381, 6382, 6383, 6384, 6385, 6386, 6387, 6388, 6389, 6390, 6391, 6392, 6393, 6394, 6395, 6396, 6397, 6398, 6399, 6400, 6401, 6402, 6403, 6404, 6405, 6406, 6407, 6408, 6409, 6410, 6411, 6412, 6413, 6414, 6415, 6416, 6417, 6418, 6419, 6420, 6421, 6422, 6423, 6424, 6425, 6426, 6427, 6428, 6429, 6430, 6431, 6432, 6433, 6434, 6435, 6436, 6437, 6438, 6439, 6440, 6441, 6442, 6443, 6444, 6445, 6446, 6447, 6448, 6449, 6450, 6451, 6452, 6453, 6454, 6455, 6456, 6457, 6458, 6459, 6460, 6461, 6462, 6463, 6464, 6465, 6466, 6467, 6468, 6469, 6470, 6471, 6472, 6473, 6474, 6475, 6476, 6477, 6478, 6479, 6480, 6481, 6482, 6483, 6484, 6485, 6486, 6487, 6488, 6489, 6490, 6491, 6492, 6493, 6494, 6495, 6496, 6497, 6498, 6499, 6500, 6501, 6502, 6503, 6504, 6505, 6506, 6507, 6508, 6509, 6510, 6511, 6512, 6513, 6514, 6515, 6516, 6517, 6518, 6519, 6520, 6521, 6522, 6523, 6524, 6525, 6526, 6527, 6528, 6529, 6530, 6531, 6532, 6533, 6534, 6535, 6536, 6537, 6538, 6539, 6540, 6541, 6542, 6543, 6544, 6545, 6546, 6547, 6548, 6549, 6550, 6551, 6552, 6553, 6554, 6555, 6556, 6557, 6558, 6559, 6560, 6561, 6562, 6563, 6564, 6565, 6566, 6567, 6568, 6569, 6570, 6571, 6572, 6573, 6574, 6575, 6576, 6577, 6578, 6579, 6580, 6581, 6582, 6583, 6584, 6585, 6586, 6587, 6588, 6589, 6590, 6591, 6592, 6593, 6594, 6595, 6596, 6597, 6598, 6599, 6600, 6601, 6602, 6603, 6604, 6605, 6606, 6607, 6608, 6609, 6610, 6611, 6612, 6613, 6614, 6615, 6616, 6617, 6618, 6619, 6620, 6621, 6622, 6623, 6624, 6625, 6626, 6627, 6628, 6629, 6630, 6631, 6632, 6633, 6634, 6635, 6636, 6637, 6638, 6639, 6640, 6641, 6642, 6643, 6644, 6645, 6646, 6647, 6648, 6649, 6650, 6651, 6652, 6653, 6654, 6655, 6656, 6657, 6658, 6659, 6660, 6661, 6662, 6663, 6664, 6665, 6666, 6667, 6668, 6669, 6670, 6671, 6672, 6673, 6674, 6675, 6676, 6677, 6678, 6679, 6680, 6681, 6682, 6683, 6684, 6685, 6686, 6687, 6688, 6689, 6690, 6691, 6692, 6693, 6694, 6695, 6696, 6697, 6698, 6699, 6700, 6701, 6702, 6703, 6704, 6705, 6706, 6707, 6708, 6709, 6710, 6711, 6712, 6713, 6714, 6715, 6716, 6717, 6718, 6719, 6720, 6721, 6722, 6723, 6724, 6725, 6726, 6727, 6728, 6729, 6730, 6731, 6732, 6733, 6734, 6735, 6736, 6737, 6738, 6739, 6740, 6741, 6742, 6743, 6744, 6745, 6746, 6747, 6748, 6749, 6750, 6751, 6752, 6753, 6754, 6755, 6756, 6757, 6758, 6759, 6760, 6761, 6762, 6763, 6764, 6765, 6766, 6767, 6768, 6769, 6770, 6771, 6772, 6773, 6774, 6775, 6776, 6777, 6778, 6779, 6780, 6781, 6782, 6783, 6784, 6785, 6786, 6787, 6788, 6789, 6790, 6791, 6792, 6793, 6794, 6795, 6796, 6797, 6798, 6799, 6800, 6801, 6802, 6803, 6804, 6805, 6806, 6807, 6808, 6809, 6810, 6811, 6812, 6813, 6814, 6815, 6816, 6817, 6818, 6819, 6820, 6821, 6822, 6823, 6824, 6825, 6826, 6827, 6828, 6829, 6830, 6831, 6832, 6833, 6834, 6835, 6836, 6837, 6838, 6839, 6840, 6841, 6842, 6843, 6844, 6845, 6846, 6847, 6848, 6849, 6850, 6851, 6852, 6853, 6854, 6855, 6856, 6857, 6858, 6859, 6860, 6861, 6862, 6863, 6864, 6865, 6866, 6867, 6868, 6869, 6870, 6871, 6872, 6873, 6874, 6875, 6876, 6877, 6878, 6879, 6880, 6881, 6882, 6883, 6884, 6885, 6886, 6887, 6888, 6889, 6890, 6891, 6892, 6893, 6894, 6895, 6896, 6897, 6898, 6899, 6900, 6901, 6902, 6903, 6904, 6905, 6906, 6907, 6908, 6909, 6910, 6911, 6912, 6913, 6914, 6915, 6916, 6917, 6918, 6919, 6920, 6921, 6922, 6923, 6924, 6925, 6926, 6927, 6928, 6929, 6930, 6931, 6932, 6933, 6934, 6935, 6936, 6937, 6938, 6939, 6940, 6941, 6942, 6943, 6944, 6945, 6946, 6947, 6948, 6949, 6950, 6951, 6952, 6953, 6954, 6955, 6956, 6957, 6958, 6959, 6960, 6961, 6962, 6963, 6964, 6965, 6966, 6967, 6968, 6969, 6970, 6971, 6972, 6973, 6974, 6975, 6976, 6977, 6978, 6979, 6980, 6981, 6982, 6983, 6984, 6985, 6986, 6987, 6988, 6989, 6990, 6991, 6992, 6993, 6994, 6995, 6996, 6997, 6998, 6999, 7000, 7001, 7002, 7003, 7004, 7005, 7006, 7007, 7008, 7009, 7010, 7011, 7012, 7013, 7014, 7015, 7016, 7017, 7018, 7019, 7020, 7021, 7022, 7023, 7024, 7025, 7026, 7027, 7028, 7029, 7030, 7031, 7032, 7033, 7034, 7035, 7036, 7037, 7038, 7039, 7040, 7041, 7042, 7043, 7044, 7045, 7046, 7047, 7048, 7049, 7050, 7051, 7052, 7053, 7054, 7055, 7056, 7057, 7058, 7059, 7060, 7061, 7062, 7063, 7064, 7065, 7066, 7067, 7068, 7069, 7070, 7071, 7072, 7073, 7074, 7075, 7076, 7077, 7078, 7079, 7080, 7081, 7082, 7083, 7084, 7085, 7086, 7087, 7088, 7089, 7090, 7091, 7092, 7093, 7094, 7095, 7096, 7097, 7098, 7099, 7100, 7101, 7102, 7103, 7104, 7105, 7106, 7107, 7108, 7109, 7110, 7111, 7112, 7113, 7114, 7115, 7116, 7117, 7118, 7119, 7120, 7121, 7122, 7123, 7124, 7125, 7126, 7127, 7128, 7129, 7130, 7131, 7132, 7133, 7134, 7135, 7136, 7137, 7138, 7139, 7140, 7141, 7142, 7143, 7144, 7145, 7146, 7147, 7148, 7149, 7150, 7151, 7152, 7153, 7154, 7155, 7156, 7157, 7158, 7159, 7160, 7161, 7162, 7163, 7164, 7165, 7166, 7167, 7168, 7169, 7170, 7171, 7172, 7173, 7174, 7175, 7176, 7177, 7178, 7179, 7180, 7181, 7182, 7183, 7184, 7185, 7186, 7187, 7188, 7189, 7190, 7191, 7192, 7193, 7194, 7195, 7196, 7197, 7198, 7199, 7200, 7201, 7202, 7203, 7204, 7205, 7206, 7207, 7208, 7209, 7210, 7211, 7212, 7213, 7214, 7215, 7216, 7217, 7218, 7219, 7220, 7221, 7222, 7223, 7224, 7225, 7226, 7227, 7228, 7229, 7230, 7231, 7232, 7233, 7234, 7235, 7236, 7237, 7238, 7239, 7240, 7241, 7242, 7243, 7244, 7245, 7246, 7247, 7248, 7249, 7250, 7251, 7252, 7253, 7254, 7255, 7256, 7257, 7258, 7259, 7260, 7261, 7262, 7263, 7264, 7265, 7266, 7267, 7268, 7269, 7270, 7271, 7272, 7273, 7274, 7275, 7276, 7277, 7278, 7279, 7280, 7281, 7282, 7283, 7284, 7285, 7286, 7287, 7288, 7289, 7290, 7291, 7292, 7293, 7294, 7295, 7296, 7297, 7298, 7299, 7300, 7301, 7302, 7303, 7304, 7305, 7306, 7307, 7308, 7309, 7310, 7311, 7312, 7313, 7314, 7315, 7316, 7317, 7318, 7319, 7320, 7321, 7322, 7323, 7324, 7325, 7326, 7327, 7328, 7329, 7330, 7331, 7332, 7333, 7334, 7335, 7336, 7337, 7338, 7339, 7340, 7341, 7342, 7343, 7344, 7345, 7346, 7347, 7348, 7349, 7350, 7351, 7352, 7353, 7354, 7355, 7356, 7357, 7358, 7359, 7360, 7361, 7362, 7363, 7364, 7365, 7366, 7367, 7368, 7369, 7370, 7371, 7372, 7373, 7374, 7375, 7376, 7377, 7378, 7379, 7380, 7381, 7382, 7383, 7384, 7385, 7386, 7387, 7388, 7389, 7390, 7391, 7392, 7393, 7394, 7395, 7396, 7397, 7398, 7399, 7400, 7401, 7402, 7403, 7404, 7405, 7406, 7407, 7408, 7409, 7410, 7411, 7412, 7413, 7414, 7415, 7416, 7417, 7418, 7419, 7420, 7421, 7422, 7423, 7424, 7425, 7426, 7427, 7428, 7429, 7430, 7431, 7432, 7433, 7434, 7435, 7436, 7437, 7438, 7439, 7440, 7441, 7442, 7443, 7444, 7445, 7446, 7447, 7448, 7449, 7450, 7451, 7452, 7453, 7454, 7455, 7456, 7457, 7458, 7459, 7460, 7461, 7462, 7463, 7464, 7465, 7466, 7467, 7468, 7469, 7470, 7471, 7472, 7473, 7474, 7475, 7476, 7477, 7478, 7479, 7480, 7481, 7482, 7483, 7484, 7485, 7486, 7487, 7488, 7489, 7490, 7491, 7492, 7493, 7494, 7495, 7496, 7497, 7498, 7499, 7500, 7501, 7502, 7503, 7504, 7505, 7506, 7507, 7508, 7509, 7510, 7511, 7512, 7513, 7514, 7515, 7516, 7517, 7518, 7519, 7520, 7521, 7522, 7523, 7524, 7525, 7526, 7527, 7528, 7529, 7530, 7531, 7532, 7533, 7534, 7535, 7536, 7537, 7538, 7539, 7540, 7541, 7542, 7543, 7544, 7545, 7546, 7547, 7548, 7549, 7550, 7551, 7552, 7553, 7554, 7555, 7556, 7557, 7558, 7559, 7560, 7561, 7562, 7563, 7564, 7565, 7566, 7567, 7568, 7569, 7570, 7571, 7572, 7573, 7574, 7575, 7576, 7577, 7578, 7579, 7580, 7581, 7582, 7583, 7584, 7585, 7586, 7587, 7588, 7589, 7590, 7591, 7592, 7593, 7594, 7595, 7596, 7597, 7598, 7599, 7600, 7601, 7602, 7603, 7604, 7605, 7606, 7607, 7608, 7609, 7610, 7611, 7612, 7613, 7614, 7615, 7616, 7617, 7618, 7619, 7620, 7621, 7622, 7623, 7624, 7625, 7626, 7627, 7628, 7629, 7630, 7631, 7632, 7633, 7634, 7635, 7636, 7637, 7638, 7639, 7640, 7641, 7642, 7643, 7644, 7645, 7646, 7647, 7648, 7649, 7650, 7651, 7652, 7653, 7654, 7655, 7656, 7657, 7658, 7659, 7660, 7661, 7662, 7663, 7664, 7665, 7666, 7667, 7668, 7669, 7670, 7671, 7672, 7673, 7674, 7675, 7676, 7677, 7678, 7679, 7680, 7681, 7682, 7683, 7684, 7685, 7686, 7687, 7688, 7689, 7690, 7691, 7692, 7693, 7694, 7695, 7696, 7697, 7698, 7699, 7700, 7701, 7702, 7703, 7704, 7705, 7706, 7707, 7708, 7709, 7710, 7711, 7712, 7713, 7714, 7715, 7716, 7717, 7718, 7719, 7720, 7721, 7722, 7723, 7724, 7725, 7726, 7727, 7728, 7729, 7730, 7731, 7732, 7733, 7734, 7735, 7736, 7737, 7738, 7739, 7740, 7741, 7742, 7743, 7744, 7745, 7746, 7747, 7748, 7749, 7750, 7751, 7752, 7753, 7754, 7755, 7756, 7757, 7758, 7759, 7760, 7761, 7762, 7763, 7764, 7765, 7766, 7767, 7768, 7769, 7770, 7771, 7772, 7773, 7774, 7775, 7776, 7777, 7778, 7779, 7780, 7781, 7782, 7783, 7784, 7785, 7786, 7787, 7788, 7789, 7790, 7791, 7792, 7793, 7794, 7795, 7796, 7797, 7798, 7799, 7800, 7801, 7802, 7803, 7804, 7805, 7806, 7807, 7808, 7809, 7810, 7811, 7812, 7813, 7814, 7815, 7816, 7817, 7818, 7819, 7820, 7821, 7822, 7823, 7824, 7825, 7826, 7827, 7828, 7829, 7830, 7831, 7832, 7833, 7834, 7835, 7836, 7837, 7838, 7839, 7840, 7841, 7842, 7843, 7844, 7845, 7846, 7847, 7848, 7849, 7850, 7851, 7852, 7853, 7854, 7855, 7856, 7857, 7858, 7859, 7860, 7861, 7862, 7863, 7864, 7865, 7866, 7867, 7868, 7869, 7870, 7871, 7872, 7873, 7874, 7875, 7876, 7877, 7878, 7879, 7880, 7881, 7882, 7883, 7884, 7885, 7886, 7887, 7888, 7889, 7890, 7891, 7892, 7893, 7894, 7895, 7896, 7897, 7898, 7899, 7900, 7901, 7902, 7903, 7904, 7905, 7906, 7907, 7908, 7909, 7910, 7911, 7912, 7913, 7914, 7915, 7916, 7917, 7918, 7919, 7920, 7921, 7922, 7923, 7924, 7925, 7926, 7927, 7928, 7929, 7930, 7931, 7932, 7933, 7934, 7935, 7936, 7937, 7938, 7939, 7940, 7941, 7942, 7943, 7944, 7945, 7946, 7947, 7948, 7949, 7950, 7951, 7952, 7953, 7954, 7955, 7956, 7957, 7958, 7959, 7960, 7961, 7962, 7963, 7964, 7965, 7966, 7967, 7968, 7969, 7970, 7971, 7972, 7973, 7974, 7975, 7976, 7977, 7978, 7979, 7980, 7981, 7982, 7983, 7984, 7985, 7986, 7987, 7988, 7989, 7990, 7991, 7992, 7993, 7994, 7995, 7996, 7997, 7998, 7999, 8000, 8001, 8002, 8003, 8004, 8005, 8006, 8007, 8008, 8009, 8010, 8011, 8012, 8013, 8014, 8015, 8016, 8017, 8018, 8019, 8020, 8021, 8022, 8023, 8024, 8025, 8026, 8027, 8028, 8029, 8030, 8031, 8032, 8033, 8034, 8035, 8036, 8037, 8038, 8039, 8040, 8041, 8042, 8043, 8044, 8045, 8046, 8047, 8048, 8049, 8050, 8051, 8052, 8053, 8054, 8055, 8056, 8057, 8058, 8059, 8060, 8061, 8062, 8063, 8064, 8065, 8066, 8067, 8068, 8069, 8070, 8071, 8072, 8073, 8074, 8075, 8076, 8077, 8078, 8079, 8080, 8081, 8082, 8083, 8084, 8085, 8086, 8087, 8088, 8089, 8090, 8091, 8092, 8093, 8094, 8095, 8096, 8097, 8098, 8099, 8100, 8101, 8102, 8103, 8104, 8105, 8106, 8107, 8108, 8109, 8110, 8111, 8112, 8113, 8114, 8115, 8116, 8117, 8118, 8119, 8120, 8121, 8122, 8123, 8124, 8125, 8126, 8127, 8128, 8129, 8130, 8131, 8132, 8133, 8134, 8135, 8136, 8137, 8138, 8139, 8140, 8141, 8142, 8143, 8144, 8145, 8146, 8147, 8148, 8149, 8150, 8151, 8152, 8153, 8154, 8155, 8156, 8157, 8158, 8159, 8160, 8161, 8162, 8163, 8164, 8165, 8166, 8167, 8168, 8169, 8170, 8171, 8172, 8173, 8174, 8175, 8176, 8177, 8178, 8179, 8180, 8181, 8182, 8183, 8184, 8185, 8186, 8187, 8188, 8189, 8190, 8191, 8192};
						static float s_var[8192] = { 1, 1.18921, 1.31607, 1.41421, 1.49535, 1.56508, 1.62658, 1.68179, 1.73205, 1.77828, 1.82116, 1.86121, 1.89883, 1.93434, 1.96799, 2, 2.03054, 2.05977, 2.0878, 2.11474, 2.1407, 2.16574, 2.18994, 2.21336, 2.23607, 2.2581, 2.27951, 2.30033, 2.3206, 2.34035, 2.35961, 2.37841, 2.39678, 2.41474, 2.4323, 2.44949, 2.46633, 2.48282, 2.499, 2.51487, 2.53044, 2.54573, 2.56075, 2.57551, 2.59002, 2.60429, 2.61833, 2.63215, 2.64575, 2.65915, 2.67235, 2.68535, 2.69817, 2.71081, 2.72327, 2.73556, 2.7477, 2.75967, 2.77149, 2.78316, 2.79468, 2.80607, 2.81731, 2.82843, 2.83941, 2.85027, 2.86101, 2.87162, 2.88212, 2.89251, 2.90278, 2.91295, 2.92301, 2.93297, 2.94283, 2.95259, 2.96226, 2.97183, 2.98131, 2.9907, 3, 3.00922, 3.01835, 3.0274, 3.03637, 3.04526, 3.05408, 3.06281, 3.07148, 3.08007, 3.08859, 3.09704, 3.10542, 3.11374, 3.12199, 3.13017, 3.13829, 3.14635, 3.15434, 3.16228, 3.17015, 3.17797, 3.18573, 3.19344, 3.20109, 3.20868, 3.21622, 3.22371, 3.23115, 3.23853, 3.24587, 3.25315, 3.26039, 3.26758, 3.27472, 3.28182, 3.28887, 3.29587, 3.30283, 3.30975, 3.31662, 3.32346, 3.33025, 3.33699, 3.3437, 3.35037, 3.357, 3.36359, 3.37014, 3.37665, 3.38312, 3.38956, 3.39596, 3.40233, 3.40866, 3.41495, 3.42121, 3.42744, 3.43363, 3.43979, 3.44592, 3.45201, 3.45807, 3.4641, 3.4701, 3.47607, 3.482, 3.48791, 3.49379, 3.49964, 3.50545, 3.51124, 3.517, 3.52274, 3.52844, 3.53412, 3.53977, 3.54539, 3.55099, 3.55656, 3.5621, 3.56762, 3.57311, 3.57858, 3.58402, 3.58944, 3.59484, 3.60021, 3.60555, 3.61087, 3.61617, 3.62145, 3.6267, 3.63193, 3.63714, 3.64232, 3.64748, 3.65262, 3.65774, 3.66284, 3.66792, 3.67297, 3.67801, 3.68302, 3.68802, 3.69299, 3.69794, 3.70288, 3.70779, 3.71269, 3.71756, 3.72242, 3.72726, 3.73208, 3.73688, 3.74166, 3.74642, 3.75117, 3.75589, 3.7606, 3.7653, 3.76997, 3.77463, 3.77927, 3.78389, 3.7885, 3.79309, 3.79766, 3.80221, 3.80675, 3.81128, 3.81579, 3.82028, 3.82475, 3.82921, 3.83366, 3.83809, 3.8425, 3.8469, 3.85129, 3.85565, 3.86001, 3.86435, 3.86867, 3.87298, 3.87728, 3.88156, 3.88583, 3.89008, 3.89432, 3.89855, 3.90276, 3.90696, 3.91115, 3.91532, 3.91948, 3.92362, 3.92775, 3.93187, 3.93598, 3.94007, 3.94415, 3.94822, 3.95228, 3.95632, 3.96035, 3.96437, 3.96838, 3.97237, 3.97635, 3.98032, 3.98428, 3.98823, 3.99216, 3.99609, 4, 4.0039, 4.00779, 4.01167, 4.01553, 4.01939, 4.02323, 4.02707, 4.03089, 4.0347, 4.0385, 4.04229, 4.04607, 4.04984, 4.0536, 4.05735, 4.06109, 4.06481, 4.06853, 4.07224, 4.07594, 4.07962, 4.0833, 4.08697, 4.09062, 4.09427, 4.09791, 4.10154, 4.10516, 4.10876, 4.11236, 4.11595, 4.11953, 4.12311, 4.12667, 4.13022, 4.13376, 4.1373, 4.14082, 4.14434, 4.14785, 4.15135, 4.15484, 4.15832, 4.16179, 4.16526, 4.16871, 4.17216, 4.1756, 4.17902, 4.18245, 4.18586, 4.18926, 4.19266, 4.19605, 4.19943, 4.2028, 4.20616, 4.20952, 4.21287, 4.21621, 4.21954, 4.22286, 4.22618, 4.22949, 4.23279, 4.23608, 4.23936, 4.24264, 4.24591, 4.24917, 4.25243, 4.25568, 4.25892, 4.26215, 4.26537, 4.26859, 4.2718, 4.275, 4.2782, 4.28139, 4.28457, 4.28775, 4.29092, 4.29408, 4.29723, 4.30038, 4.30352, 4.30665, 4.30978, 4.3129, 4.31601, 4.31912, 4.32221, 4.32531, 4.32839, 4.33147, 4.33455, 4.33761, 4.34067, 4.34373, 4.34677, 4.34981, 4.35285, 4.35588, 4.3589, 4.36191, 4.36492, 4.36793, 4.37092, 4.37391, 4.3769, 4.37988, 4.38285, 4.38582, 4.38878, 4.39173, 4.39468, 4.39762, 4.40056, 4.40349, 4.40641, 4.40933, 4.41225, 4.41515, 4.41806, 4.42095, 4.42384, 4.42673, 4.42961, 4.43248, 4.43535, 4.43821, 4.44107, 4.44392, 4.44677, 4.44961, 4.45244, 4.45527, 4.45809, 4.46091, 4.46373, 4.46654, 4.46934, 4.47214, 4.47493, 4.47772, 4.4805, 4.48327, 4.48605, 4.48881, 4.49157, 4.49433, 4.49708, 4.49983, 4.50257, 4.50531, 4.50804, 4.51076, 4.51349, 4.5162, 4.51891, 4.52162, 4.52432, 4.52702, 4.52971, 4.5324, 4.53508, 4.53776, 4.54043, 4.5431, 4.54576, 4.54842, 4.55108, 4.55373, 4.55637, 4.55901, 4.56165, 4.56428, 4.56691, 4.56953, 4.57215, 4.57476, 4.57737, 4.57998, 4.58258, 4.58517, 4.58776, 4.59035, 4.59293, 4.59551, 4.59808, 4.60065, 4.60322, 4.60578, 4.60834, 4.61089, 4.61344, 4.61598, 4.61852, 4.62106, 4.62359, 4.62611, 4.62864, 4.63116, 4.63367, 4.63618, 4.63869, 4.64119, 4.64369, 4.64618, 4.64868, 4.65116, 4.65364, 4.65612, 4.6586, 4.66107, 4.66354, 4.666, 4.66846, 4.67091, 4.67336, 4.67581, 4.67825, 4.68069, 4.68313, 4.68556, 4.68799, 4.69042, 4.69284, 4.69525, 4.69767, 4.70008, 4.70248, 4.70489, 4.70728, 4.70968, 4.71207, 4.71446, 4.71684, 4.71922, 4.7216, 4.72397, 4.72634, 4.72871, 4.73107, 4.73343, 4.73579, 4.73814, 4.74049, 4.74283, 4.74517, 4.74751, 4.74985, 4.75218, 4.7545, 4.75683, 4.75915, 4.76147, 4.76378, 4.76609, 4.7684, 4.7707, 4.773, 4.7753, 4.7776, 4.77989, 4.78217, 4.78446, 4.78674, 4.78902, 4.79129, 4.79356, 4.79583, 4.7981, 4.80036, 4.80262, 4.80487, 4.80712, 4.80937, 4.81162, 4.81386, 4.8161, 4.81834, 4.82057, 4.8228, 4.82503, 4.82725, 4.82947, 4.83169, 4.83391, 4.83612, 4.83833, 4.84053, 4.84273, 4.84493, 4.84713, 4.84932, 4.85152, 4.8537, 4.85589, 4.85807, 4.86025, 4.86243, 4.8646, 4.86677, 4.86894, 4.8711, 4.87326, 4.87542, 4.87758, 4.87973, 4.88188, 4.88403, 4.88617, 4.88831, 4.89045, 4.89259, 4.89472, 4.89685, 4.89898, 4.9011, 4.90323, 4.90535, 4.90746, 4.90958, 4.91169, 4.9138, 4.9159, 4.91801, 4.92011, 4.9222, 4.9243, 4.92639, 4.92848, 4.93057, 4.93265, 4.93473, 4.93681, 4.93889, 4.94096, 4.94303, 4.9451, 4.94717, 4.94923, 4.95129, 4.95335, 4.95541, 4.95746, 4.95951, 4.96156, 4.9636, 4.96565, 4.96769, 4.96973, 4.97176, 4.97379, 4.97583, 4.97785, 4.97988, 4.9819, 4.98392, 4.98594, 4.98796, 4.98997, 4.99198, 4.99399, 4.996, 4.998, 5, 5.002, 5.004, 5.00599, 5.00798, 5.00997, 5.01196, 5.01394, 5.01592, 5.0179, 5.01988, 5.02186, 5.02383, 5.0258, 5.02777, 5.02973, 5.0317, 5.03366, 5.03562, 5.03757, 5.03953, 5.04148, 5.04343, 5.04538, 5.04732, 5.04927, 5.05121, 5.05315, 5.05508, 5.05702, 5.05895, 5.06088, 5.06281, 5.06473, 5.06666, 5.06858, 5.0705, 5.07241, 5.07433, 5.07624, 5.07815, 5.08006, 5.08196, 5.08387, 5.08577, 5.08767, 5.08956, 5.09146, 5.09335, 5.09524, 5.09713, 5.09902, 5.1009, 5.10279, 5.10467, 5.10655, 5.10842, 5.1103, 5.11217, 5.11404, 5.11591, 5.11777, 5.11964, 5.1215, 5.12336, 5.12522, 5.12707, 5.12893, 5.13078, 5.13263, 5.13448, 5.13632, 5.13817, 5.14001, 5.14185, 5.14369, 5.14552, 5.14736, 5.14919, 5.15102, 5.15285, 5.15467, 5.1565, 5.15832, 5.16014, 5.16196, 5.16378, 5.16559, 5.1674, 5.16921, 5.17102, 5.17283, 5.17464, 5.17644, 5.17824, 5.18004, 5.18184, 5.18363, 5.18543, 5.18722, 5.18901, 5.1908, 5.19258, 5.19437, 5.19615, 5.19793, 5.19971, 5.20149, 5.20327, 5.20504, 5.20681, 5.20858, 5.21035, 5.21212, 5.21388, 5.21564, 5.21741, 5.21916, 5.22092, 5.22268, 5.22443, 5.22618, 5.22793, 5.22968, 5.23143, 5.23318, 5.23492, 5.23666, 5.2384, 5.24014, 5.24188, 5.24361, 5.24534, 5.24708, 5.24881, 5.25053, 5.25226, 5.25398, 5.25571, 5.25743, 5.25915, 5.26087, 5.26258, 5.2643, 5.26601, 5.26772, 5.26943, 5.27114, 5.27284, 5.27455, 5.27625, 5.27795, 5.27965, 5.28135, 5.28305, 5.28474, 5.28643, 5.28812, 5.28981, 5.2915, 5.29319, 5.29487, 5.29656, 5.29824, 5.29992, 5.3016, 5.30327, 5.30495, 5.30662, 5.3083, 5.30997, 5.31164, 5.3133, 5.31497, 5.31663, 5.3183, 5.31996, 5.32162, 5.32327, 5.32493, 5.32659, 5.32824, 5.32989, 5.33154, 5.33319, 5.33484, 5.33648, 5.33813, 5.33977, 5.34141, 5.34305, 5.34469, 5.34633, 5.34796, 5.3496, 5.35123, 5.35286, 5.35449, 5.35612, 5.35774, 5.35937, 5.36099, 5.36261, 5.36423, 5.36585, 5.36747, 5.36908, 5.3707, 5.37231, 5.37392, 5.37553, 5.37714, 5.37875, 5.38036, 5.38196, 5.38356, 5.38516, 5.38676, 5.38836, 5.38996, 5.39156, 5.39315, 5.39474, 5.39634, 5.39793, 5.39951, 5.4011, 5.40269, 5.40427, 5.40586, 5.40744, 5.40902, 5.4106, 5.41217, 5.41375, 5.41533, 5.4169, 5.41847, 5.42004, 5.42161, 5.42318, 5.42475, 5.42631, 5.42788, 5.42944, 5.431, 5.43256, 5.43412, 5.43568, 5.43723, 5.43879, 5.44034, 5.44189, 5.44344, 5.44499, 5.44654, 5.44809, 5.44963, 5.45118, 5.45272, 5.45426, 5.4558, 5.45734, 5.45888, 5.46041, 5.46195, 5.46348, 5.46501, 5.46654, 5.46807, 5.4696, 5.47113, 5.47266, 5.47418, 5.4757, 5.47723, 5.47875, 5.48027, 5.48178, 5.4833, 5.48482, 5.48633, 5.48784, 5.48936, 5.49087, 5.49238, 5.49389, 5.49539, 5.4969, 5.4984, 5.49991, 5.50141, 5.50291, 5.50441, 5.50591, 5.5074, 5.5089, 5.51039, 5.51189, 5.51338, 5.51487, 5.51636, 5.51785, 5.51934, 5.52082, 5.52231, 5.52379, 5.52528, 5.52676, 5.52824, 5.52972, 5.53119, 5.53267, 5.53415, 5.53562, 5.53709, 5.53857, 5.54004, 5.54151, 5.54298, 5.54444, 5.54591, 5.54737, 5.54884, 5.5503, 5.55176, 5.55322, 5.55468, 5.55614, 5.5576, 5.55905, 5.56051, 5.56196, 5.56341, 5.56487, 5.56632, 5.56776, 5.56921, 5.57066, 5.5721, 5.57355, 5.57499, 5.57643, 5.57788, 5.57932, 5.58075, 5.58219, 5.58363, 5.58506, 5.5865, 5.58793, 5.58936, 5.5908, 5.59223, 5.59365, 5.59508, 5.59651, 5.59794, 5.59936, 5.60078, 5.60221, 5.60363, 5.60505, 5.60647, 5.60788, 5.6093, 5.61072, 5.61213, 5.61355, 5.61496, 5.61637, 5.61778, 5.61919, 5.6206, 5.62201, 5.62341, 5.62482, 5.62622, 5.62763, 5.62903, 5.63043, 5.63183, 5.63323, 5.63463, 5.63602, 5.63742, 5.63881, 5.64021, 5.6416, 5.64299, 5.64438, 5.64577, 5.64716, 5.64855, 5.64994, 5.65132, 5.65271, 5.65409, 5.65547, 5.65685, 5.65823, 5.65961, 5.66099, 5.66237, 5.66375, 5.66512, 5.6665, 5.66787, 5.66924, 5.67061, 5.67199, 5.67335, 5.67472, 5.67609, 5.67746, 5.67882, 5.68019, 5.68155, 5.68291, 5.68428, 5.68564, 5.687, 5.68835, 5.68971, 5.69107, 5.69243, 5.69378, 5.69513, 5.69649, 5.69784, 5.69919, 5.70054, 5.70189, 5.70324, 5.70458, 5.70593, 5.70728, 5.70862, 5.70996, 5.71131, 5.71265, 5.71399, 5.71533, 5.71667, 5.718, 5.71934, 5.72068, 5.72201, 5.72335, 5.72468, 5.72601, 5.72734, 5.72867, 5.73, 5.73133, 5.73266, 5.73398, 5.73531, 5.73663, 5.73796, 5.73928, 5.7406, 5.74192, 5.74324, 5.74456, 5.74588, 5.7472, 5.74851, 5.74983, 5.75115, 5.75246, 5.75377, 5.75508, 5.75639, 5.75771, 5.75901, 5.76032, 5.76163, 5.76294, 5.76424, 5.76555, 5.76685, 5.76815, 5.76946, 5.77076, 5.77206, 5.77336, 5.77466, 5.77595, 5.77725, 5.77855, 5.77984, 5.78114, 5.78243, 5.78372, 5.78502, 5.78631, 5.7876, 5.78889, 5.79017, 5.79146, 5.79275, 5.79403, 5.79532, 5.7966, 5.79789, 5.79917, 5.80045, 5.80173, 5.80301, 5.80429, 5.80557, 5.80684, 5.80812, 5.8094, 5.81067, 5.81194, 5.81322, 5.81449, 5.81576, 5.81703, 5.8183, 5.81957, 5.82084, 5.8221, 5.82337, 5.82464, 5.8259, 5.82717, 5.82843, 5.82969, 5.83095, 5.83221, 5.83347, 5.83473, 5.83599, 5.83725, 5.8385, 5.83976, 5.84101, 5.84227, 5.84352, 5.84477, 5.84603, 5.84728, 5.84853, 5.84978, 5.85102, 5.85227, 5.85352, 5.85476, 5.85601, 5.85725, 5.8585, 5.85974, 5.86098, 5.86222, 5.86347, 5.86471, 5.86594, 5.86718, 5.86842, 5.86966, 5.87089, 5.87213, 5.87336, 5.8746, 5.87583, 5.87706, 5.87829, 5.87952, 5.88075, 5.88198, 5.88321, 5.88444, 5.88566, 5.88689, 5.88811, 5.88934, 5.89056, 5.89178, 5.89301, 5.89423, 5.89545, 5.89667, 5.89789, 5.8991, 5.90032, 5.90154, 5.90275, 5.90397, 5.90518, 5.9064, 5.90761, 5.90882, 5.91003, 5.91124, 5.91245, 5.91366, 5.91487, 5.91608, 5.91729, 5.91849, 5.9197, 5.9209, 5.92211, 5.92331, 5.92451, 5.92572, 5.92692, 5.92812, 5.92932, 5.93052, 5.93171, 5.93291, 5.93411, 5.9353, 5.9365, 5.93769, 5.93889, 5.94008, 5.94127, 5.94246, 5.94366, 5.94485, 5.94604, 5.94722, 5.94841, 5.9496, 5.95079, 5.95197, 5.95316, 5.95434, 5.95553, 5.95671, 5.95789, 5.95907, 5.96025, 5.96144, 5.96262, 5.96379, 5.96497, 5.96615, 5.96733, 5.9685, 5.96968, 5.97085, 5.97203, 5.9732, 5.97437, 5.97555, 5.97672, 5.97789, 5.97906, 5.98023, 5.9814, 5.98256, 5.98373, 5.9849, 5.98606, 5.98723, 5.98839, 5.98956, 5.99072, 5.99188, 5.99304, 5.9942, 5.99537, 5.99652, 5.99768, 5.99884, 6, 6.00116, 6.00231, 6.00347, 6.00462, 6.00578, 6.00693, 6.00809, 6.00924, 6.01039, 6.01154, 6.01269, 6.01384, 6.01499, 6.01614, 6.01729, 6.01843, 6.01958, 6.02073, 6.02187, 6.02302, 6.02416, 6.0253, 6.02645, 6.02759, 6.02873, 6.02987, 6.03101, 6.03215, 6.03329, 6.03442, 6.03556, 6.0367, 6.03784, 6.03897, 6.04011, 6.04124, 6.04237, 6.04351, 6.04464, 6.04577, 6.0469, 6.04803, 6.04916, 6.05029, 6.05142, 6.05255, 6.05367, 6.0548, 6.05593, 6.05705, 6.05818, 6.0593, 6.06042, 6.06155, 6.06267, 6.06379, 6.06491, 6.06603, 6.06715, 6.06827, 6.06939, 6.07051, 6.07162, 6.07274, 6.07386, 6.07497, 6.07609, 6.0772, 6.07831, 6.07943, 6.08054, 6.08165, 6.08276, 6.08387, 6.08498, 6.08609, 6.0872, 6.08831, 6.08942, 6.09052, 6.09163, 6.09274, 6.09384, 6.09494, 6.09605, 6.09715, 6.09825, 6.09936, 6.10046, 6.10156, 6.10266, 6.10376, 6.10486, 6.10596, 6.10705, 6.10815, 6.10925, 6.11034, 6.11144, 6.11253, 6.11363, 6.11472, 6.11582, 6.11691, 6.118, 6.11909, 6.12018, 6.12127, 6.12236, 6.12345, 6.12454, 6.12563, 6.12672, 6.1278, 6.12889, 6.12997, 6.13106, 6.13214, 6.13323, 6.13431, 6.13539, 6.13648, 6.13756, 6.13864, 6.13972, 6.1408, 6.14188, 6.14296, 6.14404, 6.14511, 6.14619, 6.14727, 6.14834, 6.14942, 6.15049, 6.15157, 6.15264, 6.15371, 6.15479, 6.15586, 6.15693, 6.158, 6.15907, 6.16014, 6.16121, 6.16228, 6.16335, 6.16441, 6.16548, 6.16655, 6.16761, 6.16868, 6.16974, 6.17081, 6.17187, 6.17293, 6.174, 6.17506, 6.17612, 6.17718, 6.17824, 6.1793, 6.18036, 6.18142, 6.18248, 6.18354, 6.18459, 6.18565, 6.1867, 6.18776, 6.18882, 6.18987, 6.19092, 6.19198, 6.19303, 6.19408, 6.19513, 6.19618, 6.19724, 6.19829, 6.19934, 6.20038, 6.20143, 6.20248, 6.20353, 6.20458, 6.20562, 6.20667, 6.20771, 6.20876, 6.2098, 6.21085, 6.21189, 6.21293, 6.21397, 6.21502, 6.21606, 6.2171, 6.21814, 6.21918, 6.22022, 6.22125, 6.22229, 6.22333, 6.22437, 6.2254, 6.22644, 6.22747, 6.22851, 6.22954, 6.23058, 6.23161, 6.23264, 6.23368, 6.23471, 6.23574, 6.23677, 6.2378, 6.23883, 6.23986, 6.24089, 6.24192, 6.24294, 6.24397, 6.245, 6.24602, 6.24705, 6.24808, 6.2491, 6.25012, 6.25115, 6.25217, 6.25319, 6.25422, 6.25524, 6.25626, 6.25728, 6.2583, 6.25932, 6.26034, 6.26136, 6.26238, 6.26339, 6.26441, 6.26543, 6.26644, 6.26746, 6.26847, 6.26949, 6.2705, 6.27152, 6.27253, 6.27354, 6.27455, 6.27557, 6.27658, 6.27759, 6.2786, 6.27961, 6.28062, 6.28163, 6.28264, 6.28364, 6.28465, 6.28566, 6.28666, 6.28767, 6.28868, 6.28968, 6.29069, 6.29169, 6.29269, 6.2937, 6.2947, 6.2957, 6.2967, 6.2977, 6.2987, 6.2997, 6.3007, 6.3017, 6.3027, 6.3037, 6.3047, 6.3057, 6.30669, 6.30769, 6.30868, 6.30968, 6.31067, 6.31167, 6.31266, 6.31366, 6.31465, 6.31564, 6.31663, 6.31763, 6.31862, 6.31961, 6.3206, 6.32159, 6.32258, 6.32357, 6.32456, 6.32554, 6.32653, 6.32752, 6.3285, 6.32949, 6.33048, 6.33146, 6.33245, 6.33343, 6.33441, 6.3354, 6.33638, 6.33736, 6.33835, 6.33933, 6.34031, 6.34129, 6.34227, 6.34325, 6.34423, 6.34521, 6.34618, 6.34716, 6.34814, 6.34912, 6.35009, 6.35107, 6.35205, 6.35302, 6.354, 6.35497, 6.35594, 6.35692, 6.35789, 6.35886, 6.35983, 6.36081, 6.36178, 6.36275, 6.36372, 6.36469, 6.36566, 6.36663, 6.3676, 6.36856, 6.36953, 6.3705, 6.37147, 6.37243, 6.3734, 6.37436, 6.37533, 6.37629, 6.37726, 6.37822, 6.37918, 6.38015, 6.38111, 6.38207, 6.38303, 6.38399, 6.38495, 6.38591, 6.38687, 6.38783, 6.38879, 6.38975, 6.39071, 6.39167, 6.39262, 6.39358, 6.39454, 6.39549, 6.39645, 6.3974, 6.39836, 6.39931, 6.40027, 6.40122, 6.40217, 6.40312, 6.40408, 6.40503, 6.40598, 6.40693, 6.40788, 6.40883, 6.40978, 6.41073, 6.41168, 6.41263, 6.41357, 6.41452, 6.41547, 6.41641, 6.41736, 6.41831, 6.41925, 6.4202, 6.42114, 6.42209, 6.42303, 6.42397, 6.42492, 6.42586, 6.4268, 6.42774, 6.42868, 6.42962, 6.43056, 6.4315, 6.43244, 6.43338, 6.43432, 6.43526, 6.4362, 6.43713, 6.43807, 6.43901, 6.43994, 6.44088, 6.44182, 6.44275, 6.44369, 6.44462, 6.44555, 6.44649, 6.44742, 6.44835, 6.44928, 6.45022, 6.45115, 6.45208, 6.45301, 6.45394, 6.45487, 6.4558, 6.45673, 6.45766, 6.45858, 6.45951, 6.46044, 6.46137, 6.46229, 6.46322, 6.46414, 6.46507, 6.46599, 6.46692, 6.46784, 6.46877, 6.46969, 6.47061, 6.47154, 6.47246, 6.47338, 6.4743, 6.47522, 6.47614, 6.47706, 6.47798, 6.4789, 6.47982, 6.48074, 6.48166, 6.48258, 6.48349, 6.48441, 6.48533, 6.48624, 6.48716, 6.48808, 6.48899, 6.48991, 6.49082, 6.49173, 6.49265, 6.49356, 6.49447, 6.49539, 6.4963, 6.49721, 6.49812, 6.49903, 6.49994, 6.50085, 6.50176, 6.50267, 6.50358, 6.50449, 6.5054, 6.50631, 6.50721, 6.50812, 6.50903, 6.50993, 6.51084, 6.51175, 6.51265, 6.51356, 6.51446, 6.51536, 6.51627, 6.51717, 6.51807, 6.51898, 6.51988, 6.52078, 6.52168, 6.52258, 6.52348, 6.52438, 6.52528, 6.52618, 6.52708, 6.52798, 6.52888, 6.52978, 6.53068, 6.53157, 6.53247, 6.53337, 6.53426, 6.53516, 6.53606, 6.53695, 6.53785, 6.53874, 6.53963, 6.54053, 6.54142, 6.54231, 6.54321, 6.5441, 6.54499, 6.54588, 6.54677, 6.54766, 6.54855, 6.54944, 6.55033, 6.55122, 6.55211, 6.553, 6.55389, 6.55478, 6.55566, 6.55655, 6.55744, 6.55832, 6.55921, 6.5601, 6.56098, 6.56187, 6.56275, 6.56364, 6.56452, 6.5654, 6.56629, 6.56717, 6.56805, 6.56893, 6.56982, 6.5707, 6.57158, 6.57246, 6.57334, 6.57422, 6.5751, 6.57598, 6.57686, 6.57774, 6.57861, 6.57949, 6.58037, 6.58125, 6.58212, 6.583, 6.58388, 6.58475, 6.58563, 6.5865, 6.58738, 6.58825, 6.58913, 6.59, 6.59087, 6.59175, 6.59262, 6.59349, 6.59436, 6.59524, 6.59611, 6.59698, 6.59785, 6.59872, 6.59959, 6.60046, 6.60133, 6.6022, 6.60306, 6.60393, 6.6048, 6.60567, 6.60654, 6.6074, 6.60827, 6.60913, 6.61, 6.61087, 6.61173, 6.6126, 6.61346, 6.61432, 6.61519, 6.61605, 6.61691, 6.61778, 6.61864, 6.6195, 6.62036, 6.62122, 6.62209, 6.62295, 6.62381, 6.62467, 6.62553, 6.62639, 6.62725, 6.6281, 6.62896, 6.62982, 6.63068, 6.63154, 6.63239, 6.63325, 6.63411, 6.63496, 6.63582, 6.63667, 6.63753, 6.63838, 6.63924, 6.64009, 6.64095, 6.6418, 6.64265, 6.6435, 6.64436, 6.64521, 6.64606, 6.64691, 6.64776, 6.64861, 6.64946, 6.65031, 6.65116, 6.65201, 6.65286, 6.65371, 6.65456, 6.65541, 6.65626, 6.6571, 6.65795, 6.6588, 6.65965, 6.66049, 6.66134, 6.66218, 6.66303, 6.66387, 6.66472, 6.66556, 6.66641, 6.66725, 6.66809, 6.66894, 6.66978, 6.67062, 6.67146, 6.67231, 6.67315, 6.67399, 6.67483, 6.67567, 6.67651, 6.67735, 6.67819, 6.67903, 6.67987, 6.68071, 6.68154, 6.68238, 6.68322, 6.68406, 6.68489, 6.68573, 6.68657, 6.6874, 6.68824, 6.68907, 6.68991, 6.69074, 6.69158, 6.69241, 6.69325, 6.69408, 6.69491, 6.69575, 6.69658, 6.69741, 6.69824, 6.69908, 6.69991, 6.70074, 6.70157, 6.7024, 6.70323, 6.70406, 6.70489, 6.70572, 6.70655, 6.70738, 6.7082, 6.70903, 6.70986, 6.71069, 6.71151, 6.71234, 6.71317, 6.71399, 6.71482, 6.71565, 6.71647, 6.7173, 6.71812, 6.71894, 6.71977, 6.72059, 6.72142, 6.72224, 6.72306, 6.72388, 6.72471, 6.72553, 6.72635, 6.72717, 6.72799, 6.72881, 6.72963, 6.73045, 6.73127, 6.73209, 6.73291, 6.73373, 6.73455, 6.73537, 6.73619, 6.737, 6.73782, 6.73864, 6.73946, 6.74027, 6.74109, 6.7419, 6.74272, 6.74354, 6.74435, 6.74517, 6.74598, 6.74679, 6.74761, 6.74842, 6.74923, 6.75005, 6.75086, 6.75167, 6.75248, 6.7533, 6.75411, 6.75492, 6.75573, 6.75654, 6.75735, 6.75816, 6.75897, 6.75978, 6.76059, 6.7614, 6.76221, 6.76302, 6.76382, 6.76463, 6.76544, 6.76625, 6.76705, 6.76786, 6.76867, 6.76947, 6.77028, 6.77108, 6.77189, 6.77269, 6.7735, 6.7743, 6.77511, 6.77591, 6.77671, 6.77752, 6.77832, 6.77912, 6.77992, 6.78073, 6.78153, 6.78233, 6.78313, 6.78393, 6.78473, 6.78553, 6.78633, 6.78713, 6.78793, 6.78873, 6.78953, 6.79033, 6.79113, 6.79193, 6.79272, 6.79352, 6.79432, 6.79511, 6.79591, 6.79671, 6.7975, 6.7983, 6.7991, 6.79989, 6.80069, 6.80148, 6.80227, 6.80307, 6.80386, 6.80466, 6.80545, 6.80624, 6.80704, 6.80783, 6.80862, 6.80941, 6.8102, 6.811, 6.81179, 6.81258, 6.81337, 6.81416, 6.81495, 6.81574, 6.81653, 6.81732, 6.81811, 6.81889, 6.81968, 6.82047, 6.82126, 6.82205, 6.82283, 6.82362, 6.82441, 6.82519, 6.82598, 6.82677, 6.82755, 6.82834, 6.82912, 6.82991, 6.83069, 6.83147, 6.83226, 6.83304, 6.83383, 6.83461, 6.83539, 6.83617, 6.83696, 6.83774, 6.83852, 6.8393, 6.84008, 6.84087, 6.84165, 6.84243, 6.84321, 6.84399, 6.84477, 6.84555, 6.84633, 6.8471, 6.84788, 6.84866, 6.84944, 6.85022, 6.85099, 6.85177, 6.85255, 6.85333, 6.8541, 6.85488, 6.85565, 6.85643, 6.85721, 6.85798, 6.85876, 6.85953, 6.86031, 6.86108, 6.86185, 6.86263, 6.8634, 6.86417, 6.86495, 6.86572, 6.86649, 6.86726, 6.86804, 6.86881, 6.86958, 6.87035, 6.87112, 6.87189, 6.87266, 6.87343, 6.8742, 6.87497, 6.87574, 6.87651, 6.87728, 6.87805, 6.87881, 6.87958, 6.88035, 6.88112, 6.88188, 6.88265, 6.88342, 6.88418, 6.88495, 6.88572, 6.88648, 6.88725, 6.88801, 6.88878, 6.88954, 6.89031, 6.89107, 6.89183, 6.8926, 6.89336, 6.89412, 6.89489, 6.89565, 6.89641, 6.89717, 6.89794, 6.8987, 6.89946, 6.90022, 6.90098, 6.90174, 6.9025, 6.90326, 6.90402, 6.90478, 6.90554, 6.9063, 6.90706, 6.90782, 6.90857, 6.90933, 6.91009, 6.91085, 6.9116, 6.91236, 6.91312, 6.91388, 6.91463, 6.91539, 6.91614, 6.9169, 6.91765, 6.91841, 6.91916, 6.91992, 6.92067, 6.92143, 6.92218, 6.92293, 6.92369, 6.92444, 6.92519, 6.92595, 6.9267, 6.92745, 6.9282, 6.92895, 6.92971, 6.93046, 6.93121, 6.93196, 6.93271, 6.93346, 6.93421, 6.93496, 6.93571, 6.93646, 6.93721, 6.93796, 6.9387, 6.93945, 6.9402, 6.94095, 6.9417, 6.94244, 6.94319, 6.94394, 6.94468, 6.94543, 6.94618, 6.94692, 6.94767, 6.94841, 6.94916, 6.9499, 6.95065, 6.95139, 6.95214, 6.95288, 6.95362, 6.95437, 6.95511, 6.95585, 6.9566, 6.95734, 6.95808, 6.95882, 6.95956, 6.96031, 6.96105, 6.96179, 6.96253, 6.96327, 6.96401, 6.96475, 6.96549, 6.96623, 6.96697, 6.96771, 6.96845, 6.96918, 6.96992, 6.97066, 6.9714, 6.97214, 6.97287, 6.97361, 6.97435, 6.97509, 6.97582, 6.97656, 6.9773, 6.97803, 6.97877, 6.9795, 6.98024, 6.98097, 6.98171, 6.98244, 6.98318, 6.98391, 6.98464, 6.98538, 6.98611, 6.98684, 6.98758, 6.98831, 6.98904, 6.98977, 6.99051, 6.99124, 6.99197, 6.9927, 6.99343, 6.99416, 6.99489, 6.99562, 6.99635, 6.99708, 6.99781, 6.99854, 6.99927, 7, 7.00073, 7.00146, 7.00219, 7.00291, 7.00364, 7.00437, 7.0051, 7.00582, 7.00655, 7.00728, 7.008, 7.00873, 7.00946, 7.01018, 7.01091, 7.01163, 7.01236, 7.01308, 7.01381, 7.01453, 7.01526, 7.01598, 7.0167, 7.01743, 7.01815, 7.01887, 7.0196, 7.02032, 7.02104, 7.02176, 7.02249, 7.02321, 7.02393, 7.02465, 7.02537, 7.02609, 7.02681, 7.02753, 7.02825, 7.02897, 7.02969, 7.03041, 7.03113, 7.03185, 7.03257, 7.03329, 7.03401, 7.03473, 7.03544, 7.03616, 7.03688, 7.0376, 7.03831, 7.03903, 7.03975, 7.04046, 7.04118, 7.0419, 7.04261, 7.04333, 7.04404, 7.04476, 7.04547, 7.04619, 7.0469, 7.04762, 7.04833, 7.04904, 7.04976, 7.05047, 7.05119, 7.0519, 7.05261, 7.05332, 7.05404, 7.05475, 7.05546, 7.05617, 7.05688, 7.05759, 7.05831, 7.05902, 7.05973, 7.06044, 7.06115, 7.06186, 7.06257, 7.06328, 7.06399, 7.0647, 7.0654, 7.06611, 7.06682, 7.06753, 7.06824, 7.06895, 7.06965, 7.07036, 7.07107, 7.07177, 7.07248, 7.07319, 7.07389, 7.0746, 7.07531, 7.07601, 7.07672, 7.07742, 7.07813, 7.07883, 7.07954, 7.08024, 7.08095, 7.08165, 7.08235, 7.08306, 7.08376, 7.08446, 7.08517, 7.08587, 7.08657, 7.08728, 7.08798, 7.08868, 7.08938, 7.09008, 7.09078, 7.09149, 7.09219, 7.09289, 7.09359, 7.09429, 7.09499, 7.09569, 7.09639, 7.09709, 7.09779, 7.09849, 7.09918, 7.09988, 7.10058, 7.10128, 7.10198, 7.10268, 7.10337, 7.10407, 7.10477, 7.10546, 7.10616, 7.10686, 7.10755, 7.10825, 7.10895, 7.10964, 7.11034, 7.11103, 7.11173, 7.11242, 7.11312, 7.11381, 7.11451, 7.1152, 7.11589, 7.11659, 7.11728, 7.11798, 7.11867, 7.11936, 7.12005, 7.12075, 7.12144, 7.12213, 7.12282, 7.12351, 7.12421, 7.1249, 7.12559, 7.12628, 7.12697, 7.12766, 7.12835, 7.12904, 7.12973, 7.13042, 7.13111, 7.1318, 7.13249, 7.13318, 7.13387, 7.13455, 7.13524, 7.13593, 7.13662, 7.13731, 7.13799, 7.13868, 7.13937, 7.14006, 7.14074, 7.14143, 7.14211, 7.1428, 7.14349, 7.14417, 7.14486, 7.14554, 7.14623, 7.14691, 7.1476, 7.14828, 7.14897, 7.14965, 7.15034, 7.15102, 7.1517, 7.15239, 7.15307, 7.15375, 7.15443, 7.15512, 7.1558, 7.15648, 7.15716, 7.15785, 7.15853, 7.15921, 7.15989, 7.16057, 7.16125, 7.16193, 7.16261, 7.16329, 7.16397, 7.16465, 7.16533, 7.16601, 7.16669, 7.16737, 7.16805, 7.16873, 7.16941, 7.17008, 7.17076, 7.17144, 7.17212, 7.1728, 7.17347, 7.17415, 7.17483, 7.1755, 7.17618, 7.17686, 7.17753, 7.17821, 7.17889, 7.17956, 7.18024, 7.18091, 7.18159, 7.18226, 7.18294, 7.18361, 7.18429, 7.18496, 7.18563, 7.18631, 7.18698, 7.18765, 7.18833, 7.189, 7.18967, 7.19035, 7.19102, 7.19169, 7.19236, 7.19303, 7.19371, 7.19438, 7.19505, 7.19572, 7.19639, 7.19706, 7.19773, 7.1984, 7.19907, 7.19974, 7.20041, 7.20108, 7.20175, 7.20242, 7.20309, 7.20376, 7.20443, 7.20509, 7.20576, 7.20643, 7.2071, 7.20777, 7.20843, 7.2091, 7.20977, 7.21044, 7.2111, 7.21177, 7.21244, 7.2131, 7.21377, 7.21443, 7.2151, 7.21576, 7.21643, 7.2171, 7.21776, 7.21843, 7.21909, 7.21975, 7.22042, 7.22108, 7.22175, 7.22241, 7.22307, 7.22374, 7.2244, 7.22506, 7.22573, 7.22639, 7.22705, 7.22771, 7.22837, 7.22904, 7.2297, 7.23036, 7.23102, 7.23168, 7.23234, 7.233, 7.23366, 7.23432, 7.23499, 7.23565, 7.2363, 7.23696, 7.23762, 7.23828, 7.23894, 7.2396, 7.24026, 7.24092, 7.24158, 7.24224, 7.24289, 7.24355, 7.24421, 7.24487, 7.24552, 7.24618, 7.24684, 7.2475, 7.24815, 7.24881, 7.24946, 7.25012, 7.25078, 7.25143, 7.25209, 7.25274, 7.2534, 7.25405, 7.25471, 7.25536, 7.25602, 7.25667, 7.25733, 7.25798, 7.25863, 7.25929, 7.25994, 7.26059, 7.26125, 7.2619, 7.26255, 7.2632, 7.26386, 7.26451, 7.26516, 7.26581, 7.26647, 7.26712, 7.26777, 7.26842, 7.26907, 7.26972, 7.27037, 7.27102, 7.27167, 7.27232, 7.27297, 7.27362, 7.27427, 7.27492, 7.27557, 7.27622, 7.27687, 7.27752, 7.27817, 7.27881, 7.27946, 7.28011, 7.28076, 7.28141, 7.28205, 7.2827, 7.28335, 7.28399, 7.28464, 7.28529, 7.28593, 7.28658, 7.28723, 7.28787, 7.28852, 7.28916, 7.28981, 7.29045, 7.2911, 7.29174, 7.29239, 7.29303, 7.29368, 7.29432, 7.29497, 7.29561, 7.29625, 7.2969, 7.29754, 7.29818, 7.29883, 7.29947, 7.30011, 7.30076, 7.3014, 7.30204, 7.30268, 7.30332, 7.30397, 7.30461, 7.30525, 7.30589, 7.30653, 7.30717, 7.30781, 7.30845, 7.30909, 7.30973, 7.31037, 7.31101, 7.31165, 7.31229, 7.31293, 7.31357, 7.31421, 7.31485, 7.31549, 7.31613, 7.31676, 7.3174, 7.31804, 7.31868, 7.31932, 7.31995, 7.32059, 7.32123, 7.32186, 7.3225, 7.32314, 7.32377, 7.32441, 7.32505, 7.32568, 7.32632, 7.32695, 7.32759, 7.32823, 7.32886, 7.3295, 7.33013, 7.33076, 7.3314, 7.33203, 7.33267, 7.3333, 7.33394, 7.33457, 7.3352, 7.33584, 7.33647, 7.3371, 7.33774, 7.33837, 7.339, 7.33963, 7.34027, 7.3409, 7.34153, 7.34216, 7.34279, 7.34342, 7.34406, 7.34469, 7.34532, 7.34595, 7.34658, 7.34721, 7.34784, 7.34847, 7.3491, 7.34973, 7.35036, 7.35099, 7.35162, 7.35225, 7.35288, 7.3535, 7.35413, 7.35476, 7.35539, 7.35602, 7.35665, 7.35727, 7.3579, 7.35853, 7.35916, 7.35978, 7.36041, 7.36104, 7.36166, 7.36229, 7.36292, 7.36354, 7.36417, 7.3648, 7.36542, 7.36605, 7.36667, 7.3673, 7.36792, 7.36855, 7.36917, 7.3698, 7.37042, 7.37105, 7.37167, 7.37229, 7.37292, 7.37354, 7.37416, 7.37479, 7.37541, 7.37603, 7.37666, 7.37728, 7.3779, 7.37852, 7.37915, 7.37977, 7.38039, 7.38101, 7.38163, 7.38226, 7.38288, 7.3835, 7.38412, 7.38474, 7.38536, 7.38598, 7.3866, 7.38722, 7.38784, 7.38846, 7.38908, 7.3897, 7.39032, 7.39094, 7.39156, 7.39218, 7.3928, 7.39342, 7.39403, 7.39465, 7.39527, 7.39589, 7.39651, 7.39712, 7.39774, 7.39836, 7.39898, 7.39959, 7.40021, 7.40083, 7.40144, 7.40206, 7.40268, 7.40329, 7.40391, 7.40453, 7.40514, 7.40576, 7.40637, 7.40699, 7.4076, 7.40822, 7.40883, 7.40945, 7.41006, 7.41068, 7.41129, 7.4119, 7.41252, 7.41313, 7.41375, 7.41436, 7.41497, 7.41559, 7.4162, 7.41681, 7.41742, 7.41804, 7.41865, 7.41926, 7.41987, 7.42049, 7.4211, 7.42171, 7.42232, 7.42293, 7.42354, 7.42415, 7.42476, 7.42538, 7.42599, 7.4266, 7.42721, 7.42782, 7.42843, 7.42904, 7.42965, 7.43026, 7.43086, 7.43147, 7.43208, 7.43269, 7.4333, 7.43391, 7.43452, 7.43513, 7.43573, 7.43634, 7.43695, 7.43756, 7.43817, 7.43877, 7.43938, 7.43999, 7.44059, 7.4412, 7.44181, 7.44241, 7.44302, 7.44363, 7.44423, 7.44484, 7.44544, 7.44605, 7.44666, 7.44726, 7.44787, 7.44847, 7.44908, 7.44968, 7.45029, 7.45089, 7.45149, 7.4521, 7.4527, 7.45331, 7.45391, 7.45451, 7.45512, 7.45572, 7.45632, 7.45693, 7.45753, 7.45813, 7.45873, 7.45934, 7.45994, 7.46054, 7.46114, 7.46175, 7.46235, 7.46295, 7.46355, 7.46415, 7.46475, 7.46535, 7.46595, 7.46655, 7.46716, 7.46776, 7.46836, 7.46896, 7.46956, 7.47016, 7.47076, 7.47135, 7.47195, 7.47255, 7.47315, 7.47375, 7.47435, 7.47495, 7.47555, 7.47615, 7.47674, 7.47734, 7.47794, 7.47854, 7.47914, 7.47973, 7.48033, 7.48093, 7.48152, 7.48212, 7.48272, 7.48331, 7.48391, 7.48451, 7.4851, 7.4857, 7.4863, 7.48689, 7.48749, 7.48808, 7.48868, 7.48927, 7.48987, 7.49046, 7.49106, 7.49165, 7.49225, 7.49284, 7.49344, 7.49403, 7.49462, 7.49522, 7.49581, 7.4964, 7.497, 7.49759, 7.49818, 7.49878, 7.49937, 7.49996, 7.50056, 7.50115, 7.50174, 7.50233, 7.50292, 7.50352, 7.50411, 7.5047, 7.50529, 7.50588, 7.50647, 7.50706, 7.50765, 7.50825, 7.50884, 7.50943, 7.51002, 7.51061, 7.5112, 7.51179, 7.51238, 7.51297, 7.51356, 7.51415, 7.51473, 7.51532, 7.51591, 7.5165, 7.51709, 7.51768, 7.51827, 7.51885, 7.51944, 7.52003, 7.52062, 7.52121, 7.52179, 7.52238, 7.52297, 7.52356, 7.52414, 7.52473, 7.52532, 7.5259, 7.52649, 7.52708, 7.52766, 7.52825, 7.52883, 7.52942, 7.53, 7.53059, 7.53118, 7.53176, 7.53235, 7.53293, 7.53352, 7.5341, 7.53468, 7.53527, 7.53585, 7.53644, 7.53702, 7.53761, 7.53819, 7.53877, 7.53936, 7.53994, 7.54052, 7.54111, 7.54169, 7.54227, 7.54285, 7.54344, 7.54402, 7.5446, 7.54518, 7.54576, 7.54635, 7.54693, 7.54751, 7.54809, 7.54867, 7.54925, 7.54983, 7.55042, 7.551, 7.55158, 7.55216, 7.55274, 7.55332, 7.5539, 7.55448, 7.55506, 7.55564, 7.55622, 7.5568, 7.55738, 7.55795, 7.55853, 7.55911, 7.55969, 7.56027, 7.56085, 7.56143, 7.562, 7.56258, 7.56316, 7.56374, 7.56432, 7.56489, 7.56547, 7.56605, 7.56663, 7.5672, 7.56778, 7.56836, 7.56893, 7.56951, 7.57009, 7.57066, 7.57124, 7.57181, 7.57239, 7.57297, 7.57354, 7.57412, 7.57469, 7.57527, 7.57584, 7.57642, 7.57699, 7.57757, 7.57814, 7.57872, 7.57929, 7.57986, 7.58044, 7.58101, 7.58159, 7.58216, 7.58273, 7.58331, 7.58388, 7.58445, 7.58502, 7.5856, 7.58617, 7.58674, 7.58732, 7.58789, 7.58846, 7.58903, 7.5896, 7.59018, 7.59075, 7.59132, 7.59189, 7.59246, 7.59303, 7.5936, 7.59417, 7.59475, 7.59532, 7.59589, 7.59646, 7.59703, 7.5976, 7.59817, 7.59874, 7.59931, 7.59988, 7.60045, 7.60101, 7.60158, 7.60215, 7.60272, 7.60329, 7.60386, 7.60443, 7.605, 7.60557, 7.60613, 7.6067, 7.60727, 7.60784, 7.6084, 7.60897, 7.60954, 7.61011, 7.61067, 7.61124, 7.61181, 7.61237, 7.61294, 7.61351, 7.61407, 7.61464, 7.61521, 7.61577, 7.61634, 7.6169, 7.61747, 7.61804, 7.6186, 7.61917, 7.61973, 7.6203, 7.62086, 7.62143, 7.62199, 7.62256, 7.62312, 7.62368, 7.62425, 7.62481, 7.62538, 7.62594, 7.6265, 7.62707, 7.62763, 7.62819, 7.62876, 7.62932, 7.62988, 7.63045, 7.63101, 7.63157, 7.63213, 7.6327, 7.63326, 7.63382, 7.63438, 7.63494, 7.63551, 7.63607, 7.63663, 7.63719, 7.63775, 7.63831, 7.63887, 7.63943, 7.63999, 7.64055, 7.64112, 7.64168, 7.64224, 7.6428, 7.64336, 7.64392, 7.64448, 7.64503, 7.64559, 7.64615, 7.64671, 7.64727, 7.64783, 7.64839, 7.64895, 7.64951, 7.65007, 7.65062, 7.65118, 7.65174, 7.6523, 7.65286, 7.65341, 7.65397, 7.65453, 7.65509, 7.65564, 7.6562, 7.65676, 7.65731, 7.65787, 7.65843, 7.65898, 7.65954, 7.6601, 7.66065, 7.66121, 7.66176, 7.66232, 7.66288, 7.66343, 7.66399, 7.66454, 7.6651, 7.66565, 7.66621, 7.66676, 7.66732, 7.66787, 7.66843, 7.66898, 7.66953, 7.67009, 7.67064, 7.6712, 7.67175, 7.6723, 7.67286, 7.67341, 7.67396, 7.67452, 7.67507, 7.67562, 7.67618, 7.67673, 7.67728, 7.67783, 7.67839, 7.67894, 7.67949, 7.68004, 7.68059, 7.68115, 7.6817, 7.68225, 7.6828, 7.68335, 7.6839, 7.68445, 7.685, 7.68556, 7.68611, 7.68666, 7.68721, 7.68776, 7.68831, 7.68886, 7.68941, 7.68996, 7.69051, 7.69106, 7.69161, 7.69216, 7.6927, 7.69325, 7.6938, 7.69435, 7.6949, 7.69545, 7.696, 7.69655, 7.69709, 7.69764, 7.69819, 7.69874, 7.69929, 7.69983, 7.70038, 7.70093, 7.70148, 7.70202, 7.70257, 7.70312, 7.70366, 7.70421, 7.70476, 7.7053, 7.70585, 7.7064, 7.70694, 7.70749, 7.70803, 7.70858, 7.70913, 7.70967, 7.71022, 7.71076, 7.71131, 7.71185, 7.7124, 7.71294, 7.71349, 7.71403, 7.71458, 7.71512, 7.71567, 7.71621, 7.71675, 7.7173, 7.71784, 7.71839, 7.71893, 7.71947, 7.72002, 7.72056, 7.7211, 7.72165, 7.72219, 7.72273, 7.72327, 7.72382, 7.72436, 7.7249, 7.72544, 7.72599, 7.72653, 7.72707, 7.72761, 7.72815, 7.7287, 7.72924, 7.72978, 7.73032, 7.73086, 7.7314, 7.73194, 7.73248, 7.73302, 7.73356, 7.73411, 7.73465, 7.73519, 7.73573, 7.73627, 7.73681, 7.73735, 7.73789, 7.73842, 7.73896, 7.7395, 7.74004, 7.74058, 7.74112, 7.74166, 7.7422, 7.74274, 7.74328, 7.74381, 7.74435, 7.74489, 7.74543, 7.74597, 7.7465, 7.74704, 7.74758, 7.74812, 7.74865, 7.74919, 7.74973, 7.75027, 7.7508, 7.75134, 7.75188, 7.75241, 7.75295, 7.75349, 7.75402, 7.75456, 7.7551, 7.75563, 7.75617, 7.7567, 7.75724, 7.75777, 7.75831, 7.75884, 7.75938, 7.75991, 7.76045, 7.76098, 7.76152, 7.76205, 7.76259, 7.76312, 7.76366, 7.76419, 7.76473, 7.76526, 7.76579, 7.76633, 7.76686, 7.76739, 7.76793, 7.76846, 7.76899, 7.76953, 7.77006, 7.77059, 7.77113, 7.77166, 7.77219, 7.77272, 7.77326, 7.77379, 7.77432, 7.77485, 7.77538, 7.77592, 7.77645, 7.77698, 7.77751, 7.77804, 7.77857, 7.7791, 7.77964, 7.78017, 7.7807, 7.78123, 7.78176, 7.78229, 7.78282, 7.78335, 7.78388, 7.78441, 7.78494, 7.78547, 7.786, 7.78653, 7.78706, 7.78759, 7.78812, 7.78865, 7.78917, 7.7897, 7.79023, 7.79076, 7.79129, 7.79182, 7.79235, 7.79288, 7.7934, 7.79393, 7.79446, 7.79499, 7.79552, 7.79604, 7.79657, 7.7971, 7.79763, 7.79815, 7.79868, 7.79921, 7.79973, 7.80026, 7.80079, 7.80131, 7.80184, 7.80237, 7.80289, 7.80342, 7.80395, 7.80447, 7.805, 7.80552, 7.80605, 7.80657, 7.8071, 7.80762, 7.80815, 7.80867, 7.8092, 7.80972, 7.81025, 7.81077, 7.8113, 7.81182, 7.81235, 7.81287, 7.8134, 7.81392, 7.81444, 7.81497, 7.81549, 7.81602, 7.81654, 7.81706, 7.81759, 7.81811, 7.81863, 7.81916, 7.81968, 7.8202, 7.82072, 7.82125, 7.82177, 7.82229, 7.82281, 7.82334, 7.82386, 7.82438, 7.8249, 7.82542, 7.82594, 7.82647, 7.82699, 7.82751, 7.82803, 7.82855, 7.82907, 7.82959, 7.83011, 7.83063, 7.83116, 7.83168, 7.8322, 7.83272, 7.83324, 7.83376, 7.83428, 7.8348, 7.83532, 7.83584, 7.83636, 7.83688, 7.83739, 7.83791, 7.83843, 7.83895, 7.83947, 7.83999, 7.84051, 7.84103, 7.84155, 7.84206, 7.84258, 7.8431, 7.84362, 7.84414, 7.84465, 7.84517, 7.84569, 7.84621, 7.84673, 7.84724, 7.84776, 7.84828, 7.84879, 7.84931, 7.84983, 7.85034, 7.85086, 7.85138, 7.85189, 7.85241, 7.85293, 7.85344, 7.85396, 7.85448, 7.85499, 7.85551, 7.85602, 7.85654, 7.85705, 7.85757, 7.85808, 7.8586, 7.85911, 7.85963, 7.86014, 7.86066, 7.86117, 7.86169, 7.8622, 7.86272, 7.86323, 7.86375, 7.86426, 7.86477, 7.86529, 7.8658, 7.86632, 7.86683, 7.86734, 7.86786, 7.86837, 7.86888, 7.86939, 7.86991, 7.87042, 7.87093, 7.87145, 7.87196, 7.87247, 7.87298, 7.8735, 7.87401, 7.87452, 7.87503, 7.87554, 7.87606, 7.87657, 7.87708, 7.87759, 7.8781, 7.87861, 7.87912, 7.87963, 7.88015, 7.88066, 7.88117, 7.88168, 7.88219, 7.8827, 7.88321, 7.88372, 7.88423, 7.88474, 7.88525, 7.88576, 7.88627, 7.88678, 7.88729, 7.8878, 7.88831, 7.88882, 7.88933, 7.88984, 7.89034, 7.89085, 7.89136, 7.89187, 7.89238, 7.89289, 7.8934, 7.8939, 7.89441, 7.89492, 7.89543, 7.89594, 7.89644, 7.89695, 7.89746, 7.89797, 7.89847, 7.89898, 7.89949, 7.9, 7.9005, 7.90101, 7.90152, 7.90202, 7.90253, 7.90304, 7.90354, 7.90405, 7.90456, 7.90506, 7.90557, 7.90607, 7.90658, 7.90709, 7.90759, 7.9081, 7.9086, 7.90911, 7.90961, 7.91012, 7.91062, 7.91113, 7.91163, 7.91214, 7.91264, 7.91315, 7.91365, 7.91416, 7.91466, 7.91516, 7.91567, 7.91617, 7.91668, 7.91718, 7.91768, 7.91819, 7.91869, 7.91919, 7.9197, 7.9202, 7.9207, 7.92121, 7.92171, 7.92221, 7.92272, 7.92322, 7.92372, 7.92422, 7.92473, 7.92523, 7.92573, 7.92623, 7.92673, 7.92724, 7.92774, 7.92824, 7.92874, 7.92924, 7.92974, 7.93025, 7.93075, 7.93125, 7.93175, 7.93225, 7.93275, 7.93325, 7.93375, 7.93425, 7.93475, 7.93525, 7.93575, 7.93625, 7.93675, 7.93725, 7.93775, 7.93825, 7.93875, 7.93925, 7.93975, 7.94025, 7.94075, 7.94125, 7.94175, 7.94225, 7.94275, 7.94325, 7.94375, 7.94424, 7.94474, 7.94524, 7.94574, 7.94624, 7.94674, 7.94723, 7.94773, 7.94823, 7.94873, 7.94923, 7.94972, 7.95022, 7.95072, 7.95122, 7.95171, 7.95221, 7.95271, 7.9532, 7.9537, 7.9542, 7.95469, 7.95519, 7.95569, 7.95618, 7.95668, 7.95718, 7.95767, 7.95817, 7.95867, 7.95916, 7.95966, 7.96015, 7.96065, 7.96114, 7.96164, 7.96213, 7.96263, 7.96312, 7.96362, 7.96411, 7.96461, 7.9651, 7.9656, 7.96609, 7.96659, 7.96708, 7.96758, 7.96807, 7.96857, 7.96906, 7.96955, 7.97005, 7.97054, 7.97103, 7.97153, 7.97202, 7.97251, 7.97301, 7.9735, 7.97399, 7.97449, 7.97498, 7.97547, 7.97597, 7.97646, 7.97695, 7.97744, 7.97794, 7.97843, 7.97892, 7.97941, 7.9799, 7.9804, 7.98089, 7.98138, 7.98187, 7.98236, 7.98286, 7.98335, 7.98384, 7.98433, 7.98482, 7.98531, 7.9858, 7.98629, 7.98678, 7.98727, 7.98776, 7.98826, 7.98875, 7.98924, 7.98973, 7.99022, 7.99071, 7.9912, 7.99169, 7.99218, 7.99267, 7.99316, 7.99364, 7.99413, 7.99462, 7.99511, 7.9956, 7.99609, 7.99658, 7.99707, 7.99756, 7.99805, 7.99853, 7.99902, 7.99951, 8, 8.00049, 8.00098, 8.00146, 8.00195, 8.00244, 8.00293, 8.00342, 8.0039, 8.00439, 8.00488, 8.00537, 8.00585, 8.00634, 8.00683, 8.00731, 8.0078, 8.00829, 8.00877, 8.00926, 8.00975, 8.01023, 8.01072, 8.01121, 8.01169, 8.01218, 8.01267, 8.01315, 8.01364, 8.01412, 8.01461, 8.01509, 8.01558, 8.01606, 8.01655, 8.01704, 8.01752, 8.01801, 8.01849, 8.01898, 8.01946, 8.01994, 8.02043, 8.02091, 8.0214, 8.02188, 8.02237, 8.02285, 8.02334, 8.02382, 8.0243, 8.02479, 8.02527, 8.02575, 8.02624, 8.02672, 8.0272, 8.02769, 8.02817, 8.02865, 8.02914, 8.02962, 8.0301, 8.03059, 8.03107, 8.03155, 8.03203, 8.03252, 8.033, 8.03348, 8.03396, 8.03444, 8.03493, 8.03541, 8.03589, 8.03637, 8.03685, 8.03734, 8.03782, 8.0383, 8.03878, 8.03926, 8.03974, 8.04022, 8.0407, 8.04118, 8.04167, 8.04215, 8.04263, 8.04311, 8.04359, 8.04407, 8.04455, 8.04503, 8.04551, 8.04599, 8.04647, 8.04695, 8.04743, 8.04791, 8.04839, 8.04887, 8.04935, 8.04983, 8.0503, 8.05078, 8.05126, 8.05174, 8.05222, 8.0527, 8.05318, 8.05366, 8.05414, 8.05461, 8.05509, 8.05557, 8.05605, 8.05653, 8.057, 8.05748, 8.05796, 8.05844, 8.05892, 8.05939, 8.05987, 8.06035, 8.06083, 8.0613, 8.06178, 8.06226, 8.06273, 8.06321, 8.06369, 8.06417, 8.06464, 8.06512, 8.0656, 8.06607, 8.06655, 8.06702, 8.0675, 8.06798, 8.06845, 8.06893, 8.0694, 8.06988, 8.07036, 8.07083, 8.07131, 8.07178, 8.07226, 8.07273, 8.07321, 8.07368, 8.07416, 8.07463, 8.07511, 8.07558, 8.07606, 8.07653, 8.07701, 8.07748, 8.07795, 8.07843, 8.0789, 8.07938, 8.07985, 8.08033, 8.0808, 8.08127, 8.08175, 8.08222, 8.08269, 8.08317, 8.08364, 8.08411, 8.08459, 8.08506, 8.08553, 8.08601, 8.08648, 8.08695, 8.08742, 8.0879, 8.08837, 8.08884, 8.08931, 8.08979, 8.09026, 8.09073, 8.0912, 8.09167, 8.09215, 8.09262, 8.09309, 8.09356, 8.09403, 8.0945, 8.09497, 8.09545, 8.09592, 8.09639, 8.09686, 8.09733, 8.0978, 8.09827, 8.09874, 8.09921, 8.09968, 8.10015, 8.10062, 8.10109, 8.10157, 8.10204, 8.10251, 8.10298, 8.10344, 8.10391, 8.10438, 8.10485, 8.10532, 8.10579, 8.10626, 8.10673, 8.1072, 8.10767, 8.10814, 8.10861, 8.10908, 8.10955, 8.11001, 8.11048, 8.11095, 8.11142, 8.11189, 8.11236, 8.11283, 8.11329, 8.11376, 8.11423, 8.1147, 8.11517, 8.11563, 8.1161, 8.11657, 8.11704, 8.1175, 8.11797, 8.11844, 8.1189, 8.11937, 8.11984, 8.12031, 8.12077, 8.12124, 8.12171, 8.12217, 8.12264, 8.12311, 8.12357, 8.12404, 8.1245, 8.12497, 8.12544, 8.1259, 8.12637, 8.12683, 8.1273, 8.12777, 8.12823, 8.1287, 8.12916, 8.12963, 8.13009, 8.13056, 8.13102, 8.13149, 8.13195, 8.13242, 8.13288, 8.13335, 8.13381, 8.13428, 8.13474, 8.13521, 8.13567, 8.13613, 8.1366, 8.13706, 8.13753, 8.13799, 8.13845, 8.13892, 8.13938, 8.13984, 8.14031, 8.14077, 8.14124, 8.1417, 8.14216, 8.14262, 8.14309, 8.14355, 8.14401, 8.14448, 8.14494, 8.1454, 8.14586, 8.14633, 8.14679, 8.14725, 8.14771, 8.14818, 8.14864, 8.1491, 8.14956, 8.15002, 8.15049, 8.15095, 8.15141, 8.15187, 8.15233, 8.15279, 8.15325, 8.15372, 8.15418, 8.15464, 8.1551, 8.15556, 8.15602, 8.15648, 8.15694, 8.1574, 8.15786, 8.15832, 8.15878, 8.15924, 8.1597, 8.16016, 8.16062, 8.16108, 8.16154, 8.162, 8.16246, 8.16292, 8.16338, 8.16384, 8.1643, 8.16476, 8.16522, 8.16568, 8.16614, 8.1666, 8.16706, 8.16752, 8.16797, 8.16843, 8.16889, 8.16935, 8.16981, 8.17027, 8.17073, 8.17118, 8.17164, 8.1721, 8.17256, 8.17302, 8.17347, 8.17393, 8.17439, 8.17485, 8.17531, 8.17576, 8.17622, 8.17668, 8.17713, 8.17759, 8.17805, 8.17851, 8.17896, 8.17942, 8.17988, 8.18033, 8.18079, 8.18125, 8.1817, 8.18216, 8.18262, 8.18307, 8.18353, 8.18398, 8.18444, 8.1849, 8.18535, 8.18581, 8.18626, 8.18672, 8.18718, 8.18763, 8.18809, 8.18854, 8.189, 8.18945, 8.18991, 8.19036, 8.19082, 8.19127, 8.19173, 8.19218, 8.19264, 8.19309, 8.19355, 8.194, 8.19445, 8.19491, 8.19536, 8.19582, 8.19627, 8.19673, 8.19718, 8.19763, 8.19809, 8.19854, 8.19899, 8.19945, 8.1999, 8.20035, 8.20081, 8.20126, 8.20171, 8.20217, 8.20262, 8.20307, 8.20353, 8.20398, 8.20443, 8.20488, 8.20534, 8.20579, 8.20624, 8.20669, 8.20715, 8.2076, 8.20805, 8.2085, 8.20896, 8.20941, 8.20986, 8.21031, 8.21076, 8.21121, 8.21167, 8.21212, 8.21257, 8.21302, 8.21347, 8.21392, 8.21437, 8.21482, 8.21527, 8.21573, 8.21618, 8.21663, 8.21708, 8.21753, 8.21798, 8.21843, 8.21888, 8.21933, 8.21978, 8.22023, 8.22068, 8.22113, 8.22158, 8.22203, 8.22248, 8.22293, 8.22338, 8.22383, 8.22428, 8.22473, 8.22518, 8.22563, 8.22607, 8.22652, 8.22697, 8.22742, 8.22787, 8.22832, 8.22877, 8.22922, 8.22967, 8.23011, 8.23056, 8.23101, 8.23146, 8.23191, 8.23236, 8.2328, 8.23325, 8.2337, 8.23415, 8.23459, 8.23504, 8.23549, 8.23594, 8.23639, 8.23683, 8.23728, 8.23773, 8.23817, 8.23862, 8.23907, 8.23952, 8.23996, 8.24041, 8.24086, 8.2413, 8.24175, 8.2422, 8.24264, 8.24309, 8.24353, 8.24398, 8.24443, 8.24487, 8.24532, 8.24577, 8.24621, 8.24666, 8.2471, 8.24755, 8.24799, 8.24844, 8.24888, 8.24933, 8.24978, 8.25022, 8.25067, 8.25111, 8.25156, 8.252, 8.25245, 8.25289, 8.25334, 8.25378, 8.25422, 8.25467, 8.25511, 8.25556, 8.256, 8.25645, 8.25689, 8.25733, 8.25778, 8.25822, 8.25867, 8.25911, 8.25955, 8.26, 8.26044, 8.26088, 8.26133, 8.26177, 8.26221, 8.26266, 8.2631, 8.26354, 8.26399, 8.26443, 8.26487, 8.26532, 8.26576, 8.2662, 8.26664, 8.26709, 8.26753, 8.26797, 8.26841, 8.26886, 8.2693, 8.26974, 8.27018, 8.27062, 8.27107, 8.27151, 8.27195, 8.27239, 8.27283, 8.27327, 8.27372, 8.27416, 8.2746, 8.27504, 8.27548, 8.27592, 8.27636, 8.2768, 8.27724, 8.27769, 8.27813, 8.27857, 8.27901, 8.27945, 8.27989, 8.28033, 8.28077, 8.28121, 8.28165, 8.28209, 8.28253, 8.28297, 8.28341, 8.28385, 8.28429, 8.28473, 8.28517, 8.28561, 8.28605, 8.28649, 8.28693, 8.28737, 8.2878, 8.28824, 8.28868, 8.28912, 8.28956, 8.29, 8.29044, 8.29088, 8.29132, 8.29175, 8.29219, 8.29263, 8.29307, 8.29351, 8.29395, 8.29438, 8.29482, 8.29526, 8.2957, 8.29614, 8.29657, 8.29701, 8.29745, 8.29789, 8.29832, 8.29876, 8.2992, 8.29964, 8.30007, 8.30051, 8.30095, 8.30138, 8.30182, 8.30226, 8.3027, 8.30313, 8.30357, 8.30401, 8.30444, 8.30488, 8.30532, 8.30575, 8.30619, 8.30662, 8.30706, 8.3075, 8.30793, 8.30837, 8.3088, 8.30924, 8.30968, 8.31011, 8.31055, 8.31098, 8.31142, 8.31185, 8.31229, 8.31272, 8.31316, 8.31359, 8.31403, 8.31446, 8.3149, 8.31533, 8.31577, 8.3162, 8.31664, 8.31707, 8.31751, 8.31794, 8.31838, 8.31881, 8.31924, 8.31968, 8.32011, 8.32055, 8.32098, 8.32141, 8.32185, 8.32228, 8.32272, 8.32315, 8.32358, 8.32402, 8.32445, 8.32488, 8.32532, 8.32575, 8.32618, 8.32662, 8.32705, 8.32748, 8.32791, 8.32835, 8.32878, 8.32921, 8.32965, 8.33008, 8.33051, 8.33094, 8.33138, 8.33181, 8.33224, 8.33267, 8.3331, 8.33354, 8.33397, 8.3344, 8.33483, 8.33526, 8.33569, 8.33613, 8.33656, 8.33699, 8.33742, 8.33785, 8.33828, 8.33871, 8.33915, 8.33958, 8.34001, 8.34044, 8.34087, 8.3413, 8.34173, 8.34216, 8.34259, 8.34302, 8.34345, 8.34388, 8.34431, 8.34474, 8.34517, 8.3456, 8.34603, 8.34646, 8.34689, 8.34732, 8.34775, 8.34818, 8.34861, 8.34904, 8.34947, 8.3499, 8.35033, 8.35076, 8.35119, 8.35162, 8.35205, 8.35248, 8.35291, 8.35334, 8.35376, 8.35419, 8.35462, 8.35505, 8.35548, 8.35591, 8.35634, 8.35676, 8.35719, 8.35762, 8.35805, 8.35848, 8.35891, 8.35933, 8.35976, 8.36019, 8.36062, 8.36105, 8.36147, 8.3619, 8.36233, 8.36276, 8.36318, 8.36361, 8.36404, 8.36447, 8.36489, 8.36532, 8.36575, 8.36617, 8.3666, 8.36703, 8.36745, 8.36788, 8.36831, 8.36873, 8.36916, 8.36959, 8.37001, 8.37044, 8.37087, 8.37129, 8.37172, 8.37214, 8.37257, 8.373, 8.37342, 8.37385, 8.37427, 8.3747, 8.37512, 8.37555, 8.37598, 8.3764, 8.37683, 8.37725, 8.37768, 8.3781, 8.37853, 8.37895, 8.37938, 8.3798, 8.38023, 8.38065, 8.38108, 8.3815, 8.38193, 8.38235, 8.38277, 8.3832, 8.38362, 8.38405, 8.38447, 8.3849, 8.38532, 8.38574, 8.38617, 8.38659, 8.38702, 8.38744, 8.38786, 8.38829, 8.38871, 8.38913, 8.38956, 8.38998, 8.3904, 8.39083, 8.39125, 8.39167, 8.3921, 8.39252, 8.39294, 8.39336, 8.39379, 8.39421, 8.39463, 8.39505, 8.39548, 8.3959, 8.39632, 8.39674, 8.39717, 8.39759, 8.39801, 8.39843, 8.39886, 8.39928, 8.3997, 8.40012, 8.40054, 8.40096, 8.40139, 8.40181, 8.40223, 8.40265, 8.40307, 8.40349, 8.40391, 8.40434, 8.40476, 8.40518, 8.4056, 8.40602, 8.40644, 8.40686, 8.40728, 8.4077, 8.40812, 8.40854, 8.40896, 8.40938, 8.4098, 8.41023, 8.41065, 8.41107, 8.41149, 8.41191, 8.41233, 8.41275, 8.41317, 8.41359, 8.414, 8.41442, 8.41484, 8.41526, 8.41568, 8.4161, 8.41652, 8.41694, 8.41736, 8.41778, 8.4182, 8.41862, 8.41904, 8.41946, 8.41987, 8.42029, 8.42071, 8.42113, 8.42155, 8.42197, 8.42239, 8.4228, 8.42322, 8.42364, 8.42406, 8.42448, 8.4249, 8.42531, 8.42573, 8.42615, 8.42657, 8.42699, 8.4274, 8.42782, 8.42824, 8.42866, 8.42907, 8.42949, 8.42991, 8.43033, 8.43074, 8.43116, 8.43158, 8.43199, 8.43241, 8.43283, 8.43324, 8.43366, 8.43408, 8.43449, 8.43491, 8.43533, 8.43574, 8.43616, 8.43658, 8.43699, 8.43741, 8.43783, 8.43824, 8.43866, 8.43907, 8.43949, 8.43991, 8.44032, 8.44074, 8.44115, 8.44157, 8.44198, 8.4424, 8.44282, 8.44323, 8.44365, 8.44406, 8.44448, 8.44489, 8.44531, 8.44572, 8.44614, 8.44655, 8.44697, 8.44738, 8.4478, 8.44821, 8.44863, 8.44904, 8.44945, 8.44987, 8.45028, 8.4507, 8.45111, 8.45153, 8.45194, 8.45235, 8.45277, 8.45318, 8.4536, 8.45401, 8.45442, 8.45484, 8.45525, 8.45566, 8.45608, 8.45649, 8.4569, 8.45732, 8.45773, 8.45814, 8.45856, 8.45897, 8.45938, 8.4598, 8.46021, 8.46062, 8.46103, 8.46145, 8.46186, 8.46227, 8.46268, 8.4631, 8.46351, 8.46392, 8.46433, 8.46475, 8.46516, 8.46557, 8.46598, 8.46639, 8.46681, 8.46722, 8.46763, 8.46804, 8.46845, 8.46887, 8.46928, 8.46969, 8.4701, 8.47051, 8.47092, 8.47133, 8.47175, 8.47216, 8.47257, 8.47298, 8.47339, 8.4738, 8.47421, 8.47462, 8.47503, 8.47544, 8.47585, 8.47626, 8.47667, 8.47709, 8.4775, 8.47791, 8.47832, 8.47873, 8.47914, 8.47955, 8.47996, 8.48037, 8.48078, 8.48119, 8.4816, 8.48201, 8.48242, 8.48283, 8.48323, 8.48364, 8.48405, 8.48446, 8.48487, 8.48528, 8.48569, 8.4861, 8.48651, 8.48692, 8.48733, 8.48774, 8.48814, 8.48855, 8.48896, 8.48937, 8.48978, 8.49019, 8.4906, 8.491, 8.49141, 8.49182, 8.49223, 8.49264, 8.49305, 8.49345, 8.49386, 8.49427, 8.49468, 8.49509, 8.49549, 8.4959, 8.49631, 8.49672, 8.49712, 8.49753, 8.49794, 8.49835, 8.49875, 8.49916, 8.49957, 8.49997, 8.50038, 8.50079, 8.5012, 8.5016, 8.50201, 8.50242, 8.50282, 8.50323, 8.50364, 8.50404, 8.50445, 8.50486, 8.50526, 8.50567, 8.50607, 8.50648, 8.50689, 8.50729, 8.5077, 8.5081, 8.50851, 8.50892, 8.50932, 8.50973, 8.51013, 8.51054, 8.51094, 8.51135, 8.51176, 8.51216, 8.51257, 8.51297, 8.51338, 8.51378, 8.51419, 8.51459, 8.515, 8.5154, 8.51581, 8.51621, 8.51662, 8.51702, 8.51743, 8.51783, 8.51823, 8.51864, 8.51904, 8.51945, 8.51985, 8.52026, 8.52066, 8.52106, 8.52147, 8.52187, 8.52228, 8.52268, 8.52308, 8.52349, 8.52389, 8.5243, 8.5247, 8.5251, 8.52551, 8.52591, 8.52631, 8.52672, 8.52712, 8.52752, 8.52793, 8.52833, 8.52873, 8.52913, 8.52954, 8.52994, 8.53034, 8.53075, 8.53115, 8.53155, 8.53195, 8.53236, 8.53276, 8.53316, 8.53356, 8.53397, 8.53437, 8.53477, 8.53517, 8.53557, 8.53598, 8.53638, 8.53678, 8.53718, 8.53758, 8.53799, 8.53839, 8.53879, 8.53919, 8.53959, 8.53999, 8.54039, 8.5408, 8.5412, 8.5416, 8.542, 8.5424, 8.5428, 8.5432, 8.5436, 8.544, 8.5444, 8.54481, 8.54521, 8.54561, 8.54601, 8.54641, 8.54681, 8.54721, 8.54761, 8.54801, 8.54841, 8.54881, 8.54921, 8.54961, 8.55001, 8.55041, 8.55081, 8.55121, 8.55161, 8.55201, 8.55241, 8.55281, 8.55321, 8.55361, 8.55401, 8.55441, 8.55481, 8.5552, 8.5556, 8.556, 8.5564, 8.5568, 8.5572, 8.5576, 8.558, 8.5584, 8.5588, 8.55919, 8.55959, 8.55999, 8.56039, 8.56079, 8.56119, 8.56159, 8.56198, 8.56238, 8.56278, 8.56318, 8.56358, 8.56397, 8.56437, 8.56477, 8.56517, 8.56557, 8.56596, 8.56636, 8.56676, 8.56716, 8.56755, 8.56795, 8.56835, 8.56875, 8.56914, 8.56954, 8.56994, 8.57034, 8.57073, 8.57113, 8.57153, 8.57192, 8.57232, 8.57272, 8.57311, 8.57351, 8.57391, 8.5743, 8.5747, 8.5751, 8.57549, 8.57589, 8.57629, 8.57668, 8.57708, 8.57748, 8.57787, 8.57827, 8.57866, 8.57906, 8.57946, 8.57985, 8.58025, 8.58064, 8.58104, 8.58143, 8.58183, 8.58223, 8.58262, 8.58302, 8.58341, 8.58381, 8.5842, 8.5846, 8.58499, 8.58539, 8.58578, 8.58618, 8.58657, 8.58697, 8.58736, 8.58776, 8.58815, 8.58855, 8.58894, 8.58934, 8.58973, 8.59012, 8.59052, 8.59091, 8.59131, 8.5917, 8.5921, 8.59249, 8.59288, 8.59328, 8.59367, 8.59407, 8.59446, 8.59485, 8.59525, 8.59564, 8.59603, 8.59643, 8.59682, 8.59722, 8.59761, 8.598, 8.5984, 8.59879, 8.59918, 8.59957, 8.59997, 8.60036, 8.60075, 8.60115, 8.60154, 8.60193, 8.60233, 8.60272, 8.60311, 8.6035, 8.6039, 8.60429, 8.60468, 8.60507, 8.60547, 8.60586, 8.60625, 8.60664, 8.60703, 8.60743, 8.60782, 8.60821, 8.6086, 8.60899, 8.60939, 8.60978, 8.61017, 8.61056, 8.61095, 8.61134, 8.61174, 8.61213, 8.61252, 8.61291, 8.6133, 8.61369, 8.61408, 8.61447, 8.61487, 8.61526, 8.61565, 8.61604, 8.61643, 8.61682, 8.61721, 8.6176, 8.61799, 8.61838, 8.61877, 8.61916, 8.61955, 8.61994, 8.62033, 8.62072, 8.62111, 8.6215, 8.62189, 8.62228, 8.62267, 8.62306, 8.62345, 8.62384, 8.62423, 8.62462, 8.62501, 8.6254, 8.62579, 8.62618, 8.62657, 8.62696, 8.62735, 8.62774, 8.62813, 8.62852, 8.62891, 8.6293, 8.62969, 8.63007, 8.63046, 8.63085, 8.63124, 8.63163, 8.63202, 8.63241, 8.6328, 8.63318, 8.63357, 8.63396, 8.63435, 8.63474, 8.63513, 8.63551, 8.6359, 8.63629, 8.63668, 8.63707, 8.63746, 8.63784, 8.63823, 8.63862, 8.63901, 8.63939, 8.63978, 8.64017, 8.64056, 8.64094, 8.64133, 8.64172, 8.64211, 8.64249, 8.64288, 8.64327, 8.64366, 8.64404, 8.64443, 8.64482, 8.6452, 8.64559, 8.64598, 8.64636, 8.64675, 8.64714, 8.64752, 8.64791, 8.6483, 8.64868, 8.64907, 8.64946, 8.64984, 8.65023, 8.65062, 8.651, 8.65139, 8.65177, 8.65216, 8.65255, 8.65293, 8.65332, 8.6537, 8.65409, 8.65447, 8.65486, 8.65525, 8.65563, 8.65602, 8.6564, 8.65679, 8.65717, 8.65756, 8.65794, 8.65833, 8.65871, 8.6591, 8.65948, 8.65987, 8.66025, 8.66064, 8.66102, 8.66141, 8.66179, 8.66218, 8.66256, 8.66295, 8.66333, 8.66372, 8.6641, 8.66448, 8.66487, 8.66525, 8.66564, 8.66602, 8.66641, 8.66679, 8.66717, 8.66756, 8.66794, 8.66833, 8.66871, 8.66909, 8.66948, 8.66986, 8.67024, 8.67063, 8.67101, 8.67139, 8.67178, 8.67216, 8.67254, 8.67293, 8.67331, 8.67369, 8.67408, 8.67446, 8.67484, 8.67523, 8.67561, 8.67599, 8.67637, 8.67676, 8.67714, 8.67752, 8.67791, 8.67829, 8.67867, 8.67905, 8.67944, 8.67982, 8.6802, 8.68058, 8.68096, 8.68135, 8.68173, 8.68211, 8.68249, 8.68287, 8.68326, 8.68364, 8.68402, 8.6844, 8.68478, 8.68516, 8.68555, 8.68593, 8.68631, 8.68669, 8.68707, 8.68745, 8.68783, 8.68822, 8.6886, 8.68898, 8.68936, 8.68974, 8.69012, 8.6905, 8.69088, 8.69126, 8.69164, 8.69203, 8.69241, 8.69279, 8.69317, 8.69355, 8.69393, 8.69431, 8.69469, 8.69507, 8.69545, 8.69583, 8.69621, 8.69659, 8.69697, 8.69735, 8.69773, 8.69811, 8.69849, 8.69887, 8.69925, 8.69963, 8.70001, 8.70039, 8.70077, 8.70115, 8.70153, 8.70191, 8.70229, 8.70267, 8.70304, 8.70342, 8.7038, 8.70418, 8.70456, 8.70494, 8.70532, 8.7057, 8.70608, 8.70646, 8.70683, 8.70721, 8.70759, 8.70797, 8.70835, 8.70873, 8.70911, 8.70948, 8.70986, 8.71024, 8.71062, 8.711, 8.71138, 8.71175, 8.71213, 8.71251, 8.71289, 8.71327, 8.71364, 8.71402, 8.7144, 8.71478, 8.71516, 8.71553, 8.71591, 8.71629, 8.71667, 8.71704, 8.71742, 8.7178, 8.71818, 8.71855, 8.71893, 8.71931, 8.71968, 8.72006, 8.72044, 8.72081, 8.72119, 8.72157, 8.72195, 8.72232, 8.7227, 8.72308, 8.72345, 8.72383, 8.72421, 8.72458, 8.72496, 8.72533, 8.72571, 8.72609, 8.72646, 8.72684, 8.72722, 8.72759, 8.72797, 8.72834, 8.72872, 8.7291, 8.72947, 8.72985, 8.73022, 8.7306, 8.73097, 8.73135, 8.73173, 8.7321, 8.73248, 8.73285, 8.73323, 8.7336, 8.73398, 8.73435, 8.73473, 8.7351, 8.73548, 8.73585, 8.73623, 8.7366, 8.73698, 8.73735, 8.73773, 8.7381, 8.73848, 8.73885, 8.73923, 8.7396, 8.73998, 8.74035, 8.74072, 8.7411, 8.74147, 8.74185, 8.74222, 8.7426, 8.74297, 8.74334, 8.74372, 8.74409, 8.74447, 8.74484, 8.74521, 8.74559, 8.74596, 8.74633, 8.74671, 8.74708, 8.74746, 8.74783, 8.7482, 8.74858, 8.74895, 8.74932, 8.7497, 8.75007, 8.75044, 8.75081, 8.75119, 8.75156, 8.75193, 8.75231, 8.75268, 8.75305, 8.75343, 8.7538, 8.75417, 8.75454, 8.75492, 8.75529, 8.75566, 8.75603, 8.75641, 8.75678, 8.75715, 8.75752, 8.75789, 8.75827, 8.75864, 8.75901, 8.75938, 8.75975, 8.76013, 8.7605, 8.76087, 8.76124, 8.76161, 8.76199, 8.76236, 8.76273, 8.7631, 8.76347, 8.76384, 8.76421, 8.76459, 8.76496, 8.76533, 8.7657, 8.76607, 8.76644, 8.76681, 8.76718, 8.76755, 8.76793, 8.7683, 8.76867, 8.76904, 8.76941, 8.76978, 8.77015, 8.77052, 8.77089, 8.77126, 8.77163, 8.772, 8.77237, 8.77274, 8.77311, 8.77348, 8.77385, 8.77422, 8.77459, 8.77496, 8.77533, 8.7757, 8.77607, 8.77644, 8.77681, 8.77718, 8.77755, 8.77792, 8.77829, 8.77866, 8.77903, 8.7794, 8.77977, 8.78014, 8.78051, 8.78088, 8.78125, 8.78162, 8.78199, 8.78236, 8.78272, 8.78309, 8.78346, 8.78383, 8.7842, 8.78457, 8.78494, 8.78531, 8.78567, 8.78604, 8.78641, 8.78678, 8.78715, 8.78752, 8.78789, 8.78825, 8.78862, 8.78899, 8.78936, 8.78973, 8.7901, 8.79046, 8.79083, 8.7912, 8.79157, 8.79194, 8.7923, 8.79267, 8.79304, 8.79341, 8.79377, 8.79414, 8.79451, 8.79488, 8.79524, 8.79561, 8.79598, 8.79635, 8.79671, 8.79708, 8.79745, 8.79782, 8.79818, 8.79855, 8.79892, 8.79928, 8.79965, 8.80002, 8.80038, 8.80075, 8.80112, 8.80148, 8.80185, 8.80222, 8.80258, 8.80295, 8.80332, 8.80368, 8.80405, 8.80442, 8.80478, 8.80515, 8.80551, 8.80588, 8.80625, 8.80661, 8.80698, 8.80734, 8.80771, 8.80808, 8.80844, 8.80881, 8.80917, 8.80954, 8.80991, 8.81027, 8.81064, 8.811, 8.81137, 8.81173, 8.8121, 8.81246, 8.81283, 8.81319, 8.81356, 8.81392, 8.81429, 8.81465, 8.81502, 8.81538, 8.81575, 8.81611, 8.81648, 8.81684, 8.81721, 8.81757, 8.81794, 8.8183, 8.81867, 8.81903, 8.8194, 8.81976, 8.82012, 8.82049, 8.82085, 8.82122, 8.82158, 8.82195, 8.82231, 8.82267, 8.82304, 8.8234, 8.82377, 8.82413, 8.82449, 8.82486, 8.82522, 8.82558, 8.82595, 8.82631, 8.82668, 8.82704, 8.8274, 8.82777, 8.82813, 8.82849, 8.82886, 8.82922, 8.82958, 8.82995, 8.83031, 8.83067, 8.83103, 8.8314, 8.83176, 8.83212, 8.83249, 8.83285, 8.83321, 8.83357, 8.83394, 8.8343, 8.83466, 8.83503, 8.83539, 8.83575, 8.83611, 8.83647, 8.83684, 8.8372, 8.83756, 8.83792, 8.83829, 8.83865, 8.83901, 8.83937, 8.83973, 8.8401, 8.84046, 8.84082, 8.84118, 8.84154, 8.8419, 8.84227, 8.84263, 8.84299, 8.84335, 8.84371, 8.84407, 8.84444, 8.8448, 8.84516, 8.84552, 8.84588, 8.84624, 8.8466, 8.84696, 8.84732, 8.84769, 8.84805, 8.84841, 8.84877, 8.84913, 8.84949, 8.84985, 8.85021, 8.85057, 8.85093, 8.85129, 8.85165, 8.85201, 8.85237, 8.85273, 8.8531, 8.85346, 8.85382, 8.85418, 8.85454, 8.8549, 8.85526, 8.85562, 8.85598, 8.85634, 8.8567, 8.85706, 8.85742, 8.85778, 8.85813, 8.85849, 8.85885, 8.85921, 8.85957, 8.85993, 8.86029, 8.86065, 8.86101, 8.86137, 8.86173, 8.86209, 8.86245, 8.86281, 8.86317, 8.86353, 8.86388, 8.86424, 8.8646, 8.86496, 8.86532, 8.86568, 8.86604, 8.8664, 8.86675, 8.86711, 8.86747, 8.86783, 8.86819, 8.86855, 8.86891, 8.86926, 8.86962, 8.86998, 8.87034, 8.8707, 8.87105, 8.87141, 8.87177, 8.87213, 8.87249, 8.87284, 8.8732, 8.87356, 8.87392, 8.87428, 8.87463, 8.87499, 8.87535, 8.87571, 8.87606, 8.87642, 8.87678, 8.87714, 8.87749, 8.87785, 8.87821, 8.87857, 8.87892, 8.87928, 8.87964, 8.87999, 8.88035, 8.88071, 8.88107, 8.88142, 8.88178, 8.88214, 8.88249, 8.88285, 8.88321, 8.88356, 8.88392, 8.88428, 8.88463, 8.88499, 8.88534, 8.8857, 8.88606, 8.88641, 8.88677, 8.88713, 8.88748, 8.88784, 8.88819, 8.88855, 8.88891, 8.88926, 8.88962, 8.88997, 8.89033, 8.89069, 8.89104, 8.8914, 8.89175, 8.89211, 8.89246, 8.89282, 8.89317, 8.89353, 8.89389, 8.89424, 8.8946, 8.89495, 8.89531, 8.89566, 8.89602, 8.89637, 8.89673, 8.89708, 8.89744, 8.89779, 8.89815, 8.8985, 8.89886, 8.89921, 8.89957, 8.89992, 8.90028, 8.90063, 8.90098, 8.90134, 8.90169, 8.90205, 8.9024, 8.90276, 8.90311, 8.90346, 8.90382, 8.90417, 8.90453, 8.90488, 8.90524, 8.90559, 8.90594, 8.9063, 8.90665, 8.907, 8.90736, 8.90771, 8.90807, 8.90842, 8.90877, 8.90913, 8.90948, 8.90983, 8.91019, 8.91054, 8.91089, 8.91125, 8.9116, 8.91195, 8.91231, 8.91266, 8.91301, 8.91337, 8.91372, 8.91407, 8.91443, 8.91478, 8.91513, 8.91548, 8.91584, 8.91619, 8.91654, 8.91689, 8.91725, 8.9176, 8.91795, 8.9183, 8.91866, 8.91901, 8.91936, 8.91971, 8.92007, 8.92042, 8.92077, 8.92112, 8.92147, 8.92183, 8.92218, 8.92253, 8.92288, 8.92323, 8.92359, 8.92394, 8.92429, 8.92464, 8.92499, 8.92535, 8.9257, 8.92605, 8.9264, 8.92675, 8.9271, 8.92745, 8.92781, 8.92816, 8.92851, 8.92886, 8.92921, 8.92956, 8.92991, 8.93026, 8.93061, 8.93097, 8.93132, 8.93167, 8.93202, 8.93237, 8.93272, 8.93307, 8.93342, 8.93377, 8.93412, 8.93447, 8.93482, 8.93517, 8.93552, 8.93587, 8.93623, 8.93658, 8.93693, 8.93728, 8.93763, 8.93798, 8.93833, 8.93868, 8.93903, 8.93938, 8.93973, 8.94008, 8.94043, 8.94078, 8.94113, 8.94148, 8.94183, 8.94217, 8.94252, 8.94287, 8.94322, 8.94357, 8.94392, 8.94427, 8.94462, 8.94497, 8.94532, 8.94567, 8.94602, 8.94637, 8.94672, 8.94707, 8.94741, 8.94776, 8.94811, 8.94846, 8.94881, 8.94916, 8.94951, 8.94986, 8.95021, 8.95055, 8.9509, 8.95125, 8.9516, 8.95195, 8.9523, 8.95265, 8.95299, 8.95334, 8.95369, 8.95404, 8.95439, 8.95474, 8.95508, 8.95543, 8.95578, 8.95613, 8.95648, 8.95682, 8.95717, 8.95752, 8.95787, 8.95821, 8.95856, 8.95891, 8.95926, 8.95961, 8.95995, 8.9603, 8.96065, 8.961, 8.96134, 8.96169, 8.96204, 8.96238, 8.96273, 8.96308, 8.96343, 8.96377, 8.96412, 8.96447, 8.96481, 8.96516, 8.96551, 8.96586, 8.9662, 8.96655, 8.9669, 8.96724, 8.96759, 8.96794, 8.96828, 8.96863, 8.96898, 8.96932, 8.96967, 8.97002, 8.97036, 8.97071, 8.97105, 8.9714, 8.97175, 8.97209, 8.97244, 8.97278, 8.97313, 8.97348, 8.97382, 8.97417, 8.97451, 8.97486, 8.97521, 8.97555, 8.9759, 8.97624, 8.97659, 8.97693, 8.97728, 8.97763, 8.97797, 8.97832, 8.97866, 8.97901, 8.97935, 8.9797, 8.98004, 8.98039, 8.98073, 8.98108, 8.98142, 8.98177, 8.98211, 8.98246, 8.9828, 8.98315, 8.98349, 8.98384, 8.98418, 8.98453, 8.98487, 8.98522, 8.98556, 8.98591, 8.98625, 8.9866, 8.98694, 8.98728, 8.98763, 8.98797, 8.98832, 8.98866, 8.98901, 8.98935, 8.98969, 8.99004, 8.99038, 8.99073, 8.99107, 8.99141, 8.99176, 8.9921, 8.99245, 8.99279, 8.99313, 8.99348, 8.99382, 8.99416, 8.99451, 8.99485, 8.9952, 8.99554, 8.99588, 8.99623, 8.99657, 8.99691, 8.99726, 8.9976, 8.99794, 8.99828, 8.99863, 8.99897, 8.99931, 8.99966, 9, 9.00034, 9.00069, 9.00103, 9.00137, 9.00171, 9.00206, 9.0024, 9.00274, 9.00308, 9.00343, 9.00377, 9.00411, 9.00445, 9.0048, 9.00514, 9.00548, 9.00582, 9.00617, 9.00651, 9.00685, 9.00719, 9.00754, 9.00788, 9.00822, 9.00856, 9.0089, 9.00925, 9.00959, 9.00993, 9.01027, 9.01061, 9.01095, 9.0113, 9.01164, 9.01198, 9.01232, 9.01266, 9.013, 9.01334, 9.01369, 9.01403, 9.01437, 9.01471, 9.01505, 9.01539, 9.01573, 9.01607, 9.01642, 9.01676, 9.0171, 9.01744, 9.01778, 9.01812, 9.01846, 9.0188, 9.01914, 9.01948, 9.01982, 9.02017, 9.02051, 9.02085, 9.02119, 9.02153, 9.02187, 9.02221, 9.02255, 9.02289, 9.02323, 9.02357, 9.02391, 9.02425, 9.02459, 9.02493, 9.02527, 9.02561, 9.02595, 9.02629, 9.02663, 9.02697, 9.02731, 9.02765, 9.02799, 9.02833, 9.02867, 9.02901, 9.02935, 9.02969, 9.03003, 9.03037, 9.03071, 9.03105, 9.03139, 9.03172, 9.03206, 9.0324, 9.03274, 9.03308, 9.03342, 9.03376, 9.0341, 9.03444, 9.03478, 9.03512, 9.03546, 9.03579, 9.03613, 9.03647, 9.03681, 9.03715, 9.03749, 9.03783, 9.03817, 9.0385, 9.03884, 9.03918, 9.03952, 9.03986, 9.0402, 9.04053, 9.04087, 9.04121, 9.04155, 9.04189, 9.04223, 9.04256, 9.0429, 9.04324, 9.04358, 9.04392, 9.04425, 9.04459, 9.04493, 9.04527, 9.04561, 9.04594, 9.04628, 9.04662, 9.04696, 9.04729, 9.04763, 9.04797, 9.04831, 9.04864, 9.04898, 9.04932, 9.04966, 9.04999, 9.05033, 9.05067, 9.05101, 9.05134, 9.05168, 9.05202, 9.05235, 9.05269, 9.05303, 9.05336, 9.0537, 9.05404, 9.05437, 9.05471, 9.05505, 9.05539, 9.05572, 9.05606, 9.0564, 9.05673, 9.05707, 9.0574, 9.05774, 9.05808, 9.05841, 9.05875, 9.05909, 9.05942, 9.05976, 9.0601, 9.06043, 9.06077, 9.0611, 9.06144, 9.06178, 9.06211, 9.06245, 9.06278, 9.06312, 9.06345, 9.06379, 9.06413, 9.06446, 9.0648, 9.06513, 9.06547, 9.0658, 9.06614, 9.06648, 9.06681, 9.06715, 9.06748, 9.06782, 9.06815, 9.06849, 9.06882, 9.06916, 9.06949, 9.06983, 9.07016, 9.0705, 9.07083, 9.07117, 9.0715, 9.07184, 9.07217, 9.07251, 9.07284, 9.07318, 9.07351, 9.07385, 9.07418, 9.07452, 9.07485, 9.07518, 9.07552, 9.07585, 9.07619, 9.07652, 9.07686, 9.07719, 9.07752, 9.07786, 9.07819, 9.07853, 9.07886, 9.0792, 9.07953, 9.07986, 9.0802, 9.08053, 9.08087, 9.0812, 9.08153, 9.08187, 9.0822, 9.08253, 9.08287, 9.0832, 9.08353, 9.08387, 9.0842, 9.08454, 9.08487, 9.0852, 9.08554, 9.08587, 9.0862, 9.08654, 9.08687, 9.0872, 9.08753, 9.08787, 9.0882, 9.08853, 9.08887, 9.0892, 9.08953, 9.08987, 9.0902, 9.09053, 9.09086, 9.0912, 9.09153, 9.09186, 9.0922, 9.09253, 9.09286, 9.09319, 9.09353, 9.09386, 9.09419, 9.09452, 9.09485, 9.09519, 9.09552, 9.09585, 9.09618, 9.09652, 9.09685, 9.09718, 9.09751, 9.09784, 9.09818, 9.09851, 9.09884, 9.09917, 9.0995, 9.09984, 9.10017, 9.1005, 9.10083, 9.10116, 9.10149, 9.10183, 9.10216, 9.10249, 9.10282, 9.10315, 9.10348, 9.10381, 9.10415, 9.10448, 9.10481, 9.10514, 9.10547, 9.1058, 9.10613, 9.10646, 9.10679, 9.10713, 9.10746, 9.10779, 9.10812, 9.10845, 9.10878, 9.10911, 9.10944, 9.10977, 9.1101, 9.11043, 9.11076, 9.11109, 9.11143, 9.11176, 9.11209, 9.11242, 9.11275, 9.11308, 9.11341, 9.11374, 9.11407, 9.1144, 9.11473, 9.11506, 9.11539, 9.11572, 9.11605, 9.11638, 9.11671, 9.11704, 9.11737, 9.1177, 9.11803, 9.11836, 9.11869, 9.11902, 9.11935, 9.11968, 9.12001, 9.12034, 9.12067, 9.12099, 9.12132, 9.12165, 9.12198, 9.12231, 9.12264, 9.12297, 9.1233, 9.12363, 9.12396, 9.12429, 9.12462, 9.12495, 9.12527, 9.1256, 9.12593, 9.12626, 9.12659, 9.12692, 9.12725, 9.12758, 9.12791, 9.12823, 9.12856, 9.12889, 9.12922, 9.12955, 9.12988, 9.13021, 9.13053, 9.13086, 9.13119, 9.13152, 9.13185, 9.13218, 9.1325, 9.13283, 9.13316, 9.13349, 9.13382, 9.13415, 9.13447, 9.1348, 9.13513, 9.13546, 9.13578, 9.13611, 9.13644, 9.13677, 9.1371, 9.13742, 9.13775, 9.13808, 9.13841, 9.13873, 9.13906, 9.13939, 9.13972, 9.14004, 9.14037, 9.1407, 9.14103, 9.14135, 9.14168, 9.14201, 9.14234, 9.14266, 9.14299, 9.14332, 9.14364, 9.14397, 9.1443, 9.14462, 9.14495, 9.14528, 9.14561, 9.14593, 9.14626, 9.14659, 9.14691, 9.14724, 9.14757, 9.14789, 9.14822, 9.14855, 9.14887, 9.1492, 9.14952, 9.14985, 9.15018, 9.1505, 9.15083, 9.15116, 9.15148, 9.15181, 9.15213, 9.15246, 9.15279, 9.15311, 9.15344, 9.15376, 9.15409, 9.15442, 9.15474, 9.15507, 9.15539, 9.15572, 9.15605, 9.15637, 9.1567, 9.15702, 9.15735, 9.15767, 9.158, 9.15832, 9.15865, 9.15898, 9.1593, 9.15963, 9.15995, 9.16028, 9.1606, 9.16093, 9.16125, 9.16158, 9.1619, 9.16223, 9.16255, 9.16288, 9.1632, 9.16353, 9.16385, 9.16418, 9.1645, 9.16483, 9.16515, 9.16548, 9.1658, 9.16613, 9.16645, 9.16677, 9.1671, 9.16742, 9.16775, 9.16807, 9.1684, 9.16872, 9.16905, 9.16937, 9.16969, 9.17002, 9.17034, 9.17067, 9.17099, 9.17132, 9.17164, 9.17196, 9.17229, 9.17261, 9.17293, 9.17326, 9.17358, 9.17391, 9.17423, 9.17455, 9.17488, 9.1752, 9.17553, 9.17585, 9.17617, 9.1765, 9.17682, 9.17714, 9.17747, 9.17779, 9.17811, 9.17844, 9.17876, 9.17908, 9.17941, 9.17973, 9.18005, 9.18038, 9.1807, 9.18102, 9.18134, 9.18167, 9.18199, 9.18231, 9.18264, 9.18296, 9.18328, 9.18361, 9.18393, 9.18425, 9.18457, 9.1849, 9.18522, 9.18554, 9.18586, 9.18619, 9.18651, 9.18683, 9.18715, 9.18748, 9.1878, 9.18812, 9.18844, 9.18877, 9.18909, 9.18941, 9.18973, 9.19005, 9.19038, 9.1907, 9.19102, 9.19134, 9.19166, 9.19199, 9.19231, 9.19263, 9.19295, 9.19327, 9.19359, 9.19392, 9.19424, 9.19456, 9.19488, 9.1952, 9.19552, 9.19585, 9.19617, 9.19649, 9.19681, 9.19713, 9.19745, 9.19777, 9.1981, 9.19842, 9.19874, 9.19906, 9.19938, 9.1997, 9.20002, 9.20034, 9.20066, 9.20099, 9.20131, 9.20163, 9.20195, 9.20227, 9.20259, 9.20291, 9.20323, 9.20355, 9.20387, 9.20419, 9.20451, 9.20483, 9.20516, 9.20548, 9.2058, 9.20612, 9.20644, 9.20676, 9.20708, 9.2074, 9.20772, 9.20804, 9.20836, 9.20868, 9.209, 9.20932, 9.20964, 9.20996, 9.21028, 9.2106, 9.21092, 9.21124, 9.21156, 9.21188, 9.2122, 9.21252, 9.21284, 9.21316, 9.21348, 9.2138, 9.21412, 9.21444, 9.21476, 9.21507, 9.21539, 9.21571, 9.21603, 9.21635, 9.21667, 9.21699, 9.21731, 9.21763, 9.21795, 9.21827, 9.21859, 9.21891, 9.21923, 9.21954, 9.21986, 9.22018, 9.2205, 9.22082, 9.22114, 9.22146, 9.22178, 9.2221, 9.22241, 9.22273, 9.22305, 9.22337, 9.22369, 9.22401, 9.22433, 9.22464, 9.22496, 9.22528, 9.2256, 9.22592, 9.22624, 9.22655, 9.22687, 9.22719, 9.22751, 9.22783, 9.22815, 9.22846, 9.22878, 9.2291, 9.22942, 9.22974, 9.23005, 9.23037, 9.23069, 9.23101, 9.23133, 9.23164, 9.23196, 9.23228, 9.2326, 9.23291, 9.23323, 9.23355, 9.23387, 9.23418, 9.2345, 9.23482, 9.23514, 9.23545, 9.23577, 9.23609, 9.23641, 9.23672, 9.23704, 9.23736, 9.23767, 9.23799, 9.23831, 9.23863, 9.23894, 9.23926, 9.23958, 9.23989, 9.24021, 9.24053, 9.24084, 9.24116, 9.24148, 9.24179, 9.24211, 9.24243, 9.24274, 9.24306, 9.24338, 9.24369, 9.24401, 9.24433, 9.24464, 9.24496, 9.24528, 9.24559, 9.24591, 9.24623, 9.24654, 9.24686, 9.24717, 9.24749, 9.24781, 9.24812, 9.24844, 9.24875, 9.24907, 9.24939, 9.2497, 9.25002, 9.25033, 9.25065, 9.25097, 9.25128, 9.2516, 9.25191, 9.25223, 9.25254, 9.25286, 9.25318, 9.25349, 9.25381, 9.25412, 9.25444, 9.25475, 9.25507, 9.25538, 9.2557, 9.25601, 9.25633, 9.25664, 9.25696, 9.25728, 9.25759, 9.25791, 9.25822, 9.25854, 9.25885, 9.25917, 9.25948, 9.2598, 9.26011, 9.26042, 9.26074, 9.26105, 9.26137, 9.26168, 9.262, 9.26231, 9.26263, 9.26294, 9.26326, 9.26357, 9.26389, 9.2642, 9.26451, 9.26483, 9.26514, 9.26546, 9.26577, 9.26609, 9.2664, 9.26671, 9.26703, 9.26734, 9.26766, 9.26797, 9.26828, 9.2686, 9.26891, 9.26923, 9.26954, 9.26985, 9.27017, 9.27048, 9.2708, 9.27111, 9.27142, 9.27174, 9.27205, 9.27236, 9.27268, 9.27299, 9.27331, 9.27362, 9.27393, 9.27425, 9.27456, 9.27487, 9.27519, 9.2755, 9.27581, 9.27613, 9.27644, 9.27675, 9.27706, 9.27738, 9.27769, 9.278, 9.27832, 9.27863, 9.27894, 9.27926, 9.27957, 9.27988, 9.28019, 9.28051, 9.28082, 9.28113, 9.28145, 9.28176, 9.28207, 9.28238, 9.2827, 9.28301, 9.28332, 9.28363, 9.28395, 9.28426, 9.28457, 9.28488, 9.2852, 9.28551, 9.28582, 9.28613, 9.28644, 9.28676, 9.28707, 9.28738, 9.28769, 9.288, 9.28832, 9.28863, 9.28894, 9.28925, 9.28956, 9.28988, 9.29019, 9.2905, 9.29081, 9.29112, 9.29143, 9.29175, 9.29206, 9.29237, 9.29268, 9.29299, 9.2933, 9.29362, 9.29393, 9.29424, 9.29455, 9.29486, 9.29517, 9.29548, 9.2958, 9.29611, 9.29642, 9.29673, 9.29704, 9.29735, 9.29766, 9.29797, 9.29828, 9.29859, 9.29891, 9.29922, 9.29953, 9.29984, 9.30015, 9.30046, 9.30077, 9.30108, 9.30139, 9.3017, 9.30201, 9.30232, 9.30263, 9.30295, 9.30326, 9.30357, 9.30388, 9.30419, 9.3045, 9.30481, 9.30512, 9.30543, 9.30574, 9.30605, 9.30636, 9.30667, 9.30698, 9.30729, 9.3076, 9.30791, 9.30822, 9.30853, 9.30884, 9.30915, 9.30946, 9.30977, 9.31008, 9.31039, 9.3107, 9.31101, 9.31132, 9.31163, 9.31194, 9.31225, 9.31256, 9.31287, 9.31318, 9.31348, 9.31379, 9.3141, 9.31441, 9.31472, 9.31503, 9.31534, 9.31565, 9.31596, 9.31627, 9.31658, 9.31689, 9.3172, 9.3175, 9.31781, 9.31812, 9.31843, 9.31874, 9.31905, 9.31936, 9.31967, 9.31998, 9.32029, 9.32059, 9.3209, 9.32121, 9.32152, 9.32183, 9.32214, 9.32245, 9.32275, 9.32306, 9.32337, 9.32368, 9.32399, 9.3243, 9.32461, 9.32491, 9.32522, 9.32553, 9.32584, 9.32615, 9.32645, 9.32676, 9.32707, 9.32738, 9.32769, 9.328, 9.3283, 9.32861, 9.32892, 9.32923, 9.32953, 9.32984, 9.33015, 9.33046, 9.33077, 9.33107, 9.33138, 9.33169, 9.332, 9.3323, 9.33261, 9.33292, 9.33323, 9.33353, 9.33384, 9.33415, 9.33446, 9.33476, 9.33507, 9.33538, 9.33569, 9.33599, 9.3363, 9.33661, 9.33691, 9.33722, 9.33753, 9.33784, 9.33814, 9.33845, 9.33876, 9.33906, 9.33937, 9.33968, 9.33998, 9.34029, 9.3406, 9.34091, 9.34121, 9.34152, 9.34183, 9.34213, 9.34244, 9.34274, 9.34305, 9.34336, 9.34366, 9.34397, 9.34428, 9.34458, 9.34489, 9.3452, 9.3455, 9.34581, 9.34612, 9.34642, 9.34673, 9.34703, 9.34734, 9.34765, 9.34795, 9.34826, 9.34856, 9.34887, 9.34918, 9.34948, 9.34979, 9.35009, 9.3504, 9.35071, 9.35101, 9.35132, 9.35162, 9.35193, 9.35223, 9.35254, 9.35285, 9.35315, 9.35346, 9.35376, 9.35407, 9.35437, 9.35468, 9.35498, 9.35529, 9.35559, 9.3559, 9.3562, 9.35651, 9.35681, 9.35712, 9.35743, 9.35773, 9.35804, 9.35834, 9.35865, 9.35895, 9.35926, 9.35956, 9.35987, 9.36017, 9.36047, 9.36078, 9.36108, 9.36139, 9.36169, 9.362, 9.3623, 9.36261, 9.36291, 9.36322, 9.36352, 9.36383, 9.36413, 9.36444, 9.36474, 9.36504, 9.36535, 9.36565, 9.36596, 9.36626, 9.36657, 9.36687, 9.36717, 9.36748, 9.36778, 9.36809, 9.36839, 9.36869, 9.369, 9.3693, 9.36961, 9.36991, 9.37021, 9.37052, 9.37082, 9.37113, 9.37143, 9.37173, 9.37204, 9.37234, 9.37264, 9.37295, 9.37325, 9.37355, 9.37386, 9.37416, 9.37447, 9.37477, 9.37507, 9.37538, 9.37568, 9.37598, 9.37629, 9.37659, 9.37689, 9.3772, 9.3775, 9.3778, 9.3781, 9.37841, 9.37871, 9.37901, 9.37932, 9.37962, 9.37992, 9.38023, 9.38053, 9.38083, 9.38113, 9.38144, 9.38174, 9.38204, 9.38235, 9.38265, 9.38295, 9.38325, 9.38356, 9.38386, 9.38416, 9.38446, 9.38477, 9.38507, 9.38537, 9.38567, 9.38598, 9.38628, 9.38658, 9.38688, 9.38718, 9.38749, 9.38779, 9.38809, 9.38839, 9.3887, 9.389, 9.3893, 9.3896, 9.3899, 9.39021, 9.39051, 9.39081, 9.39111, 9.39141, 9.39171, 9.39202, 9.39232, 9.39262, 9.39292, 9.39322, 9.39353, 9.39383, 9.39413, 9.39443, 9.39473, 9.39503, 9.39533, 9.39564, 9.39594, 9.39624, 9.39654, 9.39684, 9.39714, 9.39744, 9.39774, 9.39805, 9.39835, 9.39865, 9.39895, 9.39925, 9.39955, 9.39985, 9.40015, 9.40045, 9.40076, 9.40106, 9.40136, 9.40166, 9.40196, 9.40226, 9.40256, 9.40286, 9.40316, 9.40346, 9.40376, 9.40406, 9.40436, 9.40467, 9.40497, 9.40527, 9.40557, 9.40587, 9.40617, 9.40647, 9.40677, 9.40707, 9.40737, 9.40767, 9.40797, 9.40827, 9.40857, 9.40887, 9.40917, 9.40947, 9.40977, 9.41007, 9.41037, 9.41067, 9.41097, 9.41127, 9.41157, 9.41187, 9.41217, 9.41247, 9.41277, 9.41307, 9.41337, 9.41367, 9.41397, 9.41427, 9.41457, 9.41487, 9.41517, 9.41547, 9.41577, 9.41607, 9.41636, 9.41666, 9.41696, 9.41726, 9.41756, 9.41786, 9.41816, 9.41846, 9.41876, 9.41906, 9.41936, 9.41966, 9.41996, 9.42025, 9.42055, 9.42085, 9.42115, 9.42145, 9.42175, 9.42205, 9.42235, 9.42265, 9.42294, 9.42324, 9.42354, 9.42384, 9.42414, 9.42444, 9.42474, 9.42504, 9.42533, 9.42563, 9.42593, 9.42623, 9.42653, 9.42683, 9.42713, 9.42742, 9.42772, 9.42802, 9.42832, 9.42862, 9.42892, 9.42921, 9.42951, 9.42981, 9.43011, 9.43041, 9.4307, 9.431, 9.4313, 9.4316, 9.4319, 9.43219, 9.43249, 9.43279, 9.43309, 9.43339, 9.43368, 9.43398, 9.43428, 9.43458, 9.43487, 9.43517, 9.43547, 9.43577, 9.43606, 9.43636, 9.43666, 9.43696, 9.43725, 9.43755, 9.43785, 9.43815, 9.43844, 9.43874, 9.43904, 9.43934, 9.43963, 9.43993, 9.44023, 9.44052, 9.44082, 9.44112, 9.44142, 9.44171, 9.44201, 9.44231, 9.4426, 9.4429, 9.4432, 9.44349, 9.44379, 9.44409, 9.44439, 9.44468, 9.44498, 9.44528, 9.44557, 9.44587, 9.44617, 9.44646, 9.44676, 9.44706, 9.44735, 9.44765, 9.44794, 9.44824, 9.44854, 9.44883, 9.44913, 9.44943, 9.44972, 9.45002, 9.45032, 9.45061, 9.45091, 9.4512, 9.4515, 9.4518, 9.45209, 9.45239, 9.45268, 9.45298, 9.45328, 9.45357, 9.45387, 9.45416, 9.45446, 9.45476, 9.45505, 9.45535, 9.45564, 9.45594, 9.45623, 9.45653, 9.45682, 9.45712, 9.45742, 9.45771, 9.45801, 9.4583, 9.4586, 9.45889, 9.45919, 9.45948, 9.45978, 9.46007, 9.46037, 9.46067, 9.46096, 9.46126, 9.46155, 9.46185, 9.46214, 9.46244, 9.46273, 9.46303, 9.46332, 9.46362, 9.46391, 9.46421, 9.4645, 9.4648, 9.46509, 9.46539, 9.46568, 9.46598, 9.46627, 9.46656, 9.46686, 9.46715, 9.46745, 9.46774, 9.46804, 9.46833, 9.46863, 9.46892, 9.46922, 9.46951, 9.4698, 9.4701, 9.47039, 9.47069, 9.47098, 9.47128, 9.47157, 9.47186, 9.47216, 9.47245, 9.47275, 9.47304, 9.47334, 9.47363, 9.47392, 9.47422, 9.47451, 9.47481, 9.4751, 9.47539, 9.47569, 9.47598, 9.47627, 9.47657, 9.47686, 9.47716, 9.47745, 9.47774, 9.47804, 9.47833, 9.47862, 9.47892, 9.47921, 9.4795, 9.4798, 9.48009, 9.48038, 9.48068, 9.48097, 9.48126, 9.48156, 9.48185, 9.48214, 9.48244, 9.48273, 9.48302, 9.48332, 9.48361, 9.4839, 9.4842, 9.48449, 9.48478, 9.48508, 9.48537, 9.48566, 9.48595, 9.48625, 9.48654, 9.48683, 9.48713, 9.48742, 9.48771, 9.488, 9.4883, 9.48859, 9.48888, 9.48917, 9.48947, 9.48976, 9.49005, 9.49034, 9.49064, 9.49093, 9.49122, 9.49151, 9.49181, 9.4921, 9.49239, 9.49268, 9.49298, 9.49327, 9.49356, 9.49385, 9.49414, 9.49444, 9.49473, 9.49502, 9.49531, 9.4956, 9.4959, 9.49619, 9.49648, 9.49677, 9.49706, 9.49736, 9.49765, 9.49794, 9.49823, 9.49852, 9.49882, 9.49911, 9.4994, 9.49969, 9.49998, 9.50027, 9.50056, 9.50086, 9.50115, 9.50144, 9.50173, 9.50202, 9.50231, 9.5026, 9.5029, 9.50319, 9.50348, 9.50377, 9.50406, 9.50435, 9.50464, 9.50493, 9.50523, 9.50552, 9.50581, 9.5061, 9.50639, 9.50668, 9.50697, 9.50726, 9.50755, 9.50784, 9.50814, 9.50843, 9.50872, 9.50901, 9.5093, 9.50959, 9.50988, 9.51017, 9.51046, 9.51075, 9.51104, 9.51133, 9.51162, 9.51191, 9.5122, 9.5125, 9.51279, 9.51308, 9.51337, 9.51366 };
						static int n[8192] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8, 6, 9, 2, 4, 2, 11, 10, 2, 5, 5, 4, 3, 10, 9, 7, 8, 9, 3, 9, 11, 6, 11, 4, 8, 9, 4, 7, 9, 2, 7, 3, 8, 11, 3, 10, 6, 5, 9, 3, 7, 6, 7, 4, 7, 5, 3, 6, 11, 10, 7, 2, 4, 2, 8, 8, 6, 3, 10, 6, 8, 4, 2, 4, 11, 5, 10, 2, 9, 5, 7, 3, 2, 9, 4, 8, 4, 11, 2, 6, 5, 11, 10, 2, 7, 7, 11, 3, 8, 4, 5, 4, 8, 5, 5, 10, 3, 4, 6, 9, 2, 6, 3, 9, 7, 2, 9, 2, 6, 7, 2, 7, 4, 11, 2, 6, 7, 9, 5, 7, 5, 10, 10, 5, 7, 6, 2, 4, 7, 3, 6, 5, 3, 3, 8, 2, 7, 11, 3, 2, 5, 7, 9, 8, 8, 2, 4, 6, 3, 4, 6, 11, 5, 6, 6, 8, 11, 5, 4, 6, 2, 8, 7, 8, 2, 9, 6, 7, 4, 3, 8, 8, 5, 3, 7, 2, 4, 5, 6, 5, 6, 11, 9, 9, 4, 10, 5, 3, 2, 5, 6, 7, 3, 5, 2, 9, 10, 3, 8, 10, 5, 6, 5, 11, 2, 8, 2, 2, 6, 10, 6, 10, 9, 6, 8, 5, 4, 11, 5, 5, 11, 3, 6, 2, 11, 11, 3, 10, 11, 6, 6, 4, 7, 8, 11, 6, 3, 4, 5, 10, 5, 10, 2, 10, 5, 9, 4, 10, 2, 5, 6, 8, 11, 3, 7, 7, 7, 2, 5, 8, 9, 8, 9, 8, 9, 6, 5, 2, 7, 7, 10, 5, 7, 5, 10, 9, 11, 7, 2, 5, 3, 2, 5, 4, 7, 4, 5, 4, 3, 5, 6, 10, 10, 2, 6, 11, 11, 9, 4, 10, 5, 8, 6, 11, 6, 10, 7, 11, 2, 2, 9, 5, 8, 9, 8, 3, 4, 8, 2, 3, 4, 8, 9, 10, 3, 7, 4, 8, 7, 7, 6, 2, 8, 3, 5, 4, 2, 5, 7, 4, 8, 8, 10, 5, 7, 5, 3, 2, 11, 7, 11, 2, 6, 8, 2, 11, 6, 8, 3, 5, 8, 7, 5, 5, 10, 5, 6, 6, 11, 4, 5, 8, 9, 10, 10, 9, 11, 11, 4, 6, 7, 3, 2, 8, 4, 4, 3, 10, 2, 4, 6, 11, 11, 10, 5, 8, 6, 10, 8, 5, 6, 11, 2, 9, 6, 3, 9, 7, 4, 5, 4, 8, 2, 7, 8, 3, 10, 4, 11, 7, 11, 9, 10, 7, 2, 4, 10, 4, 3, 2, 6, 11, 7, 8, 11, 10, 4, 11, 4, 7, 2, 6, 7, 11, 9, 5, 5, 10, 2, 6, 2, 5, 9, 5, 8, 9, 11, 10, 3, 6, 9, 7, 10, 8, 10, 9, 4, 3, 6, 11, 11, 5, 10, 7, 5, 9, 3, 6, 6, 7, 5, 10, 11, 10, 11, 7, 6, 4, 4, 8, 6, 3, 4, 10, 3, 10, 9, 9, 10, 9, 9, 6, 5, 6, 8, 5, 9, 4, 9, 6, 4, 3, 9, 4, 3, 10, 4, 6, 2, 3, 8, 9, 8, 5, 9, 8, 11, 7, 2, 2, 9, 3, 11, 6, 2, 10, 3, 3, 9, 6, 3, 7, 10, 11, 2, 9, 2, 7, 7, 7, 2, 7, 11, 4, 11, 5, 7, 5, 6, 9, 5, 3, 8, 3, 2, 4, 4, 8, 10, 2, 6, 9, 5, 5, 6, 5, 8, 3, 11, 4, 11, 3, 3, 5, 11, 4, 2, 9, 2, 11, 4, 2, 11, 6, 6, 8, 2, 8, 4, 7, 9, 2, 6, 3, 5, 2, 9, 5, 8, 8, 2, 8, 6, 9, 7, 3, 11, 10, 10, 6, 11, 9, 3, 10, 9, 7, 9, 3, 6, 5, 3, 2, 5, 6, 7, 5, 4, 4, 2, 9, 9, 4, 4, 9, 8, 9, 11, 5, 2, 4, 11, 7, 11, 10, 7, 6, 5, 3, 11, 3, 6, 2, 3, 6, 5, 6, 6, 11, 6, 4, 9, 5, 6, 2, 10, 10, 9, 10, 4, 3, 11, 9, 2, 7, 11, 8, 4, 11, 10, 7, 9, 8, 8, 6, 2, 10, 6, 3, 9, 5, 7, 10, 10, 5, 9, 3, 7, 5, 9, 5, 11, 7, 11, 6, 7, 4, 3, 5, 4, 11, 3, 8, 4, 8, 10, 7, 11, 3, 6, 11, 10, 6, 2, 5, 8, 11, 4, 8, 3, 4, 3, 10, 10, 4, 11, 10, 11, 7, 8, 4, 5, 4, 11, 2, 10, 9, 11, 11, 6, 10, 8, 2, 3, 5, 9, 6, 2, 4, 5, 9, 7, 6, 3, 11, 4, 10, 9, 2, 7, 9, 9, 8, 4, 11, 3, 5, 9, 8, 2, 8, 5, 7, 11, 10, 10, 8, 9, 9, 2, 11, 3, 8, 3, 6, 9, 5, 9, 8, 7, 6, 8, 6, 4, 3, 6, 5, 10, 5, 2, 4, 4, 5, 2, 5, 3, 9, 3, 11, 2, 10, 5, 6, 4, 8, 8, 9, 4, 7, 5, 8, 5, 6, 4, 6, 8, 10, 9, 5, 5, 10, 7, 2, 10, 3, 5, 5, 5, 11, 3, 3, 11, 6, 11, 4, 2, 11, 6, 7, 3, 6, 7, 10, 8, 6, 5, 11, 10, 6, 5, 7, 11, 7, 5, 6, 7, 10, 6, 3, 8, 2, 11, 9, 10, 5, 2, 4, 5, 10, 10, 10, 2, 6, 5, 10, 11, 11, 3, 5, 10, 3, 9, 7, 6, 4, 3, 6, 11, 8, 9, 5, 7, 5, 5, 2, 6, 2, 8, 7, 11, 9, 3, 3, 7, 8, 9, 3, 2, 4, 4, 4, 10, 9, 11, 8, 4, 10, 8, 3, 10, 4, 6, 5, 5, 6, 2, 2, 6, 7, 10, 7, 3, 11, 7, 5, 9, 6, 8, 10, 6, 11, 7, 9, 4, 7, 6, 8, 6, 4, 7, 10, 4, 2, 9, 4, 2, 10, 11, 10, 8, 6, 4, 10, 10, 11, 11, 7, 8, 2, 11, 2, 7, 8, 8, 3, 5, 4, 2, 8, 11, 4, 4, 5, 5, 9, 6, 11, 11, 3, 7, 9, 4, 10, 11, 7, 8, 2, 6, 10, 6, 11, 5, 7, 3, 9, 2, 9, 10, 5, 10, 3, 2, 11, 3, 8, 5, 9, 10, 2, 7, 9, 6, 7, 5, 4, 4, 9, 10, 9, 10, 3, 6, 4, 7, 10, 10, 11, 6, 8, 8, 8, 7, 6, 11, 4, 9, 2, 9, 11, 6, 2, 8, 5, 2, 3, 5, 10, 2, 3, 4, 7, 11, 3, 5, 7, 11, 5, 10, 10, 2, 9, 7, 6, 3, 10, 5, 3, 2, 11, 4, 9, 5, 4, 8, 8, 8, 10, 10, 6, 6, 6, 8, 11, 7, 3, 8, 4, 5, 2, 11, 2, 3, 7, 8, 11, 2, 3, 8, 8, 6, 5, 6, 2, 9, 7, 11, 4, 7, 2, 6, 2, 5, 9, 3, 7, 3, 7, 6, 10, 8, 2, 10, 5, 2, 7, 5, 5, 4, 4, 11, 11, 9, 4, 5, 6, 9, 10, 4, 9, 10, 8, 2, 6, 7, 5, 7, 11, 3, 4, 6, 8, 6, 11, 6, 5, 8, 11, 11, 10, 10, 7, 10, 11, 2, 2, 2, 10, 6, 8, 3, 3, 8, 9, 7, 2, 8, 3, 7, 8, 3, 2, 2, 7, 9, 4, 10, 10, 4, 9, 3, 10, 7, 5, 11, 11, 4, 5, 9, 10, 5, 5, 9, 11, 10, 8, 4, 6, 5, 9, 4, 5, 5, 5, 6, 8, 10, 3, 5, 8, 7, 7, 7, 6, 5, 10, 2, 2, 11, 5, 11, 7, 8, 3, 11, 3, 5, 2, 4, 2, 2, 8, 8, 10, 2, 10, 3, 10, 6, 9, 3, 7, 5, 3, 2, 4, 9, 8, 11, 6, 2, 11, 2, 6, 9, 11, 2, 5, 11, 9, 3, 10, 6, 3, 2, 8, 4, 5, 10, 8, 8, 9, 3, 5, 7, 8, 9, 6, 9, 8, 11, 5, 10, 11, 10, 8, 7, 5, 6, 8, 4, 3, 8, 2, 6, 3, 10, 7, 2, 11, 3, 9, 3, 11, 9, 11, 8, 4, 9, 7, 6, 3, 2, 2, 2, 2, 3, 2, 3, 11, 8, 9, 11, 9, 5, 7, 8, 7, 8, 6, 5, 3, 3, 9, 10, 6, 11, 4, 4, 9, 7, 10, 7, 4, 11, 9, 11, 4, 2, 7, 5, 11, 5, 8, 6, 2, 4, 2, 11, 11, 3, 2, 3, 8, 10, 7, 2, 8, 9, 4, 9, 2, 7, 3, 4, 3, 10, 10, 10, 2, 8, 3, 3, 6, 6, 5, 4, 6, 6, 4, 7, 8, 4, 4, 11, 7, 8, 8, 7, 6, 5, 9, 6, 3, 6, 6, 11, 6, 11, 8, 2, 11, 4, 11, 3, 5, 10, 8, 7, 2, 3, 11, 4, 10, 11, 9, 8, 11, 9, 2, 7, 6, 11, 11, 9, 9, 2, 10, 7, 9, 8, 7, 7, 11, 6, 9, 2, 2, 10, 9, 7, 6, 10, 11, 3, 6, 8, 8, 8, 6, 10, 5, 7, 4, 5, 8, 9, 7, 5, 5, 6, 4, 4, 2, 5, 3, 10, 11, 3, 3, 7, 8, 6, 9, 11, 11, 8, 4, 6, 8, 6, 2, 8, 7, 8, 3, 9, 3, 7, 7, 6, 2, 5, 4, 9, 6, 6, 4, 3, 4, 4, 9, 9, 6, 4, 11, 5, 2, 9, 7, 2, 11, 6, 8, 2, 5, 4, 3, 6, 5, 2, 2, 9, 3, 4, 7, 9, 4, 8, 9, 6, 4, 6, 10, 8, 3, 10, 8, 9, 10, 10, 11, 11, 6, 11, 10, 11, 10, 2, 2, 11, 7, 3, 9, 7, 8, 9, 10, 7, 2, 8, 11, 10, 8, 3, 2, 7, 11, 3, 6, 5, 7, 7, 3, 4, 2, 2, 4, 9, 9, 5, 8, 9, 9, 9, 5, 7, 11, 10, 7, 10, 4, 3, 11, 7, 2, 4, 2, 11, 2, 10, 6, 3, 11, 11, 4, 7, 11, 2, 9, 7, 4, 8, 4, 9, 3, 5, 4, 7, 2, 10, 10, 6, 7, 8, 7, 11, 5, 4, 11, 4, 7, 7, 10, 8, 2, 10, 9, 5, 4, 8, 9, 3, 3, 2, 2, 10, 2, 9, 8, 5, 6, 5, 10, 6, 6, 8, 6, 8, 3, 8, 2, 6, 9, 10, 6, 4, 10, 10, 3, 8, 11, 2, 8, 11, 9, 2, 2, 2, 8, 5, 6, 2, 3, 9, 4, 8, 8, 6, 2, 6, 5, 9, 2, 4, 2, 4, 11, 7, 7, 8, 6, 3, 4, 10, 5, 10, 8, 5, 11, 8, 4, 7, 8, 11, 9, 6, 9, 9, 10, 5, 7, 3, 7, 2, 8, 11, 6, 2, 7, 6, 11, 9, 9, 4, 4, 10, 11, 10, 6, 4, 4, 3, 3, 9, 6, 5, 4, 6, 7, 9, 7, 7, 11, 11, 11, 5, 5, 7, 7, 8, 8, 11, 7, 6, 10, 11, 5, 8, 9, 2, 10, 5, 6, 10, 8, 2, 7, 9, 6, 3, 10, 9, 8, 2, 2, 10, 3, 4, 3, 7, 7, 2, 2, 2, 2, 4, 3, 10, 8, 10, 6, 10, 4, 9, 6, 9, 3, 9, 8, 2, 5, 5, 8, 11, 10, 11, 3, 6, 5, 4, 4, 5, 10, 6, 7, 3, 11, 3, 11, 5, 5, 9, 11, 6, 9, 10, 4, 6, 2, 9, 10, 11, 9, 2, 7, 8, 11, 2, 2, 2, 2, 8, 5, 11, 10, 9, 8, 7, 4, 8, 6, 6, 11, 10, 6, 8, 7, 8, 6, 4, 9, 3, 9, 6, 9, 3, 2, 9, 9, 3, 5, 5, 9, 11, 11, 10, 3, 11, 9, 11, 6, 6, 6, 9, 8, 5, 7, 2, 10, 4, 6, 11, 10, 8, 2, 9, 4, 5, 6, 7, 3, 2, 5, 5, 11, 4, 6, 5, 2, 10, 9, 11, 6, 8, 8, 7, 10, 6, 11, 9, 3, 6, 7, 5, 9, 11, 6, 4, 6, 3, 9, 4, 11, 3, 5, 10, 3, 9, 10, 4, 4, 2, 9, 8, 11, 2, 6, 10, 10, 2, 9, 8, 5, 4, 10, 6, 11, 11, 7, 5, 3, 6, 9, 8, 2, 4, 3, 3, 3, 6, 4, 5, 2, 5, 3, 11, 10, 6, 2, 3, 4, 4, 4, 2, 4, 3, 10, 5, 2, 2, 8, 8, 8, 6, 2, 3, 6, 6, 9, 6, 3, 5, 2, 8, 4, 3, 7, 3, 11, 6, 6, 9, 6, 11, 3, 4, 10, 2, 5, 7, 8, 9, 8, 11, 11, 8, 3, 10, 3, 5, 5, 3, 3, 2, 11, 3, 5, 10, 2, 7, 4, 10, 6, 8, 10, 10, 4, 10, 10, 5, 8, 3, 5, 7, 8, 5, 5, 3, 3, 6, 3, 3, 7, 2, 7, 8, 3, 5, 2, 10, 10, 9, 6, 10, 7, 5, 2, 8, 10, 7, 5, 8, 10, 3, 10, 7, 11, 9, 10, 6, 11, 6, 5, 10, 2, 8, 4, 7, 7, 6, 10, 5, 6, 5, 10, 2, 3, 2, 7, 11, 5, 3, 2, 2, 10, 2, 9, 2, 6, 6, 11, 4, 5, 10, 2, 7, 3, 3, 10, 7, 11, 5, 8, 7, 8, 4, 2, 6, 6, 9, 2, 4, 6, 8, 5, 3, 5, 11, 9, 3, 10, 11, 7, 10, 10, 5, 3, 4, 2, 6, 3, 11, 2, 6, 3, 7, 6, 6, 5, 8, 3, 11, 11, 11, 5, 4, 11, 9, 10, 9, 9, 10, 6, 10, 4, 2, 8, 6, 10, 10, 7, 11, 11, 3, 3, 11, 2, 3, 6, 3, 8, 6, 2, 10, 3, 6, 3, 5, 2, 5, 7, 11, 6, 2, 9, 9, 3, 8, 11, 2, 5, 11, 7, 10, 3, 6, 6, 9, 3, 5, 2, 6, 4, 11, 8, 4, 3, 11, 8, 3, 8, 7, 6, 8, 8, 8, 7, 7, 9, 4, 7, 10, 9, 6, 5, 9, 5, 3, 9, 4, 4, 10, 4, 2, 10, 2, 2, 8, 9, 6, 7, 7, 11, 8, 7, 8, 3, 4, 2, 6, 4, 10, 2, 4, 5, 8, 5, 6, 9, 2, 10, 7, 5, 6, 5, 5, 2, 9, 6, 5, 8, 2, 2, 4, 2, 2, 4, 6, 10, 7, 5, 6, 4, 6, 11, 3, 6, 4, 6, 9, 11, 7, 4, 7, 3, 7, 10, 5, 8, 7, 7, 3, 8, 8, 8, 2, 4, 9, 2, 3, 8, 3, 4, 2, 11, 11, 5, 4, 3, 8, 9, 8, 4, 4, 4, 7, 8, 10, 5, 10, 3, 4, 2, 6, 6, 4, 2, 4, 6, 2, 2, 6, 7, 5, 8, 8, 3, 5, 5, 11, 11, 2, 6, 5, 5, 3, 9, 9, 10, 7, 2, 11, 6, 3, 3, 6, 2, 5, 4, 2, 4, 10, 4, 11, 6, 7, 9, 11, 2, 3, 8, 3, 3, 8, 5, 3, 5, 10, 8, 11, 8, 2, 4, 11, 4, 2, 3, 10, 3, 10, 6, 9, 9, 4, 4, 7, 6, 3, 11, 10, 5, 5, 9, 5, 3, 7, 4, 8, 4, 2, 8, 6, 3, 7, 7, 3, 6, 11, 10, 4, 9, 4, 5, 11, 5, 11, 9, 3, 5, 3, 11, 4, 6, 2, 11, 2, 4, 10, 6, 5, 4, 4, 11, 5, 4, 4, 11, 3, 8, 6, 3, 8, 9, 10, 8, 7, 3, 9, 9, 11, 6, 8, 2, 4, 4, 3, 7, 2, 2, 5, 5, 2, 4, 11, 11, 5, 4, 2, 6, 9, 11, 5, 5, 10, 2, 4, 7, 3, 10, 2, 8, 4, 8, 8, 5, 4, 2, 9, 3, 2, 11, 2, 9, 10, 8, 6, 7, 11, 2, 11, 7, 8, 2, 3, 10, 4, 9, 7, 6, 11, 7, 10, 2, 7, 4, 2, 6, 8, 8, 5, 8, 3, 5, 7, 9, 9, 5, 2, 5, 2, 10, 9, 3, 2, 2, 6, 8, 11, 10, 3, 2, 3, 11, 4, 8, 5, 5, 8, 5, 2, 9, 10, 6, 5, 11, 7, 8, 11, 9, 9, 2, 6, 10, 3, 3, 2, 9, 5, 7, 9, 4, 10, 4, 2, 4, 10, 10, 7, 2, 2, 3, 3, 2, 7, 9, 3, 4, 7, 11, 2, 10, 10, 9, 5, 4, 11, 10, 11, 5, 7, 5, 10, 4, 11, 6, 3, 4, 7, 10, 11, 7, 11, 5, 11, 4, 5, 6, 10, 10, 2, 9, 7, 3, 3, 9, 11, 4, 7, 9, 5, 7, 10, 4, 7, 10, 3, 11, 3, 3, 2, 8, 10, 4, 10, 7, 6, 4, 4, 7, 4, 6, 6, 2, 3, 9, 8, 9, 9, 3, 7, 11, 10, 9, 9, 7, 4, 3, 5, 10, 10, 9, 11, 11, 3, 5, 3, 10, 2, 9, 10, 6, 11, 7, 10, 5, 9, 3, 11, 6, 4, 3, 3, 6, 3, 9, 10, 3, 7, 6, 4, 11, 3, 6, 8, 5, 7, 3, 3, 7, 10, 9, 4, 8, 2, 11, 7, 5, 6, 7, 2, 5, 4, 8, 9, 8, 5, 10, 6, 10, 5, 11, 7, 10, 3, 2, 8, 3, 6, 11, 2, 3, 8, 10, 8, 3, 6, 6, 8, 3, 6, 6, 3, 7, 5, 2, 6, 5, 7, 11, 2, 3, 6, 9, 8, 2, 5, 2, 2, 6, 9, 3, 2, 7, 2, 4, 10, 11, 7, 8, 2, 11, 2, 2, 3, 11, 2, 9, 6, 6, 2, 7, 8, 5, 11, 4, 2, 7, 11, 4, 5, 2, 11, 4, 3, 11, 10, 8, 3, 9, 9, 5, 9, 6, 10, 3, 3, 7, 3, 2, 8, 4, 3, 6, 8, 7, 2, 8, 2, 7, 9, 4, 5, 8, 6, 8, 5, 7, 3, 4, 11, 2, 7, 7, 6, 5, 6, 11, 9, 10, 4, 6, 11, 5, 7, 5, 9, 7, 4, 5, 2, 8, 5, 10, 6, 9, 3, 8, 6, 5, 2, 10, 6, 11, 8, 10, 10, 11, 9, 3, 10, 2, 8, 6, 3, 8, 11, 3, 5, 3, 11, 9, 11, 7, 9, 10, 3, 7, 8, 11, 3, 2, 10, 8, 11, 9, 2, 5, 8, 7, 8, 11, 11, 4, 4, 7, 2, 2, 4, 5, 10, 8, 11, 3, 2, 4, 10, 2, 8, 11, 3, 11, 4, 11, 3, 7, 4, 2, 8, 9, 4, 10, 5, 5, 8, 7, 7, 5, 3, 3, 8, 3, 6, 5, 2, 2, 4, 3, 3, 3, 5, 10, 7, 4, 4, 9, 6, 9, 7, 7, 9, 3, 3, 11, 3, 8, 5, 6, 10, 5, 2, 7, 3, 2, 5, 4, 6, 11, 6, 11, 9, 4, 4, 10, 6, 7, 8, 3, 8, 5, 7, 8, 6, 7, 8, 2, 6, 9, 6, 6, 2, 3, 4, 3, 3, 4, 8, 2, 11, 4, 4, 6, 5, 2, 8, 3, 2, 4, 9, 5, 8, 7, 4, 6, 8, 10, 11, 8, 11, 11, 3, 10, 8, 6, 10, 5, 5, 6, 4, 5, 4, 6, 7, 10, 8, 8, 3, 11, 3, 4, 11, 7, 7, 6, 2, 11, 7, 2, 5, 8, 5, 5, 9, 7, 10, 10, 4, 6, 4, 10, 2, 6, 7, 3, 10, 8, 5, 10, 11, 4, 5, 2, 5, 7, 8, 5, 9, 11, 5, 5, 8, 3, 8, 5, 2, 6, 6, 3, 8, 6, 9, 2, 2, 7, 8, 11, 9, 7, 8, 10, 6, 7, 9, 5, 3, 4, 4, 6, 8, 10, 9, 9, 2, 6, 9, 11, 4, 2, 2, 11, 2, 6, 10, 6, 7, 6, 4, 6, 6, 8, 7, 5, 9, 7, 4, 2, 4, 10, 2, 3, 6, 11, 2, 6, 4, 5, 3, 8, 2, 9, 9, 3, 5, 8, 8, 10, 7, 11, 7, 5, 6, 4, 5, 9, 2, 4, 4, 10, 8, 11, 9, 6, 4, 5, 5, 7, 2, 9, 2, 8, 7, 9, 6, 6, 6, 9, 8, 7, 4, 6, 4, 3, 11, 10, 8, 10, 4, 11, 2, 7, 3, 5, 3, 3, 4, 10, 5, 4, 3, 10, 2, 6, 2, 11, 6, 9, 4, 9, 2, 5, 6, 3, 2, 9, 11, 7, 2, 3, 4, 4, 5, 2, 6, 11, 6, 3, 8, 4, 5, 10, 2, 3, 11, 4, 6, 11, 2, 10, 4, 9, 8, 9, 11, 3, 11, 10, 8, 4, 4, 4, 9, 10, 8, 3, 4, 4, 9, 5, 7, 9, 5, 7, 5, 7, 6, 8, 2, 6, 11, 7, 2, 6, 2, 8, 7, 4, 10, 3, 10, 6, 4, 10, 9, 2, 2, 9, 11, 10, 5, 8, 7, 9, 11, 8, 5, 2, 8, 10, 6, 6, 8, 11, 4, 5, 6, 6, 10, 11, 4, 6, 6, 11, 3, 10, 7, 3, 4, 10, 3, 8, 10, 2, 11, 2, 7, 5, 6, 2, 8, 8, 9, 4, 4, 11, 4, 8, 8, 2, 5, 9, 4, 8, 4, 9, 3, 8, 11, 11, 9, 5, 3, 9, 10, 8, 3, 7, 3, 3, 9, 2, 11, 3, 5, 6, 6, 6, 4, 9, 3, 4, 8, 9, 9, 8, 9, 2, 8, 6, 2, 4, 8, 5, 3, 10, 4, 4, 2, 9, 4, 6, 5, 4, 10, 5, 4, 10, 8, 7, 6, 3, 3, 10, 10, 11, 2, 4, 9, 6, 5, 7, 6, 7, 8, 9, 6, 7, 11, 3, 11, 11, 4, 7, 5, 7, 3, 7, 8, 10, 11, 4, 3, 10, 4, 6, 7, 10, 5, 5, 6, 9, 10, 4, 3, 3, 3, 8, 11, 10, 3, 5, 7, 4, 11, 11, 8, 8, 7, 5, 5, 6, 3, 7, 4, 10, 4, 5, 7, 3, 5, 2, 6, 4, 2, 3, 8, 2, 4, 3, 9, 3, 3, 11, 2, 7, 3, 2, 2, 3, 2, 6, 4, 9, 7, 9, 8, 9, 2, 11, 5, 2, 7, 4, 10, 2, 2, 5, 8, 2, 8, 6, 8, 2, 4, 2, 10, 6, 10, 6, 5, 4, 8, 7, 2, 6, 3, 10, 3, 5, 6, 7, 6, 8, 11, 11, 2, 3, 10, 6, 5, 11, 8, 7, 6, 6, 4, 2, 2, 7, 3, 8, 2, 3, 10, 4, 7, 2, 9, 11, 8, 7, 10, 10, 3, 9, 6, 3, 11, 5, 2, 10, 5, 2, 5, 9, 11, 6, 10, 11, 10, 2, 6, 4, 10, 7, 10, 5, 2, 10, 8, 6, 9, 8, 7, 3, 10, 11, 3, 3, 8, 10, 11, 2, 9, 7, 5, 4, 8, 7, 10, 3, 2, 10, 2, 9, 2, 5, 4, 7, 5, 11, 10, 6, 10, 5, 11, 2, 9, 9, 5, 10, 11, 5, 5, 7, 11, 6, 2, 4, 2, 6, 7, 9, 9, 9, 10, 11, 3, 8, 11, 9, 7, 4, 11, 4, 8, 5, 9, 11, 11, 4, 8, 7, 10, 6, 11, 3, 9, 6, 6, 3, 8, 3, 11, 5, 7, 6, 6, 4, 6, 11, 9, 8, 4, 5, 5, 3, 5, 11, 9, 8, 5, 5, 5, 4, 11, 6, 10, 11, 6, 3, 2, 2, 9, 11, 7, 11, 6, 4, 10, 8, 11, 10, 8, 3, 4, 8, 2, 6, 4, 7, 11, 7, 2, 11, 7, 5, 10, 8, 2, 2, 7, 9, 5, 6, 6, 5, 10, 4, 10, 3, 11, 8, 10, 7, 10, 2, 11, 5, 8, 6, 7, 9, 6, 7, 8, 6, 11, 11, 2, 8, 10, 9, 4, 7, 4, 9, 2, 5, 7, 6, 6, 4, 5, 3, 2, 5, 10, 7, 3, 7, 5, 9, 5, 9, 7, 11, 3, 4, 9, 5, 3, 6, 9, 2, 2, 4, 4, 3, 7, 2, 7, 3, 5, 4, 2, 10, 6, 8, 2, 3, 2, 10, 4, 7, 8, 8, 8, 10, 11, 6, 2, 11, 6, 4, 5, 5, 7, 7, 9, 5, 10, 9, 10, 7, 5, 6, 2, 7, 11, 6, 2, 5, 2, 11, 9, 5, 5, 11, 5, 9, 10, 9, 6, 11, 2, 7, 5, 8, 5, 11, 8, 8, 9, 3, 7, 6, 5, 4, 8, 3, 4, 10, 11, 8, 5, 3, 5, 8, 3, 8, 5, 6, 11, 11, 4, 10, 4, 2, 8, 7, 10, 7, 7, 7, 3, 5, 2, 4, 8, 9, 2, 6, 10, 4, 6, 6, 3, 6, 9, 11, 10, 10, 8, 9, 11, 7, 5, 10, 2, 11, 5, 11, 4, 7, 8, 8, 7, 7, 9, 7, 4, 7, 11, 7, 4, 6, 10, 7, 4, 10, 8, 11, 3, 10, 3, 8, 9, 7, 6, 7, 6, 7, 9, 7, 2, 7, 10, 6, 8, 4, 5, 9, 3, 6, 11, 3, 2, 4, 6, 5, 11, 2, 4, 11, 6, 6, 4, 8, 11, 3, 9, 5, 5, 6, 2, 6, 3, 3, 8, 7, 11, 4, 8, 10, 4, 10, 11, 6, 5, 3, 7, 9, 9, 6, 5, 9, 3, 6, 4, 2, 4, 11, 3, 9, 6, 7, 10, 5, 10, 10, 6, 8, 9, 9, 7, 3, 4, 9, 10, 4, 8, 6, 11, 2, 8, 4, 8, 11, 9, 4, 7, 6, 10, 8, 5, 4, 6, 2, 11, 5, 10, 7, 10, 5, 4, 3, 4, 7, 10, 4, 11, 9, 10, 3, 5, 5, 3, 7, 3, 3, 5, 4, 11, 2, 7, 7, 6, 5, 2, 5, 2, 3, 4, 4, 8, 4, 10, 11, 10, 8, 9, 11, 6, 11, 2, 5, 8, 11, 11, 3, 10, 3, 3, 5, 5, 7, 3, 9, 5, 9, 8, 5, 4, 2, 7, 9, 9, 3, 7, 4, 10, 5, 4, 7, 10, 10, 4, 4, 11, 2, 6, 7, 3, 8, 3, 7, 8, 4, 5, 3, 8, 2, 8, 11, 9, 10, 8, 5, 4, 10, 5, 3, 10, 4, 11, 3, 9, 9, 5, 10, 7, 2, 2, 3, 4, 5, 5, 4, 11, 11, 10, 6, 6, 10, 6, 7, 8, 9, 2, 6, 6, 11, 4, 4, 7, 5, 2, 10, 2, 9, 2, 9, 8, 2, 11, 3, 2, 3, 9, 3, 8, 2, 11, 2, 9, 9, 3, 8, 8, 8, 4, 11, 7, 6, 10, 5, 10, 4, 10, 8, 6, 2, 2, 6, 2, 8, 7, 5, 11, 9, 10, 2, 4, 5, 5, 5, 2, 8, 2, 4, 4, 11, 7, 10, 5, 3, 3, 6, 9, 3, 4, 2, 4, 10, 11, 5, 4, 2, 8, 8, 2, 3, 7, 5, 4, 3, 2, 3, 6, 3, 11, 5, 4, 7, 7, 7, 5, 10, 8, 11, 7, 8, 7, 7, 7, 6, 3, 4, 4, 10, 7, 5, 3, 2, 5, 6, 6, 7, 8, 6, 9, 2, 11, 6, 8, 10, 2, 6, 8, 7, 10, 6, 6, 11, 7, 4, 11, 2, 3, 4, 11, 4, 11, 9, 9, 7, 10, 2, 11, 10, 11, 10, 7, 2, 10, 5, 6, 9, 6, 10, 2, 2, 4, 10, 2, 9, 3, 4, 10, 11, 3, 9, 8, 2, 10, 2, 10, 7, 4, 4, 7, 3, 3, 7, 10, 4, 6, 11, 11, 10, 8, 8, 10, 6, 4, 6, 2, 5, 11, 3, 2, 8, 5, 4, 8, 6, 9, 10, 5, 2, 11, 6, 9, 11, 3, 9, 7, 4, 5, 8, 8, 3, 8, 6, 4, 8, 11, 7, 3, 6, 10, 8, 4, 6, 11, 9, 4, 3, 7, 11, 7, 11, 4, 3, 10, 4, 11, 7, 2, 2, 3, 10, 11, 3, 3, 10, 2, 9, 9, 2, 6, 6, 2, 2, 9, 9, 11, 7, 5, 8, 6, 8, 7, 6, 3, 5, 5, 7, 2, 6, 6, 8, 4, 6, 8, 11, 3, 3, 6, 9, 6, 11, 2, 3, 6, 5, 6, 11, 4, 2, 8, 8, 3, 4, 3, 6, 9, 4, 2, 3, 6, 3, 7, 10, 2, 8, 9, 4, 11, 5, 8, 10, 4, 7, 4, 11, 2, 7, 2, 8, 8, 11, 2, 8, 11, 7, 7, 4, 10, 3, 9, 7, 5, 8, 10, 9, 10, 9, 2, 2, 5, 5, 4, 11, 11, 6, 6, 9, 6, 4, 8, 4, 10, 5, 9, 7, 2, 4, 11, 10, 6, 11, 7, 2, 7, 5, 4, 5, 10, 6, 10, 4, 8, 9, 9, 6, 5, 7, 5, 11, 6, 11, 4, 5, 7, 8, 2, 8, 8, 3, 2, 6, 4, 8, 7, 3, 4, 9, 7, 9, 3, 10, 7, 10, 6, 11, 3, 6, 2, 2, 7, 7, 7, 10, 5, 5, 5, 5, 3, 3, 10, 4, 2, 4, 10, 6, 4, 5, 10, 9, 3, 10, 9, 2, 7, 8, 9, 3, 2, 3, 10, 5, 3, 3, 6, 10, 3, 3, 8, 5, 10, 5, 6, 9, 2, 6, 10, 2, 11, 3, 6, 7, 3, 2, 6, 8, 7, 2, 10, 5, 10, 9, 2, 3, 2, 7, 4, 11, 8, 3, 9, 10, 10, 6, 7, 7, 9, 6, 8, 4, 5, 2, 5, 7, 3, 4, 4, 6, 6, 9, 10, 5, 6, 7, 4, 6, 8, 7, 7, 8, 4, 2, 2, 5, 2, 11, 9, 3, 6, 6, 8, 4, 5, 9, 4, 6, 8, 8, 9, 6, 2, 2, 9, 7, 7, 11, 7, 8, 3, 2, 4, 4, 4, 7, 7, 7, 5, 11, 2, 4, 2, 9, 2, 5, 9, 9, 5, 3, 6, 8, 7, 4, 8, 10, 4, 5, 7, 3, 6, 3, 4, 10, 10, 10, 5, 6, 6, 6, 3, 3, 7, 9, 4, 8, 4, 6, 10, 9, 4, 3, 6, 5, 4, 4, 5, 7, 6, 10, 4, 8, 10, 11, 8, 11, 11, 7, 3, 5, 9, 5, 9, 8, 4, 10, 5, 9, 3, 6, 10, 5, 10, 10, 9, 11, 4, 7, 9, 3, 10, 10, 5, 9, 10, 3, 2, 4, 11, 10, 9, 4, 4, 10, 7, 2, 4, 3, 3, 4, 9, 8, 3, 6, 8, 3, 7, 10, 11, 5, 6, 2, 4, 2, 2, 2, 6, 3, 7, 9, 4, 5, 2, 9, 10, 9, 5, 10, 2, 5, 6, 9, 2, 7, 5, 7, 6, 2, 5, 6, 6, 3, 2, 4, 10, 2, 6, 9, 7, 7, 5, 4, 11, 4, 6, 10, 3, 4, 2, 7, 8, 9, 6, 8, 9, 11, 4, 7, 5, 3, 7, 11, 3, 8, 8, 4, 6, 10, 11, 8, 9, 6, 9, 3, 6, 8, 2, 5, 10, 8, 6, 7, 6, 6, 2, 6, 7, 2, 8, 3, 2, 11, 11, 10, 11, 11, 3, 3, 2, 7, 10, 2, 10, 3, 3, 2, 6, 5, 8, 4, 5, 10, 9, 8, 3, 4, 9, 9, 3, 4, 9, 5, 6, 7, 5, 8, 9, 6, 10, 6, 4, 10, 2, 7, 5, 2, 5, 7, 5, 10, 9, 5, 4, 9, 5, 4, 3, 11, 9, 3, 3, 4, 7, 9, 3, 3, 10, 7, 8, 9, 2, 6, 9, 6, 3, 10, 9, 11, 11, 5, 8, 9, 5, 9, 9, 7, 7, 3, 9, 11, 3, 3, 9, 11, 8, 7, 9, 4, 6, 5, 4, 8, 6, 8, 2, 8, 5, 6, 5, 3, 2, 11, 5, 2, 3, 6, 2, 10, 3, 10, 4, 7, 4, 6, 6, 7, 6, 5, 8, 8, 8, 5, 4, 6, 7, 4, 9, 6, 5, 2, 8, 10, 8, 4, 11, 8, 9, 5, 5, 7, 9, 4, 11, 8, 5, 9, 6, 8, 8, 5, 5, 6, 9, 4, 5, 8, 5, 11, 2, 11, 11, 5, 8, 9, 3, 9, 4, 7, 3, 8, 9, 7, 5, 9, 7, 3, 5, 9, 11, 2, 3, 6, 2, 2, 8, 6, 11, 11, 9, 2, 4, 10, 3, 2, 6, 10, 9, 5, 7, 11, 11, 6, 3, 6, 2, 7, 6, 7, 8, 2, 10, 10, 10, 9, 2, 3, 10, 9, 6, 4, 11, 4, 7, 5, 9, 11, 4, 4, 4, 9, 6, 5, 7, 10, 5, 10, 2, 2, 3, 5, 6, 7, 4, 5, 11, 2, 4, 6, 8, 8, 4, 2, 4, 11, 3, 2, 5, 9, 9, 8, 3, 10, 2, 8, 2, 11, 4, 8, 8, 5, 8, 7, 7, 2, 2, 4, 8, 9, 2, 7, 9, 2, 7, 11, 2, 11, 9, 9, 2, 5, 10, 9, 7, 5, 10, 11, 3, 2, 5, 4, 8, 2, 8, 3, 6, 5, 11, 6, 8, 9, 11, 7, 10, 5, 6, 8, 3, 8, 8, 5, 8, 6, 5, 7, 5, 4, 7, 8, 2, 5, 5, 11, 6, 5, 8, 9, 5, 4, 7, 3, 5, 5, 2, 7, 10, 7, 11, 6, 4, 10, 4, 5, 8, 2, 9, 6, 10, 5, 8, 4, 11, 2, 9, 7, 3, 9, 4, 2, 11, 8, 7, 10, 7, 6, 10, 5, 10, 4, 11, 9, 4, 2, 6, 4, 3, 11, 8, 3, 5, 10, 8, 2, 5, 6, 11, 5, 9, 8, 10, 3, 8, 7, 6, 5, 5, 7, 5, 6, 4, 5, 6, 4, 4, 3, 10, 4, 10, 3, 7, 6, 5, 5, 9, 10, 8, 10, 3, 7, 4, 8, 7, 2, 8, 9, 8, 9, 8, 9, 8, 5, 3, 8, 3, 11, 7, 9, 2, 3, 4, 8, 9, 8, 6, 3, 6, 7, 9, 8, 7, 9, 3, 2, 11, 9, 8, 6, 5, 4, 10, 5, 7, 3, 5, 11, 7, 3, 7, 10, 4, 9, 6, 6, 6, 3, 8, 2, 7, 8, 5, 4, 5, 2, 10, 7, 10, 10, 3, 6, 10, 8, 3, 7, 3, 9, 4, 4, 6, 6, 10, 11, 2, 7, 3, 6, 11, 9, 10, 5, 6, 5, 8, 11, 7, 6, 11, 8, 6, 7, 2, 11, 4, 8, 5, 10, 11, 11, 2, 6, 3, 7, 2, 3, 6, 9, 9, 10, 4, 5, 2, 11, 10, 2, 7, 5, 2, 7, 5, 2, 8, 4, 4, 11, 7, 7, 11, 6, 11, 3, 7, 7, 2, 9, 2, 2, 10, 7, 4, 8, 4, 7, 9, 2, 10, 5, 9, 9, 8, 2, 2, 4, 5, 10, 2, 7, 9, 9, 11, 10, 5, 8, 2, 6, 6, 5, 2, 10, 2, 9, 6, 11, 2, 8, 4, 9, 2, 11, 7, 9, 3, 2, 2, 2, 4, 11, 6, 10, 8, 2, 5, 5, 10, 7, 8, 3, 8, 2, 2, 4, 3, 10, 8, 2, 3, 9, 3, 6, 4, 7, 8, 11, 3, 11, 7, 11, 2, 3, 3, 3, 11, 5, 4, 8, 9, 8, 3, 10, 7, 10, 6, 6, 11, 6, 11, 2, 6, 11, 5, 2, 10, 2, 9, 10, 3, 3, 3, 11, 3, 10, 8, 7, 5, 3, 2, 11, 7, 8, 10, 6, 4, 11, 10, 2, 5, 8, 2, 3, 6, 8, 8, 3, 8, 8, 2, 11, 9, 9, 5, 7, 5, 2, 9, 5, 5, 11, 5, 2, 2, 9, 10, 4, 8, 11, 9, 7, 9, 6, 3, 7, 9, 8, 4, 9, 9, 3, 6, 2, 3, 9, 11, 9, 8, 7, 4, 9, 6, 7, 8, 3, 10, 5, 11, 4, 7, 7, 2, 7, 2, 7, 4, 8, 9, 10, 3, 4, 9, 2, 6, 9, 8, 9, 9, 4, 8, 3, 5, 2, 11, 3, 9, 3, 6, 5, 5, 9, 8, 2, 9, 7, 6, 7, 2, 8, 8, 8, 11, 11, 7, 9, 3, 8, 6, 5, 9, 11, 5, 2, 3, 7, 5, 3, 9, 5, 4, 6, 2, 9, 8, 7, 11, 5, 8, 9, 11, 2, 10, 7, 2, 6, 7, 5, 7, 8, 8, 8, 5, 7, 2, 8, 4, 8, 7, 5, 8, 9, 8, 4, 10, 9, 2, 11, 10, 5, 8, 4, 9, 11, 5, 6, 4, 10, 7, 9, 2, 5, 4, 11, 9, 5, 6, 11, 11, 10, 9, 8, 9, 9, 9, 11, 2, 10, 4, 11, 2, 6, 11, 10, 3, 2, 3, 5, 5, 10, 9, 9, 10, 8, 3, 10, 6, 10, 9, 7, 2, 5, 11, 5, 7, 4, 10, 9, 7, 6, 7, 2, 2, 4, 5, 5, 6, 7, 6, 10, 5, 8, 8, 9, 8, 4, 11, 3, 4, 6, 6, 3, 5, 9, 2, 11, 7, 9, 9, 6, 2, 9, 5, 6, 7, 7, 3, 2, 6, 3, 10, 7, 6, 2, 5, 11, 2, 2, 8, 11, 11, 10, 9, 11, 11, 6, 10, 5, 7, 5, 9, 8, 9, 4, 9, 4, 7, 11, 2, 4, 10, 2, 3, 6, 11, 5, 3, 7, 3, 11, 8, 9, 6, 4, 9, 3, 10, 11, 3, 9, 5, 6, 5, 11, 10, 4, 8, 4, 2, 7, 3, 10, 10, 2, 2, 9, 5, 4, 2, 4, 8, 11, 7, 4, 2, 8, 5, 8, 8, 11, 5, 5, 3, 4, 7, 6, 8};
						int an[8192];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
				}
			}
		}
	}
	system("pause");
	outputFile.close();
}

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type) {

	/*
	This subroutine determines how many additional runs each
	design will should have for next iteration of simulation.
	s_mean[i]: sample mean of design i, i=0,1,..,ND-1
	s_var[i]: sample variance of design i, i=0,1,..,ND-1
	nd: the number of designs
	n[i]: number of simulation replications of design i,
	i=0,1,..,ND-1
	add_budget: the additional simulation budget
	an[i]: additional number of simulation replications assigned
	to design i, i=0,1,..,ND-1
	type: type of optimazition problem. type=1, MIN problem;
	type=2, MAX problem
	*/
	int i;
	int b, s;
	int t_budget, t1_budget;
	int morerun[8192], more_alloc; /* 1:Yes; 0:No */
	float t_s_mean[8192];
	float ratio[8192]; /* Ni/Ns */
	float ratio_s, temp;
	if (type == 1) { /*MIN problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = s_mean[i];
	}
	else { /*MAX problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = (-1)*s_mean[i];
	}
	t_budget = add_budget;
	for (i = 0; i < nd; i++) t_budget += n[i];
	b = best(t_s_mean, nd);
	s = second_best(t_s_mean, nd, b);
	ratio[s] = 1.0;
	for (i = 0; i < nd; i++)
		if (i != s && i != b) {
			temp = (t_s_mean[b] - t_s_mean[s]) / (t_s_mean[b] - t_s_mean[i]);
			ratio[i] = temp*temp*s_var[i] / s_var[s];
		} /* calculate ratio of Ni/Ns*/
	temp = 0;
	for (i = 0; i < nd; i++) if (i != b) temp += (ratio[i] * ratio[i] / s_var[i]);
	ratio[b] = sqrt(s_var[b] * temp); /* calculate Nb */
	for (i = 0; i < nd; i++) morerun[i] = 1;
	t1_budget = t_budget;
	do{
		more_alloc = 0;
		ratio_s = 0.0;
		for (i = 0; i < nd; i++) if (morerun[i]) ratio_s += ratio[i];
		for (i = 0; i < nd; i++) if (morerun[i])
		{
			an[i] = (int)(t1_budget / ratio_s*ratio[i]);
			/* disable those design which have been run too much */
			if (an[i] < n[i])
			{
				an[i] = n[i];
				morerun[i] = 0;
				more_alloc = 1;
			}
		}
		if (more_alloc)
		{
			t1_budget = t_budget;
			for (i = 0; i < nd; i++) if (!morerun[i]) t1_budget -= an[i];
		}
	} while (more_alloc); /* end of WHILE */
	/* calculate the difference */
	t1_budget = an[0];
	for (i = 1; i < nd; i++) t1_budget += an[i];
	an[b] += (t_budget - t1_budget); /* give the difference
									 to design b */
	for (i = 0; i < nd; i++) an[i] -= n[i];
}

int best(float t_s_mean[], int nd) {
	/*This function determines the best design based on current
	simulation results */
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs */

	int i, min_index;
	min_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[min_index]) {
			min_index = i;
		}
	}
	return min_index;
}

int second_best(float t_s_mean[], int nd, int b) {
	/*This function determines the second best design based on
	current simulation results*/
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs.
	b: current best design determined by function
	best() */
	int i, second_index;
	if (b == 0) second_index = 1;
	else second_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[second_index] && i != b) {
			second_index = i;
		}
	}
	return second_index;
}

