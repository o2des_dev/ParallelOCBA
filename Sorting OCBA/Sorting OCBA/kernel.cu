/*
* Parallel bitonic sort using CUDA.
* Compile with
* nvcc -arch=sm_11 bitonic_sort.cu
* Based on http://www.tools-of-computing.com/tc/CS/Sorts/bitonic_sort.htm
* License: BSD 3
*/

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <cuda.h>
#include <fstream>

void print_elapsed(clock_t start, clock_t stop)
{
	double elapsed = ((double)(stop - start)) / CLOCKS_PER_SEC;
	printf("Elapsed time: %.3fs\n", elapsed);
}

float random_float()
{
	return (float)rand() / (float)RAND_MAX;
}

void array_print(float *arr, int length)
{
	int i;
	for (i = 0; i < length; ++i) {
		printf("%1.3f ", arr[i]);
	}
	printf("\n");
}

void array_fill(float *arr, long length)
{
	srand(time(NULL));
	int i;
	for (i = 0; i < length; ++i) {
		arr[i] = random_float();
	}
}

__global__ void bitonic_sort_step(float *dev_values, int j, int k)
{
	unsigned int i, ixj; /* Sorting partners: i and ixj */
	i = threadIdx.x + blockDim.x * blockIdx.x;
	ixj = i^j;

	/* The threads with the lowest ids sort the array. */
	if ((ixj)>i) {
		if ((i&k) == 0) {
			/* Sort ascending */
			if (dev_values[i]>dev_values[ixj]) {
				/* exchange(i,ixj); */
				float temp = dev_values[i];
				dev_values[i] = dev_values[ixj];
				dev_values[ixj] = temp;
			}
		}
		if ((i&k) != 0) {
			/* Sort descending */
			if (dev_values[i]<dev_values[ixj]) {
				/* exchange(i,ixj); */
				float temp = dev_values[i];
				dev_values[i] = dev_values[ixj];
				dev_values[ixj] = temp;
			}
		}
	}
}

/**
* Inplace bitonic sort using CUDA.
*/
void bitonic_sort(float *values, int num_val, int blockCount, int threadCount)
{
	float *dev_values;
	size_t size = num_val * sizeof(float);

	cudaMalloc((void**)&dev_values, size);
	cudaMemcpy(dev_values, values, size, cudaMemcpyHostToDevice);

	dim3 blocks(blockCount, 1);    /* Number of blocks   */
	dim3 threads(threadCount, 1);  /* Number of threads  */

	int j, k;
	/* Major step */
	for (k = 2; k <= num_val; k <<= 1) {
		/* Minor step */
		for (j = k >> 1; j>0; j = j >> 1) {
			bitonic_sort_step << <blocks, threads >> >(dev_values, j, k);
		}
	}
	cudaMemcpy(values, dev_values, size, cudaMemcpyDeviceToHost);
	cudaFree(dev_values);
}

// CPU implementation of Merge Sort

void merge(float arr[], int l, int m, int r)
{
	int i, j, k;
	const int n1 = m - l + 1;
	const int n2 = r - m;

	/* create temp arrays */
	float *L = (float*)malloc(n1 * sizeof(float));
	float *R = (float*)malloc(n2 * sizeof(float));

	/* Copy data to temp arrays L[] and R[] */
	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + 1 + j];

	/* Merge the temp arrays back into arr[l..r]*/
	i = 0; // Initial index of first subarray
	j = 0; // Initial index of second subarray
	k = l; // Initial index of merged subarray
	while (i < n1 && j < n2)
	{
		if (L[i] <= R[j])
		{
			arr[k] = L[i];
			i++;
		}
		else
		{
			arr[k] = R[j];
			j++;
		}
		k++;
	}

	/* Copy the remaining elements of L[], if there
	are any */
	while (i < n1)
	{
		arr[k] = L[i];
		i++;
		k++;
	}

	/* Copy the remaining elements of R[], if there
	are any */
	while (j < n2)
	{
		arr[k] = R[j];
		j++;
		k++;
	}
}

/* l is for left index and r is right index of the
sub-array of arr to be sorted */
void mergeSort(float arr[], int l, int r)
{
	if (l < r)
	{
		// Same as (l+r)/2, but avoids overflow for
		// large l and h
		int m = l + (r - l) / 2;

		// Sort first and second halves
		mergeSort(arr, l, m);
		mergeSort(arr, m + 1, r);

		merge(arr, l, m, r);
	}
}

int main(void)
{
	std::ofstream outputFile;
	outputFile.open("results.csv");
	for (int m = 0; m < 4; m++) {
		int blockcount = 1000;
		int threadcount[9] = {4, 8, 16, 32, 64, 128, 256, 512, 1024};
		long num_val;
		for (int j = 0; j < 9; j++) {
			clock_t start, stop;
			num_val = blockcount * threadcount[j];

			float *values = (float*)malloc(num_val * sizeof(float));
			array_fill(values, num_val);
			float *valuesDuplicate = values;

			start = clock();
			bitonic_sort(values, num_val, blockcount, threadcount[j]); /* Inplace */
			stop = clock();
			print_elapsed(start, stop);
			outputFile << "GPU" << "," << (1000 * (stop - start) / CLOCKS_PER_SEC) << "\n";

			start = clock();
			mergeSort(valuesDuplicate, 0, num_val - 1);
			stop = clock();
			print_elapsed(start, stop);
			outputFile << "CPU" << "," << (1000 * (stop - start) / CLOCKS_PER_SEC) << "\n";
		}
	}
	outputFile.close();

	return 0;
}