#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <cuda.h>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include <fstream>
#include <random>
#include <time.h>

/* please put the following definitions at the head of your
program */
/*Define parameters for simulation*/
/*the number of designs. It should be larger than 2.*/
#define GRIDDIMENSION 1
#define BLOCKDIMENSION 32
#define TYPE 1
/*maximization TYPE = -1, minimization TYPE = 1*/
/*prototypes of functions and subroutines included in this
file*/

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type);
int best(float t_s_mean[], int nd);
int second_best(float t_s_mean[], int nd, int b);

#ifdef _WIN32
#include <Windows.h>
double get_wall_time(){
	LARGE_INTEGER time, freq;
	if (!QueryPerformanceFrequency(&freq)){
		//  Handle error
		return 0;
	}
	if (!QueryPerformanceCounter(&time)){
		//  Handle error
		return 0;
	}
	return (double)time.QuadPart / freq.QuadPart;
}

double get_cpu_time(){
	FILETIME a, b, c, d;
	if (GetProcessTimes(GetCurrentProcess(), &a, &b, &c, &d) != 0){
		//  Returns total user time.
		return
			(double)(d.dwLowDateTime |
			((unsigned long long)d.dwHighDateTime << 32)) * 0.0000001;
	}
	else{
		//  Handle error
		return 0;
	}
}
#endif


__device__ inline void atomicFloatSub(float *address, float val) {
	int i_val = __float_as_int(val);
	int tmp0 = 0;
	int tmp1;

	while ((tmp1 = atomicCAS((int *)address, tmp0, i_val)) != tmp0)	{
		tmp0 = tmp1;
		i_val = __float_as_int(__int_as_float(tmp1) - val);
	}
}

__global__ void parallelOCBA3(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	float temp = 0;
	unsigned int i = 0;

	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//use previous alpha of best
		if (lamda[index] == 0) {
			lamda[index] = *alphaB;
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//use previous rho
		*rho = *rhoPrevious;
		d_t[1] = d_t[0];
		//	__syncthreads();

		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];
	}
}

__global__ void parallelOCBA_1(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 2: find maximum
		*best = -FLT_MAX;
		float temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		unsigned int i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		*alphaB = *best;

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rhoPrevious = d_t[0] / *summation;
	}
}

__global__ void parallelOCBA2(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	float temp = 0;
	unsigned int i = 0;

	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//use previous alpha of best
		if (lamda[index] == 0) {
			lamda[index] = *alphaB;
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//use previous rho
		*rho = *rhoPrevious;
		d_t[1] = d_t[0];
		//	__syncthreads();

		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];

		//step 2: find maximum
		*best = -FLT_MAX;
		temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rho = d_t[0] / *summation;
	}
}

__global__ void parallelOCBA1(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 2: find maximum
		*best = -FLT_MAX;
		float temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		unsigned int i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rho = d_t[0] / *summation;
		d_t[1] = d_t[0];
		//	__syncthreads();


		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];
	}
}

void simulatorEmulator(int an[], int nd, double xToAdd[], double xSquareToAdd[]) {
	//test case: i=0 is best design
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		for (int j = 0; j < an[i]; j++) {
			std::default_random_engine generator;
			std::normal_distribution<double> distribution(-900 + (100 * i), 50);
			temp = distribution(generator);
			xToAdd[i] += temp;
			xSquareToAdd[i] += (temp*temp);
		}
	}
	return;
}

void updateMeanVariance(int nd, int an[], int n[], float mean[], float var[], double xCurrent[], double xSquareCurrent[], double xToAdd[], double xSquareToAdd[]) {
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		mean[i] = (n[i] * mean[i] + xCurrent[i]) / (n[i] + an[i]);
		xCurrent[i] += xToAdd[i];
		xSquareCurrent[i] += xSquareToAdd[i];
		var[i] = (xSquareCurrent[i] - (xCurrent[i] * xCurrent[i] / (n[i] + an[i]))) / (n[i] + an[i] - 1);
	}
	return;
}



void main()
{
	srand(time(NULL));
	std::ofstream outputFile;
	outputFile.open("results.csv");
	int budget[1] = { 1000 };
	int candidates[10] = { 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192 };
	//int budget[6] = { 10, 100, 1000, 10000, 100000, 1000000 };
	for (int k = 0; k < 1; k++) {
		int ADD_BUDGET = budget[k]; /*the additional simulation
									budget. It should be positive integer.*/
		outputFile << std::endl << "Budget" << "," << budget[k] << std::endl;
		for (int j = 0; j < 50; j++) {
			for (int l = 0; l < 10; l++) {
				outputFile << std::endl << "Candidate" << "," << candidates[l] << std::endl;
				for (int k = 0; k < 4; k++) {
					outputFile << std::endl << "Config" << "," << k << std::endl;

					int i;

					if (l == 0) {
						float s_mean[16] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
						float s_var[16] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024 };
						int n[16] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[16];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 1) {
						float s_mean[32] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 };
						float s_var[32] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024, 1191.58, 1374.62, 1573.56, 1788.85, 2020.92, 2270.16, 2536.99, 2821.81, 3125, 3446.94, 3788, 4148.54, 4528.92, 4929.5, 5350.62, 5792.62 };
						int n[32] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[32];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 2) {
						float s_mean[64] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64 };
						float s_var[64] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024, 1191.58, 1374.62, 1573.56, 1788.85, 2020.92, 2270.16, 2536.99, 2821.81, 3125, 3446.94, 3788, 4148.54, 4528.92, 4929.5, 5350.62, 5792.62, 6255.83, 6740.58, 7247.2, 7776, 8327.3, 8901.41, 9498.64, 10119.3, 10763.7, 11432, 12124.7, 12842, 13584.1, 14351.4, 15144.1, 15962.6, 16807, 17677.7, 18574.9, 19498.8, 20449.8, 21428.1, 22434, 23467.7, 24529.4, 25619.5, 26738.1, 27885.5, 29061.9, 30267.7, 31503, 32768 };
						int n[64] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[64];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 3) {
						float s_mean[128] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128 };
						float s_var[128] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024, 1191.58, 1374.62, 1573.56, 1788.85, 2020.92, 2270.16, 2536.99, 2821.81, 3125, 3446.94, 3788, 4148.54, 4528.92, 4929.5, 5350.62, 5792.62, 6255.83, 6740.58, 7247.2, 7776, 8327.3, 8901.41, 9498.64, 10119.3, 10763.7, 11432, 12124.7, 12842, 13584.1, 14351.4, 15144.1, 15962.6, 16807, 17677.7, 18574.9, 19498.8, 20449.8, 21428.1, 22434, 23467.7, 24529.4, 25619.5, 26738.1, 27885.5, 29061.9, 30267.7, 31503, 32768, 34063, 35388.3, 36744, 38130.5, 39547.8, 40996.3, 42476.2, 43987.7, 45531, 47106.3, 48713.9, 50354, 52026.8, 53732.4, 55471.2, 57243.3, 59049, 60888.4, 62761.8, 64669.3, 66611.2, 68587.7, 70598.9, 72645.2, 74726.6, 76843.3, 78995.7, 81183.8, 83407.9, 85668.2, 87964.8, 90298, 92667.9, 95074.7, 97518.7, 100000, 102519, 105075, 107670, 110302, 112973, 115682, 118429, 121216, 124041, 126906, 129810, 132753, 135736, 138759, 141822, 144926, 148069, 151253, 154478, 157744, 161051, 164399, 167789, 171220, 174693, 178208, 181765, 185364 };
						int n[128] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[128];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 4) {
						float s_mean[256] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256 };
						float s_var[256] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024, 1191.58, 1374.62, 1573.56, 1788.85, 2020.92, 2270.16, 2536.99, 2821.81, 3125, 3446.94, 3788, 4148.54, 4528.92, 4929.5, 5350.62, 5792.62, 6255.83, 6740.58, 7247.2, 7776, 8327.3, 8901.41, 9498.64, 10119.3, 10763.7, 11432, 12124.7, 12842, 13584.1, 14351.4, 15144.1, 15962.6, 16807, 17677.7, 18574.9, 19498.8, 20449.8, 21428.1, 22434, 23467.7, 24529.4, 25619.5, 26738.1, 27885.5, 29061.9, 30267.7, 31503, 32768, 34063, 35388.3, 36744, 38130.5, 39547.8, 40996.3, 42476.2, 43987.7, 45531, 47106.3, 48713.9, 50354, 52026.8, 53732.4, 55471.2, 57243.3, 59049, 60888.4, 62761.8, 64669.3, 66611.2, 68587.7, 70598.9, 72645.2, 74726.6, 76843.3, 78995.7, 81183.8, 83407.9, 85668.2, 87964.8, 90298, 92667.9, 95074.7, 97518.7, 100000, 102519, 105075, 107670, 110302, 112973, 115682, 118429, 121216, 124041, 126906, 129810, 132753, 135736, 138759, 141822, 144926, 148069, 151253, 154478, 157744, 161051, 164399, 167789, 171220, 174693, 178208, 181765, 185364, 189005, 192690, 196417, 200187, 203999, 207856, 211755, 215699, 219686, 223716, 227791, 231910, 236074, 240282, 244534, 248832, 253175, 257562, 261995, 266474, 270998, 275568, 280183, 284845, 289553, 294308, 299109, 303957, 308851, 313793, 318781, 323817, 328901, 334032, 339210, 344437, 349711, 355034, 360405, 365825, 371293, 376810, 382376, 387991, 393655, 399368, 405131, 410943, 416805, 422717, 428679, 434692, 440754, 446867, 453031, 459245, 465510, 471827, 478194, 484613, 491083, 497604, 504177, 510803, 517480, 524209, 530990, 537824, 544710, 551649, 558641, 565685, 572783, 579934, 587138, 594395, 601706, 609071, 616490, 623962, 631489, 639070, 646705, 654395, 662139, 669938, 677792, 685700, 693664, 701683, 709758, 717888, 726074, 734315, 742612, 750966, 759375, 767841, 776363, 784941, 793576, 802268, 811017, 819823, 828686, 837606, 846583, 855618, 864711, 873861, 883069, 892335, 901660, 911042, 920483, 929982, 939540, 949156, 958832, 968566, 978359, 988212, 998124, 1.00809e+006, 1.01813e+006, 1.02822e+006, 1.03837e+006, 1.04858e+006 };
						int n[256] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[256];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 5) {
						float s_mean[512] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512 };
						float s_var[512] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024, 1191.58, 1374.62, 1573.56, 1788.85, 2020.92, 2270.16, 2536.99, 2821.81, 3125, 3446.94, 3788, 4148.54, 4528.92, 4929.5, 5350.62, 5792.62, 6255.83, 6740.58, 7247.2, 7776, 8327.3, 8901.41, 9498.64, 10119.3, 10763.7, 11432, 12124.7, 12842, 13584.1, 14351.4, 15144.1, 15962.6, 16807, 17677.7, 18574.9, 19498.8, 20449.8, 21428.1, 22434, 23467.7, 24529.4, 25619.5, 26738.1, 27885.5, 29061.9, 30267.7, 31503, 32768, 34063, 35388.3, 36744, 38130.5, 39547.8, 40996.3, 42476.2, 43987.7, 45531, 47106.3, 48713.9, 50354, 52026.8, 53732.4, 55471.2, 57243.3, 59049, 60888.4, 62761.8, 64669.3, 66611.2, 68587.7, 70598.9, 72645.2, 74726.6, 76843.3, 78995.7, 81183.8, 83407.9, 85668.2, 87964.8, 90298, 92667.9, 95074.7, 97518.7, 100000, 102519, 105075, 107670, 110302, 112973, 115682, 118429, 121216, 124041, 126906, 129810, 132753, 135736, 138759, 141822, 144926, 148069, 151253, 154478, 157744, 161051, 164399, 167789, 171220, 174693, 178208, 181765, 185364, 189005, 192690, 196417, 200187, 203999, 207856, 211755, 215699, 219686, 223716, 227791, 231910, 236074, 240282, 244534, 248832, 253175, 257562, 261995, 266474, 270998, 275568, 280183, 284845, 289553, 294308, 299109, 303957, 308851, 313793, 318781, 323817, 328901, 334032, 339210, 344437, 349711, 355034, 360405, 365825, 371293, 376810, 382376, 387991, 393655, 399368, 405131, 410943, 416805, 422717, 428679, 434692, 440754, 446867, 453031, 459245, 465510, 471827, 478194, 484613, 491083, 497604, 504177, 510803, 517480, 524209, 530990, 537824, 544710, 551649, 558641, 565685, 572783, 579934, 587138, 594395, 601706, 609071, 616490, 623962, 631489, 639070, 646705, 654395, 662139, 669938, 677792, 685700, 693664, 701683, 709758, 717888, 726074, 734315, 742612, 750966, 759375, 767841, 776363, 784941, 793576, 802268, 811017, 819823, 828686, 837606, 846583, 855618, 864711, 873861, 883069, 892335, 901660, 911042, 920483, 929982, 939540, 949156, 958832, 968566, 978359, 988212, 998124, 1.00809e+006, 1.01813e+006, 1.02822e+006, 1.03837e+006, 1.04858e+006, 1.05885e+006, 1.06918e+006, 1.07957e+006, 1.09002e+006, 1.10053e+006, 1.1111e+006, 1.12173e+006, 1.13243e+006, 1.14318e+006, 1.154e+006, 1.16487e+006, 1.17581e+006, 1.18681e+006, 1.19787e+006, 1.20899e+006, 1.22018e+006, 1.23142e+006, 1.24273e+006, 1.2541e+006, 1.26553e+006, 1.27703e+006, 1.28858e+006, 1.3002e+006, 1.31188e+006, 1.32363e+006, 1.33544e+006, 1.34731e+006, 1.35924e+006, 1.37124e+006, 1.3833e+006, 1.39542e+006, 1.40761e+006, 1.41986e+006, 1.43217e+006, 1.44455e+006, 1.45699e+006, 1.4695e+006, 1.48207e+006, 1.4947e+006, 1.5074e+006, 1.52017e+006, 1.53299e+006, 1.54589e+006, 1.55885e+006, 1.57187e+006, 1.58496e+006, 1.59811e+006, 1.61133e+006, 1.62461e+006, 1.63796e+006, 1.65138e+006, 1.66486e+006, 1.6784e+006, 1.69202e+006, 1.70569e+006, 1.71944e+006, 1.73325e+006, 1.74713e+006, 1.76107e+006, 1.77508e+006, 1.78916e+006, 1.8033e+006, 1.81751e+006, 1.83179e+006, 1.84613e+006, 1.86054e+006, 1.87502e+006, 1.88957e+006, 1.90418e+006, 1.91886e+006, 1.93361e+006, 1.94843e+006, 1.96331e+006, 1.97827e+006, 1.99329e+006, 2.00838e+006, 2.02353e+006, 2.03876e+006, 2.05405e+006, 2.06942e+006, 2.08485e+006, 2.10035e+006, 2.11592e+006, 2.13156e+006, 2.14727e+006, 2.16304e+006, 2.17889e+006, 2.19481e+006, 2.21079e+006, 2.22685e+006, 2.24297e+006, 2.25917e+006, 2.27543e+006, 2.29177e+006, 2.30817e+006, 2.32465e+006, 2.34119e+006, 2.35781e+006, 2.37449e+006, 2.39125e+006, 2.40808e+006, 2.42498e+006, 2.44195e+006, 2.45899e+006, 2.4761e+006, 2.49328e+006, 2.51054e+006, 2.52786e+006, 2.54526e+006, 2.56273e+006, 2.58027e+006, 2.59788e+006, 2.61557e+006, 2.63332e+006, 2.65115e+006, 2.66905e+006, 2.68703e+006, 2.70507e+006, 2.72319e+006, 2.74138e+006, 2.75965e+006, 2.77798e+006, 2.79639e+006, 2.81487e+006, 2.83343e+006, 2.85206e+006, 2.87076e+006, 2.88954e+006, 2.90838e+006, 2.92731e+006, 2.9463e+006, 2.96537e+006, 2.98452e+006, 3.00373e+006, 3.02303e+006, 3.04239e+006, 3.06183e+006, 3.08135e+006, 3.10094e+006, 3.1206e+006, 3.14034e+006, 3.16015e+006, 3.18004e+006, 3.2e+006, 3.22004e+006, 3.24015e+006, 3.26034e+006, 3.2806e+006, 3.30094e+006, 3.32135e+006, 3.34184e+006, 3.36241e+006, 3.38305e+006, 3.40377e+006, 3.42456e+006, 3.44543e+006, 3.46637e+006, 3.48739e+006, 3.50849e+006, 3.52966e+006, 3.55091e+006, 3.57224e+006, 3.59364e+006, 3.61512e+006, 3.63668e+006, 3.65832e+006, 3.68003e+006, 3.70181e+006, 3.72368e+006, 3.74562e+006, 3.76764e+006, 3.78974e+006, 3.81192e+006, 3.83417e+006, 3.8565e+006, 3.87891e+006, 3.90139e+006, 3.92396e+006, 3.9466e+006, 3.96932e+006, 3.99212e+006, 4.015e+006, 4.03795e+006, 4.06099e+006, 4.0841e+006, 4.10729e+006, 4.13056e+006, 4.15391e+006, 4.17734e+006, 4.20085e+006, 4.22444e+006, 4.2481e+006, 4.27185e+006, 4.29567e+006, 4.31958e+006, 4.34356e+006, 4.36763e+006, 4.39177e+006, 4.41599e+006, 4.4403e+006, 4.46468e+006, 4.48915e+006, 4.51369e+006, 4.53831e+006, 4.56302e+006, 4.5878e+006, 4.61267e+006, 4.63762e+006, 4.66265e+006, 4.68775e+006, 4.71294e+006, 4.73821e+006, 4.76357e+006, 4.789e+006, 4.81451e+006, 4.84011e+006, 4.86578e+006, 4.89154e+006, 4.91738e+006, 4.9433e+006, 4.96931e+006, 4.99539e+006, 5.02156e+006, 5.04781e+006, 5.07414e+006, 5.10056e+006, 5.12705e+006, 5.15363e+006, 5.18029e+006, 5.20704e+006, 5.23386e+006, 5.26077e+006, 5.28777e+006, 5.31484e+006, 5.342e+006, 5.36924e+006, 5.39656e+006, 5.42397e+006, 5.45146e+006, 5.47904e+006, 5.50669e+006, 5.53444e+006, 5.56226e+006, 5.59017e+006, 5.61816e+006, 5.64624e+006, 5.6744e+006, 5.70265e+006, 5.73097e+006, 5.75939e+006, 5.78789e+006, 5.81647e+006, 5.84513e+006, 5.87389e+006, 5.90272e+006, 5.93164e+006 };
						int n[512] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[512];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 6) {
						float s_mean[1024] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024 };
						float s_var[1024] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024, 1191.58, 1374.62, 1573.56, 1788.85, 2020.92, 2270.16, 2536.99, 2821.81, 3125, 3446.94, 3788, 4148.54, 4528.92, 4929.5, 5350.62, 5792.62, 6255.83, 6740.58, 7247.2, 7776, 8327.3, 8901.41, 9498.64, 10119.3, 10763.7, 11432, 12124.7, 12842, 13584.1, 14351.4, 15144.1, 15962.6, 16807, 17677.7, 18574.9, 19498.8, 20449.8, 21428.1, 22434, 23467.7, 24529.4, 25619.5, 26738.1, 27885.5, 29061.9, 30267.7, 31503, 32768, 34063, 35388.3, 36744, 38130.5, 39547.8, 40996.3, 42476.2, 43987.7, 45531, 47106.3, 48713.9, 50354, 52026.8, 53732.4, 55471.2, 57243.3, 59049, 60888.4, 62761.8, 64669.3, 66611.2, 68587.7, 70598.9, 72645.2, 74726.6, 76843.3, 78995.7, 81183.8, 83407.9, 85668.2, 87964.8, 90298, 92667.9, 95074.7, 97518.7, 100000, 102519, 105075, 107670, 110302, 112973, 115682, 118429, 121216, 124041, 126906, 129810, 132753, 135736, 138759, 141822, 144926, 148069, 151253, 154478, 157744, 161051, 164399, 167789, 171220, 174693, 178208, 181765, 185364, 189005, 192690, 196417, 200187, 203999, 207856, 211755, 215699, 219686, 223716, 227791, 231910, 236074, 240282, 244534, 248832, 253175, 257562, 261995, 266474, 270998, 275568, 280183, 284845, 289553, 294308, 299109, 303957, 308851, 313793, 318781, 323817, 328901, 334032, 339210, 344437, 349711, 355034, 360405, 365825, 371293, 376810, 382376, 387991, 393655, 399368, 405131, 410943, 416805, 422717, 428679, 434692, 440754, 446867, 453031, 459245, 465510, 471827, 478194, 484613, 491083, 497604, 504177, 510803, 517480, 524209, 530990, 537824, 544710, 551649, 558641, 565685, 572783, 579934, 587138, 594395, 601706, 609071, 616490, 623962, 631489, 639070, 646705, 654395, 662139, 669938, 677792, 685700, 693664, 701683, 709758, 717888, 726074, 734315, 742612, 750966, 759375, 767841, 776363, 784941, 793576, 802268, 811017, 819823, 828686, 837606, 846583, 855618, 864711, 873861, 883069, 892335, 901660, 911042, 920483, 929982, 939540, 949156, 958832, 968566, 978359, 988212, 998124, 1.00809e+006, 1.01813e+006, 1.02822e+006, 1.03837e+006, 1.04858e+006, 1.05885e+006, 1.06918e+006, 1.07957e+006, 1.09002e+006, 1.10053e+006, 1.1111e+006, 1.12173e+006, 1.13243e+006, 1.14318e+006, 1.154e+006, 1.16487e+006, 1.17581e+006, 1.18681e+006, 1.19787e+006, 1.20899e+006, 1.22018e+006, 1.23142e+006, 1.24273e+006, 1.2541e+006, 1.26553e+006, 1.27703e+006, 1.28858e+006, 1.3002e+006, 1.31188e+006, 1.32363e+006, 1.33544e+006, 1.34731e+006, 1.35924e+006, 1.37124e+006, 1.3833e+006, 1.39542e+006, 1.40761e+006, 1.41986e+006, 1.43217e+006, 1.44455e+006, 1.45699e+006, 1.4695e+006, 1.48207e+006, 1.4947e+006, 1.5074e+006, 1.52017e+006, 1.53299e+006, 1.54589e+006, 1.55885e+006, 1.57187e+006, 1.58496e+006, 1.59811e+006, 1.61133e+006, 1.62461e+006, 1.63796e+006, 1.65138e+006, 1.66486e+006, 1.6784e+006, 1.69202e+006, 1.70569e+006, 1.71944e+006, 1.73325e+006, 1.74713e+006, 1.76107e+006, 1.77508e+006, 1.78916e+006, 1.8033e+006, 1.81751e+006, 1.83179e+006, 1.84613e+006, 1.86054e+006, 1.87502e+006, 1.88957e+006, 1.90418e+006, 1.91886e+006, 1.93361e+006, 1.94843e+006, 1.96331e+006, 1.97827e+006, 1.99329e+006, 2.00838e+006, 2.02353e+006, 2.03876e+006, 2.05405e+006, 2.06942e+006, 2.08485e+006, 2.10035e+006, 2.11592e+006, 2.13156e+006, 2.14727e+006, 2.16304e+006, 2.17889e+006, 2.19481e+006, 2.21079e+006, 2.22685e+006, 2.24297e+006, 2.25917e+006, 2.27543e+006, 2.29177e+006, 2.30817e+006, 2.32465e+006, 2.34119e+006, 2.35781e+006, 2.37449e+006, 2.39125e+006, 2.40808e+006, 2.42498e+006, 2.44195e+006, 2.45899e+006, 2.4761e+006, 2.49328e+006, 2.51054e+006, 2.52786e+006, 2.54526e+006, 2.56273e+006, 2.58027e+006, 2.59788e+006, 2.61557e+006, 2.63332e+006, 2.65115e+006, 2.66905e+006, 2.68703e+006, 2.70507e+006, 2.72319e+006, 2.74138e+006, 2.75965e+006, 2.77798e+006, 2.79639e+006, 2.81487e+006, 2.83343e+006, 2.85206e+006, 2.87076e+006, 2.88954e+006, 2.90838e+006, 2.92731e+006, 2.9463e+006, 2.96537e+006, 2.98452e+006, 3.00373e+006, 3.02303e+006, 3.04239e+006, 3.06183e+006, 3.08135e+006, 3.10094e+006, 3.1206e+006, 3.14034e+006, 3.16015e+006, 3.18004e+006, 3.2e+006, 3.22004e+006, 3.24015e+006, 3.26034e+006, 3.2806e+006, 3.30094e+006, 3.32135e+006, 3.34184e+006, 3.36241e+006, 3.38305e+006, 3.40377e+006, 3.42456e+006, 3.44543e+006, 3.46637e+006, 3.48739e+006, 3.50849e+006, 3.52966e+006, 3.55091e+006, 3.57224e+006, 3.59364e+006, 3.61512e+006, 3.63668e+006, 3.65832e+006, 3.68003e+006, 3.70181e+006, 3.72368e+006, 3.74562e+006, 3.76764e+006, 3.78974e+006, 3.81192e+006, 3.83417e+006, 3.8565e+006, 3.87891e+006, 3.90139e+006, 3.92396e+006, 3.9466e+006, 3.96932e+006, 3.99212e+006, 4.015e+006, 4.03795e+006, 4.06099e+006, 4.0841e+006, 4.10729e+006, 4.13056e+006, 4.15391e+006, 4.17734e+006, 4.20085e+006, 4.22444e+006, 4.2481e+006, 4.27185e+006, 4.29567e+006, 4.31958e+006, 4.34356e+006, 4.36763e+006, 4.39177e+006, 4.41599e+006, 4.4403e+006, 4.46468e+006, 4.48915e+006, 4.51369e+006, 4.53831e+006, 4.56302e+006, 4.5878e+006, 4.61267e+006, 4.63762e+006, 4.66265e+006, 4.68775e+006, 4.71294e+006, 4.73821e+006, 4.76357e+006, 4.789e+006, 4.81451e+006, 4.84011e+006, 4.86578e+006, 4.89154e+006, 4.91738e+006, 4.9433e+006, 4.96931e+006, 4.99539e+006, 5.02156e+006, 5.04781e+006, 5.07414e+006, 5.10056e+006, 5.12705e+006, 5.15363e+006, 5.18029e+006, 5.20704e+006, 5.23386e+006, 5.26077e+006, 5.28777e+006, 5.31484e+006, 5.342e+006, 5.36924e+006, 5.39656e+006, 5.42397e+006, 5.45146e+006, 5.47904e+006, 5.50669e+006, 5.53444e+006, 5.56226e+006, 5.59017e+006, 5.61816e+006, 5.64624e+006, 5.6744e+006, 5.70265e+006, 5.73097e+006, 5.75939e+006, 5.78789e+006, 5.81647e+006, 5.84513e+006, 5.87389e+006, 5.90272e+006, 5.93164e+006, 5.96065e+006, 5.98974e+006, 6.01891e+006, 6.04817e+006, 6.07752e+006, 6.10695e+006, 6.13647e+006, 6.16607e+006, 6.19576e+006, 6.22553e+006, 6.25539e+006, 6.28533e+006, 6.31536e+006, 6.34548e+006, 6.37568e+006, 6.40597e+006, 6.43634e+006, 6.4668e+006, 6.49735e+006, 6.52798e+006, 6.5587e+006, 6.58951e+006, 6.6204e+006, 6.65138e+006, 6.68245e+006, 6.7136e+006, 6.74484e+006, 6.77617e+006, 6.80759e+006, 6.83909e+006, 6.87068e+006, 6.90235e+006, 6.93412e+006, 6.96597e+006, 6.99791e+006, 7.02994e+006, 7.06205e+006, 7.09425e+006, 7.12654e+006, 7.15892e+006, 7.19139e+006, 7.22394e+006, 7.25659e+006, 7.28932e+006, 7.32214e+006, 7.35505e+006, 7.38804e+006, 7.42113e+006, 7.4543e+006, 7.48757e+006, 7.52092e+006, 7.55436e+006, 7.58789e+006, 7.62151e+006, 7.65522e+006, 7.68902e+006, 7.7229e+006, 7.75688e+006, 7.79095e+006, 7.8251e+006, 7.85935e+006, 7.89368e+006, 7.92811e+006, 7.96262e+006, 7.99723e+006, 8.03192e+006, 8.06671e+006, 8.10158e+006, 8.13655e+006, 8.17161e+006, 8.20675e+006, 8.24199e+006, 8.27732e+006, 8.31274e+006, 8.34825e+006, 8.38385e+006, 8.41954e+006, 8.45532e+006, 8.49119e+006, 8.52716e+006, 8.56321e+006, 8.59936e+006, 8.6356e+006, 8.67193e+006, 8.70835e+006, 8.74486e+006, 8.78147e+006, 8.81816e+006, 8.85495e+006, 8.89183e+006, 8.9288e+006, 8.96587e+006, 9.00302e+006, 9.04027e+006, 9.07761e+006, 9.11505e+006, 9.15257e+006, 9.19019e+006, 9.2279e+006, 9.26571e+006, 9.3036e+006, 9.34159e+006, 9.37967e+006, 9.41785e+006, 9.45612e+006, 9.49448e+006, 9.53293e+006, 9.57148e+006, 9.61012e+006, 9.64886e+006, 9.68769e+006, 9.72661e+006, 9.76563e+006, 9.80473e+006, 9.84394e+006, 9.88323e+006, 9.92263e+006, 9.96211e+006, 1.00017e+007, 1.00414e+007, 1.00811e+007, 1.0121e+007, 1.01609e+007, 1.0201e+007, 1.02411e+007, 1.02814e+007, 1.03217e+007, 1.03622e+007, 1.04027e+007, 1.04433e+007, 1.0484e+007, 1.05248e+007, 1.05657e+007, 1.06067e+007, 1.06478e+007, 1.0689e+007, 1.07303e+007, 1.07717e+007, 1.08132e+007, 1.08547e+007, 1.08964e+007, 1.09382e+007, 1.098e+007, 1.1022e+007, 1.1064e+007, 1.11062e+007, 1.11484e+007, 1.11908e+007, 1.12332e+007, 1.12757e+007, 1.13184e+007, 1.13611e+007, 1.14039e+007, 1.14468e+007, 1.14899e+007, 1.1533e+007, 1.15762e+007, 1.16195e+007, 1.16629e+007, 1.17064e+007, 1.175e+007, 1.17937e+007, 1.18375e+007, 1.18814e+007, 1.19254e+007, 1.19695e+007, 1.20136e+007, 1.20579e+007, 1.21023e+007, 1.21468e+007, 1.21913e+007, 1.2236e+007, 1.22808e+007, 1.23257e+007, 1.23706e+007, 1.24157e+007, 1.24609e+007, 1.25061e+007, 1.25515e+007, 1.25969e+007, 1.26425e+007, 1.26882e+007, 1.27339e+007, 1.27798e+007, 1.28257e+007, 1.28718e+007, 1.29179e+007, 1.29642e+007, 1.30105e+007, 1.3057e+007, 1.31035e+007, 1.31502e+007, 1.31969e+007, 1.32438e+007, 1.32907e+007, 1.33378e+007, 1.33849e+007, 1.34322e+007, 1.34795e+007, 1.3527e+007, 1.35745e+007, 1.36221e+007, 1.36699e+007, 1.37177e+007, 1.37657e+007, 1.38137e+007, 1.38619e+007, 1.39101e+007, 1.39585e+007, 1.40069e+007, 1.40555e+007, 1.41041e+007, 1.41529e+007, 1.42017e+007, 1.42507e+007, 1.42998e+007, 1.43489e+007, 1.43982e+007, 1.44475e+007, 1.4497e+007, 1.45465e+007, 1.45962e+007, 1.4646e+007, 1.46958e+007, 1.47458e+007, 1.47959e+007, 1.48461e+007, 1.48963e+007, 1.49467e+007, 1.49972e+007, 1.50478e+007, 1.50984e+007, 1.51492e+007, 1.52001e+007, 1.52511e+007, 1.53022e+007, 1.53534e+007, 1.54047e+007, 1.54561e+007, 1.55076e+007, 1.55592e+007, 1.56109e+007, 1.56627e+007, 1.57146e+007, 1.57667e+007, 1.58188e+007, 1.5871e+007, 1.59233e+007, 1.59758e+007, 1.60283e+007, 1.60809e+007, 1.61337e+007, 1.61865e+007, 1.62395e+007, 1.62925e+007, 1.63457e+007, 1.63989e+007, 1.64523e+007, 1.65058e+007, 1.65593e+007, 1.6613e+007, 1.66668e+007, 1.67207e+007, 1.67747e+007, 1.68288e+007, 1.6883e+007, 1.69373e+007, 1.69917e+007, 1.70462e+007, 1.71008e+007, 1.71555e+007, 1.72104e+007, 1.72653e+007, 1.73203e+007, 1.73755e+007, 1.74307e+007, 1.74861e+007, 1.75415e+007, 1.75971e+007, 1.76528e+007, 1.77085e+007, 1.77644e+007, 1.78204e+007, 1.78765e+007, 1.79327e+007, 1.7989e+007, 1.80454e+007, 1.81019e+007, 1.81586e+007, 1.82153e+007, 1.82721e+007, 1.83291e+007, 1.83861e+007, 1.84433e+007, 1.85005e+007, 1.85579e+007, 1.86154e+007, 1.86729e+007, 1.87306e+007, 1.87884e+007, 1.88463e+007, 1.89043e+007, 1.89624e+007, 1.90207e+007, 1.9079e+007, 1.91374e+007, 1.9196e+007, 1.92546e+007, 1.93134e+007, 1.93722e+007, 1.94312e+007, 1.94903e+007, 1.95495e+007, 1.96088e+007, 1.96682e+007, 1.97277e+007, 1.97873e+007, 1.9847e+007, 1.99069e+007, 1.99668e+007, 2.00268e+007, 2.0087e+007, 2.01473e+007, 2.02076e+007, 2.02681e+007, 2.03287e+007, 2.03894e+007, 2.04502e+007, 2.05111e+007, 2.05722e+007, 2.06333e+007, 2.06946e+007, 2.07559e+007, 2.08174e+007, 2.08789e+007, 2.09406e+007, 2.10024e+007, 2.10643e+007, 2.11263e+007, 2.11884e+007, 2.12507e+007, 2.1313e+007, 2.13755e+007, 2.1438e+007, 2.15007e+007, 2.15634e+007, 2.16263e+007, 2.16893e+007, 2.17524e+007, 2.18157e+007, 2.1879e+007, 2.19424e+007, 2.2006e+007, 2.20696e+007, 2.21334e+007, 2.21973e+007, 2.22612e+007, 2.23253e+007, 2.23896e+007, 2.24539e+007, 2.25183e+007, 2.25828e+007, 2.26475e+007, 2.27123e+007, 2.27771e+007, 2.28421e+007, 2.29072e+007, 2.29724e+007, 2.30377e+007, 2.31032e+007, 2.31687e+007, 2.32344e+007, 2.33001e+007, 2.3366e+007, 2.3432e+007, 2.34981e+007, 2.35643e+007, 2.36306e+007, 2.3697e+007, 2.37636e+007, 2.38303e+007, 2.3897e+007, 2.39639e+007, 2.40309e+007, 2.4098e+007, 2.41652e+007, 2.42326e+007, 2.43e+007, 2.43676e+007, 2.44352e+007, 2.4503e+007, 2.45709e+007, 2.46389e+007, 2.4707e+007, 2.47753e+007, 2.48436e+007, 2.49121e+007, 2.49806e+007, 2.50493e+007, 2.51181e+007, 2.5187e+007, 2.52561e+007, 2.53252e+007, 2.53944e+007, 2.54638e+007, 2.55333e+007, 2.56029e+007, 2.56726e+007, 2.57424e+007, 2.58123e+007, 2.58824e+007, 2.59525e+007, 2.60228e+007, 2.60932e+007, 2.61637e+007, 2.62343e+007, 2.63051e+007, 2.63759e+007, 2.64469e+007, 2.65179e+007, 2.65891e+007, 2.66604e+007, 2.67319e+007, 2.68034e+007, 2.6875e+007, 2.69468e+007, 2.70187e+007, 2.70907e+007, 2.71628e+007, 2.7235e+007, 2.73073e+007, 2.73798e+007, 2.74523e+007, 2.7525e+007, 2.75978e+007, 2.76707e+007, 2.77438e+007, 2.78169e+007, 2.78902e+007, 2.79636e+007, 2.8037e+007, 2.81107e+007, 2.81844e+007, 2.82582e+007, 2.83322e+007, 2.84062e+007, 2.84804e+007, 2.85547e+007, 2.86292e+007, 2.87037e+007, 2.87783e+007, 2.88531e+007, 2.8928e+007, 2.9003e+007, 2.90781e+007, 2.91533e+007, 2.92287e+007, 2.93042e+007, 2.93797e+007, 2.94555e+007, 2.95313e+007, 2.96072e+007, 2.96833e+007, 2.97594e+007, 2.98357e+007, 2.99121e+007, 2.99886e+007, 3.00653e+007, 3.0142e+007, 3.02189e+007, 3.02959e+007, 3.0373e+007, 3.04502e+007, 3.05276e+007, 3.0605e+007, 3.06826e+007, 3.07603e+007, 3.08381e+007, 3.09161e+007, 3.09941e+007, 3.10723e+007, 3.11506e+007, 3.1229e+007, 3.13075e+007, 3.13861e+007, 3.14649e+007, 3.15438e+007, 3.16228e+007, 3.17019e+007, 3.17811e+007, 3.18605e+007, 3.194e+007, 3.20195e+007, 3.20993e+007, 3.21791e+007, 3.2259e+007, 3.23391e+007, 3.24193e+007, 3.24996e+007, 3.258e+007, 3.26606e+007, 3.27412e+007, 3.2822e+007, 3.29029e+007, 3.29839e+007, 3.30651e+007, 3.31463e+007, 3.32277e+007, 3.33092e+007, 3.33908e+007, 3.34726e+007, 3.35544e+007 };
						int n[1024] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[1024];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 7) {
						float s_mean[2048] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 1132, 1133, 1134, 1135, 1136, 1137, 1138, 1139, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149, 1150, 1151, 1152, 1153, 1154, 1155, 1156, 1157, 1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187, 1188, 1189, 1190, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249, 1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259, 1260, 1261, 1262, 1263, 1264, 1265, 1266, 1267, 1268, 1269, 1270, 1271, 1272, 1273, 1274, 1275, 1276, 1277, 1278, 1279, 1280, 1281, 1282, 1283, 1284, 1285, 1286, 1287, 1288, 1289, 1290, 1291, 1292, 1293, 1294, 1295, 1296, 1297, 1298, 1299, 1300, 1301, 1302, 1303, 1304, 1305, 1306, 1307, 1308, 1309, 1310, 1311, 1312, 1313, 1314, 1315, 1316, 1317, 1318, 1319, 1320, 1321, 1322, 1323, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1332, 1333, 1334, 1335, 1336, 1337, 1338, 1339, 1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1371, 1372, 1373, 1374, 1375, 1376, 1377, 1378, 1379, 1380, 1381, 1382, 1383, 1384, 1385, 1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396, 1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405, 1406, 1407, 1408, 1409, 1410, 1411, 1412, 1413, 1414, 1415, 1416, 1417, 1418, 1419, 1420, 1421, 1422, 1423, 1424, 1425, 1426, 1427, 1428, 1429, 1430, 1431, 1432, 1433, 1434, 1435, 1436, 1437, 1438, 1439, 1440, 1441, 1442, 1443, 1444, 1445, 1446, 1447, 1448, 1449, 1450, 1451, 1452, 1453, 1454, 1455, 1456, 1457, 1458, 1459, 1460, 1461, 1462, 1463, 1464, 1465, 1466, 1467, 1468, 1469, 1470, 1471, 1472, 1473, 1474, 1475, 1476, 1477, 1478, 1479, 1480, 1481, 1482, 1483, 1484, 1485, 1486, 1487, 1488, 1489, 1490, 1491, 1492, 1493, 1494, 1495, 1496, 1497, 1498, 1499, 1500, 1501, 1502, 1503, 1504, 1505, 1506, 1507, 1508, 1509, 1510, 1511, 1512, 1513, 1514, 1515, 1516, 1517, 1518, 1519, 1520, 1521, 1522, 1523, 1524, 1525, 1526, 1527, 1528, 1529, 1530, 1531, 1532, 1533, 1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545, 1546, 1547, 1548, 1549, 1550, 1551, 1552, 1553, 1554, 1555, 1556, 1557, 1558, 1559, 1560, 1561, 1562, 1563, 1564, 1565, 1566, 1567, 1568, 1569, 1570, 1571, 1572, 1573, 1574, 1575, 1576, 1577, 1578, 1579, 1580, 1581, 1582, 1583, 1584, 1585, 1586, 1587, 1588, 1589, 1590, 1591, 1592, 1593, 1594, 1595, 1596, 1597, 1598, 1599, 1600, 1601, 1602, 1603, 1604, 1605, 1606, 1607, 1608, 1609, 1610, 1611, 1612, 1613, 1614, 1615, 1616, 1617, 1618, 1619, 1620, 1621, 1622, 1623, 1624, 1625, 1626, 1627, 1628, 1629, 1630, 1631, 1632, 1633, 1634, 1635, 1636, 1637, 1638, 1639, 1640, 1641, 1642, 1643, 1644, 1645, 1646, 1647, 1648, 1649, 1650, 1651, 1652, 1653, 1654, 1655, 1656, 1657, 1658, 1659, 1660, 1661, 1662, 1663, 1664, 1665, 1666, 1667, 1668, 1669, 1670, 1671, 1672, 1673, 1674, 1675, 1676, 1677, 1678, 1679, 1680, 1681, 1682, 1683, 1684, 1685, 1686, 1687, 1688, 1689, 1690, 1691, 1692, 1693, 1694, 1695, 1696, 1697, 1698, 1699, 1700, 1701, 1702, 1703, 1704, 1705, 1706, 1707, 1708, 1709, 1710, 1711, 1712, 1713, 1714, 1715, 1716, 1717, 1718, 1719, 1720, 1721, 1722, 1723, 1724, 1725, 1726, 1727, 1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736, 1737, 1738, 1739, 1740, 1741, 1742, 1743, 1744, 1745, 1746, 1747, 1748, 1749, 1750, 1751, 1752, 1753, 1754, 1755, 1756, 1757, 1758, 1759, 1760, 1761, 1762, 1763, 1764, 1765, 1766, 1767, 1768, 1769, 1770, 1771, 1772, 1773, 1774, 1775, 1776, 1777, 1778, 1779, 1780, 1781, 1782, 1783, 1784, 1785, 1786, 1787, 1788, 1789, 1790, 1791, 1792, 1793, 1794, 1795, 1796, 1797, 1798, 1799, 1800, 1801, 1802, 1803, 1804, 1805, 1806, 1807, 1808, 1809, 1810, 1811, 1812, 1813, 1814, 1815, 1816, 1817, 1818, 1819, 1820, 1821, 1822, 1823, 1824, 1825, 1826, 1827, 1828, 1829, 1830, 1831, 1832, 1833, 1834, 1835, 1836, 1837, 1838, 1839, 1840, 1841, 1842, 1843, 1844, 1845, 1846, 1847, 1848, 1849, 1850, 1851, 1852, 1853, 1854, 1855, 1856, 1857, 1858, 1859, 1860, 1861, 1862, 1863, 1864, 1865, 1866, 1867, 1868, 1869, 1870, 1871, 1872, 1873, 1874, 1875, 1876, 1877, 1878, 1879, 1880, 1881, 1882, 1883, 1884, 1885, 1886, 1887, 1888, 1889, 1890, 1891, 1892, 1893, 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909, 1910, 1911, 1912, 1913, 1914, 1915, 1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048 };
						float s_var[2048] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024, 1191.58, 1374.62, 1573.56, 1788.85, 2020.92, 2270.16, 2536.99, 2821.81, 3125, 3446.94, 3788, 4148.54, 4528.92, 4929.5, 5350.62, 5792.62, 6255.83, 6740.58, 7247.2, 7776, 8327.3, 8901.41, 9498.64, 10119.3, 10763.7, 11432, 12124.7, 12842, 13584.1, 14351.4, 15144.1, 15962.6, 16807, 17677.7, 18574.9, 19498.8, 20449.8, 21428.1, 22434, 23467.7, 24529.4, 25619.5, 26738.1, 27885.5, 29061.9, 30267.7, 31503, 32768, 34063, 35388.3, 36744, 38130.5, 39547.8, 40996.3, 42476.2, 43987.7, 45531, 47106.3, 48713.9, 50354, 52026.8, 53732.4, 55471.2, 57243.3, 59049, 60888.4, 62761.8, 64669.3, 66611.2, 68587.7, 70598.9, 72645.2, 74726.6, 76843.3, 78995.7, 81183.8, 83407.9, 85668.2, 87964.8, 90298, 92667.9, 95074.7, 97518.7, 100000, 102519, 105075, 107670, 110302, 112973, 115682, 118429, 121216, 124041, 126906, 129810, 132753, 135736, 138759, 141822, 144926, 148069, 151253, 154478, 157744, 161051, 164399, 167789, 171220, 174693, 178208, 181765, 185364, 189005, 192690, 196417, 200187, 203999, 207856, 211755, 215699, 219686, 223716, 227791, 231910, 236074, 240282, 244534, 248832, 253175, 257562, 261995, 266474, 270998, 275568, 280183, 284845, 289553, 294308, 299109, 303957, 308851, 313793, 318781, 323817, 328901, 334032, 339210, 344437, 349711, 355034, 360405, 365825, 371293, 376810, 382376, 387991, 393655, 399368, 405131, 410943, 416805, 422717, 428679, 434692, 440754, 446867, 453031, 459245, 465510, 471827, 478194, 484613, 491083, 497604, 504177, 510803, 517480, 524209, 530990, 537824, 544710, 551649, 558641, 565685, 572783, 579934, 587138, 594395, 601706, 609071, 616490, 623962, 631489, 639070, 646705, 654395, 662139, 669938, 677792, 685700, 693664, 701683, 709758, 717888, 726074, 734315, 742612, 750966, 759375, 767841, 776363, 784941, 793576, 802268, 811017, 819823, 828686, 837606, 846583, 855618, 864711, 873861, 883069, 892335, 901660, 911042, 920483, 929982, 939540, 949156, 958832, 968566, 978359, 988212, 998124, 1.00809e+006, 1.01813e+006, 1.02822e+006, 1.03837e+006, 1.04858e+006, 1.05885e+006, 1.06918e+006, 1.07957e+006, 1.09002e+006, 1.10053e+006, 1.1111e+006, 1.12173e+006, 1.13243e+006, 1.14318e+006, 1.154e+006, 1.16487e+006, 1.17581e+006, 1.18681e+006, 1.19787e+006, 1.20899e+006, 1.22018e+006, 1.23142e+006, 1.24273e+006, 1.2541e+006, 1.26553e+006, 1.27703e+006, 1.28858e+006, 1.3002e+006, 1.31188e+006, 1.32363e+006, 1.33544e+006, 1.34731e+006, 1.35924e+006, 1.37124e+006, 1.3833e+006, 1.39542e+006, 1.40761e+006, 1.41986e+006, 1.43217e+006, 1.44455e+006, 1.45699e+006, 1.4695e+006, 1.48207e+006, 1.4947e+006, 1.5074e+006, 1.52017e+006, 1.53299e+006, 1.54589e+006, 1.55885e+006, 1.57187e+006, 1.58496e+006, 1.59811e+006, 1.61133e+006, 1.62461e+006, 1.63796e+006, 1.65138e+006, 1.66486e+006, 1.6784e+006, 1.69202e+006, 1.70569e+006, 1.71944e+006, 1.73325e+006, 1.74713e+006, 1.76107e+006, 1.77508e+006, 1.78916e+006, 1.8033e+006, 1.81751e+006, 1.83179e+006, 1.84613e+006, 1.86054e+006, 1.87502e+006, 1.88957e+006, 1.90418e+006, 1.91886e+006, 1.93361e+006, 1.94843e+006, 1.96331e+006, 1.97827e+006, 1.99329e+006, 2.00838e+006, 2.02353e+006, 2.03876e+006, 2.05405e+006, 2.06942e+006, 2.08485e+006, 2.10035e+006, 2.11592e+006, 2.13156e+006, 2.14727e+006, 2.16304e+006, 2.17889e+006, 2.19481e+006, 2.21079e+006, 2.22685e+006, 2.24297e+006, 2.25917e+006, 2.27543e+006, 2.29177e+006, 2.30817e+006, 2.32465e+006, 2.34119e+006, 2.35781e+006, 2.37449e+006, 2.39125e+006, 2.40808e+006, 2.42498e+006, 2.44195e+006, 2.45899e+006, 2.4761e+006, 2.49328e+006, 2.51054e+006, 2.52786e+006, 2.54526e+006, 2.56273e+006, 2.58027e+006, 2.59788e+006, 2.61557e+006, 2.63332e+006, 2.65115e+006, 2.66905e+006, 2.68703e+006, 2.70507e+006, 2.72319e+006, 2.74138e+006, 2.75965e+006, 2.77798e+006, 2.79639e+006, 2.81487e+006, 2.83343e+006, 2.85206e+006, 2.87076e+006, 2.88954e+006, 2.90838e+006, 2.92731e+006, 2.9463e+006, 2.96537e+006, 2.98452e+006, 3.00373e+006, 3.02303e+006, 3.04239e+006, 3.06183e+006, 3.08135e+006, 3.10094e+006, 3.1206e+006, 3.14034e+006, 3.16015e+006, 3.18004e+006, 3.2e+006, 3.22004e+006, 3.24015e+006, 3.26034e+006, 3.2806e+006, 3.30094e+006, 3.32135e+006, 3.34184e+006, 3.36241e+006, 3.38305e+006, 3.40377e+006, 3.42456e+006, 3.44543e+006, 3.46637e+006, 3.48739e+006, 3.50849e+006, 3.52966e+006, 3.55091e+006, 3.57224e+006, 3.59364e+006, 3.61512e+006, 3.63668e+006, 3.65832e+006, 3.68003e+006, 3.70181e+006, 3.72368e+006, 3.74562e+006, 3.76764e+006, 3.78974e+006, 3.81192e+006, 3.83417e+006, 3.8565e+006, 3.87891e+006, 3.90139e+006, 3.92396e+006, 3.9466e+006, 3.96932e+006, 3.99212e+006, 4.015e+006, 4.03795e+006, 4.06099e+006, 4.0841e+006, 4.10729e+006, 4.13056e+006, 4.15391e+006, 4.17734e+006, 4.20085e+006, 4.22444e+006, 4.2481e+006, 4.27185e+006, 4.29567e+006, 4.31958e+006, 4.34356e+006, 4.36763e+006, 4.39177e+006, 4.41599e+006, 4.4403e+006, 4.46468e+006, 4.48915e+006, 4.51369e+006, 4.53831e+006, 4.56302e+006, 4.5878e+006, 4.61267e+006, 4.63762e+006, 4.66265e+006, 4.68775e+006, 4.71294e+006, 4.73821e+006, 4.76357e+006, 4.789e+006, 4.81451e+006, 4.84011e+006, 4.86578e+006, 4.89154e+006, 4.91738e+006, 4.9433e+006, 4.96931e+006, 4.99539e+006, 5.02156e+006, 5.04781e+006, 5.07414e+006, 5.10056e+006, 5.12705e+006, 5.15363e+006, 5.18029e+006, 5.20704e+006, 5.23386e+006, 5.26077e+006, 5.28777e+006, 5.31484e+006, 5.342e+006, 5.36924e+006, 5.39656e+006, 5.42397e+006, 5.45146e+006, 5.47904e+006, 5.50669e+006, 5.53444e+006, 5.56226e+006, 5.59017e+006, 5.61816e+006, 5.64624e+006, 5.6744e+006, 5.70265e+006, 5.73097e+006, 5.75939e+006, 5.78789e+006, 5.81647e+006, 5.84513e+006, 5.87389e+006, 5.90272e+006, 5.93164e+006, 5.96065e+006, 5.98974e+006, 6.01891e+006, 6.04817e+006, 6.07752e+006, 6.10695e+006, 6.13647e+006, 6.16607e+006, 6.19576e+006, 6.22553e+006, 6.25539e+006, 6.28533e+006, 6.31536e+006, 6.34548e+006, 6.37568e+006, 6.40597e+006, 6.43634e+006, 6.4668e+006, 6.49735e+006, 6.52798e+006, 6.5587e+006, 6.58951e+006, 6.6204e+006, 6.65138e+006, 6.68245e+006, 6.7136e+006, 6.74484e+006, 6.77617e+006, 6.80759e+006, 6.83909e+006, 6.87068e+006, 6.90235e+006, 6.93412e+006, 6.96597e+006, 6.99791e+006, 7.02994e+006, 7.06205e+006, 7.09425e+006, 7.12654e+006, 7.15892e+006, 7.19139e+006, 7.22394e+006, 7.25659e+006, 7.28932e+006, 7.32214e+006, 7.35505e+006, 7.38804e+006, 7.42113e+006, 7.4543e+006, 7.48757e+006, 7.52092e+006, 7.55436e+006, 7.58789e+006, 7.62151e+006, 7.65522e+006, 7.68902e+006, 7.7229e+006, 7.75688e+006, 7.79095e+006, 7.8251e+006, 7.85935e+006, 7.89368e+006, 7.92811e+006, 7.96262e+006, 7.99723e+006, 8.03192e+006, 8.06671e+006, 8.10158e+006, 8.13655e+006, 8.17161e+006, 8.20675e+006, 8.24199e+006, 8.27732e+006, 8.31274e+006, 8.34825e+006, 8.38385e+006, 8.41954e+006, 8.45532e+006, 8.49119e+006, 8.52716e+006, 8.56321e+006, 8.59936e+006, 8.6356e+006, 8.67193e+006, 8.70835e+006, 8.74486e+006, 8.78147e+006, 8.81816e+006, 8.85495e+006, 8.89183e+006, 8.9288e+006, 8.96587e+006, 9.00302e+006, 9.04027e+006, 9.07761e+006, 9.11505e+006, 9.15257e+006, 9.19019e+006, 9.2279e+006, 9.26571e+006, 9.3036e+006, 9.34159e+006, 9.37967e+006, 9.41785e+006, 9.45612e+006, 9.49448e+006, 9.53293e+006, 9.57148e+006, 9.61012e+006, 9.64886e+006, 9.68769e+006, 9.72661e+006, 9.76563e+006, 9.80473e+006, 9.84394e+006, 9.88323e+006, 9.92263e+006, 9.96211e+006, 1.00017e+007, 1.00414e+007, 1.00811e+007, 1.0121e+007, 1.01609e+007, 1.0201e+007, 1.02411e+007, 1.02814e+007, 1.03217e+007, 1.03622e+007, 1.04027e+007, 1.04433e+007, 1.0484e+007, 1.05248e+007, 1.05657e+007, 1.06067e+007, 1.06478e+007, 1.0689e+007, 1.07303e+007, 1.07717e+007, 1.08132e+007, 1.08547e+007, 1.08964e+007, 1.09382e+007, 1.098e+007, 1.1022e+007, 1.1064e+007, 1.11062e+007, 1.11484e+007, 1.11908e+007, 1.12332e+007, 1.12757e+007, 1.13184e+007, 1.13611e+007, 1.14039e+007, 1.14468e+007, 1.14899e+007, 1.1533e+007, 1.15762e+007, 1.16195e+007, 1.16629e+007, 1.17064e+007, 1.175e+007, 1.17937e+007, 1.18375e+007, 1.18814e+007, 1.19254e+007, 1.19695e+007, 1.20136e+007, 1.20579e+007, 1.21023e+007, 1.21468e+007, 1.21913e+007, 1.2236e+007, 1.22808e+007, 1.23257e+007, 1.23706e+007, 1.24157e+007, 1.24609e+007, 1.25061e+007, 1.25515e+007, 1.25969e+007, 1.26425e+007, 1.26882e+007, 1.27339e+007, 1.27798e+007, 1.28257e+007, 1.28718e+007, 1.29179e+007, 1.29642e+007, 1.30105e+007, 1.3057e+007, 1.31035e+007, 1.31502e+007, 1.31969e+007, 1.32438e+007, 1.32907e+007, 1.33378e+007, 1.33849e+007, 1.34322e+007, 1.34795e+007, 1.3527e+007, 1.35745e+007, 1.36221e+007, 1.36699e+007, 1.37177e+007, 1.37657e+007, 1.38137e+007, 1.38619e+007, 1.39101e+007, 1.39585e+007, 1.40069e+007, 1.40555e+007, 1.41041e+007, 1.41529e+007, 1.42017e+007, 1.42507e+007, 1.42998e+007, 1.43489e+007, 1.43982e+007, 1.44475e+007, 1.4497e+007, 1.45465e+007, 1.45962e+007, 1.4646e+007, 1.46958e+007, 1.47458e+007, 1.47959e+007, 1.48461e+007, 1.48963e+007, 1.49467e+007, 1.49972e+007, 1.50478e+007, 1.50984e+007, 1.51492e+007, 1.52001e+007, 1.52511e+007, 1.53022e+007, 1.53534e+007, 1.54047e+007, 1.54561e+007, 1.55076e+007, 1.55592e+007, 1.56109e+007, 1.56627e+007, 1.57146e+007, 1.57667e+007, 1.58188e+007, 1.5871e+007, 1.59233e+007, 1.59758e+007, 1.60283e+007, 1.60809e+007, 1.61337e+007, 1.61865e+007, 1.62395e+007, 1.62925e+007, 1.63457e+007, 1.63989e+007, 1.64523e+007, 1.65058e+007, 1.65593e+007, 1.6613e+007, 1.66668e+007, 1.67207e+007, 1.67747e+007, 1.68288e+007, 1.6883e+007, 1.69373e+007, 1.69917e+007, 1.70462e+007, 1.71008e+007, 1.71555e+007, 1.72104e+007, 1.72653e+007, 1.73203e+007, 1.73755e+007, 1.74307e+007, 1.74861e+007, 1.75415e+007, 1.75971e+007, 1.76528e+007, 1.77085e+007, 1.77644e+007, 1.78204e+007, 1.78765e+007, 1.79327e+007, 1.7989e+007, 1.80454e+007, 1.81019e+007, 1.81586e+007, 1.82153e+007, 1.82721e+007, 1.83291e+007, 1.83861e+007, 1.84433e+007, 1.85005e+007, 1.85579e+007, 1.86154e+007, 1.86729e+007, 1.87306e+007, 1.87884e+007, 1.88463e+007, 1.89043e+007, 1.89624e+007, 1.90207e+007, 1.9079e+007, 1.91374e+007, 1.9196e+007, 1.92546e+007, 1.93134e+007, 1.93722e+007, 1.94312e+007, 1.94903e+007, 1.95495e+007, 1.96088e+007, 1.96682e+007, 1.97277e+007, 1.97873e+007, 1.9847e+007, 1.99069e+007, 1.99668e+007, 2.00268e+007, 2.0087e+007, 2.01473e+007, 2.02076e+007, 2.02681e+007, 2.03287e+007, 2.03894e+007, 2.04502e+007, 2.05111e+007, 2.05722e+007, 2.06333e+007, 2.06946e+007, 2.07559e+007, 2.08174e+007, 2.08789e+007, 2.09406e+007, 2.10024e+007, 2.10643e+007, 2.11263e+007, 2.11884e+007, 2.12507e+007, 2.1313e+007, 2.13755e+007, 2.1438e+007, 2.15007e+007, 2.15634e+007, 2.16263e+007, 2.16893e+007, 2.17524e+007, 2.18157e+007, 2.1879e+007, 2.19424e+007, 2.2006e+007, 2.20696e+007, 2.21334e+007, 2.21973e+007, 2.22612e+007, 2.23253e+007, 2.23896e+007, 2.24539e+007, 2.25183e+007, 2.25828e+007, 2.26475e+007, 2.27123e+007, 2.27771e+007, 2.28421e+007, 2.29072e+007, 2.29724e+007, 2.30377e+007, 2.31032e+007, 2.31687e+007, 2.32344e+007, 2.33001e+007, 2.3366e+007, 2.3432e+007, 2.34981e+007, 2.35643e+007, 2.36306e+007, 2.3697e+007, 2.37636e+007, 2.38303e+007, 2.3897e+007, 2.39639e+007, 2.40309e+007, 2.4098e+007, 2.41652e+007, 2.42326e+007, 2.43e+007, 2.43676e+007, 2.44352e+007, 2.4503e+007, 2.45709e+007, 2.46389e+007, 2.4707e+007, 2.47753e+007, 2.48436e+007, 2.49121e+007, 2.49806e+007, 2.50493e+007, 2.51181e+007, 2.5187e+007, 2.52561e+007, 2.53252e+007, 2.53944e+007, 2.54638e+007, 2.55333e+007, 2.56029e+007, 2.56726e+007, 2.57424e+007, 2.58123e+007, 2.58824e+007, 2.59525e+007, 2.60228e+007, 2.60932e+007, 2.61637e+007, 2.62343e+007, 2.63051e+007, 2.63759e+007, 2.64469e+007, 2.65179e+007, 2.65891e+007, 2.66604e+007, 2.67319e+007, 2.68034e+007, 2.6875e+007, 2.69468e+007, 2.70187e+007, 2.70907e+007, 2.71628e+007, 2.7235e+007, 2.73073e+007, 2.73798e+007, 2.74523e+007, 2.7525e+007, 2.75978e+007, 2.76707e+007, 2.77438e+007, 2.78169e+007, 2.78902e+007, 2.79636e+007, 2.8037e+007, 2.81107e+007, 2.81844e+007, 2.82582e+007, 2.83322e+007, 2.84062e+007, 2.84804e+007, 2.85547e+007, 2.86292e+007, 2.87037e+007, 2.87783e+007, 2.88531e+007, 2.8928e+007, 2.9003e+007, 2.90781e+007, 2.91533e+007, 2.92287e+007, 2.93042e+007, 2.93797e+007, 2.94555e+007, 2.95313e+007, 2.96072e+007, 2.96833e+007, 2.97594e+007, 2.98357e+007, 2.99121e+007, 2.99886e+007, 3.00653e+007, 3.0142e+007, 3.02189e+007, 3.02959e+007, 3.0373e+007, 3.04502e+007, 3.05276e+007, 3.0605e+007, 3.06826e+007, 3.07603e+007, 3.08381e+007, 3.09161e+007, 3.09941e+007, 3.10723e+007, 3.11506e+007, 3.1229e+007, 3.13075e+007, 3.13861e+007, 3.14649e+007, 3.15438e+007, 3.16228e+007, 3.17019e+007, 3.17811e+007, 3.18605e+007, 3.194e+007, 3.20195e+007, 3.20993e+007, 3.21791e+007, 3.2259e+007, 3.23391e+007, 3.24193e+007, 3.24996e+007, 3.258e+007, 3.26606e+007, 3.27412e+007, 3.2822e+007, 3.29029e+007, 3.29839e+007, 3.30651e+007, 3.31463e+007, 3.32277e+007, 3.33092e+007, 3.33908e+007, 3.34726e+007, 3.35544e+007, 3.36364e+007, 3.37185e+007, 3.38007e+007, 3.38831e+007, 3.39655e+007, 3.40481e+007, 3.41308e+007, 3.42136e+007, 3.42966e+007, 3.43796e+007, 3.44628e+007, 3.45461e+007, 3.46296e+007, 3.47131e+007, 3.47968e+007, 3.48806e+007, 3.49645e+007, 3.50485e+007, 3.51326e+007, 3.52169e+007, 3.53013e+007, 3.53858e+007, 3.54705e+007, 3.55552e+007, 3.56401e+007, 3.57251e+007, 3.58102e+007, 3.58954e+007, 3.59808e+007, 3.60663e+007, 3.61519e+007, 3.62376e+007, 3.63235e+007, 3.64095e+007, 3.64955e+007, 3.65818e+007, 3.66681e+007, 3.67546e+007, 3.68411e+007, 3.69279e+007, 3.70147e+007, 3.71016e+007, 3.71887e+007, 3.72759e+007, 3.73632e+007, 3.74507e+007, 3.75382e+007, 3.76259e+007, 3.77137e+007, 3.78016e+007, 3.78897e+007, 3.79779e+007, 3.80662e+007, 3.81546e+007, 3.82431e+007, 3.83318e+007, 3.84206e+007, 3.85095e+007, 3.85986e+007, 3.86877e+007, 3.8777e+007, 3.88664e+007, 3.8956e+007, 3.90456e+007, 3.91354e+007, 3.92253e+007, 3.93153e+007, 3.94055e+007, 3.94958e+007, 3.95862e+007, 3.96767e+007, 3.97673e+007, 3.98581e+007, 3.9949e+007, 4.004e+007, 4.01312e+007, 4.02224e+007, 4.03138e+007, 4.04053e+007, 4.0497e+007, 4.05888e+007, 4.06806e+007, 4.07727e+007, 4.08648e+007, 4.09571e+007, 4.10495e+007, 4.1142e+007, 4.12346e+007, 4.13274e+007, 4.14203e+007, 4.15133e+007, 4.16064e+007, 4.16997e+007, 4.17931e+007, 4.18866e+007, 4.19803e+007, 4.2074e+007, 4.21679e+007, 4.22619e+007, 4.23561e+007, 4.24504e+007, 4.25448e+007, 4.26393e+007, 4.27339e+007, 4.28287e+007, 4.29236e+007, 4.30186e+007, 4.31138e+007, 4.32091e+007, 4.33045e+007, 4.34e+007, 4.34957e+007, 4.35914e+007, 4.36873e+007, 4.37834e+007, 4.38795e+007, 4.39758e+007, 4.40723e+007, 4.41688e+007, 4.42655e+007, 4.43623e+007, 4.44592e+007, 4.45562e+007, 4.46534e+007, 4.47507e+007, 4.48482e+007, 4.49457e+007, 4.50434e+007, 4.51412e+007, 4.52392e+007, 4.53372e+007, 4.54354e+007, 4.55337e+007, 4.56322e+007, 4.57308e+007, 4.58295e+007, 4.59283e+007, 4.60273e+007, 4.61264e+007, 4.62256e+007, 4.63249e+007, 4.64244e+007, 4.6524e+007, 4.66237e+007, 4.67236e+007, 4.68236e+007, 4.69237e+007, 4.70239e+007, 4.71243e+007, 4.72248e+007, 4.73254e+007, 4.74262e+007, 4.75271e+007, 4.76281e+007, 4.77292e+007, 4.78305e+007, 4.79319e+007, 4.80334e+007, 4.81351e+007, 4.82369e+007, 4.83388e+007, 4.84408e+007, 4.8543e+007, 4.86453e+007, 4.87478e+007, 4.88503e+007, 4.8953e+007, 4.90558e+007, 4.91588e+007, 4.92619e+007, 4.93651e+007, 4.94684e+007, 4.95719e+007, 4.96755e+007, 4.97792e+007, 4.98831e+007, 4.99871e+007, 5.00912e+007, 5.01954e+007, 5.02998e+007, 5.04043e+007, 5.05089e+007, 5.06137e+007, 5.07186e+007, 5.08236e+007, 5.09288e+007, 5.10341e+007, 5.11395e+007, 5.12451e+007, 5.13507e+007, 5.14566e+007, 5.15625e+007, 5.16686e+007, 5.17748e+007, 5.18811e+007, 5.19876e+007, 5.20942e+007, 5.22009e+007, 5.23078e+007, 5.24148e+007, 5.25219e+007, 5.26291e+007, 5.27365e+007, 5.2844e+007, 5.29517e+007, 5.30595e+007, 5.31674e+007, 5.32754e+007, 5.33836e+007, 5.34919e+007, 5.36003e+007, 5.37089e+007, 5.38176e+007, 5.39264e+007, 5.40354e+007, 5.41445e+007, 5.42537e+007, 5.43631e+007, 5.44726e+007, 5.45822e+007, 5.46919e+007, 5.48018e+007, 5.49119e+007, 5.5022e+007, 5.51323e+007, 5.52427e+007, 5.53533e+007, 5.5464e+007, 5.55748e+007, 5.56857e+007, 5.57968e+007, 5.5908e+007, 5.60194e+007, 5.61308e+007, 5.62425e+007, 5.63542e+007, 5.64661e+007, 5.65781e+007, 5.66903e+007, 5.68025e+007, 5.69149e+007, 5.70275e+007, 5.71402e+007, 5.7253e+007, 5.73659e+007, 5.7479e+007, 5.75922e+007, 5.77056e+007, 5.78191e+007, 5.79327e+007, 5.80464e+007, 5.81603e+007, 5.82743e+007, 5.83885e+007, 5.85028e+007, 5.86172e+007, 5.87317e+007, 5.88464e+007, 5.89612e+007, 5.90762e+007, 5.91913e+007, 5.93065e+007, 5.94219e+007, 5.95374e+007, 5.9653e+007, 5.97688e+007, 5.98847e+007, 6.00007e+007, 6.01169e+007, 6.02332e+007, 6.03496e+007, 6.04662e+007, 6.05829e+007, 6.06997e+007, 6.08167e+007, 6.09338e+007, 6.10511e+007, 6.11684e+007, 6.1286e+007, 6.14036e+007, 6.15214e+007, 6.16393e+007, 6.17574e+007, 6.18756e+007, 6.19939e+007, 6.21124e+007, 6.2231e+007, 6.23497e+007, 6.24686e+007, 6.25876e+007, 6.27068e+007, 6.2826e+007, 6.29455e+007, 6.3065e+007, 6.31847e+007, 6.33045e+007, 6.34245e+007, 6.35446e+007, 6.36648e+007, 6.37852e+007, 6.39057e+007, 6.40264e+007, 6.41471e+007, 6.42681e+007, 6.43891e+007, 6.45103e+007, 6.46316e+007, 6.47531e+007, 6.48747e+007, 6.49964e+007, 6.51183e+007, 6.52403e+007, 6.53625e+007, 6.54848e+007, 6.56072e+007, 6.57298e+007, 6.58525e+007, 6.59753e+007, 6.60983e+007, 6.62214e+007, 6.63446e+007, 6.6468e+007, 6.65915e+007, 6.67152e+007, 6.6839e+007, 6.69629e+007, 6.7087e+007, 6.72112e+007, 6.73356e+007, 6.74601e+007, 6.75847e+007, 6.77094e+007, 6.78343e+007, 6.79594e+007, 6.80846e+007, 6.82099e+007, 6.83353e+007, 6.84609e+007, 6.85867e+007, 6.87125e+007, 6.88385e+007, 6.89647e+007, 6.9091e+007, 6.92174e+007, 6.9344e+007, 6.94707e+007, 6.95975e+007, 6.97245e+007, 6.98516e+007, 6.99789e+007, 7.01063e+007, 7.02338e+007, 7.03615e+007, 7.04893e+007, 7.06172e+007, 7.07453e+007, 7.08736e+007, 7.10019e+007, 7.11304e+007, 7.12591e+007, 7.13879e+007, 7.15168e+007, 7.16459e+007, 7.17751e+007, 7.19044e+007, 7.20339e+007, 7.21635e+007, 7.22933e+007, 7.24232e+007, 7.25533e+007, 7.26834e+007, 7.28138e+007, 7.29442e+007, 7.30748e+007, 7.32056e+007, 7.33365e+007, 7.34675e+007, 7.35987e+007, 7.373e+007, 7.38614e+007, 7.3993e+007, 7.41248e+007, 7.42566e+007, 7.43886e+007, 7.45208e+007, 7.46531e+007, 7.47855e+007, 7.49181e+007, 7.50508e+007, 7.51837e+007, 7.53167e+007, 7.54498e+007, 7.55831e+007, 7.57165e+007, 7.58501e+007, 7.59838e+007, 7.61176e+007, 7.62516e+007, 7.63857e+007, 7.652e+007, 7.66544e+007, 7.6789e+007, 7.69237e+007, 7.70585e+007, 7.71935e+007, 7.73286e+007, 7.74639e+007, 7.75993e+007, 7.77348e+007, 7.78705e+007, 7.80063e+007, 7.81423e+007, 7.82784e+007, 7.84147e+007, 7.8551e+007, 7.86876e+007, 7.88243e+007, 7.89611e+007, 7.90981e+007, 7.92352e+007, 7.93724e+007, 7.95098e+007, 7.96473e+007, 7.9785e+007, 7.99229e+007, 8.00608e+007, 8.01989e+007, 8.03372e+007, 8.04756e+007, 8.06141e+007, 8.07528e+007, 8.08916e+007, 8.10306e+007, 8.11697e+007, 8.13089e+007, 8.14483e+007, 8.15879e+007, 8.17275e+007, 8.18674e+007, 8.20073e+007, 8.21474e+007, 8.22877e+007, 8.24281e+007, 8.25686e+007, 8.27093e+007, 8.28502e+007, 8.29911e+007, 8.31322e+007, 8.32735e+007, 8.34149e+007, 8.35565e+007, 8.36982e+007, 8.384e+007, 8.3982e+007, 8.41241e+007, 8.42664e+007, 8.44088e+007, 8.45513e+007, 8.4694e+007, 8.48369e+007, 8.49799e+007, 8.5123e+007, 8.52663e+007, 8.54097e+007, 8.55533e+007, 8.5697e+007, 8.58409e+007, 8.59849e+007, 8.6129e+007, 8.62733e+007, 8.64178e+007, 8.65623e+007, 8.67071e+007, 8.68519e+007, 8.6997e+007, 8.71421e+007, 8.72874e+007, 8.74329e+007, 8.75785e+007, 8.77242e+007, 8.78701e+007, 8.80162e+007, 8.81623e+007, 8.83087e+007, 8.84551e+007, 8.86018e+007, 8.87485e+007, 8.88954e+007, 8.90425e+007, 8.91897e+007, 8.9337e+007, 8.94845e+007, 8.96322e+007, 8.978e+007, 8.99279e+007, 9.0076e+007, 9.02242e+007, 9.03726e+007, 9.05211e+007, 9.06697e+007, 9.08186e+007, 9.09675e+007, 9.11166e+007, 9.12659e+007, 9.14153e+007, 9.15648e+007, 9.17145e+007, 9.18643e+007, 9.20143e+007, 9.21644e+007, 9.23147e+007, 9.24651e+007, 9.26157e+007, 9.27664e+007, 9.29173e+007, 9.30683e+007, 9.32195e+007, 9.33708e+007, 9.35222e+007, 9.36738e+007, 9.38256e+007, 9.39775e+007, 9.41295e+007, 9.42817e+007, 9.4434e+007, 9.45865e+007, 9.47392e+007, 9.48919e+007, 9.50449e+007, 9.51979e+007, 9.53512e+007, 9.55045e+007, 9.56581e+007, 9.58117e+007, 9.59655e+007, 9.61195e+007, 9.62736e+007, 9.64279e+007, 9.65823e+007, 9.67368e+007, 9.68915e+007, 9.70464e+007, 9.72014e+007, 9.73565e+007, 9.75118e+007, 9.76673e+007, 9.78229e+007, 9.79786e+007, 9.81345e+007, 9.82906e+007, 9.84468e+007, 9.86031e+007, 9.87596e+007, 9.89162e+007, 9.9073e+007, 9.92299e+007, 9.9387e+007, 9.95443e+007, 9.97016e+007, 9.98592e+007, 1.00017e+008, 1.00175e+008, 1.00333e+008, 1.00491e+008, 1.00649e+008, 1.00807e+008, 1.00966e+008, 1.01125e+008, 1.01284e+008, 1.01443e+008, 1.01602e+008, 1.01761e+008, 1.01921e+008, 1.0208e+008, 1.0224e+008, 1.024e+008, 1.0256e+008, 1.0272e+008, 1.02881e+008, 1.03041e+008, 1.03202e+008, 1.03363e+008, 1.03524e+008, 1.03685e+008, 1.03846e+008, 1.04008e+008, 1.04169e+008, 1.04331e+008, 1.04493e+008, 1.04655e+008, 1.04817e+008, 1.04979e+008, 1.05142e+008, 1.05304e+008, 1.05467e+008, 1.0563e+008, 1.05793e+008, 1.05956e+008, 1.0612e+008, 1.06283e+008, 1.06447e+008, 1.06611e+008, 1.06775e+008, 1.06939e+008, 1.07103e+008, 1.07268e+008, 1.07432e+008, 1.07597e+008, 1.07762e+008, 1.07927e+008, 1.08092e+008, 1.08258e+008, 1.08423e+008, 1.08589e+008, 1.08755e+008, 1.0892e+008, 1.09087e+008, 1.09253e+008, 1.09419e+008, 1.09586e+008, 1.09753e+008, 1.09919e+008, 1.10086e+008, 1.10254e+008, 1.10421e+008, 1.10588e+008, 1.10756e+008, 1.10924e+008, 1.11092e+008, 1.1126e+008, 1.11428e+008, 1.11597e+008, 1.11765e+008, 1.11934e+008, 1.12103e+008, 1.12272e+008, 1.12441e+008, 1.1261e+008, 1.1278e+008, 1.12949e+008, 1.13119e+008, 1.13289e+008, 1.13459e+008, 1.13629e+008, 1.138e+008, 1.1397e+008, 1.14141e+008, 1.14312e+008, 1.14483e+008, 1.14654e+008, 1.14825e+008, 1.14997e+008, 1.15168e+008, 1.1534e+008, 1.15512e+008, 1.15684e+008, 1.15856e+008, 1.16029e+008, 1.16201e+008, 1.16374e+008, 1.16547e+008, 1.1672e+008, 1.16893e+008, 1.17066e+008, 1.1724e+008, 1.17413e+008, 1.17587e+008, 1.17761e+008, 1.17935e+008, 1.18109e+008, 1.18284e+008, 1.18458e+008, 1.18633e+008, 1.18808e+008, 1.18983e+008, 1.19158e+008, 1.19333e+008, 1.19509e+008, 1.19684e+008, 1.1986e+008, 1.20036e+008, 1.20212e+008, 1.20388e+008, 1.20565e+008, 1.20741e+008, 1.20918e+008, 1.21095e+008, 1.21272e+008, 1.21449e+008, 1.21626e+008, 1.21804e+008, 1.21981e+008, 1.22159e+008, 1.22337e+008, 1.22515e+008, 1.22693e+008, 1.22872e+008, 1.2305e+008, 1.23229e+008, 1.23408e+008, 1.23587e+008, 1.23766e+008, 1.23946e+008, 1.24125e+008, 1.24305e+008, 1.24484e+008, 1.24664e+008, 1.24845e+008, 1.25025e+008, 1.25205e+008, 1.25386e+008, 1.25567e+008, 1.25748e+008, 1.25929e+008, 1.2611e+008, 1.26291e+008, 1.26473e+008, 1.26654e+008, 1.26836e+008, 1.27018e+008, 1.272e+008, 1.27383e+008, 1.27565e+008, 1.27748e+008, 1.27931e+008, 1.28114e+008, 1.28297e+008, 1.2848e+008, 1.28663e+008, 1.28847e+008, 1.29031e+008, 1.29215e+008, 1.29399e+008, 1.29583e+008, 1.29767e+008, 1.29952e+008, 1.30136e+008, 1.30321e+008, 1.30506e+008, 1.30691e+008, 1.30877e+008, 1.31062e+008, 1.31248e+008, 1.31433e+008, 1.31619e+008, 1.31805e+008, 1.31992e+008, 1.32178e+008, 1.32365e+008, 1.32551e+008, 1.32738e+008, 1.32925e+008, 1.33112e+008, 1.333e+008, 1.33487e+008, 1.33675e+008, 1.33863e+008, 1.34051e+008, 1.34239e+008, 1.34427e+008, 1.34616e+008, 1.34804e+008, 1.34993e+008, 1.35182e+008, 1.35371e+008, 1.3556e+008, 1.3575e+008, 1.35939e+008, 1.36129e+008, 1.36319e+008, 1.36509e+008, 1.36699e+008, 1.3689e+008, 1.3708e+008, 1.37271e+008, 1.37462e+008, 1.37653e+008, 1.37844e+008, 1.38035e+008, 1.38227e+008, 1.38418e+008, 1.3861e+008, 1.38802e+008, 1.38994e+008, 1.39186e+008, 1.39379e+008, 1.39571e+008, 1.39764e+008, 1.39957e+008, 1.4015e+008, 1.40343e+008, 1.40537e+008, 1.4073e+008, 1.40924e+008, 1.41118e+008, 1.41312e+008, 1.41506e+008, 1.417e+008, 1.41895e+008, 1.4209e+008, 1.42284e+008, 1.42479e+008, 1.42675e+008, 1.4287e+008, 1.43065e+008, 1.43261e+008, 1.43457e+008, 1.43653e+008, 1.43849e+008, 1.44045e+008, 1.44241e+008, 1.44438e+008, 1.44635e+008, 1.44832e+008, 1.45029e+008, 1.45226e+008, 1.45423e+008, 1.45621e+008, 1.45819e+008, 1.46017e+008, 1.46215e+008, 1.46413e+008, 1.46611e+008, 1.4681e+008, 1.47008e+008, 1.47207e+008, 1.47406e+008, 1.47605e+008, 1.47805e+008, 1.48004e+008, 1.48204e+008, 1.48404e+008, 1.48604e+008, 1.48804e+008, 1.49004e+008, 1.49205e+008, 1.49405e+008, 1.49606e+008, 1.49807e+008, 1.50008e+008, 1.50209e+008, 1.50411e+008, 1.50612e+008, 1.50814e+008, 1.51016e+008, 1.51218e+008, 1.5142e+008, 1.51623e+008, 1.51825e+008, 1.52028e+008, 1.52231e+008, 1.52434e+008, 1.52637e+008, 1.52841e+008, 1.53044e+008, 1.53248e+008, 1.53452e+008, 1.53656e+008, 1.5386e+008, 1.54064e+008, 1.54269e+008, 1.54474e+008, 1.54678e+008, 1.54883e+008, 1.55089e+008, 1.55294e+008, 1.55499e+008, 1.55705e+008, 1.55911e+008, 1.56117e+008, 1.56323e+008, 1.56529e+008, 1.56736e+008, 1.56942e+008, 1.57149e+008, 1.57356e+008, 1.57563e+008, 1.57771e+008, 1.57978e+008, 1.58186e+008, 1.58394e+008, 1.58601e+008, 1.5881e+008, 1.59018e+008, 1.59226e+008, 1.59435e+008, 1.59644e+008, 1.59853e+008, 1.60062e+008, 1.60271e+008, 1.6048e+008, 1.6069e+008, 1.609e+008, 1.6111e+008, 1.6132e+008, 1.6153e+008, 1.6174e+008, 1.61951e+008, 1.62162e+008, 1.62373e+008, 1.62584e+008, 1.62795e+008, 1.63006e+008, 1.63218e+008, 1.6343e+008, 1.63641e+008, 1.63853e+008, 1.64066e+008, 1.64278e+008, 1.64491e+008, 1.64703e+008, 1.64916e+008, 1.65129e+008, 1.65342e+008, 1.65556e+008, 1.65769e+008, 1.65983e+008, 1.66197e+008, 1.66411e+008, 1.66625e+008, 1.6684e+008, 1.67054e+008, 1.67269e+008, 1.67484e+008, 1.67699e+008, 1.67914e+008, 1.68129e+008, 1.68345e+008, 1.6856e+008, 1.68776e+008, 1.68992e+008, 1.69208e+008, 1.69425e+008, 1.69641e+008, 1.69858e+008, 1.70075e+008, 1.70292e+008, 1.70509e+008, 1.70726e+008, 1.70944e+008, 1.71162e+008, 1.71379e+008, 1.71597e+008, 1.71816e+008, 1.72034e+008, 1.72253e+008, 1.72471e+008, 1.7269e+008, 1.72909e+008, 1.73128e+008, 1.73348e+008, 1.73567e+008, 1.73787e+008, 1.74007e+008, 1.74227e+008, 1.74447e+008, 1.74667e+008, 1.74888e+008, 1.75108e+008, 1.75329e+008, 1.7555e+008, 1.75771e+008, 1.75993e+008, 1.76214e+008, 1.76436e+008, 1.76658e+008, 1.7688e+008, 1.77102e+008, 1.77324e+008, 1.77547e+008, 1.77769e+008, 1.77992e+008, 1.78215e+008, 1.78439e+008, 1.78662e+008, 1.78885e+008, 1.79109e+008, 1.79333e+008, 1.79557e+008, 1.79781e+008, 1.80006e+008, 1.8023e+008, 1.80455e+008, 1.8068e+008, 1.80905e+008, 1.8113e+008, 1.81355e+008, 1.81581e+008, 1.81807e+008, 1.82032e+008, 1.82258e+008, 1.82485e+008, 1.82711e+008, 1.82938e+008, 1.83164e+008, 1.83391e+008, 1.83618e+008, 1.83845e+008, 1.84073e+008, 1.843e+008, 1.84528e+008, 1.84756e+008, 1.84984e+008, 1.85212e+008, 1.85441e+008, 1.85669e+008, 1.85898e+008, 1.86127e+008, 1.86356e+008, 1.86585e+008, 1.86815e+008, 1.87044e+008, 1.87274e+008, 1.87504e+008, 1.87734e+008, 1.87964e+008, 1.88195e+008, 1.88425e+008, 1.88656e+008, 1.88887e+008, 1.89118e+008, 1.89349e+008, 1.89581e+008, 1.89813e+008 };
						int n[2048] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8 };
						int an[2048];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 8) {
						float s_mean[4096] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 1132, 1133, 1134, 1135, 1136, 1137, 1138, 1139, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149, 1150, 1151, 1152, 1153, 1154, 1155, 1156, 1157, 1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187, 1188, 1189, 1190, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249, 1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259, 1260, 1261, 1262, 1263, 1264, 1265, 1266, 1267, 1268, 1269, 1270, 1271, 1272, 1273, 1274, 1275, 1276, 1277, 1278, 1279, 1280, 1281, 1282, 1283, 1284, 1285, 1286, 1287, 1288, 1289, 1290, 1291, 1292, 1293, 1294, 1295, 1296, 1297, 1298, 1299, 1300, 1301, 1302, 1303, 1304, 1305, 1306, 1307, 1308, 1309, 1310, 1311, 1312, 1313, 1314, 1315, 1316, 1317, 1318, 1319, 1320, 1321, 1322, 1323, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1332, 1333, 1334, 1335, 1336, 1337, 1338, 1339, 1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1371, 1372, 1373, 1374, 1375, 1376, 1377, 1378, 1379, 1380, 1381, 1382, 1383, 1384, 1385, 1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396, 1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405, 1406, 1407, 1408, 1409, 1410, 1411, 1412, 1413, 1414, 1415, 1416, 1417, 1418, 1419, 1420, 1421, 1422, 1423, 1424, 1425, 1426, 1427, 1428, 1429, 1430, 1431, 1432, 1433, 1434, 1435, 1436, 1437, 1438, 1439, 1440, 1441, 1442, 1443, 1444, 1445, 1446, 1447, 1448, 1449, 1450, 1451, 1452, 1453, 1454, 1455, 1456, 1457, 1458, 1459, 1460, 1461, 1462, 1463, 1464, 1465, 1466, 1467, 1468, 1469, 1470, 1471, 1472, 1473, 1474, 1475, 1476, 1477, 1478, 1479, 1480, 1481, 1482, 1483, 1484, 1485, 1486, 1487, 1488, 1489, 1490, 1491, 1492, 1493, 1494, 1495, 1496, 1497, 1498, 1499, 1500, 1501, 1502, 1503, 1504, 1505, 1506, 1507, 1508, 1509, 1510, 1511, 1512, 1513, 1514, 1515, 1516, 1517, 1518, 1519, 1520, 1521, 1522, 1523, 1524, 1525, 1526, 1527, 1528, 1529, 1530, 1531, 1532, 1533, 1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545, 1546, 1547, 1548, 1549, 1550, 1551, 1552, 1553, 1554, 1555, 1556, 1557, 1558, 1559, 1560, 1561, 1562, 1563, 1564, 1565, 1566, 1567, 1568, 1569, 1570, 1571, 1572, 1573, 1574, 1575, 1576, 1577, 1578, 1579, 1580, 1581, 1582, 1583, 1584, 1585, 1586, 1587, 1588, 1589, 1590, 1591, 1592, 1593, 1594, 1595, 1596, 1597, 1598, 1599, 1600, 1601, 1602, 1603, 1604, 1605, 1606, 1607, 1608, 1609, 1610, 1611, 1612, 1613, 1614, 1615, 1616, 1617, 1618, 1619, 1620, 1621, 1622, 1623, 1624, 1625, 1626, 1627, 1628, 1629, 1630, 1631, 1632, 1633, 1634, 1635, 1636, 1637, 1638, 1639, 1640, 1641, 1642, 1643, 1644, 1645, 1646, 1647, 1648, 1649, 1650, 1651, 1652, 1653, 1654, 1655, 1656, 1657, 1658, 1659, 1660, 1661, 1662, 1663, 1664, 1665, 1666, 1667, 1668, 1669, 1670, 1671, 1672, 1673, 1674, 1675, 1676, 1677, 1678, 1679, 1680, 1681, 1682, 1683, 1684, 1685, 1686, 1687, 1688, 1689, 1690, 1691, 1692, 1693, 1694, 1695, 1696, 1697, 1698, 1699, 1700, 1701, 1702, 1703, 1704, 1705, 1706, 1707, 1708, 1709, 1710, 1711, 1712, 1713, 1714, 1715, 1716, 1717, 1718, 1719, 1720, 1721, 1722, 1723, 1724, 1725, 1726, 1727, 1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736, 1737, 1738, 1739, 1740, 1741, 1742, 1743, 1744, 1745, 1746, 1747, 1748, 1749, 1750, 1751, 1752, 1753, 1754, 1755, 1756, 1757, 1758, 1759, 1760, 1761, 1762, 1763, 1764, 1765, 1766, 1767, 1768, 1769, 1770, 1771, 1772, 1773, 1774, 1775, 1776, 1777, 1778, 1779, 1780, 1781, 1782, 1783, 1784, 1785, 1786, 1787, 1788, 1789, 1790, 1791, 1792, 1793, 1794, 1795, 1796, 1797, 1798, 1799, 1800, 1801, 1802, 1803, 1804, 1805, 1806, 1807, 1808, 1809, 1810, 1811, 1812, 1813, 1814, 1815, 1816, 1817, 1818, 1819, 1820, 1821, 1822, 1823, 1824, 1825, 1826, 1827, 1828, 1829, 1830, 1831, 1832, 1833, 1834, 1835, 1836, 1837, 1838, 1839, 1840, 1841, 1842, 1843, 1844, 1845, 1846, 1847, 1848, 1849, 1850, 1851, 1852, 1853, 1854, 1855, 1856, 1857, 1858, 1859, 1860, 1861, 1862, 1863, 1864, 1865, 1866, 1867, 1868, 1869, 1870, 1871, 1872, 1873, 1874, 1875, 1876, 1877, 1878, 1879, 1880, 1881, 1882, 1883, 1884, 1885, 1886, 1887, 1888, 1889, 1890, 1891, 1892, 1893, 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909, 1910, 1911, 1912, 1913, 1914, 1915, 1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049, 2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2059, 2060, 2061, 2062, 2063, 2064, 2065, 2066, 2067, 2068, 2069, 2070, 2071, 2072, 2073, 2074, 2075, 2076, 2077, 2078, 2079, 2080, 2081, 2082, 2083, 2084, 2085, 2086, 2087, 2088, 2089, 2090, 2091, 2092, 2093, 2094, 2095, 2096, 2097, 2098, 2099, 2100, 2101, 2102, 2103, 2104, 2105, 2106, 2107, 2108, 2109, 2110, 2111, 2112, 2113, 2114, 2115, 2116, 2117, 2118, 2119, 2120, 2121, 2122, 2123, 2124, 2125, 2126, 2127, 2128, 2129, 2130, 2131, 2132, 2133, 2134, 2135, 2136, 2137, 2138, 2139, 2140, 2141, 2142, 2143, 2144, 2145, 2146, 2147, 2148, 2149, 2150, 2151, 2152, 2153, 2154, 2155, 2156, 2157, 2158, 2159, 2160, 2161, 2162, 2163, 2164, 2165, 2166, 2167, 2168, 2169, 2170, 2171, 2172, 2173, 2174, 2175, 2176, 2177, 2178, 2179, 2180, 2181, 2182, 2183, 2184, 2185, 2186, 2187, 2188, 2189, 2190, 2191, 2192, 2193, 2194, 2195, 2196, 2197, 2198, 2199, 2200, 2201, 2202, 2203, 2204, 2205, 2206, 2207, 2208, 2209, 2210, 2211, 2212, 2213, 2214, 2215, 2216, 2217, 2218, 2219, 2220, 2221, 2222, 2223, 2224, 2225, 2226, 2227, 2228, 2229, 2230, 2231, 2232, 2233, 2234, 2235, 2236, 2237, 2238, 2239, 2240, 2241, 2242, 2243, 2244, 2245, 2246, 2247, 2248, 2249, 2250, 2251, 2252, 2253, 2254, 2255, 2256, 2257, 2258, 2259, 2260, 2261, 2262, 2263, 2264, 2265, 2266, 2267, 2268, 2269, 2270, 2271, 2272, 2273, 2274, 2275, 2276, 2277, 2278, 2279, 2280, 2281, 2282, 2283, 2284, 2285, 2286, 2287, 2288, 2289, 2290, 2291, 2292, 2293, 2294, 2295, 2296, 2297, 2298, 2299, 2300, 2301, 2302, 2303, 2304, 2305, 2306, 2307, 2308, 2309, 2310, 2311, 2312, 2313, 2314, 2315, 2316, 2317, 2318, 2319, 2320, 2321, 2322, 2323, 2324, 2325, 2326, 2327, 2328, 2329, 2330, 2331, 2332, 2333, 2334, 2335, 2336, 2337, 2338, 2339, 2340, 2341, 2342, 2343, 2344, 2345, 2346, 2347, 2348, 2349, 2350, 2351, 2352, 2353, 2354, 2355, 2356, 2357, 2358, 2359, 2360, 2361, 2362, 2363, 2364, 2365, 2366, 2367, 2368, 2369, 2370, 2371, 2372, 2373, 2374, 2375, 2376, 2377, 2378, 2379, 2380, 2381, 2382, 2383, 2384, 2385, 2386, 2387, 2388, 2389, 2390, 2391, 2392, 2393, 2394, 2395, 2396, 2397, 2398, 2399, 2400, 2401, 2402, 2403, 2404, 2405, 2406, 2407, 2408, 2409, 2410, 2411, 2412, 2413, 2414, 2415, 2416, 2417, 2418, 2419, 2420, 2421, 2422, 2423, 2424, 2425, 2426, 2427, 2428, 2429, 2430, 2431, 2432, 2433, 2434, 2435, 2436, 2437, 2438, 2439, 2440, 2441, 2442, 2443, 2444, 2445, 2446, 2447, 2448, 2449, 2450, 2451, 2452, 2453, 2454, 2455, 2456, 2457, 2458, 2459, 2460, 2461, 2462, 2463, 2464, 2465, 2466, 2467, 2468, 2469, 2470, 2471, 2472, 2473, 2474, 2475, 2476, 2477, 2478, 2479, 2480, 2481, 2482, 2483, 2484, 2485, 2486, 2487, 2488, 2489, 2490, 2491, 2492, 2493, 2494, 2495, 2496, 2497, 2498, 2499, 2500, 2501, 2502, 2503, 2504, 2505, 2506, 2507, 2508, 2509, 2510, 2511, 2512, 2513, 2514, 2515, 2516, 2517, 2518, 2519, 2520, 2521, 2522, 2523, 2524, 2525, 2526, 2527, 2528, 2529, 2530, 2531, 2532, 2533, 2534, 2535, 2536, 2537, 2538, 2539, 2540, 2541, 2542, 2543, 2544, 2545, 2546, 2547, 2548, 2549, 2550, 2551, 2552, 2553, 2554, 2555, 2556, 2557, 2558, 2559, 2560, 2561, 2562, 2563, 2564, 2565, 2566, 2567, 2568, 2569, 2570, 2571, 2572, 2573, 2574, 2575, 2576, 2577, 2578, 2579, 2580, 2581, 2582, 2583, 2584, 2585, 2586, 2587, 2588, 2589, 2590, 2591, 2592, 2593, 2594, 2595, 2596, 2597, 2598, 2599, 2600, 2601, 2602, 2603, 2604, 2605, 2606, 2607, 2608, 2609, 2610, 2611, 2612, 2613, 2614, 2615, 2616, 2617, 2618, 2619, 2620, 2621, 2622, 2623, 2624, 2625, 2626, 2627, 2628, 2629, 2630, 2631, 2632, 2633, 2634, 2635, 2636, 2637, 2638, 2639, 2640, 2641, 2642, 2643, 2644, 2645, 2646, 2647, 2648, 2649, 2650, 2651, 2652, 2653, 2654, 2655, 2656, 2657, 2658, 2659, 2660, 2661, 2662, 2663, 2664, 2665, 2666, 2667, 2668, 2669, 2670, 2671, 2672, 2673, 2674, 2675, 2676, 2677, 2678, 2679, 2680, 2681, 2682, 2683, 2684, 2685, 2686, 2687, 2688, 2689, 2690, 2691, 2692, 2693, 2694, 2695, 2696, 2697, 2698, 2699, 2700, 2701, 2702, 2703, 2704, 2705, 2706, 2707, 2708, 2709, 2710, 2711, 2712, 2713, 2714, 2715, 2716, 2717, 2718, 2719, 2720, 2721, 2722, 2723, 2724, 2725, 2726, 2727, 2728, 2729, 2730, 2731, 2732, 2733, 2734, 2735, 2736, 2737, 2738, 2739, 2740, 2741, 2742, 2743, 2744, 2745, 2746, 2747, 2748, 2749, 2750, 2751, 2752, 2753, 2754, 2755, 2756, 2757, 2758, 2759, 2760, 2761, 2762, 2763, 2764, 2765, 2766, 2767, 2768, 2769, 2770, 2771, 2772, 2773, 2774, 2775, 2776, 2777, 2778, 2779, 2780, 2781, 2782, 2783, 2784, 2785, 2786, 2787, 2788, 2789, 2790, 2791, 2792, 2793, 2794, 2795, 2796, 2797, 2798, 2799, 2800, 2801, 2802, 2803, 2804, 2805, 2806, 2807, 2808, 2809, 2810, 2811, 2812, 2813, 2814, 2815, 2816, 2817, 2818, 2819, 2820, 2821, 2822, 2823, 2824, 2825, 2826, 2827, 2828, 2829, 2830, 2831, 2832, 2833, 2834, 2835, 2836, 2837, 2838, 2839, 2840, 2841, 2842, 2843, 2844, 2845, 2846, 2847, 2848, 2849, 2850, 2851, 2852, 2853, 2854, 2855, 2856, 2857, 2858, 2859, 2860, 2861, 2862, 2863, 2864, 2865, 2866, 2867, 2868, 2869, 2870, 2871, 2872, 2873, 2874, 2875, 2876, 2877, 2878, 2879, 2880, 2881, 2882, 2883, 2884, 2885, 2886, 2887, 2888, 2889, 2890, 2891, 2892, 2893, 2894, 2895, 2896, 2897, 2898, 2899, 2900, 2901, 2902, 2903, 2904, 2905, 2906, 2907, 2908, 2909, 2910, 2911, 2912, 2913, 2914, 2915, 2916, 2917, 2918, 2919, 2920, 2921, 2922, 2923, 2924, 2925, 2926, 2927, 2928, 2929, 2930, 2931, 2932, 2933, 2934, 2935, 2936, 2937, 2938, 2939, 2940, 2941, 2942, 2943, 2944, 2945, 2946, 2947, 2948, 2949, 2950, 2951, 2952, 2953, 2954, 2955, 2956, 2957, 2958, 2959, 2960, 2961, 2962, 2963, 2964, 2965, 2966, 2967, 2968, 2969, 2970, 2971, 2972, 2973, 2974, 2975, 2976, 2977, 2978, 2979, 2980, 2981, 2982, 2983, 2984, 2985, 2986, 2987, 2988, 2989, 2990, 2991, 2992, 2993, 2994, 2995, 2996, 2997, 2998, 2999, 3000, 3001, 3002, 3003, 3004, 3005, 3006, 3007, 3008, 3009, 3010, 3011, 3012, 3013, 3014, 3015, 3016, 3017, 3018, 3019, 3020, 3021, 3022, 3023, 3024, 3025, 3026, 3027, 3028, 3029, 3030, 3031, 3032, 3033, 3034, 3035, 3036, 3037, 3038, 3039, 3040, 3041, 3042, 3043, 3044, 3045, 3046, 3047, 3048, 3049, 3050, 3051, 3052, 3053, 3054, 3055, 3056, 3057, 3058, 3059, 3060, 3061, 3062, 3063, 3064, 3065, 3066, 3067, 3068, 3069, 3070, 3071, 3072, 3073, 3074, 3075, 3076, 3077, 3078, 3079, 3080, 3081, 3082, 3083, 3084, 3085, 3086, 3087, 3088, 3089, 3090, 3091, 3092, 3093, 3094, 3095, 3096, 3097, 3098, 3099, 3100, 3101, 3102, 3103, 3104, 3105, 3106, 3107, 3108, 3109, 3110, 3111, 3112, 3113, 3114, 3115, 3116, 3117, 3118, 3119, 3120, 3121, 3122, 3123, 3124, 3125, 3126, 3127, 3128, 3129, 3130, 3131, 3132, 3133, 3134, 3135, 3136, 3137, 3138, 3139, 3140, 3141, 3142, 3143, 3144, 3145, 3146, 3147, 3148, 3149, 3150, 3151, 3152, 3153, 3154, 3155, 3156, 3157, 3158, 3159, 3160, 3161, 3162, 3163, 3164, 3165, 3166, 3167, 3168, 3169, 3170, 3171, 3172, 3173, 3174, 3175, 3176, 3177, 3178, 3179, 3180, 3181, 3182, 3183, 3184, 3185, 3186, 3187, 3188, 3189, 3190, 3191, 3192, 3193, 3194, 3195, 3196, 3197, 3198, 3199, 3200, 3201, 3202, 3203, 3204, 3205, 3206, 3207, 3208, 3209, 3210, 3211, 3212, 3213, 3214, 3215, 3216, 3217, 3218, 3219, 3220, 3221, 3222, 3223, 3224, 3225, 3226, 3227, 3228, 3229, 3230, 3231, 3232, 3233, 3234, 3235, 3236, 3237, 3238, 3239, 3240, 3241, 3242, 3243, 3244, 3245, 3246, 3247, 3248, 3249, 3250, 3251, 3252, 3253, 3254, 3255, 3256, 3257, 3258, 3259, 3260, 3261, 3262, 3263, 3264, 3265, 3266, 3267, 3268, 3269, 3270, 3271, 3272, 3273, 3274, 3275, 3276, 3277, 3278, 3279, 3280, 3281, 3282, 3283, 3284, 3285, 3286, 3287, 3288, 3289, 3290, 3291, 3292, 3293, 3294, 3295, 3296, 3297, 3298, 3299, 3300, 3301, 3302, 3303, 3304, 3305, 3306, 3307, 3308, 3309, 3310, 3311, 3312, 3313, 3314, 3315, 3316, 3317, 3318, 3319, 3320, 3321, 3322, 3323, 3324, 3325, 3326, 3327, 3328, 3329, 3330, 3331, 3332, 3333, 3334, 3335, 3336, 3337, 3338, 3339, 3340, 3341, 3342, 3343, 3344, 3345, 3346, 3347, 3348, 3349, 3350, 3351, 3352, 3353, 3354, 3355, 3356, 3357, 3358, 3359, 3360, 3361, 3362, 3363, 3364, 3365, 3366, 3367, 3368, 3369, 3370, 3371, 3372, 3373, 3374, 3375, 3376, 3377, 3378, 3379, 3380, 3381, 3382, 3383, 3384, 3385, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396, 3397, 3398, 3399, 3400, 3401, 3402, 3403, 3404, 3405, 3406, 3407, 3408, 3409, 3410, 3411, 3412, 3413, 3414, 3415, 3416, 3417, 3418, 3419, 3420, 3421, 3422, 3423, 3424, 3425, 3426, 3427, 3428, 3429, 3430, 3431, 3432, 3433, 3434, 3435, 3436, 3437, 3438, 3439, 3440, 3441, 3442, 3443, 3444, 3445, 3446, 3447, 3448, 3449, 3450, 3451, 3452, 3453, 3454, 3455, 3456, 3457, 3458, 3459, 3460, 3461, 3462, 3463, 3464, 3465, 3466, 3467, 3468, 3469, 3470, 3471, 3472, 3473, 3474, 3475, 3476, 3477, 3478, 3479, 3480, 3481, 3482, 3483, 3484, 3485, 3486, 3487, 3488, 3489, 3490, 3491, 3492, 3493, 3494, 3495, 3496, 3497, 3498, 3499, 3500, 3501, 3502, 3503, 3504, 3505, 3506, 3507, 3508, 3509, 3510, 3511, 3512, 3513, 3514, 3515, 3516, 3517, 3518, 3519, 3520, 3521, 3522, 3523, 3524, 3525, 3526, 3527, 3528, 3529, 3530, 3531, 3532, 3533, 3534, 3535, 3536, 3537, 3538, 3539, 3540, 3541, 3542, 3543, 3544, 3545, 3546, 3547, 3548, 3549, 3550, 3551, 3552, 3553, 3554, 3555, 3556, 3557, 3558, 3559, 3560, 3561, 3562, 3563, 3564, 3565, 3566, 3567, 3568, 3569, 3570, 3571, 3572, 3573, 3574, 3575, 3576, 3577, 3578, 3579, 3580, 3581, 3582, 3583, 3584, 3585, 3586, 3587, 3588, 3589, 3590, 3591, 3592, 3593, 3594, 3595, 3596, 3597, 3598, 3599, 3600, 3601, 3602, 3603, 3604, 3605, 3606, 3607, 3608, 3609, 3610, 3611, 3612, 3613, 3614, 3615, 3616, 3617, 3618, 3619, 3620, 3621, 3622, 3623, 3624, 3625, 3626, 3627, 3628, 3629, 3630, 3631, 3632, 3633, 3634, 3635, 3636, 3637, 3638, 3639, 3640, 3641, 3642, 3643, 3644, 3645, 3646, 3647, 3648, 3649, 3650, 3651, 3652, 3653, 3654, 3655, 3656, 3657, 3658, 3659, 3660, 3661, 3662, 3663, 3664, 3665, 3666, 3667, 3668, 3669, 3670, 3671, 3672, 3673, 3674, 3675, 3676, 3677, 3678, 3679, 3680, 3681, 3682, 3683, 3684, 3685, 3686, 3687, 3688, 3689, 3690, 3691, 3692, 3693, 3694, 3695, 3696, 3697, 3698, 3699, 3700, 3701, 3702, 3703, 3704, 3705, 3706, 3707, 3708, 3709, 3710, 3711, 3712, 3713, 3714, 3715, 3716, 3717, 3718, 3719, 3720, 3721, 3722, 3723, 3724, 3725, 3726, 3727, 3728, 3729, 3730, 3731, 3732, 3733, 3734, 3735, 3736, 3737, 3738, 3739, 3740, 3741, 3742, 3743, 3744, 3745, 3746, 3747, 3748, 3749, 3750, 3751, 3752, 3753, 3754, 3755, 3756, 3757, 3758, 3759, 3760, 3761, 3762, 3763, 3764, 3765, 3766, 3767, 3768, 3769, 3770, 3771, 3772, 3773, 3774, 3775, 3776, 3777, 3778, 3779, 3780, 3781, 3782, 3783, 3784, 3785, 3786, 3787, 3788, 3789, 3790, 3791, 3792, 3793, 3794, 3795, 3796, 3797, 3798, 3799, 3800, 3801, 3802, 3803, 3804, 3805, 3806, 3807, 3808, 3809, 3810, 3811, 3812, 3813, 3814, 3815, 3816, 3817, 3818, 3819, 3820, 3821, 3822, 3823, 3824, 3825, 3826, 3827, 3828, 3829, 3830, 3831, 3832, 3833, 3834, 3835, 3836, 3837, 3838, 3839, 3840, 3841, 3842, 3843, 3844, 3845, 3846, 3847, 3848, 3849, 3850, 3851, 3852, 3853, 3854, 3855, 3856, 3857, 3858, 3859, 3860, 3861, 3862, 3863, 3864, 3865, 3866, 3867, 3868, 3869, 3870, 3871, 3872, 3873, 3874, 3875, 3876, 3877, 3878, 3879, 3880, 3881, 3882, 3883, 3884, 3885, 3886, 3887, 3888, 3889, 3890, 3891, 3892, 3893, 3894, 3895, 3896, 3897, 3898, 3899, 3900, 3901, 3902, 3903, 3904, 3905, 3906, 3907, 3908, 3909, 3910, 3911, 3912, 3913, 3914, 3915, 3916, 3917, 3918, 3919, 3920, 3921, 3922, 3923, 3924, 3925, 3926, 3927, 3928, 3929, 3930, 3931, 3932, 3933, 3934, 3935, 3936, 3937, 3938, 3939, 3940, 3941, 3942, 3943, 3944, 3945, 3946, 3947, 3948, 3949, 3950, 3951, 3952, 3953, 3954, 3955, 3956, 3957, 3958, 3959, 3960, 3961, 3962, 3963, 3964, 3965, 3966, 3967, 3968, 3969, 3970, 3971, 3972, 3973, 3974, 3975, 3976, 3977, 3978, 3979, 3980, 3981, 3982, 3983, 3984, 3985, 3986, 3987, 3988, 3989, 3990, 3991, 3992, 3993, 3994, 3995, 3996, 3997, 3998, 3999, 4000, 4001, 4002, 4003, 4004, 4005, 4006, 4007, 4008, 4009, 4010, 4011, 4012, 4013, 4014, 4015, 4016, 4017, 4018, 4019, 4020, 4021, 4022, 4023, 4024, 4025, 4026, 4027, 4028, 4029, 4030, 4031, 4032, 4033, 4034, 4035, 4036, 4037, 4038, 4039, 4040, 4041, 4042, 4043, 4044, 4045, 4046, 4047, 4048, 4049, 4050, 4051, 4052, 4053, 4054, 4055, 4056, 4057, 4058, 4059, 4060, 4061, 4062, 4063, 4064, 4065, 4066, 4067, 4068, 4069, 4070, 4071, 4072, 4073, 4074, 4075, 4076, 4077, 4078, 4079, 4080, 4081, 4082, 4083, 4084, 4085, 4086, 4087, 4088, 4089, 4090, 4091, 4092, 4093, 4094, 4095, 4096 };
						float s_var[4096] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024, 1191.58, 1374.62, 1573.56, 1788.85, 2020.92, 2270.16, 2536.99, 2821.81, 3125, 3446.94, 3788, 4148.54, 4528.92, 4929.5, 5350.62, 5792.62, 6255.83, 6740.58, 7247.2, 7776, 8327.3, 8901.41, 9498.64, 10119.3, 10763.7, 11432, 12124.7, 12842, 13584.1, 14351.4, 15144.1, 15962.6, 16807, 17677.7, 18574.9, 19498.8, 20449.8, 21428.1, 22434, 23467.7, 24529.4, 25619.5, 26738.1, 27885.5, 29061.9, 30267.7, 31503, 32768, 34063, 35388.3, 36744, 38130.5, 39547.8, 40996.3, 42476.2, 43987.7, 45531, 47106.3, 48713.9, 50354, 52026.8, 53732.4, 55471.2, 57243.3, 59049, 60888.4, 62761.8, 64669.3, 66611.2, 68587.7, 70598.9, 72645.2, 74726.6, 76843.3, 78995.7, 81183.8, 83407.9, 85668.2, 87964.8, 90298, 92667.9, 95074.7, 97518.7, 100000, 102519, 105075, 107670, 110302, 112973, 115682, 118429, 121216, 124041, 126906, 129810, 132753, 135736, 138759, 141822, 144926, 148069, 151253, 154478, 157744, 161051, 164399, 167789, 171220, 174693, 178208, 181765, 185364, 189005, 192690, 196417, 200187, 203999, 207856, 211755, 215699, 219686, 223716, 227791, 231910, 236074, 240282, 244534, 248832, 253175, 257562, 261995, 266474, 270998, 275568, 280183, 284845, 289553, 294308, 299109, 303957, 308851, 313793, 318781, 323817, 328901, 334032, 339210, 344437, 349711, 355034, 360405, 365825, 371293, 376810, 382376, 387991, 393655, 399368, 405131, 410943, 416805, 422717, 428679, 434692, 440754, 446867, 453031, 459245, 465510, 471827, 478194, 484613, 491083, 497604, 504177, 510803, 517480, 524209, 530990, 537824, 544710, 551649, 558641, 565685, 572783, 579934, 587138, 594395, 601706, 609071, 616490, 623962, 631489, 639070, 646705, 654395, 662139, 669938, 677792, 685700, 693664, 701683, 709758, 717888, 726074, 734315, 742612, 750966, 759375, 767841, 776363, 784941, 793576, 802268, 811017, 819823, 828686, 837606, 846583, 855618, 864711, 873861, 883069, 892335, 901660, 911042, 920483, 929982, 939540, 949156, 958832, 968566, 978359, 988212, 998124, 1.00809e+006, 1.01813e+006, 1.02822e+006, 1.03837e+006, 1.04858e+006, 1.05885e+006, 1.06918e+006, 1.07957e+006, 1.09002e+006, 1.10053e+006, 1.1111e+006, 1.12173e+006, 1.13243e+006, 1.14318e+006, 1.154e+006, 1.16487e+006, 1.17581e+006, 1.18681e+006, 1.19787e+006, 1.20899e+006, 1.22018e+006, 1.23142e+006, 1.24273e+006, 1.2541e+006, 1.26553e+006, 1.27703e+006, 1.28858e+006, 1.3002e+006, 1.31188e+006, 1.32363e+006, 1.33544e+006, 1.34731e+006, 1.35924e+006, 1.37124e+006, 1.3833e+006, 1.39542e+006, 1.40761e+006, 1.41986e+006, 1.43217e+006, 1.44455e+006, 1.45699e+006, 1.4695e+006, 1.48207e+006, 1.4947e+006, 1.5074e+006, 1.52017e+006, 1.53299e+006, 1.54589e+006, 1.55885e+006, 1.57187e+006, 1.58496e+006, 1.59811e+006, 1.61133e+006, 1.62461e+006, 1.63796e+006, 1.65138e+006, 1.66486e+006, 1.6784e+006, 1.69202e+006, 1.70569e+006, 1.71944e+006, 1.73325e+006, 1.74713e+006, 1.76107e+006, 1.77508e+006, 1.78916e+006, 1.8033e+006, 1.81751e+006, 1.83179e+006, 1.84613e+006, 1.86054e+006, 1.87502e+006, 1.88957e+006, 1.90418e+006, 1.91886e+006, 1.93361e+006, 1.94843e+006, 1.96331e+006, 1.97827e+006, 1.99329e+006, 2.00838e+006, 2.02353e+006, 2.03876e+006, 2.05405e+006, 2.06942e+006, 2.08485e+006, 2.10035e+006, 2.11592e+006, 2.13156e+006, 2.14727e+006, 2.16304e+006, 2.17889e+006, 2.19481e+006, 2.21079e+006, 2.22685e+006, 2.24297e+006, 2.25917e+006, 2.27543e+006, 2.29177e+006, 2.30817e+006, 2.32465e+006, 2.34119e+006, 2.35781e+006, 2.37449e+006, 2.39125e+006, 2.40808e+006, 2.42498e+006, 2.44195e+006, 2.45899e+006, 2.4761e+006, 2.49328e+006, 2.51054e+006, 2.52786e+006, 2.54526e+006, 2.56273e+006, 2.58027e+006, 2.59788e+006, 2.61557e+006, 2.63332e+006, 2.65115e+006, 2.66905e+006, 2.68703e+006, 2.70507e+006, 2.72319e+006, 2.74138e+006, 2.75965e+006, 2.77798e+006, 2.79639e+006, 2.81487e+006, 2.83343e+006, 2.85206e+006, 2.87076e+006, 2.88954e+006, 2.90838e+006, 2.92731e+006, 2.9463e+006, 2.96537e+006, 2.98452e+006, 3.00373e+006, 3.02303e+006, 3.04239e+006, 3.06183e+006, 3.08135e+006, 3.10094e+006, 3.1206e+006, 3.14034e+006, 3.16015e+006, 3.18004e+006, 3.2e+006, 3.22004e+006, 3.24015e+006, 3.26034e+006, 3.2806e+006, 3.30094e+006, 3.32135e+006, 3.34184e+006, 3.36241e+006, 3.38305e+006, 3.40377e+006, 3.42456e+006, 3.44543e+006, 3.46637e+006, 3.48739e+006, 3.50849e+006, 3.52966e+006, 3.55091e+006, 3.57224e+006, 3.59364e+006, 3.61512e+006, 3.63668e+006, 3.65832e+006, 3.68003e+006, 3.70181e+006, 3.72368e+006, 3.74562e+006, 3.76764e+006, 3.78974e+006, 3.81192e+006, 3.83417e+006, 3.8565e+006, 3.87891e+006, 3.90139e+006, 3.92396e+006, 3.9466e+006, 3.96932e+006, 3.99212e+006, 4.015e+006, 4.03795e+006, 4.06099e+006, 4.0841e+006, 4.10729e+006, 4.13056e+006, 4.15391e+006, 4.17734e+006, 4.20085e+006, 4.22444e+006, 4.2481e+006, 4.27185e+006, 4.29567e+006, 4.31958e+006, 4.34356e+006, 4.36763e+006, 4.39177e+006, 4.41599e+006, 4.4403e+006, 4.46468e+006, 4.48915e+006, 4.51369e+006, 4.53831e+006, 4.56302e+006, 4.5878e+006, 4.61267e+006, 4.63762e+006, 4.66265e+006, 4.68775e+006, 4.71294e+006, 4.73821e+006, 4.76357e+006, 4.789e+006, 4.81451e+006, 4.84011e+006, 4.86578e+006, 4.89154e+006, 4.91738e+006, 4.9433e+006, 4.96931e+006, 4.99539e+006, 5.02156e+006, 5.04781e+006, 5.07414e+006, 5.10056e+006, 5.12705e+006, 5.15363e+006, 5.18029e+006, 5.20704e+006, 5.23386e+006, 5.26077e+006, 5.28777e+006, 5.31484e+006, 5.342e+006, 5.36924e+006, 5.39656e+006, 5.42397e+006, 5.45146e+006, 5.47904e+006, 5.50669e+006, 5.53444e+006, 5.56226e+006, 5.59017e+006, 5.61816e+006, 5.64624e+006, 5.6744e+006, 5.70265e+006, 5.73097e+006, 5.75939e+006, 5.78789e+006, 5.81647e+006, 5.84513e+006, 5.87389e+006, 5.90272e+006, 5.93164e+006, 5.96065e+006, 5.98974e+006, 6.01891e+006, 6.04817e+006, 6.07752e+006, 6.10695e+006, 6.13647e+006, 6.16607e+006, 6.19576e+006, 6.22553e+006, 6.25539e+006, 6.28533e+006, 6.31536e+006, 6.34548e+006, 6.37568e+006, 6.40597e+006, 6.43634e+006, 6.4668e+006, 6.49735e+006, 6.52798e+006, 6.5587e+006, 6.58951e+006, 6.6204e+006, 6.65138e+006, 6.68245e+006, 6.7136e+006, 6.74484e+006, 6.77617e+006, 6.80759e+006, 6.83909e+006, 6.87068e+006, 6.90235e+006, 6.93412e+006, 6.96597e+006, 6.99791e+006, 7.02994e+006, 7.06205e+006, 7.09425e+006, 7.12654e+006, 7.15892e+006, 7.19139e+006, 7.22394e+006, 7.25659e+006, 7.28932e+006, 7.32214e+006, 7.35505e+006, 7.38804e+006, 7.42113e+006, 7.4543e+006, 7.48757e+006, 7.52092e+006, 7.55436e+006, 7.58789e+006, 7.62151e+006, 7.65522e+006, 7.68902e+006, 7.7229e+006, 7.75688e+006, 7.79095e+006, 7.8251e+006, 7.85935e+006, 7.89368e+006, 7.92811e+006, 7.96262e+006, 7.99723e+006, 8.03192e+006, 8.06671e+006, 8.10158e+006, 8.13655e+006, 8.17161e+006, 8.20675e+006, 8.24199e+006, 8.27732e+006, 8.31274e+006, 8.34825e+006, 8.38385e+006, 8.41954e+006, 8.45532e+006, 8.49119e+006, 8.52716e+006, 8.56321e+006, 8.59936e+006, 8.6356e+006, 8.67193e+006, 8.70835e+006, 8.74486e+006, 8.78147e+006, 8.81816e+006, 8.85495e+006, 8.89183e+006, 8.9288e+006, 8.96587e+006, 9.00302e+006, 9.04027e+006, 9.07761e+006, 9.11505e+006, 9.15257e+006, 9.19019e+006, 9.2279e+006, 9.26571e+006, 9.3036e+006, 9.34159e+006, 9.37967e+006, 9.41785e+006, 9.45612e+006, 9.49448e+006, 9.53293e+006, 9.57148e+006, 9.61012e+006, 9.64886e+006, 9.68769e+006, 9.72661e+006, 9.76563e+006, 9.80473e+006, 9.84394e+006, 9.88323e+006, 9.92263e+006, 9.96211e+006, 1.00017e+007, 1.00414e+007, 1.00811e+007, 1.0121e+007, 1.01609e+007, 1.0201e+007, 1.02411e+007, 1.02814e+007, 1.03217e+007, 1.03622e+007, 1.04027e+007, 1.04433e+007, 1.0484e+007, 1.05248e+007, 1.05657e+007, 1.06067e+007, 1.06478e+007, 1.0689e+007, 1.07303e+007, 1.07717e+007, 1.08132e+007, 1.08547e+007, 1.08964e+007, 1.09382e+007, 1.098e+007, 1.1022e+007, 1.1064e+007, 1.11062e+007, 1.11484e+007, 1.11908e+007, 1.12332e+007, 1.12757e+007, 1.13184e+007, 1.13611e+007, 1.14039e+007, 1.14468e+007, 1.14899e+007, 1.1533e+007, 1.15762e+007, 1.16195e+007, 1.16629e+007, 1.17064e+007, 1.175e+007, 1.17937e+007, 1.18375e+007, 1.18814e+007, 1.19254e+007, 1.19695e+007, 1.20136e+007, 1.20579e+007, 1.21023e+007, 1.21468e+007, 1.21913e+007, 1.2236e+007, 1.22808e+007, 1.23257e+007, 1.23706e+007, 1.24157e+007, 1.24609e+007, 1.25061e+007, 1.25515e+007, 1.25969e+007, 1.26425e+007, 1.26882e+007, 1.27339e+007, 1.27798e+007, 1.28257e+007, 1.28718e+007, 1.29179e+007, 1.29642e+007, 1.30105e+007, 1.3057e+007, 1.31035e+007, 1.31502e+007, 1.31969e+007, 1.32438e+007, 1.32907e+007, 1.33378e+007, 1.33849e+007, 1.34322e+007, 1.34795e+007, 1.3527e+007, 1.35745e+007, 1.36221e+007, 1.36699e+007, 1.37177e+007, 1.37657e+007, 1.38137e+007, 1.38619e+007, 1.39101e+007, 1.39585e+007, 1.40069e+007, 1.40555e+007, 1.41041e+007, 1.41529e+007, 1.42017e+007, 1.42507e+007, 1.42998e+007, 1.43489e+007, 1.43982e+007, 1.44475e+007, 1.4497e+007, 1.45465e+007, 1.45962e+007, 1.4646e+007, 1.46958e+007, 1.47458e+007, 1.47959e+007, 1.48461e+007, 1.48963e+007, 1.49467e+007, 1.49972e+007, 1.50478e+007, 1.50984e+007, 1.51492e+007, 1.52001e+007, 1.52511e+007, 1.53022e+007, 1.53534e+007, 1.54047e+007, 1.54561e+007, 1.55076e+007, 1.55592e+007, 1.56109e+007, 1.56627e+007, 1.57146e+007, 1.57667e+007, 1.58188e+007, 1.5871e+007, 1.59233e+007, 1.59758e+007, 1.60283e+007, 1.60809e+007, 1.61337e+007, 1.61865e+007, 1.62395e+007, 1.62925e+007, 1.63457e+007, 1.63989e+007, 1.64523e+007, 1.65058e+007, 1.65593e+007, 1.6613e+007, 1.66668e+007, 1.67207e+007, 1.67747e+007, 1.68288e+007, 1.6883e+007, 1.69373e+007, 1.69917e+007, 1.70462e+007, 1.71008e+007, 1.71555e+007, 1.72104e+007, 1.72653e+007, 1.73203e+007, 1.73755e+007, 1.74307e+007, 1.74861e+007, 1.75415e+007, 1.75971e+007, 1.76528e+007, 1.77085e+007, 1.77644e+007, 1.78204e+007, 1.78765e+007, 1.79327e+007, 1.7989e+007, 1.80454e+007, 1.81019e+007, 1.81586e+007, 1.82153e+007, 1.82721e+007, 1.83291e+007, 1.83861e+007, 1.84433e+007, 1.85005e+007, 1.85579e+007, 1.86154e+007, 1.86729e+007, 1.87306e+007, 1.87884e+007, 1.88463e+007, 1.89043e+007, 1.89624e+007, 1.90207e+007, 1.9079e+007, 1.91374e+007, 1.9196e+007, 1.92546e+007, 1.93134e+007, 1.93722e+007, 1.94312e+007, 1.94903e+007, 1.95495e+007, 1.96088e+007, 1.96682e+007, 1.97277e+007, 1.97873e+007, 1.9847e+007, 1.99069e+007, 1.99668e+007, 2.00268e+007, 2.0087e+007, 2.01473e+007, 2.02076e+007, 2.02681e+007, 2.03287e+007, 2.03894e+007, 2.04502e+007, 2.05111e+007, 2.05722e+007, 2.06333e+007, 2.06946e+007, 2.07559e+007, 2.08174e+007, 2.08789e+007, 2.09406e+007, 2.10024e+007, 2.10643e+007, 2.11263e+007, 2.11884e+007, 2.12507e+007, 2.1313e+007, 2.13755e+007, 2.1438e+007, 2.15007e+007, 2.15634e+007, 2.16263e+007, 2.16893e+007, 2.17524e+007, 2.18157e+007, 2.1879e+007, 2.19424e+007, 2.2006e+007, 2.20696e+007, 2.21334e+007, 2.21973e+007, 2.22612e+007, 2.23253e+007, 2.23896e+007, 2.24539e+007, 2.25183e+007, 2.25828e+007, 2.26475e+007, 2.27123e+007, 2.27771e+007, 2.28421e+007, 2.29072e+007, 2.29724e+007, 2.30377e+007, 2.31032e+007, 2.31687e+007, 2.32344e+007, 2.33001e+007, 2.3366e+007, 2.3432e+007, 2.34981e+007, 2.35643e+007, 2.36306e+007, 2.3697e+007, 2.37636e+007, 2.38303e+007, 2.3897e+007, 2.39639e+007, 2.40309e+007, 2.4098e+007, 2.41652e+007, 2.42326e+007, 2.43e+007, 2.43676e+007, 2.44352e+007, 2.4503e+007, 2.45709e+007, 2.46389e+007, 2.4707e+007, 2.47753e+007, 2.48436e+007, 2.49121e+007, 2.49806e+007, 2.50493e+007, 2.51181e+007, 2.5187e+007, 2.52561e+007, 2.53252e+007, 2.53944e+007, 2.54638e+007, 2.55333e+007, 2.56029e+007, 2.56726e+007, 2.57424e+007, 2.58123e+007, 2.58824e+007, 2.59525e+007, 2.60228e+007, 2.60932e+007, 2.61637e+007, 2.62343e+007, 2.63051e+007, 2.63759e+007, 2.64469e+007, 2.65179e+007, 2.65891e+007, 2.66604e+007, 2.67319e+007, 2.68034e+007, 2.6875e+007, 2.69468e+007, 2.70187e+007, 2.70907e+007, 2.71628e+007, 2.7235e+007, 2.73073e+007, 2.73798e+007, 2.74523e+007, 2.7525e+007, 2.75978e+007, 2.76707e+007, 2.77438e+007, 2.78169e+007, 2.78902e+007, 2.79636e+007, 2.8037e+007, 2.81107e+007, 2.81844e+007, 2.82582e+007, 2.83322e+007, 2.84062e+007, 2.84804e+007, 2.85547e+007, 2.86292e+007, 2.87037e+007, 2.87783e+007, 2.88531e+007, 2.8928e+007, 2.9003e+007, 2.90781e+007, 2.91533e+007, 2.92287e+007, 2.93042e+007, 2.93797e+007, 2.94555e+007, 2.95313e+007, 2.96072e+007, 2.96833e+007, 2.97594e+007, 2.98357e+007, 2.99121e+007, 2.99886e+007, 3.00653e+007, 3.0142e+007, 3.02189e+007, 3.02959e+007, 3.0373e+007, 3.04502e+007, 3.05276e+007, 3.0605e+007, 3.06826e+007, 3.07603e+007, 3.08381e+007, 3.09161e+007, 3.09941e+007, 3.10723e+007, 3.11506e+007, 3.1229e+007, 3.13075e+007, 3.13861e+007, 3.14649e+007, 3.15438e+007, 3.16228e+007, 3.17019e+007, 3.17811e+007, 3.18605e+007, 3.194e+007, 3.20195e+007, 3.20993e+007, 3.21791e+007, 3.2259e+007, 3.23391e+007, 3.24193e+007, 3.24996e+007, 3.258e+007, 3.26606e+007, 3.27412e+007, 3.2822e+007, 3.29029e+007, 3.29839e+007, 3.30651e+007, 3.31463e+007, 3.32277e+007, 3.33092e+007, 3.33908e+007, 3.34726e+007, 3.35544e+007, 3.36364e+007, 3.37185e+007, 3.38007e+007, 3.38831e+007, 3.39655e+007, 3.40481e+007, 3.41308e+007, 3.42136e+007, 3.42966e+007, 3.43796e+007, 3.44628e+007, 3.45461e+007, 3.46296e+007, 3.47131e+007, 3.47968e+007, 3.48806e+007, 3.49645e+007, 3.50485e+007, 3.51326e+007, 3.52169e+007, 3.53013e+007, 3.53858e+007, 3.54705e+007, 3.55552e+007, 3.56401e+007, 3.57251e+007, 3.58102e+007, 3.58954e+007, 3.59808e+007, 3.60663e+007, 3.61519e+007, 3.62376e+007, 3.63235e+007, 3.64095e+007, 3.64955e+007, 3.65818e+007, 3.66681e+007, 3.67546e+007, 3.68411e+007, 3.69279e+007, 3.70147e+007, 3.71016e+007, 3.71887e+007, 3.72759e+007, 3.73632e+007, 3.74507e+007, 3.75382e+007, 3.76259e+007, 3.77137e+007, 3.78016e+007, 3.78897e+007, 3.79779e+007, 3.80662e+007, 3.81546e+007, 3.82431e+007, 3.83318e+007, 3.84206e+007, 3.85095e+007, 3.85986e+007, 3.86877e+007, 3.8777e+007, 3.88664e+007, 3.8956e+007, 3.90456e+007, 3.91354e+007, 3.92253e+007, 3.93153e+007, 3.94055e+007, 3.94958e+007, 3.95862e+007, 3.96767e+007, 3.97673e+007, 3.98581e+007, 3.9949e+007, 4.004e+007, 4.01312e+007, 4.02224e+007, 4.03138e+007, 4.04053e+007, 4.0497e+007, 4.05888e+007, 4.06806e+007, 4.07727e+007, 4.08648e+007, 4.09571e+007, 4.10495e+007, 4.1142e+007, 4.12346e+007, 4.13274e+007, 4.14203e+007, 4.15133e+007, 4.16064e+007, 4.16997e+007, 4.17931e+007, 4.18866e+007, 4.19803e+007, 4.2074e+007, 4.21679e+007, 4.22619e+007, 4.23561e+007, 4.24504e+007, 4.25448e+007, 4.26393e+007, 4.27339e+007, 4.28287e+007, 4.29236e+007, 4.30186e+007, 4.31138e+007, 4.32091e+007, 4.33045e+007, 4.34e+007, 4.34957e+007, 4.35914e+007, 4.36873e+007, 4.37834e+007, 4.38795e+007, 4.39758e+007, 4.40723e+007, 4.41688e+007, 4.42655e+007, 4.43623e+007, 4.44592e+007, 4.45562e+007, 4.46534e+007, 4.47507e+007, 4.48482e+007, 4.49457e+007, 4.50434e+007, 4.51412e+007, 4.52392e+007, 4.53372e+007, 4.54354e+007, 4.55337e+007, 4.56322e+007, 4.57308e+007, 4.58295e+007, 4.59283e+007, 4.60273e+007, 4.61264e+007, 4.62256e+007, 4.63249e+007, 4.64244e+007, 4.6524e+007, 4.66237e+007, 4.67236e+007, 4.68236e+007, 4.69237e+007, 4.70239e+007, 4.71243e+007, 4.72248e+007, 4.73254e+007, 4.74262e+007, 4.75271e+007, 4.76281e+007, 4.77292e+007, 4.78305e+007, 4.79319e+007, 4.80334e+007, 4.81351e+007, 4.82369e+007, 4.83388e+007, 4.84408e+007, 4.8543e+007, 4.86453e+007, 4.87478e+007, 4.88503e+007, 4.8953e+007, 4.90558e+007, 4.91588e+007, 4.92619e+007, 4.93651e+007, 4.94684e+007, 4.95719e+007, 4.96755e+007, 4.97792e+007, 4.98831e+007, 4.99871e+007, 5.00912e+007, 5.01954e+007, 5.02998e+007, 5.04043e+007, 5.05089e+007, 5.06137e+007, 5.07186e+007, 5.08236e+007, 5.09288e+007, 5.10341e+007, 5.11395e+007, 5.12451e+007, 5.13507e+007, 5.14566e+007, 5.15625e+007, 5.16686e+007, 5.17748e+007, 5.18811e+007, 5.19876e+007, 5.20942e+007, 5.22009e+007, 5.23078e+007, 5.24148e+007, 5.25219e+007, 5.26291e+007, 5.27365e+007, 5.2844e+007, 5.29517e+007, 5.30595e+007, 5.31674e+007, 5.32754e+007, 5.33836e+007, 5.34919e+007, 5.36003e+007, 5.37089e+007, 5.38176e+007, 5.39264e+007, 5.40354e+007, 5.41445e+007, 5.42537e+007, 5.43631e+007, 5.44726e+007, 5.45822e+007, 5.46919e+007, 5.48018e+007, 5.49119e+007, 5.5022e+007, 5.51323e+007, 5.52427e+007, 5.53533e+007, 5.5464e+007, 5.55748e+007, 5.56857e+007, 5.57968e+007, 5.5908e+007, 5.60194e+007, 5.61308e+007, 5.62425e+007, 5.63542e+007, 5.64661e+007, 5.65781e+007, 5.66903e+007, 5.68025e+007, 5.69149e+007, 5.70275e+007, 5.71402e+007, 5.7253e+007, 5.73659e+007, 5.7479e+007, 5.75922e+007, 5.77056e+007, 5.78191e+007, 5.79327e+007, 5.80464e+007, 5.81603e+007, 5.82743e+007, 5.83885e+007, 5.85028e+007, 5.86172e+007, 5.87317e+007, 5.88464e+007, 5.89612e+007, 5.90762e+007, 5.91913e+007, 5.93065e+007, 5.94219e+007, 5.95374e+007, 5.9653e+007, 5.97688e+007, 5.98847e+007, 6.00007e+007, 6.01169e+007, 6.02332e+007, 6.03496e+007, 6.04662e+007, 6.05829e+007, 6.06997e+007, 6.08167e+007, 6.09338e+007, 6.10511e+007, 6.11684e+007, 6.1286e+007, 6.14036e+007, 6.15214e+007, 6.16393e+007, 6.17574e+007, 6.18756e+007, 6.19939e+007, 6.21124e+007, 6.2231e+007, 6.23497e+007, 6.24686e+007, 6.25876e+007, 6.27068e+007, 6.2826e+007, 6.29455e+007, 6.3065e+007, 6.31847e+007, 6.33045e+007, 6.34245e+007, 6.35446e+007, 6.36648e+007, 6.37852e+007, 6.39057e+007, 6.40264e+007, 6.41471e+007, 6.42681e+007, 6.43891e+007, 6.45103e+007, 6.46316e+007, 6.47531e+007, 6.48747e+007, 6.49964e+007, 6.51183e+007, 6.52403e+007, 6.53625e+007, 6.54848e+007, 6.56072e+007, 6.57298e+007, 6.58525e+007, 6.59753e+007, 6.60983e+007, 6.62214e+007, 6.63446e+007, 6.6468e+007, 6.65915e+007, 6.67152e+007, 6.6839e+007, 6.69629e+007, 6.7087e+007, 6.72112e+007, 6.73356e+007, 6.74601e+007, 6.75847e+007, 6.77094e+007, 6.78343e+007, 6.79594e+007, 6.80846e+007, 6.82099e+007, 6.83353e+007, 6.84609e+007, 6.85867e+007, 6.87125e+007, 6.88385e+007, 6.89647e+007, 6.9091e+007, 6.92174e+007, 6.9344e+007, 6.94707e+007, 6.95975e+007, 6.97245e+007, 6.98516e+007, 6.99789e+007, 7.01063e+007, 7.02338e+007, 7.03615e+007, 7.04893e+007, 7.06172e+007, 7.07453e+007, 7.08736e+007, 7.10019e+007, 7.11304e+007, 7.12591e+007, 7.13879e+007, 7.15168e+007, 7.16459e+007, 7.17751e+007, 7.19044e+007, 7.20339e+007, 7.21635e+007, 7.22933e+007, 7.24232e+007, 7.25533e+007, 7.26834e+007, 7.28138e+007, 7.29442e+007, 7.30748e+007, 7.32056e+007, 7.33365e+007, 7.34675e+007, 7.35987e+007, 7.373e+007, 7.38614e+007, 7.3993e+007, 7.41248e+007, 7.42566e+007, 7.43886e+007, 7.45208e+007, 7.46531e+007, 7.47855e+007, 7.49181e+007, 7.50508e+007, 7.51837e+007, 7.53167e+007, 7.54498e+007, 7.55831e+007, 7.57165e+007, 7.58501e+007, 7.59838e+007, 7.61176e+007, 7.62516e+007, 7.63857e+007, 7.652e+007, 7.66544e+007, 7.6789e+007, 7.69237e+007, 7.70585e+007, 7.71935e+007, 7.73286e+007, 7.74639e+007, 7.75993e+007, 7.77348e+007, 7.78705e+007, 7.80063e+007, 7.81423e+007, 7.82784e+007, 7.84147e+007, 7.8551e+007, 7.86876e+007, 7.88243e+007, 7.89611e+007, 7.90981e+007, 7.92352e+007, 7.93724e+007, 7.95098e+007, 7.96473e+007, 7.9785e+007, 7.99229e+007, 8.00608e+007, 8.01989e+007, 8.03372e+007, 8.04756e+007, 8.06141e+007, 8.07528e+007, 8.08916e+007, 8.10306e+007, 8.11697e+007, 8.13089e+007, 8.14483e+007, 8.15879e+007, 8.17275e+007, 8.18674e+007, 8.20073e+007, 8.21474e+007, 8.22877e+007, 8.24281e+007, 8.25686e+007, 8.27093e+007, 8.28502e+007, 8.29911e+007, 8.31322e+007, 8.32735e+007, 8.34149e+007, 8.35565e+007, 8.36982e+007, 8.384e+007, 8.3982e+007, 8.41241e+007, 8.42664e+007, 8.44088e+007, 8.45513e+007, 8.4694e+007, 8.48369e+007, 8.49799e+007, 8.5123e+007, 8.52663e+007, 8.54097e+007, 8.55533e+007, 8.5697e+007, 8.58409e+007, 8.59849e+007, 8.6129e+007, 8.62733e+007, 8.64178e+007, 8.65623e+007, 8.67071e+007, 8.68519e+007, 8.6997e+007, 8.71421e+007, 8.72874e+007, 8.74329e+007, 8.75785e+007, 8.77242e+007, 8.78701e+007, 8.80162e+007, 8.81623e+007, 8.83087e+007, 8.84551e+007, 8.86018e+007, 8.87485e+007, 8.88954e+007, 8.90425e+007, 8.91897e+007, 8.9337e+007, 8.94845e+007, 8.96322e+007, 8.978e+007, 8.99279e+007, 9.0076e+007, 9.02242e+007, 9.03726e+007, 9.05211e+007, 9.06697e+007, 9.08186e+007, 9.09675e+007, 9.11166e+007, 9.12659e+007, 9.14153e+007, 9.15648e+007, 9.17145e+007, 9.18643e+007, 9.20143e+007, 9.21644e+007, 9.23147e+007, 9.24651e+007, 9.26157e+007, 9.27664e+007, 9.29173e+007, 9.30683e+007, 9.32195e+007, 9.33708e+007, 9.35222e+007, 9.36738e+007, 9.38256e+007, 9.39775e+007, 9.41295e+007, 9.42817e+007, 9.4434e+007, 9.45865e+007, 9.47392e+007, 9.48919e+007, 9.50449e+007, 9.51979e+007, 9.53512e+007, 9.55045e+007, 9.56581e+007, 9.58117e+007, 9.59655e+007, 9.61195e+007, 9.62736e+007, 9.64279e+007, 9.65823e+007, 9.67368e+007, 9.68915e+007, 9.70464e+007, 9.72014e+007, 9.73565e+007, 9.75118e+007, 9.76673e+007, 9.78229e+007, 9.79786e+007, 9.81345e+007, 9.82906e+007, 9.84468e+007, 9.86031e+007, 9.87596e+007, 9.89162e+007, 9.9073e+007, 9.92299e+007, 9.9387e+007, 9.95443e+007, 9.97016e+007, 9.98592e+007, 1.00017e+008, 1.00175e+008, 1.00333e+008, 1.00491e+008, 1.00649e+008, 1.00807e+008, 1.00966e+008, 1.01125e+008, 1.01284e+008, 1.01443e+008, 1.01602e+008, 1.01761e+008, 1.01921e+008, 1.0208e+008, 1.0224e+008, 1.024e+008, 1.0256e+008, 1.0272e+008, 1.02881e+008, 1.03041e+008, 1.03202e+008, 1.03363e+008, 1.03524e+008, 1.03685e+008, 1.03846e+008, 1.04008e+008, 1.04169e+008, 1.04331e+008, 1.04493e+008, 1.04655e+008, 1.04817e+008, 1.04979e+008, 1.05142e+008, 1.05304e+008, 1.05467e+008, 1.0563e+008, 1.05793e+008, 1.05956e+008, 1.0612e+008, 1.06283e+008, 1.06447e+008, 1.06611e+008, 1.06775e+008, 1.06939e+008, 1.07103e+008, 1.07268e+008, 1.07432e+008, 1.07597e+008, 1.07762e+008, 1.07927e+008, 1.08092e+008, 1.08258e+008, 1.08423e+008, 1.08589e+008, 1.08755e+008, 1.0892e+008, 1.09087e+008, 1.09253e+008, 1.09419e+008, 1.09586e+008, 1.09753e+008, 1.09919e+008, 1.10086e+008, 1.10254e+008, 1.10421e+008, 1.10588e+008, 1.10756e+008, 1.10924e+008, 1.11092e+008, 1.1126e+008, 1.11428e+008, 1.11597e+008, 1.11765e+008, 1.11934e+008, 1.12103e+008, 1.12272e+008, 1.12441e+008, 1.1261e+008, 1.1278e+008, 1.12949e+008, 1.13119e+008, 1.13289e+008, 1.13459e+008, 1.13629e+008, 1.138e+008, 1.1397e+008, 1.14141e+008, 1.14312e+008, 1.14483e+008, 1.14654e+008, 1.14825e+008, 1.14997e+008, 1.15168e+008, 1.1534e+008, 1.15512e+008, 1.15684e+008, 1.15856e+008, 1.16029e+008, 1.16201e+008, 1.16374e+008, 1.16547e+008, 1.1672e+008, 1.16893e+008, 1.17066e+008, 1.1724e+008, 1.17413e+008, 1.17587e+008, 1.17761e+008, 1.17935e+008, 1.18109e+008, 1.18284e+008, 1.18458e+008, 1.18633e+008, 1.18808e+008, 1.18983e+008, 1.19158e+008, 1.19333e+008, 1.19509e+008, 1.19684e+008, 1.1986e+008, 1.20036e+008, 1.20212e+008, 1.20388e+008, 1.20565e+008, 1.20741e+008, 1.20918e+008, 1.21095e+008, 1.21272e+008, 1.21449e+008, 1.21626e+008, 1.21804e+008, 1.21981e+008, 1.22159e+008, 1.22337e+008, 1.22515e+008, 1.22693e+008, 1.22872e+008, 1.2305e+008, 1.23229e+008, 1.23408e+008, 1.23587e+008, 1.23766e+008, 1.23946e+008, 1.24125e+008, 1.24305e+008, 1.24484e+008, 1.24664e+008, 1.24845e+008, 1.25025e+008, 1.25205e+008, 1.25386e+008, 1.25567e+008, 1.25748e+008, 1.25929e+008, 1.2611e+008, 1.26291e+008, 1.26473e+008, 1.26654e+008, 1.26836e+008, 1.27018e+008, 1.272e+008, 1.27383e+008, 1.27565e+008, 1.27748e+008, 1.27931e+008, 1.28114e+008, 1.28297e+008, 1.2848e+008, 1.28663e+008, 1.28847e+008, 1.29031e+008, 1.29215e+008, 1.29399e+008, 1.29583e+008, 1.29767e+008, 1.29952e+008, 1.30136e+008, 1.30321e+008, 1.30506e+008, 1.30691e+008, 1.30877e+008, 1.31062e+008, 1.31248e+008, 1.31433e+008, 1.31619e+008, 1.31805e+008, 1.31992e+008, 1.32178e+008, 1.32365e+008, 1.32551e+008, 1.32738e+008, 1.32925e+008, 1.33112e+008, 1.333e+008, 1.33487e+008, 1.33675e+008, 1.33863e+008, 1.34051e+008, 1.34239e+008, 1.34427e+008, 1.34616e+008, 1.34804e+008, 1.34993e+008, 1.35182e+008, 1.35371e+008, 1.3556e+008, 1.3575e+008, 1.35939e+008, 1.36129e+008, 1.36319e+008, 1.36509e+008, 1.36699e+008, 1.3689e+008, 1.3708e+008, 1.37271e+008, 1.37462e+008, 1.37653e+008, 1.37844e+008, 1.38035e+008, 1.38227e+008, 1.38418e+008, 1.3861e+008, 1.38802e+008, 1.38994e+008, 1.39186e+008, 1.39379e+008, 1.39571e+008, 1.39764e+008, 1.39957e+008, 1.4015e+008, 1.40343e+008, 1.40537e+008, 1.4073e+008, 1.40924e+008, 1.41118e+008, 1.41312e+008, 1.41506e+008, 1.417e+008, 1.41895e+008, 1.4209e+008, 1.42284e+008, 1.42479e+008, 1.42675e+008, 1.4287e+008, 1.43065e+008, 1.43261e+008, 1.43457e+008, 1.43653e+008, 1.43849e+008, 1.44045e+008, 1.44241e+008, 1.44438e+008, 1.44635e+008, 1.44832e+008, 1.45029e+008, 1.45226e+008, 1.45423e+008, 1.45621e+008, 1.45819e+008, 1.46017e+008, 1.46215e+008, 1.46413e+008, 1.46611e+008, 1.4681e+008, 1.47008e+008, 1.47207e+008, 1.47406e+008, 1.47605e+008, 1.47805e+008, 1.48004e+008, 1.48204e+008, 1.48404e+008, 1.48604e+008, 1.48804e+008, 1.49004e+008, 1.49205e+008, 1.49405e+008, 1.49606e+008, 1.49807e+008, 1.50008e+008, 1.50209e+008, 1.50411e+008, 1.50612e+008, 1.50814e+008, 1.51016e+008, 1.51218e+008, 1.5142e+008, 1.51623e+008, 1.51825e+008, 1.52028e+008, 1.52231e+008, 1.52434e+008, 1.52637e+008, 1.52841e+008, 1.53044e+008, 1.53248e+008, 1.53452e+008, 1.53656e+008, 1.5386e+008, 1.54064e+008, 1.54269e+008, 1.54474e+008, 1.54678e+008, 1.54883e+008, 1.55089e+008, 1.55294e+008, 1.55499e+008, 1.55705e+008, 1.55911e+008, 1.56117e+008, 1.56323e+008, 1.56529e+008, 1.56736e+008, 1.56942e+008, 1.57149e+008, 1.57356e+008, 1.57563e+008, 1.57771e+008, 1.57978e+008, 1.58186e+008, 1.58394e+008, 1.58601e+008, 1.5881e+008, 1.59018e+008, 1.59226e+008, 1.59435e+008, 1.59644e+008, 1.59853e+008, 1.60062e+008, 1.60271e+008, 1.6048e+008, 1.6069e+008, 1.609e+008, 1.6111e+008, 1.6132e+008, 1.6153e+008, 1.6174e+008, 1.61951e+008, 1.62162e+008, 1.62373e+008, 1.62584e+008, 1.62795e+008, 1.63006e+008, 1.63218e+008, 1.6343e+008, 1.63641e+008, 1.63853e+008, 1.64066e+008, 1.64278e+008, 1.64491e+008, 1.64703e+008, 1.64916e+008, 1.65129e+008, 1.65342e+008, 1.65556e+008, 1.65769e+008, 1.65983e+008, 1.66197e+008, 1.66411e+008, 1.66625e+008, 1.6684e+008, 1.67054e+008, 1.67269e+008, 1.67484e+008, 1.67699e+008, 1.67914e+008, 1.68129e+008, 1.68345e+008, 1.6856e+008, 1.68776e+008, 1.68992e+008, 1.69208e+008, 1.69425e+008, 1.69641e+008, 1.69858e+008, 1.70075e+008, 1.70292e+008, 1.70509e+008, 1.70726e+008, 1.70944e+008, 1.71162e+008, 1.71379e+008, 1.71597e+008, 1.71816e+008, 1.72034e+008, 1.72253e+008, 1.72471e+008, 1.7269e+008, 1.72909e+008, 1.73128e+008, 1.73348e+008, 1.73567e+008, 1.73787e+008, 1.74007e+008, 1.74227e+008, 1.74447e+008, 1.74667e+008, 1.74888e+008, 1.75108e+008, 1.75329e+008, 1.7555e+008, 1.75771e+008, 1.75993e+008, 1.76214e+008, 1.76436e+008, 1.76658e+008, 1.7688e+008, 1.77102e+008, 1.77324e+008, 1.77547e+008, 1.77769e+008, 1.77992e+008, 1.78215e+008, 1.78439e+008, 1.78662e+008, 1.78885e+008, 1.79109e+008, 1.79333e+008, 1.79557e+008, 1.79781e+008, 1.80006e+008, 1.8023e+008, 1.80455e+008, 1.8068e+008, 1.80905e+008, 1.8113e+008, 1.81355e+008, 1.81581e+008, 1.81807e+008, 1.82032e+008, 1.82258e+008, 1.82485e+008, 1.82711e+008, 1.82938e+008, 1.83164e+008, 1.83391e+008, 1.83618e+008, 1.83845e+008, 1.84073e+008, 1.843e+008, 1.84528e+008, 1.84756e+008, 1.84984e+008, 1.85212e+008, 1.85441e+008, 1.85669e+008, 1.85898e+008, 1.86127e+008, 1.86356e+008, 1.86585e+008, 1.86815e+008, 1.87044e+008, 1.87274e+008, 1.87504e+008, 1.87734e+008, 1.87964e+008, 1.88195e+008, 1.88425e+008, 1.88656e+008, 1.88887e+008, 1.89118e+008, 1.89349e+008, 1.89581e+008, 1.89813e+008, 1.90044e+008, 1.90276e+008, 1.90508e+008, 1.90741e+008, 1.90973e+008, 1.91206e+008, 1.91439e+008, 1.91672e+008, 1.91905e+008, 1.92138e+008, 1.92372e+008, 1.92605e+008, 1.92839e+008, 1.93073e+008, 1.93307e+008, 1.93542e+008, 1.93776e+008, 1.94011e+008, 1.94246e+008, 1.94481e+008, 1.94716e+008, 1.94951e+008, 1.95187e+008, 1.95422e+008, 1.95658e+008, 1.95894e+008, 1.96131e+008, 1.96367e+008, 1.96603e+008, 1.9684e+008, 1.97077e+008, 1.97314e+008, 1.97551e+008, 1.97789e+008, 1.98026e+008, 1.98264e+008, 1.98502e+008, 1.9874e+008, 1.98978e+008, 1.99217e+008, 1.99456e+008, 1.99694e+008, 1.99933e+008, 2.00172e+008, 2.00412e+008, 2.00651e+008, 2.00891e+008, 2.01131e+008, 2.01371e+008, 2.01611e+008, 2.01851e+008, 2.02092e+008, 2.02332e+008, 2.02573e+008, 2.02814e+008, 2.03055e+008, 2.03297e+008, 2.03538e+008, 2.0378e+008, 2.04022e+008, 2.04264e+008, 2.04506e+008, 2.04748e+008, 2.04991e+008, 2.05234e+008, 2.05477e+008, 2.0572e+008, 2.05963e+008, 2.06206e+008, 2.0645e+008, 2.06694e+008, 2.06938e+008, 2.07182e+008, 2.07426e+008, 2.07671e+008, 2.07915e+008, 2.0816e+008, 2.08405e+008, 2.0865e+008, 2.08895e+008, 2.09141e+008, 2.09387e+008, 2.09633e+008, 2.09879e+008, 2.10125e+008, 2.10371e+008, 2.10618e+008, 2.10864e+008, 2.11111e+008, 2.11358e+008, 2.11606e+008, 2.11853e+008, 2.121e+008, 2.12348e+008, 2.12596e+008, 2.12844e+008, 2.13093e+008, 2.13341e+008, 2.1359e+008, 2.13838e+008, 2.14087e+008, 2.14337e+008, 2.14586e+008, 2.14835e+008, 2.15085e+008, 2.15335e+008, 2.15585e+008, 2.15835e+008, 2.16085e+008, 2.16336e+008, 2.16587e+008, 2.16837e+008, 2.17089e+008, 2.1734e+008, 2.17591e+008, 2.17843e+008, 2.18095e+008, 2.18346e+008, 2.18599e+008, 2.18851e+008, 2.19103e+008, 2.19356e+008, 2.19609e+008, 2.19862e+008, 2.20115e+008, 2.20368e+008, 2.20622e+008, 2.20875e+008, 2.21129e+008, 2.21383e+008, 2.21637e+008, 2.21892e+008, 2.22146e+008, 2.22401e+008, 2.22656e+008, 2.22911e+008, 2.23166e+008, 2.23422e+008, 2.23677e+008, 2.23933e+008, 2.24189e+008, 2.24445e+008, 2.24701e+008, 2.24958e+008, 2.25215e+008, 2.25471e+008, 2.25728e+008, 2.25986e+008, 2.26243e+008, 2.26501e+008, 2.26758e+008, 2.27016e+008, 2.27274e+008, 2.27532e+008, 2.27791e+008, 2.28049e+008, 2.28308e+008, 2.28567e+008, 2.28826e+008, 2.29086e+008, 2.29345e+008, 2.29605e+008, 2.29864e+008, 2.30124e+008, 2.30385e+008, 2.30645e+008, 2.30906e+008, 2.31166e+008, 2.31427e+008, 2.31688e+008, 2.31949e+008, 2.32211e+008, 2.32472e+008, 2.32734e+008, 2.32996e+008, 2.33258e+008, 2.33521e+008, 2.33783e+008, 2.34046e+008, 2.34308e+008, 2.34571e+008, 2.34835e+008, 2.35098e+008, 2.35362e+008, 2.35625e+008, 2.35889e+008, 2.36153e+008, 2.36417e+008, 2.36682e+008, 2.36946e+008, 2.37211e+008, 2.37476e+008, 2.37741e+008, 2.38007e+008, 2.38272e+008, 2.38538e+008, 2.38804e+008, 2.3907e+008, 2.39336e+008, 2.39602e+008, 2.39869e+008, 2.40135e+008, 2.40402e+008, 2.40669e+008, 2.40937e+008, 2.41204e+008, 2.41472e+008, 2.4174e+008, 2.42008e+008, 2.42276e+008, 2.42544e+008, 2.42813e+008, 2.43081e+008, 2.4335e+008, 2.43619e+008, 2.43888e+008, 2.44158e+008, 2.44427e+008, 2.44697e+008, 2.44967e+008, 2.45237e+008, 2.45507e+008, 2.45778e+008, 2.46049e+008, 2.46319e+008, 2.4659e+008, 2.46862e+008, 2.47133e+008, 2.47404e+008, 2.47676e+008, 2.47948e+008, 2.4822e+008, 2.48492e+008, 2.48765e+008, 2.49038e+008, 2.4931e+008, 2.49583e+008, 2.49856e+008, 2.5013e+008, 2.50403e+008, 2.50677e+008, 2.50951e+008, 2.51225e+008, 2.51499e+008, 2.51774e+008, 2.52048e+008, 2.52323e+008, 2.52598e+008, 2.52873e+008, 2.53148e+008, 2.53424e+008, 2.53699e+008, 2.53975e+008, 2.54251e+008, 2.54528e+008, 2.54804e+008, 2.55081e+008, 2.55357e+008, 2.55634e+008, 2.55911e+008, 2.56189e+008, 2.56466e+008, 2.56744e+008, 2.57022e+008, 2.573e+008, 2.57578e+008, 2.57856e+008, 2.58135e+008, 2.58413e+008, 2.58692e+008, 2.58971e+008, 2.59251e+008, 2.5953e+008, 2.5981e+008, 2.6009e+008, 2.6037e+008, 2.6065e+008, 2.6093e+008, 2.61211e+008, 2.61491e+008, 2.61772e+008, 2.62053e+008, 2.62335e+008, 2.62616e+008, 2.62898e+008, 2.6318e+008, 2.63462e+008, 2.63744e+008, 2.64026e+008, 2.64309e+008, 2.64591e+008, 2.64874e+008, 2.65157e+008, 2.65441e+008, 2.65724e+008, 2.66008e+008, 2.66291e+008, 2.66575e+008, 2.6686e+008, 2.67144e+008, 2.67428e+008, 2.67713e+008, 2.67998e+008, 2.68283e+008, 2.68568e+008, 2.68854e+008, 2.69139e+008, 2.69425e+008, 2.69711e+008, 2.69997e+008, 2.70284e+008, 2.7057e+008, 2.70857e+008, 2.71144e+008, 2.71431e+008, 2.71718e+008, 2.72006e+008, 2.72293e+008, 2.72581e+008, 2.72869e+008, 2.73157e+008, 2.73446e+008, 2.73734e+008, 2.74023e+008, 2.74312e+008, 2.74601e+008, 2.7489e+008, 2.7518e+008, 2.75469e+008, 2.75759e+008, 2.76049e+008, 2.76339e+008, 2.76629e+008, 2.7692e+008, 2.77211e+008, 2.77502e+008, 2.77793e+008, 2.78084e+008, 2.78376e+008, 2.78667e+008, 2.78959e+008, 2.79251e+008, 2.79543e+008, 2.79836e+008, 2.80128e+008, 2.80421e+008, 2.80714e+008, 2.81007e+008, 2.813e+008, 2.81594e+008, 2.81887e+008, 2.82181e+008, 2.82475e+008, 2.82769e+008, 2.83064e+008, 2.83358e+008, 2.83653e+008, 2.83948e+008, 2.84243e+008, 2.84539e+008, 2.84834e+008, 2.8513e+008, 2.85426e+008, 2.85722e+008, 2.86018e+008, 2.86314e+008, 2.86611e+008, 2.86908e+008, 2.87205e+008, 2.87502e+008, 2.87799e+008, 2.88097e+008, 2.88394e+008, 2.88692e+008, 2.8899e+008, 2.89289e+008, 2.89587e+008, 2.89886e+008, 2.90185e+008, 2.90484e+008, 2.90783e+008, 2.91082e+008, 2.91382e+008, 2.91682e+008, 2.91981e+008, 2.92282e+008, 2.92582e+008, 2.92882e+008, 2.93183e+008, 2.93484e+008, 2.93785e+008, 2.94086e+008, 2.94388e+008, 2.94689e+008, 2.94991e+008, 2.95293e+008, 2.95595e+008, 2.95897e+008, 2.962e+008, 2.96503e+008, 2.96806e+008, 2.97109e+008, 2.97412e+008, 2.97715e+008, 2.98019e+008, 2.98323e+008, 2.98627e+008, 2.98931e+008, 2.99235e+008, 2.9954e+008, 2.99845e+008, 3.0015e+008, 3.00455e+008, 3.0076e+008, 3.01066e+008, 3.01371e+008, 3.01677e+008, 3.01983e+008, 3.02289e+008, 3.02596e+008, 3.02902e+008, 3.03209e+008, 3.03516e+008, 3.03823e+008, 3.04131e+008, 3.04438e+008, 3.04746e+008, 3.05054e+008, 3.05362e+008, 3.0567e+008, 3.05979e+008, 3.06287e+008, 3.06596e+008, 3.06905e+008, 3.07215e+008, 3.07524e+008, 3.07834e+008, 3.08143e+008, 3.08453e+008, 3.08763e+008, 3.09074e+008, 3.09384e+008, 3.09695e+008, 3.10006e+008, 3.10317e+008, 3.10628e+008, 3.1094e+008, 3.11251e+008, 3.11563e+008, 3.11875e+008, 3.12188e+008, 3.125e+008, 3.12813e+008, 3.13125e+008, 3.13438e+008, 3.13752e+008, 3.14065e+008, 3.14378e+008, 3.14692e+008, 3.15006e+008, 3.1532e+008, 3.15634e+008, 3.15949e+008, 3.16264e+008, 3.16578e+008, 3.16893e+008, 3.17209e+008, 3.17524e+008, 3.1784e+008, 3.18155e+008, 3.18471e+008, 3.18788e+008, 3.19104e+008, 3.1942e+008, 3.19737e+008, 3.20054e+008, 3.20371e+008, 3.20688e+008, 3.21006e+008, 3.21324e+008, 3.21641e+008, 3.2196e+008, 3.22278e+008, 3.22596e+008, 3.22915e+008, 3.23234e+008, 3.23553e+008, 3.23872e+008, 3.24191e+008, 3.24511e+008, 3.2483e+008, 3.2515e+008, 3.25471e+008, 3.25791e+008, 3.26111e+008, 3.26432e+008, 3.26753e+008, 3.27074e+008, 3.27395e+008, 3.27717e+008, 3.28038e+008, 3.2836e+008, 3.28682e+008, 3.29004e+008, 3.29327e+008, 3.29649e+008, 3.29972e+008, 3.30295e+008, 3.30618e+008, 3.30942e+008, 3.31265e+008, 3.31589e+008, 3.31913e+008, 3.32237e+008, 3.32561e+008, 3.32886e+008, 3.3321e+008, 3.33535e+008, 3.3386e+008, 3.34185e+008, 3.34511e+008, 3.34837e+008, 3.35162e+008, 3.35488e+008, 3.35815e+008, 3.36141e+008, 3.36467e+008, 3.36794e+008, 3.37121e+008, 3.37448e+008, 3.37776e+008, 3.38103e+008, 3.38431e+008, 3.38759e+008, 3.39087e+008, 3.39415e+008, 3.39744e+008, 3.40072e+008, 3.40401e+008, 3.4073e+008, 3.41059e+008, 3.41389e+008, 3.41719e+008, 3.42048e+008, 3.42378e+008, 3.42709e+008, 3.43039e+008, 3.4337e+008, 3.437e+008, 3.44031e+008, 3.44362e+008, 3.44694e+008, 3.45025e+008, 3.45357e+008, 3.45689e+008, 3.46021e+008, 3.46353e+008, 3.46686e+008, 3.47018e+008, 3.47351e+008, 3.47684e+008, 3.48018e+008, 3.48351e+008, 3.48685e+008, 3.49019e+008, 3.49353e+008, 3.49687e+008, 3.50021e+008, 3.50356e+008, 3.50691e+008, 3.51026e+008, 3.51361e+008, 3.51696e+008, 3.52032e+008, 3.52367e+008, 3.52703e+008, 3.53039e+008, 3.53376e+008, 3.53712e+008, 3.54049e+008, 3.54386e+008, 3.54723e+008, 3.5506e+008, 3.55398e+008, 3.55735e+008, 3.56073e+008, 3.56411e+008, 3.5675e+008, 3.57088e+008, 3.57427e+008, 3.57766e+008, 3.58105e+008, 3.58444e+008, 3.58783e+008, 3.59123e+008, 3.59463e+008, 3.59803e+008, 3.60143e+008, 3.60483e+008, 3.60824e+008, 3.61164e+008, 3.61505e+008, 3.61846e+008, 3.62188e+008, 3.62529e+008, 3.62871e+008, 3.63213e+008, 3.63555e+008, 3.63897e+008, 3.6424e+008, 3.64583e+008, 3.64925e+008, 3.65268e+008, 3.65612e+008, 3.65955e+008, 3.66299e+008, 3.66643e+008, 3.66987e+008, 3.67331e+008, 3.67675e+008, 3.6802e+008, 3.68365e+008, 3.6871e+008, 3.69055e+008, 3.694e+008, 3.69746e+008, 3.70092e+008, 3.70438e+008, 3.70784e+008, 3.7113e+008, 3.71477e+008, 3.71824e+008, 3.72171e+008, 3.72518e+008, 3.72865e+008, 3.73213e+008, 3.7356e+008, 3.73908e+008, 3.74256e+008, 3.74605e+008, 3.74953e+008, 3.75302e+008, 3.75651e+008, 3.76e+008, 3.76349e+008, 3.76699e+008, 3.77048e+008, 3.77398e+008, 3.77748e+008, 3.78098e+008, 3.78449e+008, 3.788e+008, 3.7915e+008, 3.79501e+008, 3.79853e+008, 3.80204e+008, 3.80556e+008, 3.80907e+008, 3.81259e+008, 3.81612e+008, 3.81964e+008, 3.82317e+008, 3.82669e+008, 3.83022e+008, 3.83376e+008, 3.83729e+008, 3.84083e+008, 3.84436e+008, 3.8479e+008, 3.85144e+008, 3.85499e+008, 3.85853e+008, 3.86208e+008, 3.86563e+008, 3.86918e+008, 3.87273e+008, 3.87629e+008, 3.87985e+008, 3.88341e+008, 3.88697e+008, 3.89053e+008, 3.8941e+008, 3.89766e+008, 3.90123e+008, 3.9048e+008, 3.90838e+008, 3.91195e+008, 3.91553e+008, 3.91911e+008, 3.92269e+008, 3.92627e+008, 3.92985e+008, 3.93344e+008, 3.93703e+008, 3.94062e+008, 3.94421e+008, 3.94781e+008, 3.9514e+008, 3.955e+008, 3.9586e+008, 3.9622e+008, 3.96581e+008, 3.96941e+008, 3.97302e+008, 3.97663e+008, 3.98025e+008, 3.98386e+008, 3.98748e+008, 3.99109e+008, 3.99471e+008, 3.99834e+008, 4.00196e+008, 4.00559e+008, 4.00921e+008, 4.01284e+008, 4.01648e+008, 4.02011e+008, 4.02374e+008, 4.02738e+008, 4.03102e+008, 4.03466e+008, 4.03831e+008, 4.04195e+008, 4.0456e+008, 4.04925e+008, 4.0529e+008, 4.05656e+008, 4.06021e+008, 4.06387e+008, 4.06753e+008, 4.07119e+008, 4.07485e+008, 4.07852e+008, 4.08219e+008, 4.08586e+008, 4.08953e+008, 4.0932e+008, 4.09688e+008, 4.10055e+008, 4.10423e+008, 4.10791e+008, 4.1116e+008, 4.11528e+008, 4.11897e+008, 4.12266e+008, 4.12635e+008, 4.13004e+008, 4.13374e+008, 4.13743e+008, 4.14113e+008, 4.14483e+008, 4.14854e+008, 4.15224e+008, 4.15595e+008, 4.15966e+008, 4.16337e+008, 4.16708e+008, 4.1708e+008, 4.17452e+008, 4.17823e+008, 4.18195e+008, 4.18568e+008, 4.1894e+008, 4.19313e+008, 4.19686e+008, 4.20059e+008, 4.20432e+008, 4.20806e+008, 4.21179e+008, 4.21553e+008, 4.21927e+008, 4.22302e+008, 4.22676e+008, 4.23051e+008, 4.23426e+008, 4.23801e+008, 4.24176e+008, 4.24552e+008, 4.24927e+008, 4.25303e+008, 4.25679e+008, 4.26055e+008, 4.26432e+008, 4.26809e+008, 4.27185e+008, 4.27563e+008, 4.2794e+008, 4.28317e+008, 4.28695e+008, 4.29073e+008, 4.29451e+008, 4.29829e+008, 4.30208e+008, 4.30586e+008, 4.30965e+008, 4.31344e+008, 4.31723e+008, 4.32103e+008, 4.32483e+008, 4.32862e+008, 4.33243e+008, 4.33623e+008, 4.34003e+008, 4.34384e+008, 4.34765e+008, 4.35146e+008, 4.35527e+008, 4.35909e+008, 4.3629e+008, 4.36672e+008, 4.37054e+008, 4.37437e+008, 4.37819e+008, 4.38202e+008, 4.38585e+008, 4.38968e+008, 4.39351e+008, 4.39734e+008, 4.40118e+008, 4.40502e+008, 4.40886e+008, 4.4127e+008, 4.41655e+008, 4.4204e+008, 4.42424e+008, 4.42809e+008, 4.43195e+008, 4.4358e+008, 4.43966e+008, 4.44352e+008, 4.44738e+008, 4.45124e+008, 4.45511e+008, 4.45897e+008, 4.46284e+008, 4.46671e+008, 4.47059e+008, 4.47446e+008, 4.47834e+008, 4.48222e+008, 4.4861e+008, 4.48998e+008, 4.49387e+008, 4.49775e+008, 4.50164e+008, 4.50553e+008, 4.50943e+008, 4.51332e+008, 4.51722e+008, 4.52112e+008, 4.52502e+008, 4.52892e+008, 4.53283e+008, 4.53674e+008, 4.54065e+008, 4.54456e+008, 4.54847e+008, 4.55239e+008, 4.5563e+008, 4.56022e+008, 4.56414e+008, 4.56807e+008, 4.57199e+008, 4.57592e+008, 4.57985e+008, 4.58378e+008, 4.58771e+008, 4.59165e+008, 4.59559e+008, 4.59953e+008, 4.60347e+008, 4.60741e+008, 4.61136e+008, 4.61531e+008, 4.61926e+008, 4.62321e+008, 4.62716e+008, 4.63112e+008, 4.63508e+008, 4.63904e+008, 4.643e+008, 4.64696e+008, 4.65093e+008, 4.6549e+008, 4.65887e+008, 4.66284e+008, 4.66681e+008, 4.67079e+008, 4.67477e+008, 4.67875e+008, 4.68273e+008, 4.68671e+008, 4.6907e+008, 4.69469e+008, 4.69868e+008, 4.70267e+008, 4.70666e+008, 4.71066e+008, 4.71466e+008, 4.71866e+008, 4.72266e+008, 4.72667e+008, 4.73067e+008, 4.73468e+008, 4.73869e+008, 4.74271e+008, 4.74672e+008, 4.75074e+008, 4.75476e+008, 4.75878e+008, 4.7628e+008, 4.76683e+008, 4.77085e+008, 4.77488e+008, 4.77891e+008, 4.78295e+008, 4.78698e+008, 4.79102e+008, 4.79506e+008, 4.7991e+008, 4.80314e+008, 4.80719e+008, 4.81124e+008, 4.81529e+008, 4.81934e+008, 4.82339e+008, 4.82745e+008, 4.8315e+008, 4.83556e+008, 4.83963e+008, 4.84369e+008, 4.84775e+008, 4.85182e+008, 4.85589e+008, 4.85996e+008, 4.86404e+008, 4.86812e+008, 4.87219e+008, 4.87627e+008, 4.88036e+008, 4.88444e+008, 4.88853e+008, 4.89261e+008, 4.89671e+008, 4.9008e+008, 4.90489e+008, 4.90899e+008, 4.91309e+008, 4.91719e+008, 4.92129e+008, 4.9254e+008, 4.9295e+008, 4.93361e+008, 4.93772e+008, 4.94184e+008, 4.94595e+008, 4.95007e+008, 4.95419e+008, 4.95831e+008, 4.96243e+008, 4.96656e+008, 4.97068e+008, 4.97481e+008, 4.97895e+008, 4.98308e+008, 4.98722e+008, 4.99135e+008, 4.99549e+008, 4.99963e+008, 5.00378e+008, 5.00792e+008, 5.01207e+008, 5.01622e+008, 5.02037e+008, 5.02453e+008, 5.02869e+008, 5.03284e+008, 5.037e+008, 5.04117e+008, 5.04533e+008, 5.0495e+008, 5.05367e+008, 5.05784e+008, 5.06201e+008, 5.06618e+008, 5.07036e+008, 5.07454e+008, 5.07872e+008, 5.0829e+008, 5.08709e+008, 5.09128e+008, 5.09547e+008, 5.09966e+008, 5.10385e+008, 5.10805e+008, 5.11224e+008, 5.11644e+008, 5.12065e+008, 5.12485e+008, 5.12906e+008, 5.13326e+008, 5.13747e+008, 5.14169e+008, 5.1459e+008, 5.15012e+008, 5.15433e+008, 5.15855e+008, 5.16278e+008, 5.167e+008, 5.17123e+008, 5.17546e+008, 5.17969e+008, 5.18392e+008, 5.18816e+008, 5.19239e+008, 5.19663e+008, 5.20087e+008, 5.20512e+008, 5.20936e+008, 5.21361e+008, 5.21786e+008, 5.22211e+008, 5.22636e+008, 5.23062e+008, 5.23488e+008, 5.23914e+008, 5.2434e+008, 5.24766e+008, 5.25193e+008, 5.2562e+008, 5.26047e+008, 5.26474e+008, 5.26901e+008, 5.27329e+008, 5.27757e+008, 5.28185e+008, 5.28613e+008, 5.29042e+008, 5.2947e+008, 5.29899e+008, 5.30328e+008, 5.30758e+008, 5.31187e+008, 5.31617e+008, 5.32047e+008, 5.32477e+008, 5.32907e+008, 5.33338e+008, 5.33769e+008, 5.342e+008, 5.34631e+008, 5.35062e+008, 5.35494e+008, 5.35926e+008, 5.36358e+008, 5.3679e+008, 5.37222e+008, 5.37655e+008, 5.38088e+008, 5.38521e+008, 5.38954e+008, 5.39388e+008, 5.39821e+008, 5.40255e+008, 5.40689e+008, 5.41124e+008, 5.41558e+008, 5.41993e+008, 5.42428e+008, 5.42863e+008, 5.43298e+008, 5.43734e+008, 5.4417e+008, 5.44606e+008, 5.45042e+008, 5.45478e+008, 5.45915e+008, 5.46352e+008, 5.46789e+008, 5.47226e+008, 5.47664e+008, 5.48101e+008, 5.48539e+008, 5.48977e+008, 5.49416e+008, 5.49854e+008, 5.50293e+008, 5.50732e+008, 5.51171e+008, 5.5161e+008, 5.5205e+008, 5.5249e+008, 5.5293e+008, 5.5337e+008, 5.5381e+008, 5.54251e+008, 5.54692e+008, 5.55133e+008, 5.55574e+008, 5.56015e+008, 5.56457e+008, 5.56899e+008, 5.57341e+008, 5.57783e+008, 5.58226e+008, 5.58669e+008, 5.59111e+008, 5.59555e+008, 5.59998e+008, 5.60442e+008, 5.60885e+008, 5.61329e+008, 5.61773e+008, 5.62218e+008, 5.62663e+008, 5.63107e+008, 5.63552e+008, 5.63998e+008, 5.64443e+008, 5.64889e+008, 5.65335e+008, 5.65781e+008, 5.66227e+008, 5.66674e+008, 5.6712e+008, 5.67567e+008, 5.68014e+008, 5.68462e+008, 5.68909e+008, 5.69357e+008, 5.69805e+008, 5.70253e+008, 5.70702e+008, 5.7115e+008, 5.71599e+008, 5.72048e+008, 5.72497e+008, 5.72947e+008, 5.73397e+008, 5.73847e+008, 5.74297e+008, 5.74747e+008, 5.75198e+008, 5.75648e+008, 5.76099e+008, 5.7655e+008, 5.77002e+008, 5.77453e+008, 5.77905e+008, 5.78357e+008, 5.78809e+008, 5.79262e+008, 5.79715e+008, 5.80167e+008, 5.8062e+008, 5.81074e+008, 5.81527e+008, 5.81981e+008, 5.82435e+008, 5.82889e+008, 5.83343e+008, 5.83798e+008, 5.84253e+008, 5.84708e+008, 5.85163e+008, 5.85618e+008, 5.86074e+008, 5.8653e+008, 5.86986e+008, 5.87442e+008, 5.87899e+008, 5.88355e+008, 5.88812e+008, 5.89269e+008, 5.89727e+008, 5.90184e+008, 5.90642e+008, 5.911e+008, 5.91558e+008, 5.92017e+008, 5.92475e+008, 5.92934e+008, 5.93393e+008, 5.93852e+008, 5.94312e+008, 5.94771e+008, 5.95231e+008, 5.95691e+008, 5.96152e+008, 5.96612e+008, 5.97073e+008, 5.97534e+008, 5.97995e+008, 5.98456e+008, 5.98918e+008, 5.9938e+008, 5.99842e+008, 6.00304e+008, 6.00767e+008, 6.01229e+008, 6.01692e+008, 6.02155e+008, 6.02618e+008, 6.03082e+008, 6.03546e+008, 6.0401e+008, 6.04474e+008, 6.04938e+008, 6.05403e+008, 6.05868e+008, 6.06333e+008, 6.06798e+008, 6.07263e+008, 6.07729e+008, 6.08195e+008, 6.08661e+008, 6.09127e+008, 6.09594e+008, 6.1006e+008, 6.10527e+008, 6.10995e+008, 6.11462e+008, 6.11929e+008, 6.12397e+008, 6.12865e+008, 6.13334e+008, 6.13802e+008, 6.14271e+008, 6.14739e+008, 6.15209e+008, 6.15678e+008, 6.16147e+008, 6.16617e+008, 6.17087e+008, 6.17557e+008, 6.18028e+008, 6.18498e+008, 6.18969e+008, 6.1944e+008, 6.19911e+008, 6.20383e+008, 6.20854e+008, 6.21326e+008, 6.21798e+008, 6.22271e+008, 6.22743e+008, 6.23216e+008, 6.23689e+008, 6.24162e+008, 6.24635e+008, 6.25109e+008, 6.25583e+008, 6.26057e+008, 6.26531e+008, 6.27006e+008, 6.2748e+008, 6.27955e+008, 6.2843e+008, 6.28906e+008, 6.29381e+008, 6.29857e+008, 6.30333e+008, 6.30809e+008, 6.31286e+008, 6.31762e+008, 6.32239e+008, 6.32716e+008, 6.33193e+008, 6.33671e+008, 6.34148e+008, 6.34626e+008, 6.35105e+008, 6.35583e+008, 6.36061e+008, 6.3654e+008, 6.37019e+008, 6.37498e+008, 6.37978e+008, 6.38458e+008, 6.38937e+008, 6.39417e+008, 6.39898e+008, 6.40378e+008, 6.40859e+008, 6.4134e+008, 6.41821e+008, 6.42302e+008, 6.42784e+008, 6.43266e+008, 6.43748e+008, 6.4423e+008, 6.44713e+008, 6.45195e+008, 6.45678e+008, 6.46161e+008, 6.46645e+008, 6.47128e+008, 6.47612e+008, 6.48096e+008, 6.4858e+008, 6.49065e+008, 6.49549e+008, 6.50034e+008, 6.50519e+008, 6.51004e+008, 6.5149e+008, 6.51976e+008, 6.52461e+008, 6.52948e+008, 6.53434e+008, 6.53921e+008, 6.54407e+008, 6.54894e+008, 6.55382e+008, 6.55869e+008, 6.56357e+008, 6.56845e+008, 6.57333e+008, 6.57821e+008, 6.5831e+008, 6.58798e+008, 6.59287e+008, 6.59777e+008, 6.60266e+008, 6.60756e+008, 6.61245e+008, 6.61736e+008, 6.62226e+008, 6.62716e+008, 6.63207e+008, 6.63698e+008, 6.64189e+008, 6.6468e+008, 6.65172e+008, 6.65664e+008, 6.66156e+008, 6.66648e+008, 6.67141e+008, 6.67633e+008, 6.68126e+008, 6.68619e+008, 6.69113e+008, 6.69606e+008, 6.701e+008, 6.70594e+008, 6.71088e+008, 6.71583e+008, 6.72077e+008, 6.72572e+008, 6.73067e+008, 6.73563e+008, 6.74058e+008, 6.74554e+008, 6.7505e+008, 6.75546e+008, 6.76042e+008, 6.76539e+008, 6.77036e+008, 6.77533e+008, 6.7803e+008, 6.78528e+008, 6.79025e+008, 6.79523e+008, 6.80021e+008, 6.8052e+008, 6.81018e+008, 6.81517e+008, 6.82016e+008, 6.82515e+008, 6.83015e+008, 6.83515e+008, 6.84014e+008, 6.84515e+008, 6.85015e+008, 6.85515e+008, 6.86016e+008, 6.86517e+008, 6.87018e+008, 6.8752e+008, 6.88022e+008, 6.88523e+008, 6.89026e+008, 6.89528e+008, 6.9003e+008, 6.90533e+008, 6.91036e+008, 6.91539e+008, 6.92043e+008, 6.92546e+008, 6.9305e+008, 6.93554e+008, 6.94059e+008, 6.94563e+008, 6.95068e+008, 6.95573e+008, 6.96078e+008, 6.96583e+008, 6.97089e+008, 6.97595e+008, 6.98101e+008, 6.98607e+008, 6.99114e+008, 6.9962e+008, 7.00127e+008, 7.00634e+008, 7.01142e+008, 7.01649e+008, 7.02157e+008, 7.02665e+008, 7.03173e+008, 7.03682e+008, 7.04191e+008, 7.047e+008, 7.05209e+008, 7.05718e+008, 7.06228e+008, 7.06737e+008, 7.07247e+008, 7.07758e+008, 7.08268e+008, 7.08779e+008, 7.0929e+008, 7.09801e+008, 7.10312e+008, 7.10824e+008, 7.11336e+008, 7.11848e+008, 7.1236e+008, 7.12872e+008, 7.13385e+008, 7.13898e+008, 7.14411e+008, 7.14924e+008, 7.15438e+008, 7.15952e+008, 7.16466e+008, 7.1698e+008, 7.17494e+008, 7.18009e+008, 7.18524e+008, 7.19039e+008, 7.19554e+008, 7.2007e+008, 7.20586e+008, 7.21102e+008, 7.21618e+008, 7.22134e+008, 7.22651e+008, 7.23168e+008, 7.23685e+008, 7.24202e+008, 7.2472e+008, 7.25238e+008, 7.25756e+008, 7.26274e+008, 7.26792e+008, 7.27311e+008, 7.2783e+008, 7.28349e+008, 7.28868e+008, 7.29388e+008, 7.29907e+008, 7.30427e+008, 7.30948e+008, 7.31468e+008, 7.31989e+008, 7.3251e+008, 7.33031e+008, 7.33552e+008, 7.34074e+008, 7.34595e+008, 7.35117e+008, 7.3564e+008, 7.36162e+008, 7.36685e+008, 7.37208e+008, 7.37731e+008, 7.38254e+008, 7.38777e+008, 7.39301e+008, 7.39825e+008, 7.40349e+008, 7.40874e+008, 7.41399e+008, 7.41923e+008, 7.42449e+008, 7.42974e+008, 7.43499e+008, 7.44025e+008, 7.44551e+008, 7.45077e+008, 7.45604e+008, 7.46131e+008, 7.46657e+008, 7.47185e+008, 7.47712e+008, 7.48239e+008, 7.48767e+008, 7.49295e+008, 7.49823e+008, 7.50352e+008, 7.50881e+008, 7.51409e+008, 7.51939e+008, 7.52468e+008, 7.52998e+008, 7.53527e+008, 7.54057e+008, 7.54588e+008, 7.55118e+008, 7.55649e+008, 7.5618e+008, 7.56711e+008, 7.57242e+008, 7.57774e+008, 7.58306e+008, 7.58838e+008, 7.5937e+008, 7.59902e+008, 7.60435e+008, 7.60968e+008, 7.61501e+008, 7.62034e+008, 7.62568e+008, 7.63102e+008, 7.63636e+008, 7.6417e+008, 7.64705e+008, 7.65239e+008, 7.65774e+008, 7.6631e+008, 7.66845e+008, 7.67381e+008, 7.67916e+008, 7.68452e+008, 7.68989e+008, 7.69525e+008, 7.70062e+008, 7.70599e+008, 7.71136e+008, 7.71674e+008, 7.72211e+008, 7.72749e+008, 7.73287e+008, 7.73826e+008, 7.74364e+008, 7.74903e+008, 7.75442e+008, 7.75981e+008, 7.7652e+008, 7.7706e+008, 7.776e+008, 7.7814e+008, 7.7868e+008, 7.79221e+008, 7.79762e+008, 7.80303e+008, 7.80844e+008, 7.81386e+008, 7.81927e+008, 7.82469e+008, 7.83011e+008, 7.83554e+008, 7.84096e+008, 7.84639e+008, 7.85182e+008, 7.85725e+008, 7.86269e+008, 7.86813e+008, 7.87356e+008, 7.87901e+008, 7.88445e+008, 7.8899e+008, 7.89535e+008, 7.9008e+008, 7.90625e+008, 7.9117e+008, 7.91716e+008, 7.92262e+008, 7.92808e+008, 7.93355e+008, 7.93901e+008, 7.94448e+008, 7.94995e+008, 7.95543e+008, 7.9609e+008, 7.96638e+008, 7.97186e+008, 7.97734e+008, 7.98283e+008, 7.98831e+008, 7.9938e+008, 7.99929e+008, 8.00479e+008, 8.01028e+008, 8.01578e+008, 8.02128e+008, 8.02679e+008, 8.03229e+008, 8.0378e+008, 8.04331e+008, 8.04882e+008, 8.05433e+008, 8.05985e+008, 8.06537e+008, 8.07089e+008, 8.07641e+008, 8.08194e+008, 8.08746e+008, 8.09299e+008, 8.09853e+008, 8.10406e+008, 8.1096e+008, 8.11514e+008, 8.12068e+008, 8.12622e+008, 8.13177e+008, 8.13732e+008, 8.14287e+008, 8.14842e+008, 8.15397e+008, 8.15953e+008, 8.16509e+008, 8.17065e+008, 8.17622e+008, 8.18178e+008, 8.18735e+008, 8.19292e+008, 8.19849e+008, 8.20407e+008, 8.20965e+008, 8.21523e+008, 8.22081e+008, 8.22639e+008, 8.23198e+008, 8.23757e+008, 8.24316e+008, 8.24875e+008, 8.25435e+008, 8.25995e+008, 8.26555e+008, 8.27115e+008, 8.27676e+008, 8.28236e+008, 8.28797e+008, 8.29358e+008, 8.2992e+008, 8.30481e+008, 8.31043e+008, 8.31605e+008, 8.32168e+008, 8.3273e+008, 8.33293e+008, 8.33856e+008, 8.34419e+008, 8.34983e+008, 8.35546e+008, 8.3611e+008, 8.36674e+008, 8.37239e+008, 8.37803e+008, 8.38368e+008, 8.38933e+008, 8.39498e+008, 8.40064e+008, 8.4063e+008, 8.41196e+008, 8.41762e+008, 8.42328e+008, 8.42895e+008, 8.43462e+008, 8.44029e+008, 8.44596e+008, 8.45164e+008, 8.45732e+008, 8.463e+008, 8.46868e+008, 8.47436e+008, 8.48005e+008, 8.48574e+008, 8.49143e+008, 8.49713e+008, 8.50282e+008, 8.50852e+008, 8.51422e+008, 8.51993e+008, 8.52563e+008, 8.53134e+008, 8.53705e+008, 8.54276e+008, 8.54848e+008, 8.55419e+008, 8.55991e+008, 8.56563e+008, 8.57136e+008, 8.57708e+008, 8.58281e+008, 8.58854e+008, 8.59427e+008, 8.60001e+008, 8.60575e+008, 8.61149e+008, 8.61723e+008, 8.62297e+008, 8.62872e+008, 8.63447e+008, 8.64022e+008, 8.64597e+008, 8.65173e+008, 8.65749e+008, 8.66325e+008, 8.66901e+008, 8.67478e+008, 8.68054e+008, 8.68631e+008, 8.69209e+008, 8.69786e+008, 8.70364e+008, 8.70942e+008, 8.7152e+008, 8.72098e+008, 8.72677e+008, 8.73256e+008, 8.73835e+008, 8.74414e+008, 8.74993e+008, 8.75573e+008, 8.76153e+008, 8.76733e+008, 8.77314e+008, 8.77894e+008, 8.78475e+008, 8.79056e+008, 8.79638e+008, 8.80219e+008, 8.80801e+008, 8.81383e+008, 8.81965e+008, 8.82548e+008, 8.83131e+008, 8.83714e+008, 8.84297e+008, 8.8488e+008, 8.85464e+008, 8.86048e+008, 8.86632e+008, 8.87216e+008, 8.87801e+008, 8.88386e+008, 8.88971e+008, 8.89556e+008, 8.90141e+008, 8.90727e+008, 8.91313e+008, 8.91899e+008, 8.92486e+008, 8.93072e+008, 8.93659e+008, 8.94246e+008, 8.94834e+008, 8.95421e+008, 8.96009e+008, 8.96597e+008, 8.97185e+008, 8.97774e+008, 8.98363e+008, 8.98952e+008, 8.99541e+008, 9.0013e+008, 9.0072e+008, 9.0131e+008, 9.019e+008, 9.0249e+008, 9.03081e+008, 9.03672e+008, 9.04263e+008, 9.04854e+008, 9.05446e+008, 9.06037e+008, 9.06629e+008, 9.07222e+008, 9.07814e+008, 9.08407e+008, 9.09e+008, 9.09593e+008, 9.10186e+008, 9.1078e+008, 9.11374e+008, 9.11968e+008, 9.12562e+008, 9.13157e+008, 9.13751e+008, 9.14346e+008, 9.14942e+008, 9.15537e+008, 9.16133e+008, 9.16729e+008, 9.17325e+008, 9.17921e+008, 9.18518e+008, 9.19115e+008, 9.19712e+008, 9.20309e+008, 9.20907e+008, 9.21505e+008, 9.22103e+008, 9.22701e+008, 9.23299e+008, 9.23898e+008, 9.24497e+008, 9.25096e+008, 9.25696e+008, 9.26295e+008, 9.26895e+008, 9.27495e+008, 9.28096e+008, 9.28696e+008, 9.29297e+008, 9.29898e+008, 9.305e+008, 9.31101e+008, 9.31703e+008, 9.32305e+008, 9.32907e+008, 9.3351e+008, 9.34112e+008, 9.34715e+008, 9.35318e+008, 9.35922e+008, 9.36525e+008, 9.37129e+008, 9.37733e+008, 9.38338e+008, 9.38942e+008, 9.39547e+008, 9.40152e+008, 9.40757e+008, 9.41363e+008, 9.41968e+008, 9.42574e+008, 9.43181e+008, 9.43787e+008, 9.44394e+008, 9.45001e+008, 9.45608e+008, 9.46215e+008, 9.46823e+008, 9.47431e+008, 9.48039e+008, 9.48647e+008, 9.49255e+008, 9.49864e+008, 9.50473e+008, 9.51082e+008, 9.51692e+008, 9.52302e+008, 9.52912e+008, 9.53522e+008, 9.54132e+008, 9.54743e+008, 9.55354e+008, 9.55965e+008, 9.56576e+008, 9.57188e+008, 9.578e+008, 9.58412e+008, 9.59024e+008, 9.59636e+008, 9.60249e+008, 9.60862e+008, 9.61475e+008, 9.62089e+008, 9.62703e+008, 9.63316e+008, 9.63931e+008, 9.64545e+008, 9.6516e+008, 9.65775e+008, 9.6639e+008, 9.67005e+008, 9.67621e+008, 9.68236e+008, 9.68852e+008, 9.69469e+008, 9.70085e+008, 9.70702e+008, 9.71319e+008, 9.71936e+008, 9.72554e+008, 9.73171e+008, 9.73789e+008, 9.74407e+008, 9.75026e+008, 9.75644e+008, 9.76263e+008, 9.76882e+008, 9.77502e+008, 9.78121e+008, 9.78741e+008, 9.79361e+008, 9.79981e+008, 9.80602e+008, 9.81223e+008, 9.81844e+008, 9.82465e+008, 9.83086e+008, 9.83708e+008, 9.8433e+008, 9.84952e+008, 9.85575e+008, 9.86197e+008, 9.8682e+008, 9.87443e+008, 9.88067e+008, 9.8869e+008, 9.89314e+008, 9.89938e+008, 9.90562e+008, 9.91187e+008, 9.91812e+008, 9.92437e+008, 9.93062e+008, 9.93687e+008, 9.94313e+008, 9.94939e+008, 9.95565e+008, 9.96192e+008, 9.96818e+008, 9.97445e+008, 9.98072e+008, 9.987e+008, 9.99327e+008, 9.99955e+008, 1.00058e+009, 1.00121e+009, 1.00184e+009, 1.00247e+009, 1.0031e+009, 1.00373e+009, 1.00436e+009, 1.00499e+009, 1.00562e+009, 1.00625e+009, 1.00688e+009, 1.00751e+009, 1.00814e+009, 1.00877e+009, 1.0094e+009, 1.01003e+009, 1.01066e+009, 1.0113e+009, 1.01193e+009, 1.01256e+009, 1.01319e+009, 1.01383e+009, 1.01446e+009, 1.01509e+009, 1.01573e+009, 1.01636e+009, 1.017e+009, 1.01763e+009, 1.01827e+009, 1.0189e+009, 1.01954e+009, 1.02017e+009, 1.02081e+009, 1.02144e+009, 1.02208e+009, 1.02271e+009, 1.02335e+009, 1.02399e+009, 1.02463e+009, 1.02526e+009, 1.0259e+009, 1.02654e+009, 1.02718e+009, 1.02781e+009, 1.02845e+009, 1.02909e+009, 1.02973e+009, 1.03037e+009, 1.03101e+009, 1.03165e+009, 1.03229e+009, 1.03293e+009, 1.03357e+009, 1.03421e+009, 1.03485e+009, 1.03549e+009, 1.03613e+009, 1.03678e+009, 1.03742e+009, 1.03806e+009, 1.0387e+009, 1.03934e+009, 1.03999e+009, 1.04063e+009, 1.04127e+009, 1.04192e+009, 1.04256e+009, 1.0432e+009, 1.04385e+009, 1.04449e+009, 1.04514e+009, 1.04578e+009, 1.04643e+009, 1.04707e+009, 1.04772e+009, 1.04837e+009, 1.04901e+009, 1.04966e+009, 1.0503e+009, 1.05095e+009, 1.0516e+009, 1.05225e+009, 1.05289e+009, 1.05354e+009, 1.05419e+009, 1.05484e+009, 1.05549e+009, 1.05613e+009, 1.05678e+009, 1.05743e+009, 1.05808e+009, 1.05873e+009, 1.05938e+009, 1.06003e+009, 1.06068e+009, 1.06133e+009, 1.06198e+009, 1.06264e+009, 1.06329e+009, 1.06394e+009, 1.06459e+009, 1.06524e+009, 1.06589e+009, 1.06655e+009, 1.0672e+009, 1.06785e+009, 1.06851e+009, 1.06916e+009, 1.06981e+009, 1.07047e+009, 1.07112e+009, 1.07178e+009, 1.07243e+009, 1.07309e+009, 1.07374e+009 };
						int n[4096] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8, 6, 9, 2, 4, 2, 11, 10, 2, 5, 5, 4, 3, 10, 9, 7, 8, 9, 3, 9, 11, 6, 11, 4, 8, 9, 4, 7, 9, 2, 7, 3, 8, 11, 3, 10, 6, 5, 9, 3, 7, 6, 7, 4, 7, 5, 3, 6, 11, 10, 7, 2, 4, 2, 8, 8, 6, 3, 10, 6, 8, 4, 2, 4, 11, 5, 10, 2, 9, 5, 7, 3, 2, 9, 4, 8, 4, 11, 2, 6, 5, 11, 10, 2, 7, 7, 11, 3, 8, 4, 5, 4, 8, 5, 5, 10, 3, 4, 6, 9, 2, 6, 3, 9, 7, 2, 9, 2, 6, 7, 2, 7, 4, 11, 2, 6, 7, 9, 5, 7, 5, 10, 10, 5, 7, 6, 2, 4, 7, 3, 6, 5, 3, 3, 8, 2, 7, 11, 3, 2, 5, 7, 9, 8, 8, 2, 4, 6, 3, 4, 6, 11, 5, 6, 6, 8, 11, 5, 4, 6, 2, 8, 7, 8, 2, 9, 6, 7, 4, 3, 8, 8, 5, 3, 7, 2, 4, 5, 6, 5, 6, 11, 9, 9, 4, 10, 5, 3, 2, 5, 6, 7, 3, 5, 2, 9, 10, 3, 8, 10, 5, 6, 5, 11, 2, 8, 2, 2, 6, 10, 6, 10, 9, 6, 8, 5, 4, 11, 5, 5, 11, 3, 6, 2, 11, 11, 3, 10, 11, 6, 6, 4, 7, 8, 11, 6, 3, 4, 5, 10, 5, 10, 2, 10, 5, 9, 4, 10, 2, 5, 6, 8, 11, 3, 7, 7, 7, 2, 5, 8, 9, 8, 9, 8, 9, 6, 5, 2, 7, 7, 10, 5, 7, 5, 10, 9, 11, 7, 2, 5, 3, 2, 5, 4, 7, 4, 5, 4, 3, 5, 6, 10, 10, 2, 6, 11, 11, 9, 4, 10, 5, 8, 6, 11, 6, 10, 7, 11, 2, 2, 9, 5, 8, 9, 8, 3, 4, 8, 2, 3, 4, 8, 9, 10, 3, 7, 4, 8, 7, 7, 6, 2, 8, 3, 5, 4, 2, 5, 7, 4, 8, 8, 10, 5, 7, 5, 3, 2, 11, 7, 11, 2, 6, 8, 2, 11, 6, 8, 3, 5, 8, 7, 5, 5, 10, 5, 6, 6, 11, 4, 5, 8, 9, 10, 10, 9, 11, 11, 4, 6, 7, 3, 2, 8, 4, 4, 3, 10, 2, 4, 6, 11, 11, 10, 5, 8, 6, 10, 8, 5, 6, 11, 2, 9, 6, 3, 9, 7, 4, 5, 4, 8, 2, 7, 8, 3, 10, 4, 11, 7, 11, 9, 10, 7, 2, 4, 10, 4, 3, 2, 6, 11, 7, 8, 11, 10, 4, 11, 4, 7, 2, 6, 7, 11, 9, 5, 5, 10, 2, 6, 2, 5, 9, 5, 8, 9, 11, 10, 3, 6, 9, 7, 10, 8, 10, 9, 4, 3, 6, 11, 11, 5, 10, 7, 5, 9, 3, 6, 6, 7, 5, 10, 11, 10, 11, 7, 6, 4, 4, 8, 6, 3, 4, 10, 3, 10, 9, 9, 10, 9, 9, 6, 5, 6, 8, 5, 9, 4, 9, 6, 4, 3, 9, 4, 3, 10, 4, 6, 2, 3, 8, 9, 8, 5, 9, 8, 11, 7, 2, 2, 9, 3, 11, 6, 2, 10, 3, 3, 9, 6, 3, 7, 10, 11, 2, 9, 2, 7, 7, 7, 2, 7, 11, 4, 11, 5, 7, 5, 6, 9, 5, 3, 8, 3, 2, 4, 4, 8, 10, 2, 6, 9, 5, 5, 6, 5, 8, 3, 11, 4, 11, 3, 3, 5, 11, 4, 2, 9, 2, 11, 4, 2, 11, 6, 6, 8, 2, 8, 4, 7, 9, 2, 6, 3, 5, 2, 9, 5, 8, 8, 2, 8, 6, 9, 7, 3, 11, 10, 10, 6, 11, 9, 3, 10, 9, 7, 9, 3, 6, 5, 3, 2, 5, 6, 7, 5, 4, 4, 2, 9, 9, 4, 4, 9, 8, 9, 11, 5, 2, 4, 11, 7, 11, 10, 7, 6, 5, 3, 11, 3, 6, 2, 3, 6, 5, 6, 6, 11, 6, 4, 9, 5, 6, 2, 10, 10, 9, 10, 4, 3, 11, 9, 2, 7, 11, 8, 4, 11, 10, 7, 9, 8, 8, 6, 2, 10, 6, 3, 9, 5, 7, 10, 10, 5, 9, 3, 7, 5, 9, 5, 11, 7, 11, 6, 7, 4, 3, 5, 4, 11, 3, 8, 4, 8, 10, 7, 11, 3, 6, 11, 10, 6, 2, 5, 8, 11, 4, 8, 3, 4, 3, 10, 10, 4, 11, 10, 11, 7, 8, 4, 5, 4, 11, 2, 10, 9, 11, 11, 6, 10, 8, 2, 3, 5, 9, 6, 2, 4, 5, 9, 7, 6, 3, 11, 4, 10, 9, 2, 7, 9, 9, 8, 4, 11, 3, 5, 9, 8, 2, 8, 5, 7, 11, 10, 10, 8, 9, 9, 2, 11, 3, 8, 3, 6, 9, 5, 9, 8, 7, 6, 8, 6, 4, 3, 6, 5, 10, 5, 2, 4, 4, 5, 2, 5, 3, 9, 3, 11, 2, 10, 5, 6, 4, 8, 8, 9, 4, 7, 5, 8, 5, 6, 4, 6, 8, 10, 9, 5, 5, 10, 7, 2, 10, 3, 5, 5, 5, 11, 3, 3, 11, 6, 11, 4, 2, 11, 6, 7, 3, 6, 7, 10, 8, 6, 5, 11, 10, 6, 5, 7, 11, 7, 5, 6, 7, 10, 6, 3, 8, 2, 11, 9, 10, 5, 2, 4, 5, 10, 10, 10, 2, 6, 5, 10, 11, 11, 3, 5, 10, 3, 9, 7, 6, 4, 3, 6, 11, 8, 9, 5, 7, 5, 5, 2, 6, 2, 8, 7, 11, 9, 3, 3, 7, 8, 9, 3, 2, 4, 4, 4, 10, 9, 11, 8, 4, 10, 8, 3, 10, 4, 6, 5, 5, 6, 2, 2, 6, 7, 10, 7, 3, 11, 7, 5, 9, 6, 8, 10, 6, 11, 7, 9, 4, 7, 6, 8, 6, 4, 7, 10, 4, 2, 9, 4, 2, 10, 11, 10, 8, 6, 4, 10, 10, 11, 11, 7, 8, 2, 11, 2, 7, 8, 8, 3, 5, 4, 2, 8, 11, 4, 4, 5, 5, 9, 6, 11, 11, 3, 7, 9, 4, 10, 11, 7, 8, 2, 6, 10, 6, 11, 5, 7, 3, 9, 2, 9, 10, 5, 10, 3, 2, 11, 3, 8, 5, 9, 10, 2, 7, 9, 6, 7, 5, 4, 4, 9, 10, 9, 10, 3, 6, 4, 7, 10, 10, 11, 6, 8, 8, 8, 7, 6, 11, 4, 9, 2, 9, 11, 6, 2, 8, 5, 2, 3, 5, 10, 2, 3, 4, 7, 11, 3, 5, 7, 11, 5, 10, 10, 2, 9, 7, 6, 3, 10, 5, 3, 2, 11, 4, 9, 5, 4, 8, 8, 8, 10, 10, 6, 6, 6, 8, 11, 7, 3, 8, 4, 5, 2, 11, 2, 3, 7, 8, 11, 2, 3, 8, 8, 6, 5, 6, 2, 9, 7, 11, 4, 7, 2, 6, 2, 5, 9, 3, 7, 3, 7, 6, 10, 8, 2, 10, 5, 2, 7, 5, 5, 4, 4, 11, 11, 9, 4, 5, 6, 9, 10, 4, 9, 10, 8, 2, 6, 7, 5, 7, 11, 3, 4, 6, 8, 6, 11, 6, 5, 8, 11, 11, 10, 10, 7, 10, 11, 2, 2, 2, 10, 6, 8, 3, 3, 8, 9, 7, 2, 8, 3, 7, 8, 3, 2, 2, 7, 9, 4, 10, 10, 4, 9, 3, 10, 7, 5, 11, 11, 4, 5, 9, 10, 5, 5, 9, 11, 10, 8, 4, 6, 5, 9, 4, 5, 5, 5, 6, 8, 10, 3, 5, 8, 7, 7, 7, 6, 5, 10, 2, 2, 11, 5, 11, 7, 8, 3, 11, 3, 5, 2, 4, 2, 2, 8, 8, 10, 2, 10, 3, 10, 6, 9, 3, 7, 5, 3, 2, 4, 9, 8, 11, 6, 2, 11, 2, 6, 9, 11, 2, 5, 11, 9, 3, 10, 6, 3, 2, 8, 4, 5, 10, 8, 8, 9, 3, 5, 7, 8, 9, 6, 9, 8, 11, 5, 10, 11, 10, 8, 7, 5, 6, 8, 4, 3, 8, 2, 6, 3, 10, 7, 2, 11, 3, 9, 3, 11, 9, 11, 8, 4, 9, 7, 6, 3, 2, 2, 2, 2, 3, 2, 3, 11, 8, 9, 11, 9, 5, 7, 8, 7, 8, 6, 5, 3, 3, 9, 10, 6, 11, 4, 4, 9, 7, 10, 7, 4, 11, 9, 11, 4, 2, 7, 5, 11, 5, 8, 6, 2, 4, 2, 11, 11, 3, 2, 3, 8, 10, 7, 2, 8, 9, 4, 9, 2, 7, 3, 4, 3, 10, 10, 10, 2, 8, 3, 3, 6, 6, 5, 4, 6, 6, 4, 7, 8, 4, 4, 11, 7, 8, 8, 7, 6, 5, 9, 6, 3, 6, 6, 11, 6, 11, 8, 2, 11, 4, 11, 3, 5, 10, 8, 7, 2, 3, 11, 4, 10, 11, 9, 8, 11, 9, 2, 7, 6, 11, 11, 9, 9, 2, 10, 7, 9, 8, 7, 7, 11, 6, 9, 2, 2, 10, 9, 7, 6, 10, 11, 3, 6, 8, 8, 8, 6, 10, 5, 7, 4, 5, 8, 9, 7, 5, 5, 6, 4, 4, 2, 5, 3, 10, 11, 3, 3, 7, 8, 6, 9, 11, 11, 8, 4, 6, 8, 6, 2, 8, 7, 8, 3, 9, 3, 7, 7, 6, 2, 5, 4, 9, 6, 6, 4, 3, 4, 4, 9, 9, 6, 4, 11, 5, 2, 9, 7, 2, 11, 6, 8, 2, 5, 4, 3, 6, 5, 2, 2, 9, 3, 4, 7, 9, 4, 8, 9, 6, 4, 6, 10, 8, 3, 10, 8, 9, 10, 10, 11, 11, 6, 11, 10, 11, 10, 2, 2, 11, 7, 3, 9, 7, 8, 9, 10, 7, 2, 8, 11, 10, 8, 3, 2, 7, 11, 3, 6, 5, 7, 7, 3, 4, 2, 2, 4, 9, 9, 5, 8, 9, 9, 9, 5, 7, 11, 10, 7, 10, 4, 3, 11, 7, 2, 4, 2, 11, 2, 10, 6, 3, 11, 11, 4, 7, 11, 2, 9, 7, 4, 8, 4, 9, 3, 5, 4, 7, 2, 10, 10, 6, 7, 8, 7, 11, 5, 4, 11, 4, 7, 7, 10, 8, 2, 10, 9, 5, 4, 8, 9, 3, 3, 2, 2, 10, 2, 9, 8, 5, 6, 5, 10, 6, 6, 8, 6, 8, 3, 8, 2, 6, 9, 10, 6, 4, 10, 10, 3, 8, 11, 2, 8, 11, 9, 2, 2, 2, 8, 5, 6, 2, 3, 9, 4, 8, 8, 6, 2, 6, 5, 9, 2, 4, 2, 4, 11, 7, 7, 8, 6, 3, 4, 10, 5, 10, 8, 5, 11, 8, 4, 7, 8, 11, 9, 6, 9, 9, 10, 5, 7, 3, 7, 2, 8, 11, 6, 2, 7, 6, 11, 9, 9, 4, 4, 10, 11, 10, 6, 4, 4, 3, 3, 9, 6, 5, 4, 6, 7, 9, 7, 7, 11, 11, 11, 5, 5, 7, 7, 8, 8, 11, 7, 6, 10, 11, 5, 8, 9, 2, 10, 5, 6, 10, 8, 2, 7, 9, 6, 3, 10, 9, 8, 2, 2, 10, 3, 4, 3, 7, 7, 2, 2, 2, 2, 4, 3, 10, 8, 10, 6, 10, 4, 9, 6, 9, 3, 9, 8, 2, 5, 5, 8, 11, 10, 11, 3, 6, 5, 4, 4, 5, 10, 6, 7, 3, 11, 3, 11, 5, 5, 9, 11, 6, 9, 10, 4, 6, 2, 9, 10, 11, 9, 2, 7, 8, 11, 2, 2, 2, 2, 8, 5, 11, 10, 9, 8, 7, 4, 8, 6, 6, 11, 10, 6, 8, 7, 8, 6, 4, 9, 3, 9, 6, 9, 3, 2, 9, 9, 3, 5, 5, 9, 11, 11, 10, 3, 11, 9, 11, 6, 6, 6, 9, 8, 5, 7, 2, 10, 4, 6, 11, 10, 8, 2, 9, 4, 5, 6, 7, 3, 2, 5, 5, 11, 4, 6, 5, 2, 10, 9, 11, 6, 8, 8, 7, 10, 6, 11, 9, 3, 6, 7, 5, 9, 11, 6, 4, 6, 3, 9, 4, 11, 3, 5, 10, 3, 9, 10, 4, 4, 2, 9, 8, 11, 2, 6, 10, 10, 2, 9, 8, 5, 4, 10, 6, 11, 11, 7, 5, 3, 6, 9, 8, 2, 4, 3, 3, 3, 6, 4, 5, 2, 5, 3, 11, 10, 6, 2, 3 };
						int an[4096];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 9) {
						static float s_mean[8192] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 1132, 1133, 1134, 1135, 1136, 1137, 1138, 1139, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149, 1150, 1151, 1152, 1153, 1154, 1155, 1156, 1157, 1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187, 1188, 1189, 1190, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249, 1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259, 1260, 1261, 1262, 1263, 1264, 1265, 1266, 1267, 1268, 1269, 1270, 1271, 1272, 1273, 1274, 1275, 1276, 1277, 1278, 1279, 1280, 1281, 1282, 1283, 1284, 1285, 1286, 1287, 1288, 1289, 1290, 1291, 1292, 1293, 1294, 1295, 1296, 1297, 1298, 1299, 1300, 1301, 1302, 1303, 1304, 1305, 1306, 1307, 1308, 1309, 1310, 1311, 1312, 1313, 1314, 1315, 1316, 1317, 1318, 1319, 1320, 1321, 1322, 1323, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1332, 1333, 1334, 1335, 1336, 1337, 1338, 1339, 1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1371, 1372, 1373, 1374, 1375, 1376, 1377, 1378, 1379, 1380, 1381, 1382, 1383, 1384, 1385, 1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396, 1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405, 1406, 1407, 1408, 1409, 1410, 1411, 1412, 1413, 1414, 1415, 1416, 1417, 1418, 1419, 1420, 1421, 1422, 1423, 1424, 1425, 1426, 1427, 1428, 1429, 1430, 1431, 1432, 1433, 1434, 1435, 1436, 1437, 1438, 1439, 1440, 1441, 1442, 1443, 1444, 1445, 1446, 1447, 1448, 1449, 1450, 1451, 1452, 1453, 1454, 1455, 1456, 1457, 1458, 1459, 1460, 1461, 1462, 1463, 1464, 1465, 1466, 1467, 1468, 1469, 1470, 1471, 1472, 1473, 1474, 1475, 1476, 1477, 1478, 1479, 1480, 1481, 1482, 1483, 1484, 1485, 1486, 1487, 1488, 1489, 1490, 1491, 1492, 1493, 1494, 1495, 1496, 1497, 1498, 1499, 1500, 1501, 1502, 1503, 1504, 1505, 1506, 1507, 1508, 1509, 1510, 1511, 1512, 1513, 1514, 1515, 1516, 1517, 1518, 1519, 1520, 1521, 1522, 1523, 1524, 1525, 1526, 1527, 1528, 1529, 1530, 1531, 1532, 1533, 1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545, 1546, 1547, 1548, 1549, 1550, 1551, 1552, 1553, 1554, 1555, 1556, 1557, 1558, 1559, 1560, 1561, 1562, 1563, 1564, 1565, 1566, 1567, 1568, 1569, 1570, 1571, 1572, 1573, 1574, 1575, 1576, 1577, 1578, 1579, 1580, 1581, 1582, 1583, 1584, 1585, 1586, 1587, 1588, 1589, 1590, 1591, 1592, 1593, 1594, 1595, 1596, 1597, 1598, 1599, 1600, 1601, 1602, 1603, 1604, 1605, 1606, 1607, 1608, 1609, 1610, 1611, 1612, 1613, 1614, 1615, 1616, 1617, 1618, 1619, 1620, 1621, 1622, 1623, 1624, 1625, 1626, 1627, 1628, 1629, 1630, 1631, 1632, 1633, 1634, 1635, 1636, 1637, 1638, 1639, 1640, 1641, 1642, 1643, 1644, 1645, 1646, 1647, 1648, 1649, 1650, 1651, 1652, 1653, 1654, 1655, 1656, 1657, 1658, 1659, 1660, 1661, 1662, 1663, 1664, 1665, 1666, 1667, 1668, 1669, 1670, 1671, 1672, 1673, 1674, 1675, 1676, 1677, 1678, 1679, 1680, 1681, 1682, 1683, 1684, 1685, 1686, 1687, 1688, 1689, 1690, 1691, 1692, 1693, 1694, 1695, 1696, 1697, 1698, 1699, 1700, 1701, 1702, 1703, 1704, 1705, 1706, 1707, 1708, 1709, 1710, 1711, 1712, 1713, 1714, 1715, 1716, 1717, 1718, 1719, 1720, 1721, 1722, 1723, 1724, 1725, 1726, 1727, 1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736, 1737, 1738, 1739, 1740, 1741, 1742, 1743, 1744, 1745, 1746, 1747, 1748, 1749, 1750, 1751, 1752, 1753, 1754, 1755, 1756, 1757, 1758, 1759, 1760, 1761, 1762, 1763, 1764, 1765, 1766, 1767, 1768, 1769, 1770, 1771, 1772, 1773, 1774, 1775, 1776, 1777, 1778, 1779, 1780, 1781, 1782, 1783, 1784, 1785, 1786, 1787, 1788, 1789, 1790, 1791, 1792, 1793, 1794, 1795, 1796, 1797, 1798, 1799, 1800, 1801, 1802, 1803, 1804, 1805, 1806, 1807, 1808, 1809, 1810, 1811, 1812, 1813, 1814, 1815, 1816, 1817, 1818, 1819, 1820, 1821, 1822, 1823, 1824, 1825, 1826, 1827, 1828, 1829, 1830, 1831, 1832, 1833, 1834, 1835, 1836, 1837, 1838, 1839, 1840, 1841, 1842, 1843, 1844, 1845, 1846, 1847, 1848, 1849, 1850, 1851, 1852, 1853, 1854, 1855, 1856, 1857, 1858, 1859, 1860, 1861, 1862, 1863, 1864, 1865, 1866, 1867, 1868, 1869, 1870, 1871, 1872, 1873, 1874, 1875, 1876, 1877, 1878, 1879, 1880, 1881, 1882, 1883, 1884, 1885, 1886, 1887, 1888, 1889, 1890, 1891, 1892, 1893, 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909, 1910, 1911, 1912, 1913, 1914, 1915, 1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049, 2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2059, 2060, 2061, 2062, 2063, 2064, 2065, 2066, 2067, 2068, 2069, 2070, 2071, 2072, 2073, 2074, 2075, 2076, 2077, 2078, 2079, 2080, 2081, 2082, 2083, 2084, 2085, 2086, 2087, 2088, 2089, 2090, 2091, 2092, 2093, 2094, 2095, 2096, 2097, 2098, 2099, 2100, 2101, 2102, 2103, 2104, 2105, 2106, 2107, 2108, 2109, 2110, 2111, 2112, 2113, 2114, 2115, 2116, 2117, 2118, 2119, 2120, 2121, 2122, 2123, 2124, 2125, 2126, 2127, 2128, 2129, 2130, 2131, 2132, 2133, 2134, 2135, 2136, 2137, 2138, 2139, 2140, 2141, 2142, 2143, 2144, 2145, 2146, 2147, 2148, 2149, 2150, 2151, 2152, 2153, 2154, 2155, 2156, 2157, 2158, 2159, 2160, 2161, 2162, 2163, 2164, 2165, 2166, 2167, 2168, 2169, 2170, 2171, 2172, 2173, 2174, 2175, 2176, 2177, 2178, 2179, 2180, 2181, 2182, 2183, 2184, 2185, 2186, 2187, 2188, 2189, 2190, 2191, 2192, 2193, 2194, 2195, 2196, 2197, 2198, 2199, 2200, 2201, 2202, 2203, 2204, 2205, 2206, 2207, 2208, 2209, 2210, 2211, 2212, 2213, 2214, 2215, 2216, 2217, 2218, 2219, 2220, 2221, 2222, 2223, 2224, 2225, 2226, 2227, 2228, 2229, 2230, 2231, 2232, 2233, 2234, 2235, 2236, 2237, 2238, 2239, 2240, 2241, 2242, 2243, 2244, 2245, 2246, 2247, 2248, 2249, 2250, 2251, 2252, 2253, 2254, 2255, 2256, 2257, 2258, 2259, 2260, 2261, 2262, 2263, 2264, 2265, 2266, 2267, 2268, 2269, 2270, 2271, 2272, 2273, 2274, 2275, 2276, 2277, 2278, 2279, 2280, 2281, 2282, 2283, 2284, 2285, 2286, 2287, 2288, 2289, 2290, 2291, 2292, 2293, 2294, 2295, 2296, 2297, 2298, 2299, 2300, 2301, 2302, 2303, 2304, 2305, 2306, 2307, 2308, 2309, 2310, 2311, 2312, 2313, 2314, 2315, 2316, 2317, 2318, 2319, 2320, 2321, 2322, 2323, 2324, 2325, 2326, 2327, 2328, 2329, 2330, 2331, 2332, 2333, 2334, 2335, 2336, 2337, 2338, 2339, 2340, 2341, 2342, 2343, 2344, 2345, 2346, 2347, 2348, 2349, 2350, 2351, 2352, 2353, 2354, 2355, 2356, 2357, 2358, 2359, 2360, 2361, 2362, 2363, 2364, 2365, 2366, 2367, 2368, 2369, 2370, 2371, 2372, 2373, 2374, 2375, 2376, 2377, 2378, 2379, 2380, 2381, 2382, 2383, 2384, 2385, 2386, 2387, 2388, 2389, 2390, 2391, 2392, 2393, 2394, 2395, 2396, 2397, 2398, 2399, 2400, 2401, 2402, 2403, 2404, 2405, 2406, 2407, 2408, 2409, 2410, 2411, 2412, 2413, 2414, 2415, 2416, 2417, 2418, 2419, 2420, 2421, 2422, 2423, 2424, 2425, 2426, 2427, 2428, 2429, 2430, 2431, 2432, 2433, 2434, 2435, 2436, 2437, 2438, 2439, 2440, 2441, 2442, 2443, 2444, 2445, 2446, 2447, 2448, 2449, 2450, 2451, 2452, 2453, 2454, 2455, 2456, 2457, 2458, 2459, 2460, 2461, 2462, 2463, 2464, 2465, 2466, 2467, 2468, 2469, 2470, 2471, 2472, 2473, 2474, 2475, 2476, 2477, 2478, 2479, 2480, 2481, 2482, 2483, 2484, 2485, 2486, 2487, 2488, 2489, 2490, 2491, 2492, 2493, 2494, 2495, 2496, 2497, 2498, 2499, 2500, 2501, 2502, 2503, 2504, 2505, 2506, 2507, 2508, 2509, 2510, 2511, 2512, 2513, 2514, 2515, 2516, 2517, 2518, 2519, 2520, 2521, 2522, 2523, 2524, 2525, 2526, 2527, 2528, 2529, 2530, 2531, 2532, 2533, 2534, 2535, 2536, 2537, 2538, 2539, 2540, 2541, 2542, 2543, 2544, 2545, 2546, 2547, 2548, 2549, 2550, 2551, 2552, 2553, 2554, 2555, 2556, 2557, 2558, 2559, 2560, 2561, 2562, 2563, 2564, 2565, 2566, 2567, 2568, 2569, 2570, 2571, 2572, 2573, 2574, 2575, 2576, 2577, 2578, 2579, 2580, 2581, 2582, 2583, 2584, 2585, 2586, 2587, 2588, 2589, 2590, 2591, 2592, 2593, 2594, 2595, 2596, 2597, 2598, 2599, 2600, 2601, 2602, 2603, 2604, 2605, 2606, 2607, 2608, 2609, 2610, 2611, 2612, 2613, 2614, 2615, 2616, 2617, 2618, 2619, 2620, 2621, 2622, 2623, 2624, 2625, 2626, 2627, 2628, 2629, 2630, 2631, 2632, 2633, 2634, 2635, 2636, 2637, 2638, 2639, 2640, 2641, 2642, 2643, 2644, 2645, 2646, 2647, 2648, 2649, 2650, 2651, 2652, 2653, 2654, 2655, 2656, 2657, 2658, 2659, 2660, 2661, 2662, 2663, 2664, 2665, 2666, 2667, 2668, 2669, 2670, 2671, 2672, 2673, 2674, 2675, 2676, 2677, 2678, 2679, 2680, 2681, 2682, 2683, 2684, 2685, 2686, 2687, 2688, 2689, 2690, 2691, 2692, 2693, 2694, 2695, 2696, 2697, 2698, 2699, 2700, 2701, 2702, 2703, 2704, 2705, 2706, 2707, 2708, 2709, 2710, 2711, 2712, 2713, 2714, 2715, 2716, 2717, 2718, 2719, 2720, 2721, 2722, 2723, 2724, 2725, 2726, 2727, 2728, 2729, 2730, 2731, 2732, 2733, 2734, 2735, 2736, 2737, 2738, 2739, 2740, 2741, 2742, 2743, 2744, 2745, 2746, 2747, 2748, 2749, 2750, 2751, 2752, 2753, 2754, 2755, 2756, 2757, 2758, 2759, 2760, 2761, 2762, 2763, 2764, 2765, 2766, 2767, 2768, 2769, 2770, 2771, 2772, 2773, 2774, 2775, 2776, 2777, 2778, 2779, 2780, 2781, 2782, 2783, 2784, 2785, 2786, 2787, 2788, 2789, 2790, 2791, 2792, 2793, 2794, 2795, 2796, 2797, 2798, 2799, 2800, 2801, 2802, 2803, 2804, 2805, 2806, 2807, 2808, 2809, 2810, 2811, 2812, 2813, 2814, 2815, 2816, 2817, 2818, 2819, 2820, 2821, 2822, 2823, 2824, 2825, 2826, 2827, 2828, 2829, 2830, 2831, 2832, 2833, 2834, 2835, 2836, 2837, 2838, 2839, 2840, 2841, 2842, 2843, 2844, 2845, 2846, 2847, 2848, 2849, 2850, 2851, 2852, 2853, 2854, 2855, 2856, 2857, 2858, 2859, 2860, 2861, 2862, 2863, 2864, 2865, 2866, 2867, 2868, 2869, 2870, 2871, 2872, 2873, 2874, 2875, 2876, 2877, 2878, 2879, 2880, 2881, 2882, 2883, 2884, 2885, 2886, 2887, 2888, 2889, 2890, 2891, 2892, 2893, 2894, 2895, 2896, 2897, 2898, 2899, 2900, 2901, 2902, 2903, 2904, 2905, 2906, 2907, 2908, 2909, 2910, 2911, 2912, 2913, 2914, 2915, 2916, 2917, 2918, 2919, 2920, 2921, 2922, 2923, 2924, 2925, 2926, 2927, 2928, 2929, 2930, 2931, 2932, 2933, 2934, 2935, 2936, 2937, 2938, 2939, 2940, 2941, 2942, 2943, 2944, 2945, 2946, 2947, 2948, 2949, 2950, 2951, 2952, 2953, 2954, 2955, 2956, 2957, 2958, 2959, 2960, 2961, 2962, 2963, 2964, 2965, 2966, 2967, 2968, 2969, 2970, 2971, 2972, 2973, 2974, 2975, 2976, 2977, 2978, 2979, 2980, 2981, 2982, 2983, 2984, 2985, 2986, 2987, 2988, 2989, 2990, 2991, 2992, 2993, 2994, 2995, 2996, 2997, 2998, 2999, 3000, 3001, 3002, 3003, 3004, 3005, 3006, 3007, 3008, 3009, 3010, 3011, 3012, 3013, 3014, 3015, 3016, 3017, 3018, 3019, 3020, 3021, 3022, 3023, 3024, 3025, 3026, 3027, 3028, 3029, 3030, 3031, 3032, 3033, 3034, 3035, 3036, 3037, 3038, 3039, 3040, 3041, 3042, 3043, 3044, 3045, 3046, 3047, 3048, 3049, 3050, 3051, 3052, 3053, 3054, 3055, 3056, 3057, 3058, 3059, 3060, 3061, 3062, 3063, 3064, 3065, 3066, 3067, 3068, 3069, 3070, 3071, 3072, 3073, 3074, 3075, 3076, 3077, 3078, 3079, 3080, 3081, 3082, 3083, 3084, 3085, 3086, 3087, 3088, 3089, 3090, 3091, 3092, 3093, 3094, 3095, 3096, 3097, 3098, 3099, 3100, 3101, 3102, 3103, 3104, 3105, 3106, 3107, 3108, 3109, 3110, 3111, 3112, 3113, 3114, 3115, 3116, 3117, 3118, 3119, 3120, 3121, 3122, 3123, 3124, 3125, 3126, 3127, 3128, 3129, 3130, 3131, 3132, 3133, 3134, 3135, 3136, 3137, 3138, 3139, 3140, 3141, 3142, 3143, 3144, 3145, 3146, 3147, 3148, 3149, 3150, 3151, 3152, 3153, 3154, 3155, 3156, 3157, 3158, 3159, 3160, 3161, 3162, 3163, 3164, 3165, 3166, 3167, 3168, 3169, 3170, 3171, 3172, 3173, 3174, 3175, 3176, 3177, 3178, 3179, 3180, 3181, 3182, 3183, 3184, 3185, 3186, 3187, 3188, 3189, 3190, 3191, 3192, 3193, 3194, 3195, 3196, 3197, 3198, 3199, 3200, 3201, 3202, 3203, 3204, 3205, 3206, 3207, 3208, 3209, 3210, 3211, 3212, 3213, 3214, 3215, 3216, 3217, 3218, 3219, 3220, 3221, 3222, 3223, 3224, 3225, 3226, 3227, 3228, 3229, 3230, 3231, 3232, 3233, 3234, 3235, 3236, 3237, 3238, 3239, 3240, 3241, 3242, 3243, 3244, 3245, 3246, 3247, 3248, 3249, 3250, 3251, 3252, 3253, 3254, 3255, 3256, 3257, 3258, 3259, 3260, 3261, 3262, 3263, 3264, 3265, 3266, 3267, 3268, 3269, 3270, 3271, 3272, 3273, 3274, 3275, 3276, 3277, 3278, 3279, 3280, 3281, 3282, 3283, 3284, 3285, 3286, 3287, 3288, 3289, 3290, 3291, 3292, 3293, 3294, 3295, 3296, 3297, 3298, 3299, 3300, 3301, 3302, 3303, 3304, 3305, 3306, 3307, 3308, 3309, 3310, 3311, 3312, 3313, 3314, 3315, 3316, 3317, 3318, 3319, 3320, 3321, 3322, 3323, 3324, 3325, 3326, 3327, 3328, 3329, 3330, 3331, 3332, 3333, 3334, 3335, 3336, 3337, 3338, 3339, 3340, 3341, 3342, 3343, 3344, 3345, 3346, 3347, 3348, 3349, 3350, 3351, 3352, 3353, 3354, 3355, 3356, 3357, 3358, 3359, 3360, 3361, 3362, 3363, 3364, 3365, 3366, 3367, 3368, 3369, 3370, 3371, 3372, 3373, 3374, 3375, 3376, 3377, 3378, 3379, 3380, 3381, 3382, 3383, 3384, 3385, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396, 3397, 3398, 3399, 3400, 3401, 3402, 3403, 3404, 3405, 3406, 3407, 3408, 3409, 3410, 3411, 3412, 3413, 3414, 3415, 3416, 3417, 3418, 3419, 3420, 3421, 3422, 3423, 3424, 3425, 3426, 3427, 3428, 3429, 3430, 3431, 3432, 3433, 3434, 3435, 3436, 3437, 3438, 3439, 3440, 3441, 3442, 3443, 3444, 3445, 3446, 3447, 3448, 3449, 3450, 3451, 3452, 3453, 3454, 3455, 3456, 3457, 3458, 3459, 3460, 3461, 3462, 3463, 3464, 3465, 3466, 3467, 3468, 3469, 3470, 3471, 3472, 3473, 3474, 3475, 3476, 3477, 3478, 3479, 3480, 3481, 3482, 3483, 3484, 3485, 3486, 3487, 3488, 3489, 3490, 3491, 3492, 3493, 3494, 3495, 3496, 3497, 3498, 3499, 3500, 3501, 3502, 3503, 3504, 3505, 3506, 3507, 3508, 3509, 3510, 3511, 3512, 3513, 3514, 3515, 3516, 3517, 3518, 3519, 3520, 3521, 3522, 3523, 3524, 3525, 3526, 3527, 3528, 3529, 3530, 3531, 3532, 3533, 3534, 3535, 3536, 3537, 3538, 3539, 3540, 3541, 3542, 3543, 3544, 3545, 3546, 3547, 3548, 3549, 3550, 3551, 3552, 3553, 3554, 3555, 3556, 3557, 3558, 3559, 3560, 3561, 3562, 3563, 3564, 3565, 3566, 3567, 3568, 3569, 3570, 3571, 3572, 3573, 3574, 3575, 3576, 3577, 3578, 3579, 3580, 3581, 3582, 3583, 3584, 3585, 3586, 3587, 3588, 3589, 3590, 3591, 3592, 3593, 3594, 3595, 3596, 3597, 3598, 3599, 3600, 3601, 3602, 3603, 3604, 3605, 3606, 3607, 3608, 3609, 3610, 3611, 3612, 3613, 3614, 3615, 3616, 3617, 3618, 3619, 3620, 3621, 3622, 3623, 3624, 3625, 3626, 3627, 3628, 3629, 3630, 3631, 3632, 3633, 3634, 3635, 3636, 3637, 3638, 3639, 3640, 3641, 3642, 3643, 3644, 3645, 3646, 3647, 3648, 3649, 3650, 3651, 3652, 3653, 3654, 3655, 3656, 3657, 3658, 3659, 3660, 3661, 3662, 3663, 3664, 3665, 3666, 3667, 3668, 3669, 3670, 3671, 3672, 3673, 3674, 3675, 3676, 3677, 3678, 3679, 3680, 3681, 3682, 3683, 3684, 3685, 3686, 3687, 3688, 3689, 3690, 3691, 3692, 3693, 3694, 3695, 3696, 3697, 3698, 3699, 3700, 3701, 3702, 3703, 3704, 3705, 3706, 3707, 3708, 3709, 3710, 3711, 3712, 3713, 3714, 3715, 3716, 3717, 3718, 3719, 3720, 3721, 3722, 3723, 3724, 3725, 3726, 3727, 3728, 3729, 3730, 3731, 3732, 3733, 3734, 3735, 3736, 3737, 3738, 3739, 3740, 3741, 3742, 3743, 3744, 3745, 3746, 3747, 3748, 3749, 3750, 3751, 3752, 3753, 3754, 3755, 3756, 3757, 3758, 3759, 3760, 3761, 3762, 3763, 3764, 3765, 3766, 3767, 3768, 3769, 3770, 3771, 3772, 3773, 3774, 3775, 3776, 3777, 3778, 3779, 3780, 3781, 3782, 3783, 3784, 3785, 3786, 3787, 3788, 3789, 3790, 3791, 3792, 3793, 3794, 3795, 3796, 3797, 3798, 3799, 3800, 3801, 3802, 3803, 3804, 3805, 3806, 3807, 3808, 3809, 3810, 3811, 3812, 3813, 3814, 3815, 3816, 3817, 3818, 3819, 3820, 3821, 3822, 3823, 3824, 3825, 3826, 3827, 3828, 3829, 3830, 3831, 3832, 3833, 3834, 3835, 3836, 3837, 3838, 3839, 3840, 3841, 3842, 3843, 3844, 3845, 3846, 3847, 3848, 3849, 3850, 3851, 3852, 3853, 3854, 3855, 3856, 3857, 3858, 3859, 3860, 3861, 3862, 3863, 3864, 3865, 3866, 3867, 3868, 3869, 3870, 3871, 3872, 3873, 3874, 3875, 3876, 3877, 3878, 3879, 3880, 3881, 3882, 3883, 3884, 3885, 3886, 3887, 3888, 3889, 3890, 3891, 3892, 3893, 3894, 3895, 3896, 3897, 3898, 3899, 3900, 3901, 3902, 3903, 3904, 3905, 3906, 3907, 3908, 3909, 3910, 3911, 3912, 3913, 3914, 3915, 3916, 3917, 3918, 3919, 3920, 3921, 3922, 3923, 3924, 3925, 3926, 3927, 3928, 3929, 3930, 3931, 3932, 3933, 3934, 3935, 3936, 3937, 3938, 3939, 3940, 3941, 3942, 3943, 3944, 3945, 3946, 3947, 3948, 3949, 3950, 3951, 3952, 3953, 3954, 3955, 3956, 3957, 3958, 3959, 3960, 3961, 3962, 3963, 3964, 3965, 3966, 3967, 3968, 3969, 3970, 3971, 3972, 3973, 3974, 3975, 3976, 3977, 3978, 3979, 3980, 3981, 3982, 3983, 3984, 3985, 3986, 3987, 3988, 3989, 3990, 3991, 3992, 3993, 3994, 3995, 3996, 3997, 3998, 3999, 4000, 4001, 4002, 4003, 4004, 4005, 4006, 4007, 4008, 4009, 4010, 4011, 4012, 4013, 4014, 4015, 4016, 4017, 4018, 4019, 4020, 4021, 4022, 4023, 4024, 4025, 4026, 4027, 4028, 4029, 4030, 4031, 4032, 4033, 4034, 4035, 4036, 4037, 4038, 4039, 4040, 4041, 4042, 4043, 4044, 4045, 4046, 4047, 4048, 4049, 4050, 4051, 4052, 4053, 4054, 4055, 4056, 4057, 4058, 4059, 4060, 4061, 4062, 4063, 4064, 4065, 4066, 4067, 4068, 4069, 4070, 4071, 4072, 4073, 4074, 4075, 4076, 4077, 4078, 4079, 4080, 4081, 4082, 4083, 4084, 4085, 4086, 4087, 4088, 4089, 4090, 4091, 4092, 4093, 4094, 4095, 4096, 4097, 4098, 4099, 4100, 4101, 4102, 4103, 4104, 4105, 4106, 4107, 4108, 4109, 4110, 4111, 4112, 4113, 4114, 4115, 4116, 4117, 4118, 4119, 4120, 4121, 4122, 4123, 4124, 4125, 4126, 4127, 4128, 4129, 4130, 4131, 4132, 4133, 4134, 4135, 4136, 4137, 4138, 4139, 4140, 4141, 4142, 4143, 4144, 4145, 4146, 4147, 4148, 4149, 4150, 4151, 4152, 4153, 4154, 4155, 4156, 4157, 4158, 4159, 4160, 4161, 4162, 4163, 4164, 4165, 4166, 4167, 4168, 4169, 4170, 4171, 4172, 4173, 4174, 4175, 4176, 4177, 4178, 4179, 4180, 4181, 4182, 4183, 4184, 4185, 4186, 4187, 4188, 4189, 4190, 4191, 4192, 4193, 4194, 4195, 4196, 4197, 4198, 4199, 4200, 4201, 4202, 4203, 4204, 4205, 4206, 4207, 4208, 4209, 4210, 4211, 4212, 4213, 4214, 4215, 4216, 4217, 4218, 4219, 4220, 4221, 4222, 4223, 4224, 4225, 4226, 4227, 4228, 4229, 4230, 4231, 4232, 4233, 4234, 4235, 4236, 4237, 4238, 4239, 4240, 4241, 4242, 4243, 4244, 4245, 4246, 4247, 4248, 4249, 4250, 4251, 4252, 4253, 4254, 4255, 4256, 4257, 4258, 4259, 4260, 4261, 4262, 4263, 4264, 4265, 4266, 4267, 4268, 4269, 4270, 4271, 4272, 4273, 4274, 4275, 4276, 4277, 4278, 4279, 4280, 4281, 4282, 4283, 4284, 4285, 4286, 4287, 4288, 4289, 4290, 4291, 4292, 4293, 4294, 4295, 4296, 4297, 4298, 4299, 4300, 4301, 4302, 4303, 4304, 4305, 4306, 4307, 4308, 4309, 4310, 4311, 4312, 4313, 4314, 4315, 4316, 4317, 4318, 4319, 4320, 4321, 4322, 4323, 4324, 4325, 4326, 4327, 4328, 4329, 4330, 4331, 4332, 4333, 4334, 4335, 4336, 4337, 4338, 4339, 4340, 4341, 4342, 4343, 4344, 4345, 4346, 4347, 4348, 4349, 4350, 4351, 4352, 4353, 4354, 4355, 4356, 4357, 4358, 4359, 4360, 4361, 4362, 4363, 4364, 4365, 4366, 4367, 4368, 4369, 4370, 4371, 4372, 4373, 4374, 4375, 4376, 4377, 4378, 4379, 4380, 4381, 4382, 4383, 4384, 4385, 4386, 4387, 4388, 4389, 4390, 4391, 4392, 4393, 4394, 4395, 4396, 4397, 4398, 4399, 4400, 4401, 4402, 4403, 4404, 4405, 4406, 4407, 4408, 4409, 4410, 4411, 4412, 4413, 4414, 4415, 4416, 4417, 4418, 4419, 4420, 4421, 4422, 4423, 4424, 4425, 4426, 4427, 4428, 4429, 4430, 4431, 4432, 4433, 4434, 4435, 4436, 4437, 4438, 4439, 4440, 4441, 4442, 4443, 4444, 4445, 4446, 4447, 4448, 4449, 4450, 4451, 4452, 4453, 4454, 4455, 4456, 4457, 4458, 4459, 4460, 4461, 4462, 4463, 4464, 4465, 4466, 4467, 4468, 4469, 4470, 4471, 4472, 4473, 4474, 4475, 4476, 4477, 4478, 4479, 4480, 4481, 4482, 4483, 4484, 4485, 4486, 4487, 4488, 4489, 4490, 4491, 4492, 4493, 4494, 4495, 4496, 4497, 4498, 4499, 4500, 4501, 4502, 4503, 4504, 4505, 4506, 4507, 4508, 4509, 4510, 4511, 4512, 4513, 4514, 4515, 4516, 4517, 4518, 4519, 4520, 4521, 4522, 4523, 4524, 4525, 4526, 4527, 4528, 4529, 4530, 4531, 4532, 4533, 4534, 4535, 4536, 4537, 4538, 4539, 4540, 4541, 4542, 4543, 4544, 4545, 4546, 4547, 4548, 4549, 4550, 4551, 4552, 4553, 4554, 4555, 4556, 4557, 4558, 4559, 4560, 4561, 4562, 4563, 4564, 4565, 4566, 4567, 4568, 4569, 4570, 4571, 4572, 4573, 4574, 4575, 4576, 4577, 4578, 4579, 4580, 4581, 4582, 4583, 4584, 4585, 4586, 4587, 4588, 4589, 4590, 4591, 4592, 4593, 4594, 4595, 4596, 4597, 4598, 4599, 4600, 4601, 4602, 4603, 4604, 4605, 4606, 4607, 4608, 4609, 4610, 4611, 4612, 4613, 4614, 4615, 4616, 4617, 4618, 4619, 4620, 4621, 4622, 4623, 4624, 4625, 4626, 4627, 4628, 4629, 4630, 4631, 4632, 4633, 4634, 4635, 4636, 4637, 4638, 4639, 4640, 4641, 4642, 4643, 4644, 4645, 4646, 4647, 4648, 4649, 4650, 4651, 4652, 4653, 4654, 4655, 4656, 4657, 4658, 4659, 4660, 4661, 4662, 4663, 4664, 4665, 4666, 4667, 4668, 4669, 4670, 4671, 4672, 4673, 4674, 4675, 4676, 4677, 4678, 4679, 4680, 4681, 4682, 4683, 4684, 4685, 4686, 4687, 4688, 4689, 4690, 4691, 4692, 4693, 4694, 4695, 4696, 4697, 4698, 4699, 4700, 4701, 4702, 4703, 4704, 4705, 4706, 4707, 4708, 4709, 4710, 4711, 4712, 4713, 4714, 4715, 4716, 4717, 4718, 4719, 4720, 4721, 4722, 4723, 4724, 4725, 4726, 4727, 4728, 4729, 4730, 4731, 4732, 4733, 4734, 4735, 4736, 4737, 4738, 4739, 4740, 4741, 4742, 4743, 4744, 4745, 4746, 4747, 4748, 4749, 4750, 4751, 4752, 4753, 4754, 4755, 4756, 4757, 4758, 4759, 4760, 4761, 4762, 4763, 4764, 4765, 4766, 4767, 4768, 4769, 4770, 4771, 4772, 4773, 4774, 4775, 4776, 4777, 4778, 4779, 4780, 4781, 4782, 4783, 4784, 4785, 4786, 4787, 4788, 4789, 4790, 4791, 4792, 4793, 4794, 4795, 4796, 4797, 4798, 4799, 4800, 4801, 4802, 4803, 4804, 4805, 4806, 4807, 4808, 4809, 4810, 4811, 4812, 4813, 4814, 4815, 4816, 4817, 4818, 4819, 4820, 4821, 4822, 4823, 4824, 4825, 4826, 4827, 4828, 4829, 4830, 4831, 4832, 4833, 4834, 4835, 4836, 4837, 4838, 4839, 4840, 4841, 4842, 4843, 4844, 4845, 4846, 4847, 4848, 4849, 4850, 4851, 4852, 4853, 4854, 4855, 4856, 4857, 4858, 4859, 4860, 4861, 4862, 4863, 4864, 4865, 4866, 4867, 4868, 4869, 4870, 4871, 4872, 4873, 4874, 4875, 4876, 4877, 4878, 4879, 4880, 4881, 4882, 4883, 4884, 4885, 4886, 4887, 4888, 4889, 4890, 4891, 4892, 4893, 4894, 4895, 4896, 4897, 4898, 4899, 4900, 4901, 4902, 4903, 4904, 4905, 4906, 4907, 4908, 4909, 4910, 4911, 4912, 4913, 4914, 4915, 4916, 4917, 4918, 4919, 4920, 4921, 4922, 4923, 4924, 4925, 4926, 4927, 4928, 4929, 4930, 4931, 4932, 4933, 4934, 4935, 4936, 4937, 4938, 4939, 4940, 4941, 4942, 4943, 4944, 4945, 4946, 4947, 4948, 4949, 4950, 4951, 4952, 4953, 4954, 4955, 4956, 4957, 4958, 4959, 4960, 4961, 4962, 4963, 4964, 4965, 4966, 4967, 4968, 4969, 4970, 4971, 4972, 4973, 4974, 4975, 4976, 4977, 4978, 4979, 4980, 4981, 4982, 4983, 4984, 4985, 4986, 4987, 4988, 4989, 4990, 4991, 4992, 4993, 4994, 4995, 4996, 4997, 4998, 4999, 5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007, 5008, 5009, 5010, 5011, 5012, 5013, 5014, 5015, 5016, 5017, 5018, 5019, 5020, 5021, 5022, 5023, 5024, 5025, 5026, 5027, 5028, 5029, 5030, 5031, 5032, 5033, 5034, 5035, 5036, 5037, 5038, 5039, 5040, 5041, 5042, 5043, 5044, 5045, 5046, 5047, 5048, 5049, 5050, 5051, 5052, 5053, 5054, 5055, 5056, 5057, 5058, 5059, 5060, 5061, 5062, 5063, 5064, 5065, 5066, 5067, 5068, 5069, 5070, 5071, 5072, 5073, 5074, 5075, 5076, 5077, 5078, 5079, 5080, 5081, 5082, 5083, 5084, 5085, 5086, 5087, 5088, 5089, 5090, 5091, 5092, 5093, 5094, 5095, 5096, 5097, 5098, 5099, 5100, 5101, 5102, 5103, 5104, 5105, 5106, 5107, 5108, 5109, 5110, 5111, 5112, 5113, 5114, 5115, 5116, 5117, 5118, 5119, 5120, 5121, 5122, 5123, 5124, 5125, 5126, 5127, 5128, 5129, 5130, 5131, 5132, 5133, 5134, 5135, 5136, 5137, 5138, 5139, 5140, 5141, 5142, 5143, 5144, 5145, 5146, 5147, 5148, 5149, 5150, 5151, 5152, 5153, 5154, 5155, 5156, 5157, 5158, 5159, 5160, 5161, 5162, 5163, 5164, 5165, 5166, 5167, 5168, 5169, 5170, 5171, 5172, 5173, 5174, 5175, 5176, 5177, 5178, 5179, 5180, 5181, 5182, 5183, 5184, 5185, 5186, 5187, 5188, 5189, 5190, 5191, 5192, 5193, 5194, 5195, 5196, 5197, 5198, 5199, 5200, 5201, 5202, 5203, 5204, 5205, 5206, 5207, 5208, 5209, 5210, 5211, 5212, 5213, 5214, 5215, 5216, 5217, 5218, 5219, 5220, 5221, 5222, 5223, 5224, 5225, 5226, 5227, 5228, 5229, 5230, 5231, 5232, 5233, 5234, 5235, 5236, 5237, 5238, 5239, 5240, 5241, 5242, 5243, 5244, 5245, 5246, 5247, 5248, 5249, 5250, 5251, 5252, 5253, 5254, 5255, 5256, 5257, 5258, 5259, 5260, 5261, 5262, 5263, 5264, 5265, 5266, 5267, 5268, 5269, 5270, 5271, 5272, 5273, 5274, 5275, 5276, 5277, 5278, 5279, 5280, 5281, 5282, 5283, 5284, 5285, 5286, 5287, 5288, 5289, 5290, 5291, 5292, 5293, 5294, 5295, 5296, 5297, 5298, 5299, 5300, 5301, 5302, 5303, 5304, 5305, 5306, 5307, 5308, 5309, 5310, 5311, 5312, 5313, 5314, 5315, 5316, 5317, 5318, 5319, 5320, 5321, 5322, 5323, 5324, 5325, 5326, 5327, 5328, 5329, 5330, 5331, 5332, 5333, 5334, 5335, 5336, 5337, 5338, 5339, 5340, 5341, 5342, 5343, 5344, 5345, 5346, 5347, 5348, 5349, 5350, 5351, 5352, 5353, 5354, 5355, 5356, 5357, 5358, 5359, 5360, 5361, 5362, 5363, 5364, 5365, 5366, 5367, 5368, 5369, 5370, 5371, 5372, 5373, 5374, 5375, 5376, 5377, 5378, 5379, 5380, 5381, 5382, 5383, 5384, 5385, 5386, 5387, 5388, 5389, 5390, 5391, 5392, 5393, 5394, 5395, 5396, 5397, 5398, 5399, 5400, 5401, 5402, 5403, 5404, 5405, 5406, 5407, 5408, 5409, 5410, 5411, 5412, 5413, 5414, 5415, 5416, 5417, 5418, 5419, 5420, 5421, 5422, 5423, 5424, 5425, 5426, 5427, 5428, 5429, 5430, 5431, 5432, 5433, 5434, 5435, 5436, 5437, 5438, 5439, 5440, 5441, 5442, 5443, 5444, 5445, 5446, 5447, 5448, 5449, 5450, 5451, 5452, 5453, 5454, 5455, 5456, 5457, 5458, 5459, 5460, 5461, 5462, 5463, 5464, 5465, 5466, 5467, 5468, 5469, 5470, 5471, 5472, 5473, 5474, 5475, 5476, 5477, 5478, 5479, 5480, 5481, 5482, 5483, 5484, 5485, 5486, 5487, 5488, 5489, 5490, 5491, 5492, 5493, 5494, 5495, 5496, 5497, 5498, 5499, 5500, 5501, 5502, 5503, 5504, 5505, 5506, 5507, 5508, 5509, 5510, 5511, 5512, 5513, 5514, 5515, 5516, 5517, 5518, 5519, 5520, 5521, 5522, 5523, 5524, 5525, 5526, 5527, 5528, 5529, 5530, 5531, 5532, 5533, 5534, 5535, 5536, 5537, 5538, 5539, 5540, 5541, 5542, 5543, 5544, 5545, 5546, 5547, 5548, 5549, 5550, 5551, 5552, 5553, 5554, 5555, 5556, 5557, 5558, 5559, 5560, 5561, 5562, 5563, 5564, 5565, 5566, 5567, 5568, 5569, 5570, 5571, 5572, 5573, 5574, 5575, 5576, 5577, 5578, 5579, 5580, 5581, 5582, 5583, 5584, 5585, 5586, 5587, 5588, 5589, 5590, 5591, 5592, 5593, 5594, 5595, 5596, 5597, 5598, 5599, 5600, 5601, 5602, 5603, 5604, 5605, 5606, 5607, 5608, 5609, 5610, 5611, 5612, 5613, 5614, 5615, 5616, 5617, 5618, 5619, 5620, 5621, 5622, 5623, 5624, 5625, 5626, 5627, 5628, 5629, 5630, 5631, 5632, 5633, 5634, 5635, 5636, 5637, 5638, 5639, 5640, 5641, 5642, 5643, 5644, 5645, 5646, 5647, 5648, 5649, 5650, 5651, 5652, 5653, 5654, 5655, 5656, 5657, 5658, 5659, 5660, 5661, 5662, 5663, 5664, 5665, 5666, 5667, 5668, 5669, 5670, 5671, 5672, 5673, 5674, 5675, 5676, 5677, 5678, 5679, 5680, 5681, 5682, 5683, 5684, 5685, 5686, 5687, 5688, 5689, 5690, 5691, 5692, 5693, 5694, 5695, 5696, 5697, 5698, 5699, 5700, 5701, 5702, 5703, 5704, 5705, 5706, 5707, 5708, 5709, 5710, 5711, 5712, 5713, 5714, 5715, 5716, 5717, 5718, 5719, 5720, 5721, 5722, 5723, 5724, 5725, 5726, 5727, 5728, 5729, 5730, 5731, 5732, 5733, 5734, 5735, 5736, 5737, 5738, 5739, 5740, 5741, 5742, 5743, 5744, 5745, 5746, 5747, 5748, 5749, 5750, 5751, 5752, 5753, 5754, 5755, 5756, 5757, 5758, 5759, 5760, 5761, 5762, 5763, 5764, 5765, 5766, 5767, 5768, 5769, 5770, 5771, 5772, 5773, 5774, 5775, 5776, 5777, 5778, 5779, 5780, 5781, 5782, 5783, 5784, 5785, 5786, 5787, 5788, 5789, 5790, 5791, 5792, 5793, 5794, 5795, 5796, 5797, 5798, 5799, 5800, 5801, 5802, 5803, 5804, 5805, 5806, 5807, 5808, 5809, 5810, 5811, 5812, 5813, 5814, 5815, 5816, 5817, 5818, 5819, 5820, 5821, 5822, 5823, 5824, 5825, 5826, 5827, 5828, 5829, 5830, 5831, 5832, 5833, 5834, 5835, 5836, 5837, 5838, 5839, 5840, 5841, 5842, 5843, 5844, 5845, 5846, 5847, 5848, 5849, 5850, 5851, 5852, 5853, 5854, 5855, 5856, 5857, 5858, 5859, 5860, 5861, 5862, 5863, 5864, 5865, 5866, 5867, 5868, 5869, 5870, 5871, 5872, 5873, 5874, 5875, 5876, 5877, 5878, 5879, 5880, 5881, 5882, 5883, 5884, 5885, 5886, 5887, 5888, 5889, 5890, 5891, 5892, 5893, 5894, 5895, 5896, 5897, 5898, 5899, 5900, 5901, 5902, 5903, 5904, 5905, 5906, 5907, 5908, 5909, 5910, 5911, 5912, 5913, 5914, 5915, 5916, 5917, 5918, 5919, 5920, 5921, 5922, 5923, 5924, 5925, 5926, 5927, 5928, 5929, 5930, 5931, 5932, 5933, 5934, 5935, 5936, 5937, 5938, 5939, 5940, 5941, 5942, 5943, 5944, 5945, 5946, 5947, 5948, 5949, 5950, 5951, 5952, 5953, 5954, 5955, 5956, 5957, 5958, 5959, 5960, 5961, 5962, 5963, 5964, 5965, 5966, 5967, 5968, 5969, 5970, 5971, 5972, 5973, 5974, 5975, 5976, 5977, 5978, 5979, 5980, 5981, 5982, 5983, 5984, 5985, 5986, 5987, 5988, 5989, 5990, 5991, 5992, 5993, 5994, 5995, 5996, 5997, 5998, 5999, 6000, 6001, 6002, 6003, 6004, 6005, 6006, 6007, 6008, 6009, 6010, 6011, 6012, 6013, 6014, 6015, 6016, 6017, 6018, 6019, 6020, 6021, 6022, 6023, 6024, 6025, 6026, 6027, 6028, 6029, 6030, 6031, 6032, 6033, 6034, 6035, 6036, 6037, 6038, 6039, 6040, 6041, 6042, 6043, 6044, 6045, 6046, 6047, 6048, 6049, 6050, 6051, 6052, 6053, 6054, 6055, 6056, 6057, 6058, 6059, 6060, 6061, 6062, 6063, 6064, 6065, 6066, 6067, 6068, 6069, 6070, 6071, 6072, 6073, 6074, 6075, 6076, 6077, 6078, 6079, 6080, 6081, 6082, 6083, 6084, 6085, 6086, 6087, 6088, 6089, 6090, 6091, 6092, 6093, 6094, 6095, 6096, 6097, 6098, 6099, 6100, 6101, 6102, 6103, 6104, 6105, 6106, 6107, 6108, 6109, 6110, 6111, 6112, 6113, 6114, 6115, 6116, 6117, 6118, 6119, 6120, 6121, 6122, 6123, 6124, 6125, 6126, 6127, 6128, 6129, 6130, 6131, 6132, 6133, 6134, 6135, 6136, 6137, 6138, 6139, 6140, 6141, 6142, 6143, 6144, 6145, 6146, 6147, 6148, 6149, 6150, 6151, 6152, 6153, 6154, 6155, 6156, 6157, 6158, 6159, 6160, 6161, 6162, 6163, 6164, 6165, 6166, 6167, 6168, 6169, 6170, 6171, 6172, 6173, 6174, 6175, 6176, 6177, 6178, 6179, 6180, 6181, 6182, 6183, 6184, 6185, 6186, 6187, 6188, 6189, 6190, 6191, 6192, 6193, 6194, 6195, 6196, 6197, 6198, 6199, 6200, 6201, 6202, 6203, 6204, 6205, 6206, 6207, 6208, 6209, 6210, 6211, 6212, 6213, 6214, 6215, 6216, 6217, 6218, 6219, 6220, 6221, 6222, 6223, 6224, 6225, 6226, 6227, 6228, 6229, 6230, 6231, 6232, 6233, 6234, 6235, 6236, 6237, 6238, 6239, 6240, 6241, 6242, 6243, 6244, 6245, 6246, 6247, 6248, 6249, 6250, 6251, 6252, 6253, 6254, 6255, 6256, 6257, 6258, 6259, 6260, 6261, 6262, 6263, 6264, 6265, 6266, 6267, 6268, 6269, 6270, 6271, 6272, 6273, 6274, 6275, 6276, 6277, 6278, 6279, 6280, 6281, 6282, 6283, 6284, 6285, 6286, 6287, 6288, 6289, 6290, 6291, 6292, 6293, 6294, 6295, 6296, 6297, 6298, 6299, 6300, 6301, 6302, 6303, 6304, 6305, 6306, 6307, 6308, 6309, 6310, 6311, 6312, 6313, 6314, 6315, 6316, 6317, 6318, 6319, 6320, 6321, 6322, 6323, 6324, 6325, 6326, 6327, 6328, 6329, 6330, 6331, 6332, 6333, 6334, 6335, 6336, 6337, 6338, 6339, 6340, 6341, 6342, 6343, 6344, 6345, 6346, 6347, 6348, 6349, 6350, 6351, 6352, 6353, 6354, 6355, 6356, 6357, 6358, 6359, 6360, 6361, 6362, 6363, 6364, 6365, 6366, 6367, 6368, 6369, 6370, 6371, 6372, 6373, 6374, 6375, 6376, 6377, 6378, 6379, 6380, 6381, 6382, 6383, 6384, 6385, 6386, 6387, 6388, 6389, 6390, 6391, 6392, 6393, 6394, 6395, 6396, 6397, 6398, 6399, 6400, 6401, 6402, 6403, 6404, 6405, 6406, 6407, 6408, 6409, 6410, 6411, 6412, 6413, 6414, 6415, 6416, 6417, 6418, 6419, 6420, 6421, 6422, 6423, 6424, 6425, 6426, 6427, 6428, 6429, 6430, 6431, 6432, 6433, 6434, 6435, 6436, 6437, 6438, 6439, 6440, 6441, 6442, 6443, 6444, 6445, 6446, 6447, 6448, 6449, 6450, 6451, 6452, 6453, 6454, 6455, 6456, 6457, 6458, 6459, 6460, 6461, 6462, 6463, 6464, 6465, 6466, 6467, 6468, 6469, 6470, 6471, 6472, 6473, 6474, 6475, 6476, 6477, 6478, 6479, 6480, 6481, 6482, 6483, 6484, 6485, 6486, 6487, 6488, 6489, 6490, 6491, 6492, 6493, 6494, 6495, 6496, 6497, 6498, 6499, 6500, 6501, 6502, 6503, 6504, 6505, 6506, 6507, 6508, 6509, 6510, 6511, 6512, 6513, 6514, 6515, 6516, 6517, 6518, 6519, 6520, 6521, 6522, 6523, 6524, 6525, 6526, 6527, 6528, 6529, 6530, 6531, 6532, 6533, 6534, 6535, 6536, 6537, 6538, 6539, 6540, 6541, 6542, 6543, 6544, 6545, 6546, 6547, 6548, 6549, 6550, 6551, 6552, 6553, 6554, 6555, 6556, 6557, 6558, 6559, 6560, 6561, 6562, 6563, 6564, 6565, 6566, 6567, 6568, 6569, 6570, 6571, 6572, 6573, 6574, 6575, 6576, 6577, 6578, 6579, 6580, 6581, 6582, 6583, 6584, 6585, 6586, 6587, 6588, 6589, 6590, 6591, 6592, 6593, 6594, 6595, 6596, 6597, 6598, 6599, 6600, 6601, 6602, 6603, 6604, 6605, 6606, 6607, 6608, 6609, 6610, 6611, 6612, 6613, 6614, 6615, 6616, 6617, 6618, 6619, 6620, 6621, 6622, 6623, 6624, 6625, 6626, 6627, 6628, 6629, 6630, 6631, 6632, 6633, 6634, 6635, 6636, 6637, 6638, 6639, 6640, 6641, 6642, 6643, 6644, 6645, 6646, 6647, 6648, 6649, 6650, 6651, 6652, 6653, 6654, 6655, 6656, 6657, 6658, 6659, 6660, 6661, 6662, 6663, 6664, 6665, 6666, 6667, 6668, 6669, 6670, 6671, 6672, 6673, 6674, 6675, 6676, 6677, 6678, 6679, 6680, 6681, 6682, 6683, 6684, 6685, 6686, 6687, 6688, 6689, 6690, 6691, 6692, 6693, 6694, 6695, 6696, 6697, 6698, 6699, 6700, 6701, 6702, 6703, 6704, 6705, 6706, 6707, 6708, 6709, 6710, 6711, 6712, 6713, 6714, 6715, 6716, 6717, 6718, 6719, 6720, 6721, 6722, 6723, 6724, 6725, 6726, 6727, 6728, 6729, 6730, 6731, 6732, 6733, 6734, 6735, 6736, 6737, 6738, 6739, 6740, 6741, 6742, 6743, 6744, 6745, 6746, 6747, 6748, 6749, 6750, 6751, 6752, 6753, 6754, 6755, 6756, 6757, 6758, 6759, 6760, 6761, 6762, 6763, 6764, 6765, 6766, 6767, 6768, 6769, 6770, 6771, 6772, 6773, 6774, 6775, 6776, 6777, 6778, 6779, 6780, 6781, 6782, 6783, 6784, 6785, 6786, 6787, 6788, 6789, 6790, 6791, 6792, 6793, 6794, 6795, 6796, 6797, 6798, 6799, 6800, 6801, 6802, 6803, 6804, 6805, 6806, 6807, 6808, 6809, 6810, 6811, 6812, 6813, 6814, 6815, 6816, 6817, 6818, 6819, 6820, 6821, 6822, 6823, 6824, 6825, 6826, 6827, 6828, 6829, 6830, 6831, 6832, 6833, 6834, 6835, 6836, 6837, 6838, 6839, 6840, 6841, 6842, 6843, 6844, 6845, 6846, 6847, 6848, 6849, 6850, 6851, 6852, 6853, 6854, 6855, 6856, 6857, 6858, 6859, 6860, 6861, 6862, 6863, 6864, 6865, 6866, 6867, 6868, 6869, 6870, 6871, 6872, 6873, 6874, 6875, 6876, 6877, 6878, 6879, 6880, 6881, 6882, 6883, 6884, 6885, 6886, 6887, 6888, 6889, 6890, 6891, 6892, 6893, 6894, 6895, 6896, 6897, 6898, 6899, 6900, 6901, 6902, 6903, 6904, 6905, 6906, 6907, 6908, 6909, 6910, 6911, 6912, 6913, 6914, 6915, 6916, 6917, 6918, 6919, 6920, 6921, 6922, 6923, 6924, 6925, 6926, 6927, 6928, 6929, 6930, 6931, 6932, 6933, 6934, 6935, 6936, 6937, 6938, 6939, 6940, 6941, 6942, 6943, 6944, 6945, 6946, 6947, 6948, 6949, 6950, 6951, 6952, 6953, 6954, 6955, 6956, 6957, 6958, 6959, 6960, 6961, 6962, 6963, 6964, 6965, 6966, 6967, 6968, 6969, 6970, 6971, 6972, 6973, 6974, 6975, 6976, 6977, 6978, 6979, 6980, 6981, 6982, 6983, 6984, 6985, 6986, 6987, 6988, 6989, 6990, 6991, 6992, 6993, 6994, 6995, 6996, 6997, 6998, 6999, 7000, 7001, 7002, 7003, 7004, 7005, 7006, 7007, 7008, 7009, 7010, 7011, 7012, 7013, 7014, 7015, 7016, 7017, 7018, 7019, 7020, 7021, 7022, 7023, 7024, 7025, 7026, 7027, 7028, 7029, 7030, 7031, 7032, 7033, 7034, 7035, 7036, 7037, 7038, 7039, 7040, 7041, 7042, 7043, 7044, 7045, 7046, 7047, 7048, 7049, 7050, 7051, 7052, 7053, 7054, 7055, 7056, 7057, 7058, 7059, 7060, 7061, 7062, 7063, 7064, 7065, 7066, 7067, 7068, 7069, 7070, 7071, 7072, 7073, 7074, 7075, 7076, 7077, 7078, 7079, 7080, 7081, 7082, 7083, 7084, 7085, 7086, 7087, 7088, 7089, 7090, 7091, 7092, 7093, 7094, 7095, 7096, 7097, 7098, 7099, 7100, 7101, 7102, 7103, 7104, 7105, 7106, 7107, 7108, 7109, 7110, 7111, 7112, 7113, 7114, 7115, 7116, 7117, 7118, 7119, 7120, 7121, 7122, 7123, 7124, 7125, 7126, 7127, 7128, 7129, 7130, 7131, 7132, 7133, 7134, 7135, 7136, 7137, 7138, 7139, 7140, 7141, 7142, 7143, 7144, 7145, 7146, 7147, 7148, 7149, 7150, 7151, 7152, 7153, 7154, 7155, 7156, 7157, 7158, 7159, 7160, 7161, 7162, 7163, 7164, 7165, 7166, 7167, 7168, 7169, 7170, 7171, 7172, 7173, 7174, 7175, 7176, 7177, 7178, 7179, 7180, 7181, 7182, 7183, 7184, 7185, 7186, 7187, 7188, 7189, 7190, 7191, 7192, 7193, 7194, 7195, 7196, 7197, 7198, 7199, 7200, 7201, 7202, 7203, 7204, 7205, 7206, 7207, 7208, 7209, 7210, 7211, 7212, 7213, 7214, 7215, 7216, 7217, 7218, 7219, 7220, 7221, 7222, 7223, 7224, 7225, 7226, 7227, 7228, 7229, 7230, 7231, 7232, 7233, 7234, 7235, 7236, 7237, 7238, 7239, 7240, 7241, 7242, 7243, 7244, 7245, 7246, 7247, 7248, 7249, 7250, 7251, 7252, 7253, 7254, 7255, 7256, 7257, 7258, 7259, 7260, 7261, 7262, 7263, 7264, 7265, 7266, 7267, 7268, 7269, 7270, 7271, 7272, 7273, 7274, 7275, 7276, 7277, 7278, 7279, 7280, 7281, 7282, 7283, 7284, 7285, 7286, 7287, 7288, 7289, 7290, 7291, 7292, 7293, 7294, 7295, 7296, 7297, 7298, 7299, 7300, 7301, 7302, 7303, 7304, 7305, 7306, 7307, 7308, 7309, 7310, 7311, 7312, 7313, 7314, 7315, 7316, 7317, 7318, 7319, 7320, 7321, 7322, 7323, 7324, 7325, 7326, 7327, 7328, 7329, 7330, 7331, 7332, 7333, 7334, 7335, 7336, 7337, 7338, 7339, 7340, 7341, 7342, 7343, 7344, 7345, 7346, 7347, 7348, 7349, 7350, 7351, 7352, 7353, 7354, 7355, 7356, 7357, 7358, 7359, 7360, 7361, 7362, 7363, 7364, 7365, 7366, 7367, 7368, 7369, 7370, 7371, 7372, 7373, 7374, 7375, 7376, 7377, 7378, 7379, 7380, 7381, 7382, 7383, 7384, 7385, 7386, 7387, 7388, 7389, 7390, 7391, 7392, 7393, 7394, 7395, 7396, 7397, 7398, 7399, 7400, 7401, 7402, 7403, 7404, 7405, 7406, 7407, 7408, 7409, 7410, 7411, 7412, 7413, 7414, 7415, 7416, 7417, 7418, 7419, 7420, 7421, 7422, 7423, 7424, 7425, 7426, 7427, 7428, 7429, 7430, 7431, 7432, 7433, 7434, 7435, 7436, 7437, 7438, 7439, 7440, 7441, 7442, 7443, 7444, 7445, 7446, 7447, 7448, 7449, 7450, 7451, 7452, 7453, 7454, 7455, 7456, 7457, 7458, 7459, 7460, 7461, 7462, 7463, 7464, 7465, 7466, 7467, 7468, 7469, 7470, 7471, 7472, 7473, 7474, 7475, 7476, 7477, 7478, 7479, 7480, 7481, 7482, 7483, 7484, 7485, 7486, 7487, 7488, 7489, 7490, 7491, 7492, 7493, 7494, 7495, 7496, 7497, 7498, 7499, 7500, 7501, 7502, 7503, 7504, 7505, 7506, 7507, 7508, 7509, 7510, 7511, 7512, 7513, 7514, 7515, 7516, 7517, 7518, 7519, 7520, 7521, 7522, 7523, 7524, 7525, 7526, 7527, 7528, 7529, 7530, 7531, 7532, 7533, 7534, 7535, 7536, 7537, 7538, 7539, 7540, 7541, 7542, 7543, 7544, 7545, 7546, 7547, 7548, 7549, 7550, 7551, 7552, 7553, 7554, 7555, 7556, 7557, 7558, 7559, 7560, 7561, 7562, 7563, 7564, 7565, 7566, 7567, 7568, 7569, 7570, 7571, 7572, 7573, 7574, 7575, 7576, 7577, 7578, 7579, 7580, 7581, 7582, 7583, 7584, 7585, 7586, 7587, 7588, 7589, 7590, 7591, 7592, 7593, 7594, 7595, 7596, 7597, 7598, 7599, 7600, 7601, 7602, 7603, 7604, 7605, 7606, 7607, 7608, 7609, 7610, 7611, 7612, 7613, 7614, 7615, 7616, 7617, 7618, 7619, 7620, 7621, 7622, 7623, 7624, 7625, 7626, 7627, 7628, 7629, 7630, 7631, 7632, 7633, 7634, 7635, 7636, 7637, 7638, 7639, 7640, 7641, 7642, 7643, 7644, 7645, 7646, 7647, 7648, 7649, 7650, 7651, 7652, 7653, 7654, 7655, 7656, 7657, 7658, 7659, 7660, 7661, 7662, 7663, 7664, 7665, 7666, 7667, 7668, 7669, 7670, 7671, 7672, 7673, 7674, 7675, 7676, 7677, 7678, 7679, 7680, 7681, 7682, 7683, 7684, 7685, 7686, 7687, 7688, 7689, 7690, 7691, 7692, 7693, 7694, 7695, 7696, 7697, 7698, 7699, 7700, 7701, 7702, 7703, 7704, 7705, 7706, 7707, 7708, 7709, 7710, 7711, 7712, 7713, 7714, 7715, 7716, 7717, 7718, 7719, 7720, 7721, 7722, 7723, 7724, 7725, 7726, 7727, 7728, 7729, 7730, 7731, 7732, 7733, 7734, 7735, 7736, 7737, 7738, 7739, 7740, 7741, 7742, 7743, 7744, 7745, 7746, 7747, 7748, 7749, 7750, 7751, 7752, 7753, 7754, 7755, 7756, 7757, 7758, 7759, 7760, 7761, 7762, 7763, 7764, 7765, 7766, 7767, 7768, 7769, 7770, 7771, 7772, 7773, 7774, 7775, 7776, 7777, 7778, 7779, 7780, 7781, 7782, 7783, 7784, 7785, 7786, 7787, 7788, 7789, 7790, 7791, 7792, 7793, 7794, 7795, 7796, 7797, 7798, 7799, 7800, 7801, 7802, 7803, 7804, 7805, 7806, 7807, 7808, 7809, 7810, 7811, 7812, 7813, 7814, 7815, 7816, 7817, 7818, 7819, 7820, 7821, 7822, 7823, 7824, 7825, 7826, 7827, 7828, 7829, 7830, 7831, 7832, 7833, 7834, 7835, 7836, 7837, 7838, 7839, 7840, 7841, 7842, 7843, 7844, 7845, 7846, 7847, 7848, 7849, 7850, 7851, 7852, 7853, 7854, 7855, 7856, 7857, 7858, 7859, 7860, 7861, 7862, 7863, 7864, 7865, 7866, 7867, 7868, 7869, 7870, 7871, 7872, 7873, 7874, 7875, 7876, 7877, 7878, 7879, 7880, 7881, 7882, 7883, 7884, 7885, 7886, 7887, 7888, 7889, 7890, 7891, 7892, 7893, 7894, 7895, 7896, 7897, 7898, 7899, 7900, 7901, 7902, 7903, 7904, 7905, 7906, 7907, 7908, 7909, 7910, 7911, 7912, 7913, 7914, 7915, 7916, 7917, 7918, 7919, 7920, 7921, 7922, 7923, 7924, 7925, 7926, 7927, 7928, 7929, 7930, 7931, 7932, 7933, 7934, 7935, 7936, 7937, 7938, 7939, 7940, 7941, 7942, 7943, 7944, 7945, 7946, 7947, 7948, 7949, 7950, 7951, 7952, 7953, 7954, 7955, 7956, 7957, 7958, 7959, 7960, 7961, 7962, 7963, 7964, 7965, 7966, 7967, 7968, 7969, 7970, 7971, 7972, 7973, 7974, 7975, 7976, 7977, 7978, 7979, 7980, 7981, 7982, 7983, 7984, 7985, 7986, 7987, 7988, 7989, 7990, 7991, 7992, 7993, 7994, 7995, 7996, 7997, 7998, 7999, 8000, 8001, 8002, 8003, 8004, 8005, 8006, 8007, 8008, 8009, 8010, 8011, 8012, 8013, 8014, 8015, 8016, 8017, 8018, 8019, 8020, 8021, 8022, 8023, 8024, 8025, 8026, 8027, 8028, 8029, 8030, 8031, 8032, 8033, 8034, 8035, 8036, 8037, 8038, 8039, 8040, 8041, 8042, 8043, 8044, 8045, 8046, 8047, 8048, 8049, 8050, 8051, 8052, 8053, 8054, 8055, 8056, 8057, 8058, 8059, 8060, 8061, 8062, 8063, 8064, 8065, 8066, 8067, 8068, 8069, 8070, 8071, 8072, 8073, 8074, 8075, 8076, 8077, 8078, 8079, 8080, 8081, 8082, 8083, 8084, 8085, 8086, 8087, 8088, 8089, 8090, 8091, 8092, 8093, 8094, 8095, 8096, 8097, 8098, 8099, 8100, 8101, 8102, 8103, 8104, 8105, 8106, 8107, 8108, 8109, 8110, 8111, 8112, 8113, 8114, 8115, 8116, 8117, 8118, 8119, 8120, 8121, 8122, 8123, 8124, 8125, 8126, 8127, 8128, 8129, 8130, 8131, 8132, 8133, 8134, 8135, 8136, 8137, 8138, 8139, 8140, 8141, 8142, 8143, 8144, 8145, 8146, 8147, 8148, 8149, 8150, 8151, 8152, 8153, 8154, 8155, 8156, 8157, 8158, 8159, 8160, 8161, 8162, 8163, 8164, 8165, 8166, 8167, 8168, 8169, 8170, 8171, 8172, 8173, 8174, 8175, 8176, 8177, 8178, 8179, 8180, 8181, 8182, 8183, 8184, 8185, 8186, 8187, 8188, 8189, 8190, 8191, 8192 };
						static float s_var[8192] = { 1, 5.65685, 15.5885, 32, 55.9017, 88.1816, 129.642, 181.019, 243, 316.228, 401.312, 498.831, 609.338, 733.365, 871.421, 1024, 1191.58, 1374.62, 1573.56, 1788.85, 2020.92, 2270.16, 2536.99, 2821.81, 3125, 3446.94, 3788, 4148.54, 4528.92, 4929.5, 5350.62, 5792.62, 6255.83, 6740.58, 7247.2, 7776, 8327.3, 8901.41, 9498.64, 10119.3, 10763.7, 11432, 12124.7, 12842, 13584.1, 14351.4, 15144.1, 15962.6, 16807, 17677.7, 18574.9, 19498.8, 20449.8, 21428.1, 22434, 23467.7, 24529.4, 25619.5, 26738.1, 27885.5, 29061.9, 30267.7, 31503, 32768, 34063, 35388.3, 36744, 38130.5, 39547.8, 40996.3, 42476.2, 43987.7, 45531, 47106.3, 48713.9, 50354, 52026.8, 53732.4, 55471.2, 57243.3, 59049, 60888.4, 62761.8, 64669.3, 66611.2, 68587.7, 70598.9, 72645.2, 74726.6, 76843.3, 78995.7, 81183.8, 83407.9, 85668.2, 87964.8, 90298, 92667.9, 95074.7, 97518.7, 100000, 102519, 105075, 107670, 110302, 112973, 115682, 118429, 121216, 124041, 126906, 129810, 132753, 135736, 138759, 141822, 144926, 148069, 151253, 154478, 157744, 161051, 164399, 167789, 171220, 174693, 178208, 181765, 185364, 189005, 192690, 196417, 200187, 203999, 207856, 211755, 215699, 219686, 223716, 227791, 231910, 236074, 240282, 244534, 248832, 253175, 257562, 261995, 266474, 270998, 275568, 280183, 284845, 289553, 294308, 299109, 303957, 308851, 313793, 318781, 323817, 328901, 334032, 339210, 344437, 349711, 355034, 360405, 365825, 371293, 376810, 382376, 387991, 393655, 399368, 405131, 410943, 416805, 422717, 428679, 434692, 440754, 446867, 453031, 459245, 465510, 471827, 478194, 484613, 491083, 497604, 504177, 510803, 517480, 524209, 530990, 537824, 544710, 551649, 558641, 565685, 572783, 579934, 587138, 594395, 601706, 609071, 616490, 623962, 631489, 639070, 646705, 654395, 662139, 669938, 677792, 685700, 693664, 701683, 709758, 717888, 726074, 734315, 742612, 750966, 759375, 767841, 776363, 784941, 793576, 802268, 811017, 819823, 828686, 837606, 846583, 855618, 864711, 873861, 883069, 892335, 901660, 911042, 920483, 929982, 939540, 949156, 958832, 968566, 978359, 988212, 998124, 1.00809e+006, 1.01813e+006, 1.02822e+006, 1.03837e+006, 1.04858e+006, 1.05885e+006, 1.06918e+006, 1.07957e+006, 1.09002e+006, 1.10053e+006, 1.1111e+006, 1.12173e+006, 1.13243e+006, 1.14318e+006, 1.154e+006, 1.16487e+006, 1.17581e+006, 1.18681e+006, 1.19787e+006, 1.20899e+006, 1.22018e+006, 1.23142e+006, 1.24273e+006, 1.2541e+006, 1.26553e+006, 1.27703e+006, 1.28858e+006, 1.3002e+006, 1.31188e+006, 1.32363e+006, 1.33544e+006, 1.34731e+006, 1.35924e+006, 1.37124e+006, 1.3833e+006, 1.39542e+006, 1.40761e+006, 1.41986e+006, 1.43217e+006, 1.44455e+006, 1.45699e+006, 1.4695e+006, 1.48207e+006, 1.4947e+006, 1.5074e+006, 1.52017e+006, 1.53299e+006, 1.54589e+006, 1.55885e+006, 1.57187e+006, 1.58496e+006, 1.59811e+006, 1.61133e+006, 1.62461e+006, 1.63796e+006, 1.65138e+006, 1.66486e+006, 1.6784e+006, 1.69202e+006, 1.70569e+006, 1.71944e+006, 1.73325e+006, 1.74713e+006, 1.76107e+006, 1.77508e+006, 1.78916e+006, 1.8033e+006, 1.81751e+006, 1.83179e+006, 1.84613e+006, 1.86054e+006, 1.87502e+006, 1.88957e+006, 1.90418e+006, 1.91886e+006, 1.93361e+006, 1.94843e+006, 1.96331e+006, 1.97827e+006, 1.99329e+006, 2.00838e+006, 2.02353e+006, 2.03876e+006, 2.05405e+006, 2.06942e+006, 2.08485e+006, 2.10035e+006, 2.11592e+006, 2.13156e+006, 2.14727e+006, 2.16304e+006, 2.17889e+006, 2.19481e+006, 2.21079e+006, 2.22685e+006, 2.24297e+006, 2.25917e+006, 2.27543e+006, 2.29177e+006, 2.30817e+006, 2.32465e+006, 2.34119e+006, 2.35781e+006, 2.37449e+006, 2.39125e+006, 2.40808e+006, 2.42498e+006, 2.44195e+006, 2.45899e+006, 2.4761e+006, 2.49328e+006, 2.51054e+006, 2.52786e+006, 2.54526e+006, 2.56273e+006, 2.58027e+006, 2.59788e+006, 2.61557e+006, 2.63332e+006, 2.65115e+006, 2.66905e+006, 2.68703e+006, 2.70507e+006, 2.72319e+006, 2.74138e+006, 2.75965e+006, 2.77798e+006, 2.79639e+006, 2.81487e+006, 2.83343e+006, 2.85206e+006, 2.87076e+006, 2.88954e+006, 2.90838e+006, 2.92731e+006, 2.9463e+006, 2.96537e+006, 2.98452e+006, 3.00373e+006, 3.02303e+006, 3.04239e+006, 3.06183e+006, 3.08135e+006, 3.10094e+006, 3.1206e+006, 3.14034e+006, 3.16015e+006, 3.18004e+006, 3.2e+006, 3.22004e+006, 3.24015e+006, 3.26034e+006, 3.2806e+006, 3.30094e+006, 3.32135e+006, 3.34184e+006, 3.36241e+006, 3.38305e+006, 3.40377e+006, 3.42456e+006, 3.44543e+006, 3.46637e+006, 3.48739e+006, 3.50849e+006, 3.52966e+006, 3.55091e+006, 3.57224e+006, 3.59364e+006, 3.61512e+006, 3.63668e+006, 3.65832e+006, 3.68003e+006, 3.70181e+006, 3.72368e+006, 3.74562e+006, 3.76764e+006, 3.78974e+006, 3.81192e+006, 3.83417e+006, 3.8565e+006, 3.87891e+006, 3.90139e+006, 3.92396e+006, 3.9466e+006, 3.96932e+006, 3.99212e+006, 4.015e+006, 4.03795e+006, 4.06099e+006, 4.0841e+006, 4.10729e+006, 4.13056e+006, 4.15391e+006, 4.17734e+006, 4.20085e+006, 4.22444e+006, 4.2481e+006, 4.27185e+006, 4.29567e+006, 4.31958e+006, 4.34356e+006, 4.36763e+006, 4.39177e+006, 4.41599e+006, 4.4403e+006, 4.46468e+006, 4.48915e+006, 4.51369e+006, 4.53831e+006, 4.56302e+006, 4.5878e+006, 4.61267e+006, 4.63762e+006, 4.66265e+006, 4.68775e+006, 4.71294e+006, 4.73821e+006, 4.76357e+006, 4.789e+006, 4.81451e+006, 4.84011e+006, 4.86578e+006, 4.89154e+006, 4.91738e+006, 4.9433e+006, 4.96931e+006, 4.99539e+006, 5.02156e+006, 5.04781e+006, 5.07414e+006, 5.10056e+006, 5.12705e+006, 5.15363e+006, 5.18029e+006, 5.20704e+006, 5.23386e+006, 5.26077e+006, 5.28777e+006, 5.31484e+006, 5.342e+006, 5.36924e+006, 5.39656e+006, 5.42397e+006, 5.45146e+006, 5.47904e+006, 5.50669e+006, 5.53444e+006, 5.56226e+006, 5.59017e+006, 5.61816e+006, 5.64624e+006, 5.6744e+006, 5.70265e+006, 5.73097e+006, 5.75939e+006, 5.78789e+006, 5.81647e+006, 5.84513e+006, 5.87389e+006, 5.90272e+006, 5.93164e+006, 5.96065e+006, 5.98974e+006, 6.01891e+006, 6.04817e+006, 6.07752e+006, 6.10695e+006, 6.13647e+006, 6.16607e+006, 6.19576e+006, 6.22553e+006, 6.25539e+006, 6.28533e+006, 6.31536e+006, 6.34548e+006, 6.37568e+006, 6.40597e+006, 6.43634e+006, 6.4668e+006, 6.49735e+006, 6.52798e+006, 6.5587e+006, 6.58951e+006, 6.6204e+006, 6.65138e+006, 6.68245e+006, 6.7136e+006, 6.74484e+006, 6.77617e+006, 6.80759e+006, 6.83909e+006, 6.87068e+006, 6.90235e+006, 6.93412e+006, 6.96597e+006, 6.99791e+006, 7.02994e+006, 7.06205e+006, 7.09425e+006, 7.12654e+006, 7.15892e+006, 7.19139e+006, 7.22394e+006, 7.25659e+006, 7.28932e+006, 7.32214e+006, 7.35505e+006, 7.38804e+006, 7.42113e+006, 7.4543e+006, 7.48757e+006, 7.52092e+006, 7.55436e+006, 7.58789e+006, 7.62151e+006, 7.65522e+006, 7.68902e+006, 7.7229e+006, 7.75688e+006, 7.79095e+006, 7.8251e+006, 7.85935e+006, 7.89368e+006, 7.92811e+006, 7.96262e+006, 7.99723e+006, 8.03192e+006, 8.06671e+006, 8.10158e+006, 8.13655e+006, 8.17161e+006, 8.20675e+006, 8.24199e+006, 8.27732e+006, 8.31274e+006, 8.34825e+006, 8.38385e+006, 8.41954e+006, 8.45532e+006, 8.49119e+006, 8.52716e+006, 8.56321e+006, 8.59936e+006, 8.6356e+006, 8.67193e+006, 8.70835e+006, 8.74486e+006, 8.78147e+006, 8.81816e+006, 8.85495e+006, 8.89183e+006, 8.9288e+006, 8.96587e+006, 9.00302e+006, 9.04027e+006, 9.07761e+006, 9.11505e+006, 9.15257e+006, 9.19019e+006, 9.2279e+006, 9.26571e+006, 9.3036e+006, 9.34159e+006, 9.37967e+006, 9.41785e+006, 9.45612e+006, 9.49448e+006, 9.53293e+006, 9.57148e+006, 9.61012e+006, 9.64886e+006, 9.68769e+006, 9.72661e+006, 9.76563e+006, 9.80473e+006, 9.84394e+006, 9.88323e+006, 9.92263e+006, 9.96211e+006, 1.00017e+007, 1.00414e+007, 1.00811e+007, 1.0121e+007, 1.01609e+007, 1.0201e+007, 1.02411e+007, 1.02814e+007, 1.03217e+007, 1.03622e+007, 1.04027e+007, 1.04433e+007, 1.0484e+007, 1.05248e+007, 1.05657e+007, 1.06067e+007, 1.06478e+007, 1.0689e+007, 1.07303e+007, 1.07717e+007, 1.08132e+007, 1.08547e+007, 1.08964e+007, 1.09382e+007, 1.098e+007, 1.1022e+007, 1.1064e+007, 1.11062e+007, 1.11484e+007, 1.11908e+007, 1.12332e+007, 1.12757e+007, 1.13184e+007, 1.13611e+007, 1.14039e+007, 1.14468e+007, 1.14899e+007, 1.1533e+007, 1.15762e+007, 1.16195e+007, 1.16629e+007, 1.17064e+007, 1.175e+007, 1.17937e+007, 1.18375e+007, 1.18814e+007, 1.19254e+007, 1.19695e+007, 1.20136e+007, 1.20579e+007, 1.21023e+007, 1.21468e+007, 1.21913e+007, 1.2236e+007, 1.22808e+007, 1.23257e+007, 1.23706e+007, 1.24157e+007, 1.24609e+007, 1.25061e+007, 1.25515e+007, 1.25969e+007, 1.26425e+007, 1.26882e+007, 1.27339e+007, 1.27798e+007, 1.28257e+007, 1.28718e+007, 1.29179e+007, 1.29642e+007, 1.30105e+007, 1.3057e+007, 1.31035e+007, 1.31502e+007, 1.31969e+007, 1.32438e+007, 1.32907e+007, 1.33378e+007, 1.33849e+007, 1.34322e+007, 1.34795e+007, 1.3527e+007, 1.35745e+007, 1.36221e+007, 1.36699e+007, 1.37177e+007, 1.37657e+007, 1.38137e+007, 1.38619e+007, 1.39101e+007, 1.39585e+007, 1.40069e+007, 1.40555e+007, 1.41041e+007, 1.41529e+007, 1.42017e+007, 1.42507e+007, 1.42998e+007, 1.43489e+007, 1.43982e+007, 1.44475e+007, 1.4497e+007, 1.45465e+007, 1.45962e+007, 1.4646e+007, 1.46958e+007, 1.47458e+007, 1.47959e+007, 1.48461e+007, 1.48963e+007, 1.49467e+007, 1.49972e+007, 1.50478e+007, 1.50984e+007, 1.51492e+007, 1.52001e+007, 1.52511e+007, 1.53022e+007, 1.53534e+007, 1.54047e+007, 1.54561e+007, 1.55076e+007, 1.55592e+007, 1.56109e+007, 1.56627e+007, 1.57146e+007, 1.57667e+007, 1.58188e+007, 1.5871e+007, 1.59233e+007, 1.59758e+007, 1.60283e+007, 1.60809e+007, 1.61337e+007, 1.61865e+007, 1.62395e+007, 1.62925e+007, 1.63457e+007, 1.63989e+007, 1.64523e+007, 1.65058e+007, 1.65593e+007, 1.6613e+007, 1.66668e+007, 1.67207e+007, 1.67747e+007, 1.68288e+007, 1.6883e+007, 1.69373e+007, 1.69917e+007, 1.70462e+007, 1.71008e+007, 1.71555e+007, 1.72104e+007, 1.72653e+007, 1.73203e+007, 1.73755e+007, 1.74307e+007, 1.74861e+007, 1.75415e+007, 1.75971e+007, 1.76528e+007, 1.77085e+007, 1.77644e+007, 1.78204e+007, 1.78765e+007, 1.79327e+007, 1.7989e+007, 1.80454e+007, 1.81019e+007, 1.81586e+007, 1.82153e+007, 1.82721e+007, 1.83291e+007, 1.83861e+007, 1.84433e+007, 1.85005e+007, 1.85579e+007, 1.86154e+007, 1.86729e+007, 1.87306e+007, 1.87884e+007, 1.88463e+007, 1.89043e+007, 1.89624e+007, 1.90207e+007, 1.9079e+007, 1.91374e+007, 1.9196e+007, 1.92546e+007, 1.93134e+007, 1.93722e+007, 1.94312e+007, 1.94903e+007, 1.95495e+007, 1.96088e+007, 1.96682e+007, 1.97277e+007, 1.97873e+007, 1.9847e+007, 1.99069e+007, 1.99668e+007, 2.00268e+007, 2.0087e+007, 2.01473e+007, 2.02076e+007, 2.02681e+007, 2.03287e+007, 2.03894e+007, 2.04502e+007, 2.05111e+007, 2.05722e+007, 2.06333e+007, 2.06946e+007, 2.07559e+007, 2.08174e+007, 2.08789e+007, 2.09406e+007, 2.10024e+007, 2.10643e+007, 2.11263e+007, 2.11884e+007, 2.12507e+007, 2.1313e+007, 2.13755e+007, 2.1438e+007, 2.15007e+007, 2.15634e+007, 2.16263e+007, 2.16893e+007, 2.17524e+007, 2.18157e+007, 2.1879e+007, 2.19424e+007, 2.2006e+007, 2.20696e+007, 2.21334e+007, 2.21973e+007, 2.22612e+007, 2.23253e+007, 2.23896e+007, 2.24539e+007, 2.25183e+007, 2.25828e+007, 2.26475e+007, 2.27123e+007, 2.27771e+007, 2.28421e+007, 2.29072e+007, 2.29724e+007, 2.30377e+007, 2.31032e+007, 2.31687e+007, 2.32344e+007, 2.33001e+007, 2.3366e+007, 2.3432e+007, 2.34981e+007, 2.35643e+007, 2.36306e+007, 2.3697e+007, 2.37636e+007, 2.38303e+007, 2.3897e+007, 2.39639e+007, 2.40309e+007, 2.4098e+007, 2.41652e+007, 2.42326e+007, 2.43e+007, 2.43676e+007, 2.44352e+007, 2.4503e+007, 2.45709e+007, 2.46389e+007, 2.4707e+007, 2.47753e+007, 2.48436e+007, 2.49121e+007, 2.49806e+007, 2.50493e+007, 2.51181e+007, 2.5187e+007, 2.52561e+007, 2.53252e+007, 2.53944e+007, 2.54638e+007, 2.55333e+007, 2.56029e+007, 2.56726e+007, 2.57424e+007, 2.58123e+007, 2.58824e+007, 2.59525e+007, 2.60228e+007, 2.60932e+007, 2.61637e+007, 2.62343e+007, 2.63051e+007, 2.63759e+007, 2.64469e+007, 2.65179e+007, 2.65891e+007, 2.66604e+007, 2.67319e+007, 2.68034e+007, 2.6875e+007, 2.69468e+007, 2.70187e+007, 2.70907e+007, 2.71628e+007, 2.7235e+007, 2.73073e+007, 2.73798e+007, 2.74523e+007, 2.7525e+007, 2.75978e+007, 2.76707e+007, 2.77438e+007, 2.78169e+007, 2.78902e+007, 2.79636e+007, 2.8037e+007, 2.81107e+007, 2.81844e+007, 2.82582e+007, 2.83322e+007, 2.84062e+007, 2.84804e+007, 2.85547e+007, 2.86292e+007, 2.87037e+007, 2.87783e+007, 2.88531e+007, 2.8928e+007, 2.9003e+007, 2.90781e+007, 2.91533e+007, 2.92287e+007, 2.93042e+007, 2.93797e+007, 2.94555e+007, 2.95313e+007, 2.96072e+007, 2.96833e+007, 2.97594e+007, 2.98357e+007, 2.99121e+007, 2.99886e+007, 3.00653e+007, 3.0142e+007, 3.02189e+007, 3.02959e+007, 3.0373e+007, 3.04502e+007, 3.05276e+007, 3.0605e+007, 3.06826e+007, 3.07603e+007, 3.08381e+007, 3.09161e+007, 3.09941e+007, 3.10723e+007, 3.11506e+007, 3.1229e+007, 3.13075e+007, 3.13861e+007, 3.14649e+007, 3.15438e+007, 3.16228e+007, 3.17019e+007, 3.17811e+007, 3.18605e+007, 3.194e+007, 3.20195e+007, 3.20993e+007, 3.21791e+007, 3.2259e+007, 3.23391e+007, 3.24193e+007, 3.24996e+007, 3.258e+007, 3.26606e+007, 3.27412e+007, 3.2822e+007, 3.29029e+007, 3.29839e+007, 3.30651e+007, 3.31463e+007, 3.32277e+007, 3.33092e+007, 3.33908e+007, 3.34726e+007, 3.35544e+007, 3.36364e+007, 3.37185e+007, 3.38007e+007, 3.38831e+007, 3.39655e+007, 3.40481e+007, 3.41308e+007, 3.42136e+007, 3.42966e+007, 3.43796e+007, 3.44628e+007, 3.45461e+007, 3.46296e+007, 3.47131e+007, 3.47968e+007, 3.48806e+007, 3.49645e+007, 3.50485e+007, 3.51326e+007, 3.52169e+007, 3.53013e+007, 3.53858e+007, 3.54705e+007, 3.55552e+007, 3.56401e+007, 3.57251e+007, 3.58102e+007, 3.58954e+007, 3.59808e+007, 3.60663e+007, 3.61519e+007, 3.62376e+007, 3.63235e+007, 3.64095e+007, 3.64955e+007, 3.65818e+007, 3.66681e+007, 3.67546e+007, 3.68411e+007, 3.69279e+007, 3.70147e+007, 3.71016e+007, 3.71887e+007, 3.72759e+007, 3.73632e+007, 3.74507e+007, 3.75382e+007, 3.76259e+007, 3.77137e+007, 3.78016e+007, 3.78897e+007, 3.79779e+007, 3.80662e+007, 3.81546e+007, 3.82431e+007, 3.83318e+007, 3.84206e+007, 3.85095e+007, 3.85986e+007, 3.86877e+007, 3.8777e+007, 3.88664e+007, 3.8956e+007, 3.90456e+007, 3.91354e+007, 3.92253e+007, 3.93153e+007, 3.94055e+007, 3.94958e+007, 3.95862e+007, 3.96767e+007, 3.97673e+007, 3.98581e+007, 3.9949e+007, 4.004e+007, 4.01312e+007, 4.02224e+007, 4.03138e+007, 4.04053e+007, 4.0497e+007, 4.05888e+007, 4.06806e+007, 4.07727e+007, 4.08648e+007, 4.09571e+007, 4.10495e+007, 4.1142e+007, 4.12346e+007, 4.13274e+007, 4.14203e+007, 4.15133e+007, 4.16064e+007, 4.16997e+007, 4.17931e+007, 4.18866e+007, 4.19803e+007, 4.2074e+007, 4.21679e+007, 4.22619e+007, 4.23561e+007, 4.24504e+007, 4.25448e+007, 4.26393e+007, 4.27339e+007, 4.28287e+007, 4.29236e+007, 4.30186e+007, 4.31138e+007, 4.32091e+007, 4.33045e+007, 4.34e+007, 4.34957e+007, 4.35914e+007, 4.36873e+007, 4.37834e+007, 4.38795e+007, 4.39758e+007, 4.40723e+007, 4.41688e+007, 4.42655e+007, 4.43623e+007, 4.44592e+007, 4.45562e+007, 4.46534e+007, 4.47507e+007, 4.48482e+007, 4.49457e+007, 4.50434e+007, 4.51412e+007, 4.52392e+007, 4.53372e+007, 4.54354e+007, 4.55337e+007, 4.56322e+007, 4.57308e+007, 4.58295e+007, 4.59283e+007, 4.60273e+007, 4.61264e+007, 4.62256e+007, 4.63249e+007, 4.64244e+007, 4.6524e+007, 4.66237e+007, 4.67236e+007, 4.68236e+007, 4.69237e+007, 4.70239e+007, 4.71243e+007, 4.72248e+007, 4.73254e+007, 4.74262e+007, 4.75271e+007, 4.76281e+007, 4.77292e+007, 4.78305e+007, 4.79319e+007, 4.80334e+007, 4.81351e+007, 4.82369e+007, 4.83388e+007, 4.84408e+007, 4.8543e+007, 4.86453e+007, 4.87478e+007, 4.88503e+007, 4.8953e+007, 4.90558e+007, 4.91588e+007, 4.92619e+007, 4.93651e+007, 4.94684e+007, 4.95719e+007, 4.96755e+007, 4.97792e+007, 4.98831e+007, 4.99871e+007, 5.00912e+007, 5.01954e+007, 5.02998e+007, 5.04043e+007, 5.05089e+007, 5.06137e+007, 5.07186e+007, 5.08236e+007, 5.09288e+007, 5.10341e+007, 5.11395e+007, 5.12451e+007, 5.13507e+007, 5.14566e+007, 5.15625e+007, 5.16686e+007, 5.17748e+007, 5.18811e+007, 5.19876e+007, 5.20942e+007, 5.22009e+007, 5.23078e+007, 5.24148e+007, 5.25219e+007, 5.26291e+007, 5.27365e+007, 5.2844e+007, 5.29517e+007, 5.30595e+007, 5.31674e+007, 5.32754e+007, 5.33836e+007, 5.34919e+007, 5.36003e+007, 5.37089e+007, 5.38176e+007, 5.39264e+007, 5.40354e+007, 5.41445e+007, 5.42537e+007, 5.43631e+007, 5.44726e+007, 5.45822e+007, 5.46919e+007, 5.48018e+007, 5.49119e+007, 5.5022e+007, 5.51323e+007, 5.52427e+007, 5.53533e+007, 5.5464e+007, 5.55748e+007, 5.56857e+007, 5.57968e+007, 5.5908e+007, 5.60194e+007, 5.61308e+007, 5.62425e+007, 5.63542e+007, 5.64661e+007, 5.65781e+007, 5.66903e+007, 5.68025e+007, 5.69149e+007, 5.70275e+007, 5.71402e+007, 5.7253e+007, 5.73659e+007, 5.7479e+007, 5.75922e+007, 5.77056e+007, 5.78191e+007, 5.79327e+007, 5.80464e+007, 5.81603e+007, 5.82743e+007, 5.83885e+007, 5.85028e+007, 5.86172e+007, 5.87317e+007, 5.88464e+007, 5.89612e+007, 5.90762e+007, 5.91913e+007, 5.93065e+007, 5.94219e+007, 5.95374e+007, 5.9653e+007, 5.97688e+007, 5.98847e+007, 6.00007e+007, 6.01169e+007, 6.02332e+007, 6.03496e+007, 6.04662e+007, 6.05829e+007, 6.06997e+007, 6.08167e+007, 6.09338e+007, 6.10511e+007, 6.11684e+007, 6.1286e+007, 6.14036e+007, 6.15214e+007, 6.16393e+007, 6.17574e+007, 6.18756e+007, 6.19939e+007, 6.21124e+007, 6.2231e+007, 6.23497e+007, 6.24686e+007, 6.25876e+007, 6.27068e+007, 6.2826e+007, 6.29455e+007, 6.3065e+007, 6.31847e+007, 6.33045e+007, 6.34245e+007, 6.35446e+007, 6.36648e+007, 6.37852e+007, 6.39057e+007, 6.40264e+007, 6.41471e+007, 6.42681e+007, 6.43891e+007, 6.45103e+007, 6.46316e+007, 6.47531e+007, 6.48747e+007, 6.49964e+007, 6.51183e+007, 6.52403e+007, 6.53625e+007, 6.54848e+007, 6.56072e+007, 6.57298e+007, 6.58525e+007, 6.59753e+007, 6.60983e+007, 6.62214e+007, 6.63446e+007, 6.6468e+007, 6.65915e+007, 6.67152e+007, 6.6839e+007, 6.69629e+007, 6.7087e+007, 6.72112e+007, 6.73356e+007, 6.74601e+007, 6.75847e+007, 6.77094e+007, 6.78343e+007, 6.79594e+007, 6.80846e+007, 6.82099e+007, 6.83353e+007, 6.84609e+007, 6.85867e+007, 6.87125e+007, 6.88385e+007, 6.89647e+007, 6.9091e+007, 6.92174e+007, 6.9344e+007, 6.94707e+007, 6.95975e+007, 6.97245e+007, 6.98516e+007, 6.99789e+007, 7.01063e+007, 7.02338e+007, 7.03615e+007, 7.04893e+007, 7.06172e+007, 7.07453e+007, 7.08736e+007, 7.10019e+007, 7.11304e+007, 7.12591e+007, 7.13879e+007, 7.15168e+007, 7.16459e+007, 7.17751e+007, 7.19044e+007, 7.20339e+007, 7.21635e+007, 7.22933e+007, 7.24232e+007, 7.25533e+007, 7.26834e+007, 7.28138e+007, 7.29442e+007, 7.30748e+007, 7.32056e+007, 7.33365e+007, 7.34675e+007, 7.35987e+007, 7.373e+007, 7.38614e+007, 7.3993e+007, 7.41248e+007, 7.42566e+007, 7.43886e+007, 7.45208e+007, 7.46531e+007, 7.47855e+007, 7.49181e+007, 7.50508e+007, 7.51837e+007, 7.53167e+007, 7.54498e+007, 7.55831e+007, 7.57165e+007, 7.58501e+007, 7.59838e+007, 7.61176e+007, 7.62516e+007, 7.63857e+007, 7.652e+007, 7.66544e+007, 7.6789e+007, 7.69237e+007, 7.70585e+007, 7.71935e+007, 7.73286e+007, 7.74639e+007, 7.75993e+007, 7.77348e+007, 7.78705e+007, 7.80063e+007, 7.81423e+007, 7.82784e+007, 7.84147e+007, 7.8551e+007, 7.86876e+007, 7.88243e+007, 7.89611e+007, 7.90981e+007, 7.92352e+007, 7.93724e+007, 7.95098e+007, 7.96473e+007, 7.9785e+007, 7.99229e+007, 8.00608e+007, 8.01989e+007, 8.03372e+007, 8.04756e+007, 8.06141e+007, 8.07528e+007, 8.08916e+007, 8.10306e+007, 8.11697e+007, 8.13089e+007, 8.14483e+007, 8.15879e+007, 8.17275e+007, 8.18674e+007, 8.20073e+007, 8.21474e+007, 8.22877e+007, 8.24281e+007, 8.25686e+007, 8.27093e+007, 8.28502e+007, 8.29911e+007, 8.31322e+007, 8.32735e+007, 8.34149e+007, 8.35565e+007, 8.36982e+007, 8.384e+007, 8.3982e+007, 8.41241e+007, 8.42664e+007, 8.44088e+007, 8.45513e+007, 8.4694e+007, 8.48369e+007, 8.49799e+007, 8.5123e+007, 8.52663e+007, 8.54097e+007, 8.55533e+007, 8.5697e+007, 8.58409e+007, 8.59849e+007, 8.6129e+007, 8.62733e+007, 8.64178e+007, 8.65623e+007, 8.67071e+007, 8.68519e+007, 8.6997e+007, 8.71421e+007, 8.72874e+007, 8.74329e+007, 8.75785e+007, 8.77242e+007, 8.78701e+007, 8.80162e+007, 8.81623e+007, 8.83087e+007, 8.84551e+007, 8.86018e+007, 8.87485e+007, 8.88954e+007, 8.90425e+007, 8.91897e+007, 8.9337e+007, 8.94845e+007, 8.96322e+007, 8.978e+007, 8.99279e+007, 9.0076e+007, 9.02242e+007, 9.03726e+007, 9.05211e+007, 9.06697e+007, 9.08186e+007, 9.09675e+007, 9.11166e+007, 9.12659e+007, 9.14153e+007, 9.15648e+007, 9.17145e+007, 9.18643e+007, 9.20143e+007, 9.21644e+007, 9.23147e+007, 9.24651e+007, 9.26157e+007, 9.27664e+007, 9.29173e+007, 9.30683e+007, 9.32195e+007, 9.33708e+007, 9.35222e+007, 9.36738e+007, 9.38256e+007, 9.39775e+007, 9.41295e+007, 9.42817e+007, 9.4434e+007, 9.45865e+007, 9.47392e+007, 9.48919e+007, 9.50449e+007, 9.51979e+007, 9.53512e+007, 9.55045e+007, 9.56581e+007, 9.58117e+007, 9.59655e+007, 9.61195e+007, 9.62736e+007, 9.64279e+007, 9.65823e+007, 9.67368e+007, 9.68915e+007, 9.70464e+007, 9.72014e+007, 9.73565e+007, 9.75118e+007, 9.76673e+007, 9.78229e+007, 9.79786e+007, 9.81345e+007, 9.82906e+007, 9.84468e+007, 9.86031e+007, 9.87596e+007, 9.89162e+007, 9.9073e+007, 9.92299e+007, 9.9387e+007, 9.95443e+007, 9.97016e+007, 9.98592e+007, 1.00017e+008, 1.00175e+008, 1.00333e+008, 1.00491e+008, 1.00649e+008, 1.00807e+008, 1.00966e+008, 1.01125e+008, 1.01284e+008, 1.01443e+008, 1.01602e+008, 1.01761e+008, 1.01921e+008, 1.0208e+008, 1.0224e+008, 1.024e+008, 1.0256e+008, 1.0272e+008, 1.02881e+008, 1.03041e+008, 1.03202e+008, 1.03363e+008, 1.03524e+008, 1.03685e+008, 1.03846e+008, 1.04008e+008, 1.04169e+008, 1.04331e+008, 1.04493e+008, 1.04655e+008, 1.04817e+008, 1.04979e+008, 1.05142e+008, 1.05304e+008, 1.05467e+008, 1.0563e+008, 1.05793e+008, 1.05956e+008, 1.0612e+008, 1.06283e+008, 1.06447e+008, 1.06611e+008, 1.06775e+008, 1.06939e+008, 1.07103e+008, 1.07268e+008, 1.07432e+008, 1.07597e+008, 1.07762e+008, 1.07927e+008, 1.08092e+008, 1.08258e+008, 1.08423e+008, 1.08589e+008, 1.08755e+008, 1.0892e+008, 1.09087e+008, 1.09253e+008, 1.09419e+008, 1.09586e+008, 1.09753e+008, 1.09919e+008, 1.10086e+008, 1.10254e+008, 1.10421e+008, 1.10588e+008, 1.10756e+008, 1.10924e+008, 1.11092e+008, 1.1126e+008, 1.11428e+008, 1.11597e+008, 1.11765e+008, 1.11934e+008, 1.12103e+008, 1.12272e+008, 1.12441e+008, 1.1261e+008, 1.1278e+008, 1.12949e+008, 1.13119e+008, 1.13289e+008, 1.13459e+008, 1.13629e+008, 1.138e+008, 1.1397e+008, 1.14141e+008, 1.14312e+008, 1.14483e+008, 1.14654e+008, 1.14825e+008, 1.14997e+008, 1.15168e+008, 1.1534e+008, 1.15512e+008, 1.15684e+008, 1.15856e+008, 1.16029e+008, 1.16201e+008, 1.16374e+008, 1.16547e+008, 1.1672e+008, 1.16893e+008, 1.17066e+008, 1.1724e+008, 1.17413e+008, 1.17587e+008, 1.17761e+008, 1.17935e+008, 1.18109e+008, 1.18284e+008, 1.18458e+008, 1.18633e+008, 1.18808e+008, 1.18983e+008, 1.19158e+008, 1.19333e+008, 1.19509e+008, 1.19684e+008, 1.1986e+008, 1.20036e+008, 1.20212e+008, 1.20388e+008, 1.20565e+008, 1.20741e+008, 1.20918e+008, 1.21095e+008, 1.21272e+008, 1.21449e+008, 1.21626e+008, 1.21804e+008, 1.21981e+008, 1.22159e+008, 1.22337e+008, 1.22515e+008, 1.22693e+008, 1.22872e+008, 1.2305e+008, 1.23229e+008, 1.23408e+008, 1.23587e+008, 1.23766e+008, 1.23946e+008, 1.24125e+008, 1.24305e+008, 1.24484e+008, 1.24664e+008, 1.24845e+008, 1.25025e+008, 1.25205e+008, 1.25386e+008, 1.25567e+008, 1.25748e+008, 1.25929e+008, 1.2611e+008, 1.26291e+008, 1.26473e+008, 1.26654e+008, 1.26836e+008, 1.27018e+008, 1.272e+008, 1.27383e+008, 1.27565e+008, 1.27748e+008, 1.27931e+008, 1.28114e+008, 1.28297e+008, 1.2848e+008, 1.28663e+008, 1.28847e+008, 1.29031e+008, 1.29215e+008, 1.29399e+008, 1.29583e+008, 1.29767e+008, 1.29952e+008, 1.30136e+008, 1.30321e+008, 1.30506e+008, 1.30691e+008, 1.30877e+008, 1.31062e+008, 1.31248e+008, 1.31433e+008, 1.31619e+008, 1.31805e+008, 1.31992e+008, 1.32178e+008, 1.32365e+008, 1.32551e+008, 1.32738e+008, 1.32925e+008, 1.33112e+008, 1.333e+008, 1.33487e+008, 1.33675e+008, 1.33863e+008, 1.34051e+008, 1.34239e+008, 1.34427e+008, 1.34616e+008, 1.34804e+008, 1.34993e+008, 1.35182e+008, 1.35371e+008, 1.3556e+008, 1.3575e+008, 1.35939e+008, 1.36129e+008, 1.36319e+008, 1.36509e+008, 1.36699e+008, 1.3689e+008, 1.3708e+008, 1.37271e+008, 1.37462e+008, 1.37653e+008, 1.37844e+008, 1.38035e+008, 1.38227e+008, 1.38418e+008, 1.3861e+008, 1.38802e+008, 1.38994e+008, 1.39186e+008, 1.39379e+008, 1.39571e+008, 1.39764e+008, 1.39957e+008, 1.4015e+008, 1.40343e+008, 1.40537e+008, 1.4073e+008, 1.40924e+008, 1.41118e+008, 1.41312e+008, 1.41506e+008, 1.417e+008, 1.41895e+008, 1.4209e+008, 1.42284e+008, 1.42479e+008, 1.42675e+008, 1.4287e+008, 1.43065e+008, 1.43261e+008, 1.43457e+008, 1.43653e+008, 1.43849e+008, 1.44045e+008, 1.44241e+008, 1.44438e+008, 1.44635e+008, 1.44832e+008, 1.45029e+008, 1.45226e+008, 1.45423e+008, 1.45621e+008, 1.45819e+008, 1.46017e+008, 1.46215e+008, 1.46413e+008, 1.46611e+008, 1.4681e+008, 1.47008e+008, 1.47207e+008, 1.47406e+008, 1.47605e+008, 1.47805e+008, 1.48004e+008, 1.48204e+008, 1.48404e+008, 1.48604e+008, 1.48804e+008, 1.49004e+008, 1.49205e+008, 1.49405e+008, 1.49606e+008, 1.49807e+008, 1.50008e+008, 1.50209e+008, 1.50411e+008, 1.50612e+008, 1.50814e+008, 1.51016e+008, 1.51218e+008, 1.5142e+008, 1.51623e+008, 1.51825e+008, 1.52028e+008, 1.52231e+008, 1.52434e+008, 1.52637e+008, 1.52841e+008, 1.53044e+008, 1.53248e+008, 1.53452e+008, 1.53656e+008, 1.5386e+008, 1.54064e+008, 1.54269e+008, 1.54474e+008, 1.54678e+008, 1.54883e+008, 1.55089e+008, 1.55294e+008, 1.55499e+008, 1.55705e+008, 1.55911e+008, 1.56117e+008, 1.56323e+008, 1.56529e+008, 1.56736e+008, 1.56942e+008, 1.57149e+008, 1.57356e+008, 1.57563e+008, 1.57771e+008, 1.57978e+008, 1.58186e+008, 1.58394e+008, 1.58601e+008, 1.5881e+008, 1.59018e+008, 1.59226e+008, 1.59435e+008, 1.59644e+008, 1.59853e+008, 1.60062e+008, 1.60271e+008, 1.6048e+008, 1.6069e+008, 1.609e+008, 1.6111e+008, 1.6132e+008, 1.6153e+008, 1.6174e+008, 1.61951e+008, 1.62162e+008, 1.62373e+008, 1.62584e+008, 1.62795e+008, 1.63006e+008, 1.63218e+008, 1.6343e+008, 1.63641e+008, 1.63853e+008, 1.64066e+008, 1.64278e+008, 1.64491e+008, 1.64703e+008, 1.64916e+008, 1.65129e+008, 1.65342e+008, 1.65556e+008, 1.65769e+008, 1.65983e+008, 1.66197e+008, 1.66411e+008, 1.66625e+008, 1.6684e+008, 1.67054e+008, 1.67269e+008, 1.67484e+008, 1.67699e+008, 1.67914e+008, 1.68129e+008, 1.68345e+008, 1.6856e+008, 1.68776e+008, 1.68992e+008, 1.69208e+008, 1.69425e+008, 1.69641e+008, 1.69858e+008, 1.70075e+008, 1.70292e+008, 1.70509e+008, 1.70726e+008, 1.70944e+008, 1.71162e+008, 1.71379e+008, 1.71597e+008, 1.71816e+008, 1.72034e+008, 1.72253e+008, 1.72471e+008, 1.7269e+008, 1.72909e+008, 1.73128e+008, 1.73348e+008, 1.73567e+008, 1.73787e+008, 1.74007e+008, 1.74227e+008, 1.74447e+008, 1.74667e+008, 1.74888e+008, 1.75108e+008, 1.75329e+008, 1.7555e+008, 1.75771e+008, 1.75993e+008, 1.76214e+008, 1.76436e+008, 1.76658e+008, 1.7688e+008, 1.77102e+008, 1.77324e+008, 1.77547e+008, 1.77769e+008, 1.77992e+008, 1.78215e+008, 1.78439e+008, 1.78662e+008, 1.78885e+008, 1.79109e+008, 1.79333e+008, 1.79557e+008, 1.79781e+008, 1.80006e+008, 1.8023e+008, 1.80455e+008, 1.8068e+008, 1.80905e+008, 1.8113e+008, 1.81355e+008, 1.81581e+008, 1.81807e+008, 1.82032e+008, 1.82258e+008, 1.82485e+008, 1.82711e+008, 1.82938e+008, 1.83164e+008, 1.83391e+008, 1.83618e+008, 1.83845e+008, 1.84073e+008, 1.843e+008, 1.84528e+008, 1.84756e+008, 1.84984e+008, 1.85212e+008, 1.85441e+008, 1.85669e+008, 1.85898e+008, 1.86127e+008, 1.86356e+008, 1.86585e+008, 1.86815e+008, 1.87044e+008, 1.87274e+008, 1.87504e+008, 1.87734e+008, 1.87964e+008, 1.88195e+008, 1.88425e+008, 1.88656e+008, 1.88887e+008, 1.89118e+008, 1.89349e+008, 1.89581e+008, 1.89813e+008, 1.90044e+008, 1.90276e+008, 1.90508e+008, 1.90741e+008, 1.90973e+008, 1.91206e+008, 1.91439e+008, 1.91672e+008, 1.91905e+008, 1.92138e+008, 1.92372e+008, 1.92605e+008, 1.92839e+008, 1.93073e+008, 1.93307e+008, 1.93542e+008, 1.93776e+008, 1.94011e+008, 1.94246e+008, 1.94481e+008, 1.94716e+008, 1.94951e+008, 1.95187e+008, 1.95422e+008, 1.95658e+008, 1.95894e+008, 1.96131e+008, 1.96367e+008, 1.96603e+008, 1.9684e+008, 1.97077e+008, 1.97314e+008, 1.97551e+008, 1.97789e+008, 1.98026e+008, 1.98264e+008, 1.98502e+008, 1.9874e+008, 1.98978e+008, 1.99217e+008, 1.99456e+008, 1.99694e+008, 1.99933e+008, 2.00172e+008, 2.00412e+008, 2.00651e+008, 2.00891e+008, 2.01131e+008, 2.01371e+008, 2.01611e+008, 2.01851e+008, 2.02092e+008, 2.02332e+008, 2.02573e+008, 2.02814e+008, 2.03055e+008, 2.03297e+008, 2.03538e+008, 2.0378e+008, 2.04022e+008, 2.04264e+008, 2.04506e+008, 2.04748e+008, 2.04991e+008, 2.05234e+008, 2.05477e+008, 2.0572e+008, 2.05963e+008, 2.06206e+008, 2.0645e+008, 2.06694e+008, 2.06938e+008, 2.07182e+008, 2.07426e+008, 2.07671e+008, 2.07915e+008, 2.0816e+008, 2.08405e+008, 2.0865e+008, 2.08895e+008, 2.09141e+008, 2.09387e+008, 2.09633e+008, 2.09879e+008, 2.10125e+008, 2.10371e+008, 2.10618e+008, 2.10864e+008, 2.11111e+008, 2.11358e+008, 2.11606e+008, 2.11853e+008, 2.121e+008, 2.12348e+008, 2.12596e+008, 2.12844e+008, 2.13093e+008, 2.13341e+008, 2.1359e+008, 2.13838e+008, 2.14087e+008, 2.14337e+008, 2.14586e+008, 2.14835e+008, 2.15085e+008, 2.15335e+008, 2.15585e+008, 2.15835e+008, 2.16085e+008, 2.16336e+008, 2.16587e+008, 2.16837e+008, 2.17089e+008, 2.1734e+008, 2.17591e+008, 2.17843e+008, 2.18095e+008, 2.18346e+008, 2.18599e+008, 2.18851e+008, 2.19103e+008, 2.19356e+008, 2.19609e+008, 2.19862e+008, 2.20115e+008, 2.20368e+008, 2.20622e+008, 2.20875e+008, 2.21129e+008, 2.21383e+008, 2.21637e+008, 2.21892e+008, 2.22146e+008, 2.22401e+008, 2.22656e+008, 2.22911e+008, 2.23166e+008, 2.23422e+008, 2.23677e+008, 2.23933e+008, 2.24189e+008, 2.24445e+008, 2.24701e+008, 2.24958e+008, 2.25215e+008, 2.25471e+008, 2.25728e+008, 2.25986e+008, 2.26243e+008, 2.26501e+008, 2.26758e+008, 2.27016e+008, 2.27274e+008, 2.27532e+008, 2.27791e+008, 2.28049e+008, 2.28308e+008, 2.28567e+008, 2.28826e+008, 2.29086e+008, 2.29345e+008, 2.29605e+008, 2.29864e+008, 2.30124e+008, 2.30385e+008, 2.30645e+008, 2.30906e+008, 2.31166e+008, 2.31427e+008, 2.31688e+008, 2.31949e+008, 2.32211e+008, 2.32472e+008, 2.32734e+008, 2.32996e+008, 2.33258e+008, 2.33521e+008, 2.33783e+008, 2.34046e+008, 2.34308e+008, 2.34571e+008, 2.34835e+008, 2.35098e+008, 2.35362e+008, 2.35625e+008, 2.35889e+008, 2.36153e+008, 2.36417e+008, 2.36682e+008, 2.36946e+008, 2.37211e+008, 2.37476e+008, 2.37741e+008, 2.38007e+008, 2.38272e+008, 2.38538e+008, 2.38804e+008, 2.3907e+008, 2.39336e+008, 2.39602e+008, 2.39869e+008, 2.40135e+008, 2.40402e+008, 2.40669e+008, 2.40937e+008, 2.41204e+008, 2.41472e+008, 2.4174e+008, 2.42008e+008, 2.42276e+008, 2.42544e+008, 2.42813e+008, 2.43081e+008, 2.4335e+008, 2.43619e+008, 2.43888e+008, 2.44158e+008, 2.44427e+008, 2.44697e+008, 2.44967e+008, 2.45237e+008, 2.45507e+008, 2.45778e+008, 2.46049e+008, 2.46319e+008, 2.4659e+008, 2.46862e+008, 2.47133e+008, 2.47404e+008, 2.47676e+008, 2.47948e+008, 2.4822e+008, 2.48492e+008, 2.48765e+008, 2.49038e+008, 2.4931e+008, 2.49583e+008, 2.49856e+008, 2.5013e+008, 2.50403e+008, 2.50677e+008, 2.50951e+008, 2.51225e+008, 2.51499e+008, 2.51774e+008, 2.52048e+008, 2.52323e+008, 2.52598e+008, 2.52873e+008, 2.53148e+008, 2.53424e+008, 2.53699e+008, 2.53975e+008, 2.54251e+008, 2.54528e+008, 2.54804e+008, 2.55081e+008, 2.55357e+008, 2.55634e+008, 2.55911e+008, 2.56189e+008, 2.56466e+008, 2.56744e+008, 2.57022e+008, 2.573e+008, 2.57578e+008, 2.57856e+008, 2.58135e+008, 2.58413e+008, 2.58692e+008, 2.58971e+008, 2.59251e+008, 2.5953e+008, 2.5981e+008, 2.6009e+008, 2.6037e+008, 2.6065e+008, 2.6093e+008, 2.61211e+008, 2.61491e+008, 2.61772e+008, 2.62053e+008, 2.62335e+008, 2.62616e+008, 2.62898e+008, 2.6318e+008, 2.63462e+008, 2.63744e+008, 2.64026e+008, 2.64309e+008, 2.64591e+008, 2.64874e+008, 2.65157e+008, 2.65441e+008, 2.65724e+008, 2.66008e+008, 2.66291e+008, 2.66575e+008, 2.6686e+008, 2.67144e+008, 2.67428e+008, 2.67713e+008, 2.67998e+008, 2.68283e+008, 2.68568e+008, 2.68854e+008, 2.69139e+008, 2.69425e+008, 2.69711e+008, 2.69997e+008, 2.70284e+008, 2.7057e+008, 2.70857e+008, 2.71144e+008, 2.71431e+008, 2.71718e+008, 2.72006e+008, 2.72293e+008, 2.72581e+008, 2.72869e+008, 2.73157e+008, 2.73446e+008, 2.73734e+008, 2.74023e+008, 2.74312e+008, 2.74601e+008, 2.7489e+008, 2.7518e+008, 2.75469e+008, 2.75759e+008, 2.76049e+008, 2.76339e+008, 2.76629e+008, 2.7692e+008, 2.77211e+008, 2.77502e+008, 2.77793e+008, 2.78084e+008, 2.78376e+008, 2.78667e+008, 2.78959e+008, 2.79251e+008, 2.79543e+008, 2.79836e+008, 2.80128e+008, 2.80421e+008, 2.80714e+008, 2.81007e+008, 2.813e+008, 2.81594e+008, 2.81887e+008, 2.82181e+008, 2.82475e+008, 2.82769e+008, 2.83064e+008, 2.83358e+008, 2.83653e+008, 2.83948e+008, 2.84243e+008, 2.84539e+008, 2.84834e+008, 2.8513e+008, 2.85426e+008, 2.85722e+008, 2.86018e+008, 2.86314e+008, 2.86611e+008, 2.86908e+008, 2.87205e+008, 2.87502e+008, 2.87799e+008, 2.88097e+008, 2.88394e+008, 2.88692e+008, 2.8899e+008, 2.89289e+008, 2.89587e+008, 2.89886e+008, 2.90185e+008, 2.90484e+008, 2.90783e+008, 2.91082e+008, 2.91382e+008, 2.91682e+008, 2.91981e+008, 2.92282e+008, 2.92582e+008, 2.92882e+008, 2.93183e+008, 2.93484e+008, 2.93785e+008, 2.94086e+008, 2.94388e+008, 2.94689e+008, 2.94991e+008, 2.95293e+008, 2.95595e+008, 2.95897e+008, 2.962e+008, 2.96503e+008, 2.96806e+008, 2.97109e+008, 2.97412e+008, 2.97715e+008, 2.98019e+008, 2.98323e+008, 2.98627e+008, 2.98931e+008, 2.99235e+008, 2.9954e+008, 2.99845e+008, 3.0015e+008, 3.00455e+008, 3.0076e+008, 3.01066e+008, 3.01371e+008, 3.01677e+008, 3.01983e+008, 3.02289e+008, 3.02596e+008, 3.02902e+008, 3.03209e+008, 3.03516e+008, 3.03823e+008, 3.04131e+008, 3.04438e+008, 3.04746e+008, 3.05054e+008, 3.05362e+008, 3.0567e+008, 3.05979e+008, 3.06287e+008, 3.06596e+008, 3.06905e+008, 3.07215e+008, 3.07524e+008, 3.07834e+008, 3.08143e+008, 3.08453e+008, 3.08763e+008, 3.09074e+008, 3.09384e+008, 3.09695e+008, 3.10006e+008, 3.10317e+008, 3.10628e+008, 3.1094e+008, 3.11251e+008, 3.11563e+008, 3.11875e+008, 3.12188e+008, 3.125e+008, 3.12813e+008, 3.13125e+008, 3.13438e+008, 3.13752e+008, 3.14065e+008, 3.14378e+008, 3.14692e+008, 3.15006e+008, 3.1532e+008, 3.15634e+008, 3.15949e+008, 3.16264e+008, 3.16578e+008, 3.16893e+008, 3.17209e+008, 3.17524e+008, 3.1784e+008, 3.18155e+008, 3.18471e+008, 3.18788e+008, 3.19104e+008, 3.1942e+008, 3.19737e+008, 3.20054e+008, 3.20371e+008, 3.20688e+008, 3.21006e+008, 3.21324e+008, 3.21641e+008, 3.2196e+008, 3.22278e+008, 3.22596e+008, 3.22915e+008, 3.23234e+008, 3.23553e+008, 3.23872e+008, 3.24191e+008, 3.24511e+008, 3.2483e+008, 3.2515e+008, 3.25471e+008, 3.25791e+008, 3.26111e+008, 3.26432e+008, 3.26753e+008, 3.27074e+008, 3.27395e+008, 3.27717e+008, 3.28038e+008, 3.2836e+008, 3.28682e+008, 3.29004e+008, 3.29327e+008, 3.29649e+008, 3.29972e+008, 3.30295e+008, 3.30618e+008, 3.30942e+008, 3.31265e+008, 3.31589e+008, 3.31913e+008, 3.32237e+008, 3.32561e+008, 3.32886e+008, 3.3321e+008, 3.33535e+008, 3.3386e+008, 3.34185e+008, 3.34511e+008, 3.34837e+008, 3.35162e+008, 3.35488e+008, 3.35815e+008, 3.36141e+008, 3.36467e+008, 3.36794e+008, 3.37121e+008, 3.37448e+008, 3.37776e+008, 3.38103e+008, 3.38431e+008, 3.38759e+008, 3.39087e+008, 3.39415e+008, 3.39744e+008, 3.40072e+008, 3.40401e+008, 3.4073e+008, 3.41059e+008, 3.41389e+008, 3.41719e+008, 3.42048e+008, 3.42378e+008, 3.42709e+008, 3.43039e+008, 3.4337e+008, 3.437e+008, 3.44031e+008, 3.44362e+008, 3.44694e+008, 3.45025e+008, 3.45357e+008, 3.45689e+008, 3.46021e+008, 3.46353e+008, 3.46686e+008, 3.47018e+008, 3.47351e+008, 3.47684e+008, 3.48018e+008, 3.48351e+008, 3.48685e+008, 3.49019e+008, 3.49353e+008, 3.49687e+008, 3.50021e+008, 3.50356e+008, 3.50691e+008, 3.51026e+008, 3.51361e+008, 3.51696e+008, 3.52032e+008, 3.52367e+008, 3.52703e+008, 3.53039e+008, 3.53376e+008, 3.53712e+008, 3.54049e+008, 3.54386e+008, 3.54723e+008, 3.5506e+008, 3.55398e+008, 3.55735e+008, 3.56073e+008, 3.56411e+008, 3.5675e+008, 3.57088e+008, 3.57427e+008, 3.57766e+008, 3.58105e+008, 3.58444e+008, 3.58783e+008, 3.59123e+008, 3.59463e+008, 3.59803e+008, 3.60143e+008, 3.60483e+008, 3.60824e+008, 3.61164e+008, 3.61505e+008, 3.61846e+008, 3.62188e+008, 3.62529e+008, 3.62871e+008, 3.63213e+008, 3.63555e+008, 3.63897e+008, 3.6424e+008, 3.64583e+008, 3.64925e+008, 3.65268e+008, 3.65612e+008, 3.65955e+008, 3.66299e+008, 3.66643e+008, 3.66987e+008, 3.67331e+008, 3.67675e+008, 3.6802e+008, 3.68365e+008, 3.6871e+008, 3.69055e+008, 3.694e+008, 3.69746e+008, 3.70092e+008, 3.70438e+008, 3.70784e+008, 3.7113e+008, 3.71477e+008, 3.71824e+008, 3.72171e+008, 3.72518e+008, 3.72865e+008, 3.73213e+008, 3.7356e+008, 3.73908e+008, 3.74256e+008, 3.74605e+008, 3.74953e+008, 3.75302e+008, 3.75651e+008, 3.76e+008, 3.76349e+008, 3.76699e+008, 3.77048e+008, 3.77398e+008, 3.77748e+008, 3.78098e+008, 3.78449e+008, 3.788e+008, 3.7915e+008, 3.79501e+008, 3.79853e+008, 3.80204e+008, 3.80556e+008, 3.80907e+008, 3.81259e+008, 3.81612e+008, 3.81964e+008, 3.82317e+008, 3.82669e+008, 3.83022e+008, 3.83376e+008, 3.83729e+008, 3.84083e+008, 3.84436e+008, 3.8479e+008, 3.85144e+008, 3.85499e+008, 3.85853e+008, 3.86208e+008, 3.86563e+008, 3.86918e+008, 3.87273e+008, 3.87629e+008, 3.87985e+008, 3.88341e+008, 3.88697e+008, 3.89053e+008, 3.8941e+008, 3.89766e+008, 3.90123e+008, 3.9048e+008, 3.90838e+008, 3.91195e+008, 3.91553e+008, 3.91911e+008, 3.92269e+008, 3.92627e+008, 3.92985e+008, 3.93344e+008, 3.93703e+008, 3.94062e+008, 3.94421e+008, 3.94781e+008, 3.9514e+008, 3.955e+008, 3.9586e+008, 3.9622e+008, 3.96581e+008, 3.96941e+008, 3.97302e+008, 3.97663e+008, 3.98025e+008, 3.98386e+008, 3.98748e+008, 3.99109e+008, 3.99471e+008, 3.99834e+008, 4.00196e+008, 4.00559e+008, 4.00921e+008, 4.01284e+008, 4.01648e+008, 4.02011e+008, 4.02374e+008, 4.02738e+008, 4.03102e+008, 4.03466e+008, 4.03831e+008, 4.04195e+008, 4.0456e+008, 4.04925e+008, 4.0529e+008, 4.05656e+008, 4.06021e+008, 4.06387e+008, 4.06753e+008, 4.07119e+008, 4.07485e+008, 4.07852e+008, 4.08219e+008, 4.08586e+008, 4.08953e+008, 4.0932e+008, 4.09688e+008, 4.10055e+008, 4.10423e+008, 4.10791e+008, 4.1116e+008, 4.11528e+008, 4.11897e+008, 4.12266e+008, 4.12635e+008, 4.13004e+008, 4.13374e+008, 4.13743e+008, 4.14113e+008, 4.14483e+008, 4.14854e+008, 4.15224e+008, 4.15595e+008, 4.15966e+008, 4.16337e+008, 4.16708e+008, 4.1708e+008, 4.17452e+008, 4.17823e+008, 4.18195e+008, 4.18568e+008, 4.1894e+008, 4.19313e+008, 4.19686e+008, 4.20059e+008, 4.20432e+008, 4.20806e+008, 4.21179e+008, 4.21553e+008, 4.21927e+008, 4.22302e+008, 4.22676e+008, 4.23051e+008, 4.23426e+008, 4.23801e+008, 4.24176e+008, 4.24552e+008, 4.24927e+008, 4.25303e+008, 4.25679e+008, 4.26055e+008, 4.26432e+008, 4.26809e+008, 4.27185e+008, 4.27563e+008, 4.2794e+008, 4.28317e+008, 4.28695e+008, 4.29073e+008, 4.29451e+008, 4.29829e+008, 4.30208e+008, 4.30586e+008, 4.30965e+008, 4.31344e+008, 4.31723e+008, 4.32103e+008, 4.32483e+008, 4.32862e+008, 4.33243e+008, 4.33623e+008, 4.34003e+008, 4.34384e+008, 4.34765e+008, 4.35146e+008, 4.35527e+008, 4.35909e+008, 4.3629e+008, 4.36672e+008, 4.37054e+008, 4.37437e+008, 4.37819e+008, 4.38202e+008, 4.38585e+008, 4.38968e+008, 4.39351e+008, 4.39734e+008, 4.40118e+008, 4.40502e+008, 4.40886e+008, 4.4127e+008, 4.41655e+008, 4.4204e+008, 4.42424e+008, 4.42809e+008, 4.43195e+008, 4.4358e+008, 4.43966e+008, 4.44352e+008, 4.44738e+008, 4.45124e+008, 4.45511e+008, 4.45897e+008, 4.46284e+008, 4.46671e+008, 4.47059e+008, 4.47446e+008, 4.47834e+008, 4.48222e+008, 4.4861e+008, 4.48998e+008, 4.49387e+008, 4.49775e+008, 4.50164e+008, 4.50553e+008, 4.50943e+008, 4.51332e+008, 4.51722e+008, 4.52112e+008, 4.52502e+008, 4.52892e+008, 4.53283e+008, 4.53674e+008, 4.54065e+008, 4.54456e+008, 4.54847e+008, 4.55239e+008, 4.5563e+008, 4.56022e+008, 4.56414e+008, 4.56807e+008, 4.57199e+008, 4.57592e+008, 4.57985e+008, 4.58378e+008, 4.58771e+008, 4.59165e+008, 4.59559e+008, 4.59953e+008, 4.60347e+008, 4.60741e+008, 4.61136e+008, 4.61531e+008, 4.61926e+008, 4.62321e+008, 4.62716e+008, 4.63112e+008, 4.63508e+008, 4.63904e+008, 4.643e+008, 4.64696e+008, 4.65093e+008, 4.6549e+008, 4.65887e+008, 4.66284e+008, 4.66681e+008, 4.67079e+008, 4.67477e+008, 4.67875e+008, 4.68273e+008, 4.68671e+008, 4.6907e+008, 4.69469e+008, 4.69868e+008, 4.70267e+008, 4.70666e+008, 4.71066e+008, 4.71466e+008, 4.71866e+008, 4.72266e+008, 4.72667e+008, 4.73067e+008, 4.73468e+008, 4.73869e+008, 4.74271e+008, 4.74672e+008, 4.75074e+008, 4.75476e+008, 4.75878e+008, 4.7628e+008, 4.76683e+008, 4.77085e+008, 4.77488e+008, 4.77891e+008, 4.78295e+008, 4.78698e+008, 4.79102e+008, 4.79506e+008, 4.7991e+008, 4.80314e+008, 4.80719e+008, 4.81124e+008, 4.81529e+008, 4.81934e+008, 4.82339e+008, 4.82745e+008, 4.8315e+008, 4.83556e+008, 4.83963e+008, 4.84369e+008, 4.84775e+008, 4.85182e+008, 4.85589e+008, 4.85996e+008, 4.86404e+008, 4.86812e+008, 4.87219e+008, 4.87627e+008, 4.88036e+008, 4.88444e+008, 4.88853e+008, 4.89261e+008, 4.89671e+008, 4.9008e+008, 4.90489e+008, 4.90899e+008, 4.91309e+008, 4.91719e+008, 4.92129e+008, 4.9254e+008, 4.9295e+008, 4.93361e+008, 4.93772e+008, 4.94184e+008, 4.94595e+008, 4.95007e+008, 4.95419e+008, 4.95831e+008, 4.96243e+008, 4.96656e+008, 4.97068e+008, 4.97481e+008, 4.97895e+008, 4.98308e+008, 4.98722e+008, 4.99135e+008, 4.99549e+008, 4.99963e+008, 5.00378e+008, 5.00792e+008, 5.01207e+008, 5.01622e+008, 5.02037e+008, 5.02453e+008, 5.02869e+008, 5.03284e+008, 5.037e+008, 5.04117e+008, 5.04533e+008, 5.0495e+008, 5.05367e+008, 5.05784e+008, 5.06201e+008, 5.06618e+008, 5.07036e+008, 5.07454e+008, 5.07872e+008, 5.0829e+008, 5.08709e+008, 5.09128e+008, 5.09547e+008, 5.09966e+008, 5.10385e+008, 5.10805e+008, 5.11224e+008, 5.11644e+008, 5.12065e+008, 5.12485e+008, 5.12906e+008, 5.13326e+008, 5.13747e+008, 5.14169e+008, 5.1459e+008, 5.15012e+008, 5.15433e+008, 5.15855e+008, 5.16278e+008, 5.167e+008, 5.17123e+008, 5.17546e+008, 5.17969e+008, 5.18392e+008, 5.18816e+008, 5.19239e+008, 5.19663e+008, 5.20087e+008, 5.20512e+008, 5.20936e+008, 5.21361e+008, 5.21786e+008, 5.22211e+008, 5.22636e+008, 5.23062e+008, 5.23488e+008, 5.23914e+008, 5.2434e+008, 5.24766e+008, 5.25193e+008, 5.2562e+008, 5.26047e+008, 5.26474e+008, 5.26901e+008, 5.27329e+008, 5.27757e+008, 5.28185e+008, 5.28613e+008, 5.29042e+008, 5.2947e+008, 5.29899e+008, 5.30328e+008, 5.30758e+008, 5.31187e+008, 5.31617e+008, 5.32047e+008, 5.32477e+008, 5.32907e+008, 5.33338e+008, 5.33769e+008, 5.342e+008, 5.34631e+008, 5.35062e+008, 5.35494e+008, 5.35926e+008, 5.36358e+008, 5.3679e+008, 5.37222e+008, 5.37655e+008, 5.38088e+008, 5.38521e+008, 5.38954e+008, 5.39388e+008, 5.39821e+008, 5.40255e+008, 5.40689e+008, 5.41124e+008, 5.41558e+008, 5.41993e+008, 5.42428e+008, 5.42863e+008, 5.43298e+008, 5.43734e+008, 5.4417e+008, 5.44606e+008, 5.45042e+008, 5.45478e+008, 5.45915e+008, 5.46352e+008, 5.46789e+008, 5.47226e+008, 5.47664e+008, 5.48101e+008, 5.48539e+008, 5.48977e+008, 5.49416e+008, 5.49854e+008, 5.50293e+008, 5.50732e+008, 5.51171e+008, 5.5161e+008, 5.5205e+008, 5.5249e+008, 5.5293e+008, 5.5337e+008, 5.5381e+008, 5.54251e+008, 5.54692e+008, 5.55133e+008, 5.55574e+008, 5.56015e+008, 5.56457e+008, 5.56899e+008, 5.57341e+008, 5.57783e+008, 5.58226e+008, 5.58669e+008, 5.59111e+008, 5.59555e+008, 5.59998e+008, 5.60442e+008, 5.60885e+008, 5.61329e+008, 5.61773e+008, 5.62218e+008, 5.62663e+008, 5.63107e+008, 5.63552e+008, 5.63998e+008, 5.64443e+008, 5.64889e+008, 5.65335e+008, 5.65781e+008, 5.66227e+008, 5.66674e+008, 5.6712e+008, 5.67567e+008, 5.68014e+008, 5.68462e+008, 5.68909e+008, 5.69357e+008, 5.69805e+008, 5.70253e+008, 5.70702e+008, 5.7115e+008, 5.71599e+008, 5.72048e+008, 5.72497e+008, 5.72947e+008, 5.73397e+008, 5.73847e+008, 5.74297e+008, 5.74747e+008, 5.75198e+008, 5.75648e+008, 5.76099e+008, 5.7655e+008, 5.77002e+008, 5.77453e+008, 5.77905e+008, 5.78357e+008, 5.78809e+008, 5.79262e+008, 5.79715e+008, 5.80167e+008, 5.8062e+008, 5.81074e+008, 5.81527e+008, 5.81981e+008, 5.82435e+008, 5.82889e+008, 5.83343e+008, 5.83798e+008, 5.84253e+008, 5.84708e+008, 5.85163e+008, 5.85618e+008, 5.86074e+008, 5.8653e+008, 5.86986e+008, 5.87442e+008, 5.87899e+008, 5.88355e+008, 5.88812e+008, 5.89269e+008, 5.89727e+008, 5.90184e+008, 5.90642e+008, 5.911e+008, 5.91558e+008, 5.92017e+008, 5.92475e+008, 5.92934e+008, 5.93393e+008, 5.93852e+008, 5.94312e+008, 5.94771e+008, 5.95231e+008, 5.95691e+008, 5.96152e+008, 5.96612e+008, 5.97073e+008, 5.97534e+008, 5.97995e+008, 5.98456e+008, 5.98918e+008, 5.9938e+008, 5.99842e+008, 6.00304e+008, 6.00767e+008, 6.01229e+008, 6.01692e+008, 6.02155e+008, 6.02618e+008, 6.03082e+008, 6.03546e+008, 6.0401e+008, 6.04474e+008, 6.04938e+008, 6.05403e+008, 6.05868e+008, 6.06333e+008, 6.06798e+008, 6.07263e+008, 6.07729e+008, 6.08195e+008, 6.08661e+008, 6.09127e+008, 6.09594e+008, 6.1006e+008, 6.10527e+008, 6.10995e+008, 6.11462e+008, 6.11929e+008, 6.12397e+008, 6.12865e+008, 6.13334e+008, 6.13802e+008, 6.14271e+008, 6.14739e+008, 6.15209e+008, 6.15678e+008, 6.16147e+008, 6.16617e+008, 6.17087e+008, 6.17557e+008, 6.18028e+008, 6.18498e+008, 6.18969e+008, 6.1944e+008, 6.19911e+008, 6.20383e+008, 6.20854e+008, 6.21326e+008, 6.21798e+008, 6.22271e+008, 6.22743e+008, 6.23216e+008, 6.23689e+008, 6.24162e+008, 6.24635e+008, 6.25109e+008, 6.25583e+008, 6.26057e+008, 6.26531e+008, 6.27006e+008, 6.2748e+008, 6.27955e+008, 6.2843e+008, 6.28906e+008, 6.29381e+008, 6.29857e+008, 6.30333e+008, 6.30809e+008, 6.31286e+008, 6.31762e+008, 6.32239e+008, 6.32716e+008, 6.33193e+008, 6.33671e+008, 6.34148e+008, 6.34626e+008, 6.35105e+008, 6.35583e+008, 6.36061e+008, 6.3654e+008, 6.37019e+008, 6.37498e+008, 6.37978e+008, 6.38458e+008, 6.38937e+008, 6.39417e+008, 6.39898e+008, 6.40378e+008, 6.40859e+008, 6.4134e+008, 6.41821e+008, 6.42302e+008, 6.42784e+008, 6.43266e+008, 6.43748e+008, 6.4423e+008, 6.44713e+008, 6.45195e+008, 6.45678e+008, 6.46161e+008, 6.46645e+008, 6.47128e+008, 6.47612e+008, 6.48096e+008, 6.4858e+008, 6.49065e+008, 6.49549e+008, 6.50034e+008, 6.50519e+008, 6.51004e+008, 6.5149e+008, 6.51976e+008, 6.52461e+008, 6.52948e+008, 6.53434e+008, 6.53921e+008, 6.54407e+008, 6.54894e+008, 6.55382e+008, 6.55869e+008, 6.56357e+008, 6.56845e+008, 6.57333e+008, 6.57821e+008, 6.5831e+008, 6.58798e+008, 6.59287e+008, 6.59777e+008, 6.60266e+008, 6.60756e+008, 6.61245e+008, 6.61736e+008, 6.62226e+008, 6.62716e+008, 6.63207e+008, 6.63698e+008, 6.64189e+008, 6.6468e+008, 6.65172e+008, 6.65664e+008, 6.66156e+008, 6.66648e+008, 6.67141e+008, 6.67633e+008, 6.68126e+008, 6.68619e+008, 6.69113e+008, 6.69606e+008, 6.701e+008, 6.70594e+008, 6.71088e+008, 6.71583e+008, 6.72077e+008, 6.72572e+008, 6.73067e+008, 6.73563e+008, 6.74058e+008, 6.74554e+008, 6.7505e+008, 6.75546e+008, 6.76042e+008, 6.76539e+008, 6.77036e+008, 6.77533e+008, 6.7803e+008, 6.78528e+008, 6.79025e+008, 6.79523e+008, 6.80021e+008, 6.8052e+008, 6.81018e+008, 6.81517e+008, 6.82016e+008, 6.82515e+008, 6.83015e+008, 6.83515e+008, 6.84014e+008, 6.84515e+008, 6.85015e+008, 6.85515e+008, 6.86016e+008, 6.86517e+008, 6.87018e+008, 6.8752e+008, 6.88022e+008, 6.88523e+008, 6.89026e+008, 6.89528e+008, 6.9003e+008, 6.90533e+008, 6.91036e+008, 6.91539e+008, 6.92043e+008, 6.92546e+008, 6.9305e+008, 6.93554e+008, 6.94059e+008, 6.94563e+008, 6.95068e+008, 6.95573e+008, 6.96078e+008, 6.96583e+008, 6.97089e+008, 6.97595e+008, 6.98101e+008, 6.98607e+008, 6.99114e+008, 6.9962e+008, 7.00127e+008, 7.00634e+008, 7.01142e+008, 7.01649e+008, 7.02157e+008, 7.02665e+008, 7.03173e+008, 7.03682e+008, 7.04191e+008, 7.047e+008, 7.05209e+008, 7.05718e+008, 7.06228e+008, 7.06737e+008, 7.07247e+008, 7.07758e+008, 7.08268e+008, 7.08779e+008, 7.0929e+008, 7.09801e+008, 7.10312e+008, 7.10824e+008, 7.11336e+008, 7.11848e+008, 7.1236e+008, 7.12872e+008, 7.13385e+008, 7.13898e+008, 7.14411e+008, 7.14924e+008, 7.15438e+008, 7.15952e+008, 7.16466e+008, 7.1698e+008, 7.17494e+008, 7.18009e+008, 7.18524e+008, 7.19039e+008, 7.19554e+008, 7.2007e+008, 7.20586e+008, 7.21102e+008, 7.21618e+008, 7.22134e+008, 7.22651e+008, 7.23168e+008, 7.23685e+008, 7.24202e+008, 7.2472e+008, 7.25238e+008, 7.25756e+008, 7.26274e+008, 7.26792e+008, 7.27311e+008, 7.2783e+008, 7.28349e+008, 7.28868e+008, 7.29388e+008, 7.29907e+008, 7.30427e+008, 7.30948e+008, 7.31468e+008, 7.31989e+008, 7.3251e+008, 7.33031e+008, 7.33552e+008, 7.34074e+008, 7.34595e+008, 7.35117e+008, 7.3564e+008, 7.36162e+008, 7.36685e+008, 7.37208e+008, 7.37731e+008, 7.38254e+008, 7.38777e+008, 7.39301e+008, 7.39825e+008, 7.40349e+008, 7.40874e+008, 7.41399e+008, 7.41923e+008, 7.42449e+008, 7.42974e+008, 7.43499e+008, 7.44025e+008, 7.44551e+008, 7.45077e+008, 7.45604e+008, 7.46131e+008, 7.46657e+008, 7.47185e+008, 7.47712e+008, 7.48239e+008, 7.48767e+008, 7.49295e+008, 7.49823e+008, 7.50352e+008, 7.50881e+008, 7.51409e+008, 7.51939e+008, 7.52468e+008, 7.52998e+008, 7.53527e+008, 7.54057e+008, 7.54588e+008, 7.55118e+008, 7.55649e+008, 7.5618e+008, 7.56711e+008, 7.57242e+008, 7.57774e+008, 7.58306e+008, 7.58838e+008, 7.5937e+008, 7.59902e+008, 7.60435e+008, 7.60968e+008, 7.61501e+008, 7.62034e+008, 7.62568e+008, 7.63102e+008, 7.63636e+008, 7.6417e+008, 7.64705e+008, 7.65239e+008, 7.65774e+008, 7.6631e+008, 7.66845e+008, 7.67381e+008, 7.67916e+008, 7.68452e+008, 7.68989e+008, 7.69525e+008, 7.70062e+008, 7.70599e+008, 7.71136e+008, 7.71674e+008, 7.72211e+008, 7.72749e+008, 7.73287e+008, 7.73826e+008, 7.74364e+008, 7.74903e+008, 7.75442e+008, 7.75981e+008, 7.7652e+008, 7.7706e+008, 7.776e+008, 7.7814e+008, 7.7868e+008, 7.79221e+008, 7.79762e+008, 7.80303e+008, 7.80844e+008, 7.81386e+008, 7.81927e+008, 7.82469e+008, 7.83011e+008, 7.83554e+008, 7.84096e+008, 7.84639e+008, 7.85182e+008, 7.85725e+008, 7.86269e+008, 7.86813e+008, 7.87356e+008, 7.87901e+008, 7.88445e+008, 7.8899e+008, 7.89535e+008, 7.9008e+008, 7.90625e+008, 7.9117e+008, 7.91716e+008, 7.92262e+008, 7.92808e+008, 7.93355e+008, 7.93901e+008, 7.94448e+008, 7.94995e+008, 7.95543e+008, 7.9609e+008, 7.96638e+008, 7.97186e+008, 7.97734e+008, 7.98283e+008, 7.98831e+008, 7.9938e+008, 7.99929e+008, 8.00479e+008, 8.01028e+008, 8.01578e+008, 8.02128e+008, 8.02679e+008, 8.03229e+008, 8.0378e+008, 8.04331e+008, 8.04882e+008, 8.05433e+008, 8.05985e+008, 8.06537e+008, 8.07089e+008, 8.07641e+008, 8.08194e+008, 8.08746e+008, 8.09299e+008, 8.09853e+008, 8.10406e+008, 8.1096e+008, 8.11514e+008, 8.12068e+008, 8.12622e+008, 8.13177e+008, 8.13732e+008, 8.14287e+008, 8.14842e+008, 8.15397e+008, 8.15953e+008, 8.16509e+008, 8.17065e+008, 8.17622e+008, 8.18178e+008, 8.18735e+008, 8.19292e+008, 8.19849e+008, 8.20407e+008, 8.20965e+008, 8.21523e+008, 8.22081e+008, 8.22639e+008, 8.23198e+008, 8.23757e+008, 8.24316e+008, 8.24875e+008, 8.25435e+008, 8.25995e+008, 8.26555e+008, 8.27115e+008, 8.27676e+008, 8.28236e+008, 8.28797e+008, 8.29358e+008, 8.2992e+008, 8.30481e+008, 8.31043e+008, 8.31605e+008, 8.32168e+008, 8.3273e+008, 8.33293e+008, 8.33856e+008, 8.34419e+008, 8.34983e+008, 8.35546e+008, 8.3611e+008, 8.36674e+008, 8.37239e+008, 8.37803e+008, 8.38368e+008, 8.38933e+008, 8.39498e+008, 8.40064e+008, 8.4063e+008, 8.41196e+008, 8.41762e+008, 8.42328e+008, 8.42895e+008, 8.43462e+008, 8.44029e+008, 8.44596e+008, 8.45164e+008, 8.45732e+008, 8.463e+008, 8.46868e+008, 8.47436e+008, 8.48005e+008, 8.48574e+008, 8.49143e+008, 8.49713e+008, 8.50282e+008, 8.50852e+008, 8.51422e+008, 8.51993e+008, 8.52563e+008, 8.53134e+008, 8.53705e+008, 8.54276e+008, 8.54848e+008, 8.55419e+008, 8.55991e+008, 8.56563e+008, 8.57136e+008, 8.57708e+008, 8.58281e+008, 8.58854e+008, 8.59427e+008, 8.60001e+008, 8.60575e+008, 8.61149e+008, 8.61723e+008, 8.62297e+008, 8.62872e+008, 8.63447e+008, 8.64022e+008, 8.64597e+008, 8.65173e+008, 8.65749e+008, 8.66325e+008, 8.66901e+008, 8.67478e+008, 8.68054e+008, 8.68631e+008, 8.69209e+008, 8.69786e+008, 8.70364e+008, 8.70942e+008, 8.7152e+008, 8.72098e+008, 8.72677e+008, 8.73256e+008, 8.73835e+008, 8.74414e+008, 8.74993e+008, 8.75573e+008, 8.76153e+008, 8.76733e+008, 8.77314e+008, 8.77894e+008, 8.78475e+008, 8.79056e+008, 8.79638e+008, 8.80219e+008, 8.80801e+008, 8.81383e+008, 8.81965e+008, 8.82548e+008, 8.83131e+008, 8.83714e+008, 8.84297e+008, 8.8488e+008, 8.85464e+008, 8.86048e+008, 8.86632e+008, 8.87216e+008, 8.87801e+008, 8.88386e+008, 8.88971e+008, 8.89556e+008, 8.90141e+008, 8.90727e+008, 8.91313e+008, 8.91899e+008, 8.92486e+008, 8.93072e+008, 8.93659e+008, 8.94246e+008, 8.94834e+008, 8.95421e+008, 8.96009e+008, 8.96597e+008, 8.97185e+008, 8.97774e+008, 8.98363e+008, 8.98952e+008, 8.99541e+008, 9.0013e+008, 9.0072e+008, 9.0131e+008, 9.019e+008, 9.0249e+008, 9.03081e+008, 9.03672e+008, 9.04263e+008, 9.04854e+008, 9.05446e+008, 9.06037e+008, 9.06629e+008, 9.07222e+008, 9.07814e+008, 9.08407e+008, 9.09e+008, 9.09593e+008, 9.10186e+008, 9.1078e+008, 9.11374e+008, 9.11968e+008, 9.12562e+008, 9.13157e+008, 9.13751e+008, 9.14346e+008, 9.14942e+008, 9.15537e+008, 9.16133e+008, 9.16729e+008, 9.17325e+008, 9.17921e+008, 9.18518e+008, 9.19115e+008, 9.19712e+008, 9.20309e+008, 9.20907e+008, 9.21505e+008, 9.22103e+008, 9.22701e+008, 9.23299e+008, 9.23898e+008, 9.24497e+008, 9.25096e+008, 9.25696e+008, 9.26295e+008, 9.26895e+008, 9.27495e+008, 9.28096e+008, 9.28696e+008, 9.29297e+008, 9.29898e+008, 9.305e+008, 9.31101e+008, 9.31703e+008, 9.32305e+008, 9.32907e+008, 9.3351e+008, 9.34112e+008, 9.34715e+008, 9.35318e+008, 9.35922e+008, 9.36525e+008, 9.37129e+008, 9.37733e+008, 9.38338e+008, 9.38942e+008, 9.39547e+008, 9.40152e+008, 9.40757e+008, 9.41363e+008, 9.41968e+008, 9.42574e+008, 9.43181e+008, 9.43787e+008, 9.44394e+008, 9.45001e+008, 9.45608e+008, 9.46215e+008, 9.46823e+008, 9.47431e+008, 9.48039e+008, 9.48647e+008, 9.49255e+008, 9.49864e+008, 9.50473e+008, 9.51082e+008, 9.51692e+008, 9.52302e+008, 9.52912e+008, 9.53522e+008, 9.54132e+008, 9.54743e+008, 9.55354e+008, 9.55965e+008, 9.56576e+008, 9.57188e+008, 9.578e+008, 9.58412e+008, 9.59024e+008, 9.59636e+008, 9.60249e+008, 9.60862e+008, 9.61475e+008, 9.62089e+008, 9.62703e+008, 9.63316e+008, 9.63931e+008, 9.64545e+008, 9.6516e+008, 9.65775e+008, 9.6639e+008, 9.67005e+008, 9.67621e+008, 9.68236e+008, 9.68852e+008, 9.69469e+008, 9.70085e+008, 9.70702e+008, 9.71319e+008, 9.71936e+008, 9.72554e+008, 9.73171e+008, 9.73789e+008, 9.74407e+008, 9.75026e+008, 9.75644e+008, 9.76263e+008, 9.76882e+008, 9.77502e+008, 9.78121e+008, 9.78741e+008, 9.79361e+008, 9.79981e+008, 9.80602e+008, 9.81223e+008, 9.81844e+008, 9.82465e+008, 9.83086e+008, 9.83708e+008, 9.8433e+008, 9.84952e+008, 9.85575e+008, 9.86197e+008, 9.8682e+008, 9.87443e+008, 9.88067e+008, 9.8869e+008, 9.89314e+008, 9.89938e+008, 9.90562e+008, 9.91187e+008, 9.91812e+008, 9.92437e+008, 9.93062e+008, 9.93687e+008, 9.94313e+008, 9.94939e+008, 9.95565e+008, 9.96192e+008, 9.96818e+008, 9.97445e+008, 9.98072e+008, 9.987e+008, 9.99327e+008, 9.99955e+008, 1.00058e+009, 1.00121e+009, 1.00184e+009, 1.00247e+009, 1.0031e+009, 1.00373e+009, 1.00436e+009, 1.00499e+009, 1.00562e+009, 1.00625e+009, 1.00688e+009, 1.00751e+009, 1.00814e+009, 1.00877e+009, 1.0094e+009, 1.01003e+009, 1.01066e+009, 1.0113e+009, 1.01193e+009, 1.01256e+009, 1.01319e+009, 1.01383e+009, 1.01446e+009, 1.01509e+009, 1.01573e+009, 1.01636e+009, 1.017e+009, 1.01763e+009, 1.01827e+009, 1.0189e+009, 1.01954e+009, 1.02017e+009, 1.02081e+009, 1.02144e+009, 1.02208e+009, 1.02271e+009, 1.02335e+009, 1.02399e+009, 1.02463e+009, 1.02526e+009, 1.0259e+009, 1.02654e+009, 1.02718e+009, 1.02781e+009, 1.02845e+009, 1.02909e+009, 1.02973e+009, 1.03037e+009, 1.03101e+009, 1.03165e+009, 1.03229e+009, 1.03293e+009, 1.03357e+009, 1.03421e+009, 1.03485e+009, 1.03549e+009, 1.03613e+009, 1.03678e+009, 1.03742e+009, 1.03806e+009, 1.0387e+009, 1.03934e+009, 1.03999e+009, 1.04063e+009, 1.04127e+009, 1.04192e+009, 1.04256e+009, 1.0432e+009, 1.04385e+009, 1.04449e+009, 1.04514e+009, 1.04578e+009, 1.04643e+009, 1.04707e+009, 1.04772e+009, 1.04837e+009, 1.04901e+009, 1.04966e+009, 1.0503e+009, 1.05095e+009, 1.0516e+009, 1.05225e+009, 1.05289e+009, 1.05354e+009, 1.05419e+009, 1.05484e+009, 1.05549e+009, 1.05613e+009, 1.05678e+009, 1.05743e+009, 1.05808e+009, 1.05873e+009, 1.05938e+009, 1.06003e+009, 1.06068e+009, 1.06133e+009, 1.06198e+009, 1.06264e+009, 1.06329e+009, 1.06394e+009, 1.06459e+009, 1.06524e+009, 1.06589e+009, 1.06655e+009, 1.0672e+009, 1.06785e+009, 1.06851e+009, 1.06916e+009, 1.06981e+009, 1.07047e+009, 1.07112e+009, 1.07178e+009, 1.07243e+009, 1.07309e+009, 1.07374e+009, 1.0744e+009, 1.07505e+009, 1.07571e+009, 1.07637e+009, 1.07702e+009, 1.07768e+009, 1.07834e+009, 1.07899e+009, 1.07965e+009, 1.08031e+009, 1.08097e+009, 1.08162e+009, 1.08228e+009, 1.08294e+009, 1.0836e+009, 1.08426e+009, 1.08492e+009, 1.08558e+009, 1.08624e+009, 1.0869e+009, 1.08756e+009, 1.08822e+009, 1.08888e+009, 1.08954e+009, 1.0902e+009, 1.09086e+009, 1.09152e+009, 1.09219e+009, 1.09285e+009, 1.09351e+009, 1.09417e+009, 1.09484e+009, 1.0955e+009, 1.09616e+009, 1.09683e+009, 1.09749e+009, 1.09815e+009, 1.09882e+009, 1.09948e+009, 1.10015e+009, 1.10081e+009, 1.10148e+009, 1.10214e+009, 1.10281e+009, 1.10348e+009, 1.10414e+009, 1.10481e+009, 1.10548e+009, 1.10614e+009, 1.10681e+009, 1.10748e+009, 1.10815e+009, 1.10881e+009, 1.10948e+009, 1.11015e+009, 1.11082e+009, 1.11149e+009, 1.11216e+009, 1.11283e+009, 1.1135e+009, 1.11417e+009, 1.11484e+009, 1.11551e+009, 1.11618e+009, 1.11685e+009, 1.11752e+009, 1.11819e+009, 1.11886e+009, 1.11953e+009, 1.12021e+009, 1.12088e+009, 1.12155e+009, 1.12222e+009, 1.1229e+009, 1.12357e+009, 1.12424e+009, 1.12492e+009, 1.12559e+009, 1.12627e+009, 1.12694e+009, 1.12762e+009, 1.12829e+009, 1.12897e+009, 1.12964e+009, 1.13032e+009, 1.13099e+009, 1.13167e+009, 1.13235e+009, 1.13302e+009, 1.1337e+009, 1.13438e+009, 1.13505e+009, 1.13573e+009, 1.13641e+009, 1.13709e+009, 1.13777e+009, 1.13845e+009, 1.13912e+009, 1.1398e+009, 1.14048e+009, 1.14116e+009, 1.14184e+009, 1.14252e+009, 1.1432e+009, 1.14388e+009, 1.14456e+009, 1.14525e+009, 1.14593e+009, 1.14661e+009, 1.14729e+009, 1.14797e+009, 1.14865e+009, 1.14934e+009, 1.15002e+009, 1.1507e+009, 1.15139e+009, 1.15207e+009, 1.15275e+009, 1.15344e+009, 1.15412e+009, 1.15481e+009, 1.15549e+009, 1.15618e+009, 1.15686e+009, 1.15755e+009, 1.15823e+009, 1.15892e+009, 1.1596e+009, 1.16029e+009, 1.16098e+009, 1.16166e+009, 1.16235e+009, 1.16304e+009, 1.16373e+009, 1.16441e+009, 1.1651e+009, 1.16579e+009, 1.16648e+009, 1.16717e+009, 1.16786e+009, 1.16855e+009, 1.16924e+009, 1.16993e+009, 1.17062e+009, 1.17131e+009, 1.172e+009, 1.17269e+009, 1.17338e+009, 1.17407e+009, 1.17476e+009, 1.17545e+009, 1.17615e+009, 1.17684e+009, 1.17753e+009, 1.17822e+009, 1.17892e+009, 1.17961e+009, 1.1803e+009, 1.181e+009, 1.18169e+009, 1.18239e+009, 1.18308e+009, 1.18377e+009, 1.18447e+009, 1.18517e+009, 1.18586e+009, 1.18656e+009, 1.18725e+009, 1.18795e+009, 1.18864e+009, 1.18934e+009, 1.19004e+009, 1.19074e+009, 1.19143e+009, 1.19213e+009, 1.19283e+009, 1.19353e+009, 1.19423e+009, 1.19492e+009, 1.19562e+009, 1.19632e+009, 1.19702e+009, 1.19772e+009, 1.19842e+009, 1.19912e+009, 1.19982e+009, 1.20052e+009, 1.20122e+009, 1.20192e+009, 1.20263e+009, 1.20333e+009, 1.20403e+009, 1.20473e+009, 1.20543e+009, 1.20614e+009, 1.20684e+009, 1.20754e+009, 1.20825e+009, 1.20895e+009, 1.20965e+009, 1.21036e+009, 1.21106e+009, 1.21177e+009, 1.21247e+009, 1.21318e+009, 1.21388e+009, 1.21459e+009, 1.21529e+009, 1.216e+009, 1.2167e+009, 1.21741e+009, 1.21812e+009, 1.21882e+009, 1.21953e+009, 1.22024e+009, 1.22095e+009, 1.22166e+009, 1.22236e+009, 1.22307e+009, 1.22378e+009, 1.22449e+009, 1.2252e+009, 1.22591e+009, 1.22662e+009, 1.22733e+009, 1.22804e+009, 1.22875e+009, 1.22946e+009, 1.23017e+009, 1.23088e+009, 1.23159e+009, 1.2323e+009, 1.23302e+009, 1.23373e+009, 1.23444e+009, 1.23515e+009, 1.23587e+009, 1.23658e+009, 1.23729e+009, 1.23801e+009, 1.23872e+009, 1.23944e+009, 1.24015e+009, 1.24086e+009, 1.24158e+009, 1.24229e+009, 1.24301e+009, 1.24373e+009, 1.24444e+009, 1.24516e+009, 1.24587e+009, 1.24659e+009, 1.24731e+009, 1.24802e+009, 1.24874e+009, 1.24946e+009, 1.25018e+009, 1.2509e+009, 1.25161e+009, 1.25233e+009, 1.25305e+009, 1.25377e+009, 1.25449e+009, 1.25521e+009, 1.25593e+009, 1.25665e+009, 1.25737e+009, 1.25809e+009, 1.25881e+009, 1.25953e+009, 1.26025e+009, 1.26098e+009, 1.2617e+009, 1.26242e+009, 1.26314e+009, 1.26386e+009, 1.26459e+009, 1.26531e+009, 1.26603e+009, 1.26676e+009, 1.26748e+009, 1.2682e+009, 1.26893e+009, 1.26965e+009, 1.27038e+009, 1.2711e+009, 1.27183e+009, 1.27255e+009, 1.27328e+009, 1.27401e+009, 1.27473e+009, 1.27546e+009, 1.27619e+009, 1.27691e+009, 1.27764e+009, 1.27837e+009, 1.2791e+009, 1.27982e+009, 1.28055e+009, 1.28128e+009, 1.28201e+009, 1.28274e+009, 1.28347e+009, 1.2842e+009, 1.28493e+009, 1.28566e+009, 1.28639e+009, 1.28712e+009, 1.28785e+009, 1.28858e+009, 1.28931e+009, 1.29004e+009, 1.29077e+009, 1.29151e+009, 1.29224e+009, 1.29297e+009, 1.2937e+009, 1.29444e+009, 1.29517e+009, 1.2959e+009, 1.29664e+009, 1.29737e+009, 1.29811e+009, 1.29884e+009, 1.29957e+009, 1.30031e+009, 1.30105e+009, 1.30178e+009, 1.30252e+009, 1.30325e+009, 1.30399e+009, 1.30473e+009, 1.30546e+009, 1.3062e+009, 1.30694e+009, 1.30767e+009, 1.30841e+009, 1.30915e+009, 1.30989e+009, 1.31063e+009, 1.31136e+009, 1.3121e+009, 1.31284e+009, 1.31358e+009, 1.31432e+009, 1.31506e+009, 1.3158e+009, 1.31654e+009, 1.31728e+009, 1.31802e+009, 1.31877e+009, 1.31951e+009, 1.32025e+009, 1.32099e+009, 1.32173e+009, 1.32248e+009, 1.32322e+009, 1.32396e+009, 1.32471e+009, 1.32545e+009, 1.32619e+009, 1.32694e+009, 1.32768e+009, 1.32843e+009, 1.32917e+009, 1.32992e+009, 1.33066e+009, 1.33141e+009, 1.33215e+009, 1.3329e+009, 1.33364e+009, 1.33439e+009, 1.33514e+009, 1.33588e+009, 1.33663e+009, 1.33738e+009, 1.33813e+009, 1.33887e+009, 1.33962e+009, 1.34037e+009, 1.34112e+009, 1.34187e+009, 1.34262e+009, 1.34337e+009, 1.34412e+009, 1.34487e+009, 1.34562e+009, 1.34637e+009, 1.34712e+009, 1.34787e+009, 1.34862e+009, 1.34937e+009, 1.35013e+009, 1.35088e+009, 1.35163e+009, 1.35238e+009, 1.35313e+009, 1.35389e+009, 1.35464e+009, 1.35539e+009, 1.35615e+009, 1.3569e+009, 1.35766e+009, 1.35841e+009, 1.35917e+009, 1.35992e+009, 1.36068e+009, 1.36143e+009, 1.36219e+009, 1.36294e+009, 1.3637e+009, 1.36446e+009, 1.36521e+009, 1.36597e+009, 1.36673e+009, 1.36749e+009, 1.36824e+009, 1.369e+009, 1.36976e+009, 1.37052e+009, 1.37128e+009, 1.37204e+009, 1.3728e+009, 1.37356e+009, 1.37431e+009, 1.37508e+009, 1.37584e+009, 1.3766e+009, 1.37736e+009, 1.37812e+009, 1.37888e+009, 1.37964e+009, 1.3804e+009, 1.38116e+009, 1.38193e+009, 1.38269e+009, 1.38345e+009, 1.38422e+009, 1.38498e+009, 1.38574e+009, 1.38651e+009, 1.38727e+009, 1.38804e+009, 1.3888e+009, 1.38956e+009, 1.39033e+009, 1.3911e+009, 1.39186e+009, 1.39263e+009, 1.39339e+009, 1.39416e+009, 1.39493e+009, 1.39569e+009, 1.39646e+009, 1.39723e+009, 1.398e+009, 1.39876e+009, 1.39953e+009, 1.4003e+009, 1.40107e+009, 1.40184e+009, 1.40261e+009, 1.40338e+009, 1.40415e+009, 1.40492e+009, 1.40569e+009, 1.40646e+009, 1.40723e+009, 1.408e+009, 1.40877e+009, 1.40954e+009, 1.41031e+009, 1.41108e+009, 1.41186e+009, 1.41263e+009, 1.4134e+009, 1.41417e+009, 1.41495e+009, 1.41572e+009, 1.41649e+009, 1.41727e+009, 1.41804e+009, 1.41882e+009, 1.41959e+009, 1.42037e+009, 1.42114e+009, 1.42192e+009, 1.42269e+009, 1.42347e+009, 1.42425e+009, 1.42502e+009, 1.4258e+009, 1.42658e+009, 1.42735e+009, 1.42813e+009, 1.42891e+009, 1.42969e+009, 1.43047e+009, 1.43124e+009, 1.43202e+009, 1.4328e+009, 1.43358e+009, 1.43436e+009, 1.43514e+009, 1.43592e+009, 1.4367e+009, 1.43748e+009, 1.43826e+009, 1.43904e+009, 1.43983e+009, 1.44061e+009, 1.44139e+009, 1.44217e+009, 1.44295e+009, 1.44374e+009, 1.44452e+009, 1.4453e+009, 1.44609e+009, 1.44687e+009, 1.44765e+009, 1.44844e+009, 1.44922e+009, 1.45001e+009, 1.45079e+009, 1.45158e+009, 1.45236e+009, 1.45315e+009, 1.45393e+009, 1.45472e+009, 1.45551e+009, 1.45629e+009, 1.45708e+009, 1.45787e+009, 1.45865e+009, 1.45944e+009, 1.46023e+009, 1.46102e+009, 1.46181e+009, 1.4626e+009, 1.46338e+009, 1.46417e+009, 1.46496e+009, 1.46575e+009, 1.46654e+009, 1.46733e+009, 1.46812e+009, 1.46892e+009, 1.46971e+009, 1.4705e+009, 1.47129e+009, 1.47208e+009, 1.47287e+009, 1.47367e+009, 1.47446e+009, 1.47525e+009, 1.47604e+009, 1.47684e+009, 1.47763e+009, 1.47842e+009, 1.47922e+009, 1.48001e+009, 1.48081e+009, 1.4816e+009, 1.4824e+009, 1.48319e+009, 1.48399e+009, 1.48478e+009, 1.48558e+009, 1.48638e+009, 1.48717e+009, 1.48797e+009, 1.48877e+009, 1.48957e+009, 1.49036e+009, 1.49116e+009, 1.49196e+009, 1.49276e+009, 1.49356e+009, 1.49436e+009, 1.49516e+009, 1.49595e+009, 1.49675e+009, 1.49755e+009, 1.49835e+009, 1.49916e+009, 1.49996e+009, 1.50076e+009, 1.50156e+009, 1.50236e+009, 1.50316e+009, 1.50396e+009, 1.50477e+009, 1.50557e+009, 1.50637e+009, 1.50717e+009, 1.50798e+009, 1.50878e+009, 1.50959e+009, 1.51039e+009, 1.51119e+009, 1.512e+009, 1.5128e+009, 1.51361e+009, 1.51441e+009, 1.51522e+009, 1.51603e+009, 1.51683e+009, 1.51764e+009, 1.51845e+009, 1.51925e+009, 1.52006e+009, 1.52087e+009, 1.52167e+009, 1.52248e+009, 1.52329e+009, 1.5241e+009, 1.52491e+009, 1.52572e+009, 1.52653e+009, 1.52734e+009, 1.52815e+009, 1.52896e+009, 1.52977e+009, 1.53058e+009, 1.53139e+009, 1.5322e+009, 1.53301e+009, 1.53382e+009, 1.53463e+009, 1.53545e+009, 1.53626e+009, 1.53707e+009, 1.53788e+009, 1.5387e+009, 1.53951e+009, 1.54032e+009, 1.54114e+009, 1.54195e+009, 1.54277e+009, 1.54358e+009, 1.5444e+009, 1.54521e+009, 1.54603e+009, 1.54684e+009, 1.54766e+009, 1.54847e+009, 1.54929e+009, 1.55011e+009, 1.55092e+009, 1.55174e+009, 1.55256e+009, 1.55338e+009, 1.55419e+009, 1.55501e+009, 1.55583e+009, 1.55665e+009, 1.55747e+009, 1.55829e+009, 1.55911e+009, 1.55993e+009, 1.56075e+009, 1.56157e+009, 1.56239e+009, 1.56321e+009, 1.56403e+009, 1.56485e+009, 1.56567e+009, 1.5665e+009, 1.56732e+009, 1.56814e+009, 1.56896e+009, 1.56979e+009, 1.57061e+009, 1.57143e+009, 1.57226e+009, 1.57308e+009, 1.57391e+009, 1.57473e+009, 1.57555e+009, 1.57638e+009, 1.5772e+009, 1.57803e+009, 1.57886e+009, 1.57968e+009, 1.58051e+009, 1.58134e+009, 1.58216e+009, 1.58299e+009, 1.58382e+009, 1.58464e+009, 1.58547e+009, 1.5863e+009, 1.58713e+009, 1.58796e+009, 1.58879e+009, 1.58962e+009, 1.59044e+009, 1.59127e+009, 1.5921e+009, 1.59293e+009, 1.59377e+009, 1.5946e+009, 1.59543e+009, 1.59626e+009, 1.59709e+009, 1.59792e+009, 1.59875e+009, 1.59959e+009, 1.60042e+009, 1.60125e+009, 1.60208e+009, 1.60292e+009, 1.60375e+009, 1.60458e+009, 1.60542e+009, 1.60625e+009, 1.60709e+009, 1.60792e+009, 1.60876e+009, 1.60959e+009, 1.61043e+009, 1.61127e+009, 1.6121e+009, 1.61294e+009, 1.61377e+009, 1.61461e+009, 1.61545e+009, 1.61629e+009, 1.61712e+009, 1.61796e+009, 1.6188e+009, 1.61964e+009, 1.62048e+009, 1.62132e+009, 1.62216e+009, 1.623e+009, 1.62384e+009, 1.62468e+009, 1.62552e+009, 1.62636e+009, 1.6272e+009, 1.62804e+009, 1.62888e+009, 1.62972e+009, 1.63056e+009, 1.63141e+009, 1.63225e+009, 1.63309e+009, 1.63393e+009, 1.63478e+009, 1.63562e+009, 1.63646e+009, 1.63731e+009, 1.63815e+009, 1.639e+009, 1.63984e+009, 1.64069e+009, 1.64153e+009, 1.64238e+009, 1.64322e+009, 1.64407e+009, 1.64492e+009, 1.64576e+009, 1.64661e+009, 1.64746e+009, 1.6483e+009, 1.64915e+009, 1.65e+009, 1.65085e+009, 1.6517e+009, 1.65255e+009, 1.65339e+009, 1.65424e+009, 1.65509e+009, 1.65594e+009, 1.65679e+009, 1.65764e+009, 1.65849e+009, 1.65934e+009, 1.6602e+009, 1.66105e+009, 1.6619e+009, 1.66275e+009, 1.6636e+009, 1.66445e+009, 1.66531e+009, 1.66616e+009, 1.66701e+009, 1.66787e+009, 1.66872e+009, 1.66957e+009, 1.67043e+009, 1.67128e+009, 1.67214e+009, 1.67299e+009, 1.67385e+009, 1.6747e+009, 1.67556e+009, 1.67642e+009, 1.67727e+009, 1.67813e+009, 1.67899e+009, 1.67984e+009, 1.6807e+009, 1.68156e+009, 1.68242e+009, 1.68327e+009, 1.68413e+009, 1.68499e+009, 1.68585e+009, 1.68671e+009, 1.68757e+009, 1.68843e+009, 1.68929e+009, 1.69015e+009, 1.69101e+009, 1.69187e+009, 1.69273e+009, 1.69359e+009, 1.69445e+009, 1.69532e+009, 1.69618e+009, 1.69704e+009, 1.6979e+009, 1.69877e+009, 1.69963e+009, 1.70049e+009, 1.70136e+009, 1.70222e+009, 1.70308e+009, 1.70395e+009, 1.70481e+009, 1.70568e+009, 1.70654e+009, 1.70741e+009, 1.70827e+009, 1.70914e+009, 1.71001e+009, 1.71087e+009, 1.71174e+009, 1.71261e+009, 1.71347e+009, 1.71434e+009, 1.71521e+009, 1.71608e+009, 1.71695e+009, 1.71782e+009, 1.71868e+009, 1.71955e+009, 1.72042e+009, 1.72129e+009, 1.72216e+009, 1.72303e+009, 1.7239e+009, 1.72477e+009, 1.72565e+009, 1.72652e+009, 1.72739e+009, 1.72826e+009, 1.72913e+009, 1.73e+009, 1.73088e+009, 1.73175e+009, 1.73262e+009, 1.7335e+009, 1.73437e+009, 1.73524e+009, 1.73612e+009, 1.73699e+009, 1.73787e+009, 1.73874e+009, 1.73962e+009, 1.74049e+009, 1.74137e+009, 1.74225e+009, 1.74312e+009, 1.744e+009, 1.74488e+009, 1.74575e+009, 1.74663e+009, 1.74751e+009, 1.74839e+009, 1.74926e+009, 1.75014e+009, 1.75102e+009, 1.7519e+009, 1.75278e+009, 1.75366e+009, 1.75454e+009, 1.75542e+009, 1.7563e+009, 1.75718e+009, 1.75806e+009, 1.75894e+009, 1.75982e+009, 1.7607e+009, 1.76159e+009, 1.76247e+009, 1.76335e+009, 1.76423e+009, 1.76512e+009, 1.766e+009, 1.76688e+009, 1.76777e+009, 1.76865e+009, 1.76954e+009, 1.77042e+009, 1.7713e+009, 1.77219e+009, 1.77308e+009, 1.77396e+009, 1.77485e+009, 1.77573e+009, 1.77662e+009, 1.77751e+009, 1.77839e+009, 1.77928e+009, 1.78017e+009, 1.78106e+009, 1.78194e+009, 1.78283e+009, 1.78372e+009, 1.78461e+009, 1.7855e+009, 1.78639e+009, 1.78728e+009, 1.78817e+009, 1.78906e+009, 1.78995e+009, 1.79084e+009, 1.79173e+009, 1.79262e+009, 1.79351e+009, 1.7944e+009, 1.79529e+009, 1.79619e+009, 1.79708e+009, 1.79797e+009, 1.79887e+009, 1.79976e+009, 1.80065e+009, 1.80155e+009, 1.80244e+009, 1.80333e+009, 1.80423e+009, 1.80512e+009, 1.80602e+009, 1.80691e+009, 1.80781e+009, 1.80871e+009, 1.8096e+009, 1.8105e+009, 1.8114e+009, 1.81229e+009, 1.81319e+009, 1.81409e+009, 1.81499e+009, 1.81588e+009, 1.81678e+009, 1.81768e+009, 1.81858e+009, 1.81948e+009, 1.82038e+009, 1.82128e+009, 1.82218e+009, 1.82308e+009, 1.82398e+009, 1.82488e+009, 1.82578e+009, 1.82668e+009, 1.82758e+009, 1.82849e+009, 1.82939e+009, 1.83029e+009, 1.83119e+009, 1.8321e+009, 1.833e+009, 1.8339e+009, 1.83481e+009, 1.83571e+009, 1.83661e+009, 1.83752e+009, 1.83842e+009, 1.83933e+009, 1.84023e+009, 1.84114e+009, 1.84205e+009, 1.84295e+009, 1.84386e+009, 1.84476e+009, 1.84567e+009, 1.84658e+009, 1.84749e+009, 1.84839e+009, 1.8493e+009, 1.85021e+009, 1.85112e+009, 1.85203e+009, 1.85294e+009, 1.85385e+009, 1.85476e+009, 1.85567e+009, 1.85658e+009, 1.85749e+009, 1.8584e+009, 1.85931e+009, 1.86022e+009, 1.86113e+009, 1.86204e+009, 1.86295e+009, 1.86387e+009, 1.86478e+009, 1.86569e+009, 1.8666e+009, 1.86752e+009, 1.86843e+009, 1.86935e+009, 1.87026e+009, 1.87117e+009, 1.87209e+009, 1.873e+009, 1.87392e+009, 1.87483e+009, 1.87575e+009, 1.87667e+009, 1.87758e+009, 1.8785e+009, 1.87942e+009, 1.88033e+009, 1.88125e+009, 1.88217e+009, 1.88309e+009, 1.884e+009, 1.88492e+009, 1.88584e+009, 1.88676e+009, 1.88768e+009, 1.8886e+009, 1.88952e+009, 1.89044e+009, 1.89136e+009, 1.89228e+009, 1.8932e+009, 1.89412e+009, 1.89504e+009, 1.89596e+009, 1.89689e+009, 1.89781e+009, 1.89873e+009, 1.89965e+009, 1.90058e+009, 1.9015e+009, 1.90242e+009, 1.90335e+009, 1.90427e+009, 1.9052e+009, 1.90612e+009, 1.90705e+009, 1.90797e+009, 1.9089e+009, 1.90982e+009, 1.91075e+009, 1.91167e+009, 1.9126e+009, 1.91353e+009, 1.91445e+009, 1.91538e+009, 1.91631e+009, 1.91724e+009, 1.91817e+009, 1.91909e+009, 1.92002e+009, 1.92095e+009, 1.92188e+009, 1.92281e+009, 1.92374e+009, 1.92467e+009, 1.9256e+009, 1.92653e+009, 1.92746e+009, 1.92839e+009, 1.92932e+009, 1.93026e+009, 1.93119e+009, 1.93212e+009, 1.93305e+009, 1.93398e+009, 1.93492e+009, 1.93585e+009, 1.93678e+009, 1.93772e+009, 1.93865e+009, 1.93959e+009, 1.94052e+009, 1.94146e+009, 1.94239e+009, 1.94333e+009, 1.94426e+009, 1.9452e+009, 1.94613e+009, 1.94707e+009, 1.94801e+009, 1.94894e+009, 1.94988e+009, 1.95082e+009, 1.95176e+009, 1.9527e+009, 1.95363e+009, 1.95457e+009, 1.95551e+009, 1.95645e+009, 1.95739e+009, 1.95833e+009, 1.95927e+009, 1.96021e+009, 1.96115e+009, 1.96209e+009, 1.96303e+009, 1.96397e+009, 1.96492e+009, 1.96586e+009, 1.9668e+009, 1.96774e+009, 1.96869e+009, 1.96963e+009, 1.97057e+009, 1.97151e+009, 1.97246e+009, 1.9734e+009, 1.97435e+009, 1.97529e+009, 1.97624e+009, 1.97718e+009, 1.97813e+009, 1.97907e+009, 1.98002e+009, 1.98097e+009, 1.98191e+009, 1.98286e+009, 1.98381e+009, 1.98475e+009, 1.9857e+009, 1.98665e+009, 1.9876e+009, 1.98854e+009, 1.98949e+009, 1.99044e+009, 1.99139e+009, 1.99234e+009, 1.99329e+009, 1.99424e+009, 1.99519e+009, 1.99614e+009, 1.99709e+009, 1.99804e+009, 1.999e+009, 1.99995e+009, 2.0009e+009, 2.00185e+009, 2.0028e+009, 2.00376e+009, 2.00471e+009, 2.00566e+009, 2.00662e+009, 2.00757e+009, 2.00852e+009, 2.00948e+009, 2.01043e+009, 2.01139e+009, 2.01234e+009, 2.0133e+009, 2.01425e+009, 2.01521e+009, 2.01617e+009, 2.01712e+009, 2.01808e+009, 2.01904e+009, 2.02e+009, 2.02095e+009, 2.02191e+009, 2.02287e+009, 2.02383e+009, 2.02479e+009, 2.02575e+009, 2.0267e+009, 2.02766e+009, 2.02862e+009, 2.02958e+009, 2.03054e+009, 2.03151e+009, 2.03247e+009, 2.03343e+009, 2.03439e+009, 2.03535e+009, 2.03631e+009, 2.03727e+009, 2.03824e+009, 2.0392e+009, 2.04016e+009, 2.04113e+009, 2.04209e+009, 2.04305e+009, 2.04402e+009, 2.04498e+009, 2.04595e+009, 2.04691e+009, 2.04788e+009, 2.04884e+009, 2.04981e+009, 2.05078e+009, 2.05174e+009, 2.05271e+009, 2.05368e+009, 2.05464e+009, 2.05561e+009, 2.05658e+009, 2.05755e+009, 2.05851e+009, 2.05948e+009, 2.06045e+009, 2.06142e+009, 2.06239e+009, 2.06336e+009, 2.06433e+009, 2.0653e+009, 2.06627e+009, 2.06724e+009, 2.06821e+009, 2.06918e+009, 2.07016e+009, 2.07113e+009, 2.0721e+009, 2.07307e+009, 2.07404e+009, 2.07502e+009, 2.07599e+009, 2.07696e+009, 2.07794e+009, 2.07891e+009, 2.07989e+009, 2.08086e+009, 2.08184e+009, 2.08281e+009, 2.08379e+009, 2.08476e+009, 2.08574e+009, 2.08671e+009, 2.08769e+009, 2.08867e+009, 2.08964e+009, 2.09062e+009, 2.0916e+009, 2.09258e+009, 2.09356e+009, 2.09453e+009, 2.09551e+009, 2.09649e+009, 2.09747e+009, 2.09845e+009, 2.09943e+009, 2.10041e+009, 2.10139e+009, 2.10237e+009, 2.10335e+009, 2.10433e+009, 2.10531e+009, 2.1063e+009, 2.10728e+009, 2.10826e+009, 2.10924e+009, 2.11023e+009, 2.11121e+009, 2.11219e+009, 2.11318e+009, 2.11416e+009, 2.11514e+009, 2.11613e+009, 2.11711e+009, 2.1181e+009, 2.11908e+009, 2.12007e+009, 2.12106e+009, 2.12204e+009, 2.12303e+009, 2.12401e+009, 2.125e+009, 2.12599e+009, 2.12698e+009, 2.12796e+009, 2.12895e+009, 2.12994e+009, 2.13093e+009, 2.13192e+009, 2.13291e+009, 2.1339e+009, 2.13489e+009, 2.13588e+009, 2.13687e+009, 2.13786e+009, 2.13885e+009, 2.13984e+009, 2.14083e+009, 2.14182e+009, 2.14281e+009, 2.14381e+009, 2.1448e+009, 2.14579e+009, 2.14678e+009, 2.14778e+009, 2.14877e+009, 2.14976e+009, 2.15076e+009, 2.15175e+009, 2.15275e+009, 2.15374e+009, 2.15474e+009, 2.15573e+009, 2.15673e+009, 2.15773e+009, 2.15872e+009, 2.15972e+009, 2.16072e+009, 2.16171e+009, 2.16271e+009, 2.16371e+009, 2.16471e+009, 2.1657e+009, 2.1667e+009, 2.1677e+009, 2.1687e+009, 2.1697e+009, 2.1707e+009, 2.1717e+009, 2.1727e+009, 2.1737e+009, 2.1747e+009, 2.1757e+009, 2.1767e+009, 2.1777e+009, 2.17871e+009, 2.17971e+009, 2.18071e+009, 2.18171e+009, 2.18272e+009, 2.18372e+009, 2.18472e+009, 2.18573e+009, 2.18673e+009, 2.18773e+009, 2.18874e+009, 2.18974e+009, 2.19075e+009, 2.19176e+009, 2.19276e+009, 2.19377e+009, 2.19477e+009, 2.19578e+009, 2.19679e+009, 2.19779e+009, 2.1988e+009, 2.19981e+009, 2.20082e+009, 2.20182e+009, 2.20283e+009, 2.20384e+009, 2.20485e+009, 2.20586e+009, 2.20687e+009, 2.20788e+009, 2.20889e+009, 2.2099e+009, 2.21091e+009, 2.21192e+009, 2.21293e+009, 2.21394e+009, 2.21496e+009, 2.21597e+009, 2.21698e+009, 2.21799e+009, 2.21901e+009, 2.22002e+009, 2.22103e+009, 2.22205e+009, 2.22306e+009, 2.22408e+009, 2.22509e+009, 2.2261e+009, 2.22712e+009, 2.22814e+009, 2.22915e+009, 2.23017e+009, 2.23118e+009, 2.2322e+009, 2.23322e+009, 2.23423e+009, 2.23525e+009, 2.23627e+009, 2.23729e+009, 2.2383e+009, 2.23932e+009, 2.24034e+009, 2.24136e+009, 2.24238e+009, 2.2434e+009, 2.24442e+009, 2.24544e+009, 2.24646e+009, 2.24748e+009, 2.2485e+009, 2.24952e+009, 2.25054e+009, 2.25157e+009, 2.25259e+009, 2.25361e+009, 2.25463e+009, 2.25566e+009, 2.25668e+009, 2.2577e+009, 2.25873e+009, 2.25975e+009, 2.26078e+009, 2.2618e+009, 2.26283e+009, 2.26385e+009, 2.26488e+009, 2.2659e+009, 2.26693e+009, 2.26795e+009, 2.26898e+009, 2.27001e+009, 2.27103e+009, 2.27206e+009, 2.27309e+009, 2.27412e+009, 2.27515e+009, 2.27617e+009, 2.2772e+009, 2.27823e+009, 2.27926e+009, 2.28029e+009, 2.28132e+009, 2.28235e+009, 2.28338e+009, 2.28441e+009, 2.28544e+009, 2.28647e+009, 2.28751e+009, 2.28854e+009, 2.28957e+009, 2.2906e+009, 2.29163e+009, 2.29267e+009, 2.2937e+009, 2.29473e+009, 2.29577e+009, 2.2968e+009, 2.29784e+009, 2.29887e+009, 2.29991e+009, 2.30094e+009, 2.30198e+009, 2.30301e+009, 2.30405e+009, 2.30509e+009, 2.30612e+009, 2.30716e+009, 2.3082e+009, 2.30923e+009, 2.31027e+009, 2.31131e+009, 2.31235e+009, 2.31339e+009, 2.31442e+009, 2.31546e+009, 2.3165e+009, 2.31754e+009, 2.31858e+009, 2.31962e+009, 2.32066e+009, 2.3217e+009, 2.32275e+009, 2.32379e+009, 2.32483e+009, 2.32587e+009, 2.32691e+009, 2.32795e+009, 2.329e+009, 2.33004e+009, 2.33108e+009, 2.33213e+009, 2.33317e+009, 2.33422e+009, 2.33526e+009, 2.3363e+009, 2.33735e+009, 2.3384e+009, 2.33944e+009, 2.34049e+009, 2.34153e+009, 2.34258e+009, 2.34363e+009, 2.34467e+009, 2.34572e+009, 2.34677e+009, 2.34782e+009, 2.34886e+009, 2.34991e+009, 2.35096e+009, 2.35201e+009, 2.35306e+009, 2.35411e+009, 2.35516e+009, 2.35621e+009, 2.35726e+009, 2.35831e+009, 2.35936e+009, 2.36041e+009, 2.36146e+009, 2.36251e+009, 2.36357e+009, 2.36462e+009, 2.36567e+009, 2.36672e+009, 2.36778e+009, 2.36883e+009, 2.36988e+009, 2.37094e+009, 2.37199e+009, 2.37305e+009, 2.3741e+009, 2.37516e+009, 2.37621e+009, 2.37727e+009, 2.37832e+009, 2.37938e+009, 2.38044e+009, 2.38149e+009, 2.38255e+009, 2.38361e+009, 2.38467e+009, 2.38572e+009, 2.38678e+009, 2.38784e+009, 2.3889e+009, 2.38996e+009, 2.39102e+009, 2.39208e+009, 2.39314e+009, 2.3942e+009, 2.39526e+009, 2.39632e+009, 2.39738e+009, 2.39844e+009, 2.3995e+009, 2.40056e+009, 2.40163e+009, 2.40269e+009, 2.40375e+009, 2.40481e+009, 2.40588e+009, 2.40694e+009, 2.408e+009, 2.40907e+009, 2.41013e+009, 2.4112e+009, 2.41226e+009, 2.41333e+009, 2.41439e+009, 2.41546e+009, 2.41653e+009, 2.41759e+009, 2.41866e+009, 2.41973e+009, 2.42079e+009, 2.42186e+009, 2.42293e+009, 2.424e+009, 2.42506e+009, 2.42613e+009, 2.4272e+009, 2.42827e+009, 2.42934e+009, 2.43041e+009, 2.43148e+009, 2.43255e+009, 2.43362e+009, 2.43469e+009, 2.43576e+009, 2.43684e+009, 2.43791e+009, 2.43898e+009, 2.44005e+009, 2.44112e+009, 2.4422e+009, 2.44327e+009, 2.44434e+009, 2.44542e+009, 2.44649e+009, 2.44757e+009, 2.44864e+009, 2.44971e+009, 2.45079e+009, 2.45187e+009, 2.45294e+009, 2.45402e+009, 2.45509e+009, 2.45617e+009, 2.45725e+009, 2.45832e+009, 2.4594e+009, 2.46048e+009, 2.46156e+009, 2.46264e+009, 2.46371e+009, 2.46479e+009, 2.46587e+009, 2.46695e+009, 2.46803e+009, 2.46911e+009, 2.47019e+009, 2.47127e+009, 2.47235e+009, 2.47343e+009, 2.47451e+009, 2.4756e+009, 2.47668e+009, 2.47776e+009, 2.47884e+009, 2.47993e+009, 2.48101e+009, 2.48209e+009, 2.48318e+009, 2.48426e+009, 2.48534e+009, 2.48643e+009, 2.48751e+009, 2.4886e+009, 2.48968e+009, 2.49077e+009, 2.49186e+009, 2.49294e+009, 2.49403e+009, 2.49511e+009, 2.4962e+009, 2.49729e+009, 2.49838e+009, 2.49946e+009, 2.50055e+009, 2.50164e+009, 2.50273e+009, 2.50382e+009, 2.50491e+009, 2.506e+009, 2.50709e+009, 2.50818e+009, 2.50927e+009, 2.51036e+009, 2.51145e+009, 2.51254e+009, 2.51363e+009, 2.51473e+009, 2.51582e+009, 2.51691e+009, 2.518e+009, 2.5191e+009, 2.52019e+009, 2.52128e+009, 2.52238e+009, 2.52347e+009, 2.52457e+009, 2.52566e+009, 2.52675e+009, 2.52785e+009, 2.52895e+009, 2.53004e+009, 2.53114e+009, 2.53223e+009, 2.53333e+009, 2.53443e+009, 2.53553e+009, 2.53662e+009, 2.53772e+009, 2.53882e+009, 2.53992e+009, 2.54102e+009, 2.54212e+009, 2.54321e+009, 2.54431e+009, 2.54541e+009, 2.54651e+009, 2.54761e+009, 2.54872e+009, 2.54982e+009, 2.55092e+009, 2.55202e+009, 2.55312e+009, 2.55422e+009, 2.55533e+009, 2.55643e+009, 2.55753e+009, 2.55863e+009, 2.55974e+009, 2.56084e+009, 2.56195e+009, 2.56305e+009, 2.56416e+009, 2.56526e+009, 2.56637e+009, 2.56747e+009, 2.56858e+009, 2.56968e+009, 2.57079e+009, 2.5719e+009, 2.573e+009, 2.57411e+009, 2.57522e+009, 2.57633e+009, 2.57743e+009, 2.57854e+009, 2.57965e+009, 2.58076e+009, 2.58187e+009, 2.58298e+009, 2.58409e+009, 2.5852e+009, 2.58631e+009, 2.58742e+009, 2.58853e+009, 2.58964e+009, 2.59075e+009, 2.59187e+009, 2.59298e+009, 2.59409e+009, 2.5952e+009, 2.59632e+009, 2.59743e+009, 2.59854e+009, 2.59966e+009, 2.60077e+009, 2.60189e+009, 2.603e+009, 2.60412e+009, 2.60523e+009, 2.60635e+009, 2.60746e+009, 2.60858e+009, 2.60969e+009, 2.61081e+009, 2.61193e+009, 2.61305e+009, 2.61416e+009, 2.61528e+009, 2.6164e+009, 2.61752e+009, 2.61864e+009, 2.61976e+009, 2.62088e+009, 2.62199e+009, 2.62311e+009, 2.62423e+009, 2.62536e+009, 2.62648e+009, 2.6276e+009, 2.62872e+009, 2.62984e+009, 2.63096e+009, 2.63208e+009, 2.63321e+009, 2.63433e+009, 2.63545e+009, 2.63658e+009, 2.6377e+009, 2.63882e+009, 2.63995e+009, 2.64107e+009, 2.6422e+009, 2.64332e+009, 2.64445e+009, 2.64557e+009, 2.6467e+009, 2.64782e+009, 2.64895e+009, 2.65008e+009, 2.65121e+009, 2.65233e+009, 2.65346e+009, 2.65459e+009, 2.65572e+009, 2.65684e+009, 2.65797e+009, 2.6591e+009, 2.66023e+009, 2.66136e+009, 2.66249e+009, 2.66362e+009, 2.66475e+009, 2.66588e+009, 2.66701e+009, 2.66815e+009, 2.66928e+009, 2.67041e+009, 2.67154e+009, 2.67267e+009, 2.67381e+009, 2.67494e+009, 2.67607e+009, 2.67721e+009, 2.67834e+009, 2.67948e+009, 2.68061e+009, 2.68174e+009, 2.68288e+009, 2.68402e+009, 2.68515e+009, 2.68629e+009, 2.68742e+009, 2.68856e+009, 2.6897e+009, 2.69083e+009, 2.69197e+009, 2.69311e+009, 2.69425e+009, 2.69539e+009, 2.69652e+009, 2.69766e+009, 2.6988e+009, 2.69994e+009, 2.70108e+009, 2.70222e+009, 2.70336e+009, 2.7045e+009, 2.70564e+009, 2.70678e+009, 2.70793e+009, 2.70907e+009, 2.71021e+009, 2.71135e+009, 2.71249e+009, 2.71364e+009, 2.71478e+009, 2.71592e+009, 2.71707e+009, 2.71821e+009, 2.71936e+009, 2.7205e+009, 2.72165e+009, 2.72279e+009, 2.72394e+009, 2.72508e+009, 2.72623e+009, 2.72737e+009, 2.72852e+009, 2.72967e+009, 2.73082e+009, 2.73196e+009, 2.73311e+009, 2.73426e+009, 2.73541e+009, 2.73656e+009, 2.73771e+009, 2.73885e+009, 2.74e+009, 2.74115e+009, 2.7423e+009, 2.74345e+009, 2.74461e+009, 2.74576e+009, 2.74691e+009, 2.74806e+009, 2.74921e+009, 2.75036e+009, 2.75152e+009, 2.75267e+009, 2.75382e+009, 2.75498e+009, 2.75613e+009, 2.75728e+009, 2.75844e+009, 2.75959e+009, 2.76075e+009, 2.7619e+009, 2.76306e+009, 2.76421e+009, 2.76537e+009, 2.76652e+009, 2.76768e+009, 2.76884e+009, 2.76999e+009, 2.77115e+009, 2.77231e+009, 2.77347e+009, 2.77463e+009, 2.77578e+009, 2.77694e+009, 2.7781e+009, 2.77926e+009, 2.78042e+009, 2.78158e+009, 2.78274e+009, 2.7839e+009, 2.78506e+009, 2.78622e+009, 2.78739e+009, 2.78855e+009, 2.78971e+009, 2.79087e+009, 2.79204e+009, 2.7932e+009, 2.79436e+009, 2.79552e+009, 2.79669e+009, 2.79785e+009, 2.79902e+009, 2.80018e+009, 2.80135e+009, 2.80251e+009, 2.80368e+009, 2.80484e+009, 2.80601e+009, 2.80718e+009, 2.80834e+009, 2.80951e+009, 2.81068e+009, 2.81184e+009, 2.81301e+009, 2.81418e+009, 2.81535e+009, 2.81652e+009, 2.81769e+009, 2.81886e+009, 2.82003e+009, 2.8212e+009, 2.82237e+009, 2.82354e+009, 2.82471e+009, 2.82588e+009, 2.82705e+009, 2.82822e+009, 2.82939e+009, 2.83056e+009, 2.83174e+009, 2.83291e+009, 2.83408e+009, 2.83526e+009, 2.83643e+009, 2.8376e+009, 2.83878e+009, 2.83995e+009, 2.84113e+009, 2.8423e+009, 2.84348e+009, 2.84465e+009, 2.84583e+009, 2.84701e+009, 2.84818e+009, 2.84936e+009, 2.85054e+009, 2.85171e+009, 2.85289e+009, 2.85407e+009, 2.85525e+009, 2.85643e+009, 2.85761e+009, 2.85879e+009, 2.85996e+009, 2.86114e+009, 2.86232e+009, 2.86351e+009, 2.86469e+009, 2.86587e+009, 2.86705e+009, 2.86823e+009, 2.86941e+009, 2.87059e+009, 2.87178e+009, 2.87296e+009, 2.87414e+009, 2.87533e+009, 2.87651e+009, 2.87769e+009, 2.87888e+009, 2.88006e+009, 2.88125e+009, 2.88243e+009, 2.88362e+009, 2.8848e+009, 2.88599e+009, 2.88717e+009, 2.88836e+009, 2.88955e+009, 2.89073e+009, 2.89192e+009, 2.89311e+009, 2.8943e+009, 2.89549e+009, 2.89667e+009, 2.89786e+009, 2.89905e+009, 2.90024e+009, 2.90143e+009, 2.90262e+009, 2.90381e+009, 2.905e+009, 2.90619e+009, 2.90739e+009, 2.90858e+009, 2.90977e+009, 2.91096e+009, 2.91215e+009, 2.91335e+009, 2.91454e+009, 2.91573e+009, 2.91693e+009, 2.91812e+009, 2.91931e+009, 2.92051e+009, 2.9217e+009, 2.9229e+009, 2.92409e+009, 2.92529e+009, 2.92648e+009, 2.92768e+009, 2.92888e+009, 2.93007e+009, 2.93127e+009, 2.93247e+009, 2.93367e+009, 2.93486e+009, 2.93606e+009, 2.93726e+009, 2.93846e+009, 2.93966e+009, 2.94086e+009, 2.94206e+009, 2.94326e+009, 2.94446e+009, 2.94566e+009, 2.94686e+009, 2.94806e+009, 2.94926e+009, 2.95046e+009, 2.95167e+009, 2.95287e+009, 2.95407e+009, 2.95527e+009, 2.95648e+009, 2.95768e+009, 2.95888e+009, 2.96009e+009, 2.96129e+009, 2.9625e+009, 2.9637e+009, 2.96491e+009, 2.96611e+009, 2.96732e+009, 2.96853e+009, 2.96973e+009, 2.97094e+009, 2.97215e+009, 2.97335e+009, 2.97456e+009, 2.97577e+009, 2.97698e+009, 2.97819e+009, 2.97939e+009, 2.9806e+009, 2.98181e+009, 2.98302e+009, 2.98423e+009, 2.98544e+009, 2.98665e+009, 2.98786e+009, 2.98908e+009, 2.99029e+009, 2.9915e+009, 2.99271e+009, 2.99392e+009, 2.99514e+009, 2.99635e+009, 2.99756e+009, 2.99878e+009, 2.99999e+009, 3.0012e+009, 3.00242e+009, 3.00363e+009, 3.00485e+009, 3.00606e+009, 3.00728e+009, 3.00849e+009, 3.00971e+009, 3.01093e+009, 3.01214e+009, 3.01336e+009, 3.01458e+009, 3.0158e+009, 3.01701e+009, 3.01823e+009, 3.01945e+009, 3.02067e+009, 3.02189e+009, 3.02311e+009, 3.02433e+009, 3.02555e+009, 3.02677e+009, 3.02799e+009, 3.02921e+009, 3.03043e+009, 3.03165e+009, 3.03287e+009, 3.0341e+009, 3.03532e+009, 3.03654e+009, 3.03776e+009, 3.03899e+009, 3.04021e+009, 3.04144e+009, 3.04266e+009, 3.04388e+009, 3.04511e+009, 3.04633e+009, 3.04756e+009, 3.04878e+009, 3.05001e+009, 3.05124e+009, 3.05246e+009, 3.05369e+009, 3.05492e+009, 3.05615e+009, 3.05737e+009, 3.0586e+009, 3.05983e+009, 3.06106e+009, 3.06229e+009, 3.06352e+009, 3.06475e+009, 3.06598e+009, 3.06721e+009, 3.06844e+009, 3.06967e+009, 3.0709e+009, 3.07213e+009, 3.07336e+009, 3.07459e+009, 3.07582e+009, 3.07706e+009, 3.07829e+009, 3.07952e+009, 3.08076e+009, 3.08199e+009, 3.08322e+009, 3.08446e+009, 3.08569e+009, 3.08693e+009, 3.08816e+009, 3.0894e+009, 3.09063e+009, 3.09187e+009, 3.09311e+009, 3.09434e+009, 3.09558e+009, 3.09682e+009, 3.09805e+009, 3.09929e+009, 3.10053e+009, 3.10177e+009, 3.10301e+009, 3.10425e+009, 3.10548e+009, 3.10672e+009, 3.10796e+009, 3.1092e+009, 3.11044e+009, 3.11169e+009, 3.11293e+009, 3.11417e+009, 3.11541e+009, 3.11665e+009, 3.11789e+009, 3.11914e+009, 3.12038e+009, 3.12162e+009, 3.12287e+009, 3.12411e+009, 3.12535e+009, 3.1266e+009, 3.12784e+009, 3.12909e+009, 3.13033e+009, 3.13158e+009, 3.13282e+009, 3.13407e+009, 3.13532e+009, 3.13656e+009, 3.13781e+009, 3.13906e+009, 3.1403e+009, 3.14155e+009, 3.1428e+009, 3.14405e+009, 3.1453e+009, 3.14655e+009, 3.1478e+009, 3.14905e+009, 3.1503e+009, 3.15155e+009, 3.1528e+009, 3.15405e+009, 3.1553e+009, 3.15655e+009, 3.1578e+009, 3.15905e+009, 3.16031e+009, 3.16156e+009, 3.16281e+009, 3.16407e+009, 3.16532e+009, 3.16657e+009, 3.16783e+009, 3.16908e+009, 3.17034e+009, 3.17159e+009, 3.17285e+009, 3.1741e+009, 3.17536e+009, 3.17661e+009, 3.17787e+009, 3.17913e+009, 3.18038e+009, 3.18164e+009, 3.1829e+009, 3.18416e+009, 3.18542e+009, 3.18667e+009, 3.18793e+009, 3.18919e+009, 3.19045e+009, 3.19171e+009, 3.19297e+009, 3.19423e+009, 3.19549e+009, 3.19675e+009, 3.19802e+009, 3.19928e+009, 3.20054e+009, 3.2018e+009, 3.20306e+009, 3.20433e+009, 3.20559e+009, 3.20685e+009, 3.20812e+009, 3.20938e+009, 3.21065e+009, 3.21191e+009, 3.21317e+009, 3.21444e+009, 3.21571e+009, 3.21697e+009, 3.21824e+009, 3.2195e+009, 3.22077e+009, 3.22204e+009, 3.2233e+009, 3.22457e+009, 3.22584e+009, 3.22711e+009, 3.22838e+009, 3.22965e+009, 3.23091e+009, 3.23218e+009, 3.23345e+009, 3.23472e+009, 3.23599e+009, 3.23726e+009, 3.23853e+009, 3.23981e+009, 3.24108e+009, 3.24235e+009, 3.24362e+009, 3.24489e+009, 3.24617e+009, 3.24744e+009, 3.24871e+009, 3.24999e+009, 3.25126e+009, 3.25253e+009, 3.25381e+009, 3.25508e+009, 3.25636e+009, 3.25763e+009, 3.25891e+009, 3.26019e+009, 3.26146e+009, 3.26274e+009, 3.26401e+009, 3.26529e+009, 3.26657e+009, 3.26785e+009, 3.26913e+009, 3.2704e+009, 3.27168e+009, 3.27296e+009, 3.27424e+009, 3.27552e+009, 3.2768e+009, 3.27808e+009, 3.27936e+009, 3.28064e+009, 3.28192e+009, 3.2832e+009, 3.28449e+009, 3.28577e+009, 3.28705e+009, 3.28833e+009, 3.28962e+009, 3.2909e+009, 3.29218e+009, 3.29347e+009, 3.29475e+009, 3.29603e+009, 3.29732e+009, 3.2986e+009, 3.29989e+009, 3.30117e+009, 3.30246e+009, 3.30375e+009, 3.30503e+009, 3.30632e+009, 3.30761e+009, 3.30889e+009, 3.31018e+009, 3.31147e+009, 3.31276e+009, 3.31405e+009, 3.31534e+009, 3.31662e+009, 3.31791e+009, 3.3192e+009, 3.32049e+009, 3.32178e+009, 3.32307e+009, 3.32437e+009, 3.32566e+009, 3.32695e+009, 3.32824e+009, 3.32953e+009, 3.33082e+009, 3.33212e+009, 3.33341e+009, 3.3347e+009, 3.336e+009, 3.33729e+009, 3.33859e+009, 3.33988e+009, 3.34118e+009, 3.34247e+009, 3.34377e+009, 3.34506e+009, 3.34636e+009, 3.34765e+009, 3.34895e+009, 3.35025e+009, 3.35155e+009, 3.35284e+009, 3.35414e+009, 3.35544e+009, 3.35674e+009, 3.35804e+009, 3.35934e+009, 3.36063e+009, 3.36193e+009, 3.36323e+009, 3.36453e+009, 3.36584e+009, 3.36714e+009, 3.36844e+009, 3.36974e+009, 3.37104e+009, 3.37234e+009, 3.37365e+009, 3.37495e+009, 3.37625e+009, 3.37755e+009, 3.37886e+009, 3.38016e+009, 3.38147e+009, 3.38277e+009, 3.38408e+009, 3.38538e+009, 3.38669e+009, 3.38799e+009, 3.3893e+009, 3.3906e+009, 3.39191e+009, 3.39322e+009, 3.39453e+009, 3.39583e+009, 3.39714e+009, 3.39845e+009, 3.39976e+009, 3.40107e+009, 3.40237e+009, 3.40368e+009, 3.40499e+009, 3.4063e+009, 3.40761e+009, 3.40892e+009, 3.41024e+009, 3.41155e+009, 3.41286e+009, 3.41417e+009, 3.41548e+009, 3.41679e+009, 3.41811e+009, 3.41942e+009, 3.42073e+009, 3.42205e+009, 3.42336e+009, 3.42468e+009, 3.42599e+009, 3.4273e+009, 3.42862e+009, 3.42994e+009, 3.43125e+009, 3.43257e+009, 3.43388e+009, 3.4352e+009, 3.43652e+009, 3.43783e+009, 3.43915e+009, 3.44047e+009, 3.44179e+009, 3.44311e+009, 3.44442e+009, 3.44574e+009, 3.44706e+009, 3.44838e+009, 3.4497e+009, 3.45102e+009, 3.45234e+009, 3.45366e+009, 3.45499e+009, 3.45631e+009, 3.45763e+009, 3.45895e+009, 3.46027e+009, 3.4616e+009, 3.46292e+009, 3.46424e+009, 3.46557e+009, 3.46689e+009, 3.46821e+009, 3.46954e+009, 3.47086e+009, 3.47219e+009, 3.47351e+009, 3.47484e+009, 3.47617e+009, 3.47749e+009, 3.47882e+009, 3.48015e+009, 3.48147e+009, 3.4828e+009, 3.48413e+009, 3.48546e+009, 3.48678e+009, 3.48811e+009, 3.48944e+009, 3.49077e+009, 3.4921e+009, 3.49343e+009, 3.49476e+009, 3.49609e+009, 3.49742e+009, 3.49875e+009, 3.50009e+009, 3.50142e+009, 3.50275e+009, 3.50408e+009, 3.50541e+009, 3.50675e+009, 3.50808e+009, 3.50941e+009, 3.51075e+009, 3.51208e+009, 3.51342e+009, 3.51475e+009, 3.51609e+009, 3.51742e+009, 3.51876e+009, 3.52009e+009, 3.52143e+009, 3.52277e+009, 3.5241e+009, 3.52544e+009, 3.52678e+009, 3.52812e+009, 3.52946e+009, 3.53079e+009, 3.53213e+009, 3.53347e+009, 3.53481e+009, 3.53615e+009, 3.53749e+009, 3.53883e+009, 3.54017e+009, 3.54151e+009, 3.54285e+009, 3.5442e+009, 3.54554e+009, 3.54688e+009, 3.54822e+009, 3.54956e+009, 3.55091e+009, 3.55225e+009, 3.55359e+009, 3.55494e+009, 3.55628e+009, 3.55763e+009, 3.55897e+009, 3.56032e+009, 3.56166e+009, 3.56301e+009, 3.56436e+009, 3.5657e+009, 3.56705e+009, 3.5684e+009, 3.56974e+009, 3.57109e+009, 3.57244e+009, 3.57379e+009, 3.57513e+009, 3.57648e+009, 3.57783e+009, 3.57918e+009, 3.58053e+009, 3.58188e+009, 3.58323e+009, 3.58458e+009, 3.58593e+009, 3.58729e+009, 3.58864e+009, 3.58999e+009, 3.59134e+009, 3.59269e+009, 3.59405e+009, 3.5954e+009, 3.59675e+009, 3.59811e+009, 3.59946e+009, 3.60082e+009, 3.60217e+009, 3.60352e+009, 3.60488e+009, 3.60624e+009, 3.60759e+009, 3.60895e+009, 3.6103e+009, 3.61166e+009, 3.61302e+009, 3.61438e+009, 3.61573e+009, 3.61709e+009, 3.61845e+009, 3.61981e+009, 3.62117e+009, 3.62253e+009, 3.62389e+009, 3.62525e+009, 3.62661e+009, 3.62797e+009, 3.62933e+009, 3.63069e+009, 3.63205e+009, 3.63341e+009, 3.63477e+009, 3.63614e+009, 3.6375e+009, 3.63886e+009, 3.64022e+009, 3.64159e+009, 3.64295e+009, 3.64432e+009, 3.64568e+009, 3.64705e+009, 3.64841e+009, 3.64978e+009, 3.65114e+009, 3.65251e+009, 3.65387e+009, 3.65524e+009, 3.65661e+009, 3.65797e+009, 3.65934e+009, 3.66071e+009, 3.66208e+009, 3.66345e+009, 3.66482e+009, 3.66618e+009, 3.66755e+009, 3.66892e+009, 3.67029e+009, 3.67166e+009, 3.67303e+009, 3.6744e+009, 3.67578e+009, 3.67715e+009, 3.67852e+009, 3.67989e+009, 3.68126e+009, 3.68264e+009, 3.68401e+009, 3.68538e+009, 3.68676e+009, 3.68813e+009, 3.6895e+009, 3.69088e+009, 3.69225e+009, 3.69363e+009, 3.69501e+009, 3.69638e+009, 3.69776e+009, 3.69913e+009, 3.70051e+009, 3.70189e+009, 3.70326e+009, 3.70464e+009, 3.70602e+009, 3.7074e+009, 3.70878e+009, 3.71016e+009, 3.71154e+009, 3.71291e+009, 3.71429e+009, 3.71567e+009, 3.71705e+009, 3.71844e+009, 3.71982e+009, 3.7212e+009, 3.72258e+009, 3.72396e+009, 3.72534e+009, 3.72673e+009, 3.72811e+009, 3.72949e+009, 3.73088e+009, 3.73226e+009, 3.73364e+009, 3.73503e+009, 3.73641e+009, 3.7378e+009, 3.73918e+009, 3.74057e+009, 3.74196e+009, 3.74334e+009, 3.74473e+009, 3.74611e+009, 3.7475e+009, 3.74889e+009, 3.75028e+009, 3.75167e+009, 3.75305e+009, 3.75444e+009, 3.75583e+009, 3.75722e+009, 3.75861e+009, 3.76e+009, 3.76139e+009, 3.76278e+009, 3.76417e+009, 3.76556e+009, 3.76696e+009, 3.76835e+009, 3.76974e+009, 3.77113e+009, 3.77252e+009, 3.77392e+009, 3.77531e+009, 3.7767e+009, 3.7781e+009, 3.77949e+009, 3.78089e+009, 3.78228e+009, 3.78368e+009, 3.78507e+009, 3.78647e+009, 3.78786e+009, 3.78926e+009, 3.79066e+009, 3.79206e+009, 3.79345e+009, 3.79485e+009, 3.79625e+009, 3.79765e+009, 3.79904e+009, 3.80044e+009, 3.80184e+009, 3.80324e+009, 3.80464e+009, 3.80604e+009, 3.80744e+009, 3.80884e+009, 3.81024e+009, 3.81165e+009, 3.81305e+009, 3.81445e+009, 3.81585e+009, 3.81726e+009, 3.81866e+009, 3.82006e+009, 3.82146e+009, 3.82287e+009, 3.82427e+009, 3.82568e+009, 3.82708e+009, 3.82849e+009, 3.82989e+009, 3.8313e+009, 3.8327e+009, 3.83411e+009, 3.83552e+009, 3.83692e+009, 3.83833e+009, 3.83974e+009, 3.84115e+009, 3.84256e+009, 3.84396e+009, 3.84537e+009, 3.84678e+009, 3.84819e+009, 3.8496e+009, 3.85101e+009, 3.85242e+009, 3.85383e+009, 3.85524e+009, 3.85665e+009, 3.85807e+009, 3.85948e+009, 3.86089e+009, 3.8623e+009, 3.86372e+009, 3.86513e+009, 3.86654e+009, 3.86796e+009, 3.86937e+009, 3.87078e+009, 3.8722e+009, 3.87361e+009, 3.87503e+009, 3.87645e+009, 3.87786e+009, 3.87928e+009, 3.88069e+009, 3.88211e+009, 3.88353e+009, 3.88495e+009, 3.88636e+009, 3.88778e+009, 3.8892e+009, 3.89062e+009, 3.89204e+009, 3.89346e+009, 3.89488e+009, 3.8963e+009, 3.89772e+009, 3.89914e+009, 3.90056e+009, 3.90198e+009, 3.9034e+009, 3.90482e+009, 3.90625e+009, 3.90767e+009, 3.90909e+009, 3.91051e+009, 3.91194e+009, 3.91336e+009, 3.91478e+009, 3.91621e+009, 3.91763e+009, 3.91906e+009, 3.92048e+009, 3.92191e+009, 3.92334e+009, 3.92476e+009, 3.92619e+009, 3.92761e+009, 3.92904e+009, 3.93047e+009, 3.9319e+009, 3.93333e+009, 3.93475e+009, 3.93618e+009, 3.93761e+009, 3.93904e+009, 3.94047e+009, 3.9419e+009, 3.94333e+009, 3.94476e+009, 3.94619e+009, 3.94762e+009, 3.94905e+009, 3.95049e+009, 3.95192e+009, 3.95335e+009, 3.95478e+009, 3.95622e+009, 3.95765e+009, 3.95908e+009, 3.96052e+009, 3.96195e+009, 3.96339e+009, 3.96482e+009, 3.96626e+009, 3.96769e+009, 3.96913e+009, 3.97056e+009, 3.972e+009, 3.97344e+009, 3.97487e+009, 3.97631e+009, 3.97775e+009, 3.97919e+009, 3.98063e+009, 3.98206e+009, 3.9835e+009, 3.98494e+009, 3.98638e+009, 3.98782e+009, 3.98926e+009, 3.9907e+009, 3.99214e+009, 3.99359e+009, 3.99503e+009, 3.99647e+009, 3.99791e+009, 3.99935e+009, 4.0008e+009, 4.00224e+009, 4.00368e+009, 4.00513e+009, 4.00657e+009, 4.00801e+009, 4.00946e+009, 4.0109e+009, 4.01235e+009, 4.01379e+009, 4.01524e+009, 4.01669e+009, 4.01813e+009, 4.01958e+009, 4.02103e+009, 4.02247e+009, 4.02392e+009, 4.02537e+009, 4.02682e+009, 4.02827e+009, 4.02972e+009, 4.03116e+009, 4.03261e+009, 4.03406e+009, 4.03551e+009, 4.03697e+009, 4.03842e+009, 4.03987e+009, 4.04132e+009, 4.04277e+009, 4.04422e+009, 4.04567e+009, 4.04713e+009, 4.04858e+009, 4.05003e+009, 4.05149e+009, 4.05294e+009, 4.0544e+009, 4.05585e+009, 4.05731e+009, 4.05876e+009, 4.06022e+009, 4.06167e+009, 4.06313e+009, 4.06458e+009, 4.06604e+009, 4.0675e+009, 4.06896e+009, 4.07041e+009, 4.07187e+009, 4.07333e+009, 4.07479e+009, 4.07625e+009, 4.07771e+009, 4.07917e+009, 4.08063e+009, 4.08209e+009, 4.08355e+009, 4.08501e+009, 4.08647e+009, 4.08793e+009, 4.08939e+009, 4.09085e+009, 4.09232e+009, 4.09378e+009, 4.09524e+009, 4.09671e+009, 4.09817e+009, 4.09963e+009, 4.1011e+009, 4.10256e+009, 4.10403e+009, 4.10549e+009, 4.10696e+009, 4.10842e+009, 4.10989e+009, 4.11136e+009, 4.11282e+009, 4.11429e+009, 4.11576e+009, 4.11723e+009, 4.11869e+009, 4.12016e+009, 4.12163e+009, 4.1231e+009, 4.12457e+009, 4.12604e+009, 4.12751e+009, 4.12898e+009, 4.13045e+009, 4.13192e+009, 4.13339e+009, 4.13486e+009, 4.13634e+009, 4.13781e+009, 4.13928e+009, 4.14075e+009, 4.14223e+009, 4.1437e+009, 4.14517e+009, 4.14665e+009, 4.14812e+009, 4.1496e+009, 4.15107e+009, 4.15255e+009, 4.15402e+009, 4.1555e+009, 4.15698e+009, 4.15845e+009, 4.15993e+009, 4.16141e+009, 4.16288e+009, 4.16436e+009, 4.16584e+009, 4.16732e+009, 4.1688e+009, 4.17028e+009, 4.17175e+009, 4.17323e+009, 4.17471e+009, 4.17619e+009, 4.17768e+009, 4.17916e+009, 4.18064e+009, 4.18212e+009, 4.1836e+009, 4.18508e+009, 4.18657e+009, 4.18805e+009, 4.18953e+009, 4.19102e+009, 4.1925e+009, 4.19398e+009, 4.19547e+009, 4.19695e+009, 4.19844e+009, 4.19992e+009, 4.20141e+009, 4.20289e+009, 4.20438e+009, 4.20587e+009, 4.20735e+009, 4.20884e+009, 4.21033e+009, 4.21182e+009, 4.21331e+009, 4.21479e+009, 4.21628e+009, 4.21777e+009, 4.21926e+009, 4.22075e+009, 4.22224e+009, 4.22373e+009, 4.22522e+009, 4.22671e+009, 4.22821e+009, 4.2297e+009, 4.23119e+009, 4.23268e+009, 4.23417e+009, 4.23567e+009, 4.23716e+009, 4.23865e+009, 4.24015e+009, 4.24164e+009, 4.24314e+009, 4.24463e+009, 4.24613e+009, 4.24762e+009, 4.24912e+009, 4.25061e+009, 4.25211e+009, 4.25361e+009, 4.2551e+009, 4.2566e+009, 4.2581e+009, 4.2596e+009, 4.2611e+009, 4.26259e+009, 4.26409e+009, 4.26559e+009, 4.26709e+009, 4.26859e+009, 4.27009e+009, 4.27159e+009, 4.27309e+009, 4.27459e+009, 4.2761e+009, 4.2776e+009, 4.2791e+009, 4.2806e+009, 4.28211e+009, 4.28361e+009, 4.28511e+009, 4.28662e+009, 4.28812e+009, 4.28962e+009, 4.29113e+009, 4.29263e+009, 4.29414e+009, 4.29564e+009, 4.29715e+009, 4.29866e+009, 4.30016e+009, 4.30167e+009, 4.30318e+009, 4.30468e+009, 4.30619e+009, 4.3077e+009, 4.30921e+009, 4.31072e+009, 4.31223e+009, 4.31374e+009, 4.31525e+009, 4.31676e+009, 4.31827e+009, 4.31978e+009, 4.32129e+009, 4.3228e+009, 4.32431e+009, 4.32582e+009, 4.32734e+009, 4.32885e+009, 4.33036e+009, 4.33187e+009, 4.33339e+009, 4.3349e+009, 4.33642e+009, 4.33793e+009, 4.33944e+009, 4.34096e+009, 4.34248e+009, 4.34399e+009, 4.34551e+009, 4.34702e+009, 4.34854e+009, 4.35006e+009, 4.35157e+009, 4.35309e+009, 4.35461e+009, 4.35613e+009, 4.35765e+009, 4.35917e+009, 4.36069e+009, 4.36221e+009, 4.36372e+009, 4.36525e+009, 4.36677e+009, 4.36829e+009, 4.36981e+009, 4.37133e+009, 4.37285e+009, 4.37437e+009, 4.3759e+009, 4.37742e+009, 4.37894e+009, 4.38046e+009, 4.38199e+009, 4.38351e+009, 4.38504e+009, 4.38656e+009, 4.38809e+009, 4.38961e+009, 4.39114e+009, 4.39266e+009, 4.39419e+009, 4.39572e+009, 4.39724e+009, 4.39877e+009, 4.4003e+009, 4.40183e+009, 4.40335e+009, 4.40488e+009, 4.40641e+009, 4.40794e+009, 4.40947e+009, 4.411e+009, 4.41253e+009, 4.41406e+009, 4.41559e+009, 4.41712e+009, 4.41865e+009, 4.42018e+009, 4.42172e+009, 4.42325e+009, 4.42478e+009, 4.42631e+009, 4.42785e+009, 4.42938e+009, 4.43091e+009, 4.43245e+009, 4.43398e+009, 4.43552e+009, 4.43705e+009, 4.43859e+009, 4.44012e+009, 4.44166e+009, 4.4432e+009, 4.44473e+009, 4.44627e+009, 4.44781e+009, 4.44935e+009, 4.45088e+009, 4.45242e+009, 4.45396e+009, 4.4555e+009, 4.45704e+009, 4.45858e+009, 4.46012e+009, 4.46166e+009, 4.4632e+009, 4.46474e+009, 4.46628e+009, 4.46782e+009, 4.46937e+009, 4.47091e+009, 4.47245e+009, 4.47399e+009, 4.47554e+009, 4.47708e+009, 4.47862e+009, 4.48017e+009, 4.48171e+009, 4.48326e+009, 4.4848e+009, 4.48635e+009, 4.48789e+009, 4.48944e+009, 4.49098e+009, 4.49253e+009, 4.49408e+009, 4.49563e+009, 4.49717e+009, 4.49872e+009, 4.50027e+009, 4.50182e+009, 4.50337e+009, 4.50492e+009, 4.50647e+009, 4.50802e+009, 4.50957e+009, 4.51112e+009, 4.51267e+009, 4.51422e+009, 4.51577e+009, 4.51732e+009, 4.51887e+009, 4.52043e+009, 4.52198e+009, 4.52353e+009, 4.52508e+009, 4.52664e+009, 4.52819e+009, 4.52975e+009, 4.5313e+009, 4.53286e+009, 4.53441e+009, 4.53597e+009, 4.53752e+009, 4.53908e+009, 4.54064e+009, 4.54219e+009, 4.54375e+009, 4.54531e+009, 4.54687e+009, 4.54842e+009, 4.54998e+009, 4.55154e+009, 4.5531e+009, 4.55466e+009, 4.55622e+009, 4.55778e+009, 4.55934e+009, 4.5609e+009, 4.56246e+009, 4.56402e+009, 4.56558e+009, 4.56715e+009, 4.56871e+009, 4.57027e+009, 4.57183e+009, 4.5734e+009, 4.57496e+009, 4.57652e+009, 4.57809e+009, 4.57965e+009, 4.58122e+009, 4.58278e+009, 4.58435e+009, 4.58592e+009, 4.58748e+009, 4.58905e+009, 4.59061e+009, 4.59218e+009, 4.59375e+009, 4.59532e+009, 4.59689e+009, 4.59845e+009, 4.60002e+009, 4.60159e+009, 4.60316e+009, 4.60473e+009, 4.6063e+009, 4.60787e+009, 4.60944e+009, 4.61101e+009, 4.61258e+009, 4.61416e+009, 4.61573e+009, 4.6173e+009, 4.61887e+009, 4.62045e+009, 4.62202e+009, 4.62359e+009, 4.62517e+009, 4.62674e+009, 4.62831e+009, 4.62989e+009, 4.63146e+009, 4.63304e+009, 4.63462e+009, 4.63619e+009, 4.63777e+009, 4.63935e+009, 4.64092e+009, 4.6425e+009, 4.64408e+009, 4.64566e+009, 4.64723e+009, 4.64881e+009, 4.65039e+009, 4.65197e+009, 4.65355e+009, 4.65513e+009, 4.65671e+009, 4.65829e+009, 4.65987e+009, 4.66145e+009, 4.66304e+009, 4.66462e+009, 4.6662e+009, 4.66778e+009, 4.66937e+009, 4.67095e+009, 4.67253e+009, 4.67412e+009, 4.6757e+009, 4.67728e+009, 4.67887e+009, 4.68045e+009, 4.68204e+009, 4.68363e+009, 4.68521e+009, 4.6868e+009, 4.68838e+009, 4.68997e+009, 4.69156e+009, 4.69315e+009, 4.69474e+009, 4.69632e+009, 4.69791e+009, 4.6995e+009, 4.70109e+009, 4.70268e+009, 4.70427e+009, 4.70586e+009, 4.70745e+009, 4.70904e+009, 4.71063e+009, 4.71222e+009, 4.71382e+009, 4.71541e+009, 4.717e+009, 4.71859e+009, 4.72019e+009, 4.72178e+009, 4.72338e+009, 4.72497e+009, 4.72656e+009, 4.72816e+009, 4.72975e+009, 4.73135e+009, 4.73294e+009, 4.73454e+009, 4.73614e+009, 4.73773e+009, 4.73933e+009, 4.74093e+009, 4.74253e+009, 4.74412e+009, 4.74572e+009, 4.74732e+009, 4.74892e+009, 4.75052e+009, 4.75212e+009, 4.75372e+009, 4.75532e+009, 4.75692e+009, 4.75852e+009, 4.76012e+009, 4.76172e+009, 4.76333e+009, 4.76493e+009, 4.76653e+009, 4.76813e+009, 4.76974e+009, 4.77134e+009, 4.77294e+009, 4.77455e+009, 4.77615e+009, 4.77776e+009, 4.77936e+009, 4.78097e+009, 4.78257e+009, 4.78418e+009, 4.78579e+009, 4.78739e+009, 4.789e+009, 4.79061e+009, 4.79222e+009, 4.79382e+009, 4.79543e+009, 4.79704e+009, 4.79865e+009, 4.80026e+009, 4.80187e+009, 4.80348e+009, 4.80509e+009, 4.8067e+009, 4.80831e+009, 4.80992e+009, 4.81153e+009, 4.81315e+009, 4.81476e+009, 4.81637e+009, 4.81798e+009, 4.8196e+009, 4.82121e+009, 4.82283e+009, 4.82444e+009, 4.82605e+009, 4.82767e+009, 4.82928e+009, 4.8309e+009, 4.83252e+009, 4.83413e+009, 4.83575e+009, 4.83736e+009, 4.83898e+009, 4.8406e+009, 4.84222e+009, 4.84384e+009, 4.84545e+009, 4.84707e+009, 4.84869e+009, 4.85031e+009, 4.85193e+009, 4.85355e+009, 4.85517e+009, 4.85679e+009, 4.85841e+009, 4.86003e+009, 4.86166e+009, 4.86328e+009, 4.8649e+009, 4.86652e+009, 4.86815e+009, 4.86977e+009, 4.87139e+009, 4.87302e+009, 4.87464e+009, 4.87627e+009, 4.87789e+009, 4.87952e+009, 4.88114e+009, 4.88277e+009, 4.88439e+009, 4.88602e+009, 4.88765e+009, 4.88927e+009, 4.8909e+009, 4.89253e+009, 4.89416e+009, 4.89579e+009, 4.89742e+009, 4.89904e+009, 4.90067e+009, 4.9023e+009, 4.90393e+009, 4.90556e+009, 4.9072e+009, 4.90883e+009, 4.91046e+009, 4.91209e+009, 4.91372e+009, 4.91535e+009, 4.91699e+009, 4.91862e+009, 4.92025e+009, 4.92189e+009, 4.92352e+009, 4.92516e+009, 4.92679e+009, 4.92842e+009, 4.93006e+009, 4.9317e+009, 4.93333e+009, 4.93497e+009, 4.9366e+009, 4.93824e+009, 4.93988e+009, 4.94152e+009, 4.94315e+009, 4.94479e+009, 4.94643e+009, 4.94807e+009, 4.94971e+009, 4.95135e+009, 4.95299e+009, 4.95463e+009, 4.95627e+009, 4.95791e+009, 4.95955e+009, 4.96119e+009, 4.96284e+009, 4.96448e+009, 4.96612e+009, 4.96776e+009, 4.96941e+009, 4.97105e+009, 4.97269e+009, 4.97434e+009, 4.97598e+009, 4.97763e+009, 4.97927e+009, 4.98092e+009, 4.98256e+009, 4.98421e+009, 4.98586e+009, 4.9875e+009, 4.98915e+009, 4.9908e+009, 4.99244e+009, 4.99409e+009, 4.99574e+009, 4.99739e+009, 4.99904e+009, 5.00069e+009, 5.00234e+009, 5.00399e+009, 5.00564e+009, 5.00729e+009, 5.00894e+009, 5.01059e+009, 5.01224e+009, 5.01389e+009, 5.01555e+009, 5.0172e+009, 5.01885e+009, 5.02051e+009, 5.02216e+009, 5.02381e+009, 5.02547e+009, 5.02712e+009, 5.02878e+009, 5.03043e+009, 5.03209e+009, 5.03374e+009, 5.0354e+009, 5.03706e+009, 5.03871e+009, 5.04037e+009, 5.04203e+009, 5.04369e+009, 5.04534e+009, 5.047e+009, 5.04866e+009, 5.05032e+009, 5.05198e+009, 5.05364e+009, 5.0553e+009, 5.05696e+009, 5.05862e+009, 5.06028e+009, 5.06194e+009, 5.06361e+009, 5.06527e+009, 5.06693e+009, 5.06859e+009, 5.07026e+009, 5.07192e+009, 5.07358e+009, 5.07525e+009, 5.07691e+009, 5.07858e+009, 5.08024e+009, 5.08191e+009, 5.08357e+009, 5.08524e+009, 5.08691e+009, 5.08857e+009, 5.09024e+009, 5.09191e+009, 5.09357e+009, 5.09524e+009, 5.09691e+009, 5.09858e+009, 5.10025e+009, 5.10192e+009, 5.10359e+009, 5.10526e+009, 5.10693e+009, 5.1086e+009, 5.11027e+009, 5.11194e+009, 5.11361e+009, 5.11528e+009, 5.11696e+009, 5.11863e+009, 5.1203e+009, 5.12197e+009, 5.12365e+009, 5.12532e+009, 5.127e+009, 5.12867e+009, 5.13035e+009, 5.13202e+009, 5.1337e+009, 5.13537e+009, 5.13705e+009, 5.13872e+009, 5.1404e+009, 5.14208e+009, 5.14376e+009, 5.14543e+009, 5.14711e+009, 5.14879e+009, 5.15047e+009, 5.15215e+009, 5.15383e+009, 5.15551e+009, 5.15719e+009, 5.15887e+009, 5.16055e+009, 5.16223e+009, 5.16391e+009, 5.16559e+009, 5.16728e+009, 5.16896e+009, 5.17064e+009, 5.17232e+009, 5.17401e+009, 5.17569e+009, 5.17738e+009, 5.17906e+009, 5.18074e+009, 5.18243e+009, 5.18412e+009, 5.1858e+009, 5.18749e+009, 5.18917e+009, 5.19086e+009, 5.19255e+009, 5.19423e+009, 5.19592e+009, 5.19761e+009, 5.1993e+009, 5.20099e+009, 5.20268e+009, 5.20437e+009, 5.20606e+009, 5.20775e+009, 5.20944e+009, 5.21113e+009, 5.21282e+009, 5.21451e+009, 5.2162e+009, 5.21789e+009, 5.21958e+009, 5.22128e+009, 5.22297e+009, 5.22466e+009, 5.22636e+009, 5.22805e+009, 5.22975e+009, 5.23144e+009, 5.23313e+009, 5.23483e+009, 5.23653e+009, 5.23822e+009, 5.23992e+009, 5.24161e+009, 5.24331e+009, 5.24501e+009, 5.24671e+009, 5.2484e+009, 5.2501e+009, 5.2518e+009, 5.2535e+009, 5.2552e+009, 5.2569e+009, 5.2586e+009, 5.2603e+009, 5.262e+009, 5.2637e+009, 5.2654e+009, 5.2671e+009, 5.2688e+009, 5.27051e+009, 5.27221e+009, 5.27391e+009, 5.27562e+009, 5.27732e+009, 5.27902e+009, 5.28073e+009, 5.28243e+009, 5.28414e+009, 5.28584e+009, 5.28755e+009, 5.28925e+009, 5.29096e+009, 5.29267e+009, 5.29437e+009, 5.29608e+009, 5.29779e+009, 5.29949e+009, 5.3012e+009, 5.30291e+009, 5.30462e+009, 5.30633e+009, 5.30804e+009, 5.30975e+009, 5.31146e+009, 5.31317e+009, 5.31488e+009, 5.31659e+009, 5.3183e+009, 5.32001e+009, 5.32173e+009, 5.32344e+009, 5.32515e+009, 5.32686e+009, 5.32858e+009, 5.33029e+009, 5.33201e+009, 5.33372e+009, 5.33544e+009, 5.33715e+009, 5.33887e+009, 5.34058e+009, 5.3423e+009, 5.34401e+009, 5.34573e+009, 5.34745e+009, 5.34917e+009, 5.35088e+009, 5.3526e+009, 5.35432e+009, 5.35604e+009, 5.35776e+009, 5.35948e+009, 5.3612e+009, 5.36292e+009, 5.36464e+009, 5.36636e+009, 5.36808e+009, 5.3698e+009, 5.37152e+009, 5.37324e+009, 5.37497e+009, 5.37669e+009, 5.37841e+009, 5.38013e+009, 5.38186e+009, 5.38358e+009, 5.38531e+009, 5.38703e+009, 5.38876e+009, 5.39048e+009, 5.39221e+009, 5.39393e+009, 5.39566e+009, 5.39739e+009, 5.39911e+009, 5.40084e+009, 5.40257e+009, 5.4043e+009, 5.40602e+009, 5.40775e+009, 5.40948e+009, 5.41121e+009, 5.41294e+009, 5.41467e+009, 5.4164e+009, 5.41813e+009, 5.41986e+009, 5.42159e+009, 5.42333e+009, 5.42506e+009, 5.42679e+009, 5.42852e+009, 5.43026e+009, 5.43199e+009, 5.43372e+009, 5.43546e+009, 5.43719e+009, 5.43893e+009, 5.44066e+009, 5.4424e+009, 5.44413e+009, 5.44587e+009, 5.4476e+009, 5.44934e+009, 5.45108e+009, 5.45281e+009, 5.45455e+009, 5.45629e+009, 5.45803e+009, 5.45977e+009, 5.46151e+009, 5.46325e+009, 5.46499e+009, 5.46673e+009, 5.46847e+009, 5.47021e+009, 5.47195e+009, 5.47369e+009, 5.47543e+009, 5.47717e+009, 5.47891e+009, 5.48066e+009, 5.4824e+009, 5.48414e+009, 5.48589e+009, 5.48763e+009, 5.48937e+009, 5.49112e+009, 5.49286e+009, 5.49461e+009, 5.49636e+009, 5.4981e+009, 5.49985e+009, 5.50159e+009, 5.50334e+009, 5.50509e+009, 5.50684e+009, 5.50858e+009, 5.51033e+009, 5.51208e+009, 5.51383e+009, 5.51558e+009, 5.51733e+009, 5.51908e+009, 5.52083e+009, 5.52258e+009, 5.52433e+009, 5.52608e+009, 5.52783e+009, 5.52958e+009, 5.53134e+009, 5.53309e+009, 5.53484e+009, 5.5366e+009, 5.53835e+009, 5.5401e+009, 5.54186e+009, 5.54361e+009, 5.54537e+009, 5.54712e+009, 5.54888e+009, 5.55063e+009, 5.55239e+009, 5.55415e+009, 5.5559e+009, 5.55766e+009, 5.55942e+009, 5.56118e+009, 5.56293e+009, 5.56469e+009, 5.56645e+009, 5.56821e+009, 5.56997e+009, 5.57173e+009, 5.57349e+009, 5.57525e+009, 5.57701e+009, 5.57877e+009, 5.58054e+009, 5.5823e+009, 5.58406e+009, 5.58582e+009, 5.58758e+009, 5.58935e+009, 5.59111e+009, 5.59288e+009, 5.59464e+009, 5.5964e+009, 5.59817e+009, 5.59993e+009, 5.6017e+009, 5.60347e+009, 5.60523e+009, 5.607e+009, 5.60877e+009, 5.61053e+009, 5.6123e+009, 5.61407e+009, 5.61584e+009, 5.61761e+009, 5.61937e+009, 5.62114e+009, 5.62291e+009, 5.62468e+009, 5.62645e+009, 5.62822e+009, 5.63e+009, 5.63177e+009, 5.63354e+009, 5.63531e+009, 5.63708e+009, 5.63886e+009, 5.64063e+009, 5.6424e+009, 5.64417e+009, 5.64595e+009, 5.64772e+009, 5.6495e+009, 5.65127e+009, 5.65305e+009, 5.65482e+009, 5.6566e+009, 5.65838e+009, 5.66015e+009, 5.66193e+009, 5.66371e+009, 5.66548e+009, 5.66726e+009, 5.66904e+009, 5.67082e+009, 5.6726e+009, 5.67438e+009, 5.67616e+009, 5.67794e+009, 5.67972e+009, 5.6815e+009, 5.68328e+009, 5.68506e+009, 5.68684e+009, 5.68862e+009, 5.69041e+009, 5.69219e+009, 5.69397e+009, 5.69576e+009, 5.69754e+009, 5.69932e+009, 5.70111e+009, 5.70289e+009, 5.70468e+009, 5.70646e+009, 5.70825e+009, 5.71003e+009, 5.71182e+009, 5.71361e+009, 5.71539e+009, 5.71718e+009, 5.71897e+009, 5.72076e+009, 5.72255e+009, 5.72433e+009, 5.72612e+009, 5.72791e+009, 5.7297e+009, 5.73149e+009, 5.73328e+009, 5.73507e+009, 5.73686e+009, 5.73866e+009, 5.74045e+009, 5.74224e+009, 5.74403e+009, 5.74582e+009, 5.74762e+009, 5.74941e+009, 5.7512e+009, 5.753e+009, 5.75479e+009, 5.75659e+009, 5.75838e+009, 5.76018e+009, 5.76197e+009, 5.76377e+009, 5.76557e+009, 5.76736e+009, 5.76916e+009, 5.77096e+009, 5.77276e+009, 5.77455e+009, 5.77635e+009, 5.77815e+009, 5.77995e+009, 5.78175e+009, 5.78355e+009, 5.78535e+009, 5.78715e+009, 5.78895e+009, 5.79075e+009, 5.79255e+009, 5.79435e+009, 5.79616e+009, 5.79796e+009, 5.79976e+009, 5.80157e+009, 5.80337e+009, 5.80517e+009, 5.80698e+009, 5.80878e+009, 5.81059e+009, 5.81239e+009, 5.8142e+009, 5.816e+009, 5.81781e+009, 5.81961e+009, 5.82142e+009, 5.82323e+009, 5.82504e+009, 5.82684e+009, 5.82865e+009, 5.83046e+009, 5.83227e+009, 5.83408e+009, 5.83589e+009, 5.8377e+009, 5.83951e+009, 5.84132e+009, 5.84313e+009, 5.84494e+009, 5.84675e+009, 5.84856e+009, 5.85038e+009, 5.85219e+009, 5.854e+009, 5.85582e+009, 5.85763e+009, 5.85944e+009, 5.86126e+009, 5.86307e+009, 5.86489e+009, 5.8667e+009, 5.86852e+009, 5.87033e+009, 5.87215e+009, 5.87397e+009, 5.87578e+009, 5.8776e+009, 5.87942e+009, 5.88124e+009, 5.88305e+009, 5.88487e+009, 5.88669e+009, 5.88851e+009, 5.89033e+009, 5.89215e+009, 5.89397e+009, 5.89579e+009, 5.89761e+009, 5.89943e+009, 5.90126e+009, 5.90308e+009, 5.9049e+009, 5.90672e+009, 5.90855e+009, 5.91037e+009, 5.91219e+009, 5.91402e+009, 5.91584e+009, 5.91767e+009, 5.91949e+009, 5.92132e+009, 5.92314e+009, 5.92497e+009, 5.92679e+009, 5.92862e+009, 5.93045e+009, 5.93228e+009, 5.9341e+009, 5.93593e+009, 5.93776e+009, 5.93959e+009, 5.94142e+009, 5.94325e+009, 5.94508e+009, 5.94691e+009, 5.94874e+009, 5.95057e+009, 5.9524e+009, 5.95423e+009, 5.95606e+009, 5.95789e+009, 5.95973e+009, 5.96156e+009, 5.96339e+009, 5.96523e+009, 5.96706e+009, 5.96889e+009, 5.97073e+009, 5.97256e+009, 5.9744e+009, 5.97623e+009, 5.97807e+009, 5.97991e+009, 5.98174e+009, 5.98358e+009, 5.98542e+009, 5.98725e+009, 5.98909e+009, 5.99093e+009, 5.99277e+009, 5.99461e+009, 5.99645e+009, 5.99829e+009, 6.00013e+009, 6.00197e+009, 6.00381e+009, 6.00565e+009, 6.00749e+009, 6.00933e+009, 6.01117e+009, 6.01302e+009, 6.01486e+009, 6.0167e+009, 6.01854e+009, 6.02039e+009, 6.02223e+009, 6.02408e+009, 6.02592e+009, 6.02777e+009, 6.02961e+009, 6.03146e+009, 6.0333e+009, 6.03515e+009, 6.037e+009, 6.03884e+009, 6.04069e+009, 6.04254e+009, 6.04439e+009, 6.04623e+009, 6.04808e+009, 6.04993e+009, 6.05178e+009, 6.05363e+009, 6.05548e+009, 6.05733e+009, 6.05918e+009, 6.06103e+009, 6.06289e+009, 6.06474e+009, 6.06659e+009, 6.06844e+009, 6.07029e+009, 6.07215e+009, 6.074e+009 };
						static int n[8192] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8, 6, 9, 2, 4, 2, 11, 10, 2, 5, 5, 4, 3, 10, 9, 7, 8, 9, 3, 9, 11, 6, 11, 4, 8, 9, 4, 7, 9, 2, 7, 3, 8, 11, 3, 10, 6, 5, 9, 3, 7, 6, 7, 4, 7, 5, 3, 6, 11, 10, 7, 2, 4, 2, 8, 8, 6, 3, 10, 6, 8, 4, 2, 4, 11, 5, 10, 2, 9, 5, 7, 3, 2, 9, 4, 8, 4, 11, 2, 6, 5, 11, 10, 2, 7, 7, 11, 3, 8, 4, 5, 4, 8, 5, 5, 10, 3, 4, 6, 9, 2, 6, 3, 9, 7, 2, 9, 2, 6, 7, 2, 7, 4, 11, 2, 6, 7, 9, 5, 7, 5, 10, 10, 5, 7, 6, 2, 4, 7, 3, 6, 5, 3, 3, 8, 2, 7, 11, 3, 2, 5, 7, 9, 8, 8, 2, 4, 6, 3, 4, 6, 11, 5, 6, 6, 8, 11, 5, 4, 6, 2, 8, 7, 8, 2, 9, 6, 7, 4, 3, 8, 8, 5, 3, 7, 2, 4, 5, 6, 5, 6, 11, 9, 9, 4, 10, 5, 3, 2, 5, 6, 7, 3, 5, 2, 9, 10, 3, 8, 10, 5, 6, 5, 11, 2, 8, 2, 2, 6, 10, 6, 10, 9, 6, 8, 5, 4, 11, 5, 5, 11, 3, 6, 2, 11, 11, 3, 10, 11, 6, 6, 4, 7, 8, 11, 6, 3, 4, 5, 10, 5, 10, 2, 10, 5, 9, 4, 10, 2, 5, 6, 8, 11, 3, 7, 7, 7, 2, 5, 8, 9, 8, 9, 8, 9, 6, 5, 2, 7, 7, 10, 5, 7, 5, 10, 9, 11, 7, 2, 5, 3, 2, 5, 4, 7, 4, 5, 4, 3, 5, 6, 10, 10, 2, 6, 11, 11, 9, 4, 10, 5, 8, 6, 11, 6, 10, 7, 11, 2, 2, 9, 5, 8, 9, 8, 3, 4, 8, 2, 3, 4, 8, 9, 10, 3, 7, 4, 8, 7, 7, 6, 2, 8, 3, 5, 4, 2, 5, 7, 4, 8, 8, 10, 5, 7, 5, 3, 2, 11, 7, 11, 2, 6, 8, 2, 11, 6, 8, 3, 5, 8, 7, 5, 5, 10, 5, 6, 6, 11, 4, 5, 8, 9, 10, 10, 9, 11, 11, 4, 6, 7, 3, 2, 8, 4, 4, 3, 10, 2, 4, 6, 11, 11, 10, 5, 8, 6, 10, 8, 5, 6, 11, 2, 9, 6, 3, 9, 7, 4, 5, 4, 8, 2, 7, 8, 3, 10, 4, 11, 7, 11, 9, 10, 7, 2, 4, 10, 4, 3, 2, 6, 11, 7, 8, 11, 10, 4, 11, 4, 7, 2, 6, 7, 11, 9, 5, 5, 10, 2, 6, 2, 5, 9, 5, 8, 9, 11, 10, 3, 6, 9, 7, 10, 8, 10, 9, 4, 3, 6, 11, 11, 5, 10, 7, 5, 9, 3, 6, 6, 7, 5, 10, 11, 10, 11, 7, 6, 4, 4, 8, 6, 3, 4, 10, 3, 10, 9, 9, 10, 9, 9, 6, 5, 6, 8, 5, 9, 4, 9, 6, 4, 3, 9, 4, 3, 10, 4, 6, 2, 3, 8, 9, 8, 5, 9, 8, 11, 7, 2, 2, 9, 3, 11, 6, 2, 10, 3, 3, 9, 6, 3, 7, 10, 11, 2, 9, 2, 7, 7, 7, 2, 7, 11, 4, 11, 5, 7, 5, 6, 9, 5, 3, 8, 3, 2, 4, 4, 8, 10, 2, 6, 9, 5, 5, 6, 5, 8, 3, 11, 4, 11, 3, 3, 5, 11, 4, 2, 9, 2, 11, 4, 2, 11, 6, 6, 8, 2, 8, 4, 7, 9, 2, 6, 3, 5, 2, 9, 5, 8, 8, 2, 8, 6, 9, 7, 3, 11, 10, 10, 6, 11, 9, 3, 10, 9, 7, 9, 3, 6, 5, 3, 2, 5, 6, 7, 5, 4, 4, 2, 9, 9, 4, 4, 9, 8, 9, 11, 5, 2, 4, 11, 7, 11, 10, 7, 6, 5, 3, 11, 3, 6, 2, 3, 6, 5, 6, 6, 11, 6, 4, 9, 5, 6, 2, 10, 10, 9, 10, 4, 3, 11, 9, 2, 7, 11, 8, 4, 11, 10, 7, 9, 8, 8, 6, 2, 10, 6, 3, 9, 5, 7, 10, 10, 5, 9, 3, 7, 5, 9, 5, 11, 7, 11, 6, 7, 4, 3, 5, 4, 11, 3, 8, 4, 8, 10, 7, 11, 3, 6, 11, 10, 6, 2, 5, 8, 11, 4, 8, 3, 4, 3, 10, 10, 4, 11, 10, 11, 7, 8, 4, 5, 4, 11, 2, 10, 9, 11, 11, 6, 10, 8, 2, 3, 5, 9, 6, 2, 4, 5, 9, 7, 6, 3, 11, 4, 10, 9, 2, 7, 9, 9, 8, 4, 11, 3, 5, 9, 8, 2, 8, 5, 7, 11, 10, 10, 8, 9, 9, 2, 11, 3, 8, 3, 6, 9, 5, 9, 8, 7, 6, 8, 6, 4, 3, 6, 5, 10, 5, 2, 4, 4, 5, 2, 5, 3, 9, 3, 11, 2, 10, 5, 6, 4, 8, 8, 9, 4, 7, 5, 8, 5, 6, 4, 6, 8, 10, 9, 5, 5, 10, 7, 2, 10, 3, 5, 5, 5, 11, 3, 3, 11, 6, 11, 4, 2, 11, 6, 7, 3, 6, 7, 10, 8, 6, 5, 11, 10, 6, 5, 7, 11, 7, 5, 6, 7, 10, 6, 3, 8, 2, 11, 9, 10, 5, 2, 4, 5, 10, 10, 10, 2, 6, 5, 10, 11, 11, 3, 5, 10, 3, 9, 7, 6, 4, 3, 6, 11, 8, 9, 5, 7, 5, 5, 2, 6, 2, 8, 7, 11, 9, 3, 3, 7, 8, 9, 3, 2, 4, 4, 4, 10, 9, 11, 8, 4, 10, 8, 3, 10, 4, 6, 5, 5, 6, 2, 2, 6, 7, 10, 7, 3, 11, 7, 5, 9, 6, 8, 10, 6, 11, 7, 9, 4, 7, 6, 8, 6, 4, 7, 10, 4, 2, 9, 4, 2, 10, 11, 10, 8, 6, 4, 10, 10, 11, 11, 7, 8, 2, 11, 2, 7, 8, 8, 3, 5, 4, 2, 8, 11, 4, 4, 5, 5, 9, 6, 11, 11, 3, 7, 9, 4, 10, 11, 7, 8, 2, 6, 10, 6, 11, 5, 7, 3, 9, 2, 9, 10, 5, 10, 3, 2, 11, 3, 8, 5, 9, 10, 2, 7, 9, 6, 7, 5, 4, 4, 9, 10, 9, 10, 3, 6, 4, 7, 10, 10, 11, 6, 8, 8, 8, 7, 6, 11, 4, 9, 2, 9, 11, 6, 2, 8, 5, 2, 3, 5, 10, 2, 3, 4, 7, 11, 3, 5, 7, 11, 5, 10, 10, 2, 9, 7, 6, 3, 10, 5, 3, 2, 11, 4, 9, 5, 4, 8, 8, 8, 10, 10, 6, 6, 6, 8, 11, 7, 3, 8, 4, 5, 2, 11, 2, 3, 7, 8, 11, 2, 3, 8, 8, 6, 5, 6, 2, 9, 7, 11, 4, 7, 2, 6, 2, 5, 9, 3, 7, 3, 7, 6, 10, 8, 2, 10, 5, 2, 7, 5, 5, 4, 4, 11, 11, 9, 4, 5, 6, 9, 10, 4, 9, 10, 8, 2, 6, 7, 5, 7, 11, 3, 4, 6, 8, 6, 11, 6, 5, 8, 11, 11, 10, 10, 7, 10, 11, 2, 2, 2, 10, 6, 8, 3, 3, 8, 9, 7, 2, 8, 3, 7, 8, 3, 2, 2, 7, 9, 4, 10, 10, 4, 9, 3, 10, 7, 5, 11, 11, 4, 5, 9, 10, 5, 5, 9, 11, 10, 8, 4, 6, 5, 9, 4, 5, 5, 5, 6, 8, 10, 3, 5, 8, 7, 7, 7, 6, 5, 10, 2, 2, 11, 5, 11, 7, 8, 3, 11, 3, 5, 2, 4, 2, 2, 8, 8, 10, 2, 10, 3, 10, 6, 9, 3, 7, 5, 3, 2, 4, 9, 8, 11, 6, 2, 11, 2, 6, 9, 11, 2, 5, 11, 9, 3, 10, 6, 3, 2, 8, 4, 5, 10, 8, 8, 9, 3, 5, 7, 8, 9, 6, 9, 8, 11, 5, 10, 11, 10, 8, 7, 5, 6, 8, 4, 3, 8, 2, 6, 3, 10, 7, 2, 11, 3, 9, 3, 11, 9, 11, 8, 4, 9, 7, 6, 3, 2, 2, 2, 2, 3, 2, 3, 11, 8, 9, 11, 9, 5, 7, 8, 7, 8, 6, 5, 3, 3, 9, 10, 6, 11, 4, 4, 9, 7, 10, 7, 4, 11, 9, 11, 4, 2, 7, 5, 11, 5, 8, 6, 2, 4, 2, 11, 11, 3, 2, 3, 8, 10, 7, 2, 8, 9, 4, 9, 2, 7, 3, 4, 3, 10, 10, 10, 2, 8, 3, 3, 6, 6, 5, 4, 6, 6, 4, 7, 8, 4, 4, 11, 7, 8, 8, 7, 6, 5, 9, 6, 3, 6, 6, 11, 6, 11, 8, 2, 11, 4, 11, 3, 5, 10, 8, 7, 2, 3, 11, 4, 10, 11, 9, 8, 11, 9, 2, 7, 6, 11, 11, 9, 9, 2, 10, 7, 9, 8, 7, 7, 11, 6, 9, 2, 2, 10, 9, 7, 6, 10, 11, 3, 6, 8, 8, 8, 6, 10, 5, 7, 4, 5, 8, 9, 7, 5, 5, 6, 4, 4, 2, 5, 3, 10, 11, 3, 3, 7, 8, 6, 9, 11, 11, 8, 4, 6, 8, 6, 2, 8, 7, 8, 3, 9, 3, 7, 7, 6, 2, 5, 4, 9, 6, 6, 4, 3, 4, 4, 9, 9, 6, 4, 11, 5, 2, 9, 7, 2, 11, 6, 8, 2, 5, 4, 3, 6, 5, 2, 2, 9, 3, 4, 7, 9, 4, 8, 9, 6, 4, 6, 10, 8, 3, 10, 8, 9, 10, 10, 11, 11, 6, 11, 10, 11, 10, 2, 2, 11, 7, 3, 9, 7, 8, 9, 10, 7, 2, 8, 11, 10, 8, 3, 2, 7, 11, 3, 6, 5, 7, 7, 3, 4, 2, 2, 4, 9, 9, 5, 8, 9, 9, 9, 5, 7, 11, 10, 7, 10, 4, 3, 11, 7, 2, 4, 2, 11, 2, 10, 6, 3, 11, 11, 4, 7, 11, 2, 9, 7, 4, 8, 4, 9, 3, 5, 4, 7, 2, 10, 10, 6, 7, 8, 7, 11, 5, 4, 11, 4, 7, 7, 10, 8, 2, 10, 9, 5, 4, 8, 9, 3, 3, 2, 2, 10, 2, 9, 8, 5, 6, 5, 10, 6, 6, 8, 6, 8, 3, 8, 2, 6, 9, 10, 6, 4, 10, 10, 3, 8, 11, 2, 8, 11, 9, 2, 2, 2, 8, 5, 6, 2, 3, 9, 4, 8, 8, 6, 2, 6, 5, 9, 2, 4, 2, 4, 11, 7, 7, 8, 6, 3, 4, 10, 5, 10, 8, 5, 11, 8, 4, 7, 8, 11, 9, 6, 9, 9, 10, 5, 7, 3, 7, 2, 8, 11, 6, 2, 7, 6, 11, 9, 9, 4, 4, 10, 11, 10, 6, 4, 4, 3, 3, 9, 6, 5, 4, 6, 7, 9, 7, 7, 11, 11, 11, 5, 5, 7, 7, 8, 8, 11, 7, 6, 10, 11, 5, 8, 9, 2, 10, 5, 6, 10, 8, 2, 7, 9, 6, 3, 10, 9, 8, 2, 2, 10, 3, 4, 3, 7, 7, 2, 2, 2, 2, 4, 3, 10, 8, 10, 6, 10, 4, 9, 6, 9, 3, 9, 8, 2, 5, 5, 8, 11, 10, 11, 3, 6, 5, 4, 4, 5, 10, 6, 7, 3, 11, 3, 11, 5, 5, 9, 11, 6, 9, 10, 4, 6, 2, 9, 10, 11, 9, 2, 7, 8, 11, 2, 2, 2, 2, 8, 5, 11, 10, 9, 8, 7, 4, 8, 6, 6, 11, 10, 6, 8, 7, 8, 6, 4, 9, 3, 9, 6, 9, 3, 2, 9, 9, 3, 5, 5, 9, 11, 11, 10, 3, 11, 9, 11, 6, 6, 6, 9, 8, 5, 7, 2, 10, 4, 6, 11, 10, 8, 2, 9, 4, 5, 6, 7, 3, 2, 5, 5, 11, 4, 6, 5, 2, 10, 9, 11, 6, 8, 8, 7, 10, 6, 11, 9, 3, 6, 7, 5, 9, 11, 6, 4, 6, 3, 9, 4, 11, 3, 5, 10, 3, 9, 10, 4, 4, 2, 9, 8, 11, 2, 6, 10, 10, 2, 9, 8, 5, 4, 10, 6, 11, 11, 7, 5, 3, 6, 9, 8, 2, 4, 3, 3, 3, 6, 4, 5, 2, 5, 3, 11, 10, 6, 2, 3, 4, 4, 4, 2, 4, 3, 10, 5, 2, 2, 8, 8, 8, 6, 2, 3, 6, 6, 9, 6, 3, 5, 2, 8, 4, 3, 7, 3, 11, 6, 6, 9, 6, 11, 3, 4, 10, 2, 5, 7, 8, 9, 8, 11, 11, 8, 3, 10, 3, 5, 5, 3, 3, 2, 11, 3, 5, 10, 2, 7, 4, 10, 6, 8, 10, 10, 4, 10, 10, 5, 8, 3, 5, 7, 8, 5, 5, 3, 3, 6, 3, 3, 7, 2, 7, 8, 3, 5, 2, 10, 10, 9, 6, 10, 7, 5, 2, 8, 10, 7, 5, 8, 10, 3, 10, 7, 11, 9, 10, 6, 11, 6, 5, 10, 2, 8, 4, 7, 7, 6, 10, 5, 6, 5, 10, 2, 3, 2, 7, 11, 5, 3, 2, 2, 10, 2, 9, 2, 6, 6, 11, 4, 5, 10, 2, 7, 3, 3, 10, 7, 11, 5, 8, 7, 8, 4, 2, 6, 6, 9, 2, 4, 6, 8, 5, 3, 5, 11, 9, 3, 10, 11, 7, 10, 10, 5, 3, 4, 2, 6, 3, 11, 2, 6, 3, 7, 6, 6, 5, 8, 3, 11, 11, 11, 5, 4, 11, 9, 10, 9, 9, 10, 6, 10, 4, 2, 8, 6, 10, 10, 7, 11, 11, 3, 3, 11, 2, 3, 6, 3, 8, 6, 2, 10, 3, 6, 3, 5, 2, 5, 7, 11, 6, 2, 9, 9, 3, 8, 11, 2, 5, 11, 7, 10, 3, 6, 6, 9, 3, 5, 2, 6, 4, 11, 8, 4, 3, 11, 8, 3, 8, 7, 6, 8, 8, 8, 7, 7, 9, 4, 7, 10, 9, 6, 5, 9, 5, 3, 9, 4, 4, 10, 4, 2, 10, 2, 2, 8, 9, 6, 7, 7, 11, 8, 7, 8, 3, 4, 2, 6, 4, 10, 2, 4, 5, 8, 5, 6, 9, 2, 10, 7, 5, 6, 5, 5, 2, 9, 6, 5, 8, 2, 2, 4, 2, 2, 4, 6, 10, 7, 5, 6, 4, 6, 11, 3, 6, 4, 6, 9, 11, 7, 4, 7, 3, 7, 10, 5, 8, 7, 7, 3, 8, 8, 8, 2, 4, 9, 2, 3, 8, 3, 4, 2, 11, 11, 5, 4, 3, 8, 9, 8, 4, 4, 4, 7, 8, 10, 5, 10, 3, 4, 2, 6, 6, 4, 2, 4, 6, 2, 2, 6, 7, 5, 8, 8, 3, 5, 5, 11, 11, 2, 6, 5, 5, 3, 9, 9, 10, 7, 2, 11, 6, 3, 3, 6, 2, 5, 4, 2, 4, 10, 4, 11, 6, 7, 9, 11, 2, 3, 8, 3, 3, 8, 5, 3, 5, 10, 8, 11, 8, 2, 4, 11, 4, 2, 3, 10, 3, 10, 6, 9, 9, 4, 4, 7, 6, 3, 11, 10, 5, 5, 9, 5, 3, 7, 4, 8, 4, 2, 8, 6, 3, 7, 7, 3, 6, 11, 10, 4, 9, 4, 5, 11, 5, 11, 9, 3, 5, 3, 11, 4, 6, 2, 11, 2, 4, 10, 6, 5, 4, 4, 11, 5, 4, 4, 11, 3, 8, 6, 3, 8, 9, 10, 8, 7, 3, 9, 9, 11, 6, 8, 2, 4, 4, 3, 7, 2, 2, 5, 5, 2, 4, 11, 11, 5, 4, 2, 6, 9, 11, 5, 5, 10, 2, 4, 7, 3, 10, 2, 8, 4, 8, 8, 5, 4, 2, 9, 3, 2, 11, 2, 9, 10, 8, 6, 7, 11, 2, 11, 7, 8, 2, 3, 10, 4, 9, 7, 6, 11, 7, 10, 2, 7, 4, 2, 6, 8, 8, 5, 8, 3, 5, 7, 9, 9, 5, 2, 5, 2, 10, 9, 3, 2, 2, 6, 8, 11, 10, 3, 2, 3, 11, 4, 8, 5, 5, 8, 5, 2, 9, 10, 6, 5, 11, 7, 8, 11, 9, 9, 2, 6, 10, 3, 3, 2, 9, 5, 7, 9, 4, 10, 4, 2, 4, 10, 10, 7, 2, 2, 3, 3, 2, 7, 9, 3, 4, 7, 11, 2, 10, 10, 9, 5, 4, 11, 10, 11, 5, 7, 5, 10, 4, 11, 6, 3, 4, 7, 10, 11, 7, 11, 5, 11, 4, 5, 6, 10, 10, 2, 9, 7, 3, 3, 9, 11, 4, 7, 9, 5, 7, 10, 4, 7, 10, 3, 11, 3, 3, 2, 8, 10, 4, 10, 7, 6, 4, 4, 7, 4, 6, 6, 2, 3, 9, 8, 9, 9, 3, 7, 11, 10, 9, 9, 7, 4, 3, 5, 10, 10, 9, 11, 11, 3, 5, 3, 10, 2, 9, 10, 6, 11, 7, 10, 5, 9, 3, 11, 6, 4, 3, 3, 6, 3, 9, 10, 3, 7, 6, 4, 11, 3, 6, 8, 5, 7, 3, 3, 7, 10, 9, 4, 8, 2, 11, 7, 5, 6, 7, 2, 5, 4, 8, 9, 8, 5, 10, 6, 10, 5, 11, 7, 10, 3, 2, 8, 3, 6, 11, 2, 3, 8, 10, 8, 3, 6, 6, 8, 3, 6, 6, 3, 7, 5, 2, 6, 5, 7, 11, 2, 3, 6, 9, 8, 2, 5, 2, 2, 6, 9, 3, 2, 7, 2, 4, 10, 11, 7, 8, 2, 11, 2, 2, 3, 11, 2, 9, 6, 6, 2, 7, 8, 5, 11, 4, 2, 7, 11, 4, 5, 2, 11, 4, 3, 11, 10, 8, 3, 9, 9, 5, 9, 6, 10, 3, 3, 7, 3, 2, 8, 4, 3, 6, 8, 7, 2, 8, 2, 7, 9, 4, 5, 8, 6, 8, 5, 7, 3, 4, 11, 2, 7, 7, 6, 5, 6, 11, 9, 10, 4, 6, 11, 5, 7, 5, 9, 7, 4, 5, 2, 8, 5, 10, 6, 9, 3, 8, 6, 5, 2, 10, 6, 11, 8, 10, 10, 11, 9, 3, 10, 2, 8, 6, 3, 8, 11, 3, 5, 3, 11, 9, 11, 7, 9, 10, 3, 7, 8, 11, 3, 2, 10, 8, 11, 9, 2, 5, 8, 7, 8, 11, 11, 4, 4, 7, 2, 2, 4, 5, 10, 8, 11, 3, 2, 4, 10, 2, 8, 11, 3, 11, 4, 11, 3, 7, 4, 2, 8, 9, 4, 10, 5, 5, 8, 7, 7, 5, 3, 3, 8, 3, 6, 5, 2, 2, 4, 3, 3, 3, 5, 10, 7, 4, 4, 9, 6, 9, 7, 7, 9, 3, 3, 11, 3, 8, 5, 6, 10, 5, 2, 7, 3, 2, 5, 4, 6, 11, 6, 11, 9, 4, 4, 10, 6, 7, 8, 3, 8, 5, 7, 8, 6, 7, 8, 2, 6, 9, 6, 6, 2, 3, 4, 3, 3, 4, 8, 2, 11, 4, 4, 6, 5, 2, 8, 3, 2, 4, 9, 5, 8, 7, 4, 6, 8, 10, 11, 8, 11, 11, 3, 10, 8, 6, 10, 5, 5, 6, 4, 5, 4, 6, 7, 10, 8, 8, 3, 11, 3, 4, 11, 7, 7, 6, 2, 11, 7, 2, 5, 8, 5, 5, 9, 7, 10, 10, 4, 6, 4, 10, 2, 6, 7, 3, 10, 8, 5, 10, 11, 4, 5, 2, 5, 7, 8, 5, 9, 11, 5, 5, 8, 3, 8, 5, 2, 6, 6, 3, 8, 6, 9, 2, 2, 7, 8, 11, 9, 7, 8, 10, 6, 7, 9, 5, 3, 4, 4, 6, 8, 10, 9, 9, 2, 6, 9, 11, 4, 2, 2, 11, 2, 6, 10, 6, 7, 6, 4, 6, 6, 8, 7, 5, 9, 7, 4, 2, 4, 10, 2, 3, 6, 11, 2, 6, 4, 5, 3, 8, 2, 9, 9, 3, 5, 8, 8, 10, 7, 11, 7, 5, 6, 4, 5, 9, 2, 4, 4, 10, 8, 11, 9, 6, 4, 5, 5, 7, 2, 9, 2, 8, 7, 9, 6, 6, 6, 9, 8, 7, 4, 6, 4, 3, 11, 10, 8, 10, 4, 11, 2, 7, 3, 5, 3, 3, 4, 10, 5, 4, 3, 10, 2, 6, 2, 11, 6, 9, 4, 9, 2, 5, 6, 3, 2, 9, 11, 7, 2, 3, 4, 4, 5, 2, 6, 11, 6, 3, 8, 4, 5, 10, 2, 3, 11, 4, 6, 11, 2, 10, 4, 9, 8, 9, 11, 3, 11, 10, 8, 4, 4, 4, 9, 10, 8, 3, 4, 4, 9, 5, 7, 9, 5, 7, 5, 7, 6, 8, 2, 6, 11, 7, 2, 6, 2, 8, 7, 4, 10, 3, 10, 6, 4, 10, 9, 2, 2, 9, 11, 10, 5, 8, 7, 9, 11, 8, 5, 2, 8, 10, 6, 6, 8, 11, 4, 5, 6, 6, 10, 11, 4, 6, 6, 11, 3, 10, 7, 3, 4, 10, 3, 8, 10, 2, 11, 2, 7, 5, 6, 2, 8, 8, 9, 4, 4, 11, 4, 8, 8, 2, 5, 9, 4, 8, 4, 9, 3, 8, 11, 11, 9, 5, 3, 9, 10, 8, 3, 7, 3, 3, 9, 2, 11, 3, 5, 6, 6, 6, 4, 9, 3, 4, 8, 9, 9, 8, 9, 2, 8, 6, 2, 4, 8, 5, 3, 10, 4, 4, 2, 9, 4, 6, 5, 4, 10, 5, 4, 10, 8, 7, 6, 3, 3, 10, 10, 11, 2, 4, 9, 6, 5, 7, 6, 7, 8, 9, 6, 7, 11, 3, 11, 11, 4, 7, 5, 7, 3, 7, 8, 10, 11, 4, 3, 10, 4, 6, 7, 10, 5, 5, 6, 9, 10, 4, 3, 3, 3, 8, 11, 10, 3, 5, 7, 4, 11, 11, 8, 8, 7, 5, 5, 6, 3, 7, 4, 10, 4, 5, 7, 3, 5, 2, 6, 4, 2, 3, 8, 2, 4, 3, 9, 3, 3, 11, 2, 7, 3, 2, 2, 3, 2, 6, 4, 9, 7, 9, 8, 9, 2, 11, 5, 2, 7, 4, 10, 2, 2, 5, 8, 2, 8, 6, 8, 2, 4, 2, 10, 6, 10, 6, 5, 4, 8, 7, 2, 6, 3, 10, 3, 5, 6, 7, 6, 8, 11, 11, 2, 3, 10, 6, 5, 11, 8, 7, 6, 6, 4, 2, 2, 7, 3, 8, 2, 3, 10, 4, 7, 2, 9, 11, 8, 7, 10, 10, 3, 9, 6, 3, 11, 5, 2, 10, 5, 2, 5, 9, 11, 6, 10, 11, 10, 2, 6, 4, 10, 7, 10, 5, 2, 10, 8, 6, 9, 8, 7, 3, 10, 11, 3, 3, 8, 10, 11, 2, 9, 7, 5, 4, 8, 7, 10, 3, 2, 10, 2, 9, 2, 5, 4, 7, 5, 11, 10, 6, 10, 5, 11, 2, 9, 9, 5, 10, 11, 5, 5, 7, 11, 6, 2, 4, 2, 6, 7, 9, 9, 9, 10, 11, 3, 8, 11, 9, 7, 4, 11, 4, 8, 5, 9, 11, 11, 4, 8, 7, 10, 6, 11, 3, 9, 6, 6, 3, 8, 3, 11, 5, 7, 6, 6, 4, 6, 11, 9, 8, 4, 5, 5, 3, 5, 11, 9, 8, 5, 5, 5, 4, 11, 6, 10, 11, 6, 3, 2, 2, 9, 11, 7, 11, 6, 4, 10, 8, 11, 10, 8, 3, 4, 8, 2, 6, 4, 7, 11, 7, 2, 11, 7, 5, 10, 8, 2, 2, 7, 9, 5, 6, 6, 5, 10, 4, 10, 3, 11, 8, 10, 7, 10, 2, 11, 5, 8, 6, 7, 9, 6, 7, 8, 6, 11, 11, 2, 8, 10, 9, 4, 7, 4, 9, 2, 5, 7, 6, 6, 4, 5, 3, 2, 5, 10, 7, 3, 7, 5, 9, 5, 9, 7, 11, 3, 4, 9, 5, 3, 6, 9, 2, 2, 4, 4, 3, 7, 2, 7, 3, 5, 4, 2, 10, 6, 8, 2, 3, 2, 10, 4, 7, 8, 8, 8, 10, 11, 6, 2, 11, 6, 4, 5, 5, 7, 7, 9, 5, 10, 9, 10, 7, 5, 6, 2, 7, 11, 6, 2, 5, 2, 11, 9, 5, 5, 11, 5, 9, 10, 9, 6, 11, 2, 7, 5, 8, 5, 11, 8, 8, 9, 3, 7, 6, 5, 4, 8, 3, 4, 10, 11, 8, 5, 3, 5, 8, 3, 8, 5, 6, 11, 11, 4, 10, 4, 2, 8, 7, 10, 7, 7, 7, 3, 5, 2, 4, 8, 9, 2, 6, 10, 4, 6, 6, 3, 6, 9, 11, 10, 10, 8, 9, 11, 7, 5, 10, 2, 11, 5, 11, 4, 7, 8, 8, 7, 7, 9, 7, 4, 7, 11, 7, 4, 6, 10, 7, 4, 10, 8, 11, 3, 10, 3, 8, 9, 7, 6, 7, 6, 7, 9, 7, 2, 7, 10, 6, 8, 4, 5, 9, 3, 6, 11, 3, 2, 4, 6, 5, 11, 2, 4, 11, 6, 6, 4, 8, 11, 3, 9, 5, 5, 6, 2, 6, 3, 3, 8, 7, 11, 4, 8, 10, 4, 10, 11, 6, 5, 3, 7, 9, 9, 6, 5, 9, 3, 6, 4, 2, 4, 11, 3, 9, 6, 7, 10, 5, 10, 10, 6, 8, 9, 9, 7, 3, 4, 9, 10, 4, 8, 6, 11, 2, 8, 4, 8, 11, 9, 4, 7, 6, 10, 8, 5, 4, 6, 2, 11, 5, 10, 7, 10, 5, 4, 3, 4, 7, 10, 4, 11, 9, 10, 3, 5, 5, 3, 7, 3, 3, 5, 4, 11, 2, 7, 7, 6, 5, 2, 5, 2, 3, 4, 4, 8, 4, 10, 11, 10, 8, 9, 11, 6, 11, 2, 5, 8, 11, 11, 3, 10, 3, 3, 5, 5, 7, 3, 9, 5, 9, 8, 5, 4, 2, 7, 9, 9, 3, 7, 4, 10, 5, 4, 7, 10, 10, 4, 4, 11, 2, 6, 7, 3, 8, 3, 7, 8, 4, 5, 3, 8, 2, 8, 11, 9, 10, 8, 5, 4, 10, 5, 3, 10, 4, 11, 3, 9, 9, 5, 10, 7, 2, 2, 3, 4, 5, 5, 4, 11, 11, 10, 6, 6, 10, 6, 7, 8, 9, 2, 6, 6, 11, 4, 4, 7, 5, 2, 10, 2, 9, 2, 9, 8, 2, 11, 3, 2, 3, 9, 3, 8, 2, 11, 2, 9, 9, 3, 8, 8, 8, 4, 11, 7, 6, 10, 5, 10, 4, 10, 8, 6, 2, 2, 6, 2, 8, 7, 5, 11, 9, 10, 2, 4, 5, 5, 5, 2, 8, 2, 4, 4, 11, 7, 10, 5, 3, 3, 6, 9, 3, 4, 2, 4, 10, 11, 5, 4, 2, 8, 8, 2, 3, 7, 5, 4, 3, 2, 3, 6, 3, 11, 5, 4, 7, 7, 7, 5, 10, 8, 11, 7, 8, 7, 7, 7, 6, 3, 4, 4, 10, 7, 5, 3, 2, 5, 6, 6, 7, 8, 6, 9, 2, 11, 6, 8, 10, 2, 6, 8, 7, 10, 6, 6, 11, 7, 4, 11, 2, 3, 4, 11, 4, 11, 9, 9, 7, 10, 2, 11, 10, 11, 10, 7, 2, 10, 5, 6, 9, 6, 10, 2, 2, 4, 10, 2, 9, 3, 4, 10, 11, 3, 9, 8, 2, 10, 2, 10, 7, 4, 4, 7, 3, 3, 7, 10, 4, 6, 11, 11, 10, 8, 8, 10, 6, 4, 6, 2, 5, 11, 3, 2, 8, 5, 4, 8, 6, 9, 10, 5, 2, 11, 6, 9, 11, 3, 9, 7, 4, 5, 8, 8, 3, 8, 6, 4, 8, 11, 7, 3, 6, 10, 8, 4, 6, 11, 9, 4, 3, 7, 11, 7, 11, 4, 3, 10, 4, 11, 7, 2, 2, 3, 10, 11, 3, 3, 10, 2, 9, 9, 2, 6, 6, 2, 2, 9, 9, 11, 7, 5, 8, 6, 8, 7, 6, 3, 5, 5, 7, 2, 6, 6, 8, 4, 6, 8, 11, 3, 3, 6, 9, 6, 11, 2, 3, 6, 5, 6, 11, 4, 2, 8, 8, 3, 4, 3, 6, 9, 4, 2, 3, 6, 3, 7, 10, 2, 8, 9, 4, 11, 5, 8, 10, 4, 7, 4, 11, 2, 7, 2, 8, 8, 11, 2, 8, 11, 7, 7, 4, 10, 3, 9, 7, 5, 8, 10, 9, 10, 9, 2, 2, 5, 5, 4, 11, 11, 6, 6, 9, 6, 4, 8, 4, 10, 5, 9, 7, 2, 4, 11, 10, 6, 11, 7, 2, 7, 5, 4, 5, 10, 6, 10, 4, 8, 9, 9, 6, 5, 7, 5, 11, 6, 11, 4, 5, 7, 8, 2, 8, 8, 3, 2, 6, 4, 8, 7, 3, 4, 9, 7, 9, 3, 10, 7, 10, 6, 11, 3, 6, 2, 2, 7, 7, 7, 10, 5, 5, 5, 5, 3, 3, 10, 4, 2, 4, 10, 6, 4, 5, 10, 9, 3, 10, 9, 2, 7, 8, 9, 3, 2, 3, 10, 5, 3, 3, 6, 10, 3, 3, 8, 5, 10, 5, 6, 9, 2, 6, 10, 2, 11, 3, 6, 7, 3, 2, 6, 8, 7, 2, 10, 5, 10, 9, 2, 3, 2, 7, 4, 11, 8, 3, 9, 10, 10, 6, 7, 7, 9, 6, 8, 4, 5, 2, 5, 7, 3, 4, 4, 6, 6, 9, 10, 5, 6, 7, 4, 6, 8, 7, 7, 8, 4, 2, 2, 5, 2, 11, 9, 3, 6, 6, 8, 4, 5, 9, 4, 6, 8, 8, 9, 6, 2, 2, 9, 7, 7, 11, 7, 8, 3, 2, 4, 4, 4, 7, 7, 7, 5, 11, 2, 4, 2, 9, 2, 5, 9, 9, 5, 3, 6, 8, 7, 4, 8, 10, 4, 5, 7, 3, 6, 3, 4, 10, 10, 10, 5, 6, 6, 6, 3, 3, 7, 9, 4, 8, 4, 6, 10, 9, 4, 3, 6, 5, 4, 4, 5, 7, 6, 10, 4, 8, 10, 11, 8, 11, 11, 7, 3, 5, 9, 5, 9, 8, 4, 10, 5, 9, 3, 6, 10, 5, 10, 10, 9, 11, 4, 7, 9, 3, 10, 10, 5, 9, 10, 3, 2, 4, 11, 10, 9, 4, 4, 10, 7, 2, 4, 3, 3, 4, 9, 8, 3, 6, 8, 3, 7, 10, 11, 5, 6, 2, 4, 2, 2, 2, 6, 3, 7, 9, 4, 5, 2, 9, 10, 9, 5, 10, 2, 5, 6, 9, 2, 7, 5, 7, 6, 2, 5, 6, 6, 3, 2, 4, 10, 2, 6, 9, 7, 7, 5, 4, 11, 4, 6, 10, 3, 4, 2, 7, 8, 9, 6, 8, 9, 11, 4, 7, 5, 3, 7, 11, 3, 8, 8, 4, 6, 10, 11, 8, 9, 6, 9, 3, 6, 8, 2, 5, 10, 8, 6, 7, 6, 6, 2, 6, 7, 2, 8, 3, 2, 11, 11, 10, 11, 11, 3, 3, 2, 7, 10, 2, 10, 3, 3, 2, 6, 5, 8, 4, 5, 10, 9, 8, 3, 4, 9, 9, 3, 4, 9, 5, 6, 7, 5, 8, 9, 6, 10, 6, 4, 10, 2, 7, 5, 2, 5, 7, 5, 10, 9, 5, 4, 9, 5, 4, 3, 11, 9, 3, 3, 4, 7, 9, 3, 3, 10, 7, 8, 9, 2, 6, 9, 6, 3, 10, 9, 11, 11, 5, 8, 9, 5, 9, 9, 7, 7, 3, 9, 11, 3, 3, 9, 11, 8, 7, 9, 4, 6, 5, 4, 8, 6, 8, 2, 8, 5, 6, 5, 3, 2, 11, 5, 2, 3, 6, 2, 10, 3, 10, 4, 7, 4, 6, 6, 7, 6, 5, 8, 8, 8, 5, 4, 6, 7, 4, 9, 6, 5, 2, 8, 10, 8, 4, 11, 8, 9, 5, 5, 7, 9, 4, 11, 8, 5, 9, 6, 8, 8, 5, 5, 6, 9, 4, 5, 8, 5, 11, 2, 11, 11, 5, 8, 9, 3, 9, 4, 7, 3, 8, 9, 7, 5, 9, 7, 3, 5, 9, 11, 2, 3, 6, 2, 2, 8, 6, 11, 11, 9, 2, 4, 10, 3, 2, 6, 10, 9, 5, 7, 11, 11, 6, 3, 6, 2, 7, 6, 7, 8, 2, 10, 10, 10, 9, 2, 3, 10, 9, 6, 4, 11, 4, 7, 5, 9, 11, 4, 4, 4, 9, 6, 5, 7, 10, 5, 10, 2, 2, 3, 5, 6, 7, 4, 5, 11, 2, 4, 6, 8, 8, 4, 2, 4, 11, 3, 2, 5, 9, 9, 8, 3, 10, 2, 8, 2, 11, 4, 8, 8, 5, 8, 7, 7, 2, 2, 4, 8, 9, 2, 7, 9, 2, 7, 11, 2, 11, 9, 9, 2, 5, 10, 9, 7, 5, 10, 11, 3, 2, 5, 4, 8, 2, 8, 3, 6, 5, 11, 6, 8, 9, 11, 7, 10, 5, 6, 8, 3, 8, 8, 5, 8, 6, 5, 7, 5, 4, 7, 8, 2, 5, 5, 11, 6, 5, 8, 9, 5, 4, 7, 3, 5, 5, 2, 7, 10, 7, 11, 6, 4, 10, 4, 5, 8, 2, 9, 6, 10, 5, 8, 4, 11, 2, 9, 7, 3, 9, 4, 2, 11, 8, 7, 10, 7, 6, 10, 5, 10, 4, 11, 9, 4, 2, 6, 4, 3, 11, 8, 3, 5, 10, 8, 2, 5, 6, 11, 5, 9, 8, 10, 3, 8, 7, 6, 5, 5, 7, 5, 6, 4, 5, 6, 4, 4, 3, 10, 4, 10, 3, 7, 6, 5, 5, 9, 10, 8, 10, 3, 7, 4, 8, 7, 2, 8, 9, 8, 9, 8, 9, 8, 5, 3, 8, 3, 11, 7, 9, 2, 3, 4, 8, 9, 8, 6, 3, 6, 7, 9, 8, 7, 9, 3, 2, 11, 9, 8, 6, 5, 4, 10, 5, 7, 3, 5, 11, 7, 3, 7, 10, 4, 9, 6, 6, 6, 3, 8, 2, 7, 8, 5, 4, 5, 2, 10, 7, 10, 10, 3, 6, 10, 8, 3, 7, 3, 9, 4, 4, 6, 6, 10, 11, 2, 7, 3, 6, 11, 9, 10, 5, 6, 5, 8, 11, 7, 6, 11, 8, 6, 7, 2, 11, 4, 8, 5, 10, 11, 11, 2, 6, 3, 7, 2, 3, 6, 9, 9, 10, 4, 5, 2, 11, 10, 2, 7, 5, 2, 7, 5, 2, 8, 4, 4, 11, 7, 7, 11, 6, 11, 3, 7, 7, 2, 9, 2, 2, 10, 7, 4, 8, 4, 7, 9, 2, 10, 5, 9, 9, 8, 2, 2, 4, 5, 10, 2, 7, 9, 9, 11, 10, 5, 8, 2, 6, 6, 5, 2, 10, 2, 9, 6, 11, 2, 8, 4, 9, 2, 11, 7, 9, 3, 2, 2, 2, 4, 11, 6, 10, 8, 2, 5, 5, 10, 7, 8, 3, 8, 2, 2, 4, 3, 10, 8, 2, 3, 9, 3, 6, 4, 7, 8, 11, 3, 11, 7, 11, 2, 3, 3, 3, 11, 5, 4, 8, 9, 8, 3, 10, 7, 10, 6, 6, 11, 6, 11, 2, 6, 11, 5, 2, 10, 2, 9, 10, 3, 3, 3, 11, 3, 10, 8, 7, 5, 3, 2, 11, 7, 8, 10, 6, 4, 11, 10, 2, 5, 8, 2, 3, 6, 8, 8, 3, 8, 8, 2, 11, 9, 9, 5, 7, 5, 2, 9, 5, 5, 11, 5, 2, 2, 9, 10, 4, 8, 11, 9, 7, 9, 6, 3, 7, 9, 8, 4, 9, 9, 3, 6, 2, 3, 9, 11, 9, 8, 7, 4, 9, 6, 7, 8, 3, 10, 5, 11, 4, 7, 7, 2, 7, 2, 7, 4, 8, 9, 10, 3, 4, 9, 2, 6, 9, 8, 9, 9, 4, 8, 3, 5, 2, 11, 3, 9, 3, 6, 5, 5, 9, 8, 2, 9, 7, 6, 7, 2, 8, 8, 8, 11, 11, 7, 9, 3, 8, 6, 5, 9, 11, 5, 2, 3, 7, 5, 3, 9, 5, 4, 6, 2, 9, 8, 7, 11, 5, 8, 9, 11, 2, 10, 7, 2, 6, 7, 5, 7, 8, 8, 8, 5, 7, 2, 8, 4, 8, 7, 5, 8, 9, 8, 4, 10, 9, 2, 11, 10, 5, 8, 4, 9, 11, 5, 6, 4, 10, 7, 9, 2, 5, 4, 11, 9, 5, 6, 11, 11, 10, 9, 8, 9, 9, 9, 11, 2, 10, 4, 11, 2, 6, 11, 10, 3, 2, 3, 5, 5, 10, 9, 9, 10, 8, 3, 10, 6, 10, 9, 7, 2, 5, 11, 5, 7, 4, 10, 9, 7, 6, 7, 2, 2, 4, 5, 5, 6, 7, 6, 10, 5, 8, 8, 9, 8, 4, 11, 3, 4, 6, 6, 3, 5, 9, 2, 11, 7, 9, 9, 6, 2, 9, 5, 6, 7, 7, 3, 2, 6, 3, 10, 7, 6, 2, 5, 11, 2, 2, 8, 11, 11, 10, 9, 11, 11, 6, 10, 5, 7, 5, 9, 8, 9, 4, 9, 4, 7, 11, 2, 4, 10, 2, 3, 6, 11, 5, 3, 7, 3, 11, 8, 9, 6, 4, 9, 3, 10, 11, 3, 9, 5, 6, 5, 11, 10, 4, 8, 4, 2, 7, 3, 10, 10, 2, 2, 9, 5, 4, 2, 4, 8, 11, 7, 4, 2, 8, 5, 8, 8, 11, 5, 5, 3, 4, 7, 6, 8 };
						int an[8192];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
				}
			}
		}
	}
	system("pause");
	outputFile.close();
}

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type) {

	/*
	This subroutine determines how many additional runs each
	design will should have for next iteration of simulation.
	s_mean[i]: sample mean of design i, i=0,1,..,ND-1
	s_var[i]: sample variance of design i, i=0,1,..,ND-1
	nd: the number of designs
	n[i]: number of simulation replications of design i,
	i=0,1,..,ND-1
	add_budget: the additional simulation budget
	an[i]: additional number of simulation replications assigned
	to design i, i=0,1,..,ND-1
	type: type of optimazition problem. type=1, MIN problem;
	type=2, MAX problem
	*/
	int i;
	int b, s;
	int t_budget, t1_budget;
	int morerun[8192], more_alloc; /* 1:Yes; 0:No */
	float t_s_mean[8192];
	float ratio[8192]; /* Ni/Ns */
	float ratio_s, temp;
	if (type == 1) { /*MIN problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = s_mean[i];
	}
	else { /*MAX problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = (-1)*s_mean[i];
	}
	t_budget = add_budget;
	for (i = 0; i < nd; i++) t_budget += n[i];
	b = best(t_s_mean, nd);
	s = second_best(t_s_mean, nd, b);
	ratio[s] = 1.0;
	for (i = 0; i < nd; i++)
		if (i != s && i != b) {
			temp = (t_s_mean[b] - t_s_mean[s]) / (t_s_mean[b] - t_s_mean[i]);
			ratio[i] = temp*temp*s_var[i] / s_var[s];
		} /* calculate ratio of Ni/Ns*/
	temp = 0;
	for (i = 0; i < nd; i++) if (i != b) temp += (ratio[i] * ratio[i] / s_var[i]);
	ratio[b] = sqrt(s_var[b] * temp); /* calculate Nb */
	for (i = 0; i < nd; i++) morerun[i] = 1;
	t1_budget = t_budget;
	do{
		more_alloc = 0;
		ratio_s = 0.0;
		for (i = 0; i < nd; i++) if (morerun[i]) ratio_s += ratio[i];
		for (i = 0; i < nd; i++) if (morerun[i])
		{
			an[i] = (int)(t1_budget / ratio_s*ratio[i]);
			/* disable those design which have been run too much */
			if (an[i] < n[i])
			{
				an[i] = n[i];
				morerun[i] = 0;
				more_alloc = 1;
			}
		}
		if (more_alloc)
		{
			t1_budget = t_budget;
			for (i = 0; i < nd; i++) if (!morerun[i]) t1_budget -= an[i];
		}
	} while (more_alloc); /* end of WHILE */
	/* calculate the difference */
	t1_budget = an[0];
	for (i = 1; i < nd; i++) t1_budget += an[i];
	an[b] += (t_budget - t1_budget); /* give the difference
									 to design b */
	for (i = 0; i < nd; i++) an[i] -= n[i];
}

int best(float t_s_mean[], int nd) {
	/*This function determines the best design based on current
	simulation results */
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs */

	int i, min_index;
	min_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[min_index]) {
			min_index = i;
		}
	}
	return min_index;
}

int second_best(float t_s_mean[], int nd, int b) {
	/*This function determines the second best design based on
	current simulation results*/
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs.
	b: current best design determined by function
	best() */
	int i, second_index;
	if (b == 0) second_index = 1;
	else second_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[second_index] && i != b) {
			second_index = i;
		}
	}
	return second_index;
}