#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <cuda.h>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include <fstream>
#include <random>
#include <time.h>

/* please put the following definitions at the head of your
program */
/*Define parameters for simulation*/
/*the number of designs. It should be larger than 2.*/
#define GRIDDIMENSION 1
#define BLOCKDIMENSION 32
#define TYPE 1
/*maximization TYPE = -1, minimization TYPE = 1*/
/*prototypes of functions and subroutines included in this
file*/

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type);
int best(float t_s_mean[], int nd);
int second_best(float t_s_mean[], int nd, int b);

#ifdef _WIN32
#include <Windows.h>
double get_wall_time(){
	LARGE_INTEGER time, freq;
	if (!QueryPerformanceFrequency(&freq)){
		//  Handle error
		return 0;
	}
	if (!QueryPerformanceCounter(&time)){
		//  Handle error
		return 0;
	}
	return (double)time.QuadPart / freq.QuadPart;
}

double get_cpu_time(){
	FILETIME a, b, c, d;
	if (GetProcessTimes(GetCurrentProcess(), &a, &b, &c, &d) != 0){
		//  Returns total user time.
		return
			(double)(d.dwLowDateTime |
			((unsigned long long)d.dwHighDateTime << 32)) * 0.0000001;
	}
	else{
		//  Handle error
		return 0;
	}
}
#endif


__device__ inline void atomicFloatSub(float *address, float val) {
	int i_val = __float_as_int(val);
	int tmp0 = 0;
	int tmp1;

	while ((tmp1 = atomicCAS((int *)address, tmp0, i_val)) != tmp0)	{
		tmp0 = tmp1;
		i_val = __float_as_int(__int_as_float(tmp1) - val);
	}
}

__global__ void parallelOCBA3(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	float temp = 0;
	unsigned int i = 0;

	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//use previous alpha of best
		if (lamda[index] == 0) {
			lamda[index] = *alphaB;
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//use previous rho
		*rho = *rhoPrevious;
		d_t[1] = d_t[0];
		//	__syncthreads();

		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];
	}
}

__global__ void parallelOCBA_1(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 2: find maximum
		*best = -FLT_MAX;
		float temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		unsigned int i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		*alphaB = *best;

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rhoPrevious = d_t[0] / *summation;
	}
}

__global__ void parallelOCBA2(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	float temp = 0;
	unsigned int i = 0;

	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//use previous alpha of best
		if (lamda[index] == 0) {
			lamda[index] = *alphaB;
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//use previous rho
		*rho = *rhoPrevious;
		d_t[1] = d_t[0];
		//	__syncthreads();

		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];

		//step 2: find maximum
		*best = -FLT_MAX;
		temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rho = d_t[0] / *summation;
	}
}

__global__ void parallelOCBA1(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 2: find maximum
		*best = -FLT_MAX;
		float temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		unsigned int i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rho = d_t[0] / *summation;
		d_t[1] = d_t[0];
		//	__syncthreads();


		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];
	}
}

void simulatorEmulator(int an[], int nd, double xToAdd[], double xSquareToAdd[]) {
	//test case: i=0 is best design
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		for (int j = 0; j < an[i]; j++) {
			std::default_random_engine generator;
			std::normal_distribution<double> distribution(-900 + (100 * i), 50);
			temp = distribution(generator);
			xToAdd[i] += temp;
			xSquareToAdd[i] += (temp*temp);
		}
	}
	return;
}

void updateMeanVariance(int nd, int an[], int n[], float mean[], float var[], double xCurrent[], double xSquareCurrent[], double xToAdd[], double xSquareToAdd[]) {
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		mean[i] = (n[i] * mean[i] + xCurrent[i]) / (n[i] + an[i]);
		xCurrent[i] += xToAdd[i];
		xSquareCurrent[i] += xSquareToAdd[i];
		var[i] = (xSquareCurrent[i] - (xCurrent[i] * xCurrent[i] / (n[i] + an[i]))) / (n[i] + an[i] - 1);
	}
	return;
}



void main()
{
	srand(time(NULL));
	std::ofstream outputFile;
	outputFile.open("results.csv");
	int budget[1] = { 1000 };
	int candidates[10] = { 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192 };
	//int budget[6] = { 10, 100, 1000, 10000, 100000, 1000000 };
	for (int k = 0; k < 1; k++) {
		int ADD_BUDGET = budget[k]; /*the additional simulation
									budget. It should be positive integer.*/
		outputFile << std::endl << "Budget" << "," << budget[k] << std::endl;
		for (int j = 0; j < 50; j++) {
			for (int l = 0; l < 10; l++) {
				outputFile << std::endl << "Candidate" << "," << candidates[l] << std::endl;
				for (int k = 0; k < 4; k++) {
					outputFile << std::endl << "Config" << "," << k << std::endl;

					int i;

					if (l == 0) {
						float s_mean[16] = { -891.43, -778.404, -672.865, -465.137, -458.288, -496.536, -210.198, -218.259, -176.049, -261.049, 149.1107, 248.7619, 329.7235, 260.4459, 466.6979, 656.9188 };
						float s_var[16] = { 30369.75261, 10515.76669, 14897.38777, 12937.74895, 32876.36093, 39461.76848, 33378.26336, 60845.15336, 44810.58394, 51579.91784, 27141.06839, 38102.93198, 88345.47724, 69870.57323, 45359.79586, 28824.56514 };
						int n[16] = { 8, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[16];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 1) {
						float s_mean[32] = { -995.3, -619.617, -904.904, -209.754, -268.715, -751.091, -715.262, 507.553, -811.945, 523.789, 641.285, -681.082, 822.138, 369.61, 912.717, -652.76, -745.293, -982.177, -53.1938, -885.983, 195.166, 992.981, 730.583, -108.982, 604.968, 589.404, 678.64, -698.172, -888.119, -402.203, -827.204, -826.716 };
						float s_var[32] = { 165.648, 22.6753, 40.3882, 17.3406, 63.4541, 25.2571, 186.395, 32.9417, 90.0601, 187.402, 71.4927, 128.117, 181.957, 179.608, 109.94, 48.2925, 2.82601, 167.382, 120.945, 133.543, 117.826, 179.192, 40.3821, 123.978, 42.7503, 149.431, 107.541, 40.2661, 168.285, 36.3964, 126.798, 139.091 };
						int n[32] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[32];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 2) {
						float s_mean[64] = { -973.144, 408.429, 975.341, 876.827, -404.279, 246.925, -726.554, 857.295, 877.377, 926.756, -19.8065, -957.091, -244.179, 732.963, 614.002, -321.635, -363.933, -862.606, -125.706, 136.448, 576.525, -6.07318, 399.274, -261.635, 671.926, -16.2053, 773.553, -175.695, 686.209, 311.075, 786.126, -926.572, -64.5466, -352.336, -61.9221, 74.4346, -579.577, 329.02, -799.31, -854.366, 209.571, -200.964, 22.9194, -594.592, -928.343, -694.388, 589.16, -907.407, -641.041, -153.905, -672.72, -863.399, -860.408, -637.928, -333.415, -22.0649, 580.615, -814.631, 45.1979, -361.736, 580.798, -267.19, -831.904, -959.044 };
						float s_var[64] = { 83.7062, 120.017, 180.071, 0.543229, 28.7057, 195.422, 176.421, 86.5566, 7.15354, 49.6658, 183.312, 175.793, 89.7366, 17.4871, 99.2584, 128.361, 178.387, 64.6565, 52.0585, 17.8167, 46.0952, 146.55, 26.1177, 177.282, 151.769, 141.765, 64.8091, 20.484, 30.0729, 160.723, 47.7981, 37.9223, 56.27, 29.2428, 43.4034, 167.901, 186.077, 147.154, 35.9386, 142.644, 70.4794, 121.848, 28.0465, 9.38749, 145.616, 176.94, 118.052, 26.4962, 97.586, 135.801, 152.3, 134.526, 102.316, 180.187, 152.507, 2.3133, 40.3943, 183.819, 118.088, 48.1155, 86.9106, 20.9723, 3.52184, 35.6395 };
						int n[64] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[64];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 3) {
						float s_mean[128] = { -751.701, -221.107, 407.27, -67.7206, -752.434, 217.383, 511.948, -290.872, -593.005, -932.127, 66.2557, -800.775, -15.656, -333.171, 237.403, -827.387, 300.272, -584.887, -725.272, 639.76, 339.64, 192.541, 236.549, 390.606, 603.198, -978.454, 966.613, 783.929, 209.204, 981.75, 320.353, -214.209, -755.242, 373.394, 642.079, -740.654, -632.252, 131.932, 490.951, -496.567, 658.01, -508.225, -382.122, 41.7188, 719.047, -99.2767, 191.992, 661.672, 29.6335, -816.645, 277.2, 703.848, -454.573, -44.0382, -441.389, 600.879, 550.401, 289.712, -680.532, -333.476, 758.477, 625.538, 667.348, 134.556, 665.029, 937.559, -989.257, -551.866, 148.473, 147.557, 785.943, 798.639, -678.091, -211.219, 433.271, -85.2992, -365.337, -79.7449, -376.202, 101.596, -776.36, -709.464, 735.832, -470.138, 515.305, -247.902, 581.53, 774.834, 76.632, 925.108, 383.526, 605.09, 717.338, -888.424, 1.3123, 22.1259, -159.032, 459.761, 407.941, 549.242, 46.7238, 377.544, 416.608, 770.257, -19.0741, -802.911, -342.998, -881.039, -561.571, 88.7173, -197.729, 918.271, 714.53, -775.14, 45.1979, -331.278, 206.214, 194.25, 824.824, 588.122, 263.833, 665.944, -196.875, 400.311, -707.266, -696.768, 91.7081, -625.66 };
						float s_var[128] = { 198.7, 146.544, 152.898, 50.4105, 118.796, 96.7376, 175.689, 116.861, 65.6026, 128.739, 6.97653, 9.76592, 100.082, 95.7793, 68.0074, 199.976, 176.995, 45.4909, 79.7021, 5.46281, 25.4097, 144.151, 180.639, 53.0839, 104.154, 75.8568, 119.999, 15.2715, 32.2764, 116.788, 125.565, 21.7536, 94.5402, 84.4142, 46.6872, 146.281, 21.8451, 161.693, 172.234, 168.328, 46.7727, 196.313, 123.551, 9.83917, 33.961, 74.8985, 22.4738, 46.9375, 85.6594, 172.234, 134.263, 96.6338, 22.1259, 75.4906, 185.577, 78.5363, 137.162, 183.422, 157.439, 151.616, 28.5226, 180.602, 153.221, 125.864, 103.427, 186.645, 141.6, 149.059, 159.008, 180.413, 174.529, 68.4469, 37.7697, 99.2401, 80.2026, 67.5069, 158.098, 13.1474, 31.0312, 177.63, 119.681, 174.078, 186.621, 64.8274, 184.002, 140.086, 11.6947, 91.5189, 29.8044, 174.584, 69.1427, 53.1693, 183.581, 120.542, 87.8994, 190.063, 124.986, 149.718, 105.991, 73.983, 67.3727, 186.883, 88.4426, 169.97, 13.2084, 9.00296, 20.2948, 73.0064, 84.5973, 19.5196, 74.5506, 19.7638, 128.672, 97.5066, 55.1286, 75.2953, 198.138, 121.921, 114.866, 194.769, 106.699, 170.824, 148.466, 160.179, 59.5416, 195.593, 60.4633, 192.083 };
						int n[128] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[128];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 4) {
						float s_mean[256] = { -537.095, 574.45, -644.276, 677.114, 774.468, -86.9472, 247.78, -418.073, 340.739, 591.54, -376.324, -321.39, -839.106, 306.253, -918.943, 298.135, -207.678, -725.333, -595.691, -47.7004, -79.928, 367.779, -240.333, -908.2, -821.894, 492.782, 140.965, 113.804, -781.06, -773.919, -935.301, 619.861, -965.697, 696.402, 515.427, -76.4489, -474.654, 716.788, 140.904, 808.588, -677.114, -831.721, 697.684, 536.302, -142.064, -367.656, 746.086, -896.237, -318.339, 273.476, 595.935, 751.457, 36.0424, -27.6193, -498.642, 720.939, -325.968, -291.421, 106.723, -737.053, 929.93, -806.635, 141.27, 895.016, -960.631, 752.617, -635.121, 909.238, 710.44, 459.212, -236.61, -869.747, -57.1001, 516.343, 993.652, 923.215, -900.632, 890.316, -921.201, -549.12, -774.651, 192.846, 354.228, -817.621, -380.657, -185.583, 661.855, 650.38, 653.432, -693.045, -203.65, 458.907, -156.468, -983.642, -800.043, 541.673, -830.439, -34.2723, 908.078, 669.24, -353.984, 987.915, -255.898, 207.373, -857.967, -891.781, 765.496, -875.546, -548.204, -822.321, -41.6578, 249.672, 614.856, 755.486, 183.142, 780.328, 486.435, 241.676, 370.037, -959.899, -442.305, 260.537, -36.1034, 486.862, -973.754, 292.703, 619.922, 970.031, 116.245, 558.519, -851.253, -761.223, 648.671, -849.422, -288.003, 699.942, 861.812, 416.608, -732.841, 425.398, 481.552, 592.334, 263.405, -589.831, -958.495, 684.011, 586.657, 80.2942, -546.373, 601.794, 754.814, -949.522, 828.303, 541.917, 363.201, -868.099, 544.603, -380.047, -220.069, 590.197, -429.243, -246.498, -908.994, -346.965, -965.392, 794.977, 295.877, 268.105, 43.5499, 914.182, 752.556, -889.096, 754.997, 231.666, -126.988, -157.384, 696.768, -298.868, -931.455, 617.603, 337.443, -757.744, 945.067, 841.609, -93.4782, -876.278, -251.808, -820.734, -535.264, 595.935, -941.16, 812.433, 224.647, -226.539, 738.639, -52.8886, -879.635, 768.609, -875.301, -427.9, 628.712, 73.5191, 475.57, 466.414, -161.473, 193.518, -333.964, -542.894, 772.088, 529.527, 132.908, 927.305, -45.7472, -489.059, -930.296, -426.496, -766.167, -21.2104, -127.293, 414.167, -146.641, -181.921, -164.281, 46.7849, -230.811, -159.093, -904.477, -843.867, 222.388, 919.187, 827.448, 272.073, -441.389, 367.412, 690.176, 776.91, 298.685, -424.482, -478.561, -836.116, -541.307, 891.781, -716.605, 544.542, 120.457, -244.606, 257.729, -593.554, 251.747, 79.6228, -509.262, -378.399, 776.666, 884.274, 489.364, -296.548 };
						float s_var[256] = { 83.0287, 64.9556, 57.1429, 166.961, 57.149, 109.665, 67.3544, 114.084, 137.645, 175.896, 149.187, 106.711, 136.003, 178.1, 31.1167, 187.109, 6.08539, 61.9587, 72.6585, 26.8136, 115.226, 44.2091, 22.9499, 153.996, 90.2554, 27.5094, 154.344, 155.766, 38.6792, 190.289, 171.001, 55.5437, 144.737, 151.537, 92.4955, 65.4012, 40.4736, 110.733, 112.4, 113.401, 151.878, 162.615, 31.8613, 145.933, 19.9408, 187.06, 68.2455, 5.06607, 196.765, 113.926, 38.6731, 80.9107, 116.648, 162.889, 118.65, 138.926, 142.198, 58.2781, 175.646, 122.275, 185.4, 18.9459, 77.6879, 129.191, 103.726, 123.087, 176.971, 165.294, 71.2302, 48.085, 168.426, 70.9494, 124.638, 185.376, 123.331, 23.6091, 84.4203, 130.253, 97.9095, 15.1555, 50.9293, 146.757, 116.898, 104.239, 75.4662, 7.49535, 132.176, 43.611, 6.36616, 8.48415, 156.322, 82.522, 110.117, 159.001, 171.239, 143.986, 67.4276, 146.409, 11.7313, 177.184, 144.346, 76.0277, 24.1035, 84.7926, 138.334, 185.98, 99.7589, 25.3059, 99.3622, 136.985, 83.7672, 90.4202, 26.0201, 77.2607, 158.501, 157.335, 136.607, 82.345, 2.18513, 116.111, 115.195, 169.939, 122.544, 72.0481, 132.762, 89.7366, 165.325, 122.971, 43.263, 88.29, 197.967, 111.618, 154.741, 48.8601, 168.12, 152.965, 175.396, 6.21967, 137.315, 136.558, 134.69, 36.5429, 89.4986, 3.89416, 79.458, 122.227, 114.646, 0.738548, 144.987, 191.729, 167.37, 104.465, 1.09256, 134.428, 0.476089, 26.4046, 73.2505, 190.454, 73.6167, 189.209, 138.658, 115.543, 36.8236, 79.1467, 97.3357, 120.866, 189.135, 173.516, 118.235, 187.902, 112.18, 42.909, 160.32, 158.238, 78.7744, 28.6447, 68.6605, 198.456, 36.5123, 95.0224, 45.5275, 39.9365, 166.381, 56.7217, 96.4873, 193.75, 16.303, 181.146, 51.5091, 140.446, 9.79034, 165.728, 161.022, 94.9431, 129.496, 90.3104, 67.0492, 146.355, 175.823, 144.487, 149.29, 172.82, 0.512711, 129.917, 46.1806, 131.364, 115.262, 137.706, 151.048, 122.739, 75.277, 0.708029, 83.1507, 31.2754, 37.904, 120.933, 133.805, 139.702, 96.1943, 71.8039, 87.4538, 119.016, 187.518, 175.585, 17.7862, 193.31, 177.148, 55.6597, 58.8092, 79.6106, 116.849, 163.939, 150.969, 102.762, 78.4936, 83.1202, 162.194, 85.2016, 162.639, 4.12, 184.045, 11.8778, 82.107, 178.362, 2.77718, 70.0339, 120.609, 145.134, 145.457, 64.0828, 66.3167, 133.934, 101.01, 5.45671, 120.463, 41.8226 };
						int n[256] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[256];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 5) {
						float s_mean[512] = { -391.034, -380.474, -529.649, 315.226, -949.461, 861.202, 957.274, -335.063, -685.598, -61.0065, -105.258, -610.889, -182.043, 0.885037, 128.208, -263.833, -437.117, 952.574, 825.434, -642.811, -325.846, 309.427, 140.782, 282.388, 189.184, 298.135, -870.724, -852.474, -903.439, -792.047, -89.2666, 671.01, -374.31, 992.737, 83.6512, -620.411, -214.759, -878.353, -610.828, -414.167, 152.623, 681.997, 625.721, -385.846, -87.1914, 729.545, 812.25, 277.383, -852.412, 890.866, 603.015, -396.466, -625.477, 214.454, -48.8601, -187.78, 336.1, 272.744, 23.1025, 541.368, -961.547, 513.291, -637.623, 323.588, -958.922, -139.683, 303.873, 312.784, -4.73037, 566.698, -362.407, 260.842, -566.576, 315.043, -476.852, 744.133, 808.222, 569.445, 653.554, -360.027, -218.604, -822.932, -330.851, 853.45, 763.176, -780.084, 822.077, -706.595, -298.99, 115.94, -109.958, 99.8871, -881.222, 970.824, 340.373, 119.846, -594.043, 179.54, -255.715, -365.764, 356.426, -721.793, 440.657, 334.391, 957.701, -954.955, 450.423, -451.46, -570.116, -147.435, 640.309, 718.619, -484.97, 268.532, 137.181, 126.377, -984.191, -92.0133, -8.14844, -129.551, 584.765, 534.654, -906.613, -907.407, 763.421, -587.512, 333.048, -545.701, -728.874, 907.407, -981.811, 863.582, 544.542, 338.237, -463.301, -548.997, -473.861, 79.2566, 270.73, -203.04, -296.243, -89.0225, 760.918, -592.395, -600.635, -378.033, -879.696, -558.092, 308.267, -139.5, -301.248, -312.174, 288.125, -764.031, 996.582, -210.913, 397.992, -329.386, -171.361, 721.61, 187.353, 0.762963, -261.025, 520.249, -764.702, -171.178, -335.978, 255.287, 939.573, -375.469, 847.468, 258.889, 555.528, -923.338, 3.57067, 601.733, -514.878, -305.399, 731.986, -180.334, -183.691, -340.373, -818.964, -109.836, -888.18, 927.061, -37.3852, -541.185, -551.683, 577.013, 315.531, -683.889, 705.374, 416.852, 519.761, 123.875, 104.587, 832.026, 560.961, 391.766, -1.25126, 887.997, -956.236, 703.848, -89.0835, 681.814, 356.67, 195.349, 823.115, -56.6118, 85.2992, -720.939, -284.28, 887.387, 677.114, 621.876, 356.731, -685.354, -644.948, 371.38, 340.251, -36.2255, 281.838, -367.534, 375.958, 112.583, 973.998, 193.945, -171.911, -570.238, 965.636, 504.868, 504.563, 76.632, 723.075, -98.6053, 695.791, -941.771, 808.405, -916.929, -535.997, 825.373, -706.412, 839.717, -817.682, 125.767, -574.938, -972.472, 323.527, -893.979, -462.996, -203.711, 499.496, 662.954, -784.051, -934.812, -250.099, 45.0148, -285.257, 249.977, 81.637, 704.276, 616.443, 239.296, -742.424, 397.504, 748.589, -378.948, 835.871, -461.898, -653.005, -840.632, 458.785, -202.49, -529.771, 521.043, 391.827, 956.786, -336.711, -748.283, 313.089, 8.14844, -128.208, 3.08237, 351.909, 506.516, -393.841, 8.81985, 378.094, 171.239, 355.51, 811.579, -458.968, 737.419, 251.625, 239.418, 58.8092, 57.5884, 983.52, 632.374, -743.034, 453.413, 960.875, -681.143, -142.003, -683.035, 728.385, -657.338, 520.066, -37.5072, 630.055, 439.009, 626.148, 728.019, 722.77, 261.269, -676.809, 326.579, 940.733, -142.186, 811.213, -551.866, -490.646, -601.856, -117.954, 611.255, -390.484, 149.815, -291.116, 263.955, 291.238, 704.825, -115.085, -863.826, -361.858, -543.992, 712.699, 840.754, 181.372, -675.893, -257.241, 725.028, 563.646, 733.818, 563.463, 972.655, -546.922, 840.449, -160.802, -44.9538, -292.093, -993.286, 600.024, -167.089, 789.85, 529.344, -219.581, 407.819, -571.703, 615.101, 8.63674, -776.36, 535.02, 608.02, -354.534, -81.5149, 367.29, 465.438, -926.634, 825.8, -57.4664, -515.122, -764.214, -173.803, 702.261, -719.413, 425.886, -550.89, 426.191, -990.051, -674.368, 224.097, -603.992, 850.154, 295.389, 282.571, 809.992, -275.43, 912.046, -790.826, -553.636, -720.328, -241.127, 143.712, 578.173, 111.423, 464.339, 132.298, -863.582, 241.493, -9.00296, 729.179, -874.996, -155.431, -825.19, 848.262, -715.751, 224.158, -485.946, -135.533, -994.873, -960.448, 208.411, 965.758, 493.332, -217.444, -552.049, 754.021, -816.828, -496.506, -47.4563, -902.707, 401.959, 788.263, 760.369, -768.914, 283.853, -15.4118, -732.658, 109.653, 933.958, -321.146, -483.688, -992.065, -157.75, -577.441, 735.771, 373.089, -452.01, 708.792, 128.941, 925.718, -446.577, 198.706, -86.8252, -988.159, 488.083, -801.508, 608.692, 204.382, 522.691, -614.49, 577.807, -845.698, 33.3567, 785.15, -186.193, -307.108, 515.488, -2.9603, -19.6844, -817.377, -251.93, 672.048, 700.125, -880.856, -792.596, 238.563, 94.2717, -17.243, -268.41, 879.147, -428.816, 339.885, -565.783, 756.951, 865.78, 194.678, -131.382, -507.492, -671.743, 590.686, 441.877, -109.714, -536.485, -537.767, -921.201, -84.994, 720.084, -263.833, -863.704, 918.394, -471.053, -316.813, 567.064, 115.574, -406.842, -480.209, -666.189, -688.04, 426.252, 259.316, -926.572, -120.334, 141.697, 813.959, -989.196, -16.2664, 491.989, -653.92, 123.203, 59.1754 };
						float s_var[512] = { 60.7257, 102.237, 75.6798, 150.31, 78.2556, 38.6486, 83.0531, 180.474, 145.561, 103.842, 76.7663, 33.2957, 27.7169, 100.742, 137.748, 129.417, 140.086, 34.6629, 118.699, 185.235, 109.995, 168.847, 143.272, 105.893, 13.4831, 154.851, 180.322, 153.27, 87.0327, 38.0749, 93.112, 188.91, 114.243, 138.218, 163.536, 192.004, 87.5881, 4.71816, 91.3236, 52.3698, 145.134, 47.2671, 68.2821, 38.4228, 182.22, 41.2305, 127.5, 140.135, 138.224, 108.158, 167.565, 86.9778, 158.263, 171.233, 51.7777, 40.9436, 29.8898, 85.7326, 182.409, 164.501, 94.8637, 44.5204, 149.858, 63.509, 35.8043, 58.4979, 56.9048, 105.264, 137.968, 112.07, 117.954, 11.42, 25.9285, 193.64, 133.195, 125.968, 75.3136, 1.01932, 37.8552, 158.214, 99.5209, 122.001, 126.444, 0.720237, 111.435, 89.6939, 151.903, 22.3334, 160.149, 113.114, 99.8871, 36.0668, 42.6649, 82.5709, 37.9467, 142.161, 90.6522, 50.1785, 39.79, 45.9609, 32.1238, 185.308, 184.436, 50.9781, 128.196, 5.05387, 30.4758, 126.786, 184.753, 79.7327, 165.319, 52.0157, 39.2163, 13.2939, 29.3222, 32.3374, 122.257, 98.8556, 15.1067, 193.408, 94.4792, 49.1043, 41.7249, 148.845, 158.171, 1.83721, 3.88195, 43.2081, 44.0077, 0.366222, 96.1638, 69.9545, 72.7989, 149.04, 12.244, 64.4063, 92.1537, 43.6476, 19.129, 171.74, 88.2595, 76.9616, 106.345, 189.911, 72.5486, 19.1595, 10.181, 187.677, 43.4584, 73.3116, 112.894, 197.534, 23.1269, 5.81683, 16.9256, 183.685, 169.115, 151.939, 194.14, 85.0246, 161.119, 96.1882, 110.013, 133.103, 62.7155, 27.8695, 22.8706, 45.0636, 58.0523, 171.514, 58.1256, 91.6776, 63.5701, 92.3246, 157.445, 40.2356, 14.7465, 76.7052, 103.012, 172.594, 27.7779, 187.677, 16.7364, 178.063, 36.3292, 195.367, 134.465, 121.445, 103.232, 182.47, 55.1958, 85.9645, 7.20847, 24.7322, 65.3157, 91.2564, 104.617, 146.422, 137.803, 86.3124, 66.1153, 150.761, 6.44551, 17.4688, 88.4243, 125.431, 115.085, 171.38, 181.048, 23.0354, 41.792, 192.956, 127.946, 116.562, 198.132, 178.362, 124.314, 129.844, 3.32652, 42.9823, 88.2534, 43.0433, 133.549, 164.391, 47.5234, 173.125, 46.0707, 100.284, 1.23295, 129.295, 178.521, 181.506, 56.5813, 43.5011, 23.0903, 179.98, 40.553, 75.8873, 75.7653, 51.6739, 186.682, 195.264, 60.5121, 154.613, 96.6948, 34.5714, 28.5409, 141.789, 177.319, 21.3691, 132.499, 129.173, 151.927, 183.282, 75.6676, 138.658, 47.2854, 28.4616, 36.2499, 16.0405, 55.2995, 101.102, 66.5059, 23.9998, 69.6799, 11.8412, 167.034, 110.05, 97.6959, 143.303, 35.9508, 49.4949, 146.886, 30.4941, 93.1486, 63.4297, 186.767, 16.3213, 179.473, 111.704, 185.577, 182.061, 130.955, 60.8478, 21.9672, 152.782, 28.0526, 127.122, 71.7429, 177.868, 119.84, 194.391, 69.5578, 86.4467, 23.2429, 7.16575, 45.6557, 117.899, 197.259, 38.551, 137.584, 27.2835, 92.1476, 33.1919, 76.8883, 65.8345, 173.699, 25.2693, 150.89, 36.4208, 22.3945, 31.4585, 39.32, 24.012, 107.694, 18.891, 126.127, 1.1475, 37.1776, 148.149, 86.111, 108.432, 80.8313, 91.1893, 146.733, 9.94293, 170.232, 24.3599, 24.7322, 123.777, 172.753, 96.3469, 137.944, 127.451, 40.8216, 171.142, 91.9401, 138.408, 36.1278, 45.9914, 41.792, 169.475, 28.7973, 144.426, 128.794, 59.3402, 71.688, 84.1517, 192.126, 78.0053, 49.5254, 120.096, 155.644, 7.27561, 3.19834, 62.2944, 89.5535, 96.2065, 128.935, 68.9413, 147.154, 91.586, 52.4979, 114.292, 176.519, 143.406, 178.478, 131.681, 7.2573, 185.314, 114.432, 31.4463, 12.2196, 190.1, 170.177, 193.957, 144.218, 100.156, 131.932, 37.495, 47.7004, 32.9539, 111.594, 61.3178, 45.5641, 161.516, 70.9372, 163.573, 0.769066, 69.8386, 20.838, 34.2601, 53.1388, 183.441, 95.4802, 23.2429, 31.0312, 74.752, 101.572, 83.1812, 138.09, 43.6537, 142.601, 187.512, 114.908, 172.893, 117.997, 23.4016, 75.8324, 58.565, 97.3968, 159.777, 20.6, 15.6072, 169.854, 90.1822, 21.1066, 158.245, 168.029, 150.871, 83.108, 155.449, 67.4459, 113.688, 82.9371, 61.4338, 60.683, 54.0605, 179.241, 21.1066, 106.613, 93.997, 99.6551, 50.5509, 154.607, 20.0201, 102.585, 92.465, 43.7513, 176.147, 13.8676, 146.678, 77.0837, 31.843, 138.542, 75.576, 70.7907, 25.0435, 47.5112, 84.5851, 78.4509, 80.0623, 39.3017, 77.6452, 190.228, 0.567644, 76.7846, 87.6919, 7.0925, 137.608, 198.859, 182.086, 190.075, 132.499, 7.19626, 43.1043, 84.4386, 134.849, 27.9794, 23.4382, 18.9215, 95.5229, 148.576, 76.6076, 79.9646, 134.581, 129.27, 162.462, 160.9, 184.271, 190.008, 127.641, 86.1904, 106.998, 72.0908, 61.0431, 10.6632, 88.2839, 174.902, 124.32, 93.6918, 104.74, 167.626, 57.2771, 48.4451, 67.0309, 75.9545, 158.44, 166.088, 37.8552, 43.8429, 140.416, 14.9968, 124.9, 170.171, 60.4572, 15.0273, 48.8235, 20.5084, 125.877, 86.6482, 186.206 };
						int n[512] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[512];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 6) {
						float s_mean[1024] = { -929.807, -838.862, -753.838, 886.776, 819.819, 332.804, -596.545, -150.609, 688.467, -474.044, -697.928, -589.587, -720.634, 247.78, -791.009, 300.333, 118.931, 813.166, -838.191, -313.944, -835.688, -84.7499, -898.19, 366.924, -438.459, -554.369, 256.63, -780.511, -346.965, 564.867, -227.027, 893.124, 235.633, -977.783, 599.658, 754.997, -931.761, -273.171, 620.35, -914.182, 629.933, 567.309, -963.805, 525.01, -872.738, -517.624, -0.946074, 765.618, 752.8, 781.182, -506.333, 500.29, -807.917, 713.37, -528.855, 616.016, 382.183, 289.773, -768.731, 639.882, 519.028, -647.572, 55.7573, 434.004, -684.5, -104.953, -909.482, -858.882, 509.079, 633.168, 478.988, -620.228, -380.352, 824.58, -255.348, 689.688, 957.64, -660.39, -575.976, 226.905, -118.748, 981.689, -50.7523, 709.342, 304.788, 56.9781, -218.909, -387.982, -543.87, -804.498, 791.681, -387.677, -115.024, -579.089, 897.397, 285.318, 720.084, -528.916, -58.565, -950.926, 177.343, 368.145, 827.143, -639.943, -682.119, -760.918, -99.8871, 158.361, -944.517, 385.968, 581.652, 562.975, 414.411, -393.963, 251.198, -182.836, 37.5072, -237.526, 379.315, 483.871, 943.724, -395.795, -89.6939, 890.805, 536.058, -163.305, -104.587, 825.922, -140.599, 441.755, -624.928, 482.772, 108.31, -751.762, 512.314, 655.629, 384.747, -247.658, 975.158, 185.034, 706.046, 187.719, -430.464, 652.821, 262.49, -154.21, -877.804, 327.677, 925.474, 646.29, 613.453, -639.637, 4.91348, -42.6344, 559.923, 229.102, 257.363, 666.066, -352.458, -565.355, -856.319, 498.398, 614.124, 446.028, -662.954, 589.892, -617.725, -104.648, 480.392, -694.266, -557.543, -976.562, 194.433, -688.711, -514.695, 375.286, 335.612, -107.761, 284.89, -255.287, 503.769, 524.827, 831.233, -629.505, 799.921, -303.629, -286.843, 381.207, -165.929, 975.524, -248.573, 490.585, -16.8157, 332.499, -118.87, 942.869, -101.23, 709.403, 284.768, 708.182, 264.87, -159.337, 774.773, -72.2983, 805.841, -676.87, 426.74, 609.302, 543.992, 936.827, 461.959, 572.253, -692.373, 214.454, 380.169, 643.178, 488.998, -737.48, -657.338, -898.984, -948.607, 831.416, 804.804, -627.552, 787.957, -364.666, 398.907, 977.111, 828.608, 923.215, -932.798, -277.078, -601.55, 298.502, -555.345, -848.384, 825.373, 789.727, -508.591, -766.289, -202.368, -829.157, -624.012, 314.127, 872.25, -468.429, -391.888, 307.291, -767.876, 97.3846, 421.125, -857.54, 694.51, -699.82, 405.194, 459.273, 968.139, -901.547, 389.996, 552.66, 161.718, -452.254, 577.502, 568.59, 541.307, 620.655, 924.802, -50.9354, -461.837, -722.709, 776.971, -872.433, 6.86666, -214.087, -250.221, -525.681, -267.373, -707.389, -137.425, 491.745, 189.673, 642.262, 921.14, 259.438, 548.448, 448.225, -849.727, -870.174, 933.958, 413.251, -922.971, 673.513, 933.287, -100.558, 663.137, -125.95, -646.535, -251.503, -312.174, 574.023, -770.501, -647.633, -618.946, 169.713, 81.7591, 973.327, -694.205, -443.281, -575.915, -686.026, -508.774, 325.419, -478.378, 338.542, -184.668, -794.244, -601.611, -658.071, 637.806, -236.793, 95.4314, 271.096, 682.791, -656.606, -821.65, 294.656, 171.789, -731.803, 495.285, -376.446, -793.573, 280.313, -815.546, 581.408, -598.621, -761.04, -67.5375, -75.5943, 766.9, -934.751, 481.735, 412.152, -158.177, -62.7155, 388.226, -372.356, -923.521, -264.504, -970.397, 47.7615, -534.654, -261.879, 862.911, 363.384, 429.792, -433.576, -67.7816, 49.7757, 951.17, -620.045, -373.272, -812.86, -46.6628, -678.152, -311.808, -469.405, -64.4246, 214.331, 469.222, 830.683, -144.444, -496.261, 440.657, 403.424, -738.09, 456.16, 949.583, -48.9212, -322.794, 602.466, 227.149, -367.962, -769.402, -566.393, 646.474, -618.519, 540.819, -653.615, -770.013, -518.723, -151.097, -161.779, -591.968, 380.779, -598.987, 952.696, 25.8492, -233.436, -918.699, -25.2998, -397.931, 883.541, -705.435, -623.829, -163.976, 104.221, -900.754, 11.0172, 365.886, -249.855, -409.894, -439.497, 776.238, 161.29, 531.236, 228.614, -143.834, -983.154, 238.441, -312.723, 106.845, -94.4548, 712.699, -728.08, -500.656, 145.665, 22.309, -573.534, 482.65, -144.322, -63.7532, 398.968, 241.249, 274.27, 806.574, 186.438, 203.04, -668.996, -298.624, -269.204, 797.296, 466.292, -212.073, -636.402, -538.011, 701.834, 964.843, 501.877, -97.6897, -39.0942, -254.616, 592.944, 597.888, -361.064, 390.118, -690.054, 560.839, 423.383, -710.257, -2.65511, 343.669, -635.792, 598.01, 366.008, 296.854, 194.494, -885.983, 438.398, 76.8761, 900.204, 416.059, -255.715, -596.118, 540.88, 243.202, -234.291, 69.5517, -679.983, -870.907, -367.595, -805.048, -94.9431, 284.219, -932.859, -261.757, -2.10578, 728.935, 581.225, 252.663, 155.248, -323.893, 919.187, -105.93, 858.76, 164.22, -245.277, 679.128, 835.444, -60.0909, -363.994, 992.676, 628.101, -409.162, -819.514, 345.927, -584.643, -71.3218, -479.354, -615.894, -917.661, 621.754, -0.885037, -855.708, -459.334, 77.6696, -885.922, -436.811, -701.163, 576.647, 714.591, -765.069, -717.338, 653.92, -554.308, 419.111, -94.4548, 732.597, -555.895, -701.895, -611.438, 6.62252, 283.181, -514.512, -668.874, 573.534, 168.31, 783.563, -468.856, 817.072, 382.366, -809.198, 55.269, 40.315, 980.895, 160.497, 559.191, 348.369, -262.551, -892.209, 858.211, -218.97, 339.03, -832.148, 747.185, 695.914, -168.981, 203.772, -928.953, -599.231, -687.002, -935.972, 45.2589, -963.073, -116.245, -909.36, 825.8, -288.43, -163.427, 876.888, 329.63, -119.175, -895.322, 274.819, -694.998, 982.543, 956.664, -346.294, -348.003, -681.082, 941.282, 407.27, 992.309, -810.236, -916.684, 711.661, 490.036, -725.333, -671.072, 533.372, -78.28, 266.457, 240.333, -112.949, -3.02133, -246.498, 941.71, 343.242, -71.5659, 352.824, 314.249, -213.965, 252.174, -907.224, -558.336, 421.308, 898.251, 46.0524, -694.815, 34.0892, 510.544, 212.134, -270.669, 496.689, -982.482, 650.441, -423.383, -473.678, -547.349, 863.033, 193.396, -306.986, -976.073, 427.229, 104.953, 677.358, -564.867, -121.067, 960.936, 778.375, 455.489, 844.844, 730.277, -328.776, 740.287, -574.45, 147.13, 415.387, -98.9105, 828.547, 405.683, 711.783, -173.925, -749.504, 105.625, -588.733, -749.565, -989.319, -552.416, 272.134, -423.505, 933.409, -321.696, 967.833, -55.8794, -19.1961, -214.148, -655.019, -448.958, 135.899, 445.235, 931.211, -776.727, 535.264, -786.554, -307.352, 235.267, 851.802, 974.975, 42.2071, 380.108, -72.4204, -363.262, 96.9573, -709.769, -343.242, -926.023, 186.193, -992.676, 118.259, -674.795, 609.18, -197.058, -41.4136, 37.5072, 642.994, -751.396, 24.8115, -838.13, 800.226, 536.241, 247.108, 898.74, 336.894, 48.3718, -228.187, 892.819, 10.4068, 178.564, 344.157, 679.067, 86.9472, -598.315, 940.916, -622.608, -400.25, -405.744, -346.477, 638.478, 856.563, 604.663, 820.734, 838.496, -802.362, 73.4581, -588.794, 525.01, 3.6317, 503.952, 656.911, -888.058, 953.124, -418.012, 935.179, 825.129, -469.344, -252.968, -97.8729, -102.817, -215.369, -988.403, -124.79, -986.633, 334.819, -967.772, 466.17, -993.408, -369.671, 429.67, 463.546, -197.18, 457.503, -641.224, 965.758, 616.199, -302.347, -670.644, -236.061, -605.396, -665.09, 276.345, 974.731, -485.031, 713.919, 879.879, -77.3034, -271.645, 47.6394, 135.716, -726.188, -644.581, -824.458, 175.756, 417.768, 731.864, -429.121, 338.237, 100.986, 903.256, -401.166, 487.777, 309.305, 571.032, -975.768, -413.923, -598.987, 287.332, 769.463, -487.838, 489.486, -99.6429, -77.9748, 132.359, -924.131, -750.114, -7.29392, -378.948, -67.5985, 112.461, 422.956, 30.7932, -765.435, -603.442, -656.789, 112.461, -55.5742, -411.725, -419.66, -614.124, 443.037, 446.15, -84.5668, -540.025, 413.617, -53.0107, -408.063, 603.076, -25.7881, 480.697, 73.1529, -436.872, 60.3961, 296.06, 50.1419, 638.356, 58.1988, 195.593, -300.333, -2.83822, 687.002, -518.784, 191.809, -751.335, 132.115, -850.52, 447.92, 263.1, -572.253, 336.65, -851.68, -758.232, -910.092, 14.3132, -9.67437, 37.8124, 999.145, -65.4622, 195.715, 726.066, -467.086, 825.312, 268.838, -576.22, -50.6302, -164.403, -995.666, -639.271, 19.1961, -769.097, -255.043, -800.531, 909.055, 338.664, -110.691, 605.884, -686.026, 743.034, -350.322, -930.906, 840.571, 730.888, 60.0909, -27.7413, 639.454, 986.633, 233.619, -87.4966, -711.661, -462.325, -129.307, -728.507, -75.1061, -50.0198, -983.276, 35.493, 629.078, 772.942, -930.174, 865.963, 56.6118, 234.901, -961.241, -580.493, 321.207, 188.513, -949.4, -81.3318, -872.127, -693.899, -891.232, -28.1686, -844.661, -475.204, -899.411, 643.727, 689.138, -258.095, 55.4521, -224.403, -495.895, 760.308, 652.15, 555.773, -2.34993, -516.282, 977.05, -660.39, -427.839, 361.919, -468.184, 720.328, 267.983, 86.2758, -508.835, -151.463, -353.801, -465.194, 210.913, 3.50963, 921.506, 967.711, -14.7404, -587.268, 344.89, 472.396, 55.208, -677.419, -163.366, -340.861, -133.946, -681.509, 869.015, 606.311, -424.909, -104.709, -401.044, 55.3301, -190.222, 277.688, -748.711, 856.563, 675.344, -509.751, -248.939, 664.113, 12.4821, 678.091, 606.616, -118.564, 905.393, 676.87, -957.579, 856.624, 439.375, 833.308, -618.213, 327.494, -290.994, -924.741, -320.353, -882.748, -924.558, -767.51, -394.391, -920.53, 674.795, -393.536, -423.505, -706.229, 175.268, 791.009, -860.897, -489.364, -214.393, 742.241, 603.504, 847.224, 512.68, -900.571, -74.8619, -267.739, -330.973, 755.058, 838.374, -898.251, -368.816, 674.734, 238.685, 844.111, -152.928, -31.8308, 815.912, -504.929, 93.9665, -790.887, 936.766, -816.095, 986.328, -994.263, -222.205, -172.033, -266.457, -179.052, -291.726, 578.173, -852.229, -957.396, -77.1203, 514.573, -98.056, -695.853, -675.588, 591.296, 37.2021, 54.4755, 656.972, 832.209, -279.031, 378.582, -111.728, 776.055, -745.354, -932.371, 842.952, -828.425 };
						float s_var[1024] = { 172.1, 128.135, 37.0739, 1.66631, 126.841, 128.245, 139.085, 138.542, 112.308, 39.7717, 58.1011, 56.3738, 77.4316, 26.5633, 79.6716, 23.5054, 123.081, 169.836, 95.5596, 114.292, 154.381, 139.232, 43.611, 167.705, 108.09, 38.9477, 122.575, 120.945, 154.918, 28.84, 181.951, 118.015, 76.8761, 120.475, 86.8557, 193.347, 19.6295, 148.253, 57.8448, 130.326, 19.8431, 115.5, 185.754, 94.9797, 107.492, 115.555, 121.555, 44.496, 183.251, 27.8573, 141.197, 110.843, 70.7968, 189.062, 185.449, 159.374, 144.206, 39.1003, 74.7398, 122.391, 130.235, 142.326, 99.0143, 107.779, 85.696, 183.538, 182.55, 82.5709, 149.821, 139.14, 23.8289, 131.541, 71.5537, 23.188, 10.3885, 90.8109, 111.954, 8.51466, 108.615, 156.45, 121.482, 122.819, 128.642, 68.038, 192.389, 119.724, 164.165, 67.2933, 86.0256, 124.931, 7.28782, 31.6477, 35.2184, 57.2466, 138.401, 182.842, 121.604, 24.7505, 90.9452, 170.525, 27.9366, 95.8159, 191.351, 130.845, 165.496, 45.2528, 73.806, 120.042, 176.36, 75.3807, 170.153, 123.038, 10.8219, 112.137, 174.688, 37.0495, 179.681, 76.1437, 84.048, 30.1462, 181.591, 128.843, 12.0182, 74.8863, 105.435, 93.5331, 179.406, 56.8743, 7.64183, 51.5336, 78.8415, 58.3819, 64.5405, 113.211, 14.6794, 16.9134, 99.6979, 164.122, 17.7923, 158.843, 86.2636, 186.071, 135.453, 7.93481, 24.3171, 153.24, 96.1394, 169.048, 193.225, 103.244, 149.406, 86.6054, 178.948, 183.264, 80.7276, 178.289, 76.4122, 71.102, 1.33061, 143.559, 66.86, 171.306, 113.041, 47.3098, 170.171, 186.248, 48.1338, 178.43, 172.222, 62.2028, 184.167, 83.3766, 127.207, 110.501, 133.83, 174.822, 35.0169, 103.537, 9.78423, 115.201, 93.5759, 145.946, 64.5283, 175.341, 126.145, 120.621, 105.203, 168.01, 183.709, 109.873, 83.4742, 156.56, 122.306, 191.498, 60.3351, 138.487, 155.382, 9.83306, 146.001, 104.044, 181.103, 121.94, 14.6977, 69.8263, 84.7377, 118.595, 194.568, 87.3562, 15.9673, 120.469, 132.426, 26.6854, 41.2732, 49.1592, 33.7107, 135.014, 171.227, 73.3299, 77.2546, 170.232, 162.865, 29.7739, 5.12711, 109.543, 97.0244, 19.5135, 87.5271, 33.076, 130.711, 178.912, 62.5813, 182.452, 24.3965, 25.7332, 51.204, 194.36, 44.3983, 180.279, 46.9558, 109.214, 40.2173, 61.4399, 180.865, 154.68, 96.8719, 157.823, 161.266, 104.306, 162.243, 73.8182, 88.6685, 11.1148, 49.4217, 150.725, 45.5885, 116.19, 14.0629, 62.7949, 61.4032, 132.261, 106.711, 188.391, 52.1683, 102.304, 100.15, 156.023, 135.478, 33.1492, 191.748, 165.471, 120.316, 104.068, 80.813, 83.8832, 171.886, 50.3372, 20.9113, 116.849, 51.4664, 121.891, 47.2365, 54.5061, 131.242, 16.8523, 178.942, 92.6359, 184.167, 190.173, 196.472, 28.7484, 148.778, 137.199, 193.933, 33.1919, 69.0634, 52.2782, 106.4, 195.215, 9.30815, 62.3676, 12.3234, 44.0931, 143.925, 35.5846, 10.7913, 45.2101, 22.2907, 164.849, 73.9402, 71.157, 8.61843, 126.438, 74.2637, 47.8103, 96.7803, 13.599, 186.962, 175.451, 100.137, 45.1552, 67.2872, 124.815, 16.419, 49.0677, 2.90536, 8.24, 167.559, 121.061, 41.2061, 125.742, 114.499, 58.1866, 91.6715, 86.0683, 147.087, 167.455, 3.21055, 102.86, 148.888, 49.3728, 16.5105, 65.6819, 37.0312, 156.615, 191.369, 186.169, 177.074, 127.537, 9.02738, 19.068, 172.814, 94.8149, 149.571, 58.3941, 31.1533, 169.048, 44.1969, 108.463, 101.956, 97.3052, 2.60628, 30.018, 192.846, 96.7803, 115.348, 61.3178, 59.096, 63.3259, 131.12, 114.237, 68.4591, 155.095, 109.165, 45.3322, 40.9864, 151.097, 46.5835, 101.981, 63.918, 184.826, 80.6909, 143.431, 81.4844, 52.8214, 66.3594, 36.9213, 59.7613, 98.294, 128.349, 28.7729, 126.548, 69.6921, 188.665, 90.4752, 86.1415, 154.314, 68.8864, 39.7656, 109.256, 186.889, 167.553, 36.7626, 197.809, 33.1675, 112.442, 55.031, 24.7871, 148.558, 187.976, 127.677, 149.12, 111.649, 137.504, 168.45, 5.4445, 132.853, 140.703, 167.821, 69.0512, 137.999, 165.905, 164.104, 195.636, 26.8929, 97.3968, 189.526, 136.192, 106.967, 37.843, 191.333, 26.8502, 101.804, 185.571, 127.879, 53.7248, 134.788, 86.9045, 39.7107, 92.7335, 87.6675, 63.5701, 95.6877, 124.461, 134.172, 14.9968, 14.0385, 23.9998, 48.5672, 195.654, 58.4979, 125.803, 89.3155, 167.406, 9.35698, 163.506, 57.5823, 126.878, 109.598, 184.783, 130.906, 7.76391, 185.943, 41.1878, 71.7429, 158.208, 144.987, 53.0168, 11.1026, 193.091, 44.2824, 49.1104, 7.0925, 21.4362, 138.426, 181.75, 178.564, 48.7259, 100.137, 116.446, 13.654, 154.241, 16.3884, 196.832, 53.4196, 43.4034, 159.478, 184.472, 150.426, 175.14, 133.11, 173.717, 102.298, 64.1987, 193.512, 81.5638, 60.3839, 158.519, 145.518, 112.473, 113.297, 91.5433, 13.6723, 198.23, 45.5153, 109.61, 88.3145, 12.775, 71.0044, 74.2454, 146.66, 162.078, 141.777, 39.5276, 48.9151, 148.924, 31.4951, 4.4496, 109.372, 112.815, 194.098, 40.2295, 42.4818, 144.792, 197.333, 192.688, 83.9625, 177.27, 131.4, 159.545, 86.8923, 48.8907, 192.547, 142.821, 192.81, 144.377, 41.6028, 76.1376, 77.7795, 9.34477, 137.663, 27.0882, 173.528, 140.184, 46.6018, 174.566, 63.4968, 148.875, 124.705, 161.095, 53.9689, 102.841, 177.27, 20.2033, 120.139, 39.7961, 92.1598, 78.7744, 27.2164, 57.6067, 48.4573, 34.3333, 26.5328, 179.077, 100.009, 7.45872, 126.603, 191.858, 134.031, 169.073, 139.238, 63.2466, 4.71206, 142.814, 100.662, 130.07, 41.8043, 182.385, 67.5497, 63.4846, 134.22, 123.869, 88.1863, 8.83816, 188.61, 113.108, 148.509, 100.546, 162.969, 108.017, 93.2096, 151.872, 75.2281, 155.943, 121.86, 119.816, 186.871, 46.0097, 59.975, 144.963, 161.461, 136.448, 5.1149, 166.289, 198.321, 156.957, 147.984, 111.032, 141.069, 186.041, 168.511, 59.5477, 161.217, 3.32652, 6.94601, 185.125, 16.7913, 16.9744, 9.99786, 68.8192, 85.9218, 165.954, 51.8448, 189.355, 175.396, 197.412, 30.2927, 187.915, 104.288, 119.022, 137.858, 107.987, 62.569, 26.3253, 107.834, 10.2481, 144.664, 105.075, 35.4137, 162.371, 58.0462, 8.5696, 173.778, 3.86975, 6.86056, 10.0223, 105.179, 85.9706, 150.499, 129.868, 87.0449, 186.425, 37.1899, 38.1359, 173.058, 199.365, 12.6102, 131.785, 109.262, 0.152593, 3.44859, 187.042, 5.16373, 85.2321, 76.0338, 186.749, 179.809, 186.938, 169.909, 122.709, 182.879, 63.3686, 6.41499, 173.046, 190.796, 63.2405, 172.332, 54.5, 177.801, 65.688, 178.393, 149.828, 62.0563, 195.148, 92.9533, 10.1077, 26.7525, 111.185, 150.56, 32.1055, 16.1748, 73.3482, 135.52, 110.605, 6.81784, 180.999, 140.526, 65.7491, 53.4684, 188.153, 100.229, 45.4054, 110.3, 93.3439, 99.8627, 102.487, 26.9723, 104.459, 58.7909, 147.301, 119.724, 178.57, 189.148, 74.8985, 117.472, 136.796, 29.3771, 67.4215, 82.8761, 154.125, 52.913, 71.4316, 108.475, 24.2866, 51.6678, 50.2701, 187.75, 199.573, 94.705, 11.3956, 61.2323, 5.67644, 61.8, 8.71609, 37.5927, 31.5744, 152.043, 119.956, 189.74, 94.3144, 167.425, 198.975, 125.883, 178.918, 171.429, 3.1251, 135.698, 153.16, 122.642, 95.6145, 111.332, 57.4725, 83.3399, 122.806, 43.5621, 169.768, 3.07016, 66.5853, 92.8678, 45.2528, 77.2546, 71.7246, 35.5235, 131.327, 80.7947, 48.1826, 84.6889, 157.366, 76.632, 48.4878, 86.5078, 99.6185, 99.3866, 20.0629, 91.1954, 121.018, 13.9103, 132.566, 99.1607, 86.0378, 148.912, 80.2454, 182.043, 50.5325, 105.515, 159.27, 178.875, 126.591, 140.898, 120.096, 167.107, 122.349, 126.847, 194.787, 108.768, 156.89, 83.3766, 99.3561, 71.5049, 50.0137, 87.3074, 11.1454, 24.7017, 87.1548, 155.87, 121.128, 10.5838, 159.178, 105.277, 161.26, 90.2799, 77.224, 184.356, 106.851, 64.9373, 62.5935, 83.108, 53.8774, 171.819, 11.5421, 116.318, 71.3462, 155.382, 135.148, 182.153, 28.6203, 113.376, 174.157, 95.7244, 197.784, 117.96, 188.818, 97.7386, 30.9641, 11.7557, 123.728, 138.029, 112.699, 178.973, 147.685, 134.184, 87.2524, 91.4823, 168.523, 5.14542, 160.979, 29.9326, 122.959, 79.7876, 179.675, 179.827, 41.4258, 80.5078, 80.8802, 102.609, 87.9482, 81.8323, 121.281, 181.787, 174.932, 44.0077, 79.0063, 43.9772, 153.069, 121.592, 30.9946, 155.083, 176.128, 147.038, 105.332, 191.284, 40.846, 89.3094, 8.58791, 185.199, 101.73, 87.466, 128.184, 46.9558, 160.924, 59.2303, 9.69268, 60.3168, 133.048, 84.64, 9.24711, 153.722, 85.0429, 164.574, 120.811, 146.403, 51.0086, 154.405, 178.576, 169.488, 22.0588, 72.5425, 34.2845, 62.0991, 111.161, 184.082, 109.574, 142.424, 92.9533, 59.328, 123.325, 153.825, 118.632, 7.47093, 194.006, 185.931, 5.7741, 85.11, 117.008, 170.721, 103.189, 79.8791, 16.9988, 153.636, 130.735, 69.039, 175.262, 139.653, 194.195, 153.075, 32.0383, 114.402, 175.109, 164.269, 163.878, 40.2356, 95.6145, 150.316, 35.9691, 27.3324, 58.2965, 32.9112, 129.502, 79.8059, 161.04, 41.3465, 43.9894, 16.7669, 186.834, 121.097, 174.645, 168.45, 68.6544, 117.405, 164.678, 47.2671, 129.319, 133.909, 132.676, 193.97, 96.2676, 66.1946, 74.9413, 198.651, 91.4884, 110.434, 42.8175, 96.9085, 196.295, 46.9192, 95.0652, 140.934, 191.192, 98.7335, 25.1656, 145.555, 70.7358, 165.264, 63.6921, 9.3936, 133.372, 30.8542, 155.657, 10.9195, 46.2416, 148.387, 12.8239, 79.5557, 85.0856, 197.845, 141.099, 41.7554, 149.046, 19.9408, 42.8236, 42.14, 150.511, 124.711, 194.165, 139.543, 179.986, 175.072, 197.412, 78.1335, 45.912, 45.4543, 108.365, 33.4971, 189.532, 55.385, 138.658, 133.738, 174.7, 178.076, 133.091, 155.571, 152.745, 23.9937, 21.9977, 195.41, 102.341, 199.53, 48.5855, 115.452, 44.3068, 40.4859, 171.252, 164.812, 31.4646, 120.774 };
						int n[1024] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[1024];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 7) {
						float s_mean[2048] = { -997.253, 476.974, 799.615, -109.47, -346.599, -660.817, -227.76, 962.523, 819.697, -593.616, -326.212, 154.637, -90.8536, -636.097, 813.471, -484.359, 751.885, 196.265, 692.129, 319.254, 701.346, -899.045, -771.172, 103, -13.7028, -347.026, 237.648, -42.5733, -687.613, 218.238, -846.37, 65.2181, -286.355, 618.213, -990.051, -510.056, 653.92, -956.481, 664.174, 215.613, 378.521, -781.426, -725.089, -564.867, 516.037, -184.912, -971.618, 90.0601, 929.563, 116.245, 114.963, -340.983, 818.232, -68.9413, -44.4044, -802.789, 69.0634, -241.859, 678.701, 625.355, 858.455, -800.104, 846.492, 268.41, -496.506, -373.455, -568.651, 645.314, 563.89, -909.848, 43.0616, 507.492, -806.879, -511.155, -595.508, -872.005, -259.56, -19.3793, 78.9514, 840.999, 602.161, 647.633, -553.88, -169.836, -676.931, 410.138, 929.075, -831.782, -51.4237, -809.503, -529.527, 105.93, 625.538, 327.616, -740.41, -388.958, -545.579, -759.575, -291.604, 157.384, 590.503, 342.448, -321.574, -236.061, 595.569, 205.42, -42.8785, 284.097, -960.509, 973.876, -15.4118, -55.7573, -648.183, 28.5958, 830.195, 133.335, -76.8761, -98.6663, 418.744, 441.694, -543.138, 438.337, -992.492, -49.8978, 570.788, -785.394, 912.656, 768.853, -351.665, 198.096, 485.641, -173.07, 940.733, 663.564, 339.824, 910.886, 917.844, 356.548, -172.765, 954.1, 870.174, 907.956, -235.084, 575.61, -239.418, -502.548, -588.427, 869.442, -674.734, 768.12, -305.826, 443.464, -573.962, 989.929, 684.378, 708.609, -348.857, 474.227, -674.917, -582.385, -832.636, 237.831, -592.578, 454.756, 821.589, 916.746, -203.284, -126.072, -508.164, -69.4296, 909.421, 188.757, 689.505, 896.298, -560.595, 910.825, -589.587, -707.45, -665.029, -660.878, -763.359, 373.638, -975.097, -400.739, -311.991, -666.616, 895.505, -67.8426, -176.305, -280.374, -983.52, 233.253, 93.6003, 97.8118, -881.527, -158.422, 440.168, 965.453, 818.781, -421.003, 649.831, -84.2616, 98.117, 38.9111, 954.588, -947.874, 27.6193, 125.645, 883.419, 652.15, -255.165, -156.652, 364.238, -918.821, -319.437, -665.029, -925.108, 957.274, -964.599, 171.606, 45.4421, -435.835, -493.576, 891.354, -491.134, -483.566, -736.747, 788.263, -996.582, -327.25, -824.641, -48.2498, -276.345, 300.272, -712.027, -444.624, -877.743, 488.754, -450.911, 395.978, -238.441, -185.583, 785.699, -317.301, 414.96, 254.189, -69.4296, 76.3268, -581.286, 349.651, -501.205, 977.783, 273.415, -378.826, -685.72, 67.7816, -504.624, -768.059, 486.312, -904.477, 718.497, 902.158, -363.872, -81.9422, -954.588, 72.6035, -626.514, 788.934, -350.2, -680.593, -976.745, 897.519, 626.942, 17.7313, -397.992, -841.914, 96.0417, 255.776, -955.565, -751.03, -448.469, 998.901, -319.926, 901.364, -535.325, 503.891, -40.2539, -561.693, -864.925, 982.727, 432.173, -435.774, 792.291, -483.261, -29.4504, -596.362, -52.7665, -779.168, 249.184, 790.277, -5.95111, 657.46, 469.832, -194.006, -938.536, -499.619, 338.481, -429.731, -885.678, 449.263, -708.182, -721.061, -694.693, -606.738, -26.2154, -506.882, -423.75, -174.474, -37.7514, -321.268, 510.056, -493.21, -254.677, -4.85244, -568.102, -620.106, 948.241, -665.7, 761.04, 733.451, 809.565, -389.813, 612.232, 616.077, 908.2, 297.281, -352.702, -499.435, 139.5, -779.962, 177.038, -802.118, -57.8936, -408.185, -487.899, -978.942, -225.501, -696.036, -434.858, 199.438, 841.365, 464.888, 702.322, 542.711, 70.3452, -811.09, -155.614, -645.131, 338.603, -364.116, 818.842, 598.56, 949.217, 299.6, 961.18, 308.634, 357.952, 547.166, -33.5398, 256.691, -850.398, -591.906, 991.089, -134.922, -735.099, 613.514, 3.93689, 370.586, -523.301, -115.39, -611.56, -761.101, -883.419, 472.396, -206.153, 828.608, 51.7289, 946.532, 236.732, -694.998, 911.252, -765.923, 452.681, -195.532, -437.422, 909.177, -501.999, -151.708, -161.473, -268.654, 600.94, -993.042, 513.474, 435.347, 263.466, 813.959, 529.832, -230.323, -387.188, 930.052, 474.96, -214.209, 3.14341, -442.793, 307.474, -622.852, 866.756, 102.817, -665.944, 68.5751, -184.057, 93.5392, 435.652, 54.7197, 994.69, 339.518, 544.298, 489.669, -320.292, 348.491, -630.421, 25.6661, -867.306, -10.3458, 375.408, -397.504, -298.746, -592.151, 6.19526, -513.84, 170.873, -812.25, -836.299, 558.336, -633.778, 767.632, 933.592, -144.993, 2.9603, -896.481, 561.876, 355.937, 789.117, -416.303, -318.583, -976.501, 776.788, -808.039, 788.812, -908.139, -36.5307, -77.5475, -29.3893, 605.884, 143.956, 443.098, -119.907, -789.727, 11.5665, 10.1627, 963.744, -811.274, 510.056, 632.069, 937.62, 752.983, 434.797, -516.648, 514.451, -418.439, 284.646, -430.83, -620.411, 609.424, 363.262, 638.844, 418.012, 66.3778, -869.015, 350.139, 190.71, -233.314, -193.884, -316.63, -338.847, -352.214, -783.502, 605.518, -827.998, 559.618, 901.364, -16.0222, -328.166, -696.768, 462.386, -248.878, 191.137, -79.4397, -834.162, -682.18, -430.525, -767.266, -206.336, 238.014, 431.867, 629.749, 745.415, -877.804, -584.948, 108.982, 389.447, 802.24, -355.51, 564.745, -335.185, -255.348, -539.354, 327.189, 932.615, -284.829, -604.236, -48.8601, -922.422, -307.291, -599.902, -744.621, 859.981, 33.2957, 318.827, 611.866, 516.404, 884.274, -381.634, 484.298, -387.799, 848.201, -636.891, -782.342, -857.418, -102.878, 442, -922.91, -115.879, 536.607, -955.321, 536.973, 996.582, -780.511, -611.255, 819.819, 874.142, 10.4678, 695.853, 685.72, 247.475, 499.619, 418.073, -529.038, -465.255, -33.1126, -284.829, 377.606, 443.525, -204.138, -819.452, 11.9327, -177.709, 138.096, 818.049, 174.047, -238.197, 826.838, 268.349, -103.427, -52.9496, 541.49, -714.164, -266.03, 258.339, 221.229, 464.949, 301.431, -376.812, 729.545, 216.712, -935.972, -882.199, 767.937, -4.73037, -624.195, -681.204, -959.96, -225.196, -231.361, -95.7366, 666.005, -224.403, -508.469, -494.186, -154.088, 441.023, 299.6, -189.856, 774.651, 663.747, 493.149, -89.7549, 805.109, 262.551, -793.39, 907.224, -136.876, 708.792, 947.935, 761.773, 110.813, 102.939, 387.066, -604.175, -139.012, 401.227, -604.419, 44.1603, -258.705, 892.209, 335.673, 855.586, -892.087, -76.9372, -401.227, 744.987, 710.135, 876.705, -604.846, -505.417, -396.283, 397.626, -207.373, 105.747, 315.897, 869.259, -994.263, -599.78, 258.4, -860.286, -65.0349, 871.944, -607.593, -500.961, 955.443, -211.463, 142.064, 905.454, 956.236, 542.833, -441.328, 461.837, 629.566, 254.555, -265.053, -857.601, 9.55229, 621.693, 468.795, -230.567, 261.513, 22.248, 177.526, -760.43, -279.214, -134.373, -938.047, -999.573, 182.104, -805.597, 520.432, -222.694, -561.632, -464.339, 812.738, 549.669, 69.9179, 2.83822, 974.425, -222.51, 483.383, 646.901, 980.59, 878.292, 201.086, 58.1988, 160.314, -46.7849, -600.391, -782.037, -139.683, -236.061, 699.454, -400.006, 359.233, -40.9253, -796.564, -478.133, 31.7698, -449.263, 786.493, 746.818, 762.505, -358.806, 633.961, -664.907, -389.996, 399.213, 31.3425, 745.903, 721.305, 286.843, -597.095, -811.701, -504.746, -463.973, -399.091, -776.482, 997.314, -125.034, -555.345, -384.014, 622.974, 504.685, -490.402, -564.989, -987.854, 325.724, 166.295, 406.537, -786.554, 414.716, -130.467, -485.641, 97.7508, 750.786, -302.53, -147.801, 22.5532, -807.794, 140.538, -275.369, 746.086, 703.116, 140.599, -0.824, -408.979, 829.768, 379.986, -292.337, -140.782, -572.741, 820.612, -102.329, -505.051, 636.158, 492.965, -265.725, -0.396741, -891.049, -464.827, 851.619, -404.645, 486.801, -517.808, -45.7472, 639.637, 125.645, -195.288, -331.095, -711.417, 73.2749, 698.233, -353.862, -588.122, 295.816, 122.166, -88.4732, 756.218, 153.844, -911.069, 467.513, -640.675, 129.551, -796.93, -854.976, 214.637, -197.058, 450.179, -295.206, 184.729, 78.4631, -764.153, 189.184, -834.284, -338.481, 675.222, -476.608, -498.703, -214.698, -868.587, 367.473, -353.496, -813.105, -994.873, 217.444, 82.2474, -665.639, -226.6, 757.317, -521.104, -97.1404, -721.366, -539.842, -889.096, 501.45, -732.047, -559.862, -479.72, 649.586, -817.988, -446.333, -569.018, 3.57067, 194.128, 751.274, 912.961, 833.491, -500.656, 678.884, 796.136, 749.321, 853.45, 529.71, 334.513, -322.367, -637.867, 145.604, -879.818, -911.557, 520.127, -631.397, -752.434, -528.611, -500.168, -357.28, -272.744, 62.4714, 691.092, -134.922, -849.849, 820.551, -510.605, 487.899, 601.55, 653.981, 335.795, -130.161, 825.19, -102.817, -994.69, 877.682, -362.712, -962.889, -853.511, -262.246, -409.162, -310.038, -929.991, -862.423, -110.874, -172.46, 788.141, -422.468, 550.462, -746.086, -696.646, 972.106, 821.711, -667.959, 148.045, 501.755, -157.445, 960.387, -827.021, 714.469, -440.291, 799.127, 586.596, -123.02, -109.287, -252.846, -755.974, 576.891, 138.218, -331.584, -813.593, 573.656, 653.859, 368.511, 429.548, -99.5209, 752.373, 821.162, 3.14341, 215.979, 875.851, 645.009, -760.063, 405.377, -992.615, -170.141, -685.232, 432.295, -664.296, 713.675, -966.43, -559.618, -831.233, 208.533, 872.311, 104.77, 97.9339, 277.932, -733.818, -766.533, -587.939, 662.648, -851.07, 619.373, -238.258, -142.369, 741.447, 141.331, -926.939, 338.786, 590.991, -883.969, 711.905, 39.0942, 860.408, -100.436, -102.268, 75.3502, 867.122, -261.269, 368.023, 724.845, 872.005, 872.86, -707.999, -348.186, 118.686, 885.189, 435.041, 384.014, -877.377, -695.486, -427.839, -960.753, 209.876, 456.954, 152.928, -697.989, 306.558, 567.064, -785.333, -349.956, -22.6142, -890.255, -452.986, 830.805, 715.506, 411.725, 105.93, -411.969, 480.636, -496.994, 677.664, 638.6, -511.093, 218.726, 792.718, 898.495, 251.625, -449.812, 890.255, -40.0098, -135.96, 247.047, -268.166, 817.927, 545.03, -331.523, 65.5843, 846.187, 425.581, 884.823, 540.33, -975.585, 588.244, 29.1452, -130.528, -585.376, 355.998, -101.108, -754.692, 887.57, 831.294, -192.724, -997.131, -900.754, 685.293, -314.92, -465.865, 718.009, 196.814, 431.928, 452.01, -99.6429, -458.724, 518.113, 4.30311, -981.933, -669.912, 835.994, 738.09, -365.398, -600.635, -198.462, -586.657, -268.532, -150.792, 706.29, -832.514, -814.875, 332.682, -681.936, -70.8335, -506.699, -405.255, -266.518, -917.966, -248.512, -738.273, 478.988, 379.62, 879.94, -119.968, 862.423, -891.598, 99.6429, -794.916, -425.336, -497.177, -340.678, 740.959, -458.602, 408.551, -188.147, 528.733, -975.646, -802.973, 40.7422, 186.132, 973.083, -70.5283, 536.851, 196.326, 626.331, -229.286, -75.1061, -553.148, -636.341, -346.538, -760.186, -980.895, 807.428, -807.672, 153.844, 227.088, -826.472, -152.074, -547.655, 906.308, -126.194, 340.739, -238.868, -46.7238, 366.863, 315.409, 522.385, 114.414, -739.738, 254.677, -778.314, -975.219, -338.359, -356.426, 864.315, 497.726, 780.633, -616.749, -298.196, 63.448, -220.069, 455.367, -791.253, 758.293, -122.532, 835.688, 309.244, -836.238, 697.623, 658.742, -146.886, -304.361, 872.555, -368.755, -656.972, -857.051, 894.406, 696.89, -89.1446, -542.894, 216.285, -50.4471, -456.099, 790.948, 604.053, 798.151, 561.998, 787.713, -262.001, 380.169, -656.423, 120.212, -935.789, 225.99, 302.896, 279.885, -103.854, -827.143, 115.635, -384.198, -233.619, 529.099, 196.387, -376.08, -972.106, -512.253, -575.365, -514.267, -294.046, 791.253, -878.353, 857.784, 599.414, 644.948, 276.894, 910.398, 632.313, 881.405, -975.585, -383.038, 455.611, -625.416, 369.854, -707.083, 298.624, 703.665, 166.601, 384.747, 70.5283, -932.798, -657.46, -490.951, -287.942, -552.721, 376.69, -258.095, -203.04, -976.562, 25.4219, -842.647, 383.648, 836.848, 299.478, -382.977, 116.672, -295.938, -309.854, -191.076, -421.613, 381.695, 646.229, 536.363, 890.378, -20.1117, -700.003, 257.424, -434.919, 626.698, -555.467, 873.653, 607.532, 382, 759.636, -230.689, 866.573, 401.715, -711.478, 100.375, -363.323, 351.665, 164.892, -666.311, -490.524, 71.8101, 588.733, 35.7982, -524.827, -509.751, -946.715, 912.29, 274.392, 787.286, 821.833, 96.6521, -0.701926, 809.32, -346.355, 689.444, -54.2314, -266.701, -218.787, 547.411, 714.286, -19.5013, 136.265, 322.245, 561.998, -619.495, 826.594, -249.855, 4.12, 585.681, -137.425, 190.405, 665.456, 734.611, 133.946, -585.253, 381.939, -633.229, 635.853, 646.29, 552.293, 586.413, -54.1704, -151.463, -260.475, -523.85, -49.6536, -735.221, -477.462, 761.834, -178.869, -793.329, 597.766, -274.087, 242.042, 93.7223, -869.625, -627.979, -114.78, 741.997, -643.666, -161.29, 138.096, 903.378, 433.638, 63.448, 79.6838, 54.6587, -456.099, -303.018, -292.764, 436.689, -755.913, -732.658, -587.268, 342.936, -932.005, 593.738, -290.445, -340.739, -622.425, -549.547, -791.131, -347.575, -534.959, 481.674, 846.492, -990.539, -96.2859, 364.605, 359.05, 126.621, -849.666, -775.933, -989.135, -351.726, 396.039, 296.548, 570.544, -743.583, 668.264, -155.919, 537.156, 709.281, 181.982, -681.509, -382.916, -869.442, -58.1988, -224.769, -549.181, 548.631, -407.086, 537.645, 588.977, -314.615, -476.913, 110.935, 719.169, -4.60829, -785.943, -653.615, -857.479, -45.1369, -156.835, 855.098, -499.496, -398.968, -865.291, 672.292, 750.969, -679.312, 461.837, 171.422, 394.94, 889.828, 197.851, 228.004, -46.5407, 762.2, -682.119, -366.008, 693.716, -391.339, -749.565, -238.502, -394.147, 106.235, -20.9662, -522.935, -370.525, 977.111, 393.475, 305.704, 130.345, -722.465, 891.842, -155.065, -373.455, 937.315, 231.666, -571.154, 765.496, -451.643, 631.825, 42.1461, -449.751, -595.874, -876.278, 484.298, 816.34, 788.629, -846.675, -216.651, -42.2681, -484.848, 432.356, -139.622, 904.172, 715.873, -960.204, 443.342, 347.209, 880.49, -226.051, 230.934, 216.895, 145.421, 753.41, 307.047, 408.429, 832.942, 268.96, -195.41, -241.31, 240.15, -114.841, -180.334, -727.409, -322.001, -208.228, -424.543, -399.823, -239.418, 405.255, 836.482, 762.383, -56.1235, 220.557, 572.558, 128.391, 314.982, -87.4966, -127.049, 641.652, -368.755, 687.49, -889.828, -703.299, -390.79, 884.884, -645.436, -134.251, -536.363, 828.608, 37.8124, -124.973, 696.829, -71.0166, 867.244, -168.432, 137.181, 33.9061, -10.1016, 699.637, -771.111, -969.481, 406.781, -372.784, -429.426, 697.134, 636.341, -76.9982, 704.398, 804.193, -370.769, -717.582, 155.248, -533.86, -68.0868, 264.382, -280.251, -256.752, -899.167, -981.994, 5.34074, 492.66, -633.29, 259.133, -780.877, -529.771, -451.949, 496.628, 207.495, 682.119, -784.112, -490.89, -255.654, -13.7028, -892.636, 71.8711, 71.0776, 944.151, -210.059, -67.3544, 684.927, 921.812, 575.488, 39.0332, 378.277, -558.031, 684.317, -985.595, -472.152, 785.638, 659.23, -115.146, 633.656, 843.135, 29.6335, -944.578, -892.026, 215.918, -774.468, -462.752, 314.615, -431.44, -258.278, 423.322, 857.784, -770.44, 551.744, 615.711, 121.189, -956.481, 771.722, 801.874, 57.2832, 472.396, 574.999, -318.949, -773.98, 947.081, 355.632, 441.389, -915.769, 469.039, 142.796, -514.389, 524.461, 462.02, -735.221, -591.418, 664.113, 873.226, 262.917, 223.06, -115.574, -474.837, -944.334, 836.665, -880.184, -282.632, -647.084, 778.375, -603.381, 77.4255, -680.105, 175.024, -377.728, 663.015, 491.012, -21.9428, 624.378, -686.758, 934.202, 439.741, -560.289, 250.465, 665.029, -160.009, -146.519, 931.394, -689.505, -537.034, -811.823, -851.07, 23.4077, 823.542, -657.338, 384.075, -354.045, -856.38, 81.3929, -593.31, -875.546, -920.713, 573.229, 127.781, 172.765, 696.829, -819.33, -299.295, 186.804, -300.577, -501.022, 872.799, -591.723, -95.9807, 645.07, 586.962, 339.152, -32.5632, -203.406, 833.247, -594.897, 363.262, 981.14, -249.672, 870.357, 608.264, 115.207, 977.233, -999.084, 738.945, 293.252, -745.415, 479.476, -961.913, -673.269, 276.711, -931.394, 11.6886, -466.659, 792.535, 525.681, 614.856, -224.219, 957.579, -233.802, 728.874, -248.939, 935.545, 278.359, -112.461, 529.466, -250.343, -213.172, 253.945, -656.362, 384.32, 167.028, -153.294, 627.735, -863.399, -391.156, 16.9378, -693.899, -781.915, -968.749, -825.678, 332.987, 606.128, 745.781, 764.275, 480.941, 145.421, -629.078, -506.272, 380.413, -100.314, 996.216, -191.321, 808.466, -246.742, 596.057, 619.739, -772.088, 262.673, -306.07, 279.458, 576.586, -242.103, 276.65, -191.565, 305.948, -305.582, 958.068, 372.234, -329.264, 254.311, -606.922, -992.553, -732.353, 198.157, 942.503, 598.926, 824.335, 966.491, -414.167, 105.747, -761.223, -968.932, -913.45, -961.974, 868.16, 497.177, 405.438, 183.63, 67.7816, -45.7472, -635.243, 584.46, -19.3793, -421.857, 562.609, 763.054, -424.177, 482.528, -463.363, 199.316, 938.108, 320.17, -821.65, -942.991, 806.635, -865.169, 396.954, 160.314, 364.299, -537.278, -966.369, -779.778, -723.441, 683.95, -974.425, -688.528, -19.2572, -375.774, -180.212, -401.288, 224.83, -593.554, 37.5683, 313.395, -143.406, 552.171, -611.866, -20.1117, -136.998, 333.048, 833.125, 892.758, -341.288, 372.784, -999.817, 842.586, 625.599, 498.154, 847.591, -907.102, -98.4832, 841.182, 577.746, 606.922, 419.599, 190.161, 801.508, 523.606, -802.85, 591.601, -47.5173, 884.64, -125.278, 201.392, -49.4095, -389.203, -494.675, 386.883, 116.489, -271.34, -406.659, -921.995, -664.418, 847.102, -616.443, 369.06, 472.03, 883.602, -599.597, 370.159, -967.467, 41.2915, -763.115, -691.153, 81.2098, -964.843, -406.354, -118.137, -597.339, -431.196, 568.102, -143.895, 916.684, -939.268, 193.396, 921.018, 342.143, -378.765, -772.454, -210.669, -889.035, -721.122, 192.602, -450.606, 532.456, -543.199, 663.381, -733.879, 206.946, 699.698, 806.879, -167.15, 690.298, 650.014, -828.852, 72.9087, 407.514, 274.697, 724.54, -909.055, 817.316, 341.716, -855.647, 874.996, -174.291, 146.641, -643.483, -852.718, -549.486, 489.303, 324.076, 466.659, -265.114, 900.021, 86.52, -420.698, 911.985, -883.541, -951.17, -979.003, -181.616, 810.358, -479.354, 968.322, 558.885, -436.506, -494.919, -659.841, -580.187, -905.21, 738.029, 149.632, 33.723, -263.222, 976.867, 105.136, 792.962, -838.069, 540.025, -537.645, 513.962, -640.614, -904.355, -874.325, 351.482, -138.401, 700.919, -0.274667, -37.6293, -779.351, -909.116, 590.747, -80.3552, 735.16, -782.342, 858.577, 376.751, 426.374, -960.509, 317.85, -18.4637, -734.794, -440.229, 389.813, -959.838, -351.36, -883.908, -996.338, 931.516, 126.072, -80.5383, -551.744, -229.225, 376.141, 843.074, -526.353, 324.076, 433.21, 461.592, 642.689, 171.972, 163.793, 340.19, -67.6595, 123.875, 993.53, 573.656, -414.411, 882.138, -116.916, -424.848, -787.164, -44.2213, -444.624, 115.757, 143.529, 825.007, 971.801, -386.883, 27.5582, 354.778, 58.6261, 670.827, -313.028, 459.334, 684.072, -132.908, 217.566, 15.9001, -115.696, 794.305, 340.19, -971.007, -659.536, -652.455, 2.77718, -955.565, -894.955, -735.71, 231.483, -186.682, -421.735, -87.1914, -767.693, 205.969, -265.175, 331.645, 742.912, -623.646, -399.335, -481.429, 633.9, 176, 979.125, 969.787, -239.479, 817.743, 427.595, -531.48, -935.728, 790.277, 484.481, 124.79, 924.375, -978.149, -854.732, -336.161, 759.209, 294.656, -410.688, -34.5164, 878.231, -91.464, 156.346, -146.764, 583.178, 595.386, 573.168, -866.817, 519.883, -317.301, 429.914, 883.358, -720.389, 508.469, -92.9899, -7.84326, 251.015, 84.3837, 751.396, -51.7899, 721.061, -989.624, -496.75, -121.067, -772.881, -361.248, 972.655, -160.314, -841.426, 487.777, -34.2112, 352.947, 607.898, 906.186, -738.09, 197.302, 472.518, -12.8483, -160.192, 446.333, 806.513, -267.8, -645.985, 758.049, -578.112, 465.499, 861.934, -86.3369, 740.349, 497.543, -200.11, 606.8, 894.772, 39.7046, -789.85, 482.406, -919.431, -632.069, -782.891, -574.023, -842.097, -763.054, 754.082, -897.519 };
						float s_var[2048] = { 21.6742, 91.3846, 80.0623, 38.6731, 88.2046, 27.9427, 196.301, 91.1771, 14.6489, 23.2307, 16.3701, 156.255, 56.9292, 185.205, 154.149, 138.383, 172.6, 86.5444, 155.98, 84.5119, 151.317, 138.89, 68.6972, 95.7549, 20.3192, 42.0118, 82.5648, 114.127, 124.882, 108.084, 49.2264, 184.246, 83.7855, 60.86, 74.7276, 190.057, 19.8004, 109.922, 187.439, 66.5731, 65.1387, 99.8749, 81.0144, 86.5688, 141.935, 134.587, 132.426, 22.2907, 17.6946, 76.3878, 94.8576, 182.22, 35.8043, 120.304, 65.7369, 122.733, 120.945, 43.263, 129.643, 34.8582, 59.6026, 128.343, 76.3268, 6.00604, 55.5315, 163.939, 193.664, 156.652, 160.21, 118.027, 135.588, 52.0585, 178.301, 87.3623, 65.1265, 189.038, 48.1521, 121.72, 72.3106, 157.604, 97.2442, 123.069, 67.1773, 77.7551, 172.655, 19.837, 147.728, 111.161, 46.5346, 16.1077, 42.8785, 47.8652, 96.5911, 115.842, 30.4697, 168.047, 61.5253, 122.098, 130.21, 178.698, 71.5232, 111.24, 171.63, 25.367, 153.728, 182.183, 137.077, 178.887, 12.6835, 80.5811, 179.675, 197.046, 85.8364, 98.0743, 116.3, 161.364, 73.9708, 46.4736, 195.532, 188.781, 146.709, 107.022, 140.629, 18.366, 22.9438, 154.57, 67.5192, 73.0735, 126.371, 156.822, 77.7367, 196.423, 15.8696, 48.5488, 126.408, 64.2293, 91.0367, 80.813, 165.215, 71.3462, 172.649, 45.1674, 160.369, 93.35, 158.086, 172.893, 185.577, 123.27, 151.866, 109.207, 190.002, 183.325, 71.0593, 162.493, 41.2488, 111.606, 125.791, 28.5043, 43.3241, 23.835, 174.682, 189.599, 129.606, 50.0626, 155.345, 188.946, 130.79, 146.391, 189.843, 179.742, 117.911, 181.671, 165.142, 149.455, 20.2521, 99.2523, 188.61, 19.0008, 118.369, 153.716, 118.455, 17.0049, 12.006, 22.5166, 169.213, 129.002, 63.1062, 192.816, 192.224, 21.6315, 113.755, 23.0659, 31.0617, 41.908, 79.165, 79.8608, 152.068, 91.3968, 15.6316, 149.571, 149.748, 175.451, 63.5151, 125.071, 127.287, 27.4178, 65.1875, 167.498, 152.007, 7.88598, 58.0401, 142.924, 185.839, 78.8659, 42.4024, 134.117, 90.7682, 53.029, 24.5491, 27.2164, 109.354, 117.942, 134.666, 173.498, 20.13, 35.2428, 19.7821, 126.048, 168.145, 14.9052, 111.795, 51.503, 170.36, 64.4246, 170.397, 16.7852, 96.1821, 13.0619, 101.041, 150.798, 122.892, 11.0355, 47.9446, 81.1365, 66.1214, 103.769, 162.053, 45.8327, 185.345, 81.0755, 43.9528, 80.0623, 64.2903, 1.40996, 61.916, 77.8649, 166.491, 191.082, 144.823, 128.849, 65.6758, 118.448, 50.1419, 129.942, 151.292, 143.156, 106.9, 72.0664, 8.94192, 140.361, 33.6558, 1.90435, 75.8751, 199.939, 31.4524, 58.5894, 171.435, 0.689718, 151.805, 185.418, 102.536, 59.6088, 139.665, 116.581, 144.151, 56.0259, 151.366, 38.8989, 194.751, 30.903, 147.636, 128.507, 6.78121, 61.0004, 161.528, 162.603, 92.1537, 8.11182, 41.4014, 6.12812, 102.683, 143.297, 68.8986, 173.553, 85.5617, 46.0585, 51.912, 65.511, 13.8615, 178.631, 92.5321, 80.5872, 191.968, 135.118, 30.0363, 2.00201, 136.039, 165.783, 158.873, 59.3341, 129.521, 176.208, 21.5766, 65.2547, 53.5722, 1.56255, 76.8029, 27.2225, 92.5565, 155.797, 42.2498, 34.2174, 114.499, 55.855, 112.638, 18.4698, 126.572, 120.908, 52.6811, 192.145, 5.53606, 146.562, 165.087, 159.099, 103.238, 154.094, 133.451, 101.566, 127.067, 114.646, 140.3, 41.9141, 95.4131, 148.466, 60.0177, 179.333, 168.151, 199.115, 126.847, 101.547, 152.104, 182.684, 77.81, 71.5659, 81.5088, 12.0182, 170.721, 189.392, 73.5008, 95.9502, 77.1325, 28.7118, 58.5467, 113.474, 50.325, 148.668, 185.968, 63.0085, 168.249, 80.5017, 104.343, 186.615, 51.503, 189.367, 117.032, 169.164, 56.3128, 10.9561, 12.6225, 12.6286, 142.503, 63.2954, 170.409, 185.26, 16.4312, 133.409, 140.825, 45.7778, 149.01, 39.0515, 100.961, 142.674, 68.6117, 65.4622, 185.022, 188.348, 129.112, 77.0287, 89.7549, 108.359, 152.794, 104.05, 154.973, 165.661, 148.344, 4.41908, 118.992, 159.795, 103.824, 0.37843, 149.113, 101.108, 54.6342, 90.2127, 147.081, 160.002, 26.4901, 134.971, 129.551, 91.1161, 53.3647, 20.1849, 83.5719, 132.572, 70.4855, 92.6237, 97.0489, 180.169, 82.3084, 71.9321, 164.214, 36.6222, 5.4445, 37.4401, 66.0665, 135.783, 186.682, 89.8221, 125.901, 141.832, 195.904, 87.8323, 39.7168, 23.5237, 94.1069, 60.0299, 181.726, 3.39976, 176.336, 91.4945, 184.796, 182.018, 189.648, 85.5373, 195.935, 37.1166, 26.1116, 0.421155, 6.75069, 197.925, 90.7682, 161.101, 58.9496, 94.2839, 144.566, 45.9059, 124.741, 80.9534, 22.3701, 148.296, 62.3676, 36.7565, 25.9163, 113.547, 46.5529, 38.8623, 191.778, 150.487, 73.9586, 111.948, 34.315, 118.216, 143.669, 52.4613, 163.713, 87.8262, 112.656, 127.805, 178.857, 179.107, 26.2398, 50.5936, 35.3526, 72.2678, 123.576, 27.8329, 88.5098, 194.696, 118.094, 85.165, 11.2735, 182.586, 153.001, 66.0726, 187.225, 17.7496, 31.7209, 106.113, 178.497, 39.3017, 123.386, 182.122, 68.8131, 30.3598, 143.284, 136.436, 81.4234, 179.791, 8.34986, 176.116, 72.0603, 118.894, 167.626, 47.1633, 96.9268, 33.1736, 7.70287, 83.7489, 55.6413, 49.0127, 133.25, 132.395, 9.25932, 95.4131, 122.41, 151.25, 173.174, 40.6873, 157.341, 84.2555, 185.131, 104.227, 129.618, 10.3946, 48.1704, 143.181, 20.7465, 167.925, 148.643, 14.1667, 157.628, 53.5295, 149.101, 10.0345, 143.004, 196.625, 156.34, 171.557, 60.9394, 8.50856, 164.324, 74.1234, 157.305, 106.638, 172.179, 89.523, 195.209, 113.773, 177.142, 122.593, 85.5068, 132.273, 42.5367, 22.9865, 192.261, 93.2707, 40.7605, 71.8894, 113.169, 180.908, 71.218, 143.693, 135.099, 131.535, 153.331, 142.759, 164.495, 7.94702, 194.458, 115.134, 132.896, 48.616, 32.9051, 35.2489, 77.3705, 191.87, 155.889, 67.6412, 95.999, 75.1305, 92.4711, 103.696, 178.35, 109.153, 81.576, 173.327, 110.923, 27.2225, 14.9236, 131.736, 57.6556, 109.378, 55.0249, 21.0944, 15.363, 81.6126, 128.104, 107.297, 144.206, 90.1334, 92.1354, 191.656, 173.18, 150.542, 179.754, 197.626, 80.5567, 72.0664, 116.245, 32.5083, 49.3912, 56.801, 112.534, 124.229, 97.9583, 173.888, 133.11, 32.9661, 185.229, 108.744, 39.0393, 137.944, 191.327, 187.317, 5.90838, 199.115, 173.04, 75.2525, 16.7913, 193.89, 94.2473, 152.153, 30.2377, 28.8461, 31.3913, 160.772, 23.2185, 126.481, 53.975, 131.236, 109.122, 86.8618, 180.004, 180.279, 130.68, 24.427, 6.45161, 185.443, 127.995, 179.65, 14.7588, 134.306, 81.4539, 5.74969, 151.28, 148.704, 129.734, 130.485, 82.7357, 183.941, 62.7461, 192.761, 126.725, 2.99081, 89.6207, 154.454, 57.1551, 25.7881, 81.6736, 87.051, 184.741, 53.3403, 90.3653, 33.43, 112.436, 67.037, 140.281, 144.536, 165.947, 102.817, 97.0916, 91.1039, 99.1546, 0.396741, 10.535, 150.401, 74.6239, 27.5277, 132.365, 167.937, 119.059, 179.15, 9.32646, 194.159, 53.7492, 151.646, 51.1429, 196.197, 100.076, 192.083, 173.467, 44.5814, 159.954, 22.3762, 115.836, 174.957, 171.306, 38.5693, 34.6812, 124.778, 138.536, 184.991, 171.264, 145.067, 38.0016, 103.513, 194.635, 87.1181, 198.486, 157.775, 19.6478, 164.208, 156.621, 132.371, 99.8932, 188.305, 187.439, 7.0864, 195.672, 81.814, 109.763, 6.13422, 16.9561, 64.2964, 166.729, 122.33, 141.404, 114.158, 181.213, 116.355, 7.2512, 105.387, 186.346, 190.948, 181.359, 19.2755, 130.924, 140.129, 102.115, 31.1411, 59.9506, 173.431, 97.4578, 72.0237, 170.031, 15.0884, 77.8039, 65.4317, 139.891, 78.3044, 104.056, 39.0454, 135.82, 114.432, 30.5124, 111.435, 92.1659, 137.181, 22.9133, 153.783, 66.7684, 21.8757, 151.353, 126.603, 71.4682, 179.675, 58.2415, 91.464, 177.526, 106.723, 101.334, 97.702, 96.1394, 8.45973, 171.648, 144.139, 33.8939, 98.7884, 186.431, 15.7903, 23.2124, 174.291, 57.1551, 195.624, 127.866, 63.448, 132.493, 51.2223, 123.386, 150.908, 116.239, 96.5606, 52.5895, 176.629, 60.329, 128.66, 56.0198, 110.147, 32.7769, 39.3628, 77.4133, 99.7894, 60.6586, 102.036, 44.1664, 96.878, 148.07, 133.268, 122.825, 154.961, 169.109, 129.606, 49.5254, 4.00403, 35.8348, 179.284, 135.502, 99.6857, 141.252, 48.909, 147.496, 147.502, 77.2301, 45.2223, 63.2832, 102.084, 27.5521, 151.787, 102.121, 41.3648, 35.8165, 40.7117, 30.195, 115.812, 148.436, 159.38, 141.136, 172.655, 32.5877, 92.1537, 111.429, 55.0737, 69.216, 197.113, 130.937, 176.464, 120.957, 117.753, 108.335, 169.616, 175.146, 169.72, 151.598, 10.9439, 61.7512, 2.50862, 123.453, 19.7272, 46.382, 136.229, 71.865, 132.304, 98.233, 173.241, 135.49, 51.2711, 33.0332, 84.9574, 124.155, 69.7653, 131.071, 173.528, 174.322, 170.623, 30.1035, 108.542, 79.5495, 0.628681, 93.3744, 38.7707, 95.9868, 119.724, 52.5346, 178.887, 22.8095, 125.23, 125.657, 71.157, 157.714, 51.0575, 10.1749, 122.208, 127.372, 41.9446, 150.871, 14.6794, 192.901, 102.652, 82.8089, 194.024, 30.3293, 122.208, 49.3667, 30.1279, 186.102, 173.095, 95.1384, 54.1276, 7.47093, 85.4946, 33.9427, 70.0705, 48.0544, 96.4995, 91.7875, 141.649, 148.241, 174.328, 24.8054, 53.8957, 190.674, 17.542, 112.29, 49.9649, 130.747, 129.276, 24.5857, 42.7747, 102.029, 166.442, 166.124, 101.791, 196.789, 163.964, 93.3378, 14.6855, 121.384, 33.961, 7.67235, 124.119, 126.823, 140.043, 123.533, 39.3933, 14.1545, 146.422, 111.252, 23.2307, 14.9846, 142.228, 15.0639, 53.4623, 146.941, 40.669, 46.0829, 159.734, 44.0931, 53.2792, 50.2579, 35.255, 62.5019, 39.1186, 51.3321, 123.508, 60.6159, 129.008, 68.8498, 179.943, 54.7807, 24.72, 11.0721, 108.731, 149.705, 173.254, 42.9945, 74.4652, 143.895, 15.9795, 29.2917, 88.8821, 10.1749, 112.918, 52.2904, 72.5852, 51.1185, 141.935, 4.91958, 191.754, 129.533, 172.948, 58.7298, 69.8569, 173.919, 17.1758, 6.79952, 74.398, 158.94, 51.3321, 81.753, 78.6157, 160.906, 116.855, 48.6892, 98.2025, 88.2229, 94.1191, 173.199, 3.02133, 86.8862, 151.659, 64.272, 103.83, 20.9052, 70.3818, 192.34, 14.0019, 27.8268, 42.8053, 82.1619, 186.248, 149.168, 147.258, 180.871, 124.308, 35.8837, 97.0916, 0.878933, 69.7043, 128.855, 64.9251, 137.907, 163.463, 114.359, 181.848, 163.89, 82.7784, 54.5, 111.618, 194.989, 152.8, 192.737, 99.9298, 134.55, 133.311, 34.2906, 98.5992, 191.07, 80.4529, 101.675, 164.782, 71.1631, 83.4559, 165.111, 135.197, 47.5295, 76.3451, 148.015, 175.353, 101.938, 124.662, 107.486, 3.76598, 143.181, 187.201, 89.7488, 191.174, 100.027, 152.684, 60.6342, 134.739, 89.23, 151.531, 111.051, 107.297, 58.4796, 142.552, 137.443, 181.561, 104.343, 174.664, 197.272, 29.3344, 65.2791, 123.612, 139.47, 195.746, 189.581, 103.702, 109.922, 48.439, 78.3288, 154.503, 131.932, 1.83721, 103.421, 2.56355, 161.693, 16.3213, 100.461, 98.1536, 18.7384, 172.46, 136.79, 126.957, 191.449, 120.231, 154.424, 196.796, 117.466, 3.24107, 88.0215, 99.3744, 139.213, 57.9546, 57.8753, 46.9558, 134.349, 87.8872, 37.8613, 84.1212, 21.2043, 12.8422, 198.419, 174.462, 139.305, 93.1364, 124.497, 162.297, 104.794, 28.7851, 108.56, 9.42412, 140.922, 133.775, 118.522, 106.796, 83.5231, 185.223, 40.7361, 61.269, 61.4643, 147.642, 197.961, 157.317, 52.1256, 172.13, 142.308, 93.7162, 111.863, 70.6137, 123.252, 195.331, 81.4783, 65.041, 30.549, 99.765, 166.717, 11.6276, 12.2623, 119.132, 193.664, 100.046, 167.675, 76.7296, 139.299, 27.6559, 115.281, 18.7994, 136.9, 48.7503, 35.786, 1.21464, 156.56, 98.7274, 190.02, 151.219, 160.607, 193.341, 111.942, 4.1261, 134.117, 102.902, 76.6015, 115.061, 190.362, 101.523, 43.0982, 145.659, 151.268, 191.308, 72.5791, 25.7942, 2.90536, 1.57476, 21.1554, 190.307, 25.9651, 163.103, 107.938, 90.6949, 1.17801, 157.305, 157.927, 181.866, 89.8526, 40.492, 120.06, 183.831, 189.276, 43.0677, 60.1276, 18.421, 163.231, 43.7452, 28.7667, 21.0517, 147.447, 118.772, 4.41908, 27.369, 130.076, 166.161, 111.307, 135.032, 7.14133, 4.87075, 112.387, 140.385, 113.047, 149.089, 177.337, 50.2152, 25.9774, 57.3687, 125.517, 68.453, 48.4512, 56.9231, 143.937, 57.8692, 162.731, 142.802, 180.236, 76.8456, 61.2995, 52.1195, 10.6143, 191.04, 52.3515, 13.2694, 182.879, 160.137, 40.2478, 170.165, 34.3028, 199.567, 180.981, 81.4173, 184.71, 17.2002, 12.6347, 94.5952, 104.349, 125.101, 60.8966, 195.428, 156.92, 26.2093, 119.48, 131.999, 117.521, 76.0826, 5.59099, 14.0202, 165.618, 19.6173, 190.448, 187.011, 153.008, 2.74667, 104.691, 181.506, 146.818, 59.1937, 118.9, 137.309, 47.4319, 72.1397, 199.023, 102.689, 81.4844, 10.9928, 36.8603, 8.61232, 169.829, 43.0799, 55.5559, 117.954, 170.495, 120.371, 90.0296, 149.712, 196.661, 41.2, 142.46, 78.2189, 9.62554, 186.987, 12.7689, 7.40379, 86.7946, 133.927, 4.06507, 119.956, 164.611, 139.775, 103.128, 101.578, 56.5447, 105.484, 35.8715, 40.1807, 146.08, 11.9877, 197.821, 0.128178, 154.643, 31.8857, 59.5538, 71.4499, 184.594, 116.178, 47.2121, 94.0336, 37.2143, 55.9526, 97.9522, 56.8743, 5.63372, 19.1656, 90.3836, 172.289, 142.845, 162.304, 150.09, 25.1106, 4.60829, 117.112, 30.0485, 196.954, 67.513, 152.336, 166.155, 165.374, 21.8451, 10.4251, 7.61742, 78.1274, 139.634, 19.2572, 22.4982, 2.38655, 117.588, 78.8537, 178.155, 48.9578, 195.05, 123.087, 96.6704, 171.471, 147.002, 56.4165, 85.403, 40.6384, 152.007, 165.606, 42.3658, 193.463, 135.185, 6.4333, 36.7748, 57.4175, 186.187, 165.429, 164.47, 49.7391, 52.1439, 96.5423, 78.3715, 151.585, 105.679, 32.9295, 194.018, 157.262, 187.829, 173.644, 169.945, 198.468, 126.023, 145.323, 63.2344, 143.498, 127.079, 138.957, 159.929, 71.4499, 45.143, 121.58, 80.5139, 21.4728, 124.68, 122.99, 160.417, 16.9622, 18.4149, 197.491, 105.423, 164.788, 80.3369, 60.5609, 122.196, 29.487, 133.634, 0.634785, 123.698, 82.5465, 113.669, 65.7674, 9.69878, 26.545, 185.992, 125.394, 22.2358, 194.433, 139.225, 7.01926, 176.067, 23.7007, 53.1205, 107.95, 164.141, 5.64592, 41.4624, 82.0032, 138.072, 109.574, 70.0827, 1.25736, 163.634, 81.9788, 49.7818, 192.73, 151.189, 20.8258, 4.34584, 150.108, 61.1591, 24.7261, 113.486, 120.481, 184.539, 117.179, 30.8115, 166.051, 98.3062, 45.7533, 76.8029, 100.845, 95.938, 156.377, 106.79, 102.451, 92.8434, 142.467, 53.8957, 198.718, 6.86056, 69.2282, 147.032, 29.4748, 13.7028, 72.8111, 3.83312, 83.2728, 77.456, 134.587, 198.694, 9.0762, 156.017, 130.302, 105.625, 2.99692, 81.5577, 154.35, 16.3457, 70.0095, 116.037, 118.967, 133.177, 160.674, 181.646, 28.4555, 37.019, 182.83, 166.247, 128.41, 30.3354, 88.3572, 103.885, 129.911, 155.126, 34.8704, 30.2499, 108.768, 85.8669, 22.4311, 113.126, 79.7388, 97.5127, 5.28581, 57.8631, 45.735, 108.451, 78.5669, 187.872, 122.037, 198.462, 140.019, 0.0488296, 135.539, 1.69683, 26.722, 195.3, 98.9349, 174.664, 126.804, 183.953, 159.96, 139.14, 66.158, 66.2618, 25.605, 100.223, 70.1743, 87.8811, 61.33, 139.225, 0.433363, 170.293, 65.2791, 181.689, 66.4632, 116.324, 117.521, 40.3211, 12.4149, 90.585, 104.233, 133.012, 178.808, 174.944, 116.959, 75.7286, 39.2346, 199.011, 126.786, 150.572, 12.1158, 45.5641, 133.604, 48.6648, 147.362, 31.0984, 190.301, 88.1802, 88.1252, 161.199, 101.981, 107.273, 107.529, 61.0736, 0.189215, 32.0566, 20.3497, 181.005, 192.151, 119.596, 169.781, 76.8151, 84.9269, 75.8446, 89.1018, 165.337, 184.716, 48.6343, 38.4533, 154.881, 171.001, 95.6206, 16.5044, 150.713, 45.8266, 127.28, 47.1938, 35.4991, 157.982, 124.863, 141.459, 124.815, 102.206, 33.3628, 133.934, 97.5677, 93.4599, 129.496, 194.678, 1.50761, 16.0894, 64.6382, 15.8879, 133.665, 148.088, 182.842, 73.6534, 195.831, 160.283, 113.761, 164.605, 125.028, 190.564, 40.6568, 17.6946, 198.987, 193.121, 43.3363, 180.902, 16.5777, 162.627, 116.129, 182.91, 153.343, 33.491, 60.8661, 169.884, 91.8973, 54.0605, 54.8479, 17.1697, 27.4483, 35.8409, 93.8017, 115.989, 148.253, 77.7123, 109.842, 35.1817, 191.046, 165.154, 164.483, 57.9241, 71.5598, 44.4533, 116.55, 193.432, 190.002, 171.953, 165.197, 63.6982, 11.4383, 138.224, 67.8365, 65.1997, 121.384, 167.327, 33.1431, 5.62151, 156.163, 90.9879, 88.992, 15.5705, 8.44142, 197.888, 125.443, 135.307, 107.675, 157.781, 12.714, 176.434, 175.573, 20.8075, 194.153, 58.6383, 56.5874, 91.6654, 154.155, 21.4362, 23.3589, 78.8171, 161.217, 110.837, 157.207, 118.76, 85.8425, 141.081, 3.55235, 75.1793, 93.5392, 131.382, 102.475, 113.224, 12.7689, 17.6153, 194.086, 148.698, 177.935, 39.0698, 99.0448, 172.277, 147.325, 0.122074, 74.0379, 48.7381, 96.1211, 6.99484, 42.2193, 191.174, 84.3226, 79.458, 98.6847, 110.202, 199.762, 37.5439, 90.9696, 38.2092, 176.019, 63.2771, 74.4652, 28.547, 85.4274, 137.394, 42.9456, 129.752, 107.053, 151.048, 133.995, 34.7728, 169.622, 92.526, 109.482, 5.18204, 69.5212, 109.006, 111.051, 70.4184, 63.7593, 43.2203, 90.5606, 101.962, 137.931, 45.2162, 115.586, 162.59, 149.187, 59.0533, 149.852, 130.57, 44.203, 184.533, 108.725, 198.92, 152.159, 113.895, 189.691, 49.5499, 152.69, 43.5743, 77.7795, 102.591, 118.87, 60.9027, 26.0811, 40.492, 35.1634, 5.80462, 11.2064, 7.33665, 190.423, 7.55028, 163.726, 83.0226, 45.8205, 96.4629, 73.6534, 32.4534, 177.85, 90.6095, 170.952, 129.82, 96.2676, 157.567, 180.944, 57.7593, 137.095, 51.1124, 54.799, 81.6187, 30.549, 22.956, 175.048, 98.6297, 144.041, 9.43632, 35.963, 162.181, 0.11597, 93.2646, 199.133, 80.9839, 69.4723, 40.2234, 123.814, 93.3317, 38.8623, 110.916, 72.5181, 16.2908, 154.112, 147.435, 91.3907, 69.9423, 38.0749, 135.124, 65.7186, 176.58, 183.599, 39.2956, 50.3983, 134.922, 121.488, 116.916, 75.4051, 69.3869, 63.4846, 103.36, 26.2337, 154.234, 20.2399, 74.1111, 199.42, 12.8361, 194.269, 181.475, 132.951, 191.888, 47.908, 66.3106, 111.777, 110.587, 46.3332, 144.761, 148.271, 84.9269, 75.4784, 169.939, 37.672, 26.838, 135.026, 68.6666, 88.5952, 18.4576, 106.363, 53.1877, 128.465, 161.339, 195.581, 7.9104, 184.008, 100.638, 51.3932, 151.079, 36.6894, 24.3782, 158.165, 171.526, 53.2304, 137.657, 103.824, 40.3943, 91.0428, 11.8168, 122.88, 97.8362, 78.7011, 8.7466, 114.341, 14.7404, 71.4133, 180.035, 134.733, 29.4565, 181.5, 41.2122, 166.039, 59.5111, 183.844, 20.9662, 158.666, 88.3145, 196.381, 185.382, 149.431, 26.899, 35.548, 37.1044, 54.5244, 50.618, 46.7727, 189.636, 137.468, 10.889, 9.92462, 116.471, 152.098, 121.885, 119.797, 175.402, 160.521, 122.269, 41.5418, 107.651, 26.0811, 111.917, 41.2061, 12.1586, 181.365, 63.6189, 191.137, 194.769, 71.102, 9.81475, 50.4654, 92.4345, 70.2109, 104.318, 20.6061, 72.4876, 76.7052, 158.995, 175.689, 106.796, 179.699, 55.5376, 168.041, 121.03, 2.34993, 32.1421, 8.65505, 18.8604, 119.871, 45.2528, 122.684, 181.17, 152.44, 6.2624, 79.8425, 40.2905, 133.207, 93.2402, 199.963, 129.209, 151.268, 137.651, 33.3811, 5.79241, 69.6005, 86.0561, 116.471, 52.0096, 118.07, 136.882, 2.06915, 148.137, 161.376, 152.074, 164.983, 21.9794, 42.0057, 143.852, 182.403, 73.1162, 198.26, 36.433, 189.434, 72.0725, 157.152, 102.744, 9.50957, 134.587, 102.426, 76.1132, 159.929, 170.794, 117.502, 137.04, 132.231, 9.44243, 103.452, 174.859 };
						int n[2048] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8 };
						int an[2048];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 8) {
						float s_mean[4096] = { -997.253, 476.974, 799.615, -109.47, -346.599, -660.817, -227.76, 962.523, 819.697, -593.616, -326.212, 154.637, -90.8536, -636.097, 813.471, -484.359, 751.885, 196.265, 692.129, 319.254, 701.346, -899.045, -771.172, 103, -13.7028, -347.026, 237.648, -42.5733, -687.613, 218.238, -846.37, 65.2181, -286.355, 618.213, -990.051, -510.056, 653.92, -956.481, 664.174, 215.613, 378.521, -781.426, -725.089, -564.867, 516.037, -184.912, -971.618, 90.0601, 929.563, 116.245, 114.963, -340.983, 818.232, -68.9413, -44.4044, -802.789, 69.0634, -241.859, 678.701, 625.355, 858.455, -800.104, 846.492, 268.41, -496.506, -373.455, -568.651, 645.314, 563.89, -909.848, 43.0616, 507.492, -806.879, -511.155, -595.508, -872.005, -259.56, -19.3793, 78.9514, 840.999, 602.161, 647.633, -553.88, -169.836, -676.931, 410.138, 929.075, -831.782, -51.4237, -809.503, -529.527, 105.93, 625.538, 327.616, -740.41, -388.958, -545.579, -759.575, -291.604, 157.384, 590.503, 342.448, -321.574, -236.061, 595.569, 205.42, -42.8785, 284.097, -960.509, 973.876, -15.4118, -55.7573, -648.183, 28.5958, 830.195, 133.335, -76.8761, -98.6663, 418.744, 441.694, -543.138, 438.337, -992.492, -49.8978, 570.788, -785.394, 912.656, 768.853, -351.665, 198.096, 485.641, -173.07, 940.733, 663.564, 339.824, 910.886, 917.844, 356.548, -172.765, 954.1, 870.174, 907.956, -235.084, 575.61, -239.418, -502.548, -588.427, 869.442, -674.734, 768.12, -305.826, 443.464, -573.962, 989.929, 684.378, 708.609, -348.857, 474.227, -674.917, -582.385, -832.636, 237.831, -592.578, 454.756, 821.589, 916.746, -203.284, -126.072, -508.164, -69.4296, 909.421, 188.757, 689.505, 896.298, -560.595, 910.825, -589.587, -707.45, -665.029, -660.878, -763.359, 373.638, -975.097, -400.739, -311.991, -666.616, 895.505, -67.8426, -176.305, -280.374, -983.52, 233.253, 93.6003, 97.8118, -881.527, -158.422, 440.168, 965.453, 818.781, -421.003, 649.831, -84.2616, 98.117, 38.9111, 954.588, -947.874, 27.6193, 125.645, 883.419, 652.15, -255.165, -156.652, 364.238, -918.821, -319.437, -665.029, -925.108, 957.274, -964.599, 171.606, 45.4421, -435.835, -493.576, 891.354, -491.134, -483.566, -736.747, 788.263, -996.582, -327.25, -824.641, -48.2498, -276.345, 300.272, -712.027, -444.624, -877.743, 488.754, -450.911, 395.978, -238.441, -185.583, 785.699, -317.301, 414.96, 254.189, -69.4296, 76.3268, -581.286, 349.651, -501.205, 977.783, 273.415, -378.826, -685.72, 67.7816, -504.624, -768.059, 486.312, -904.477, 718.497, 902.158, -363.872, -81.9422, -954.588, 72.6035, -626.514, 788.934, -350.2, -680.593, -976.745, 897.519, 626.942, 17.7313, -397.992, -841.914, 96.0417, 255.776, -955.565, -751.03, -448.469, 998.901, -319.926, 901.364, -535.325, 503.891, -40.2539, -561.693, -864.925, 982.727, 432.173, -435.774, 792.291, -483.261, -29.4504, -596.362, -52.7665, -779.168, 249.184, 790.277, -5.95111, 657.46, 469.832, -194.006, -938.536, -499.619, 338.481, -429.731, -885.678, 449.263, -708.182, -721.061, -694.693, -606.738, -26.2154, -506.882, -423.75, -174.474, -37.7514, -321.268, 510.056, -493.21, -254.677, -4.85244, -568.102, -620.106, 948.241, -665.7, 761.04, 733.451, 809.565, -389.813, 612.232, 616.077, 908.2, 297.281, -352.702, -499.435, 139.5, -779.962, 177.038, -802.118, -57.8936, -408.185, -487.899, -978.942, -225.501, -696.036, -434.858, 199.438, 841.365, 464.888, 702.322, 542.711, 70.3452, -811.09, -155.614, -645.131, 338.603, -364.116, 818.842, 598.56, 949.217, 299.6, 961.18, 308.634, 357.952, 547.166, -33.5398, 256.691, -850.398, -591.906, 991.089, -134.922, -735.099, 613.514, 3.93689, 370.586, -523.301, -115.39, -611.56, -761.101, -883.419, 472.396, -206.153, 828.608, 51.7289, 946.532, 236.732, -694.998, 911.252, -765.923, 452.681, -195.532, -437.422, 909.177, -501.999, -151.708, -161.473, -268.654, 600.94, -993.042, 513.474, 435.347, 263.466, 813.959, 529.832, -230.323, -387.188, 930.052, 474.96, -214.209, 3.14341, -442.793, 307.474, -622.852, 866.756, 102.817, -665.944, 68.5751, -184.057, 93.5392, 435.652, 54.7197, 994.69, 339.518, 544.298, 489.669, -320.292, 348.491, -630.421, 25.6661, -867.306, -10.3458, 375.408, -397.504, -298.746, -592.151, 6.19526, -513.84, 170.873, -812.25, -836.299, 558.336, -633.778, 767.632, 933.592, -144.993, 2.9603, -896.481, 561.876, 355.937, 789.117, -416.303, -318.583, -976.501, 776.788, -808.039, 788.812, -908.139, -36.5307, -77.5475, -29.3893, 605.884, 143.956, 443.098, -119.907, -789.727, 11.5665, 10.1627, 963.744, -811.274, 510.056, 632.069, 937.62, 752.983, 434.797, -516.648, 514.451, -418.439, 284.646, -430.83, -620.411, 609.424, 363.262, 638.844, 418.012, 66.3778, -869.015, 350.139, 190.71, -233.314, -193.884, -316.63, -338.847, -352.214, -783.502, 605.518, -827.998, 559.618, 901.364, -16.0222, -328.166, -696.768, 462.386, -248.878, 191.137, -79.4397, -834.162, -682.18, -430.525, -767.266, -206.336, 238.014, 431.867, 629.749, 745.415, -877.804, -584.948, 108.982, 389.447, 802.24, -355.51, 564.745, -335.185, -255.348, -539.354, 327.189, 932.615, -284.829, -604.236, -48.8601, -922.422, -307.291, -599.902, -744.621, 859.981, 33.2957, 318.827, 611.866, 516.404, 884.274, -381.634, 484.298, -387.799, 848.201, -636.891, -782.342, -857.418, -102.878, 442, -922.91, -115.879, 536.607, -955.321, 536.973, 996.582, -780.511, -611.255, 819.819, 874.142, 10.4678, 695.853, 685.72, 247.475, 499.619, 418.073, -529.038, -465.255, -33.1126, -284.829, 377.606, 443.525, -204.138, -819.452, 11.9327, -177.709, 138.096, 818.049, 174.047, -238.197, 826.838, 268.349, -103.427, -52.9496, 541.49, -714.164, -266.03, 258.339, 221.229, 464.949, 301.431, -376.812, 729.545, 216.712, -935.972, -882.199, 767.937, -4.73037, -624.195, -681.204, -959.96, -225.196, -231.361, -95.7366, 666.005, -224.403, -508.469, -494.186, -154.088, 441.023, 299.6, -189.856, 774.651, 663.747, 493.149, -89.7549, 805.109, 262.551, -793.39, 907.224, -136.876, 708.792, 947.935, 761.773, 110.813, 102.939, 387.066, -604.175, -139.012, 401.227, -604.419, 44.1603, -258.705, 892.209, 335.673, 855.586, -892.087, -76.9372, -401.227, 744.987, 710.135, 876.705, -604.846, -505.417, -396.283, 397.626, -207.373, 105.747, 315.897, 869.259, -994.263, -599.78, 258.4, -860.286, -65.0349, 871.944, -607.593, -500.961, 955.443, -211.463, 142.064, 905.454, 956.236, 542.833, -441.328, 461.837, 629.566, 254.555, -265.053, -857.601, 9.55229, 621.693, 468.795, -230.567, 261.513, 22.248, 177.526, -760.43, -279.214, -134.373, -938.047, -999.573, 182.104, -805.597, 520.432, -222.694, -561.632, -464.339, 812.738, 549.669, 69.9179, 2.83822, 974.425, -222.51, 483.383, 646.901, 980.59, 878.292, 201.086, 58.1988, 160.314, -46.7849, -600.391, -782.037, -139.683, -236.061, 699.454, -400.006, 359.233, -40.9253, -796.564, -478.133, 31.7698, -449.263, 786.493, 746.818, 762.505, -358.806, 633.961, -664.907, -389.996, 399.213, 31.3425, 745.903, 721.305, 286.843, -597.095, -811.701, -504.746, -463.973, -399.091, -776.482, 997.314, -125.034, -555.345, -384.014, 622.974, 504.685, -490.402, -564.989, -987.854, 325.724, 166.295, 406.537, -786.554, 414.716, -130.467, -485.641, 97.7508, 750.786, -302.53, -147.801, 22.5532, -807.794, 140.538, -275.369, 746.086, 703.116, 140.599, -0.824, -408.979, 829.768, 379.986, -292.337, -140.782, -572.741, 820.612, -102.329, -505.051, 636.158, 492.965, -265.725, -0.396741, -891.049, -464.827, 851.619, -404.645, 486.801, -517.808, -45.7472, 639.637, 125.645, -195.288, -331.095, -711.417, 73.2749, 698.233, -353.862, -588.122, 295.816, 122.166, -88.4732, 756.218, 153.844, -911.069, 467.513, -640.675, 129.551, -796.93, -854.976, 214.637, -197.058, 450.179, -295.206, 184.729, 78.4631, -764.153, 189.184, -834.284, -338.481, 675.222, -476.608, -498.703, -214.698, -868.587, 367.473, -353.496, -813.105, -994.873, 217.444, 82.2474, -665.639, -226.6, 757.317, -521.104, -97.1404, -721.366, -539.842, -889.096, 501.45, -732.047, -559.862, -479.72, 649.586, -817.988, -446.333, -569.018, 3.57067, 194.128, 751.274, 912.961, 833.491, -500.656, 678.884, 796.136, 749.321, 853.45, 529.71, 334.513, -322.367, -637.867, 145.604, -879.818, -911.557, 520.127, -631.397, -752.434, -528.611, -500.168, -357.28, -272.744, 62.4714, 691.092, -134.922, -849.849, 820.551, -510.605, 487.899, 601.55, 653.981, 335.795, -130.161, 825.19, -102.817, -994.69, 877.682, -362.712, -962.889, -853.511, -262.246, -409.162, -310.038, -929.991, -862.423, -110.874, -172.46, 788.141, -422.468, 550.462, -746.086, -696.646, 972.106, 821.711, -667.959, 148.045, 501.755, -157.445, 960.387, -827.021, 714.469, -440.291, 799.127, 586.596, -123.02, -109.287, -252.846, -755.974, 576.891, 138.218, -331.584, -813.593, 573.656, 653.859, 368.511, 429.548, -99.5209, 752.373, 821.162, 3.14341, 215.979, 875.851, 645.009, -760.063, 405.377, -992.615, -170.141, -685.232, 432.295, -664.296, 713.675, -966.43, -559.618, -831.233, 208.533, 872.311, 104.77, 97.9339, 277.932, -733.818, -766.533, -587.939, 662.648, -851.07, 619.373, -238.258, -142.369, 741.447, 141.331, -926.939, 338.786, 590.991, -883.969, 711.905, 39.0942, 860.408, -100.436, -102.268, 75.3502, 867.122, -261.269, 368.023, 724.845, 872.005, 872.86, -707.999, -348.186, 118.686, 885.189, 435.041, 384.014, -877.377, -695.486, -427.839, -960.753, 209.876, 456.954, 152.928, -697.989, 306.558, 567.064, -785.333, -349.956, -22.6142, -890.255, -452.986, 830.805, 715.506, 411.725, 105.93, -411.969, 480.636, -496.994, 677.664, 638.6, -511.093, 218.726, 792.718, 898.495, 251.625, -449.812, 890.255, -40.0098, -135.96, 247.047, -268.166, 817.927, 545.03, -331.523, 65.5843, 846.187, 425.581, 884.823, 540.33, -975.585, 588.244, 29.1452, -130.528, -585.376, 355.998, -101.108, -754.692, 887.57, 831.294, -192.724, -997.131, -900.754, 685.293, -314.92, -465.865, 718.009, 196.814, 431.928, 452.01, -99.6429, -458.724, 518.113, 4.30311, -981.933, -669.912, 835.994, 738.09, -365.398, -600.635, -198.462, -586.657, -268.532, -150.792, 706.29, -832.514, -814.875, 332.682, -681.936, -70.8335, -506.699, -405.255, -266.518, -917.966, -248.512, -738.273, 478.988, 379.62, 879.94, -119.968, 862.423, -891.598, 99.6429, -794.916, -425.336, -497.177, -340.678, 740.959, -458.602, 408.551, -188.147, 528.733, -975.646, -802.973, 40.7422, 186.132, 973.083, -70.5283, 536.851, 196.326, 626.331, -229.286, -75.1061, -553.148, -636.341, -346.538, -760.186, -980.895, 807.428, -807.672, 153.844, 227.088, -826.472, -152.074, -547.655, 906.308, -126.194, 340.739, -238.868, -46.7238, 366.863, 315.409, 522.385, 114.414, -739.738, 254.677, -778.314, -975.219, -338.359, -356.426, 864.315, 497.726, 780.633, -616.749, -298.196, 63.448, -220.069, 455.367, -791.253, 758.293, -122.532, 835.688, 309.244, -836.238, 697.623, 658.742, -146.886, -304.361, 872.555, -368.755, -656.972, -857.051, 894.406, 696.89, -89.1446, -542.894, 216.285, -50.4471, -456.099, 790.948, 604.053, 798.151, 561.998, 787.713, -262.001, 380.169, -656.423, 120.212, -935.789, 225.99, 302.896, 279.885, -103.854, -827.143, 115.635, -384.198, -233.619, 529.099, 196.387, -376.08, -972.106, -512.253, -575.365, -514.267, -294.046, 791.253, -878.353, 857.784, 599.414, 644.948, 276.894, 910.398, 632.313, 881.405, -975.585, -383.038, 455.611, -625.416, 369.854, -707.083, 298.624, 703.665, 166.601, 384.747, 70.5283, -932.798, -657.46, -490.951, -287.942, -552.721, 376.69, -258.095, -203.04, -976.562, 25.4219, -842.647, 383.648, 836.848, 299.478, -382.977, 116.672, -295.938, -309.854, -191.076, -421.613, 381.695, 646.229, 536.363, 890.378, -20.1117, -700.003, 257.424, -434.919, 626.698, -555.467, 873.653, 607.532, 382, 759.636, -230.689, 866.573, 401.715, -711.478, 100.375, -363.323, 351.665, 164.892, -666.311, -490.524, 71.8101, 588.733, 35.7982, -524.827, -509.751, -946.715, 912.29, 274.392, 787.286, 821.833, 96.6521, -0.701926, 809.32, -346.355, 689.444, -54.2314, -266.701, -218.787, 547.411, 714.286, -19.5013, 136.265, 322.245, 561.998, -619.495, 826.594, -249.855, 4.12, 585.681, -137.425, 190.405, 665.456, 734.611, 133.946, -585.253, 381.939, -633.229, 635.853, 646.29, 552.293, 586.413, -54.1704, -151.463, -260.475, -523.85, -49.6536, -735.221, -477.462, 761.834, -178.869, -793.329, 597.766, -274.087, 242.042, 93.7223, -869.625, -627.979, -114.78, 741.997, -643.666, -161.29, 138.096, 903.378, 433.638, 63.448, 79.6838, 54.6587, -456.099, -303.018, -292.764, 436.689, -755.913, -732.658, -587.268, 342.936, -932.005, 593.738, -290.445, -340.739, -622.425, -549.547, -791.131, -347.575, -534.959, 481.674, 846.492, -990.539, -96.2859, 364.605, 359.05, 126.621, -849.666, -775.933, -989.135, -351.726, 396.039, 296.548, 570.544, -743.583, 668.264, -155.919, 537.156, 709.281, 181.982, -681.509, -382.916, -869.442, -58.1988, -224.769, -549.181, 548.631, -407.086, 537.645, 588.977, -314.615, -476.913, 110.935, 719.169, -4.60829, -785.943, -653.615, -857.479, -45.1369, -156.835, 855.098, -499.496, -398.968, -865.291, 672.292, 750.969, -679.312, 461.837, 171.422, 394.94, 889.828, 197.851, 228.004, -46.5407, 762.2, -682.119, -366.008, 693.716, -391.339, -749.565, -238.502, -394.147, 106.235, -20.9662, -522.935, -370.525, 977.111, 393.475, 305.704, 130.345, -722.465, 891.842, -155.065, -373.455, 937.315, 231.666, -571.154, 765.496, -451.643, 631.825, 42.1461, -449.751, -595.874, -876.278, 484.298, 816.34, 788.629, -846.675, -216.651, -42.2681, -484.848, 432.356, -139.622, 904.172, 715.873, -960.204, 443.342, 347.209, 880.49, -226.051, 230.934, 216.895, 145.421, 753.41, 307.047, 408.429, 832.942, 268.96, -195.41, -241.31, 240.15, -114.841, -180.334, -727.409, -322.001, -208.228, -424.543, -399.823, -239.418, 405.255, 836.482, 762.383, -56.1235, 220.557, 572.558, 128.391, 314.982, -87.4966, -127.049, 641.652, -368.755, 687.49, -889.828, -703.299, -390.79, 884.884, -645.436, -134.251, -536.363, 828.608, 37.8124, -124.973, 696.829, -71.0166, 867.244, -168.432, 137.181, 33.9061, -10.1016, 699.637, -771.111, -969.481, 406.781, -372.784, -429.426, 697.134, 636.341, -76.9982, 704.398, 804.193, -370.769, -717.582, 155.248, -533.86, -68.0868, 264.382, -280.251, -256.752, -899.167, -981.994, 5.34074, 492.66, -633.29, 259.133, -780.877, -529.771, -451.949, 496.628, 207.495, 682.119, -784.112, -490.89, -255.654, -13.7028, -892.636, 71.8711, 71.0776, 944.151, -210.059, -67.3544, 684.927, 921.812, 575.488, 39.0332, 378.277, -558.031, 684.317, -985.595, -472.152, 785.638, 659.23, -115.146, 633.656, 843.135, 29.6335, -944.578, -892.026, 215.918, -774.468, -462.752, 314.615, -431.44, -258.278, 423.322, 857.784, -770.44, 551.744, 615.711, 121.189, -956.481, 771.722, 801.874, 57.2832, 472.396, 574.999, -318.949, -773.98, 947.081, 355.632, 441.389, -915.769, 469.039, 142.796, -514.389, 524.461, 462.02, -735.221, -591.418, 664.113, 873.226, 262.917, 223.06, -115.574, -474.837, -944.334, 836.665, -880.184, -282.632, -647.084, 778.375, -603.381, 77.4255, -680.105, 175.024, -377.728, 663.015, 491.012, -21.9428, 624.378, -686.758, 934.202, 439.741, -560.289, 250.465, 665.029, -160.009, -146.519, 931.394, -689.505, -537.034, -811.823, -851.07, 23.4077, 823.542, -657.338, 384.075, -354.045, -856.38, 81.3929, -593.31, -875.546, -920.713, 573.229, 127.781, 172.765, 696.829, -819.33, -299.295, 186.804, -300.577, -501.022, 872.799, -591.723, -95.9807, 645.07, 586.962, 339.152, -32.5632, -203.406, 833.247, -594.897, 363.262, 981.14, -249.672, 870.357, 608.264, 115.207, 977.233, -999.084, 738.945, 293.252, -745.415, 479.476, -961.913, -673.269, 276.711, -931.394, 11.6886, -466.659, 792.535, 525.681, 614.856, -224.219, 957.579, -233.802, 728.874, -248.939, 935.545, 278.359, -112.461, 529.466, -250.343, -213.172, 253.945, -656.362, 384.32, 167.028, -153.294, 627.735, -863.399, -391.156, 16.9378, -693.899, -781.915, -968.749, -825.678, 332.987, 606.128, 745.781, 764.275, 480.941, 145.421, -629.078, -506.272, 380.413, -100.314, 996.216, -191.321, 808.466, -246.742, 596.057, 619.739, -772.088, 262.673, -306.07, 279.458, 576.586, -242.103, 276.65, -191.565, 305.948, -305.582, 958.068, 372.234, -329.264, 254.311, -606.922, -992.553, -732.353, 198.157, 942.503, 598.926, 824.335, 966.491, -414.167, 105.747, -761.223, -968.932, -913.45, -961.974, 868.16, 497.177, 405.438, 183.63, 67.7816, -45.7472, -635.243, 584.46, -19.3793, -421.857, 562.609, 763.054, -424.177, 482.528, -463.363, 199.316, 938.108, 320.17, -821.65, -942.991, 806.635, -865.169, 396.954, 160.314, 364.299, -537.278, -966.369, -779.778, -723.441, 683.95, -974.425, -688.528, -19.2572, -375.774, -180.212, -401.288, 224.83, -593.554, 37.5683, 313.395, -143.406, 552.171, -611.866, -20.1117, -136.998, 333.048, 833.125, 892.758, -341.288, 372.784, -999.817, 842.586, 625.599, 498.154, 847.591, -907.102, -98.4832, 841.182, 577.746, 606.922, 419.599, 190.161, 801.508, 523.606, -802.85, 591.601, -47.5173, 884.64, -125.278, 201.392, -49.4095, -389.203, -494.675, 386.883, 116.489, -271.34, -406.659, -921.995, -664.418, 847.102, -616.443, 369.06, 472.03, 883.602, -599.597, 370.159, -967.467, 41.2915, -763.115, -691.153, 81.2098, -964.843, -406.354, -118.137, -597.339, -431.196, 568.102, -143.895, 916.684, -939.268, 193.396, 921.018, 342.143, -378.765, -772.454, -210.669, -889.035, -721.122, 192.602, -450.606, 532.456, -543.199, 663.381, -733.879, 206.946, 699.698, 806.879, -167.15, 690.298, 650.014, -828.852, 72.9087, 407.514, 274.697, 724.54, -909.055, 817.316, 341.716, -855.647, 874.996, -174.291, 146.641, -643.483, -852.718, -549.486, 489.303, 324.076, 466.659, -265.114, 900.021, 86.52, -420.698, 911.985, -883.541, -951.17, -979.003, -181.616, 810.358, -479.354, 968.322, 558.885, -436.506, -494.919, -659.841, -580.187, -905.21, 738.029, 149.632, 33.723, -263.222, 976.867, 105.136, 792.962, -838.069, 540.025, -537.645, 513.962, -640.614, -904.355, -874.325, 351.482, -138.401, 700.919, -0.274667, -37.6293, -779.351, -909.116, 590.747, -80.3552, 735.16, -782.342, 858.577, 376.751, 426.374, -960.509, 317.85, -18.4637, -734.794, -440.229, 389.813, -959.838, -351.36, -883.908, -996.338, 931.516, 126.072, -80.5383, -551.744, -229.225, 376.141, 843.074, -526.353, 324.076, 433.21, 461.592, 642.689, 171.972, 163.793, 340.19, -67.6595, 123.875, 993.53, 573.656, -414.411, 882.138, -116.916, -424.848, -787.164, -44.2213, -444.624, 115.757, 143.529, 825.007, 971.801, -386.883, 27.5582, 354.778, 58.6261, 670.827, -313.028, 459.334, 684.072, -132.908, 217.566, 15.9001, -115.696, 794.305, 340.19, -971.007, -659.536, -652.455, 2.77718, -955.565, -894.955, -735.71, 231.483, -186.682, -421.735, -87.1914, -767.693, 205.969, -265.175, 331.645, 742.912, -623.646, -399.335, -481.429, 633.9, 176, 979.125, 969.787, -239.479, 817.743, 427.595, -531.48, -935.728, 790.277, 484.481, 124.79, 924.375, -978.149, -854.732, -336.161, 759.209, 294.656, -410.688, -34.5164, 878.231, -91.464, 156.346, -146.764, 583.178, 595.386, 573.168, -866.817, 519.883, -317.301, 429.914, 883.358, -720.389, 508.469, -92.9899, -7.84326, 251.015, 84.3837, 751.396, -51.7899, 721.061, -989.624, -496.75, -121.067, -772.881, -361.248, 972.655, -160.314, -841.426, 487.777, -34.2112, 352.947, 607.898, 906.186, -738.09, 197.302, 472.518, -12.8483, -160.192, 446.333, 806.513, -267.8, -645.985, 758.049, -578.112, 465.499, 861.934, -86.3369, 740.349, 497.543, -200.11, 606.8, 894.772, 39.7046, -789.85, 482.406, -919.431, -632.069, -782.891, -574.023, -842.097, -763.054, 754.082, -897.519, -497.604, -455.55, -460.433, 745.659, -892.697, 244.728, 304.361, -803.339, -65.2181, 513.169, -732.475, -806.452, -482.406, 160.253, 863.399, -89.816, -189.367, -704.581, 717.765, -254.86, 292.459, 241.371, 874.752, -638.966, -683.218, -302.286, -726.737, 973.51, -285.684, -844.356, -526.475, -110.08, 644.032, -438.398, 633.717, 299.173, -305.338, 231.3, -263.71, -689.932, -763.176, 13.8859, 64.9739, -759.331, -976.135, 706.168, -69.1244, 157.445, -956.114, 716.483, -350.688, 664.052, 621.204, 978.698, 766.533, 940.672, -232.276, -883.602, -464.4, 32.9295, 784.783, -142.918, 643.91, -609.058, 114.536, -303.995, -811.09, 658.681, 441.633, -189.856, 905.271, -287.698, -955.382, -777.398, 640.858, 950.255, 397.748, 216.468, -916.074, -168.737, -977.783, -1.25126, -260.72, -335.734, 588.061, 614.917, 154.942, 647.023, -734.611, -887.814, -601.306, -844.905, -673.757, -45.4421, -215.369, -718.558, 996.094, -314.188, 849.788, 624.805, 555.162, -155.431, -419.355, 329.203, 690.237, 127.598, 889.767, -627.186, -903.806, -195.654, 342.143, -892.697, -672.475, -619.312, -867.855, 46.6018, -100.009, 513.352, -690.359, 905.881, -358.135, 173.559, -743.034, -109.104, -704.093, -938.78, -584.826, 237.159, -629.322, 269.509, 260.72, -1.55644, 242.286, 197.913, 532.09, 86.4589, 505.6, -282.754, -293.863, 559.435, -423.75, -416.791, 725.028, -838.496, 541.978, 100.62, 972.411, 111.728, -735.954, -962.706, -499.802, -857.051, -270.852, -211.463, 505.112, 791.009, -349.345, -209.449, -944.823, -66.1946, 320.414, -814.875, -933.653, -419.66, 710.074, -266.64, -470.992, 909.909, -177.16, -20.2948, 984.619, -439.314, -705.008, 666.311, 252.052, 169.591, 247.536, 396.649, -595.447, -839.412, 387.371, -449.934, 996.033, -215.857, -640.675, 445.54, -462.447, -593.86, 347.636, -244.301, -775.872, -952.818, -72.7866, -828.852, -88.7783, -828.73, 165.563, 314.249, -658.925, -274.758, 973.998, -96.6521, -380.413, 427.534, 709.464, -833.186, -57.2222, -692.679, -973.876, -492.843, -426.801, -506.149, -23.2246, 414.96, -933.348, 255.165, -635.609, -367.168, -251.198, 287.271, 343.852, -429.975, -558.458, -746.879, -726.798, -34.2112, 439.802, -427.9, -184.24, 803.827, -262.246, 458.357, -709.037, -523.606, -494.247, 210.547, -723.38, 198.157, -183.264, 750.481, 460.005, -329.569, -377.178, 412.397, -260.537, 995.117, -18.2806, 981.872, -423.627, -161.29, -466.536, 984.985, -403.79, 846.736, 227.027, -772.576, 440.413, 307.291, -703.421, 583.544, -132.298, -934.629, -511.46, 35.0047, -894.101, -312.845, -462.447, 960.692, -546.495, -126.438, 893.551, -325.175, -869.015, 124.729, 697.989, 216.895, 382.427, -346.11, -989.38, -610.096, 827.387, 551.5, 286.05, 50.1419, -938.108, 199.133, -900.021, 645.863, 510.788, -66.0726, 126.255, -993.713, -471.603, -37.7514, 363.018, 380.963, -249.123, 523.911, -390.484, -271.401, 697.256, -693.716, -807.428, 256.203, 621.265, -891.659, 943.236, 208.167, -387.31, -245.399, 423.017, -94.821, 949.278, 259.316, 401.044, -966.43, 697.928, -759.514, 580.065, 184.118, -433.638, -415.937, -426.801, 641.285, -995.056, -642.384, -869.32, 422.04, -442.183, 974.975, 616.199, 565.172, -535.264, 609.668, -266.518, -329.63, -487.289, -140.416, -496.994, 687.002, 137.303, -339.518, -511.643, -46.6018, 774.224, 450.606, -726.981, 486.007, -538.804, 584.216, 186.682, -481.918, 689.322, 995.972, -718.009, 289.163, -495.712, 114.902, -438.948, 83.8954, -141.209, -971.496, 452.132, 521.165, -786.859, 896.97, 535.752, 93.0509, -612.476, 616.688, 521.958, 954.222, -336.772, 734.916, 476.241, -39.5825, 134.251, -386.456, -703.543, -41.7798, -691.092, -156.652, 439.497, -676.504, -38.5449, 907.834, -102.817, -249.184, -766.472, 97.2015, 423.75, 292.459, 985.839, -327.067, 11.8107, -352.702, 399.396, 281.411, 618.946, 700.125, 827.692, 617.908, -855.525, -689.505, 897.58, -658.986, 656.117, -648.061, -138.707, -763.482, -134.007, 1.00711, -801.996, -368.633, 857.906, -723.869, 465.072, -942.076, -936.705, -173.009, -600.269, 167.943, -511.765, 973.449, 270.974, 896.542, -810.236, 994.446, 480.575, -123.692, 2.53304, -332.743, -584.887, 101.535, -484.725, -943.297, -452.742, 842.219, 297.586, 553.819, -212.5, -192.297, -623.89, 165.136, -914.487, -438.52, -38.4838, 51.7899, 281.961, 184.729, -575.182, 318.522, 209.815, 190.344, -475.997, -947.447, 46.2355, -312.601, 880.612, 613.269, -86.3369, -205.054, -293.924, 170.385, -268.044, 305.948, 778.802, 493.637, 319.498, -398.358, 670.156, 991.821, 744.865, 815.485, -778.741, -998.596, -71.5049, 247.047, -730.094, -673.818, 494.247, 26.3375, 626.392, 26.0323, 870.907, -972.289, 300.699, 455.794, -356.365, 34.0281, 978.82, -445.357, 155.797, -849.422, -250.832, 385.113, -639.76, -530.686, -920.53, 922.056, 921.201, 982.788, 43.3058, -908.444, 565.355, 748.039, -203.894, 60.5792, -316.08, -698.843, -697.501, -224.769, -816.034, -678.945, -687.551, 750.481, -410.077, 543.321, 587.756, 108.737, -604.785, 924.68, 378.887, -130.65, -336.161, 56.6118, -848.811, -512.68, 882.015, 830.073, -437.971, 642.689, 59.3585, -651.357, -313.395, 279.092, 81.0877, -906.369, 495.102, -106.54, -691.092, -662.709, -421.064, -925.413, -566.82, -192.785, -872.494, -707.511, -425.947, -399.579, 154.759, -62.7766, 633.106, 347.453, 975.402, 608.142, -105.014, -373.821, -68.1478, -347.209, -120.823, 626.27, 370.83, 956.359, -558.885, 62.7766, -130.406, 980.102, -876.034, 340.739, -287.271, 50.8133, 760.491, -31.3425, 618.702, -99.8871, -676.077, 223.121, 716.239, -547.044, -969.848, 62.4104, -228.858, 867.061, -310.831, -709.22, -337.199, -178.259, -693.838, -27.3751, -889.462, 57.3443, 933.897, 96.3469, 650.502, 163.793, -226.6, -511.643, -138.157, 620.228, 526.231, -22.5532, 843.684, -916.562, 995.605, -191.443, -58.7481, -72.4204, -61.861, 306.986, -277.017, 32.6853, -834.59, 792.413, -793.451, 824.03, -104.465, -257.057, 335.795, 561.083, -645.497, 209.265, 706.961, 876.644, -371.868, -521.47, 545.579, -434.126, 763.359, 988.647, -67.1712, -374.615, 119.785, 492.111, 282.266, 542.161, -28.6569, 470.199, 66.2557, -167.272, -642.079, 797.113, 792.413, 832.392, 186.804, -346.232, -769.524, 733.879, -415.815, 692.19, -111.24, 876.827, 496.994, 449.324, -259.621, -713.492, 543.992, 11.1393, -799.554, -536.668, -882.138, 926.939, -647.938, -578.661, 736.625, -627.064, -576.83, 977.66, 644.765, 566.942, 513.169, -708.67, 665.334, -568.285, -431.013, 652.821, -698.721, -490.219, -387.494, 522.385, 87.2524, 126.743, 216.285, -180.273, 190.527, -193.945, 230.567, -537.217, -736.625, -67.5375, 759.88, -843.074, 544.176, 666.494, 389.203, 433.882, 208.838, -891.659, 661, -59.2975, 972.839, -288.003, -198.523, 979.247, -57.7105, 949.278, 697.317, -705.252, 716.056, 223.731, -877.987, -884.701, -263.405, -965.026, 212.867, -559.374, 892.392, 392.804, -803.095, 581.286, 368.511, 480.697, 368.206, 947.569, -250.893, 570.605, 313.578, 88.7173, -631.581, -813.471, -609.973, -17.8533, -87.4355, 518.784, -549.242, 572.192, 270.974, -912.168, -178.198, -440.962, -362.041, 658.864, -471.297, 438.581, -135.533, -594.836, -226.905, 790.399, 59.3585, -677.236, 659.108, 817.621, -558.824, -105.808, 170.019, 135.777, 439.741, 2.59407, -605.152, 811.762, 47.4563, 95.7976, 5.03555, 652.821, -578.967, 600.391, -19.4403, 745.903, -266.701, 585.864, -412.946, -753.533, 274.148, 678.518, -397.992, -582.263, 802.728, 225.562, -208.533, -80.3552, 148.656, 709.403, -363.567, -652.333, -936.399, -588.733, -690.176, -977.477, -676.809, -674.306, 902.585, 28.6569, -922.117, -675.649, -99.5819, 503.403, 455.367, 570.238, 910.459, -823.786, -588.672, -818.537, -362.346, -842.219, -168.432, -854.122, -888.546, -21.2104, -116.55, -515.549, 162.511, -120.762, 617.176, -334.208, 148.656, 347.27, 817.804, -729.911, -67.7206, 908.628, 106.418, -96.7742, -893.796, 165.258, 291.91, -639.393, -529.283, -915.342, 972.472, 41.5357, 759.88, 539.048, -612.903, -386.883, 618.091, -906.247, 517.38, -75.6554, 826.716, -952.574, -561.388, 170.08, -219.275, 719.962, -77.9138, 305.338, -784.783, 992.187, -714.286, 709.098, -818.232, 94.76, -503.525, 98.4222, -724.662, 647.389, 521.409, -307.291, 110.569, 947.569, -70.6503, -372.356, -951.659, 509.934, 742.668, -381.024, 172.826, -144.749, -461.531, -122.898, 517.136, 549.425, 800.47, 270.791, 540.757, -507.431, -479.72, -258.705, 855.647, -541.978, 49.4705, -33.6009, -797.113, -118.32, -799.615, -725.394, -320.536, -99.3377, -401.593, 809.137, 214.331, -357.402, -716.849, 941.771, 295.572, -235.206, -439.985, -62.6545, 946.471, -757.073, -898.068, 867.55, 952.33, -589.465, 300.76, 774.895, -663.015, 247.841, -131.077, 86.9472, 89.4498, 920.835, -309.061, 280.374, -859.859, 570.299, -74.8009, -204.993, -411.908, -648.976, 355.083, 197.913, -823.359, -279.214, -9.12503, 597.217, 20.9052, -20.6, 921.262, 306.497, 583.483, 57.6495, 414.838, -241.188, -98.2391, -443.403, -728.385, -329.508, 64.0584, 36.6527, 424.909, 455.55, 936.155, -443.831, -207.617, -110.691, -861.873, -538.743, -622.12, -976.989, -729.24, -575.793, 623.341, 347.453, -607.898, 732.902, 9.49126, 207.251, 743.95, -170.507, 908.383, -535.264, -271.523, 100.131, -315.836, 414.289, 135.105, -86.581, 45.1979, -53.9262, -383.587, 909.665, -354.595, 960.387, 415.387, 540.025, 935.667, 31.4646, 341.35, 855.525, 265.603, -678.213, -2.89926, -453.963, -764.824, 746.086, 255.104, -770.501, 296.06, 385.174, -15.8391, 990.967, 603.32, -849.666, 25.9713, 171.239, 116.916, -302.774, -578.234, 406.659, -46.6018, -410.199, -111.301, -82.3695, 987.365, 714.713, 546.556, 47.9446, 702.872, 945.128, 894.284, 53.6821, 295.267, 54.6587, 36.5307, 142.186, 22.7363, -118.015, 160.253, 761.65, 807.672, 744.316, 261.208, 139.378, 88.229, 101.596, 120.518, 71.6269, -79.8059, -529.16, 531.358, -37.8124, -671.682, -410.016, -224.647, -888.546, 907.834, -168.126, 349.284, -744.133, -324.015, -131.504, -58.504, -643.483, 173.925, -415.082, 658.62, 638.539, -182.043, -756.462, 502.06, -120.09, 501.205, -102.145, -905.576, 950.743, 445.112, 56.7949, -343.486, 602.222, 301.065, 598.56, -766.717, 505.783, -498.093, -65.096, -829.096, -458.113, 863.826, -574.877, -156.957, 339.579, -562.914, -960.082, -372.539, 65.0349, -391.339, -724.174, 132.908, -539.171, 452.986, -681.082, -324.992, 135.166, -666.189, -540.88, -996.46, 100.742, -244.057, -245.888, -721.366, -436.262, -533.616, 943.541, 297.281, 963.195, 806.33, -387.127, 969.97, -740.287, -687.674, -392.254, 47.3952, 474.837, 568.224, -302.225, -903.195, 479.781, -693.594, -509.262, -271.157, 34.8216, 759.758, -403.485, -926.267, 78.2189, 262.612, 41.4136, -491.928, -734.916, -897.153, 932.005, 631.397, -243.08, 623.402, -630.909, -507.309, -739.799, 573.473, 612.171, -933.042, -684.378, -289.529, -506.943, 981.872, -162.694, 651.418, 80.2332, 494.064, -98.3612, 974.364, -407.208, -283.914, 786.37, 570.299, 564.562, -907.346, -740.287, -755.608, -741.997, -872.616, -880.306, 567.858, 328.593, 359.233, 982.665, -21.0273, -261.513, -410.871, 322.672, -864.559, -79.0124, 356.548, -239.479, 515.305, -805.78, -334.025, -397.076, 268.654, -574.45, 963.744, 631.397, -680.593, -54.5976, -635.853, -858.882, 716.727, -288.675, -873.592, 230.628, -763.604, -277.81, -618.824, -659.719, -767.144, -317.728, 106.784, 41.5967, 740.287, 915.098, 356.12, 76.2658, -175.756, -472.274, 733.146, -354.9, -816.279, -175.329, 119.297, -52.5834, -998.047, -27.8634, 650.319, -30.2438, 48.616, -88.229, 942.503, -475.021, -969.115, -474.96, -430.464, 541.612, 195.593, -952.818, 788.934, 602.649, -820.002, -268.777, 518.54, 734.611, -885.861, 762.139, 563.219, -553.209, 971.984, 420.576, -908.75, -979.797, 172.887, -502.365, -638.722, 46.6018, -566.576, 958.19, 698.355, 217.566, -130.955, -144.81, -279.336, -636.341, 345.073, -505.905, -183.569, 743.522, -921.079, -153.661, 870.113, 582.568, -508.713, 213.843, -761.04, -137.547, 307.84, 142.796, -20.1727, 360.149, -89.4498, 587.878, -674.429, 936.583, 939.756, -673.696, -588.488, 76.1437, -854.915, 621.937, -747.673, 675.649, 487.716, -861.019, -31.7698, -916.99, 491.195, 106.723, 261.757, 387.066, -156.407, -267.067, 144.322, -44.2824, 248.817, -131.687, 912.717, -774.102, 528.245, 920.53, -624.317, -258.095, 972.167, -982.116, 330.546, -555.04, -58.6261, -575.915, -710.44, -788.079, 718.986, 201.697, -543.138, -355.327, 611.621, 126.438, 954.711, 108.798, 301.187, 851.192, 299.966, 23.0415, -553.575, -679.8, 131.077, 398.053, 534.532, 433.027, -815.79, -374.248, 982.421, -41.5357, 520.188, -481.918, -511.399, -301.126, 0.640889, -403.79, 634.816, 904.233, -985.107, -370.281, -262.978, -64.1804, -543.504, 425.52, -52.2172, 180.761, 949.583, 866.756, -392.804, 215.796, 325.602, -797.784, -155.37, 221.473, -650.136, 585.314, 430.525, 646.107, 698.111, 862.789, -122.593, -880.062, 182.836, 614.307, -207.862, -380.84, -769.585, -558.885, -522.996, -120.09, -392.926, 505.356, 781.426, -937.559, 395.734, -681.143, -796.686, -334.758, -777.581, 782.708, -758.782, -311.197, -266.762, 475.448, -4.79141, -950.072, -679.739, 193.152, -981.323, 245.949, -901.242, -262.246, 420.27, -731.742, 133.274, 956.359, -785.333, 767.022, -85.4823, 30.3659, -74.9229, -535.447, -914.426, 14.4353, -566.027, -179.968, 419.05, -374.371, 85.2382, -56.7339, 42.8175, 282.327, -964.721, -241.493, 917.356, -606.555, -144.932, -580.981, 561.815, -102.512, 381.268, -29.2062, -969.176, 51.4237, 817.988, 266.762, -859.066, -529.954, 88.3511, 130.65, 947.386, -585.192, 436.079, -228.675, 470.077, -667.104, 276.955, -553.209, 671.01, -353.496, -699.515, -622.059, 680.227, 791.559, -178.503, -86.8252, -727.531, 507.065, 381.39, -58.0767, 594.043, -625.965, -399.884, -441.023, 138.829, 604.663, 277.566, 978.637, 975.524, 81.2098, -254.982, -301.614, 223.487, 645.253, 466.964, -86.3979, -220.069, 636.525, 892.27, 180.944, -703.848, 682.302, 556.261, -667.287, -595.813, 736.076, -568.285, 641.163, -806.757, -633.045, -468.917, 231.91, -63.5701, -701.407, 757.378, -251.32, -932.737, -801.813, -993.896, -732.902, 406.293, -284.402, 516.892, 458.602, 637.44, -227.638, 474.776, 4.66933, 34.7606, -898.984, -138.035, -739.189, -560.961, -966.308, 559.374, 170.873, -515.793, -458.296, 698.172, -569.384, 74.0684, 524.094, 261.269, 902.28, 108.554, -653.005, 935.057, 278.298, -539.293, 632.801, 210.669, -4.42518, -797.052, 298.929, -264.138, 399.457, 83.7733, -152.928, -83.4681, -842.341, -745.964, 902.219, 768.059, 924.741, -755.852, 1.67852, -983.032, -744.499, -810.114, -365.764, -547.227, 769.951, 309.549, -470.992, 528.977, -433.699, -370.037, 194.922, 215.125, -113.987, -36.2865, 709.281, 98.178, -972.716, 135.044, 757.073, -265.969, 509.262, -543.016, 920.103, -750.664, -513.962, 457.442, -784.173, 192.602, 410.871, -143.895, -113.132, 636.158, 950.133, -209.204, 439.375, 527.573, 147.313, -705.863, 932.737, -708.853, 837.336, 20.1727, -481.796, -320.353, 310.221, 220.679, 228.37, -124.302, -774.468, 972.106, -984.313, -156.529, 776.36, 651.662, 42.1461, -69.7958, 103, -335.673, 217.75, -282.388, -42.3902, -580.737, -9.91852, -876.156, 617.847, -443.709, 383.038, -245.399, 51.1185, -577.563, 285.928, 504.929, 328.532, -201.331, -7.29392, -589.343, 289.102, 876.278, 990.905, -249.672, 453.597, 483.505, 0.701926, 396.161, 188.879, 562.243, -735.954, 416.12, -878.231, -105.686, -916.318, -74.8009, 803.156, -921.69, 612.171, -812.86, 621.998, -243.934, 890.866, -34.0892, -30.488, 89.0225, 139.317, -872.799, 653.249, 485.031, -705.985, 490.219, -439.131, -425.092, -202.368, 929.014, -425.275, 438.948, -362.163, 724.113, 735.282, -916.684, -363.323, 164.098, -344.218, -833.064, -10.5289, 397.32, -814.203, 977.783, -946.287, 533.067, 325.907, 659.597, 388.714, 993.53, 390.973, -311.747, 43.7941, -884.884, -575.243, 598.804, 98.117, -999.695, 864.071, 961.241, 449.385, -381.207, 452.193, -392.132, 852.901, -840.693, -596.24, -890.561, -789.3, -368.755, -181.86, 501.694, 110.752, -284.097, 343.974, 262.123, -384.259, -131.443, 92.8068, -463.668, 302.347, -330.607, 803.583, -385.174, 550.096, -574.267, -743.645, -340.617, 80.6604, 23.0415, -63.3259, 679.739, -253.151, -973.205, -50.6912, -240.15, -428.449, -966.063, 182.592, 535.142, -963.073, 274.575, 722.282, -81.8812, -376.507, -536.79, 515.732, -924.131, -401.105, 900.754, -714.042, 655.934, -942.503, -174.291, -304.361, 263.283, -791.07, 16.0222, -238.685, 186.499, -82.5526, -359.6, -482.833, -625.111, -509.14, -640.065, -70.6503, -335.551, 719.718, 679.983, 60.0909, 254.86, -833.857, 551.073, 481.368, 435.774, -296.67, 221.107, 939.512, 563.585, -695.975, 336.65, 268.715, -712.149, 80.0501, 294.412, 555.589, -972.045, 571.276, -157.018, -447.127, -300.394, -715.69, 934.385, -656.24, 691.336, 482.284, -174.657, -744.377, -908.872, 223.121, -570.666, -975.524, 163.244, 649.22, -82.2474, 208.777, 74.1905, 526.17, -428.327, -146.519, 869.869, 31.8308, -116.733, 404.34, -298.318, -204.26, 907.529, -128.452, 303.629, 187.658, 863.765, 244.423, 718.192, 856.99, -12.9704, 154.759, -726.554, 521.714, 629.994, -208.594, 809.503, -992.492, -385.846, 64.1194, -845.271, -463.24, 831.782, 17.7313, -147.191, -732.414, 96.9573, 496.017, -860.958, 83.1629, 247.658, 56.0015, -833.125, -369.671, 497.116, -868.099, -515.366, 609.058, 35.2489, 449.141, 761.834, 311.258, -119.602, -311.625, 765.984, 410.077, 980.956, 399.884, 588.977, 998.779, 566.515, -832.82, -876.095, 879.696, 750.053, -978.515, -208.716, 103.671, -621.082, -894.65, -484.115, 758.782, -863.155, 587.146, -46.9069, 9.61333, 895.199, 824.885, 965.636, -613.88, -685.476, -219.703, 350.017, 322.977, 696.524, 683.035, -186.804, -51.8509, -914.914, 944.517, -800.836, -877.499, 618.336, -677.908, 965.331, 728.69, 129.612, -666.616, 909.116, 209.937, -0.335704, 919.858, -855.22, 359.478, -742.18, 361.003, -670.278, 359.416, 933.775, 438.765, 141.087, -930.235, -341.35, 851.253, -997.742, 567.797, -482.589, 876.888, 59.9078, -63.3869, 149.693, 288.369, 409.65, -695.364, -898.373, 517.075, 573.473, 134.617, 427.534, 66.6829, -774.224, -237.709, -458.785, 177.648, 170.324, 186.01, 317.728, 740.776, -433.638, -894.65, 444.868, 461.837, -594.531, -789.3, -975.402, -724.784, -761.773, -278.42, 425.153, -991.821, 150.426, 176.366, 381.634, 294.9, 989.868, 470.992, 348.552, -259.682, 842.891, -851.802, 100.864, -665.334, -239.174, -63.5701, 585.62, 117.1, -263.039, 539.109, -976.562, 89.816, -67.4764, -326.579, 260.537, 382.427, 153.478, 359.722, 369.549, -334.452, 577.38, -539.903, 501.694, 815.363, -270.669, -531.907, 987.976, -172.704, 709.342, 547.227, -308.939, 980.285, -331.95, 820.307, 606.616, 250.649, 533.86, 908.322, 714.835, 976.501, -10.651, 719.718, -917.844, -228.614, 697.745, 489.364, 212.867, 688.711, 779.595, 67.5375, -349.651, -364.544, -460.86, 459.151, -170.934, -963.439, -558.763, -387.371, 175.756, 748.894, 337.687, 459.212, -538.804, -775.018, -56.4287, -798.456, 401.776, 927.854, 564.379, -146.519, 510.91, -5.89007, 71.9321, -971.618, -799.188, 148.839, -868.465, 978.82, -866.207, 869.076, 678.213, 885.006, -175.878, -201.758, -356.853, 394.513, -510.666, -589.831, 930.662, -589.221, -848.018, -77.1203, -355.632, 132.603, 991.028, 462.996, -436.018, -657.094, 943.419, 479.598, -128.208, 947.935, -444.075, -591.784, 129.246, 766.167, -913.633, 585.314, -496.506, -432.356, 855.464, -588.305, 243.995, 362.896, 847.957, -232.765, -591.113, -58.1378 };
						float s_var[4096] = { 21.6742, 91.3846, 80.0623, 38.6731, 88.2046, 27.9427, 196.301, 91.1771, 14.6489, 23.2307, 16.3701, 156.255, 56.9292, 185.205, 154.149, 138.383, 172.6, 86.5444, 155.98, 84.5119, 151.317, 138.89, 68.6972, 95.7549, 20.3192, 42.0118, 82.5648, 114.127, 124.882, 108.084, 49.2264, 184.246, 83.7855, 60.86, 74.7276, 190.057, 19.8004, 109.922, 187.439, 66.5731, 65.1387, 99.8749, 81.0144, 86.5688, 141.935, 134.587, 132.426, 22.2907, 17.6946, 76.3878, 94.8576, 182.22, 35.8043, 120.304, 65.7369, 122.733, 120.945, 43.263, 129.643, 34.8582, 59.6026, 128.343, 76.3268, 6.00604, 55.5315, 163.939, 193.664, 156.652, 160.21, 118.027, 135.588, 52.0585, 178.301, 87.3623, 65.1265, 189.038, 48.1521, 121.72, 72.3106, 157.604, 97.2442, 123.069, 67.1773, 77.7551, 172.655, 19.837, 147.728, 111.161, 46.5346, 16.1077, 42.8785, 47.8652, 96.5911, 115.842, 30.4697, 168.047, 61.5253, 122.098, 130.21, 178.698, 71.5232, 111.24, 171.63, 25.367, 153.728, 182.183, 137.077, 178.887, 12.6835, 80.5811, 179.675, 197.046, 85.8364, 98.0743, 116.3, 161.364, 73.9708, 46.4736, 195.532, 188.781, 146.709, 107.022, 140.629, 18.366, 22.9438, 154.57, 67.5192, 73.0735, 126.371, 156.822, 77.7367, 196.423, 15.8696, 48.5488, 126.408, 64.2293, 91.0367, 80.813, 165.215, 71.3462, 172.649, 45.1674, 160.369, 93.35, 158.086, 172.893, 185.577, 123.27, 151.866, 109.207, 190.002, 183.325, 71.0593, 162.493, 41.2488, 111.606, 125.791, 28.5043, 43.3241, 23.835, 174.682, 189.599, 129.606, 50.0626, 155.345, 188.946, 130.79, 146.391, 189.843, 179.742, 117.911, 181.671, 165.142, 149.455, 20.2521, 99.2523, 188.61, 19.0008, 118.369, 153.716, 118.455, 17.0049, 12.006, 22.5166, 169.213, 129.002, 63.1062, 192.816, 192.224, 21.6315, 113.755, 23.0659, 31.0617, 41.908, 79.165, 79.8608, 152.068, 91.3968, 15.6316, 149.571, 149.748, 175.451, 63.5151, 125.071, 127.287, 27.4178, 65.1875, 167.498, 152.007, 7.88598, 58.0401, 142.924, 185.839, 78.8659, 42.4024, 134.117, 90.7682, 53.029, 24.5491, 27.2164, 109.354, 117.942, 134.666, 173.498, 20.13, 35.2428, 19.7821, 126.048, 168.145, 14.9052, 111.795, 51.503, 170.36, 64.4246, 170.397, 16.7852, 96.1821, 13.0619, 101.041, 150.798, 122.892, 11.0355, 47.9446, 81.1365, 66.1214, 103.769, 162.053, 45.8327, 185.345, 81.0755, 43.9528, 80.0623, 64.2903, 1.40996, 61.916, 77.8649, 166.491, 191.082, 144.823, 128.849, 65.6758, 118.448, 50.1419, 129.942, 151.292, 143.156, 106.9, 72.0664, 8.94192, 140.361, 33.6558, 1.90435, 75.8751, 199.939, 31.4524, 58.5894, 171.435, 0.689718, 151.805, 185.418, 102.536, 59.6088, 139.665, 116.581, 144.151, 56.0259, 151.366, 38.8989, 194.751, 30.903, 147.636, 128.507, 6.78121, 61.0004, 161.528, 162.603, 92.1537, 8.11182, 41.4014, 6.12812, 102.683, 143.297, 68.8986, 173.553, 85.5617, 46.0585, 51.912, 65.511, 13.8615, 178.631, 92.5321, 80.5872, 191.968, 135.118, 30.0363, 2.00201, 136.039, 165.783, 158.873, 59.3341, 129.521, 176.208, 21.5766, 65.2547, 53.5722, 1.56255, 76.8029, 27.2225, 92.5565, 155.797, 42.2498, 34.2174, 114.499, 55.855, 112.638, 18.4698, 126.572, 120.908, 52.6811, 192.145, 5.53606, 146.562, 165.087, 159.099, 103.238, 154.094, 133.451, 101.566, 127.067, 114.646, 140.3, 41.9141, 95.4131, 148.466, 60.0177, 179.333, 168.151, 199.115, 126.847, 101.547, 152.104, 182.684, 77.81, 71.5659, 81.5088, 12.0182, 170.721, 189.392, 73.5008, 95.9502, 77.1325, 28.7118, 58.5467, 113.474, 50.325, 148.668, 185.968, 63.0085, 168.249, 80.5017, 104.343, 186.615, 51.503, 189.367, 117.032, 169.164, 56.3128, 10.9561, 12.6225, 12.6286, 142.503, 63.2954, 170.409, 185.26, 16.4312, 133.409, 140.825, 45.7778, 149.01, 39.0515, 100.961, 142.674, 68.6117, 65.4622, 185.022, 188.348, 129.112, 77.0287, 89.7549, 108.359, 152.794, 104.05, 154.973, 165.661, 148.344, 4.41908, 118.992, 159.795, 103.824, 0.37843, 149.113, 101.108, 54.6342, 90.2127, 147.081, 160.002, 26.4901, 134.971, 129.551, 91.1161, 53.3647, 20.1849, 83.5719, 132.572, 70.4855, 92.6237, 97.0489, 180.169, 82.3084, 71.9321, 164.214, 36.6222, 5.4445, 37.4401, 66.0665, 135.783, 186.682, 89.8221, 125.901, 141.832, 195.904, 87.8323, 39.7168, 23.5237, 94.1069, 60.0299, 181.726, 3.39976, 176.336, 91.4945, 184.796, 182.018, 189.648, 85.5373, 195.935, 37.1166, 26.1116, 0.421155, 6.75069, 197.925, 90.7682, 161.101, 58.9496, 94.2839, 144.566, 45.9059, 124.741, 80.9534, 22.3701, 148.296, 62.3676, 36.7565, 25.9163, 113.547, 46.5529, 38.8623, 191.778, 150.487, 73.9586, 111.948, 34.315, 118.216, 143.669, 52.4613, 163.713, 87.8262, 112.656, 127.805, 178.857, 179.107, 26.2398, 50.5936, 35.3526, 72.2678, 123.576, 27.8329, 88.5098, 194.696, 118.094, 85.165, 11.2735, 182.586, 153.001, 66.0726, 187.225, 17.7496, 31.7209, 106.113, 178.497, 39.3017, 123.386, 182.122, 68.8131, 30.3598, 143.284, 136.436, 81.4234, 179.791, 8.34986, 176.116, 72.0603, 118.894, 167.626, 47.1633, 96.9268, 33.1736, 7.70287, 83.7489, 55.6413, 49.0127, 133.25, 132.395, 9.25932, 95.4131, 122.41, 151.25, 173.174, 40.6873, 157.341, 84.2555, 185.131, 104.227, 129.618, 10.3946, 48.1704, 143.181, 20.7465, 167.925, 148.643, 14.1667, 157.628, 53.5295, 149.101, 10.0345, 143.004, 196.625, 156.34, 171.557, 60.9394, 8.50856, 164.324, 74.1234, 157.305, 106.638, 172.179, 89.523, 195.209, 113.773, 177.142, 122.593, 85.5068, 132.273, 42.5367, 22.9865, 192.261, 93.2707, 40.7605, 71.8894, 113.169, 180.908, 71.218, 143.693, 135.099, 131.535, 153.331, 142.759, 164.495, 7.94702, 194.458, 115.134, 132.896, 48.616, 32.9051, 35.2489, 77.3705, 191.87, 155.889, 67.6412, 95.999, 75.1305, 92.4711, 103.696, 178.35, 109.153, 81.576, 173.327, 110.923, 27.2225, 14.9236, 131.736, 57.6556, 109.378, 55.0249, 21.0944, 15.363, 81.6126, 128.104, 107.297, 144.206, 90.1334, 92.1354, 191.656, 173.18, 150.542, 179.754, 197.626, 80.5567, 72.0664, 116.245, 32.5083, 49.3912, 56.801, 112.534, 124.229, 97.9583, 173.888, 133.11, 32.9661, 185.229, 108.744, 39.0393, 137.944, 191.327, 187.317, 5.90838, 199.115, 173.04, 75.2525, 16.7913, 193.89, 94.2473, 152.153, 30.2377, 28.8461, 31.3913, 160.772, 23.2185, 126.481, 53.975, 131.236, 109.122, 86.8618, 180.004, 180.279, 130.68, 24.427, 6.45161, 185.443, 127.995, 179.65, 14.7588, 134.306, 81.4539, 5.74969, 151.28, 148.704, 129.734, 130.485, 82.7357, 183.941, 62.7461, 192.761, 126.725, 2.99081, 89.6207, 154.454, 57.1551, 25.7881, 81.6736, 87.051, 184.741, 53.3403, 90.3653, 33.43, 112.436, 67.037, 140.281, 144.536, 165.947, 102.817, 97.0916, 91.1039, 99.1546, 0.396741, 10.535, 150.401, 74.6239, 27.5277, 132.365, 167.937, 119.059, 179.15, 9.32646, 194.159, 53.7492, 151.646, 51.1429, 196.197, 100.076, 192.083, 173.467, 44.5814, 159.954, 22.3762, 115.836, 174.957, 171.306, 38.5693, 34.6812, 124.778, 138.536, 184.991, 171.264, 145.067, 38.0016, 103.513, 194.635, 87.1181, 198.486, 157.775, 19.6478, 164.208, 156.621, 132.371, 99.8932, 188.305, 187.439, 7.0864, 195.672, 81.814, 109.763, 6.13422, 16.9561, 64.2964, 166.729, 122.33, 141.404, 114.158, 181.213, 116.355, 7.2512, 105.387, 186.346, 190.948, 181.359, 19.2755, 130.924, 140.129, 102.115, 31.1411, 59.9506, 173.431, 97.4578, 72.0237, 170.031, 15.0884, 77.8039, 65.4317, 139.891, 78.3044, 104.056, 39.0454, 135.82, 114.432, 30.5124, 111.435, 92.1659, 137.181, 22.9133, 153.783, 66.7684, 21.8757, 151.353, 126.603, 71.4682, 179.675, 58.2415, 91.464, 177.526, 106.723, 101.334, 97.702, 96.1394, 8.45973, 171.648, 144.139, 33.8939, 98.7884, 186.431, 15.7903, 23.2124, 174.291, 57.1551, 195.624, 127.866, 63.448, 132.493, 51.2223, 123.386, 150.908, 116.239, 96.5606, 52.5895, 176.629, 60.329, 128.66, 56.0198, 110.147, 32.7769, 39.3628, 77.4133, 99.7894, 60.6586, 102.036, 44.1664, 96.878, 148.07, 133.268, 122.825, 154.961, 169.109, 129.606, 49.5254, 4.00403, 35.8348, 179.284, 135.502, 99.6857, 141.252, 48.909, 147.496, 147.502, 77.2301, 45.2223, 63.2832, 102.084, 27.5521, 151.787, 102.121, 41.3648, 35.8165, 40.7117, 30.195, 115.812, 148.436, 159.38, 141.136, 172.655, 32.5877, 92.1537, 111.429, 55.0737, 69.216, 197.113, 130.937, 176.464, 120.957, 117.753, 108.335, 169.616, 175.146, 169.72, 151.598, 10.9439, 61.7512, 2.50862, 123.453, 19.7272, 46.382, 136.229, 71.865, 132.304, 98.233, 173.241, 135.49, 51.2711, 33.0332, 84.9574, 124.155, 69.7653, 131.071, 173.528, 174.322, 170.623, 30.1035, 108.542, 79.5495, 0.628681, 93.3744, 38.7707, 95.9868, 119.724, 52.5346, 178.887, 22.8095, 125.23, 125.657, 71.157, 157.714, 51.0575, 10.1749, 122.208, 127.372, 41.9446, 150.871, 14.6794, 192.901, 102.652, 82.8089, 194.024, 30.3293, 122.208, 49.3667, 30.1279, 186.102, 173.095, 95.1384, 54.1276, 7.47093, 85.4946, 33.9427, 70.0705, 48.0544, 96.4995, 91.7875, 141.649, 148.241, 174.328, 24.8054, 53.8957, 190.674, 17.542, 112.29, 49.9649, 130.747, 129.276, 24.5857, 42.7747, 102.029, 166.442, 166.124, 101.791, 196.789, 163.964, 93.3378, 14.6855, 121.384, 33.961, 7.67235, 124.119, 126.823, 140.043, 123.533, 39.3933, 14.1545, 146.422, 111.252, 23.2307, 14.9846, 142.228, 15.0639, 53.4623, 146.941, 40.669, 46.0829, 159.734, 44.0931, 53.2792, 50.2579, 35.255, 62.5019, 39.1186, 51.3321, 123.508, 60.6159, 129.008, 68.8498, 179.943, 54.7807, 24.72, 11.0721, 108.731, 149.705, 173.254, 42.9945, 74.4652, 143.895, 15.9795, 29.2917, 88.8821, 10.1749, 112.918, 52.2904, 72.5852, 51.1185, 141.935, 4.91958, 191.754, 129.533, 172.948, 58.7298, 69.8569, 173.919, 17.1758, 6.79952, 74.398, 158.94, 51.3321, 81.753, 78.6157, 160.906, 116.855, 48.6892, 98.2025, 88.2229, 94.1191, 173.199, 3.02133, 86.8862, 151.659, 64.272, 103.83, 20.9052, 70.3818, 192.34, 14.0019, 27.8268, 42.8053, 82.1619, 186.248, 149.168, 147.258, 180.871, 124.308, 35.8837, 97.0916, 0.878933, 69.7043, 128.855, 64.9251, 137.907, 163.463, 114.359, 181.848, 163.89, 82.7784, 54.5, 111.618, 194.989, 152.8, 192.737, 99.9298, 134.55, 133.311, 34.2906, 98.5992, 191.07, 80.4529, 101.675, 164.782, 71.1631, 83.4559, 165.111, 135.197, 47.5295, 76.3451, 148.015, 175.353, 101.938, 124.662, 107.486, 3.76598, 143.181, 187.201, 89.7488, 191.174, 100.027, 152.684, 60.6342, 134.739, 89.23, 151.531, 111.051, 107.297, 58.4796, 142.552, 137.443, 181.561, 104.343, 174.664, 197.272, 29.3344, 65.2791, 123.612, 139.47, 195.746, 189.581, 103.702, 109.922, 48.439, 78.3288, 154.503, 131.932, 1.83721, 103.421, 2.56355, 161.693, 16.3213, 100.461, 98.1536, 18.7384, 172.46, 136.79, 126.957, 191.449, 120.231, 154.424, 196.796, 117.466, 3.24107, 88.0215, 99.3744, 139.213, 57.9546, 57.8753, 46.9558, 134.349, 87.8872, 37.8613, 84.1212, 21.2043, 12.8422, 198.419, 174.462, 139.305, 93.1364, 124.497, 162.297, 104.794, 28.7851, 108.56, 9.42412, 140.922, 133.775, 118.522, 106.796, 83.5231, 185.223, 40.7361, 61.269, 61.4643, 147.642, 197.961, 157.317, 52.1256, 172.13, 142.308, 93.7162, 111.863, 70.6137, 123.252, 195.331, 81.4783, 65.041, 30.549, 99.765, 166.717, 11.6276, 12.2623, 119.132, 193.664, 100.046, 167.675, 76.7296, 139.299, 27.6559, 115.281, 18.7994, 136.9, 48.7503, 35.786, 1.21464, 156.56, 98.7274, 190.02, 151.219, 160.607, 193.341, 111.942, 4.1261, 134.117, 102.902, 76.6015, 115.061, 190.362, 101.523, 43.0982, 145.659, 151.268, 191.308, 72.5791, 25.7942, 2.90536, 1.57476, 21.1554, 190.307, 25.9651, 163.103, 107.938, 90.6949, 1.17801, 157.305, 157.927, 181.866, 89.8526, 40.492, 120.06, 183.831, 189.276, 43.0677, 60.1276, 18.421, 163.231, 43.7452, 28.7667, 21.0517, 147.447, 118.772, 4.41908, 27.369, 130.076, 166.161, 111.307, 135.032, 7.14133, 4.87075, 112.387, 140.385, 113.047, 149.089, 177.337, 50.2152, 25.9774, 57.3687, 125.517, 68.453, 48.4512, 56.9231, 143.937, 57.8692, 162.731, 142.802, 180.236, 76.8456, 61.2995, 52.1195, 10.6143, 191.04, 52.3515, 13.2694, 182.879, 160.137, 40.2478, 170.165, 34.3028, 199.567, 180.981, 81.4173, 184.71, 17.2002, 12.6347, 94.5952, 104.349, 125.101, 60.8966, 195.428, 156.92, 26.2093, 119.48, 131.999, 117.521, 76.0826, 5.59099, 14.0202, 165.618, 19.6173, 190.448, 187.011, 153.008, 2.74667, 104.691, 181.506, 146.818, 59.1937, 118.9, 137.309, 47.4319, 72.1397, 199.023, 102.689, 81.4844, 10.9928, 36.8603, 8.61232, 169.829, 43.0799, 55.5559, 117.954, 170.495, 120.371, 90.0296, 149.712, 196.661, 41.2, 142.46, 78.2189, 9.62554, 186.987, 12.7689, 7.40379, 86.7946, 133.927, 4.06507, 119.956, 164.611, 139.775, 103.128, 101.578, 56.5447, 105.484, 35.8715, 40.1807, 146.08, 11.9877, 197.821, 0.128178, 154.643, 31.8857, 59.5538, 71.4499, 184.594, 116.178, 47.2121, 94.0336, 37.2143, 55.9526, 97.9522, 56.8743, 5.63372, 19.1656, 90.3836, 172.289, 142.845, 162.304, 150.09, 25.1106, 4.60829, 117.112, 30.0485, 196.954, 67.513, 152.336, 166.155, 165.374, 21.8451, 10.4251, 7.61742, 78.1274, 139.634, 19.2572, 22.4982, 2.38655, 117.588, 78.8537, 178.155, 48.9578, 195.05, 123.087, 96.6704, 171.471, 147.002, 56.4165, 85.403, 40.6384, 152.007, 165.606, 42.3658, 193.463, 135.185, 6.4333, 36.7748, 57.4175, 186.187, 165.429, 164.47, 49.7391, 52.1439, 96.5423, 78.3715, 151.585, 105.679, 32.9295, 194.018, 157.262, 187.829, 173.644, 169.945, 198.468, 126.023, 145.323, 63.2344, 143.498, 127.079, 138.957, 159.929, 71.4499, 45.143, 121.58, 80.5139, 21.4728, 124.68, 122.99, 160.417, 16.9622, 18.4149, 197.491, 105.423, 164.788, 80.3369, 60.5609, 122.196, 29.487, 133.634, 0.634785, 123.698, 82.5465, 113.669, 65.7674, 9.69878, 26.545, 185.992, 125.394, 22.2358, 194.433, 139.225, 7.01926, 176.067, 23.7007, 53.1205, 107.95, 164.141, 5.64592, 41.4624, 82.0032, 138.072, 109.574, 70.0827, 1.25736, 163.634, 81.9788, 49.7818, 192.73, 151.189, 20.8258, 4.34584, 150.108, 61.1591, 24.7261, 113.486, 120.481, 184.539, 117.179, 30.8115, 166.051, 98.3062, 45.7533, 76.8029, 100.845, 95.938, 156.377, 106.79, 102.451, 92.8434, 142.467, 53.8957, 198.718, 6.86056, 69.2282, 147.032, 29.4748, 13.7028, 72.8111, 3.83312, 83.2728, 77.456, 134.587, 198.694, 9.0762, 156.017, 130.302, 105.625, 2.99692, 81.5577, 154.35, 16.3457, 70.0095, 116.037, 118.967, 133.177, 160.674, 181.646, 28.4555, 37.019, 182.83, 166.247, 128.41, 30.3354, 88.3572, 103.885, 129.911, 155.126, 34.8704, 30.2499, 108.768, 85.8669, 22.4311, 113.126, 79.7388, 97.5127, 5.28581, 57.8631, 45.735, 108.451, 78.5669, 187.872, 122.037, 198.462, 140.019, 0.0488296, 135.539, 1.69683, 26.722, 195.3, 98.9349, 174.664, 126.804, 183.953, 159.96, 139.14, 66.158, 66.2618, 25.605, 100.223, 70.1743, 87.8811, 61.33, 139.225, 0.433363, 170.293, 65.2791, 181.689, 66.4632, 116.324, 117.521, 40.3211, 12.4149, 90.585, 104.233, 133.012, 178.808, 174.944, 116.959, 75.7286, 39.2346, 199.011, 126.786, 150.572, 12.1158, 45.5641, 133.604, 48.6648, 147.362, 31.0984, 190.301, 88.1802, 88.1252, 161.199, 101.981, 107.273, 107.529, 61.0736, 0.189215, 32.0566, 20.3497, 181.005, 192.151, 119.596, 169.781, 76.8151, 84.9269, 75.8446, 89.1018, 165.337, 184.716, 48.6343, 38.4533, 154.881, 171.001, 95.6206, 16.5044, 150.713, 45.8266, 127.28, 47.1938, 35.4991, 157.982, 124.863, 141.459, 124.815, 102.206, 33.3628, 133.934, 97.5677, 93.4599, 129.496, 194.678, 1.50761, 16.0894, 64.6382, 15.8879, 133.665, 148.088, 182.842, 73.6534, 195.831, 160.283, 113.761, 164.605, 125.028, 190.564, 40.6568, 17.6946, 198.987, 193.121, 43.3363, 180.902, 16.5777, 162.627, 116.129, 182.91, 153.343, 33.491, 60.8661, 169.884, 91.8973, 54.0605, 54.8479, 17.1697, 27.4483, 35.8409, 93.8017, 115.989, 148.253, 77.7123, 109.842, 35.1817, 191.046, 165.154, 164.483, 57.9241, 71.5598, 44.4533, 116.55, 193.432, 190.002, 171.953, 165.197, 63.6982, 11.4383, 138.224, 67.8365, 65.1997, 121.384, 167.327, 33.1431, 5.62151, 156.163, 90.9879, 88.992, 15.5705, 8.44142, 197.888, 125.443, 135.307, 107.675, 157.781, 12.714, 176.434, 175.573, 20.8075, 194.153, 58.6383, 56.5874, 91.6654, 154.155, 21.4362, 23.3589, 78.8171, 161.217, 110.837, 157.207, 118.76, 85.8425, 141.081, 3.55235, 75.1793, 93.5392, 131.382, 102.475, 113.224, 12.7689, 17.6153, 194.086, 148.698, 177.935, 39.0698, 99.0448, 172.277, 147.325, 0.122074, 74.0379, 48.7381, 96.1211, 6.99484, 42.2193, 191.174, 84.3226, 79.458, 98.6847, 110.202, 199.762, 37.5439, 90.9696, 38.2092, 176.019, 63.2771, 74.4652, 28.547, 85.4274, 137.394, 42.9456, 129.752, 107.053, 151.048, 133.995, 34.7728, 169.622, 92.526, 109.482, 5.18204, 69.5212, 109.006, 111.051, 70.4184, 63.7593, 43.2203, 90.5606, 101.962, 137.931, 45.2162, 115.586, 162.59, 149.187, 59.0533, 149.852, 130.57, 44.203, 184.533, 108.725, 198.92, 152.159, 113.895, 189.691, 49.5499, 152.69, 43.5743, 77.7795, 102.591, 118.87, 60.9027, 26.0811, 40.492, 35.1634, 5.80462, 11.2064, 7.33665, 190.423, 7.55028, 163.726, 83.0226, 45.8205, 96.4629, 73.6534, 32.4534, 177.85, 90.6095, 170.952, 129.82, 96.2676, 157.567, 180.944, 57.7593, 137.095, 51.1124, 54.799, 81.6187, 30.549, 22.956, 175.048, 98.6297, 144.041, 9.43632, 35.963, 162.181, 0.11597, 93.2646, 199.133, 80.9839, 69.4723, 40.2234, 123.814, 93.3317, 38.8623, 110.916, 72.5181, 16.2908, 154.112, 147.435, 91.3907, 69.9423, 38.0749, 135.124, 65.7186, 176.58, 183.599, 39.2956, 50.3983, 134.922, 121.488, 116.916, 75.4051, 69.3869, 63.4846, 103.36, 26.2337, 154.234, 20.2399, 74.1111, 199.42, 12.8361, 194.269, 181.475, 132.951, 191.888, 47.908, 66.3106, 111.777, 110.587, 46.3332, 144.761, 148.271, 84.9269, 75.4784, 169.939, 37.672, 26.838, 135.026, 68.6666, 88.5952, 18.4576, 106.363, 53.1877, 128.465, 161.339, 195.581, 7.9104, 184.008, 100.638, 51.3932, 151.079, 36.6894, 24.3782, 158.165, 171.526, 53.2304, 137.657, 103.824, 40.3943, 91.0428, 11.8168, 122.88, 97.8362, 78.7011, 8.7466, 114.341, 14.7404, 71.4133, 180.035, 134.733, 29.4565, 181.5, 41.2122, 166.039, 59.5111, 183.844, 20.9662, 158.666, 88.3145, 196.381, 185.382, 149.431, 26.899, 35.548, 37.1044, 54.5244, 50.618, 46.7727, 189.636, 137.468, 10.889, 9.92462, 116.471, 152.098, 121.885, 119.797, 175.402, 160.521, 122.269, 41.5418, 107.651, 26.0811, 111.917, 41.2061, 12.1586, 181.365, 63.6189, 191.137, 194.769, 71.102, 9.81475, 50.4654, 92.4345, 70.2109, 104.318, 20.6061, 72.4876, 76.7052, 158.995, 175.689, 106.796, 179.699, 55.5376, 168.041, 121.03, 2.34993, 32.1421, 8.65505, 18.8604, 119.871, 45.2528, 122.684, 181.17, 152.44, 6.2624, 79.8425, 40.2905, 133.207, 93.2402, 199.963, 129.209, 151.268, 137.651, 33.3811, 5.79241, 69.6005, 86.0561, 116.471, 52.0096, 118.07, 136.882, 2.06915, 148.137, 161.376, 152.074, 164.983, 21.9794, 42.0057, 143.852, 182.403, 73.1162, 198.26, 36.433, 189.434, 72.0725, 157.152, 102.744, 9.50957, 134.587, 102.426, 76.1132, 159.929, 170.794, 117.502, 137.04, 132.231, 9.44243, 103.452, 174.859, 147.758, 197.174, 171.978, 124.509, 188.055, 72.6341, 113.352, 178.222, 100.369, 115.812, 139.421, 72.2373, 178.137, 172.155, 101.541, 48.5427, 16.3945, 151.854, 183.593, 91.586, 42.3231, 4.03455, 69.8813, 39.9731, 120.64, 165.99, 188.177, 173.229, 34.1258, 87.3684, 27.6315, 73.4519, 39.0637, 29.4382, 131.785, 162.12, 95.7244, 139.28, 166.497, 111.722, 173.815, 198.999, 62.9414, 132.536, 32.9661, 49.9527, 35.5785, 134.312, 78.2006, 91.9889, 75.1061, 163.024, 90.3836, 57.6189, 116.428, 112.558, 31.7087, 61.2812, 81.6431, 111.441, 126.176, 123.545, 197.998, 184.582, 15.009, 170.312, 190.851, 189.947, 187.201, 107.053, 151.665, 30.311, 84.9147, 168.029, 80.9412, 139.982, 84.0052, 140.507, 106.229, 46.4736, 149.443, 63.9607, 175.06, 7.28782, 165.807, 4.32142, 22.9255, 121.592, 142.753, 103.171, 16.2603, 116.929, 95.4802, 101.962, 70.4001, 10.9684, 0.195318, 141.258, 42.5306, 48.2192, 24.5918, 34.9315, 199.713, 111.728, 33.2957, 19.3854, 161.779, 9.80865, 168.365, 177.789, 119.358, 37.7697, 104.733, 139.262, 126.817, 14.6245, 180.7, 53.1266, 70.2414, 194.818, 3.36924, 38.1237, 110.398, 54.9089, 129.167, 106.607, 0.701926, 155.358, 19.245, 163.781, 80.1965, 77.1752, 70.0095, 81.9117, 141.508, 33.6924, 118.558, 49.5621, 173.809, 157.256, 23.1452, 195.795, 180.816, 60.9333, 118.686, 89.3704, 73.7266, 75.2647, 147.172, 113.028, 39.2285, 159.966, 63.1733, 112.638, 182.409, 90.6278, 114.328, 121.458, 44.7768, 120.475, 196.759, 159.349, 197.205, 117.051, 85.5617, 106.973, 107.901, 173.962, 140.312, 55.031, 85.3847, 97.5555, 85.4701, 128.495, 96.4568, 168.694, 65.9017, 185.772, 59.1571, 179.443, 177.331, 192.999, 10.7425, 144.768, 71.7795, 73.1895, 150.652, 89.3094, 38.081, 30.5063, 158.47, 109.061, 157.341, 149.455, 186.901, 108.078, 133.36, 69.0939, 129.752, 10.2359, 198.163, 132.286, 164.879, 45.9365, 92.4345, 9.87579, 172.161, 113.785, 121.006, 199.957, 26.4657, 17.3772, 83.285, 2.02033, 165.654, 176.971, 78.7866, 166.265, 33.4849, 16.1992, 187.542, 17.7313, 197.644, 23.8411, 25.5989, 55.5803, 142.662, 90.4935, 175.506, 27.2713, 137.931, 120.286, 91.7264, 77.8039, 180.645, 117.637, 81.1548, 173.943, 13.7638, 148.949, 158.592, 3.39366, 19.013, 179.327, 177.154, 149.089, 184.814, 184.295, 33.8023, 138.347, 149.272, 136.528, 149.809, 167.125, 118.149, 67.568, 26.1605, 99.2096, 57.8326, 4.52284, 74.1111, 140.483, 163.286, 181.829, 120.609, 188.079, 1.03763, 127.897, 188.745, 54.6525, 127.146, 26.9112, 153.27, 167.589, 180.157, 155.51, 71.0349, 143.187, 49.0799, 194.482, 186.456, 35.9203, 116.367, 15.5217, 11.9877, 62.6545, 175.86, 142.961, 83.6207, 1.82501, 25.7759, 90.4141, 191.296, 67.7023, 136.68, 172.594, 84.4142, 59.5782, 194.427, 191.943, 52.3026, 186.01, 118.155, 0.408948, 179.662, 164.006, 153.252, 136.161, 77.0409, 193.292, 39.3139, 135.258, 86.3308, 5.47502, 53.1327, 118.43, 161.205, 188.33, 126.634, 52.7482, 22.5898, 184.924, 39.8633, 88.3938, 189.16, 167.937, 88.9737, 170.324, 82.7662, 188.806, 29.1574, 125.01, 8.94192, 1.9837, 92.6481, 22.1809, 12.36, 164.147, 81.6614, 11.5177, 7.28172, 8.38649, 136.381, 34.3699, 183.056, 93.4965, 177.929, 10.4678, 84.5546, 128.111, 26.3131, 99.6857, 167.681, 169.976, 193.445, 38.6975, 29.8105, 100.15, 151.024, 152.745, 115.262, 172.234, 73.6045, 42.3353, 95.7854, 36.5673, 56.4898, 144.963, 125.993, 39.4787, 8.7405, 171.825, 173.876, 197.473, 21.131, 19.5441, 26.2276, 46.4553, 9.10672, 156.89, 169.152, 165.996, 30.781, 86.4895, 143.504, 178.466, 79.873, 81.5577, 175.365, 23.3345, 153.697, 197.607, 81.5149, 106.351, 196.124, 159.16, 49.9222, 123.746, 115.543, 121.482, 95.1567, 161.498, 77.8649, 107.303, 188.916, 69.509, 74.4346, 118.516, 65.4622, 110.849, 165.044, 122.916, 102.103, 85.9584, 195.886, 115.903, 74.6605, 131.889, 192.517, 57.21, 187.927, 196.484, 27.839, 82.6258, 181.14, 187.64, 165.062, 190.704, 108.469, 197.504, 75.6615, 4.46791, 194.464, 1.68462, 132.377, 8.18506, 164.525, 33.4971, 177.142, 59.0228, 50.3677, 33.1553, 164.086, 173.791, 70.5283, 172.002, 77.4804, 190.661, 185.772, 51.7167, 124.735, 105.893, 129.6, 178.57, 4.31532, 85.5495, 113.736, 112.583, 48.7869, 152.208, 8.53908, 91.818, 42.7015, 146.678, 198.529, 131.529, 115.641, 164.727, 112.803, 198.425, 110.111, 68.7948, 30.5551, 59.859, 24.5918, 74.7337, 161.522, 8.08741, 131.437, 13.4831, 1.42827, 59.4378, 169.884, 67.8365, 162.511, 28.5714, 13.9958, 184.112, 78.9148, 77.6696, 15.6621, 158.91, 187.243, 140.678, 22.5349, 0.811792, 181.951, 22.5043, 71.0105, 167.138, 145.866, 173.266, 154.21, 75.5699, 155.638, 137.449, 64.9251, 90.994, 162.206, 78.2189, 113.608, 187.133, 146.269, 176.55, 121.323, 83.2728, 148.753, 105.49, 87.7285, 56.032, 176.293, 179.388, 134.05, 26.0323, 181.683, 178.076, 121.012, 119.639, 119.34, 46.1074, 141.856, 105.655, 75.8141, 67.0003, 90.2921, 107.083, 75.2098, 112.406, 103.446, 187.805, 159.679, 166.369, 79.8486, 14.3132, 144.877, 124.516, 72.4143, 192.444, 50.441, 68.3004, 156.377, 79.0613, 5.46892, 14.2033, 172.295, 68.8314, 28.9621, 137.98, 143.986, 155.87, 85.3175, 73.8182, 171.538, 170.531, 14.771, 88.6258, 199.219, 91.0672, 138.896, 131.217, 58.5101, 30.3903, 141.057, 156.523, 186.029, 97.7752, 21.8696, 124.833, 16.5105, 135.575, 129.691, 75.985, 50.795, 130.931, 66.0665, 78.6218, 198.767, 102.524, 66.213, 54.3779, 197.333, 62.2517, 54.4816, 77.1142, 103.397, 51.9791, 174.841, 147.124, 61.8427, 55.2507, 196.301, 40.3638, 148.479, 183.996, 193.451, 32.6731, 87.9971, 140.257, 101.859, 98.9776, 46.9802, 152.91, 148.601, 187.536, 101.852, 189.373, 157.683, 169.121, 181.823, 42.3414, 130.528, 61.269, 86.8923, 91.5128, 101.45, 127.488, 180.816, 140.251, 68.1234, 51.4725, 159.038, 127.378, 197.034, 186.334, 91.7081, 113.852, 103.964, 78.7927, 141.624, 144.481, 9.45463, 68.4469, 157.183, 184.674, 169.292, 161.931, 140.397, 132.121, 179.931, 182.183, 189.825, 103.537, 159.02, 192.627, 118.54, 196.387, 0.824, 196.423, 147.136, 125.571, 106.735, 110.66, 71.6514, 135.179, 190.704, 15.6011, 106.565, 185.192, 159.709, 78.8781, 2.75277, 13.184, 69.2648, 154.717, 40.4675, 71.102, 153.862, 58.1561, 97.293, 136.906, 132.42, 114.286, 191.125, 1.3245, 188.794, 182.434, 98.1292, 29.6091, 27.2164, 155.394, 45.7228, 198.175, 2.78939, 133.671, 80.5445, 6.38447, 126.914, 74.6666, 111.405, 63.2405, 147.795, 116.904, 27.2591, 149.113, 105.899, 179.272, 90.2005, 152.44, 164.422, 35.2, 3.39976, 81.4661, 114.756, 61.9221, 54.0849, 150.957, 156.969, 64.6748, 131.516, 143.919, 155.248, 175.903, 19.9408, 48.2986, 168.395, 29.487, 166.588, 168.737, 55.2812, 9.99786, 124.589, 113.254, 155.333, 176.861, 70.9983, 54.9883, 43.9589, 63.5945, 199.75, 68.9047, 163.091, 152.043, 189.581, 143.535, 126.56, 21.5766, 147.075, 67.6534, 100.644, 173.339, 24.5613, 192.047, 59.505, 129.417, 113.15, 66.1763, 115.653, 7.09861, 10.6143, 149.962, 125.413, 168.096, 64.4673, 0.44557, 169.427, 142.729, 89.8282, 133.549, 108.072, 29.1696, 162.45, 160.753, 11.9633, 167.669, 86.6848, 0.848415, 67.4276, 89.7366, 138.438, 166.662, 156.938, 142.405, 83.1324, 126.719, 114.609, 187.432, 68.6972, 142.039, 56.6729, 175.488, 193.518, 177.801, 186.926, 195.434, 149.413, 90.0662, 77.1874, 198.498, 139.604, 87.5271, 22.8278, 197.491, 71.6819, 68.1967, 73.1651, 141.063, 25.5806, 31.1411, 136.979, 62.8742, 196.551, 163.201, 162.957, 105.863, 113.083, 0.683615, 195.178, 21.4179, 129.954, 66.1275, 103.574, 143.638, 196.136, 179.98, 189.416, 38.496, 97.7752, 47.3342, 65.7308, 28.1381, 176.189, 119.663, 12.8483, 190.033, 120.804, 131.486, 8.79543, 31.2021, 37.1288, 112.149, 34.8094, 37.4828, 189.758, 72.512, 55.8, 97.6959, 20.2704, 166.674, 75.8019, 2.88095, 40.2722, 128.672, 68.4652, 20.5206, 199.329, 184.246, 182.501, 143.779, 127.915, 37.5011, 22.1625, 15.6499, 55.4399, 132.682, 2.73446, 128.282, 3.16172, 190.844, 180.969, 97.0916, 52.1867, 21.601, 160.546, 10.0467, 86.4223, 9.83306, 72.5913, 51.8876, 10.5533, 77.4133, 31.1045, 3.2899, 43.0128, 119.907, 63.4419, 15.1433, 83.9564, 118.32, 115.787, 57.7471, 158.196, 125.791, 133.598, 45.0148, 133.219, 129.325, 17.1087, 33.7168, 112.131, 186.517, 51.7838, 97.6959, 101.608, 37.2326, 13.7883, 77.0531, 154.906, 190.399, 150.438, 2.41707, 189.801, 85.641, 35.6944, 194.134, 81.5455, 130.07, 100.632, 36.4757, 30.8786, 9.94293, 38.8806, 26.899, 6.97043, 77.4377, 87.1303, 100.217, 65.389, 12.3234, 180.572, 27.839, 141.057, 158.727, 183.367, 92.0011, 116.257, 136.784, 194.147, 34.9742, 163.573, 129.441, 126.444, 83.2789, 163.854, 44.0992, 164.177, 46.8947, 199.219, 149.388, 92.1293, 93.5148, 120.969, 186.853, 101.401, 197.723, 192.615, 157.683, 69.8813, 65.2303, 7.80053, 184.423, 117.728, 132.121, 83.3094, 47.7126, 37.8124, 39.0027, 126.994, 197.986, 1.22684, 180.309, 180.773, 42.1888, 137.718, 128.452, 38.5876, 105.515, 191.168, 133.061, 139.39, 68.099, 50.0504, 95.23, 135.069, 76.7785, 152.953, 16.6814, 133.47, 131.748, 168.963, 124.052, 16.3213, 148.344, 153.191, 68.038, 32.2398, 93.7101, 177.245, 197.681, 64.6687, 198.572, 34.6446, 109.531, 174.871, 43.8795, 21.3691, 71.9138, 19.8553, 122.623, 191.266, 124.113, 145.421, 76.4428, 86.5078, 50.6058, 154.045, 7.45262, 143.144, 30.2805, 147.899, 64.8762, 171.471, 160.662, 46.8032, 106.821, 111.576, 57.094, 181.219, 36.9579, 63.7532, 127.012, 160.161, 16.2664, 194.012, 119.132, 14.2155, 149.76, 114.518, 147.661, 13.5197, 48.1521, 139.085, 191.54, 160.674, 64.6504, 13.6052, 68.6422, 158.751, 6.61031, 121.58, 85.0002, 54.2619, 122.306, 164.531, 159.044, 155.119, 162.597, 117.484, 20.7587, 23.5908, 117.685, 59.4623, 85.989, 144.31, 28.8278, 78.927, 5.58489, 67.1896, 75.045, 54.2619, 89.6695, 25.4402, 134.855, 109.232, 6.81784, 28.5043, 22.956, 143.455, 158.312, 30.0546, 1.99591, 37.8796, 127.323, 170.922, 169.268, 135.24, 3.17392, 97.1099, 118.4, 195.532, 180.529, 152.727, 76.1254, 26.4718, 33.5948, 47.9751, 113.974, 189.013, 65.389, 2.13019, 34.9803, 177.715, 131.932, 161.473, 135.447, 76.3207, 141.441, 92.2819, 41.5906, 88.1619, 28.5958, 148.814, 21.3324, 141.649, 3.72936, 116.239, 170.989, 170.714, 77.0531, 128.642, 76.9616, 70.3146, 57.8692, 61.977, 152.513, 126.908, 28.8095, 2.03253, 49.4339, 123.862, 95.0224, 187.414, 144.804, 121.989, 8.417, 27.4728, 102.67, 114.963, 66.8844, 195.361, 110.794, 159.844, 88.4854, 195.886, 192.364, 182.488, 60.7074, 8.56349, 192.712, 58.0523, 64.6565, 5.57878, 41.023, 166.466, 74.2882, 144.713, 87.0876, 22.8584, 30.6223, 37.3302, 112.961, 183.099, 190.973, 91.3785, 19.7394, 24.1768, 153.136, 129.313, 189.257, 199.768, 154.643, 81.0877, 120.798, 170.605, 46.968, 152.123, 142.814, 10.0162, 144.743, 66.2679, 199.67, 84.3776, 149.968, 183.782, 113.321, 8.5696, 151.256, 94.2839, 120.078, 76.4489, 98.0193, 84.7316, 82.2718, 166.326, 149.712, 82.4061, 175.03, 121.415, 100.046, 72.8416, 47.9446, 5.33464, 76.1864, 192.84, 61.8305, 32.5449, 113.022, 98.3978, 0.421155, 173.846, 2.3133, 7.70287, 57.094, 36.7931, 16.9744, 19.0985, 96.585, 21.2531, 110.257, 104.05, 191.723, 149.785, 189.978, 175.097, 35.1085, 173.15, 67.7328, 28.84, 25.898, 150.792, 83.1385, 163.927, 31.4646, 106.284, 3.01523, 73.9647, 24.134, 157.793, 195.636, 15.5583, 192.926, 123.576, 79.9097, 51.2284, 130.222, 2.41096, 49.6597, 167.406, 93.5514, 129.118, 72.7805, 67.3727, 142.247, 4.96231, 165.49, 39.2773, 149.211, 3.18613, 141.527, 99.0509, 87.5942, 149.431, 36.4086, 8.4109, 192.56, 177.221, 6.60421, 167.827, 197.949, 58.4429, 103.781, 184.478, 48.0483, 8.81375, 124.015, 115.213, 84.2921, 168.346, 132.011, 184.552, 106.949, 156.78, 60.3046, 177.618, 111.899, 104.117, 183.532, 108.139, 102.298, 156.047, 67.568, 161.388, 175.341, 54.4267, 193.835, 112.174, 83.9442, 192.755, 58.4429, 17.7862, 73.4947, 146.617, 146.464, 185.998, 193.542, 79.1894, 181.115, 17.9998, 84.5241, 21.6865, 47.9995, 126.389, 39.5154, 28.8339, 194.324, 128.135, 140.904, 9.33256, 103.085, 112.54, 72.1152, 31.3303, 42.3414, 184.222, 4.37635, 102.896, 89.3948, 81.0572, 158.379, 27.485, 30.0058, 67.2018, 41.0169, 134.41, 132.652, 100.992, 157.128, 84.2921, 197.693, 77.9565, 196.173, 149.565, 81.6309, 148.79, 93.9909, 169.909, 96.3103, 112.284, 169.5, 182.733, 83.1507, 83.3399, 136.399, 128.654, 162.34, 35.8959, 189.941, 31.9529, 157.085, 20.9357, 168.578, 56.0381, 92.7946, 59.4317, 190.747, 33.9366, 171.337, 110.312, 166.54, 73.7388, 20.2277, 113.45, 184.46, 39.0759, 16.8706, 107.608, 173.101, 104.593, 87.759, 23.4321, 37.9101, 186.425, 182.702, 151.396, 72.8782, 47.6272, 24.0364, 84.7865, 47.2549, 78.7011, 130.876, 137.663, 167.284, 153.02, 118.278, 37.8307, 112.912, 157.317, 71.4988, 94.821, 149.516, 197.143, 195.215, 42.8236, 114.615, 77.3339, 137.718, 158.708, 79.5862, 57.5152, 190.149, 54.9333, 86.3918, 82.5098, 160.723, 147.569, 159.288, 87.8567, 123.93, 199.243, 191.565, 180.255, 44.7584, 137.095, 107.224, 93.0326, 77.9382, 145.775, 97.7508, 116.648, 142.125, 35.3771, 133.793, 104.099, 169.127, 84.4508, 151.885, 151.457, 28.1381, 157.939, 116.178, 153.276, 124.967, 100.143, 155.162, 33.1065, 76.1986, 2.71615, 173.37, 48.9456, 58.4857, 147.63, 138.096, 173.711, 32.7219, 25.0923, 41.1451, 150.694, 43.2752, 26.5999, 135.508, 24.0364, 192.199, 12.8361, 47.0962, 144.23, 88.4548, 156.688, 161.492, 48.5366, 172.143, 61.1652, 85.6044, 16.48, 113.694, 152.434, 184.454, 100.327, 88.113, 147.71, 192.846, 30.5185, 124.204, 198.303, 102.091, 196.197, 128.751, 107.517, 121.775, 100.601, 185.766, 23.6763, 116.404, 91.6044, 130.705, 8.06299, 117.179, 167.144, 199.683, 77.8832, 108.683, 55.6963, 153.691, 124.253, 90.6156, 171.752, 77.81, 105.148, 14.5085, 46.2661, 55.4582, 113.395, 195.453, 32.1726, 117.563, 119.62, 198.785, 84.8048, 66.8172, 190.228, 1.84332, 17.3711, 176.891, 180.377, 30.6772, 31.4524, 166.631, 101.92, 71.7551, 122.446, 49.2874, 166.417, 58.681, 160.039, 56.7705, 103.055, 133.934, 43.6781, 38.8134, 151.659, 185.565, 133.048, 137.211, 130.631, 58.9312, 153.526, 135.051, 179.943, 43.4034, 176.659, 80.9229, 179.083, 34.0953, 75.6188, 188.141, 40.0342, 168.572, 55.6963, 8.94803, 75.2586, 138.572, 59.743, 83.7672, 40.6262, 176.733, 48.6709, 65.3829, 53.4257, 118.296, 156.89, 118.18, 185.314, 137.852, 122.776, 192.358, 130.284, 161.016, 194.183, 173.144, 147.636, 151.189, 108.86, 105.039, 79.9402, 71.2546, 149.944, 90.994, 46.028, 120.304, 76.4367, 66.6585, 8.93582, 45.3505, 128.66, 49.5987, 18.8787, 128.062, 108.713, 63.4968, 131.236, 46.437, 135.984, 137.394, 199.487, 11.5909, 77.5597, 179.876, 143.211, 179.437, 53.7492, 128.33, 91.0489, 137.37, 18.5125, 173.15, 198.669, 66.0604, 14.9541, 56.9353, 141.716, 176.128, 105.789, 48.7381, 83.8221, 19.8431, 178.79, 193.274, 119.547, 77.9015, 27.2652, 35.1085, 192.34, 21.0883, 105.576, 17.9327, 87.521, 47.0412, 159.514, 158.757, 100.266, 91.9279, 131.84, 197.101, 178.552, 117.185, 142.741, 19.245, 141.111, 71.2485, 145.274, 157.457, 170.452, 126.261, 55.8611, 164.177, 180.773, 189.776, 159.74, 61.2568, 60.9699, 91.4945, 191.449, 64.1865, 192.59, 44.9965, 90.2676, 181.304, 42.787, 184.423, 30.7321, 87.9849, 32.3191, 176.226, 128.062, 75.9301, 78.4875, 105.863, 18.6407, 77.3095, 13.1352, 39.552, 115.5, 164.037, 75.5882, 60.9394, 138.762, 121.946, 80.6421, 182.879, 121.134, 100.052, 61.5192, 134.074, 88.3511, 134.733, 187.86, 20.8808, 14.6733, 148.967, 158.647, 63.9668, 53.3403, 26.545, 70.2231, 10.0589, 42.024, 161.309, 159.239, 18.6468, 83.3338, 81.3257, 20.6, 23.7007, 0.244148, 148.155, 100.497, 170.415, 66.6036, 131.04, 64.2903, 126.884, 12.5675, 56.1357, 39.552, 70.8701, 105.1, 130.717, 133.995, 151.482, 103.409, 142.418, 28.4433, 25.9163, 89.4375, 133.427, 44.8317, 71.1753, 173.345, 149.718, 89.1446, 70.7663, 73.9158, 65.2791, 69.3747, 176.44, 174.12, 81.3074, 188.537, 144.902, 51.4908, 100.443, 112.583, 49.5315, 83.6634, 175.994, 13.422, 67.1163, 128.8, 36.1278, 86.8618, 139.537, 81.0755, 83.1385, 145.555, 110.508, 101.144, 67.5985, 87.0205, 137.303, 121.616, 37.6415, 36.5734, 177.459, 27.5033, 163.152, 165.307, 150.493, 111.728, 34.9681, 183.056, 174.645, 5.53606, 39.1369, 106.375, 78.5607, 84.0236, 6.1037, 101.505, 188.055, 146.556, 134.959, 2.3133, 187.774, 172.283, 192.615, 103.33, 57.9608, 150.56, 188.061, 27.6315, 86.166, 186.462, 121.574, 69.8813, 153.948, 81.2098, 8.92361, 14.2521, 98.5992, 187.689, 180.633, 27.4239, 149.876, 140.849, 56.1052, 26.5267, 41.7005, 63.2527, 192.969, 122.141, 198.914, 45.4848, 11.182, 136.229, 198.932, 101.639, 192.328, 195.135, 108.524, 199.799, 153.789, 197.54, 95.5168, 167.486, 197.986, 157.439, 115.476, 72.1397, 136.595, 22.0832, 73.4886, 76.5954, 189.77, 165.001, 60.8234, 169.543, 160.338, 153.899, 66.689, 170.971, 83.0531, 193.695, 164.928, 12.6896, 148.79, 44.4411, 175.744, 93.759, 103.763, 14.3132, 2.42317, 110.965, 123.765, 27.5887, 43.0677, 164.293, 163.366, 167.589, 77.279, 137.4, 180.139, 94.6501, 27.9, 153.075, 33.8694, 139.14, 197.736, 103.714, 60.8112, 69.3442, 181.256, 56.7766, 122.855, 134.532, 51.326, 135.282, 131.761, 182.824, 20.9418, 73.2505, 48.4085, 54.2436, 179.907, 46.1684, 156.987, 48.6465, 106.577, 176.507, 14.1972, 68.6972, 154.595, 114.103, 73.5374, 37.8124, 14.5695, 91.7264, 184.436, 193.2, 150.835, 55.4827, 166.521, 53.7309, 113.132, 70.2902, 58.8458, 63.0024, 123.356, 0.372326, 44.0809, 107.236, 137.266, 64.7176, 47.7432, 108.28, 181.188, 67.5314, 12.421, 91.5799, 163.768, 103.72, 46.9741, 129.124, 162.651, 139.36, 50.7767, 157.439, 186.236, 67.1651, 12.9154, 70.5405, 10.358, 36.9518, 186.438, 87.9177, 90.0601, 61.1103, 137.523, 197.479, 146.245, 95.0713, 69.7409, 73.5313, 27.4422, 192.779, 23.2612, 69.7775, 46.5102, 137.15, 149.065, 87.2341, 192.37, 145.555, 58.1683, 117.234, 16.6753, 50.2762, 45.8449, 59.3768, 195.618, 154.003, 190.832, 14.5268, 166.552, 160.247, 129.557, 52.7543, 39.7717, 178.863, 105.362, 56.3066, 185.229, 6.48213, 92.7152, 37.5744, 94.2167, 82.0338, 196.435, 132.096, 116.117, 117.13, 125.901, 66.4754, 70.1193, 1.60527, 144.273, 101.376, 131.919, 183.966, 92.7213, 49.4949, 195.459, 126.835, 89.639, 168.523, 109.629, 37.4523, 104.855, 129.112, 190.002, 73.1285, 8.84426, 57.8509, 5.52995, 21.7231, 96.9939, 55.2263, 40.3088, 57.1001, 88.7539, 188.604, 30.2316, 169.597, 1.90435, 160.161, 174.755, 71.4988, 101.767, 49.9161, 23.603, 177.538, 125.401, 133.647, 38.4899, 60.2802, 180.84, 175.878, 29.8349, 180.7, 121.317, 197.845, 68.4774, 93.5453, 194.47, 120.475, 72.4082, 127.903, 166.564, 82.345, 68.4652 };
						int n[4096] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8, 6, 9, 2, 4, 2, 11, 10, 2, 5, 5, 4, 3, 10, 9, 7, 8, 9, 3, 9, 11, 6, 11, 4, 8, 9, 4, 7, 9, 2, 7, 3, 8, 11, 3, 10, 6, 5, 9, 3, 7, 6, 7, 4, 7, 5, 3, 6, 11, 10, 7, 2, 4, 2, 8, 8, 6, 3, 10, 6, 8, 4, 2, 4, 11, 5, 10, 2, 9, 5, 7, 3, 2, 9, 4, 8, 4, 11, 2, 6, 5, 11, 10, 2, 7, 7, 11, 3, 8, 4, 5, 4, 8, 5, 5, 10, 3, 4, 6, 9, 2, 6, 3, 9, 7, 2, 9, 2, 6, 7, 2, 7, 4, 11, 2, 6, 7, 9, 5, 7, 5, 10, 10, 5, 7, 6, 2, 4, 7, 3, 6, 5, 3, 3, 8, 2, 7, 11, 3, 2, 5, 7, 9, 8, 8, 2, 4, 6, 3, 4, 6, 11, 5, 6, 6, 8, 11, 5, 4, 6, 2, 8, 7, 8, 2, 9, 6, 7, 4, 3, 8, 8, 5, 3, 7, 2, 4, 5, 6, 5, 6, 11, 9, 9, 4, 10, 5, 3, 2, 5, 6, 7, 3, 5, 2, 9, 10, 3, 8, 10, 5, 6, 5, 11, 2, 8, 2, 2, 6, 10, 6, 10, 9, 6, 8, 5, 4, 11, 5, 5, 11, 3, 6, 2, 11, 11, 3, 10, 11, 6, 6, 4, 7, 8, 11, 6, 3, 4, 5, 10, 5, 10, 2, 10, 5, 9, 4, 10, 2, 5, 6, 8, 11, 3, 7, 7, 7, 2, 5, 8, 9, 8, 9, 8, 9, 6, 5, 2, 7, 7, 10, 5, 7, 5, 10, 9, 11, 7, 2, 5, 3, 2, 5, 4, 7, 4, 5, 4, 3, 5, 6, 10, 10, 2, 6, 11, 11, 9, 4, 10, 5, 8, 6, 11, 6, 10, 7, 11, 2, 2, 9, 5, 8, 9, 8, 3, 4, 8, 2, 3, 4, 8, 9, 10, 3, 7, 4, 8, 7, 7, 6, 2, 8, 3, 5, 4, 2, 5, 7, 4, 8, 8, 10, 5, 7, 5, 3, 2, 11, 7, 11, 2, 6, 8, 2, 11, 6, 8, 3, 5, 8, 7, 5, 5, 10, 5, 6, 6, 11, 4, 5, 8, 9, 10, 10, 9, 11, 11, 4, 6, 7, 3, 2, 8, 4, 4, 3, 10, 2, 4, 6, 11, 11, 10, 5, 8, 6, 10, 8, 5, 6, 11, 2, 9, 6, 3, 9, 7, 4, 5, 4, 8, 2, 7, 8, 3, 10, 4, 11, 7, 11, 9, 10, 7, 2, 4, 10, 4, 3, 2, 6, 11, 7, 8, 11, 10, 4, 11, 4, 7, 2, 6, 7, 11, 9, 5, 5, 10, 2, 6, 2, 5, 9, 5, 8, 9, 11, 10, 3, 6, 9, 7, 10, 8, 10, 9, 4, 3, 6, 11, 11, 5, 10, 7, 5, 9, 3, 6, 6, 7, 5, 10, 11, 10, 11, 7, 6, 4, 4, 8, 6, 3, 4, 10, 3, 10, 9, 9, 10, 9, 9, 6, 5, 6, 8, 5, 9, 4, 9, 6, 4, 3, 9, 4, 3, 10, 4, 6, 2, 3, 8, 9, 8, 5, 9, 8, 11, 7, 2, 2, 9, 3, 11, 6, 2, 10, 3, 3, 9, 6, 3, 7, 10, 11, 2, 9, 2, 7, 7, 7, 2, 7, 11, 4, 11, 5, 7, 5, 6, 9, 5, 3, 8, 3, 2, 4, 4, 8, 10, 2, 6, 9, 5, 5, 6, 5, 8, 3, 11, 4, 11, 3, 3, 5, 11, 4, 2, 9, 2, 11, 4, 2, 11, 6, 6, 8, 2, 8, 4, 7, 9, 2, 6, 3, 5, 2, 9, 5, 8, 8, 2, 8, 6, 9, 7, 3, 11, 10, 10, 6, 11, 9, 3, 10, 9, 7, 9, 3, 6, 5, 3, 2, 5, 6, 7, 5, 4, 4, 2, 9, 9, 4, 4, 9, 8, 9, 11, 5, 2, 4, 11, 7, 11, 10, 7, 6, 5, 3, 11, 3, 6, 2, 3, 6, 5, 6, 6, 11, 6, 4, 9, 5, 6, 2, 10, 10, 9, 10, 4, 3, 11, 9, 2, 7, 11, 8, 4, 11, 10, 7, 9, 8, 8, 6, 2, 10, 6, 3, 9, 5, 7, 10, 10, 5, 9, 3, 7, 5, 9, 5, 11, 7, 11, 6, 7, 4, 3, 5, 4, 11, 3, 8, 4, 8, 10, 7, 11, 3, 6, 11, 10, 6, 2, 5, 8, 11, 4, 8, 3, 4, 3, 10, 10, 4, 11, 10, 11, 7, 8, 4, 5, 4, 11, 2, 10, 9, 11, 11, 6, 10, 8, 2, 3, 5, 9, 6, 2, 4, 5, 9, 7, 6, 3, 11, 4, 10, 9, 2, 7, 9, 9, 8, 4, 11, 3, 5, 9, 8, 2, 8, 5, 7, 11, 10, 10, 8, 9, 9, 2, 11, 3, 8, 3, 6, 9, 5, 9, 8, 7, 6, 8, 6, 4, 3, 6, 5, 10, 5, 2, 4, 4, 5, 2, 5, 3, 9, 3, 11, 2, 10, 5, 6, 4, 8, 8, 9, 4, 7, 5, 8, 5, 6, 4, 6, 8, 10, 9, 5, 5, 10, 7, 2, 10, 3, 5, 5, 5, 11, 3, 3, 11, 6, 11, 4, 2, 11, 6, 7, 3, 6, 7, 10, 8, 6, 5, 11, 10, 6, 5, 7, 11, 7, 5, 6, 7, 10, 6, 3, 8, 2, 11, 9, 10, 5, 2, 4, 5, 10, 10, 10, 2, 6, 5, 10, 11, 11, 3, 5, 10, 3, 9, 7, 6, 4, 3, 6, 11, 8, 9, 5, 7, 5, 5, 2, 6, 2, 8, 7, 11, 9, 3, 3, 7, 8, 9, 3, 2, 4, 4, 4, 10, 9, 11, 8, 4, 10, 8, 3, 10, 4, 6, 5, 5, 6, 2, 2, 6, 7, 10, 7, 3, 11, 7, 5, 9, 6, 8, 10, 6, 11, 7, 9, 4, 7, 6, 8, 6, 4, 7, 10, 4, 2, 9, 4, 2, 10, 11, 10, 8, 6, 4, 10, 10, 11, 11, 7, 8, 2, 11, 2, 7, 8, 8, 3, 5, 4, 2, 8, 11, 4, 4, 5, 5, 9, 6, 11, 11, 3, 7, 9, 4, 10, 11, 7, 8, 2, 6, 10, 6, 11, 5, 7, 3, 9, 2, 9, 10, 5, 10, 3, 2, 11, 3, 8, 5, 9, 10, 2, 7, 9, 6, 7, 5, 4, 4, 9, 10, 9, 10, 3, 6, 4, 7, 10, 10, 11, 6, 8, 8, 8, 7, 6, 11, 4, 9, 2, 9, 11, 6, 2, 8, 5, 2, 3, 5, 10, 2, 3, 4, 7, 11, 3, 5, 7, 11, 5, 10, 10, 2, 9, 7, 6, 3, 10, 5, 3, 2, 11, 4, 9, 5, 4, 8, 8, 8, 10, 10, 6, 6, 6, 8, 11, 7, 3, 8, 4, 5, 2, 11, 2, 3, 7, 8, 11, 2, 3, 8, 8, 6, 5, 6, 2, 9, 7, 11, 4, 7, 2, 6, 2, 5, 9, 3, 7, 3, 7, 6, 10, 8, 2, 10, 5, 2, 7, 5, 5, 4, 4, 11, 11, 9, 4, 5, 6, 9, 10, 4, 9, 10, 8, 2, 6, 7, 5, 7, 11, 3, 4, 6, 8, 6, 11, 6, 5, 8, 11, 11, 10, 10, 7, 10, 11, 2, 2, 2, 10, 6, 8, 3, 3, 8, 9, 7, 2, 8, 3, 7, 8, 3, 2, 2, 7, 9, 4, 10, 10, 4, 9, 3, 10, 7, 5, 11, 11, 4, 5, 9, 10, 5, 5, 9, 11, 10, 8, 4, 6, 5, 9, 4, 5, 5, 5, 6, 8, 10, 3, 5, 8, 7, 7, 7, 6, 5, 10, 2, 2, 11, 5, 11, 7, 8, 3, 11, 3, 5, 2, 4, 2, 2, 8, 8, 10, 2, 10, 3, 10, 6, 9, 3, 7, 5, 3, 2, 4, 9, 8, 11, 6, 2, 11, 2, 6, 9, 11, 2, 5, 11, 9, 3, 10, 6, 3, 2, 8, 4, 5, 10, 8, 8, 9, 3, 5, 7, 8, 9, 6, 9, 8, 11, 5, 10, 11, 10, 8, 7, 5, 6, 8, 4, 3, 8, 2, 6, 3, 10, 7, 2, 11, 3, 9, 3, 11, 9, 11, 8, 4, 9, 7, 6, 3, 2, 2, 2, 2, 3, 2, 3, 11, 8, 9, 11, 9, 5, 7, 8, 7, 8, 6, 5, 3, 3, 9, 10, 6, 11, 4, 4, 9, 7, 10, 7, 4, 11, 9, 11, 4, 2, 7, 5, 11, 5, 8, 6, 2, 4, 2, 11, 11, 3, 2, 3, 8, 10, 7, 2, 8, 9, 4, 9, 2, 7, 3, 4, 3, 10, 10, 10, 2, 8, 3, 3, 6, 6, 5, 4, 6, 6, 4, 7, 8, 4, 4, 11, 7, 8, 8, 7, 6, 5, 9, 6, 3, 6, 6, 11, 6, 11, 8, 2, 11, 4, 11, 3, 5, 10, 8, 7, 2, 3, 11, 4, 10, 11, 9, 8, 11, 9, 2, 7, 6, 11, 11, 9, 9, 2, 10, 7, 9, 8, 7, 7, 11, 6, 9, 2, 2, 10, 9, 7, 6, 10, 11, 3, 6, 8, 8, 8, 6, 10, 5, 7, 4, 5, 8, 9, 7, 5, 5, 6, 4, 4, 2, 5, 3, 10, 11, 3, 3, 7, 8, 6, 9, 11, 11, 8, 4, 6, 8, 6, 2, 8, 7, 8, 3, 9, 3, 7, 7, 6, 2, 5, 4, 9, 6, 6, 4, 3, 4, 4, 9, 9, 6, 4, 11, 5, 2, 9, 7, 2, 11, 6, 8, 2, 5, 4, 3, 6, 5, 2, 2, 9, 3, 4, 7, 9, 4, 8, 9, 6, 4, 6, 10, 8, 3, 10, 8, 9, 10, 10, 11, 11, 6, 11, 10, 11, 10, 2, 2, 11, 7, 3, 9, 7, 8, 9, 10, 7, 2, 8, 11, 10, 8, 3, 2, 7, 11, 3, 6, 5, 7, 7, 3, 4, 2, 2, 4, 9, 9, 5, 8, 9, 9, 9, 5, 7, 11, 10, 7, 10, 4, 3, 11, 7, 2, 4, 2, 11, 2, 10, 6, 3, 11, 11, 4, 7, 11, 2, 9, 7, 4, 8, 4, 9, 3, 5, 4, 7, 2, 10, 10, 6, 7, 8, 7, 11, 5, 4, 11, 4, 7, 7, 10, 8, 2, 10, 9, 5, 4, 8, 9, 3, 3, 2, 2, 10, 2, 9, 8, 5, 6, 5, 10, 6, 6, 8, 6, 8, 3, 8, 2, 6, 9, 10, 6, 4, 10, 10, 3, 8, 11, 2, 8, 11, 9, 2, 2, 2, 8, 5, 6, 2, 3, 9, 4, 8, 8, 6, 2, 6, 5, 9, 2, 4, 2, 4, 11, 7, 7, 8, 6, 3, 4, 10, 5, 10, 8, 5, 11, 8, 4, 7, 8, 11, 9, 6, 9, 9, 10, 5, 7, 3, 7, 2, 8, 11, 6, 2, 7, 6, 11, 9, 9, 4, 4, 10, 11, 10, 6, 4, 4, 3, 3, 9, 6, 5, 4, 6, 7, 9, 7, 7, 11, 11, 11, 5, 5, 7, 7, 8, 8, 11, 7, 6, 10, 11, 5, 8, 9, 2, 10, 5, 6, 10, 8, 2, 7, 9, 6, 3, 10, 9, 8, 2, 2, 10, 3, 4, 3, 7, 7, 2, 2, 2, 2, 4, 3, 10, 8, 10, 6, 10, 4, 9, 6, 9, 3, 9, 8, 2, 5, 5, 8, 11, 10, 11, 3, 6, 5, 4, 4, 5, 10, 6, 7, 3, 11, 3, 11, 5, 5, 9, 11, 6, 9, 10, 4, 6, 2, 9, 10, 11, 9, 2, 7, 8, 11, 2, 2, 2, 2, 8, 5, 11, 10, 9, 8, 7, 4, 8, 6, 6, 11, 10, 6, 8, 7, 8, 6, 4, 9, 3, 9, 6, 9, 3, 2, 9, 9, 3, 5, 5, 9, 11, 11, 10, 3, 11, 9, 11, 6, 6, 6, 9, 8, 5, 7, 2, 10, 4, 6, 11, 10, 8, 2, 9, 4, 5, 6, 7, 3, 2, 5, 5, 11, 4, 6, 5, 2, 10, 9, 11, 6, 8, 8, 7, 10, 6, 11, 9, 3, 6, 7, 5, 9, 11, 6, 4, 6, 3, 9, 4, 11, 3, 5, 10, 3, 9, 10, 4, 4, 2, 9, 8, 11, 2, 6, 10, 10, 2, 9, 8, 5, 4, 10, 6, 11, 11, 7, 5, 3, 6, 9, 8, 2, 4, 3, 3, 3, 6, 4, 5, 2, 5, 3, 11, 10, 6, 2, 3 };
						int an[4096];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 9) {
						static float s_mean[8192] = { -997.253, 476.974, 799.615, -109.47, -346.599, -660.817, -227.76, 962.523, 819.697, -593.616, -326.212, 154.637, -90.8536, -636.097, 813.471, -484.359, 751.885, 196.265, 692.129, 319.254, 701.346, -899.045, -771.172, 103, -13.7028, -347.026, 237.648, -42.5733, -687.613, 218.238, -846.37, 65.2181, -286.355, 618.213, -990.051, -510.056, 653.92, -956.481, 664.174, 215.613, 378.521, -781.426, -725.089, -564.867, 516.037, -184.912, -971.618, 90.0601, 929.563, 116.245, 114.963, -340.983, 818.232, -68.9413, -44.4044, -802.789, 69.0634, -241.859, 678.701, 625.355, 858.455, -800.104, 846.492, 268.41, -496.506, -373.455, -568.651, 645.314, 563.89, -909.848, 43.0616, 507.492, -806.879, -511.155, -595.508, -872.005, -259.56, -19.3793, 78.9514, 840.999, 602.161, 647.633, -553.88, -169.836, -676.931, 410.138, 929.075, -831.782, -51.4237, -809.503, -529.527, 105.93, 625.538, 327.616, -740.41, -388.958, -545.579, -759.575, -291.604, 157.384, 590.503, 342.448, -321.574, -236.061, 595.569, 205.42, -42.8785, 284.097, -960.509, 973.876, -15.4118, -55.7573, -648.183, 28.5958, 830.195, 133.335, -76.8761, -98.6663, 418.744, 441.694, -543.138, 438.337, -992.492, -49.8978, 570.788, -785.394, 912.656, 768.853, -351.665, 198.096, 485.641, -173.07, 940.733, 663.564, 339.824, 910.886, 917.844, 356.548, -172.765, 954.1, 870.174, 907.956, -235.084, 575.61, -239.418, -502.548, -588.427, 869.442, -674.734, 768.12, -305.826, 443.464, -573.962, 989.929, 684.378, 708.609, -348.857, 474.227, -674.917, -582.385, -832.636, 237.831, -592.578, 454.756, 821.589, 916.746, -203.284, -126.072, -508.164, -69.4296, 909.421, 188.757, 689.505, 896.298, -560.595, 910.825, -589.587, -707.45, -665.029, -660.878, -763.359, 373.638, -975.097, -400.739, -311.991, -666.616, 895.505, -67.8426, -176.305, -280.374, -983.52, 233.253, 93.6003, 97.8118, -881.527, -158.422, 440.168, 965.453, 818.781, -421.003, 649.831, -84.2616, 98.117, 38.9111, 954.588, -947.874, 27.6193, 125.645, 883.419, 652.15, -255.165, -156.652, 364.238, -918.821, -319.437, -665.029, -925.108, 957.274, -964.599, 171.606, 45.4421, -435.835, -493.576, 891.354, -491.134, -483.566, -736.747, 788.263, -996.582, -327.25, -824.641, -48.2498, -276.345, 300.272, -712.027, -444.624, -877.743, 488.754, -450.911, 395.978, -238.441, -185.583, 785.699, -317.301, 414.96, 254.189, -69.4296, 76.3268, -581.286, 349.651, -501.205, 977.783, 273.415, -378.826, -685.72, 67.7816, -504.624, -768.059, 486.312, -904.477, 718.497, 902.158, -363.872, -81.9422, -954.588, 72.6035, -626.514, 788.934, -350.2, -680.593, -976.745, 897.519, 626.942, 17.7313, -397.992, -841.914, 96.0417, 255.776, -955.565, -751.03, -448.469, 998.901, -319.926, 901.364, -535.325, 503.891, -40.2539, -561.693, -864.925, 982.727, 432.173, -435.774, 792.291, -483.261, -29.4504, -596.362, -52.7665, -779.168, 249.184, 790.277, -5.95111, 657.46, 469.832, -194.006, -938.536, -499.619, 338.481, -429.731, -885.678, 449.263, -708.182, -721.061, -694.693, -606.738, -26.2154, -506.882, -423.75, -174.474, -37.7514, -321.268, 510.056, -493.21, -254.677, -4.85244, -568.102, -620.106, 948.241, -665.7, 761.04, 733.451, 809.565, -389.813, 612.232, 616.077, 908.2, 297.281, -352.702, -499.435, 139.5, -779.962, 177.038, -802.118, -57.8936, -408.185, -487.899, -978.942, -225.501, -696.036, -434.858, 199.438, 841.365, 464.888, 702.322, 542.711, 70.3452, -811.09, -155.614, -645.131, 338.603, -364.116, 818.842, 598.56, 949.217, 299.6, 961.18, 308.634, 357.952, 547.166, -33.5398, 256.691, -850.398, -591.906, 991.089, -134.922, -735.099, 613.514, 3.93689, 370.586, -523.301, -115.39, -611.56, -761.101, -883.419, 472.396, -206.153, 828.608, 51.7289, 946.532, 236.732, -694.998, 911.252, -765.923, 452.681, -195.532, -437.422, 909.177, -501.999, -151.708, -161.473, -268.654, 600.94, -993.042, 513.474, 435.347, 263.466, 813.959, 529.832, -230.323, -387.188, 930.052, 474.96, -214.209, 3.14341, -442.793, 307.474, -622.852, 866.756, 102.817, -665.944, 68.5751, -184.057, 93.5392, 435.652, 54.7197, 994.69, 339.518, 544.298, 489.669, -320.292, 348.491, -630.421, 25.6661, -867.306, -10.3458, 375.408, -397.504, -298.746, -592.151, 6.19526, -513.84, 170.873, -812.25, -836.299, 558.336, -633.778, 767.632, 933.592, -144.993, 2.9603, -896.481, 561.876, 355.937, 789.117, -416.303, -318.583, -976.501, 776.788, -808.039, 788.812, -908.139, -36.5307, -77.5475, -29.3893, 605.884, 143.956, 443.098, -119.907, -789.727, 11.5665, 10.1627, 963.744, -811.274, 510.056, 632.069, 937.62, 752.983, 434.797, -516.648, 514.451, -418.439, 284.646, -430.83, -620.411, 609.424, 363.262, 638.844, 418.012, 66.3778, -869.015, 350.139, 190.71, -233.314, -193.884, -316.63, -338.847, -352.214, -783.502, 605.518, -827.998, 559.618, 901.364, -16.0222, -328.166, -696.768, 462.386, -248.878, 191.137, -79.4397, -834.162, -682.18, -430.525, -767.266, -206.336, 238.014, 431.867, 629.749, 745.415, -877.804, -584.948, 108.982, 389.447, 802.24, -355.51, 564.745, -335.185, -255.348, -539.354, 327.189, 932.615, -284.829, -604.236, -48.8601, -922.422, -307.291, -599.902, -744.621, 859.981, 33.2957, 318.827, 611.866, 516.404, 884.274, -381.634, 484.298, -387.799, 848.201, -636.891, -782.342, -857.418, -102.878, 442, -922.91, -115.879, 536.607, -955.321, 536.973, 996.582, -780.511, -611.255, 819.819, 874.142, 10.4678, 695.853, 685.72, 247.475, 499.619, 418.073, -529.038, -465.255, -33.1126, -284.829, 377.606, 443.525, -204.138, -819.452, 11.9327, -177.709, 138.096, 818.049, 174.047, -238.197, 826.838, 268.349, -103.427, -52.9496, 541.49, -714.164, -266.03, 258.339, 221.229, 464.949, 301.431, -376.812, 729.545, 216.712, -935.972, -882.199, 767.937, -4.73037, -624.195, -681.204, -959.96, -225.196, -231.361, -95.7366, 666.005, -224.403, -508.469, -494.186, -154.088, 441.023, 299.6, -189.856, 774.651, 663.747, 493.149, -89.7549, 805.109, 262.551, -793.39, 907.224, -136.876, 708.792, 947.935, 761.773, 110.813, 102.939, 387.066, -604.175, -139.012, 401.227, -604.419, 44.1603, -258.705, 892.209, 335.673, 855.586, -892.087, -76.9372, -401.227, 744.987, 710.135, 876.705, -604.846, -505.417, -396.283, 397.626, -207.373, 105.747, 315.897, 869.259, -994.263, -599.78, 258.4, -860.286, -65.0349, 871.944, -607.593, -500.961, 955.443, -211.463, 142.064, 905.454, 956.236, 542.833, -441.328, 461.837, 629.566, 254.555, -265.053, -857.601, 9.55229, 621.693, 468.795, -230.567, 261.513, 22.248, 177.526, -760.43, -279.214, -134.373, -938.047, -999.573, 182.104, -805.597, 520.432, -222.694, -561.632, -464.339, 812.738, 549.669, 69.9179, 2.83822, 974.425, -222.51, 483.383, 646.901, 980.59, 878.292, 201.086, 58.1988, 160.314, -46.7849, -600.391, -782.037, -139.683, -236.061, 699.454, -400.006, 359.233, -40.9253, -796.564, -478.133, 31.7698, -449.263, 786.493, 746.818, 762.505, -358.806, 633.961, -664.907, -389.996, 399.213, 31.3425, 745.903, 721.305, 286.843, -597.095, -811.701, -504.746, -463.973, -399.091, -776.482, 997.314, -125.034, -555.345, -384.014, 622.974, 504.685, -490.402, -564.989, -987.854, 325.724, 166.295, 406.537, -786.554, 414.716, -130.467, -485.641, 97.7508, 750.786, -302.53, -147.801, 22.5532, -807.794, 140.538, -275.369, 746.086, 703.116, 140.599, -0.824, -408.979, 829.768, 379.986, -292.337, -140.782, -572.741, 820.612, -102.329, -505.051, 636.158, 492.965, -265.725, -0.396741, -891.049, -464.827, 851.619, -404.645, 486.801, -517.808, -45.7472, 639.637, 125.645, -195.288, -331.095, -711.417, 73.2749, 698.233, -353.862, -588.122, 295.816, 122.166, -88.4732, 756.218, 153.844, -911.069, 467.513, -640.675, 129.551, -796.93, -854.976, 214.637, -197.058, 450.179, -295.206, 184.729, 78.4631, -764.153, 189.184, -834.284, -338.481, 675.222, -476.608, -498.703, -214.698, -868.587, 367.473, -353.496, -813.105, -994.873, 217.444, 82.2474, -665.639, -226.6, 757.317, -521.104, -97.1404, -721.366, -539.842, -889.096, 501.45, -732.047, -559.862, -479.72, 649.586, -817.988, -446.333, -569.018, 3.57067, 194.128, 751.274, 912.961, 833.491, -500.656, 678.884, 796.136, 749.321, 853.45, 529.71, 334.513, -322.367, -637.867, 145.604, -879.818, -911.557, 520.127, -631.397, -752.434, -528.611, -500.168, -357.28, -272.744, 62.4714, 691.092, -134.922, -849.849, 820.551, -510.605, 487.899, 601.55, 653.981, 335.795, -130.161, 825.19, -102.817, -994.69, 877.682, -362.712, -962.889, -853.511, -262.246, -409.162, -310.038, -929.991, -862.423, -110.874, -172.46, 788.141, -422.468, 550.462, -746.086, -696.646, 972.106, 821.711, -667.959, 148.045, 501.755, -157.445, 960.387, -827.021, 714.469, -440.291, 799.127, 586.596, -123.02, -109.287, -252.846, -755.974, 576.891, 138.218, -331.584, -813.593, 573.656, 653.859, 368.511, 429.548, -99.5209, 752.373, 821.162, 3.14341, 215.979, 875.851, 645.009, -760.063, 405.377, -992.615, -170.141, -685.232, 432.295, -664.296, 713.675, -966.43, -559.618, -831.233, 208.533, 872.311, 104.77, 97.9339, 277.932, -733.818, -766.533, -587.939, 662.648, -851.07, 619.373, -238.258, -142.369, 741.447, 141.331, -926.939, 338.786, 590.991, -883.969, 711.905, 39.0942, 860.408, -100.436, -102.268, 75.3502, 867.122, -261.269, 368.023, 724.845, 872.005, 872.86, -707.999, -348.186, 118.686, 885.189, 435.041, 384.014, -877.377, -695.486, -427.839, -960.753, 209.876, 456.954, 152.928, -697.989, 306.558, 567.064, -785.333, -349.956, -22.6142, -890.255, -452.986, 830.805, 715.506, 411.725, 105.93, -411.969, 480.636, -496.994, 677.664, 638.6, -511.093, 218.726, 792.718, 898.495, 251.625, -449.812, 890.255, -40.0098, -135.96, 247.047, -268.166, 817.927, 545.03, -331.523, 65.5843, 846.187, 425.581, 884.823, 540.33, -975.585, 588.244, 29.1452, -130.528, -585.376, 355.998, -101.108, -754.692, 887.57, 831.294, -192.724, -997.131, -900.754, 685.293, -314.92, -465.865, 718.009, 196.814, 431.928, 452.01, -99.6429, -458.724, 518.113, 4.30311, -981.933, -669.912, 835.994, 738.09, -365.398, -600.635, -198.462, -586.657, -268.532, -150.792, 706.29, -832.514, -814.875, 332.682, -681.936, -70.8335, -506.699, -405.255, -266.518, -917.966, -248.512, -738.273, 478.988, 379.62, 879.94, -119.968, 862.423, -891.598, 99.6429, -794.916, -425.336, -497.177, -340.678, 740.959, -458.602, 408.551, -188.147, 528.733, -975.646, -802.973, 40.7422, 186.132, 973.083, -70.5283, 536.851, 196.326, 626.331, -229.286, -75.1061, -553.148, -636.341, -346.538, -760.186, -980.895, 807.428, -807.672, 153.844, 227.088, -826.472, -152.074, -547.655, 906.308, -126.194, 340.739, -238.868, -46.7238, 366.863, 315.409, 522.385, 114.414, -739.738, 254.677, -778.314, -975.219, -338.359, -356.426, 864.315, 497.726, 780.633, -616.749, -298.196, 63.448, -220.069, 455.367, -791.253, 758.293, -122.532, 835.688, 309.244, -836.238, 697.623, 658.742, -146.886, -304.361, 872.555, -368.755, -656.972, -857.051, 894.406, 696.89, -89.1446, -542.894, 216.285, -50.4471, -456.099, 790.948, 604.053, 798.151, 561.998, 787.713, -262.001, 380.169, -656.423, 120.212, -935.789, 225.99, 302.896, 279.885, -103.854, -827.143, 115.635, -384.198, -233.619, 529.099, 196.387, -376.08, -972.106, -512.253, -575.365, -514.267, -294.046, 791.253, -878.353, 857.784, 599.414, 644.948, 276.894, 910.398, 632.313, 881.405, -975.585, -383.038, 455.611, -625.416, 369.854, -707.083, 298.624, 703.665, 166.601, 384.747, 70.5283, -932.798, -657.46, -490.951, -287.942, -552.721, 376.69, -258.095, -203.04, -976.562, 25.4219, -842.647, 383.648, 836.848, 299.478, -382.977, 116.672, -295.938, -309.854, -191.076, -421.613, 381.695, 646.229, 536.363, 890.378, -20.1117, -700.003, 257.424, -434.919, 626.698, -555.467, 873.653, 607.532, 382, 759.636, -230.689, 866.573, 401.715, -711.478, 100.375, -363.323, 351.665, 164.892, -666.311, -490.524, 71.8101, 588.733, 35.7982, -524.827, -509.751, -946.715, 912.29, 274.392, 787.286, 821.833, 96.6521, -0.701926, 809.32, -346.355, 689.444, -54.2314, -266.701, -218.787, 547.411, 714.286, -19.5013, 136.265, 322.245, 561.998, -619.495, 826.594, -249.855, 4.12, 585.681, -137.425, 190.405, 665.456, 734.611, 133.946, -585.253, 381.939, -633.229, 635.853, 646.29, 552.293, 586.413, -54.1704, -151.463, -260.475, -523.85, -49.6536, -735.221, -477.462, 761.834, -178.869, -793.329, 597.766, -274.087, 242.042, 93.7223, -869.625, -627.979, -114.78, 741.997, -643.666, -161.29, 138.096, 903.378, 433.638, 63.448, 79.6838, 54.6587, -456.099, -303.018, -292.764, 436.689, -755.913, -732.658, -587.268, 342.936, -932.005, 593.738, -290.445, -340.739, -622.425, -549.547, -791.131, -347.575, -534.959, 481.674, 846.492, -990.539, -96.2859, 364.605, 359.05, 126.621, -849.666, -775.933, -989.135, -351.726, 396.039, 296.548, 570.544, -743.583, 668.264, -155.919, 537.156, 709.281, 181.982, -681.509, -382.916, -869.442, -58.1988, -224.769, -549.181, 548.631, -407.086, 537.645, 588.977, -314.615, -476.913, 110.935, 719.169, -4.60829, -785.943, -653.615, -857.479, -45.1369, -156.835, 855.098, -499.496, -398.968, -865.291, 672.292, 750.969, -679.312, 461.837, 171.422, 394.94, 889.828, 197.851, 228.004, -46.5407, 762.2, -682.119, -366.008, 693.716, -391.339, -749.565, -238.502, -394.147, 106.235, -20.9662, -522.935, -370.525, 977.111, 393.475, 305.704, 130.345, -722.465, 891.842, -155.065, -373.455, 937.315, 231.666, -571.154, 765.496, -451.643, 631.825, 42.1461, -449.751, -595.874, -876.278, 484.298, 816.34, 788.629, -846.675, -216.651, -42.2681, -484.848, 432.356, -139.622, 904.172, 715.873, -960.204, 443.342, 347.209, 880.49, -226.051, 230.934, 216.895, 145.421, 753.41, 307.047, 408.429, 832.942, 268.96, -195.41, -241.31, 240.15, -114.841, -180.334, -727.409, -322.001, -208.228, -424.543, -399.823, -239.418, 405.255, 836.482, 762.383, -56.1235, 220.557, 572.558, 128.391, 314.982, -87.4966, -127.049, 641.652, -368.755, 687.49, -889.828, -703.299, -390.79, 884.884, -645.436, -134.251, -536.363, 828.608, 37.8124, -124.973, 696.829, -71.0166, 867.244, -168.432, 137.181, 33.9061, -10.1016, 699.637, -771.111, -969.481, 406.781, -372.784, -429.426, 697.134, 636.341, -76.9982, 704.398, 804.193, -370.769, -717.582, 155.248, -533.86, -68.0868, 264.382, -280.251, -256.752, -899.167, -981.994, 5.34074, 492.66, -633.29, 259.133, -780.877, -529.771, -451.949, 496.628, 207.495, 682.119, -784.112, -490.89, -255.654, -13.7028, -892.636, 71.8711, 71.0776, 944.151, -210.059, -67.3544, 684.927, 921.812, 575.488, 39.0332, 378.277, -558.031, 684.317, -985.595, -472.152, 785.638, 659.23, -115.146, 633.656, 843.135, 29.6335, -944.578, -892.026, 215.918, -774.468, -462.752, 314.615, -431.44, -258.278, 423.322, 857.784, -770.44, 551.744, 615.711, 121.189, -956.481, 771.722, 801.874, 57.2832, 472.396, 574.999, -318.949, -773.98, 947.081, 355.632, 441.389, -915.769, 469.039, 142.796, -514.389, 524.461, 462.02, -735.221, -591.418, 664.113, 873.226, 262.917, 223.06, -115.574, -474.837, -944.334, 836.665, -880.184, -282.632, -647.084, 778.375, -603.381, 77.4255, -680.105, 175.024, -377.728, 663.015, 491.012, -21.9428, 624.378, -686.758, 934.202, 439.741, -560.289, 250.465, 665.029, -160.009, -146.519, 931.394, -689.505, -537.034, -811.823, -851.07, 23.4077, 823.542, -657.338, 384.075, -354.045, -856.38, 81.3929, -593.31, -875.546, -920.713, 573.229, 127.781, 172.765, 696.829, -819.33, -299.295, 186.804, -300.577, -501.022, 872.799, -591.723, -95.9807, 645.07, 586.962, 339.152, -32.5632, -203.406, 833.247, -594.897, 363.262, 981.14, -249.672, 870.357, 608.264, 115.207, 977.233, -999.084, 738.945, 293.252, -745.415, 479.476, -961.913, -673.269, 276.711, -931.394, 11.6886, -466.659, 792.535, 525.681, 614.856, -224.219, 957.579, -233.802, 728.874, -248.939, 935.545, 278.359, -112.461, 529.466, -250.343, -213.172, 253.945, -656.362, 384.32, 167.028, -153.294, 627.735, -863.399, -391.156, 16.9378, -693.899, -781.915, -968.749, -825.678, 332.987, 606.128, 745.781, 764.275, 480.941, 145.421, -629.078, -506.272, 380.413, -100.314, 996.216, -191.321, 808.466, -246.742, 596.057, 619.739, -772.088, 262.673, -306.07, 279.458, 576.586, -242.103, 276.65, -191.565, 305.948, -305.582, 958.068, 372.234, -329.264, 254.311, -606.922, -992.553, -732.353, 198.157, 942.503, 598.926, 824.335, 966.491, -414.167, 105.747, -761.223, -968.932, -913.45, -961.974, 868.16, 497.177, 405.438, 183.63, 67.7816, -45.7472, -635.243, 584.46, -19.3793, -421.857, 562.609, 763.054, -424.177, 482.528, -463.363, 199.316, 938.108, 320.17, -821.65, -942.991, 806.635, -865.169, 396.954, 160.314, 364.299, -537.278, -966.369, -779.778, -723.441, 683.95, -974.425, -688.528, -19.2572, -375.774, -180.212, -401.288, 224.83, -593.554, 37.5683, 313.395, -143.406, 552.171, -611.866, -20.1117, -136.998, 333.048, 833.125, 892.758, -341.288, 372.784, -999.817, 842.586, 625.599, 498.154, 847.591, -907.102, -98.4832, 841.182, 577.746, 606.922, 419.599, 190.161, 801.508, 523.606, -802.85, 591.601, -47.5173, 884.64, -125.278, 201.392, -49.4095, -389.203, -494.675, 386.883, 116.489, -271.34, -406.659, -921.995, -664.418, 847.102, -616.443, 369.06, 472.03, 883.602, -599.597, 370.159, -967.467, 41.2915, -763.115, -691.153, 81.2098, -964.843, -406.354, -118.137, -597.339, -431.196, 568.102, -143.895, 916.684, -939.268, 193.396, 921.018, 342.143, -378.765, -772.454, -210.669, -889.035, -721.122, 192.602, -450.606, 532.456, -543.199, 663.381, -733.879, 206.946, 699.698, 806.879, -167.15, 690.298, 650.014, -828.852, 72.9087, 407.514, 274.697, 724.54, -909.055, 817.316, 341.716, -855.647, 874.996, -174.291, 146.641, -643.483, -852.718, -549.486, 489.303, 324.076, 466.659, -265.114, 900.021, 86.52, -420.698, 911.985, -883.541, -951.17, -979.003, -181.616, 810.358, -479.354, 968.322, 558.885, -436.506, -494.919, -659.841, -580.187, -905.21, 738.029, 149.632, 33.723, -263.222, 976.867, 105.136, 792.962, -838.069, 540.025, -537.645, 513.962, -640.614, -904.355, -874.325, 351.482, -138.401, 700.919, -0.274667, -37.6293, -779.351, -909.116, 590.747, -80.3552, 735.16, -782.342, 858.577, 376.751, 426.374, -960.509, 317.85, -18.4637, -734.794, -440.229, 389.813, -959.838, -351.36, -883.908, -996.338, 931.516, 126.072, -80.5383, -551.744, -229.225, 376.141, 843.074, -526.353, 324.076, 433.21, 461.592, 642.689, 171.972, 163.793, 340.19, -67.6595, 123.875, 993.53, 573.656, -414.411, 882.138, -116.916, -424.848, -787.164, -44.2213, -444.624, 115.757, 143.529, 825.007, 971.801, -386.883, 27.5582, 354.778, 58.6261, 670.827, -313.028, 459.334, 684.072, -132.908, 217.566, 15.9001, -115.696, 794.305, 340.19, -971.007, -659.536, -652.455, 2.77718, -955.565, -894.955, -735.71, 231.483, -186.682, -421.735, -87.1914, -767.693, 205.969, -265.175, 331.645, 742.912, -623.646, -399.335, -481.429, 633.9, 176, 979.125, 969.787, -239.479, 817.743, 427.595, -531.48, -935.728, 790.277, 484.481, 124.79, 924.375, -978.149, -854.732, -336.161, 759.209, 294.656, -410.688, -34.5164, 878.231, -91.464, 156.346, -146.764, 583.178, 595.386, 573.168, -866.817, 519.883, -317.301, 429.914, 883.358, -720.389, 508.469, -92.9899, -7.84326, 251.015, 84.3837, 751.396, -51.7899, 721.061, -989.624, -496.75, -121.067, -772.881, -361.248, 972.655, -160.314, -841.426, 487.777, -34.2112, 352.947, 607.898, 906.186, -738.09, 197.302, 472.518, -12.8483, -160.192, 446.333, 806.513, -267.8, -645.985, 758.049, -578.112, 465.499, 861.934, -86.3369, 740.349, 497.543, -200.11, 606.8, 894.772, 39.7046, -789.85, 482.406, -919.431, -632.069, -782.891, -574.023, -842.097, -763.054, 754.082, -897.519, -497.604, -455.55, -460.433, 745.659, -892.697, 244.728, 304.361, -803.339, -65.2181, 513.169, -732.475, -806.452, -482.406, 160.253, 863.399, -89.816, -189.367, -704.581, 717.765, -254.86, 292.459, 241.371, 874.752, -638.966, -683.218, -302.286, -726.737, 973.51, -285.684, -844.356, -526.475, -110.08, 644.032, -438.398, 633.717, 299.173, -305.338, 231.3, -263.71, -689.932, -763.176, 13.8859, 64.9739, -759.331, -976.135, 706.168, -69.1244, 157.445, -956.114, 716.483, -350.688, 664.052, 621.204, 978.698, 766.533, 940.672, -232.276, -883.602, -464.4, 32.9295, 784.783, -142.918, 643.91, -609.058, 114.536, -303.995, -811.09, 658.681, 441.633, -189.856, 905.271, -287.698, -955.382, -777.398, 640.858, 950.255, 397.748, 216.468, -916.074, -168.737, -977.783, -1.25126, -260.72, -335.734, 588.061, 614.917, 154.942, 647.023, -734.611, -887.814, -601.306, -844.905, -673.757, -45.4421, -215.369, -718.558, 996.094, -314.188, 849.788, 624.805, 555.162, -155.431, -419.355, 329.203, 690.237, 127.598, 889.767, -627.186, -903.806, -195.654, 342.143, -892.697, -672.475, -619.312, -867.855, 46.6018, -100.009, 513.352, -690.359, 905.881, -358.135, 173.559, -743.034, -109.104, -704.093, -938.78, -584.826, 237.159, -629.322, 269.509, 260.72, -1.55644, 242.286, 197.913, 532.09, 86.4589, 505.6, -282.754, -293.863, 559.435, -423.75, -416.791, 725.028, -838.496, 541.978, 100.62, 972.411, 111.728, -735.954, -962.706, -499.802, -857.051, -270.852, -211.463, 505.112, 791.009, -349.345, -209.449, -944.823, -66.1946, 320.414, -814.875, -933.653, -419.66, 710.074, -266.64, -470.992, 909.909, -177.16, -20.2948, 984.619, -439.314, -705.008, 666.311, 252.052, 169.591, 247.536, 396.649, -595.447, -839.412, 387.371, -449.934, 996.033, -215.857, -640.675, 445.54, -462.447, -593.86, 347.636, -244.301, -775.872, -952.818, -72.7866, -828.852, -88.7783, -828.73, 165.563, 314.249, -658.925, -274.758, 973.998, -96.6521, -380.413, 427.534, 709.464, -833.186, -57.2222, -692.679, -973.876, -492.843, -426.801, -506.149, -23.2246, 414.96, -933.348, 255.165, -635.609, -367.168, -251.198, 287.271, 343.852, -429.975, -558.458, -746.879, -726.798, -34.2112, 439.802, -427.9, -184.24, 803.827, -262.246, 458.357, -709.037, -523.606, -494.247, 210.547, -723.38, 198.157, -183.264, 750.481, 460.005, -329.569, -377.178, 412.397, -260.537, 995.117, -18.2806, 981.872, -423.627, -161.29, -466.536, 984.985, -403.79, 846.736, 227.027, -772.576, 440.413, 307.291, -703.421, 583.544, -132.298, -934.629, -511.46, 35.0047, -894.101, -312.845, -462.447, 960.692, -546.495, -126.438, 893.551, -325.175, -869.015, 124.729, 697.989, 216.895, 382.427, -346.11, -989.38, -610.096, 827.387, 551.5, 286.05, 50.1419, -938.108, 199.133, -900.021, 645.863, 510.788, -66.0726, 126.255, -993.713, -471.603, -37.7514, 363.018, 380.963, -249.123, 523.911, -390.484, -271.401, 697.256, -693.716, -807.428, 256.203, 621.265, -891.659, 943.236, 208.167, -387.31, -245.399, 423.017, -94.821, 949.278, 259.316, 401.044, -966.43, 697.928, -759.514, 580.065, 184.118, -433.638, -415.937, -426.801, 641.285, -995.056, -642.384, -869.32, 422.04, -442.183, 974.975, 616.199, 565.172, -535.264, 609.668, -266.518, -329.63, -487.289, -140.416, -496.994, 687.002, 137.303, -339.518, -511.643, -46.6018, 774.224, 450.606, -726.981, 486.007, -538.804, 584.216, 186.682, -481.918, 689.322, 995.972, -718.009, 289.163, -495.712, 114.902, -438.948, 83.8954, -141.209, -971.496, 452.132, 521.165, -786.859, 896.97, 535.752, 93.0509, -612.476, 616.688, 521.958, 954.222, -336.772, 734.916, 476.241, -39.5825, 134.251, -386.456, -703.543, -41.7798, -691.092, -156.652, 439.497, -676.504, -38.5449, 907.834, -102.817, -249.184, -766.472, 97.2015, 423.75, 292.459, 985.839, -327.067, 11.8107, -352.702, 399.396, 281.411, 618.946, 700.125, 827.692, 617.908, -855.525, -689.505, 897.58, -658.986, 656.117, -648.061, -138.707, -763.482, -134.007, 1.00711, -801.996, -368.633, 857.906, -723.869, 465.072, -942.076, -936.705, -173.009, -600.269, 167.943, -511.765, 973.449, 270.974, 896.542, -810.236, 994.446, 480.575, -123.692, 2.53304, -332.743, -584.887, 101.535, -484.725, -943.297, -452.742, 842.219, 297.586, 553.819, -212.5, -192.297, -623.89, 165.136, -914.487, -438.52, -38.4838, 51.7899, 281.961, 184.729, -575.182, 318.522, 209.815, 190.344, -475.997, -947.447, 46.2355, -312.601, 880.612, 613.269, -86.3369, -205.054, -293.924, 170.385, -268.044, 305.948, 778.802, 493.637, 319.498, -398.358, 670.156, 991.821, 744.865, 815.485, -778.741, -998.596, -71.5049, 247.047, -730.094, -673.818, 494.247, 26.3375, 626.392, 26.0323, 870.907, -972.289, 300.699, 455.794, -356.365, 34.0281, 978.82, -445.357, 155.797, -849.422, -250.832, 385.113, -639.76, -530.686, -920.53, 922.056, 921.201, 982.788, 43.3058, -908.444, 565.355, 748.039, -203.894, 60.5792, -316.08, -698.843, -697.501, -224.769, -816.034, -678.945, -687.551, 750.481, -410.077, 543.321, 587.756, 108.737, -604.785, 924.68, 378.887, -130.65, -336.161, 56.6118, -848.811, -512.68, 882.015, 830.073, -437.971, 642.689, 59.3585, -651.357, -313.395, 279.092, 81.0877, -906.369, 495.102, -106.54, -691.092, -662.709, -421.064, -925.413, -566.82, -192.785, -872.494, -707.511, -425.947, -399.579, 154.759, -62.7766, 633.106, 347.453, 975.402, 608.142, -105.014, -373.821, -68.1478, -347.209, -120.823, 626.27, 370.83, 956.359, -558.885, 62.7766, -130.406, 980.102, -876.034, 340.739, -287.271, 50.8133, 760.491, -31.3425, 618.702, -99.8871, -676.077, 223.121, 716.239, -547.044, -969.848, 62.4104, -228.858, 867.061, -310.831, -709.22, -337.199, -178.259, -693.838, -27.3751, -889.462, 57.3443, 933.897, 96.3469, 650.502, 163.793, -226.6, -511.643, -138.157, 620.228, 526.231, -22.5532, 843.684, -916.562, 995.605, -191.443, -58.7481, -72.4204, -61.861, 306.986, -277.017, 32.6853, -834.59, 792.413, -793.451, 824.03, -104.465, -257.057, 335.795, 561.083, -645.497, 209.265, 706.961, 876.644, -371.868, -521.47, 545.579, -434.126, 763.359, 988.647, -67.1712, -374.615, 119.785, 492.111, 282.266, 542.161, -28.6569, 470.199, 66.2557, -167.272, -642.079, 797.113, 792.413, 832.392, 186.804, -346.232, -769.524, 733.879, -415.815, 692.19, -111.24, 876.827, 496.994, 449.324, -259.621, -713.492, 543.992, 11.1393, -799.554, -536.668, -882.138, 926.939, -647.938, -578.661, 736.625, -627.064, -576.83, 977.66, 644.765, 566.942, 513.169, -708.67, 665.334, -568.285, -431.013, 652.821, -698.721, -490.219, -387.494, 522.385, 87.2524, 126.743, 216.285, -180.273, 190.527, -193.945, 230.567, -537.217, -736.625, -67.5375, 759.88, -843.074, 544.176, 666.494, 389.203, 433.882, 208.838, -891.659, 661, -59.2975, 972.839, -288.003, -198.523, 979.247, -57.7105, 949.278, 697.317, -705.252, 716.056, 223.731, -877.987, -884.701, -263.405, -965.026, 212.867, -559.374, 892.392, 392.804, -803.095, 581.286, 368.511, 480.697, 368.206, 947.569, -250.893, 570.605, 313.578, 88.7173, -631.581, -813.471, -609.973, -17.8533, -87.4355, 518.784, -549.242, 572.192, 270.974, -912.168, -178.198, -440.962, -362.041, 658.864, -471.297, 438.581, -135.533, -594.836, -226.905, 790.399, 59.3585, -677.236, 659.108, 817.621, -558.824, -105.808, 170.019, 135.777, 439.741, 2.59407, -605.152, 811.762, 47.4563, 95.7976, 5.03555, 652.821, -578.967, 600.391, -19.4403, 745.903, -266.701, 585.864, -412.946, -753.533, 274.148, 678.518, -397.992, -582.263, 802.728, 225.562, -208.533, -80.3552, 148.656, 709.403, -363.567, -652.333, -936.399, -588.733, -690.176, -977.477, -676.809, -674.306, 902.585, 28.6569, -922.117, -675.649, -99.5819, 503.403, 455.367, 570.238, 910.459, -823.786, -588.672, -818.537, -362.346, -842.219, -168.432, -854.122, -888.546, -21.2104, -116.55, -515.549, 162.511, -120.762, 617.176, -334.208, 148.656, 347.27, 817.804, -729.911, -67.7206, 908.628, 106.418, -96.7742, -893.796, 165.258, 291.91, -639.393, -529.283, -915.342, 972.472, 41.5357, 759.88, 539.048, -612.903, -386.883, 618.091, -906.247, 517.38, -75.6554, 826.716, -952.574, -561.388, 170.08, -219.275, 719.962, -77.9138, 305.338, -784.783, 992.187, -714.286, 709.098, -818.232, 94.76, -503.525, 98.4222, -724.662, 647.389, 521.409, -307.291, 110.569, 947.569, -70.6503, -372.356, -951.659, 509.934, 742.668, -381.024, 172.826, -144.749, -461.531, -122.898, 517.136, 549.425, 800.47, 270.791, 540.757, -507.431, -479.72, -258.705, 855.647, -541.978, 49.4705, -33.6009, -797.113, -118.32, -799.615, -725.394, -320.536, -99.3377, -401.593, 809.137, 214.331, -357.402, -716.849, 941.771, 295.572, -235.206, -439.985, -62.6545, 946.471, -757.073, -898.068, 867.55, 952.33, -589.465, 300.76, 774.895, -663.015, 247.841, -131.077, 86.9472, 89.4498, 920.835, -309.061, 280.374, -859.859, 570.299, -74.8009, -204.993, -411.908, -648.976, 355.083, 197.913, -823.359, -279.214, -9.12503, 597.217, 20.9052, -20.6, 921.262, 306.497, 583.483, 57.6495, 414.838, -241.188, -98.2391, -443.403, -728.385, -329.508, 64.0584, 36.6527, 424.909, 455.55, 936.155, -443.831, -207.617, -110.691, -861.873, -538.743, -622.12, -976.989, -729.24, -575.793, 623.341, 347.453, -607.898, 732.902, 9.49126, 207.251, 743.95, -170.507, 908.383, -535.264, -271.523, 100.131, -315.836, 414.289, 135.105, -86.581, 45.1979, -53.9262, -383.587, 909.665, -354.595, 960.387, 415.387, 540.025, 935.667, 31.4646, 341.35, 855.525, 265.603, -678.213, -2.89926, -453.963, -764.824, 746.086, 255.104, -770.501, 296.06, 385.174, -15.8391, 990.967, 603.32, -849.666, 25.9713, 171.239, 116.916, -302.774, -578.234, 406.659, -46.6018, -410.199, -111.301, -82.3695, 987.365, 714.713, 546.556, 47.9446, 702.872, 945.128, 894.284, 53.6821, 295.267, 54.6587, 36.5307, 142.186, 22.7363, -118.015, 160.253, 761.65, 807.672, 744.316, 261.208, 139.378, 88.229, 101.596, 120.518, 71.6269, -79.8059, -529.16, 531.358, -37.8124, -671.682, -410.016, -224.647, -888.546, 907.834, -168.126, 349.284, -744.133, -324.015, -131.504, -58.504, -643.483, 173.925, -415.082, 658.62, 638.539, -182.043, -756.462, 502.06, -120.09, 501.205, -102.145, -905.576, 950.743, 445.112, 56.7949, -343.486, 602.222, 301.065, 598.56, -766.717, 505.783, -498.093, -65.096, -829.096, -458.113, 863.826, -574.877, -156.957, 339.579, -562.914, -960.082, -372.539, 65.0349, -391.339, -724.174, 132.908, -539.171, 452.986, -681.082, -324.992, 135.166, -666.189, -540.88, -996.46, 100.742, -244.057, -245.888, -721.366, -436.262, -533.616, 943.541, 297.281, 963.195, 806.33, -387.127, 969.97, -740.287, -687.674, -392.254, 47.3952, 474.837, 568.224, -302.225, -903.195, 479.781, -693.594, -509.262, -271.157, 34.8216, 759.758, -403.485, -926.267, 78.2189, 262.612, 41.4136, -491.928, -734.916, -897.153, 932.005, 631.397, -243.08, 623.402, -630.909, -507.309, -739.799, 573.473, 612.171, -933.042, -684.378, -289.529, -506.943, 981.872, -162.694, 651.418, 80.2332, 494.064, -98.3612, 974.364, -407.208, -283.914, 786.37, 570.299, 564.562, -907.346, -740.287, -755.608, -741.997, -872.616, -880.306, 567.858, 328.593, 359.233, 982.665, -21.0273, -261.513, -410.871, 322.672, -864.559, -79.0124, 356.548, -239.479, 515.305, -805.78, -334.025, -397.076, 268.654, -574.45, 963.744, 631.397, -680.593, -54.5976, -635.853, -858.882, 716.727, -288.675, -873.592, 230.628, -763.604, -277.81, -618.824, -659.719, -767.144, -317.728, 106.784, 41.5967, 740.287, 915.098, 356.12, 76.2658, -175.756, -472.274, 733.146, -354.9, -816.279, -175.329, 119.297, -52.5834, -998.047, -27.8634, 650.319, -30.2438, 48.616, -88.229, 942.503, -475.021, -969.115, -474.96, -430.464, 541.612, 195.593, -952.818, 788.934, 602.649, -820.002, -268.777, 518.54, 734.611, -885.861, 762.139, 563.219, -553.209, 971.984, 420.576, -908.75, -979.797, 172.887, -502.365, -638.722, 46.6018, -566.576, 958.19, 698.355, 217.566, -130.955, -144.81, -279.336, -636.341, 345.073, -505.905, -183.569, 743.522, -921.079, -153.661, 870.113, 582.568, -508.713, 213.843, -761.04, -137.547, 307.84, 142.796, -20.1727, 360.149, -89.4498, 587.878, -674.429, 936.583, 939.756, -673.696, -588.488, 76.1437, -854.915, 621.937, -747.673, 675.649, 487.716, -861.019, -31.7698, -916.99, 491.195, 106.723, 261.757, 387.066, -156.407, -267.067, 144.322, -44.2824, 248.817, -131.687, 912.717, -774.102, 528.245, 920.53, -624.317, -258.095, 972.167, -982.116, 330.546, -555.04, -58.6261, -575.915, -710.44, -788.079, 718.986, 201.697, -543.138, -355.327, 611.621, 126.438, 954.711, 108.798, 301.187, 851.192, 299.966, 23.0415, -553.575, -679.8, 131.077, 398.053, 534.532, 433.027, -815.79, -374.248, 982.421, -41.5357, 520.188, -481.918, -511.399, -301.126, 0.640889, -403.79, 634.816, 904.233, -985.107, -370.281, -262.978, -64.1804, -543.504, 425.52, -52.2172, 180.761, 949.583, 866.756, -392.804, 215.796, 325.602, -797.784, -155.37, 221.473, -650.136, 585.314, 430.525, 646.107, 698.111, 862.789, -122.593, -880.062, 182.836, 614.307, -207.862, -380.84, -769.585, -558.885, -522.996, -120.09, -392.926, 505.356, 781.426, -937.559, 395.734, -681.143, -796.686, -334.758, -777.581, 782.708, -758.782, -311.197, -266.762, 475.448, -4.79141, -950.072, -679.739, 193.152, -981.323, 245.949, -901.242, -262.246, 420.27, -731.742, 133.274, 956.359, -785.333, 767.022, -85.4823, 30.3659, -74.9229, -535.447, -914.426, 14.4353, -566.027, -179.968, 419.05, -374.371, 85.2382, -56.7339, 42.8175, 282.327, -964.721, -241.493, 917.356, -606.555, -144.932, -580.981, 561.815, -102.512, 381.268, -29.2062, -969.176, 51.4237, 817.988, 266.762, -859.066, -529.954, 88.3511, 130.65, 947.386, -585.192, 436.079, -228.675, 470.077, -667.104, 276.955, -553.209, 671.01, -353.496, -699.515, -622.059, 680.227, 791.559, -178.503, -86.8252, -727.531, 507.065, 381.39, -58.0767, 594.043, -625.965, -399.884, -441.023, 138.829, 604.663, 277.566, 978.637, 975.524, 81.2098, -254.982, -301.614, 223.487, 645.253, 466.964, -86.3979, -220.069, 636.525, 892.27, 180.944, -703.848, 682.302, 556.261, -667.287, -595.813, 736.076, -568.285, 641.163, -806.757, -633.045, -468.917, 231.91, -63.5701, -701.407, 757.378, -251.32, -932.737, -801.813, -993.896, -732.902, 406.293, -284.402, 516.892, 458.602, 637.44, -227.638, 474.776, 4.66933, 34.7606, -898.984, -138.035, -739.189, -560.961, -966.308, 559.374, 170.873, -515.793, -458.296, 698.172, -569.384, 74.0684, 524.094, 261.269, 902.28, 108.554, -653.005, 935.057, 278.298, -539.293, 632.801, 210.669, -4.42518, -797.052, 298.929, -264.138, 399.457, 83.7733, -152.928, -83.4681, -842.341, -745.964, 902.219, 768.059, 924.741, -755.852, 1.67852, -983.032, -744.499, -810.114, -365.764, -547.227, 769.951, 309.549, -470.992, 528.977, -433.699, -370.037, 194.922, 215.125, -113.987, -36.2865, 709.281, 98.178, -972.716, 135.044, 757.073, -265.969, 509.262, -543.016, 920.103, -750.664, -513.962, 457.442, -784.173, 192.602, 410.871, -143.895, -113.132, 636.158, 950.133, -209.204, 439.375, 527.573, 147.313, -705.863, 932.737, -708.853, 837.336, 20.1727, -481.796, -320.353, 310.221, 220.679, 228.37, -124.302, -774.468, 972.106, -984.313, -156.529, 776.36, 651.662, 42.1461, -69.7958, 103, -335.673, 217.75, -282.388, -42.3902, -580.737, -9.91852, -876.156, 617.847, -443.709, 383.038, -245.399, 51.1185, -577.563, 285.928, 504.929, 328.532, -201.331, -7.29392, -589.343, 289.102, 876.278, 990.905, -249.672, 453.597, 483.505, 0.701926, 396.161, 188.879, 562.243, -735.954, 416.12, -878.231, -105.686, -916.318, -74.8009, 803.156, -921.69, 612.171, -812.86, 621.998, -243.934, 890.866, -34.0892, -30.488, 89.0225, 139.317, -872.799, 653.249, 485.031, -705.985, 490.219, -439.131, -425.092, -202.368, 929.014, -425.275, 438.948, -362.163, 724.113, 735.282, -916.684, -363.323, 164.098, -344.218, -833.064, -10.5289, 397.32, -814.203, 977.783, -946.287, 533.067, 325.907, 659.597, 388.714, 993.53, 390.973, -311.747, 43.7941, -884.884, -575.243, 598.804, 98.117, -999.695, 864.071, 961.241, 449.385, -381.207, 452.193, -392.132, 852.901, -840.693, -596.24, -890.561, -789.3, -368.755, -181.86, 501.694, 110.752, -284.097, 343.974, 262.123, -384.259, -131.443, 92.8068, -463.668, 302.347, -330.607, 803.583, -385.174, 550.096, -574.267, -743.645, -340.617, 80.6604, 23.0415, -63.3259, 679.739, -253.151, -973.205, -50.6912, -240.15, -428.449, -966.063, 182.592, 535.142, -963.073, 274.575, 722.282, -81.8812, -376.507, -536.79, 515.732, -924.131, -401.105, 900.754, -714.042, 655.934, -942.503, -174.291, -304.361, 263.283, -791.07, 16.0222, -238.685, 186.499, -82.5526, -359.6, -482.833, -625.111, -509.14, -640.065, -70.6503, -335.551, 719.718, 679.983, 60.0909, 254.86, -833.857, 551.073, 481.368, 435.774, -296.67, 221.107, 939.512, 563.585, -695.975, 336.65, 268.715, -712.149, 80.0501, 294.412, 555.589, -972.045, 571.276, -157.018, -447.127, -300.394, -715.69, 934.385, -656.24, 691.336, 482.284, -174.657, -744.377, -908.872, 223.121, -570.666, -975.524, 163.244, 649.22, -82.2474, 208.777, 74.1905, 526.17, -428.327, -146.519, 869.869, 31.8308, -116.733, 404.34, -298.318, -204.26, 907.529, -128.452, 303.629, 187.658, 863.765, 244.423, 718.192, 856.99, -12.9704, 154.759, -726.554, 521.714, 629.994, -208.594, 809.503, -992.492, -385.846, 64.1194, -845.271, -463.24, 831.782, 17.7313, -147.191, -732.414, 96.9573, 496.017, -860.958, 83.1629, 247.658, 56.0015, -833.125, -369.671, 497.116, -868.099, -515.366, 609.058, 35.2489, 449.141, 761.834, 311.258, -119.602, -311.625, 765.984, 410.077, 980.956, 399.884, 588.977, 998.779, 566.515, -832.82, -876.095, 879.696, 750.053, -978.515, -208.716, 103.671, -621.082, -894.65, -484.115, 758.782, -863.155, 587.146, -46.9069, 9.61333, 895.199, 824.885, 965.636, -613.88, -685.476, -219.703, 350.017, 322.977, 696.524, 683.035, -186.804, -51.8509, -914.914, 944.517, -800.836, -877.499, 618.336, -677.908, 965.331, 728.69, 129.612, -666.616, 909.116, 209.937, -0.335704, 919.858, -855.22, 359.478, -742.18, 361.003, -670.278, 359.416, 933.775, 438.765, 141.087, -930.235, -341.35, 851.253, -997.742, 567.797, -482.589, 876.888, 59.9078, -63.3869, 149.693, 288.369, 409.65, -695.364, -898.373, 517.075, 573.473, 134.617, 427.534, 66.6829, -774.224, -237.709, -458.785, 177.648, 170.324, 186.01, 317.728, 740.776, -433.638, -894.65, 444.868, 461.837, -594.531, -789.3, -975.402, -724.784, -761.773, -278.42, 425.153, -991.821, 150.426, 176.366, 381.634, 294.9, 989.868, 470.992, 348.552, -259.682, 842.891, -851.802, 100.864, -665.334, -239.174, -63.5701, 585.62, 117.1, -263.039, 539.109, -976.562, 89.816, -67.4764, -326.579, 260.537, 382.427, 153.478, 359.722, 369.549, -334.452, 577.38, -539.903, 501.694, 815.363, -270.669, -531.907, 987.976, -172.704, 709.342, 547.227, -308.939, 980.285, -331.95, 820.307, 606.616, 250.649, 533.86, 908.322, 714.835, 976.501, -10.651, 719.718, -917.844, -228.614, 697.745, 489.364, 212.867, 688.711, 779.595, 67.5375, -349.651, -364.544, -460.86, 459.151, -170.934, -963.439, -558.763, -387.371, 175.756, 748.894, 337.687, 459.212, -538.804, -775.018, -56.4287, -798.456, 401.776, 927.854, 564.379, -146.519, 510.91, -5.89007, 71.9321, -971.618, -799.188, 148.839, -868.465, 978.82, -866.207, 869.076, 678.213, 885.006, -175.878, -201.758, -356.853, 394.513, -510.666, -589.831, 930.662, -589.221, -848.018, -77.1203, -355.632, 132.603, 991.028, 462.996, -436.018, -657.094, 943.419, 479.598, -128.208, 947.935, -444.075, -591.784, 129.246, 766.167, -913.633, 585.314, -496.506, -432.356, 855.464, -588.305, 243.995, 362.896, 847.957, -232.765, -591.113, -58.1378, -435.408, 674.429, 842.097, 663.32, 123.753, -787.347, -601.062, 493.332, 612.476, -317.545, -576.22, -705.008, 688.589, -980.895, -524.216, -632.801, 431.928, 457.137, -694.143, 233.619, -553.941, -555.834, -916.929, -318.339, 209.815, -195.044, 871.456, -948.057, -321.268, 155.675, 355.998, 777.154, -863.216, 567.553, 820.002, 170.934, 298.013, -518.54, -629.078, -532.945, -342.326, 871.639, -582.507, 108.798, -905.759, -340.251, -604.236, -712.638, 720.878, -620.899, -253.761, 731.62, -13.2756, 88.7173, 139.927, -253.395, -971.191, 537.156, 955.077, 503.037, 273.598, 576.83, -996.155, -423.933, 288.064, -172.094, -491.012, -265.542, -118.137, 592.578, 329.936, -20.3558, 458.663, -981.201, 439.68, -165.136, 617.542, 514.756, 651.479, -116.001, -995.178, -587.573, 595.019, 560.9, -584.521, 882.199, -56.6118, -811.762, 144.688, -903.562, -110.569, -733.146, -410.504, -356.059, 872.188, 14.3742, 100.253, 193.762, 553.636, 154.698, 82.3084, -590.808, 45.32, -43.1227, 347.392, 112.339, 384.869, -475.875, 715.384, 697.378, -737.846, -667.043, 865.841, 795.404, -3.38755, -977.721, -560.656, -812.128, 763.176, 432.478, -610.645, -28.779, 69.0023, 894.223, -416.425, 970.458, 480.27, 767.937, 655.568, 403.363, 598.376, -767.571, -893.674, -205.237, -713.187, 324.503, -344.096, -859.615, 147.496, -772.759, -155.126, 320.963, 247.597, 809.992, 885.922, 766.228, -904.355, 416.608, 765.374, -630.909, -131.321, 904.965, -405.194, 649.709, 888.424, -64.1194, -787.286, -830.561, -652.211, -487.594, -964.11, 194.983, -712.21, -231.544, 161.046, 612.476, -176.183, -991.76, -283.67, 91.3419, -377.728, -4.85244, -536.973, 498.764, -372.845, 490.951, 647.084, -436.75, 36.5917, 44.5265, -899.533, 789.117, -470.443, -968.566, 593.188, -379.803, 742.18, -57.2832, 434.126, -145.665, -5.70696, 923.704, -676.626, 306.986, -733.573, -436.445, -546.556, -274.453, 425.947, 934.019, 860.652, -46.5407, -296.426, -121.433, 26.8868, -655.995, 420.454, -448.531, 731.437, 424.726, -35.9813, 206.885, -848.14, -188.879, -984.741, 237.831, -783.624, 371.014, -975.341, -534.654, 204.749, -361.675, -60.7624, 677.541, 600.085, 477.584, 178.808, -581.591, 190.527, -2.59407, 862.606, 27.4972, 420.82, 715.018, 286.05, -71.8101, 993.53, -29.8776, 646.962, 167.516, 720.878, -411.054, -977.538, 204.505, 626.514, -201.514, 595.386, 949.889, -703.482, -609.668, 130.65, 54.7197, 481.552, 134.8, -297.769, -550.34, 947.935, -554.979, 669.424, -866.085, 579.394, -708.853, -96.5911, -785.516, 728.935, -635.792, 264.077, 194.922, 819.819, 490.28, -673.757, -485.336, -802.484, 294.29, 356.426, 338.115, 231.361, -885.495, -460.677, 593.31, -334.33, 166.54, -545.518, 261.513, 221.656, -43.0616, 802.728, 915.952, -551.073, 947.691, 382.855, -489.181, -172.948, 470.26, -682.058, 420.82, -882.992, -110.508, -467.635, -270.547, 962.951, 17.7313, 477.889, -231.056, -256.508, 778.863, 110.446, -91.525, -326.456, -877.56, 116.733, -406.11, 155.736, -812.128, -609.241, -363.506, -617.969, 718.07, -239.662, -247.963, 185.217, -276.223, -36.4696, 349.895, 140.477, -602.161, -124.363, 572.192, -82.8578, -721.061, 985.351, 582.69, -120.151, 665.822, -878.719, 105.93, 940.672, 281.045, -570.971, -783.624, -340.007, 185.644, -402.875, -622.486, -401.166, -57.4053, -665.944, 730.522, 919.797, -968.627, 94.5158, -366.192, 238.807, -488.388, -943.907, 451.949, 726.798, 937.376, -653.92, -405.56, 461.287, -478.988, 517.563, -194.8, -972.35, -452.254, -723.93, 701.529, 371.136, -960.936, 456.771, -437.3, -102.145, -332.804, 250.038, 369.854, -172.948, 919.126, -321.207, -905.698, 791.925, 510.361, 324.808, -762.932, -308.39, -950.56, 305.155, -382.305, -207.068, 951.964, 498.825, 413.312, -918.577, -396.039, -976.562, -552.049, 863.338, 776.971, -38.1787, 731.437, 616.932, 291.299, 338.115, -751.885, -905.881, -69.4296, -655.08, -3.26548, -327.738, -394.513, -180.395, -721.305, -44.6486, -231.971, -29.2062, 237.587, -374.187, 75.5943, -645.009, 648, 546.068, 304.849, -896.664, 954.649, -890.255, -484.054, 397.809, 561.693, 843.928, -533.433, -802.057, 371.441, -739.982, 275.979, -782.342, -426.557, 955.809, 338.969, -848.567, 684.011, 966.674, 490.158, 150.975, 834.468, 367.29, 417.951, -448.042, -688.955, 123.447, -671.682, 850.398, -656.484, -807.001, 859.554, -70.8335, 144.2, 878.17, 245.399, -465.133, -802.85, -573.595, -270.058, 4.05896, 797.357, 882.565, 231.361, 239.967, 839.534, 189.917, -744.377, 939.024, -142.735, 542.161, -938.658, 67.4764, 312.357, 984.741, -926.878, 587.878, 133.396, 266.396, 545.946, 435.713, 505.173, 941.038, -520.92, 745.903, -92.3185, -281.228, -219.52, 329.386, 405.133, 67.9647, 680.41, 399.64, 184.973, -933.531, 395.734, -450.545, 346.965, 503.525, 43.9772, 977.233, -57.5274, 230.811, -192.602, -36.4086, -736.869, -235.45, -652.944, -401.959, -619.739, 136.814, 466.659, 755.608, -66.3167, -3.02133, 26.9478, 322.977, -829.035, -36.4696, -890.5, -324.32, 236.427, -774.59, 431.074, -595.752, -289.956, 393.902, 7.35496, 290.811, 396.71, 603.626, -932.859, 535.63, 365.337, 864.681, 216.59, -766.167, 237.648, -336.039, -147.13, 102.023, 727.531, 816.706, 828.852, -545.579, 429.853, -432.051, -633.717, 430.036, -140.172, 944.578, 220.801, -531.48, -258.217, 355.998, 95.3703, -962.584, -451.643, 446.76, -695.486, 608.386, 289.468, 197.241, -311.502, -116.367, -448.408, 275.796, 777.093, 272.805, 786.615, 639.943, 204.016, 619.312, 850.887, 388.043, 370.647, -823.115, -205.542, -138.951, -639.576, -163.549, 900.082, 993.774, 102.634, 602.283, 972.106, -83.4071, -213.416, 318.705, 875.729, -192.907, -474.41, -353.191, 326.09, -8.51466, 271.279, 290.933, 927.122, -703.848, 977.294, 474.96, -539.232, -350.566, -986.45, 669.424, -573.901, -67.0492, 830.5, -432.539, 828.242, 83.224, 872.677, 311.93, 217.322, -556.749, 743.278, 971.557, 977.966, -635.67, 617.908, -732.475, -132.115, 57.0391, 908.811, -449.385, -192.785, -306.681, 178.808, 26.9478, 917.661, -242.164, -694.632, 234.901, -188.94, 649.648, -105.014, 771.966, -870.907, -466.597, 946.593, -729.362, -668.02, 441.511, -233.741, 625.904, 737.541, 625.172, -869.076, -529.283, -189.734, -843.867, 702.689, 903.623, -299.356, -221.473, 692.373, -535.63, -966.247, 9.79644, -225.745, 361.248, 948.973, 492.904, -750.237, 947.142, 388.165, 597.4, -538.621, 446.333, -364.422, 271.462, 457.259, -68.9413, 106.54, 142.918, 904.538, 547.899, 386.395, -128.697, -745.964, -5.40178, 519.822, 749.199, -845.882, -753.716, 549.791, 553.636, -885.372, -467.513, -693.533, 648, 421.064, -436.689, -553.209, -666.738, -701.407, -993.896, 742.851, -550.157, -673.818, 212.195, -378.704, 229.102, 100.681, 193.152, 952.635, -605.457, 150.67, 847.53, 89.6939, -486.129, 100.742, -562.548, -842.708, 600.391, 85.6044, -684.805, -408.979, 964.354, -586.108, 899.655, 926.206, 314.798, 482.467, 902.829, -162.938, -888.974, 940.306, -193.457, -574.084, 164.098, 102.695, -410.932, 762.139, 488.449, -384.747, -298.563, 671.072, -596.606, 368.023, -705.802, -226.722, 884.884, -340.617, -306.009, 773.675, 166.845, 610.401, -870.113, -999.756, 592.761, -171.606, -289.712, -806.452, 717.399, -678.396, 649.159, 545.396, -341.777, 663.015, -257.179, -11.8717, 697.012, -595.996, -903.623, -266.274, -868.648, -260.781, -899.533, -937.498, 376.507, -580.126, 657.582, 362.896, 316.324, 918.332, -341.411, -580.493, -94.5158, 841.67, -290.201, -46.7849, 20.8441, -403.607, -891.11, 409.406, 562.792, -698.05, 71.8711, 997.681, -193.152, -44.9538, 209.51, 980.346, 5.70696, -156.407, 253.822, 211.951, -759.941, 210.913, 34.8827, 928.037, -479.171, 169.53, 707.083, -215.735, -859.859, -134.373, -774.041, 145.726, -726.554, -107.212, -925.474, 625.66, 830.683, 956.297, -759.88, -125.401, -442.122, -213.294, -559.435, -380.413, -611.011, -695.242, -398.907, -46.7238, 47.2121, -965.392, -419.233, 872.555, 896.603, -231.544, -851.009, 68.0868, -956.481, 480.209, 212.317, 963.256, -311.441, -510.239, -709.525, -997.009, 727.775, -122.898, 243.629, 193.335, -395.734, -853.877, -751.579, -110.202, 409.894, -860.897, 24.6895, -162.938, -763.359, 200.415, -687.857, -542.344, -221.473, -409.528, 596.85, -608.753, -817.255, 150.304, 87.5576, -716.422, 151.524, -784.783, 942.808, -174.413, -706.534, -221.046, -253.212, -957.762, -274.575, -92.2575, -763.421, 832.148, -76.3878, 404.462, -721.671, -244.118, 825.739, 722.587, 93.8444, 75.7775, -818.171, -869.808, -262.734, -955.626, -843.257, -875.729, -178.442, 444.014, 180.822, 94.6379, -504.99, 375.408, -948.119, 237.831, -332.011, -702.994, 287.027, -278.542, -510.727, 314.188, 723.441, 98.117, -927.122, -282.266, 370.83, -30.549, 474.41, -226.417, -487.594, 900.266, 917.6, -238.563, 56.6729, -417.524, 270.913, -735.832, -861.141, 249.489, 774.468, -293.558, 972.716, -660.207, -984.191, 144.2, 951.781, 411.359, 157.506, -696.524, 1.06815, -694.449, 412.702, -130.467, -821.162, -438.581, -227.821, -974.059, 527.879, 79.3176, 981.201, -756.34, 177.038, -964.599, 765.313, -679.983, -306.986, 45.5641, -692.434, -332.194, -318.094, 579.272, -24.9947, -145.604, -359.6, 624.012, -705.374, 390.79, 57.9546, -400.983, -819.147, 770.623, 767.815, -480.392, -588.672, -240.883, -451.277, 410.993, 353.923, -67.4154, 976.867, -27.5582, -56.856, 466.964, -473.373, -508.591, 906.674, 177.16, 923.093, 7.66015, 241.92, 806.085, -257.118, 726.981, 942.808, 563.707, -733.94, -314.432, -24.0791, -84.0175, -422.346, 664.235, 507.614, -375.835, -80.8435, 351.848, 379.925, -966.247, -709.891, -239.906, 837.703, 613.453, -700.308, -112.949, -495.163, 744.255, -731.803, -368.45, -388.897, 758.721, -421.186, -42.9395, -531.663, 229.774, -721.183, 305.765, 197.302, 510.056, -68.3309, -737.236, -295.816, 520.554, -282.144, -529.893, 907.224, 790.094, -212.745, 790.521, 212.806, -255.776, 859.981, -278.054, 242.897, 244.24, 78.4631, -938.597, 810.846, 376.263, 108.798, -180.944, 148.228, 490.402, 492.233, -617.969, 381.451, 489.669, -753.166, -70.9555, -741.874, -630.787, 719.23, -906.064, 124.058, -994.812, -314.371, 880.062, 149.205, 194.006, -295.999, 823.969, 804.743, 941.282, -284.646, -590.075, 589.221, -999.268, 895.322, -266.091, 5.64592, 663.808, -244.545, -582.812, 787.53, -202.002, -504.624, 770.989, -624.317, -863.521, -396.1, -789.972, 644.826, -182.958, -289.285, -82.6746, -120.029, -829.218, -312.113, 757.256, -996.826, 761.834, 1.55644, -559.679, -950.011, 675.649, 313.334, 937.376, 896.176, 58.8092, 332.072, -857.356, 233.802, -92.3795, 915.159, 789.3, 798.578, 107.334, -579.699, -388.043, -154.332, 721.183, -692.984, 762.139, -123.203, -470.748, -11.6886, -528.916, 616.504, -181.86, 931.822, -35.0047, -761.406, 419.172, 11.8717, 628.101, 843.379, -1.80059, 110.935, -582.69, -816.889, -405.805, -29.0841, -473.922, -360.21, -591.418, -705.069, -701.468, 450.85, -155.797, 99.0326, 393.536, 185.949, 794.366, 313.334, 198.401, -526.292, -818.293, -366.131, 493.759, 978.149, 747.917, -375.713, 366.68, -374.92, 91.9523, 67.0492, -624.561, -920.896, 959.716, -434.858, 963.317, 469.832, 782.708, 393.841, 122.471, -631.764, -532.579, -398.114, -691.641, -986.145, 434.37, 363.628, 603.748, -209.021, -93.173, -420.27, 132.298, 955.077, 450.179, -270.425, 723.38, 609.485, 760.979, 972.472, 825.495, -67.8426, -294.107, -501.022, -329.386, -347.942, 610.889, 132.969, 688.467, 775.689, -199.988, -817.194, -830.073, -436.445, -615.101, -456.954, 181.982, 757.317, 303.568, 45.9914, 730.949, -944.7, -390.606, -936.705, 177.038, 840.327, 10.3458, 357.463, 898.373, 155.004, -341.899, -816.706, -795.343, 929.258, 186.804, 639.882, 501.999, -543.504, -997.497, 271.584, 706.107, -879.818, -178.014, 919.98, -93.0509, -629.627, -538.194, -174.596, 282.693, 950.56, 877.682, -21.6987, 263.466, -626.453, 452.864, -278.909, 945.982, 649.037, -461.898, 599.414, 756.89, -740.776, 240.028, -536.119, 367.595, 385.968, -539.537, 821.65, -142.247, 387.127, -814.508, -562.181, 129.49, -477.035, -975.585, -812.738, 571.703, 933.287, 999.023, 155.98, 973.693, -136.876, 582.324, -7.47703, -310.465, -859.859, -11.9938, -489.914, 52.2172, 900.815, -290.811, 537.584, -962.645, 566.759, 307.596, 21.2104, 177.526, -928.22, 452.681, 376.324, 726.249, -661.733, -789.544, 88.9004, -928.037, -996.46, 583.117, 566.82, 402.448, -603.442, 297.159, -758.232, 618.336, 764.641, 843.44, 920.774, 497.543, -250.343, 765.618, -612.598, 109.226, -885.006, 199.377, -746.086, -522.019, -277.932, 661.55, 62.2883, -502.304, 756.401, -910.886, 636.036, -904.355, 204.749, 128.391, -318.339, 646.84, -418.561, 673.757, 640.553, -362.468, 905.026, 657.094, 789.911, 579.882, -75.4723, 86.7031, 594.958, -824.458, -403.424, 336.406, -689.993, 730.216, -188.635, -315.592, 830.561, 817.133, -630.36, -897.397, -205.786, 191.443, 209.082, 295.694, -67.2933, -45.5641, -119.175, -10.1627, 673.391, 967.284, 276.162, 472.152, -9.49126, -484.787, 603.015, 379.62, -492.66, -778.436, 612.476, -262.001, -152.867, -787.957, 75.4723, -748.772, 235.023, 528.489, 509.384, -159.886, 349.467, 792.962, -845.882, 431.745, 423.994, -877.987, -800.47, -1.86163, -401.593, -709.281, 261.208, -154.332, 520.859, -853.389, -892.819, 154.149, 198.828, 273.782, 635.121, 765.435, -898.862, -93.173, -902.158, 223.06, 509.384, -904.782, -419.111, -939.207, 119.297, -882.809, -271.584, -657.704, -107.334, 143.773, -279.946, 449.568, 158.605, 152.684, 497.299, -877.804, -34.8216, 92.8678, 240.272, -253.273, -318.766, -701.834, -905.82, 895.505, -152.562, -848.567, 145.848, 115.39, 278.359, 998.84, 650.319, 967.65, -586.352, 313.456, -492.66, 316.019, -79.989, -890.194, 988.22, -133.274, 542.222, 607.898, 711.844, -43.1227, 205.115, 146.764, -128.147, -406.293, 365.093, 349.162, -287.332, 743.767, -388.47, 535.203, 256.63, 871.944, 518.418, 386.578, -720.206, -269.753, 745.964, -139.195, -460.677, 397.504, 392.132, -645.192, 249.367, 567.614, -405.866, 119.541, 808.161, 89.1446, 380.108, 923.643, 774.102, 754.509, 770.623, 967.528, 92.6847, 338.42, -869.93, -952.269, -427.046, 887.57, 768.792, 970.946, -386.029, -740.776, 793.817, -782.586, -818.537, -89.1446, -788.995, 856.136, -965.636, -61.5558, 276.711, -292.215, -798.517, 366.253, 98.7274, 759.697, 707.816, -314.615, 488.449, 829.218, -448.714, -356.792, -938.047, -521.775, 929.441, -717.948, -407.758, 177.099, -284.463, -771.233, 609.973, -281.35, -814.203, 233.314, 255.531, -117.71, -4.60829, -42.1461, -619.434, -651.967, -594.897, 16.3274, 899.472, -139.439, 758.293, -737.175, -609.18, 643.422, -793.268, -668.813, 949.156, -937.132, -311.991, 423.75, -713.614, -457.869, -848.506, -649.464, 83.5902, 725.455, -964.965, -181.616, -407.819, 891.781, -8.27052, 969.848, -514.267, -380.596, 866.085, 172.155, -38.3618, -300.211, -79.0124, 56.856, -907.59, -698.599, 314.188, -191.87, 427.412, 384.136, -175.512, 702.383, 844.356, -637.013, 828.852, -103.061, 650.929, 22.6142, 528.55, 469.588, -63.8142, 680.96, -574.023, -292.947, -827.57, -396.588, 51.1795, -355.449, 66.4388, 667.165, -61.9221, -145.543, 661.306, -725.272, 851.619, 485.824, 687.674, 518.723, 263.771, -447.127, -834.468, -404.95, -919.492, 474.654, 256.081, 467.879, -381.817, -136.387, -637.623, 782.952, -169.958, -435.041, -517.991, -968.017, -217.078, 945.677, -192.236, -33.0515, 383.709, -566.698, 579.699, 263.71, 355.937, -694.388, 186.438, 436.201, -533.616, 728.996, 517.136, -999.39, 1.55644, -952.147, 562.365, 889.584, 494.43, 323.832, 266.274, 115.696, -299.356, -875.607, 628.04, 686.27, 825.556, 562.426, 53.1327, -233.131, 0.274667, 584.826, 955.199, -904.355, -991.211, 511.948, 607.41, 647.389, 264.138, -173.62, 244.24, 547.288, 34.9437, -787.835, 412.885, -764.519, -77.8527, -156.041, 574.694, -119.236, 585.681, 925.779, 957.091, -955.26, -279.275, -519.761, 654.775, -404.035, 284.524, 387.799, -18.4637, -126.194, 972.716, 675.466, -928.282, -897.214, 946.165, 56.1235, 731.742, -943.602, 807.306, -282.571, -458.296, -28.5958, -173.254, -638.844, -793.085, 711.6, -580.004, 491.867, 159.703, -817.927, 873.898, -43.8551, -589.587, 993.408, -676.992, -268.166, 649.709, 375.347, -23.4687, 966.857, 599.353, -658.925, 594.653, 268.288, -385.54, -898.373, 215.125, 241.676, 534.654, -270.791, 129.795, 279.763, -214.209, 71.8101, -888.668, -398.297, 731.437, -82.7967, 889.706, 542.344, -725.639, 406.781, -121.494, 90.7926, -291.604, 699.881, -848.018, -320.048, 819.758, 887.631, 671.194, -774.712, 238.502, -842.952, -239.296, -585.864, -959.655, -794.549, -164.708, 244.362, 598.254, -96.1028, -623.707, -186.071, -316.385, -788.263, -841.67, -640.37, -181.738, 340.617, -274.27, 546.251, 875.79, 168.126, -880.856, -994.507, 933.103, -97.5677, 852.229, 125.462, 372.051, -3.93689, 137.913, -910.947, 231.056, -690.054, 365.337, -836.299, 806.513, 527.573, -173.742, 240.028, 504.501, -181.127, 691.336, -379.131, 942.198, 754.631, 237.22, -939.39, 299.539, 470.077, 322.672, 54.2924, 920.957, 79.3176, -884.762, -623.951, -833.613, -660.634, -931.883, 869.076, -486.435, -435.957, -853.755, -516.16, 909.055, 788.079, -696.89, -432.112, -630.116, -818.781, -792.596, -595.63, 835.383, 712.455, -780.633, 1.3123, 268.471, -936.46, -382.55, 130.589, -992.737, -206.153, 74.984, -382.733, 366.802, 753.777, -613.575, 878.231, -509.568, 749.016, -555.651, 711.539, -548.631, 454.817, -534.227, 335.429, -395.489, -420.331, 468.612, -322.611, 187.597, 279.763, -1.55644, -86.7641, -826.96, 822.748, -895.016, 256.02, -630.482, -498.093, -431.318, 995.3, -226.539, 800.348, -622.791, 786.004, -829.524, -195.349, 452.193, 187.719, -12.7873, 869.625, 625.477, -546.373, 392.56, 531.602, -133.946, 569.567, 649.525, 673.025, 756.34, -457.015, -283.425, -63.6311, -781.243, 942.381, 99.4598, -611.866, -507.309, 685.781, 952.513, 432.966, -639.698, -919.675, -569.811, 977.66, -376.08, 111.179, 289.102, -154.698, -168.004, 980.468, -261.208, -560.595, 255.898, -103.793, -455.977, 657.704, 116.55, 112.094, -624.012, 56.4287, 60.7013, 215.552, -586.108, -735.588, 888.058, 667.592, -803.339, 646.962, 527.024, 591.418, 724.296, -780.023, -738.029, 811.762, -190.466, 814.203, -790.338, -856.319, -320.536, 489.059, 45.5031, -67.9647, 827.143, -803.278, 980.285, -159.948, -887.143, 696.463, 986.816, -111.24, 116.062, 718.253, -723.624, -254.25, -263.344, 377.117, 25.605, 264.626, -953.307, -162.633, -647.572, 273.659, -608.081, 226.234, 543.26, -384.869, -256.63, -201.758, -471.053, 434.309, 298.807, 349.101, 612.11, 718.436, 334.33, -120.151, 442.976, 85.9096, 306.131, -488.937, 246.315, -174.657, 201.758, 646.596, 297.891, -815.607, 141.82, 387.799, 651.173, -90.7315, -768.426, -802.24, 113.62, -966.735, 662.587, 815.119, -526.536, -83.6512, -936.155, 418.012, 976.318, -466.292, 104.038, -961.303, 51.4237, 15.3508, 342.448, 227.088, -711.356, 434.614, -669.179, 964.904, 664.724, 847.652, -34.9437, 829.89, 19.7455, 582.324, -913.633, -590.258, -215.857, 777.337, 333.354, -33.6009, -45.0148, -828.913, -756.951, -650.319, -677.664, -858.882, 603.015, 161.84, 946.287, 992.859, 52.2172, -828.791, -116.977, -501.511, -66.6219, -23.3467, -838.191, -313.211, -606.006, -78.8293, 11.3834, 881.466, 738.029, -743.706, 336.406, 990.295, 27.4361, 556.2, -555.834, -955.443, -65.9505, 381.512, -978.942, -12.0548, 840.632, -806.635, 359.355, -284.646, 192.785, -482.04, -166.662, 362.285, 100.436, 359.966, 626.331, -156.224, -810.724, -133.152, 707.022, -356.609, 702.689, 243.141, -943.968, 852.474, 852.657, 914.243, 142.491, 459.029, -577.99, -59.5416, 650.685, -113.193, 615.711, -318.644, 456.465, -215.491, 162.206, 709.525, -146.031, -935.301, 665.334, -25.2388, -967.956, 192.969, -794.366, -781.915, -199.072, 726.92, -807.855, -363.994, -431.257, -894.894, 463.851, 794.183, -431.928, 686.575, -358.989, -208.167, -667.531, 39.3384, 727.165, 675.832, -576.769, -520.249, -39.7046, 104.282, 405.683, -138.401, 914.792, -738.762, 75.7775, -384.991, -147.496, 20.3558, 937.01, 35.6761, -675.1, -641.041, -73.6412, 823.664, 24.0181, 22.309, 391.583, -127.232, 884.64, -562.548, 317.057, -690.481, -564.928, 877.56, 801.019, -217.994, 399.823, 875.546, 781.426, 999.268, 549.913, 888.607, 13.2145, 520.005, -194.555, -788.018, 294.351, 792.108, 586.535, -856.868, 942.625, 441.084, -584.765, -604.175, 522.141, -190.161, 766.961, 764.153, 820.002, 747.185, -828.12, -963.622, -927.488, 647.084, -432.966, 159.52, 442.427, 737.907, -102.939, -347.087, 744.743, 621.082, -33.4178, 272.5, -576.403, -939.512, 541.246, -75.1061, 779.168, -978.454, 699.332, -168.615, -556.505, 960.082, -566.332, 942.076, 107.822, 361.187, -497.116, 599.719, -501.511, -470.992, -467.147, -545.885, 604.053, -374.981, 368.755, 626.087, -848.628, 957.701, -324.015, -878.842, 332.682, -479.049, 792.352, -505.722, -218.543, -216.102, -170.873, -236.671, 799.738, -270.608, -977.05, -426.679, -165.807, 143.345, 337.321, 610.828, 202.918, 153.539, -686.087, -732.658, 71.749, -980.895, -825.495, -445.967, 681.143, 169.164, -827.692, 265.542, 822.443, -507.859, -806.452, 393.719, -435.286, -125.278, 609.18, 792.352, -768.853, -9.064, -623.829, 90.5484, 625.66, -658.742, -610.523, 857.356, 509.201, -458.296, 83.1019, 15.4118, -673.086, -137.303, 282.083, -494.736, -815.912, -981.75, 303.812, -800.592, 73.2139, -794.733, 309.732, 66.0726, 350.078, 392.132, 906.858, -416.242, -539.415, 858.15, -0.824, -595.202, 917.417, -17.5481, -110.508, -730.216, -473.678, -716.971, 630.909, -828.425, 863.094, -294.046, -371.929, -230.873, 999.451, -835.688, -510.605, -948.18, 480.331, 327.25, 127.842, -746.574, 549.974, 659.108, -886.898, 16.0833, -371.258, 708.426, 272.805, -195.471, 39.6435, 647.023, 544.298, -429.975, 984.619, -940.916, -924.009, 664.48, -228.553, -19.6234, 579.211, -995.605, -709.708, 186.926, 929.38, -514.695, -260.048, 734.367, -982.177, 645.314, 604.724, 746.879, 853.633, -420.515, 880.856, -543.565, -85.6044, -896.176, -446.883, 491.745, -251.442, -830.622, 321.512, 417.096, 826.655, 526.292, 577.38, -478.195, -357.219, 637.745, 630.543, 859.188, 66.4388, -155.919, -814.57, -464.705, 943.907, -222.816, -932.066, 248.451, -50.4471, 23.957, -798.151, -922.178, -311.808, -959.227, 835.444, -476.913, 45.6252, -682.485, 17.6702, 792.901, 791.131, 791.62, 325.663, 344.157, 428.205, 511.826, -159.825, 671.316, -703.116, 552.904, 372.967, 345.195, 924.741, 178.93, 943.052, -698.111, -371.319, 258.034, -496.933, 382.427, 366.558, -74.0074, 916.44, -878.964, 838.557, -499.374, -816.889, -215.186, -161.107, -354.472, -83.0409, -337.382, -142.552, 784.417, -928.404, -396.039, -68.9413, 765.007, -82.4915, 808.405, 745.293, 773.247, 268.41, -135.899, -14.008, -502.853, -42.4512, 37.5683, -59.1754, -458.907, 564.867, 812.128, -649.281, 89.816, -607.227, -322.855, -488.266, -177.709, -306.375, 167.455, 626.209, 76.0826, -854.61, 151.83, 404.645, 261.147, 578.967, 372.417, 917.173, -475.997, 954.1, -766.289, -304.971, 662.038, 586.352, -158.177, -917.722, 287.088, 623.646, -707.022, -162.694, -760.247, 772.698, 213.172, 984.313, -746.391, -724.418, 547.044, -85.1772, -130.894, -863.643, -403.302, 739.616, -402.57, 597.705, 311.136, 889.767, -665.029, -132.786, -174.841, -31.4035, 27.253, 94.2106, 248.268, 853.206, 582.629, 596.301, -295.938, -954.283, 36.9579, 335.734, -381.085, 275.246, -32.7464, -353.801, 261.513, 349.162, -844.234, 209.265, 531.419, -745.231, 504.318, -719.108, -1.61748, -488.998, 197.913, 168.249, -808.527, 769.768, -880.673, 955.931, 138.035, 667.47, 512.864, -517.441, -847.285, -73.2139, -101.779, 567.797, -123.386, -291.726, -764.885, -511.643, 566.332, -788.446, -663.198, -295.083, -367.107, -472.64, 534.471, 838.435, -568.957, -261.269, -408.856, 896.847, 298.257, -727.409, 865.841, 751.701, 843.562, 501.694, -272.5, 864.559, 486.801, -404.401, 219.214, 263.649, 247.841, -377.178, 972.228, -229.591, -214.759, -98.056, -630.177, -523.85, 685.781, -396.649, -38.2397, 468.368, 3.02133, -398.968, -872.005, 807.55, 46.8459, 573.656, -7.04978, -246.437, 422.163, -643.666, -410.627, 242.409, 318.155, -457.442, 272.622, -387.371, -952.757, 335.246, -615.467, 581.957, 834.468, -351.665, -832.331, 473.678, -144.2, -541.246, 929.38, 693.594, -471.847, -592.334, -544.603, 456.099, 47.7615, 652.15, -325.297, 761.162, 179.479, -33.4788, -426.984, 620.716, -704.276, 94.3327, -643.117, -464.949, -334.025, 349.528, 868.099, 219.825, 995.239, -531.724, -752.861, -418.012, -786.798, 710.379, 848.14, -52.9496, -486.007, -789.239, -598.071, 535.142, -355.754, -418.5, 431.013, -812.433, -557.176, -237.342, -87.1303, 343.852, 629.749, -875.607, -805.414, -732.292, 845.027, 818.476, 156.713, 524.155, -62.0441, 379.498, 627.125, 705.557, 506.394, 576.83, 45.381, -591.601, 685.171, 515.915, -623.341, 460.189, -814.386, 990.112, -724.052, -604.663, 584.826, 81.8812, 501.816, -817.621, 864.071, -985.351, 580.737, 200.11, -801.63, 254.433, 721, 464.278, -4.48622, -139.134, 976.989, 104.221, -815.18, 89.5108, -337.016, 89.3277, 756.584, 242.836, -982.665, -794.366, -449.202, -227.943, -883.358, -717.521, 296.548, -371.38, 39.5825, 668.813, 900.693, -134.19, 281.106, -39.9487, -12.7873, -509.751, -583.544, -25.544, 686.27, 563.524, -984.558, -20.2338, 673.147, 553.575, -193.945, 169.713, 457.32, 936.644, -911.008, 390.728, -78.4021, 48.4329, 401.532, -881.771, -888.913, 185.827, -568.59, 683.584, -727.653, 958.495, 181.188, 750.053, 142.491, 890.439, -617.786, 40.0708, -512.009, -728.69, -841.426, 311.441, 894.162, 258.705, -58.7481, 583.789, -196.326, -771.416, -367.718, -384.747, -193.762, -622.12, -981.506, 629.322, -822.81, 575.61, -563.89, 290.811, -333.232, 464.217, 684.194, -67.1102, -460.982, -188.33, -810.968, -966.491, 861.202, 112.583, 418.439, 942.076, -957.701, -559.923, -454.817, 855.647, 622.791, 547.044, 361.736, 45.8083, -687.124, -782.098, 443.831, 335.368, 282.388, 870.357, 564.745, 824.58, 699.21, -593.677, 359.355, 867.733, 518.601, 314.249, 238.197, -700.003, -477.767, 20.1727, -448.653, -86.1538, 607.532, -510.544, -997.07, -814.57, -674.062, 78.4021, -685.354, 251.442, -327.067, -364.422, 140.11, -864.559, 126.621, 853.694, -770.989, -775.445, -63.448, -129.063, -159.093, -3.99792, -336.711, -877.316, 409.589, -6.37837, -32.6853, 443.037, 456.771, 54.5366, 647.877, 416.12, 756.523, 785.577, -67.8426, -21.8818, -250.893, 289.834, 14.5573, 268.349, 205.176, 143.59, 845.515, 731.315, 139.744, -377.789, 170.507, -532.212, -23.835, 177.343, -180.578, -163.182, -39.5825, 163.182, 245.399, -693.228, -174.963, -186.987, -686.209, 557.543, 703.238, 465.926, -658.437, 831.538, -707.327, 43.1227, 386.029, -464.583, 270.241, 774.773, -215.979, 92.0133, 955.199, -823.603, -58.8702, 41.2915, 223.792, 275.613, 370.464, -607.593, -326.518, 88.29, -18.0975, -775.14, -963.561, 345.683, -378.826, 540.574, -155.98, -529.527, 858.821, -635.243, -659.291, -196.326, -970.824, 98.3001, 648.305, -918.943, 519.822, -881.466, -820.49, 159.703, 310.099, 631.397, -697.256, 994.568, -239.357, 772.637, -744.377, 865.169, 652.211, -40.5591, 309.305, 851.924, -525.315, -507.736, 182.348, 562.975, -33.5398, 406.049, 704.886, -889.584, -96.9573, 571.886, -267.312, 337.443, 136.509, 48.616, -688.589, 293.985, -115.696, 684.5, 90.3043, 964.232, 268.532, -31.5256, -13.6418, 242.531, 104.831, -867.855, -901.425, 160.436, -58.504, -499.008, 871.944, -637.257, 348.674, -904.05, -488.693, -165.441, 457.015, -514.756, -354.228, -122.471, 747.368, 885.739, 242.409, -663.442, 89.2666, -485.58, 770.562, -923.704, -969.726, -809.809, -384.198, 707.816, -664.846, -814.631, 744.194, 564.867, -225.501, 327.128, 615.833, -69.1244, 164.22, -650.929, -650.441, -484.725, -345.561, -506.943, 136.692, 180.09, -731.376, -639.698, 905.087, -83.7123, 692.557, 13.4587, 233.009, -929.258, -686.697, 327.799, -486.19, -177.282, -491.379, 592.883, -630.604, -89.938, 185.705, 364.116, 314.493, 606.128, 570.36, 564.44, -329.325, 946.226, 246.071, -164.281, 730.583, 204.138, -557.054, -859.554, 171.789, 618.946, -422.895, 613.208, -279.031, 371.136, 39.3384, 662.16, 67.9037, 809.503, -765.618, -679.25, -970.458, -72.3594, -129.978, -150.67, -35.6761, 90.4263, 496.017, 586.657, 435.591, 10.712, -706.778, -948.851, -873.592, 975.768, 112.827, 293.008, -996.826, -676.26, 764.946, -504.685, 729.85, -829.279, -441.816, 825.922, 331.828, -613.33, 88.5342, 538.682, -889.706, 822.321, 278.298, 325.297, -193.884, -214.393, -334.758, 916.807, -986.206, -231.483, 941.893, -64.9739, -154.21, -576.708, -256.02, -5.70696, 846.37, 789.117, 690.298, -947.142, -844.356, -231.239, -328.898, -985.412, -75.8995, 855.037, 387.677, 580.554, -24.0791, 107.456, -859.127, 350.2, -276.467, 378.704, -129.246, 173.498, -668.325, 791.253, -554.857, 752.251, -868.831, 463.912, -801.08, -508.652, 283.364, 488.388, 723.258, 600.94, -306.314, 155.858, -95.7366, -552.599, -823.664, -566.454, -787.652, -844.6, 867.977, -308.695, 241.371, -541.002, 891.537, 677.969, 497.299, -196.326, -729.118, -251.808, 437.666, 697.378, 94.5769, -661.061, 760.43, -687.429, -70.2231, 695.975, -56.856, -662.465, 486.557, -149.571, 31.8918, -921.995, -468.184, -154.515, 680.532, -191.382, 53.56, -787.225, -560.595, 374.676, -325.419, 699.087, 295.022, 43.4889, 316.874, 242.714, 458.419, -393.841, 115.635, 200.354, -595.325, 728.996, -416.974, 823.359, 161.901, -444.99, 22.7973, -15.656, 850.215, 555.834, 193.64, -845.943, 470.687, 632.679, -776.055, -372.417, 944.823, -531.419, -224.586, -999.084, -596.973, 129.49, -542.711, -102.084, 724.418, -84.0785, 490.341, 946.348, 325.053, -535.325, -379.009, 4.66933, -802.362, 100.314, 285.44, -361.126, -799.615, -563.524, -976.501, 848.567, 595.874, -772.027, 142.979, -956.236, -159.642, -528.123, 633.412, 271.584, -377.117, 759.331, -669.668, -307.413, -846.004, 876.888, 329.386, 307.352, 705.313, -341.594, 650.136, -66.744, 398.053, 86.3369, 150.304, 430.769, -757.622, -356.609, -310.038, -819.88, 426.618, -85.9706, -454.756, 808.1, 856.014, 436.201, 72.7866, -835.932, 941.832, 802.728, 623.402, -452.925, 640.614, -987.243, 706.595, -817.866, -256.813, 73.8853, -595.935, -53.0717, 413.556, 852.901, -965.514, -651.967, -21.9428, -821.955, -933.164, -251.93, -35.432, -809.748, 284.951, -158.3, -608.631, 944.761, -798.7, 382.55, 306.253, 262.49, 721.366, 159.52, -424.299, 832.148, -942.747, 223.609, 923.765, 561.083, 892.392, 364.299, -778.741, 177.465, 794.794, -163.915, -975.158, 81.4539, 881.832, 335.307, -634.022, -553.087, 733.94, -773.858, 790.094, -427.046, -441.389, -981.994, 645.497, -514.512, 604.968, -908.628, 622.547, -211.158, -173.07, -325.358, 542.894, 767.205, 798.334, 70.7114, 398.053, 736.259, -123.997, -866.573, 639.088, 644.765, -385.357, -5.52385, 715.079, 926.206, 430.342, -960.631, -137.425, 600.024, 514.756, -865.413, 144.688, 623.585, -0.640889, -896.359, -260.353, -506.455, -630.604, -545.824, 480.636, -902.646, 127.903, 353.923, -355.937, 814.997, -290.262, -142.186, -10.651, -129.734, 645.863, 384.259, 47.3342, -67.2933, -22.9804, -558.458, 659.902, -31.0373, 918.271, 605.029, -231.361, -655.812, 772.637, 742.851, -545.946, -825.983, -904.66, -506.821, -775.262, -74.2515, 108.005, 990.478, -443.831, -730.033, -637.562, -158.055, -319.864, 182.47, 654.958, -645.436, -376.69, -704.825, -809.931, 11.0782, 496.933, -501.572, 995.239, 196.936, -964.843, 791.986, 583.972, -823.42, 384.869, -613.33, 870.846, 320.475, -855.892, -943.602, -665.09, 581.042, 651.479, 739.189, 434.065, 219.703, 525.132, 388.836, 268.899, 783.319, 451.765, -923.338, -502.976, -669.546, 225.867, -656.972, 497.055, 908.628, 38.5449, -201.147, 782.464, -719.962, -983.337, -22.8584, 665.761, -101.23, 12.1158, -565.538, -827.326, -862.972, 640.614, -897.214, 206.336, -238.197, 570.666, 890.927, 960.753, 661.55, -936.888, -464.156, 585.498, 822.382, 759.819, 704.703, -900.571, -444.99, -277.322, -654.103, 555.04, 971.313, -277.017, 291.116, 452.01, 710.44, 780.206, -893.49, -598.254, 915.952, -48.7991, -805.536, -958.556, -538.011, 369.243, -854.854, 197.913, -569.933, 533.067, 337.931, -698.538, 835.444, -153.966, 793.146, -949.461, 603.809, 405.072, -726.493, -674.612, 797.845, -960.448, -364.238, 352.214, -906.308, -398.968, -567.858, 117.161, -492.294, -498.581, 762.2, 30.427, -627.308, 502.792, -626.148, 840.51, 564.989, -244.179, 15.5339, 797.845, 980.285, -910.52, 37.2631, 69.4296, 260.964, 549.669, -757.317, -597.644, -227.943, 188.208, -9.85748, -961.791, 828.364, 102.39, -455.367, -123.02, -829.707, 723.563, 679.128, 891.232, -308.695, -643.727, 526.17, 789.972, -586.352, 967.772, -320.414, -67.1102, 715.445, -773.797, -454.085, 494.491, 807.001, -209.265, -53.9872, 877.987, -542.467, -150.365, 966.613, 379.192, 971.801, -89.877, -938.353, 101.169, 416.12, -163.182, -595.447, 117.649, 95.7366, -858.821, 488.876, 887.509, 280.862, -479.537, 980.468, 918.638, 183.203, -353.191, 818.72, 682.119, -817.133, 206.153, 568.407, 776.177, 304.727, -812.494, -978.881, 340.251, -55.1469, -578.051, -953.246, -280.74, -152.684, 371.563, -304.849, -622.608, -669.362, 168.676, 547.655, -937.681, -235.572, -95.1872, 318.583, -780.816, -704.215, 169.774, -292.947, 654.775, -174.047, -159.215, 498.215, 451.338, 236.915, -878.658, 915.036, 131.016, -131.993, 830.683, -780.328, 69.7348, 242.347, -57.7715, -434.675, 160.985, -956.664, -626.759, 97.5066, 730.461, 341.594, 23.7129, -694.205, -300.516, -929.014, -934.874, 625.233, -0.335704, -244.301, 479.965, -711.966, -763.482, -513.596, -149.571, -574.816, -396.161, -852.962, -602.344, -582.263, 47.4563, 891.476, -729.118, -501.328, 679.556, -130.467, -913.755, -395.734, -557.482, 204.382, -283.914, -524.155, -976.684, -781.671, -672.231, -988.891, 17.304, -90.1212, -326.945, 865.902, 342.204, -319.926, 626.881, -694.51, 902.463, -690.909, 976.623, -693.35, -496.994, 182.836, -539.109, -91.3419, -986.328, -230.567, 59.1144, -184.851, -216.956, -653.981, -470.077, -754.326, 302.225, -25.1167, -799.249, 37.2021, 119.541, 730.522, -993.408, -4.36415, -995.972, -824.335, -220.74, -502.548, 359.966, 88.5342, -213.782, 341.411, 890.561, 438.581, -480.026, 849.666, 512.07, 613.758, 736.808, -780.206, 518.113, -103.366, -254.006, -873.836, -6.56148, 191.259, 948.973, -428.449, 478.378, -0.518815, 628.101, 988.647, 882.26, 647.572, 986.023, -408.612, 648.061, 762.932, 434.553, -22.8584, -833.674, -999.512, 133.824, -22.248, -560.839, -980.651, 781.426, -238.807, -5.768, 709.403, -348.918, -816.889, -951.231, -332.438, -117.649, -611.255, 924.192, 924.863, 19.0741, 663.991, 709.159, 922.666, -201.209, 537.034, -825.312, -260.903, 191.687, 130.406, -62.4104, -433.882, -310.343, 761.956, 874.691, -564.44, 845.515, 796.686, -213.294, 196.02, -938.414, 748.283, 312.845, -260.842, -370.342, 630.787, 94.2106, 476.669, -981.017, -440.107, -811.701, -933.714, 416.181, -395.184, 732.963, 388.897, -486.251, -110.446, -139.744, -225.379, 549.547, 944.7, 860.958, -520.249, 595.508, -691.275, 598.071, -586.779, 96.8963, 380.047, 171.361, 113.01, -338.542, -482.04, 831.233, -27.253, -901.852, -605.274, 142.552, 718.558, 706.656, -403.302, 399.274, -864.925, 815.973, 476.302, -712.027, -957.274, -426.374, -746.818, 802.24, -476.363, 985.229, 366.253, 406.354, -601.55, -797.357, 74.7398, 449.934, -46.9069, -936.094, -689.993, -168.615, -85.1161, 881.71, 777.337, -87.4966, 679.678, -517.502, 306.986, 682.119, -212.317, -503.891, 456.16, -829.707, 650.868, 894.223, 360.332, 112.644, 528.855, 779.351, -165.014, 863.826, 755.73, -575.426, -272.805, 562.181, -783.319, 562.181, 516.831, -456.526, -925.413, -77.6696, -831.416, -994.873, -253.761, 23.0415, 233.375, 920.164, 129.307, -791.559, -800.409, 201.209, 78.6462, 938.292, 95.2483, -885.311, 133.03, -673.33, -661.061, -54.9638, -508.469, 931.639, -157.994, -91.7081, 240.394, 793.512, -640.492, 316.568, -170.934, 724.601, -907.834, -7.96533, -202.673, -152.074, 537.889, 930.906, 751.579, -613.697, 593.127, 217.383, -408.673, -298.318, 904.66, 188.086, 986.023, -416.303, -528.977, 845.82, -898.862, -210.791, 494.308, 523.606, -424.482, 831.172, 481.246, 799.005, -252.846, -982.482, 595.264, -342.082, -870.052, 740.776, -742.302, 908.933, 140.782, -983.642, 999.756, -153.172, -924.68, 272.378, -436.445, -832.636, -664.785, -611.927, -745.964, -869.015, 904.172, 983.398, 540.574, -23.2856, 74.8619, 957.64, -91.7081, 586.535, -574.694, 278.848, -40.315, -600.818, 45.5641, -977.355, -233.619, -425.459, 14.069, 661.55, -529.466, 407.27, -875.484, 603.626, -863.704, 991.333, -137.791, 981.933, -148.228, 133.58, 630.543, -306.619, -920.103, 518.296, -772.271, -312.052, 260.903, -879.147, 573.595, 507.614, 663.015, -127.842, 685.354, 75.4112, -515.61, 572.192, 905.148, 734.611, 205.054, 54.5976, 375.835, -462.386, -812.433, -399.518, 768.12, 785.333, 529.649, 602.588, -743.461, 815.729, -835.261, 479.171, -253.273, -956.664, 214.331, 838.252, 351.36, -751.762, 589.038, -108.493, -883.175, -885.067, 840.449, -881.344, 0.21363, 21.6376, -737.541, 181.188, -904.599, -920.896, 243.141, 86.4589, -890.194, -580.126, 866.085, 601.917, 90.9757, 737.358, -238.258, -81.0266, 403.058, 74.6178, 209.632, 567.431, -46.5407, 863.826, -510.788, -420.942, -330.668, 215.125, 482.04, 901.975, -775.811, 455.55, -477.401, 64.4856, -985.595, 918.394, 500.9, -910.459, 385.907, -18.0364, -787.164, -341.655, 110.508, -641.041, -37.2021, 524.644, -887.326, 635.365, 414.655, -821.284, -553.819, 59.2975, 693.35, -238.319, -479.843, -419.477, 736.686, -770.928, -423.627, 139.195, -103.183, -234.657, -584.826, -794.977, 791.681, -552.232, 372.539, 566.332, -593.371, -839.534, -453.841, 368.999, -553.941, -241.432, 272.5, -594.226, 934.996, -517.93, 852.901, -344.768, -929.746, -379.803, -469.588, -929.38, -677.114, 425.214, -283.609, 619.312, 788.751, -723.441, -533.067, -428.632, -209.449, -904.904, -790.643, -960.753, -270.486, -993.103, -280.923, -233.741, -813.227, -742.668, 345.805, 165.929, 274.941, 375.408, -509.751, 611.072, -760.491, 587.512, -669.057, -27.253, -689.138, -518.479, 800.409, 409.406, -194.617, -78.7683, 811.762, 405.866, -522.629, 209.876, -264.443, 62.4104, 120.212, 504.501, -873.775, 634.022, 418.317, -519.944, -301.431, 319.01, 48.4329, -756.645, -445.845, 298.563, 31.3425, 497.177, 658.437, -974.425, -666.494, -45.8693, -433.515, 763.726, -281.716, 1.12918, 400.555, 376.385, 847.896, 613.086, 75.6554, 366.07, -365.337, -482.833, -223.182, 163.121, -157.201, 606.8, 685.415, -335.002, -60.0909, 605.64, -44.4655, -974.731, -801.569, -18.0975, 940.977, 692.74, 47.4563, 112.094, -328.654, -299.905, 674.001, 695.364, -420.942, 203.772, -125.34, -582.812, 475.448, 272.256, 157.323, -135.96, -222.327, 925.596, -907.407, -313.211, -14.8625, -575.793, 915.464, -984.863, 406.232, 808.161 };
						static float s_var[8192] = { 21.6742, 91.3846, 80.0623, 38.6731, 88.2046, 27.9427, 196.301, 91.1771, 14.6489, 23.2307, 16.3701, 156.255, 56.9292, 185.205, 154.149, 138.383, 172.6, 86.5444, 155.98, 84.5119, 151.317, 138.89, 68.6972, 95.7549, 20.3192, 42.0118, 82.5648, 114.127, 124.882, 108.084, 49.2264, 184.246, 83.7855, 60.86, 74.7276, 190.057, 19.8004, 109.922, 187.439, 66.5731, 65.1387, 99.8749, 81.0144, 86.5688, 141.935, 134.587, 132.426, 22.2907, 17.6946, 76.3878, 94.8576, 182.22, 35.8043, 120.304, 65.7369, 122.733, 120.945, 43.263, 129.643, 34.8582, 59.6026, 128.343, 76.3268, 6.00604, 55.5315, 163.939, 193.664, 156.652, 160.21, 118.027, 135.588, 52.0585, 178.301, 87.3623, 65.1265, 189.038, 48.1521, 121.72, 72.3106, 157.604, 97.2442, 123.069, 67.1773, 77.7551, 172.655, 19.837, 147.728, 111.161, 46.5346, 16.1077, 42.8785, 47.8652, 96.5911, 115.842, 30.4697, 168.047, 61.5253, 122.098, 130.21, 178.698, 71.5232, 111.24, 171.63, 25.367, 153.728, 182.183, 137.077, 178.887, 12.6835, 80.5811, 179.675, 197.046, 85.8364, 98.0743, 116.3, 161.364, 73.9708, 46.4736, 195.532, 188.781, 146.709, 107.022, 140.629, 18.366, 22.9438, 154.57, 67.5192, 73.0735, 126.371, 156.822, 77.7367, 196.423, 15.8696, 48.5488, 126.408, 64.2293, 91.0367, 80.813, 165.215, 71.3462, 172.649, 45.1674, 160.369, 93.35, 158.086, 172.893, 185.577, 123.27, 151.866, 109.207, 190.002, 183.325, 71.0593, 162.493, 41.2488, 111.606, 125.791, 28.5043, 43.3241, 23.835, 174.682, 189.599, 129.606, 50.0626, 155.345, 188.946, 130.79, 146.391, 189.843, 179.742, 117.911, 181.671, 165.142, 149.455, 20.2521, 99.2523, 188.61, 19.0008, 118.369, 153.716, 118.455, 17.0049, 12.006, 22.5166, 169.213, 129.002, 63.1062, 192.816, 192.224, 21.6315, 113.755, 23.0659, 31.0617, 41.908, 79.165, 79.8608, 152.068, 91.3968, 15.6316, 149.571, 149.748, 175.451, 63.5151, 125.071, 127.287, 27.4178, 65.1875, 167.498, 152.007, 7.88598, 58.0401, 142.924, 185.839, 78.8659, 42.4024, 134.117, 90.7682, 53.029, 24.5491, 27.2164, 109.354, 117.942, 134.666, 173.498, 20.13, 35.2428, 19.7821, 126.048, 168.145, 14.9052, 111.795, 51.503, 170.36, 64.4246, 170.397, 16.7852, 96.1821, 13.0619, 101.041, 150.798, 122.892, 11.0355, 47.9446, 81.1365, 66.1214, 103.769, 162.053, 45.8327, 185.345, 81.0755, 43.9528, 80.0623, 64.2903, 1.40996, 61.916, 77.8649, 166.491, 191.082, 144.823, 128.849, 65.6758, 118.448, 50.1419, 129.942, 151.292, 143.156, 106.9, 72.0664, 8.94192, 140.361, 33.6558, 1.90435, 75.8751, 199.939, 31.4524, 58.5894, 171.435, 0.689718, 151.805, 185.418, 102.536, 59.6088, 139.665, 116.581, 144.151, 56.0259, 151.366, 38.8989, 194.751, 30.903, 147.636, 128.507, 6.78121, 61.0004, 161.528, 162.603, 92.1537, 8.11182, 41.4014, 6.12812, 102.683, 143.297, 68.8986, 173.553, 85.5617, 46.0585, 51.912, 65.511, 13.8615, 178.631, 92.5321, 80.5872, 191.968, 135.118, 30.0363, 2.00201, 136.039, 165.783, 158.873, 59.3341, 129.521, 176.208, 21.5766, 65.2547, 53.5722, 1.56255, 76.8029, 27.2225, 92.5565, 155.797, 42.2498, 34.2174, 114.499, 55.855, 112.638, 18.4698, 126.572, 120.908, 52.6811, 192.145, 5.53606, 146.562, 165.087, 159.099, 103.238, 154.094, 133.451, 101.566, 127.067, 114.646, 140.3, 41.9141, 95.4131, 148.466, 60.0177, 179.333, 168.151, 199.115, 126.847, 101.547, 152.104, 182.684, 77.81, 71.5659, 81.5088, 12.0182, 170.721, 189.392, 73.5008, 95.9502, 77.1325, 28.7118, 58.5467, 113.474, 50.325, 148.668, 185.968, 63.0085, 168.249, 80.5017, 104.343, 186.615, 51.503, 189.367, 117.032, 169.164, 56.3128, 10.9561, 12.6225, 12.6286, 142.503, 63.2954, 170.409, 185.26, 16.4312, 133.409, 140.825, 45.7778, 149.01, 39.0515, 100.961, 142.674, 68.6117, 65.4622, 185.022, 188.348, 129.112, 77.0287, 89.7549, 108.359, 152.794, 104.05, 154.973, 165.661, 148.344, 4.41908, 118.992, 159.795, 103.824, 0.37843, 149.113, 101.108, 54.6342, 90.2127, 147.081, 160.002, 26.4901, 134.971, 129.551, 91.1161, 53.3647, 20.1849, 83.5719, 132.572, 70.4855, 92.6237, 97.0489, 180.169, 82.3084, 71.9321, 164.214, 36.6222, 5.4445, 37.4401, 66.0665, 135.783, 186.682, 89.8221, 125.901, 141.832, 195.904, 87.8323, 39.7168, 23.5237, 94.1069, 60.0299, 181.726, 3.39976, 176.336, 91.4945, 184.796, 182.018, 189.648, 85.5373, 195.935, 37.1166, 26.1116, 0.421155, 6.75069, 197.925, 90.7682, 161.101, 58.9496, 94.2839, 144.566, 45.9059, 124.741, 80.9534, 22.3701, 148.296, 62.3676, 36.7565, 25.9163, 113.547, 46.5529, 38.8623, 191.778, 150.487, 73.9586, 111.948, 34.315, 118.216, 143.669, 52.4613, 163.713, 87.8262, 112.656, 127.805, 178.857, 179.107, 26.2398, 50.5936, 35.3526, 72.2678, 123.576, 27.8329, 88.5098, 194.696, 118.094, 85.165, 11.2735, 182.586, 153.001, 66.0726, 187.225, 17.7496, 31.7209, 106.113, 178.497, 39.3017, 123.386, 182.122, 68.8131, 30.3598, 143.284, 136.436, 81.4234, 179.791, 8.34986, 176.116, 72.0603, 118.894, 167.626, 47.1633, 96.9268, 33.1736, 7.70287, 83.7489, 55.6413, 49.0127, 133.25, 132.395, 9.25932, 95.4131, 122.41, 151.25, 173.174, 40.6873, 157.341, 84.2555, 185.131, 104.227, 129.618, 10.3946, 48.1704, 143.181, 20.7465, 167.925, 148.643, 14.1667, 157.628, 53.5295, 149.101, 10.0345, 143.004, 196.625, 156.34, 171.557, 60.9394, 8.50856, 164.324, 74.1234, 157.305, 106.638, 172.179, 89.523, 195.209, 113.773, 177.142, 122.593, 85.5068, 132.273, 42.5367, 22.9865, 192.261, 93.2707, 40.7605, 71.8894, 113.169, 180.908, 71.218, 143.693, 135.099, 131.535, 153.331, 142.759, 164.495, 7.94702, 194.458, 115.134, 132.896, 48.616, 32.9051, 35.2489, 77.3705, 191.87, 155.889, 67.6412, 95.999, 75.1305, 92.4711, 103.696, 178.35, 109.153, 81.576, 173.327, 110.923, 27.2225, 14.9236, 131.736, 57.6556, 109.378, 55.0249, 21.0944, 15.363, 81.6126, 128.104, 107.297, 144.206, 90.1334, 92.1354, 191.656, 173.18, 150.542, 179.754, 197.626, 80.5567, 72.0664, 116.245, 32.5083, 49.3912, 56.801, 112.534, 124.229, 97.9583, 173.888, 133.11, 32.9661, 185.229, 108.744, 39.0393, 137.944, 191.327, 187.317, 5.90838, 199.115, 173.04, 75.2525, 16.7913, 193.89, 94.2473, 152.153, 30.2377, 28.8461, 31.3913, 160.772, 23.2185, 126.481, 53.975, 131.236, 109.122, 86.8618, 180.004, 180.279, 130.68, 24.427, 6.45161, 185.443, 127.995, 179.65, 14.7588, 134.306, 81.4539, 5.74969, 151.28, 148.704, 129.734, 130.485, 82.7357, 183.941, 62.7461, 192.761, 126.725, 2.99081, 89.6207, 154.454, 57.1551, 25.7881, 81.6736, 87.051, 184.741, 53.3403, 90.3653, 33.43, 112.436, 67.037, 140.281, 144.536, 165.947, 102.817, 97.0916, 91.1039, 99.1546, 0.396741, 10.535, 150.401, 74.6239, 27.5277, 132.365, 167.937, 119.059, 179.15, 9.32646, 194.159, 53.7492, 151.646, 51.1429, 196.197, 100.076, 192.083, 173.467, 44.5814, 159.954, 22.3762, 115.836, 174.957, 171.306, 38.5693, 34.6812, 124.778, 138.536, 184.991, 171.264, 145.067, 38.0016, 103.513, 194.635, 87.1181, 198.486, 157.775, 19.6478, 164.208, 156.621, 132.371, 99.8932, 188.305, 187.439, 7.0864, 195.672, 81.814, 109.763, 6.13422, 16.9561, 64.2964, 166.729, 122.33, 141.404, 114.158, 181.213, 116.355, 7.2512, 105.387, 186.346, 190.948, 181.359, 19.2755, 130.924, 140.129, 102.115, 31.1411, 59.9506, 173.431, 97.4578, 72.0237, 170.031, 15.0884, 77.8039, 65.4317, 139.891, 78.3044, 104.056, 39.0454, 135.82, 114.432, 30.5124, 111.435, 92.1659, 137.181, 22.9133, 153.783, 66.7684, 21.8757, 151.353, 126.603, 71.4682, 179.675, 58.2415, 91.464, 177.526, 106.723, 101.334, 97.702, 96.1394, 8.45973, 171.648, 144.139, 33.8939, 98.7884, 186.431, 15.7903, 23.2124, 174.291, 57.1551, 195.624, 127.866, 63.448, 132.493, 51.2223, 123.386, 150.908, 116.239, 96.5606, 52.5895, 176.629, 60.329, 128.66, 56.0198, 110.147, 32.7769, 39.3628, 77.4133, 99.7894, 60.6586, 102.036, 44.1664, 96.878, 148.07, 133.268, 122.825, 154.961, 169.109, 129.606, 49.5254, 4.00403, 35.8348, 179.284, 135.502, 99.6857, 141.252, 48.909, 147.496, 147.502, 77.2301, 45.2223, 63.2832, 102.084, 27.5521, 151.787, 102.121, 41.3648, 35.8165, 40.7117, 30.195, 115.812, 148.436, 159.38, 141.136, 172.655, 32.5877, 92.1537, 111.429, 55.0737, 69.216, 197.113, 130.937, 176.464, 120.957, 117.753, 108.335, 169.616, 175.146, 169.72, 151.598, 10.9439, 61.7512, 2.50862, 123.453, 19.7272, 46.382, 136.229, 71.865, 132.304, 98.233, 173.241, 135.49, 51.2711, 33.0332, 84.9574, 124.155, 69.7653, 131.071, 173.528, 174.322, 170.623, 30.1035, 108.542, 79.5495, 0.628681, 93.3744, 38.7707, 95.9868, 119.724, 52.5346, 178.887, 22.8095, 125.23, 125.657, 71.157, 157.714, 51.0575, 10.1749, 122.208, 127.372, 41.9446, 150.871, 14.6794, 192.901, 102.652, 82.8089, 194.024, 30.3293, 122.208, 49.3667, 30.1279, 186.102, 173.095, 95.1384, 54.1276, 7.47093, 85.4946, 33.9427, 70.0705, 48.0544, 96.4995, 91.7875, 141.649, 148.241, 174.328, 24.8054, 53.8957, 190.674, 17.542, 112.29, 49.9649, 130.747, 129.276, 24.5857, 42.7747, 102.029, 166.442, 166.124, 101.791, 196.789, 163.964, 93.3378, 14.6855, 121.384, 33.961, 7.67235, 124.119, 126.823, 140.043, 123.533, 39.3933, 14.1545, 146.422, 111.252, 23.2307, 14.9846, 142.228, 15.0639, 53.4623, 146.941, 40.669, 46.0829, 159.734, 44.0931, 53.2792, 50.2579, 35.255, 62.5019, 39.1186, 51.3321, 123.508, 60.6159, 129.008, 68.8498, 179.943, 54.7807, 24.72, 11.0721, 108.731, 149.705, 173.254, 42.9945, 74.4652, 143.895, 15.9795, 29.2917, 88.8821, 10.1749, 112.918, 52.2904, 72.5852, 51.1185, 141.935, 4.91958, 191.754, 129.533, 172.948, 58.7298, 69.8569, 173.919, 17.1758, 6.79952, 74.398, 158.94, 51.3321, 81.753, 78.6157, 160.906, 116.855, 48.6892, 98.2025, 88.2229, 94.1191, 173.199, 3.02133, 86.8862, 151.659, 64.272, 103.83, 20.9052, 70.3818, 192.34, 14.0019, 27.8268, 42.8053, 82.1619, 186.248, 149.168, 147.258, 180.871, 124.308, 35.8837, 97.0916, 0.878933, 69.7043, 128.855, 64.9251, 137.907, 163.463, 114.359, 181.848, 163.89, 82.7784, 54.5, 111.618, 194.989, 152.8, 192.737, 99.9298, 134.55, 133.311, 34.2906, 98.5992, 191.07, 80.4529, 101.675, 164.782, 71.1631, 83.4559, 165.111, 135.197, 47.5295, 76.3451, 148.015, 175.353, 101.938, 124.662, 107.486, 3.76598, 143.181, 187.201, 89.7488, 191.174, 100.027, 152.684, 60.6342, 134.739, 89.23, 151.531, 111.051, 107.297, 58.4796, 142.552, 137.443, 181.561, 104.343, 174.664, 197.272, 29.3344, 65.2791, 123.612, 139.47, 195.746, 189.581, 103.702, 109.922, 48.439, 78.3288, 154.503, 131.932, 1.83721, 103.421, 2.56355, 161.693, 16.3213, 100.461, 98.1536, 18.7384, 172.46, 136.79, 126.957, 191.449, 120.231, 154.424, 196.796, 117.466, 3.24107, 88.0215, 99.3744, 139.213, 57.9546, 57.8753, 46.9558, 134.349, 87.8872, 37.8613, 84.1212, 21.2043, 12.8422, 198.419, 174.462, 139.305, 93.1364, 124.497, 162.297, 104.794, 28.7851, 108.56, 9.42412, 140.922, 133.775, 118.522, 106.796, 83.5231, 185.223, 40.7361, 61.269, 61.4643, 147.642, 197.961, 157.317, 52.1256, 172.13, 142.308, 93.7162, 111.863, 70.6137, 123.252, 195.331, 81.4783, 65.041, 30.549, 99.765, 166.717, 11.6276, 12.2623, 119.132, 193.664, 100.046, 167.675, 76.7296, 139.299, 27.6559, 115.281, 18.7994, 136.9, 48.7503, 35.786, 1.21464, 156.56, 98.7274, 190.02, 151.219, 160.607, 193.341, 111.942, 4.1261, 134.117, 102.902, 76.6015, 115.061, 190.362, 101.523, 43.0982, 145.659, 151.268, 191.308, 72.5791, 25.7942, 2.90536, 1.57476, 21.1554, 190.307, 25.9651, 163.103, 107.938, 90.6949, 1.17801, 157.305, 157.927, 181.866, 89.8526, 40.492, 120.06, 183.831, 189.276, 43.0677, 60.1276, 18.421, 163.231, 43.7452, 28.7667, 21.0517, 147.447, 118.772, 4.41908, 27.369, 130.076, 166.161, 111.307, 135.032, 7.14133, 4.87075, 112.387, 140.385, 113.047, 149.089, 177.337, 50.2152, 25.9774, 57.3687, 125.517, 68.453, 48.4512, 56.9231, 143.937, 57.8692, 162.731, 142.802, 180.236, 76.8456, 61.2995, 52.1195, 10.6143, 191.04, 52.3515, 13.2694, 182.879, 160.137, 40.2478, 170.165, 34.3028, 199.567, 180.981, 81.4173, 184.71, 17.2002, 12.6347, 94.5952, 104.349, 125.101, 60.8966, 195.428, 156.92, 26.2093, 119.48, 131.999, 117.521, 76.0826, 5.59099, 14.0202, 165.618, 19.6173, 190.448, 187.011, 153.008, 2.74667, 104.691, 181.506, 146.818, 59.1937, 118.9, 137.309, 47.4319, 72.1397, 199.023, 102.689, 81.4844, 10.9928, 36.8603, 8.61232, 169.829, 43.0799, 55.5559, 117.954, 170.495, 120.371, 90.0296, 149.712, 196.661, 41.2, 142.46, 78.2189, 9.62554, 186.987, 12.7689, 7.40379, 86.7946, 133.927, 4.06507, 119.956, 164.611, 139.775, 103.128, 101.578, 56.5447, 105.484, 35.8715, 40.1807, 146.08, 11.9877, 197.821, 0.128178, 154.643, 31.8857, 59.5538, 71.4499, 184.594, 116.178, 47.2121, 94.0336, 37.2143, 55.9526, 97.9522, 56.8743, 5.63372, 19.1656, 90.3836, 172.289, 142.845, 162.304, 150.09, 25.1106, 4.60829, 117.112, 30.0485, 196.954, 67.513, 152.336, 166.155, 165.374, 21.8451, 10.4251, 7.61742, 78.1274, 139.634, 19.2572, 22.4982, 2.38655, 117.588, 78.8537, 178.155, 48.9578, 195.05, 123.087, 96.6704, 171.471, 147.002, 56.4165, 85.403, 40.6384, 152.007, 165.606, 42.3658, 193.463, 135.185, 6.4333, 36.7748, 57.4175, 186.187, 165.429, 164.47, 49.7391, 52.1439, 96.5423, 78.3715, 151.585, 105.679, 32.9295, 194.018, 157.262, 187.829, 173.644, 169.945, 198.468, 126.023, 145.323, 63.2344, 143.498, 127.079, 138.957, 159.929, 71.4499, 45.143, 121.58, 80.5139, 21.4728, 124.68, 122.99, 160.417, 16.9622, 18.4149, 197.491, 105.423, 164.788, 80.3369, 60.5609, 122.196, 29.487, 133.634, 0.634785, 123.698, 82.5465, 113.669, 65.7674, 9.69878, 26.545, 185.992, 125.394, 22.2358, 194.433, 139.225, 7.01926, 176.067, 23.7007, 53.1205, 107.95, 164.141, 5.64592, 41.4624, 82.0032, 138.072, 109.574, 70.0827, 1.25736, 163.634, 81.9788, 49.7818, 192.73, 151.189, 20.8258, 4.34584, 150.108, 61.1591, 24.7261, 113.486, 120.481, 184.539, 117.179, 30.8115, 166.051, 98.3062, 45.7533, 76.8029, 100.845, 95.938, 156.377, 106.79, 102.451, 92.8434, 142.467, 53.8957, 198.718, 6.86056, 69.2282, 147.032, 29.4748, 13.7028, 72.8111, 3.83312, 83.2728, 77.456, 134.587, 198.694, 9.0762, 156.017, 130.302, 105.625, 2.99692, 81.5577, 154.35, 16.3457, 70.0095, 116.037, 118.967, 133.177, 160.674, 181.646, 28.4555, 37.019, 182.83, 166.247, 128.41, 30.3354, 88.3572, 103.885, 129.911, 155.126, 34.8704, 30.2499, 108.768, 85.8669, 22.4311, 113.126, 79.7388, 97.5127, 5.28581, 57.8631, 45.735, 108.451, 78.5669, 187.872, 122.037, 198.462, 140.019, 0.0488296, 135.539, 1.69683, 26.722, 195.3, 98.9349, 174.664, 126.804, 183.953, 159.96, 139.14, 66.158, 66.2618, 25.605, 100.223, 70.1743, 87.8811, 61.33, 139.225, 0.433363, 170.293, 65.2791, 181.689, 66.4632, 116.324, 117.521, 40.3211, 12.4149, 90.585, 104.233, 133.012, 178.808, 174.944, 116.959, 75.7286, 39.2346, 199.011, 126.786, 150.572, 12.1158, 45.5641, 133.604, 48.6648, 147.362, 31.0984, 190.301, 88.1802, 88.1252, 161.199, 101.981, 107.273, 107.529, 61.0736, 0.189215, 32.0566, 20.3497, 181.005, 192.151, 119.596, 169.781, 76.8151, 84.9269, 75.8446, 89.1018, 165.337, 184.716, 48.6343, 38.4533, 154.881, 171.001, 95.6206, 16.5044, 150.713, 45.8266, 127.28, 47.1938, 35.4991, 157.982, 124.863, 141.459, 124.815, 102.206, 33.3628, 133.934, 97.5677, 93.4599, 129.496, 194.678, 1.50761, 16.0894, 64.6382, 15.8879, 133.665, 148.088, 182.842, 73.6534, 195.831, 160.283, 113.761, 164.605, 125.028, 190.564, 40.6568, 17.6946, 198.987, 193.121, 43.3363, 180.902, 16.5777, 162.627, 116.129, 182.91, 153.343, 33.491, 60.8661, 169.884, 91.8973, 54.0605, 54.8479, 17.1697, 27.4483, 35.8409, 93.8017, 115.989, 148.253, 77.7123, 109.842, 35.1817, 191.046, 165.154, 164.483, 57.9241, 71.5598, 44.4533, 116.55, 193.432, 190.002, 171.953, 165.197, 63.6982, 11.4383, 138.224, 67.8365, 65.1997, 121.384, 167.327, 33.1431, 5.62151, 156.163, 90.9879, 88.992, 15.5705, 8.44142, 197.888, 125.443, 135.307, 107.675, 157.781, 12.714, 176.434, 175.573, 20.8075, 194.153, 58.6383, 56.5874, 91.6654, 154.155, 21.4362, 23.3589, 78.8171, 161.217, 110.837, 157.207, 118.76, 85.8425, 141.081, 3.55235, 75.1793, 93.5392, 131.382, 102.475, 113.224, 12.7689, 17.6153, 194.086, 148.698, 177.935, 39.0698, 99.0448, 172.277, 147.325, 0.122074, 74.0379, 48.7381, 96.1211, 6.99484, 42.2193, 191.174, 84.3226, 79.458, 98.6847, 110.202, 199.762, 37.5439, 90.9696, 38.2092, 176.019, 63.2771, 74.4652, 28.547, 85.4274, 137.394, 42.9456, 129.752, 107.053, 151.048, 133.995, 34.7728, 169.622, 92.526, 109.482, 5.18204, 69.5212, 109.006, 111.051, 70.4184, 63.7593, 43.2203, 90.5606, 101.962, 137.931, 45.2162, 115.586, 162.59, 149.187, 59.0533, 149.852, 130.57, 44.203, 184.533, 108.725, 198.92, 152.159, 113.895, 189.691, 49.5499, 152.69, 43.5743, 77.7795, 102.591, 118.87, 60.9027, 26.0811, 40.492, 35.1634, 5.80462, 11.2064, 7.33665, 190.423, 7.55028, 163.726, 83.0226, 45.8205, 96.4629, 73.6534, 32.4534, 177.85, 90.6095, 170.952, 129.82, 96.2676, 157.567, 180.944, 57.7593, 137.095, 51.1124, 54.799, 81.6187, 30.549, 22.956, 175.048, 98.6297, 144.041, 9.43632, 35.963, 162.181, 0.11597, 93.2646, 199.133, 80.9839, 69.4723, 40.2234, 123.814, 93.3317, 38.8623, 110.916, 72.5181, 16.2908, 154.112, 147.435, 91.3907, 69.9423, 38.0749, 135.124, 65.7186, 176.58, 183.599, 39.2956, 50.3983, 134.922, 121.488, 116.916, 75.4051, 69.3869, 63.4846, 103.36, 26.2337, 154.234, 20.2399, 74.1111, 199.42, 12.8361, 194.269, 181.475, 132.951, 191.888, 47.908, 66.3106, 111.777, 110.587, 46.3332, 144.761, 148.271, 84.9269, 75.4784, 169.939, 37.672, 26.838, 135.026, 68.6666, 88.5952, 18.4576, 106.363, 53.1877, 128.465, 161.339, 195.581, 7.9104, 184.008, 100.638, 51.3932, 151.079, 36.6894, 24.3782, 158.165, 171.526, 53.2304, 137.657, 103.824, 40.3943, 91.0428, 11.8168, 122.88, 97.8362, 78.7011, 8.7466, 114.341, 14.7404, 71.4133, 180.035, 134.733, 29.4565, 181.5, 41.2122, 166.039, 59.5111, 183.844, 20.9662, 158.666, 88.3145, 196.381, 185.382, 149.431, 26.899, 35.548, 37.1044, 54.5244, 50.618, 46.7727, 189.636, 137.468, 10.889, 9.92462, 116.471, 152.098, 121.885, 119.797, 175.402, 160.521, 122.269, 41.5418, 107.651, 26.0811, 111.917, 41.2061, 12.1586, 181.365, 63.6189, 191.137, 194.769, 71.102, 9.81475, 50.4654, 92.4345, 70.2109, 104.318, 20.6061, 72.4876, 76.7052, 158.995, 175.689, 106.796, 179.699, 55.5376, 168.041, 121.03, 2.34993, 32.1421, 8.65505, 18.8604, 119.871, 45.2528, 122.684, 181.17, 152.44, 6.2624, 79.8425, 40.2905, 133.207, 93.2402, 199.963, 129.209, 151.268, 137.651, 33.3811, 5.79241, 69.6005, 86.0561, 116.471, 52.0096, 118.07, 136.882, 2.06915, 148.137, 161.376, 152.074, 164.983, 21.9794, 42.0057, 143.852, 182.403, 73.1162, 198.26, 36.433, 189.434, 72.0725, 157.152, 102.744, 9.50957, 134.587, 102.426, 76.1132, 159.929, 170.794, 117.502, 137.04, 132.231, 9.44243, 103.452, 174.859, 147.758, 197.174, 171.978, 124.509, 188.055, 72.6341, 113.352, 178.222, 100.369, 115.812, 139.421, 72.2373, 178.137, 172.155, 101.541, 48.5427, 16.3945, 151.854, 183.593, 91.586, 42.3231, 4.03455, 69.8813, 39.9731, 120.64, 165.99, 188.177, 173.229, 34.1258, 87.3684, 27.6315, 73.4519, 39.0637, 29.4382, 131.785, 162.12, 95.7244, 139.28, 166.497, 111.722, 173.815, 198.999, 62.9414, 132.536, 32.9661, 49.9527, 35.5785, 134.312, 78.2006, 91.9889, 75.1061, 163.024, 90.3836, 57.6189, 116.428, 112.558, 31.7087, 61.2812, 81.6431, 111.441, 126.176, 123.545, 197.998, 184.582, 15.009, 170.312, 190.851, 189.947, 187.201, 107.053, 151.665, 30.311, 84.9147, 168.029, 80.9412, 139.982, 84.0052, 140.507, 106.229, 46.4736, 149.443, 63.9607, 175.06, 7.28782, 165.807, 4.32142, 22.9255, 121.592, 142.753, 103.171, 16.2603, 116.929, 95.4802, 101.962, 70.4001, 10.9684, 0.195318, 141.258, 42.5306, 48.2192, 24.5918, 34.9315, 199.713, 111.728, 33.2957, 19.3854, 161.779, 9.80865, 168.365, 177.789, 119.358, 37.7697, 104.733, 139.262, 126.817, 14.6245, 180.7, 53.1266, 70.2414, 194.818, 3.36924, 38.1237, 110.398, 54.9089, 129.167, 106.607, 0.701926, 155.358, 19.245, 163.781, 80.1965, 77.1752, 70.0095, 81.9117, 141.508, 33.6924, 118.558, 49.5621, 173.809, 157.256, 23.1452, 195.795, 180.816, 60.9333, 118.686, 89.3704, 73.7266, 75.2647, 147.172, 113.028, 39.2285, 159.966, 63.1733, 112.638, 182.409, 90.6278, 114.328, 121.458, 44.7768, 120.475, 196.759, 159.349, 197.205, 117.051, 85.5617, 106.973, 107.901, 173.962, 140.312, 55.031, 85.3847, 97.5555, 85.4701, 128.495, 96.4568, 168.694, 65.9017, 185.772, 59.1571, 179.443, 177.331, 192.999, 10.7425, 144.768, 71.7795, 73.1895, 150.652, 89.3094, 38.081, 30.5063, 158.47, 109.061, 157.341, 149.455, 186.901, 108.078, 133.36, 69.0939, 129.752, 10.2359, 198.163, 132.286, 164.879, 45.9365, 92.4345, 9.87579, 172.161, 113.785, 121.006, 199.957, 26.4657, 17.3772, 83.285, 2.02033, 165.654, 176.971, 78.7866, 166.265, 33.4849, 16.1992, 187.542, 17.7313, 197.644, 23.8411, 25.5989, 55.5803, 142.662, 90.4935, 175.506, 27.2713, 137.931, 120.286, 91.7264, 77.8039, 180.645, 117.637, 81.1548, 173.943, 13.7638, 148.949, 158.592, 3.39366, 19.013, 179.327, 177.154, 149.089, 184.814, 184.295, 33.8023, 138.347, 149.272, 136.528, 149.809, 167.125, 118.149, 67.568, 26.1605, 99.2096, 57.8326, 4.52284, 74.1111, 140.483, 163.286, 181.829, 120.609, 188.079, 1.03763, 127.897, 188.745, 54.6525, 127.146, 26.9112, 153.27, 167.589, 180.157, 155.51, 71.0349, 143.187, 49.0799, 194.482, 186.456, 35.9203, 116.367, 15.5217, 11.9877, 62.6545, 175.86, 142.961, 83.6207, 1.82501, 25.7759, 90.4141, 191.296, 67.7023, 136.68, 172.594, 84.4142, 59.5782, 194.427, 191.943, 52.3026, 186.01, 118.155, 0.408948, 179.662, 164.006, 153.252, 136.161, 77.0409, 193.292, 39.3139, 135.258, 86.3308, 5.47502, 53.1327, 118.43, 161.205, 188.33, 126.634, 52.7482, 22.5898, 184.924, 39.8633, 88.3938, 189.16, 167.937, 88.9737, 170.324, 82.7662, 188.806, 29.1574, 125.01, 8.94192, 1.9837, 92.6481, 22.1809, 12.36, 164.147, 81.6614, 11.5177, 7.28172, 8.38649, 136.381, 34.3699, 183.056, 93.4965, 177.929, 10.4678, 84.5546, 128.111, 26.3131, 99.6857, 167.681, 169.976, 193.445, 38.6975, 29.8105, 100.15, 151.024, 152.745, 115.262, 172.234, 73.6045, 42.3353, 95.7854, 36.5673, 56.4898, 144.963, 125.993, 39.4787, 8.7405, 171.825, 173.876, 197.473, 21.131, 19.5441, 26.2276, 46.4553, 9.10672, 156.89, 169.152, 165.996, 30.781, 86.4895, 143.504, 178.466, 79.873, 81.5577, 175.365, 23.3345, 153.697, 197.607, 81.5149, 106.351, 196.124, 159.16, 49.9222, 123.746, 115.543, 121.482, 95.1567, 161.498, 77.8649, 107.303, 188.916, 69.509, 74.4346, 118.516, 65.4622, 110.849, 165.044, 122.916, 102.103, 85.9584, 195.886, 115.903, 74.6605, 131.889, 192.517, 57.21, 187.927, 196.484, 27.839, 82.6258, 181.14, 187.64, 165.062, 190.704, 108.469, 197.504, 75.6615, 4.46791, 194.464, 1.68462, 132.377, 8.18506, 164.525, 33.4971, 177.142, 59.0228, 50.3677, 33.1553, 164.086, 173.791, 70.5283, 172.002, 77.4804, 190.661, 185.772, 51.7167, 124.735, 105.893, 129.6, 178.57, 4.31532, 85.5495, 113.736, 112.583, 48.7869, 152.208, 8.53908, 91.818, 42.7015, 146.678, 198.529, 131.529, 115.641, 164.727, 112.803, 198.425, 110.111, 68.7948, 30.5551, 59.859, 24.5918, 74.7337, 161.522, 8.08741, 131.437, 13.4831, 1.42827, 59.4378, 169.884, 67.8365, 162.511, 28.5714, 13.9958, 184.112, 78.9148, 77.6696, 15.6621, 158.91, 187.243, 140.678, 22.5349, 0.811792, 181.951, 22.5043, 71.0105, 167.138, 145.866, 173.266, 154.21, 75.5699, 155.638, 137.449, 64.9251, 90.994, 162.206, 78.2189, 113.608, 187.133, 146.269, 176.55, 121.323, 83.2728, 148.753, 105.49, 87.7285, 56.032, 176.293, 179.388, 134.05, 26.0323, 181.683, 178.076, 121.012, 119.639, 119.34, 46.1074, 141.856, 105.655, 75.8141, 67.0003, 90.2921, 107.083, 75.2098, 112.406, 103.446, 187.805, 159.679, 166.369, 79.8486, 14.3132, 144.877, 124.516, 72.4143, 192.444, 50.441, 68.3004, 156.377, 79.0613, 5.46892, 14.2033, 172.295, 68.8314, 28.9621, 137.98, 143.986, 155.87, 85.3175, 73.8182, 171.538, 170.531, 14.771, 88.6258, 199.219, 91.0672, 138.896, 131.217, 58.5101, 30.3903, 141.057, 156.523, 186.029, 97.7752, 21.8696, 124.833, 16.5105, 135.575, 129.691, 75.985, 50.795, 130.931, 66.0665, 78.6218, 198.767, 102.524, 66.213, 54.3779, 197.333, 62.2517, 54.4816, 77.1142, 103.397, 51.9791, 174.841, 147.124, 61.8427, 55.2507, 196.301, 40.3638, 148.479, 183.996, 193.451, 32.6731, 87.9971, 140.257, 101.859, 98.9776, 46.9802, 152.91, 148.601, 187.536, 101.852, 189.373, 157.683, 169.121, 181.823, 42.3414, 130.528, 61.269, 86.8923, 91.5128, 101.45, 127.488, 180.816, 140.251, 68.1234, 51.4725, 159.038, 127.378, 197.034, 186.334, 91.7081, 113.852, 103.964, 78.7927, 141.624, 144.481, 9.45463, 68.4469, 157.183, 184.674, 169.292, 161.931, 140.397, 132.121, 179.931, 182.183, 189.825, 103.537, 159.02, 192.627, 118.54, 196.387, 0.824, 196.423, 147.136, 125.571, 106.735, 110.66, 71.6514, 135.179, 190.704, 15.6011, 106.565, 185.192, 159.709, 78.8781, 2.75277, 13.184, 69.2648, 154.717, 40.4675, 71.102, 153.862, 58.1561, 97.293, 136.906, 132.42, 114.286, 191.125, 1.3245, 188.794, 182.434, 98.1292, 29.6091, 27.2164, 155.394, 45.7228, 198.175, 2.78939, 133.671, 80.5445, 6.38447, 126.914, 74.6666, 111.405, 63.2405, 147.795, 116.904, 27.2591, 149.113, 105.899, 179.272, 90.2005, 152.44, 164.422, 35.2, 3.39976, 81.4661, 114.756, 61.9221, 54.0849, 150.957, 156.969, 64.6748, 131.516, 143.919, 155.248, 175.903, 19.9408, 48.2986, 168.395, 29.487, 166.588, 168.737, 55.2812, 9.99786, 124.589, 113.254, 155.333, 176.861, 70.9983, 54.9883, 43.9589, 63.5945, 199.75, 68.9047, 163.091, 152.043, 189.581, 143.535, 126.56, 21.5766, 147.075, 67.6534, 100.644, 173.339, 24.5613, 192.047, 59.505, 129.417, 113.15, 66.1763, 115.653, 7.09861, 10.6143, 149.962, 125.413, 168.096, 64.4673, 0.44557, 169.427, 142.729, 89.8282, 133.549, 108.072, 29.1696, 162.45, 160.753, 11.9633, 167.669, 86.6848, 0.848415, 67.4276, 89.7366, 138.438, 166.662, 156.938, 142.405, 83.1324, 126.719, 114.609, 187.432, 68.6972, 142.039, 56.6729, 175.488, 193.518, 177.801, 186.926, 195.434, 149.413, 90.0662, 77.1874, 198.498, 139.604, 87.5271, 22.8278, 197.491, 71.6819, 68.1967, 73.1651, 141.063, 25.5806, 31.1411, 136.979, 62.8742, 196.551, 163.201, 162.957, 105.863, 113.083, 0.683615, 195.178, 21.4179, 129.954, 66.1275, 103.574, 143.638, 196.136, 179.98, 189.416, 38.496, 97.7752, 47.3342, 65.7308, 28.1381, 176.189, 119.663, 12.8483, 190.033, 120.804, 131.486, 8.79543, 31.2021, 37.1288, 112.149, 34.8094, 37.4828, 189.758, 72.512, 55.8, 97.6959, 20.2704, 166.674, 75.8019, 2.88095, 40.2722, 128.672, 68.4652, 20.5206, 199.329, 184.246, 182.501, 143.779, 127.915, 37.5011, 22.1625, 15.6499, 55.4399, 132.682, 2.73446, 128.282, 3.16172, 190.844, 180.969, 97.0916, 52.1867, 21.601, 160.546, 10.0467, 86.4223, 9.83306, 72.5913, 51.8876, 10.5533, 77.4133, 31.1045, 3.2899, 43.0128, 119.907, 63.4419, 15.1433, 83.9564, 118.32, 115.787, 57.7471, 158.196, 125.791, 133.598, 45.0148, 133.219, 129.325, 17.1087, 33.7168, 112.131, 186.517, 51.7838, 97.6959, 101.608, 37.2326, 13.7883, 77.0531, 154.906, 190.399, 150.438, 2.41707, 189.801, 85.641, 35.6944, 194.134, 81.5455, 130.07, 100.632, 36.4757, 30.8786, 9.94293, 38.8806, 26.899, 6.97043, 77.4377, 87.1303, 100.217, 65.389, 12.3234, 180.572, 27.839, 141.057, 158.727, 183.367, 92.0011, 116.257, 136.784, 194.147, 34.9742, 163.573, 129.441, 126.444, 83.2789, 163.854, 44.0992, 164.177, 46.8947, 199.219, 149.388, 92.1293, 93.5148, 120.969, 186.853, 101.401, 197.723, 192.615, 157.683, 69.8813, 65.2303, 7.80053, 184.423, 117.728, 132.121, 83.3094, 47.7126, 37.8124, 39.0027, 126.994, 197.986, 1.22684, 180.309, 180.773, 42.1888, 137.718, 128.452, 38.5876, 105.515, 191.168, 133.061, 139.39, 68.099, 50.0504, 95.23, 135.069, 76.7785, 152.953, 16.6814, 133.47, 131.748, 168.963, 124.052, 16.3213, 148.344, 153.191, 68.038, 32.2398, 93.7101, 177.245, 197.681, 64.6687, 198.572, 34.6446, 109.531, 174.871, 43.8795, 21.3691, 71.9138, 19.8553, 122.623, 191.266, 124.113, 145.421, 76.4428, 86.5078, 50.6058, 154.045, 7.45262, 143.144, 30.2805, 147.899, 64.8762, 171.471, 160.662, 46.8032, 106.821, 111.576, 57.094, 181.219, 36.9579, 63.7532, 127.012, 160.161, 16.2664, 194.012, 119.132, 14.2155, 149.76, 114.518, 147.661, 13.5197, 48.1521, 139.085, 191.54, 160.674, 64.6504, 13.6052, 68.6422, 158.751, 6.61031, 121.58, 85.0002, 54.2619, 122.306, 164.531, 159.044, 155.119, 162.597, 117.484, 20.7587, 23.5908, 117.685, 59.4623, 85.989, 144.31, 28.8278, 78.927, 5.58489, 67.1896, 75.045, 54.2619, 89.6695, 25.4402, 134.855, 109.232, 6.81784, 28.5043, 22.956, 143.455, 158.312, 30.0546, 1.99591, 37.8796, 127.323, 170.922, 169.268, 135.24, 3.17392, 97.1099, 118.4, 195.532, 180.529, 152.727, 76.1254, 26.4718, 33.5948, 47.9751, 113.974, 189.013, 65.389, 2.13019, 34.9803, 177.715, 131.932, 161.473, 135.447, 76.3207, 141.441, 92.2819, 41.5906, 88.1619, 28.5958, 148.814, 21.3324, 141.649, 3.72936, 116.239, 170.989, 170.714, 77.0531, 128.642, 76.9616, 70.3146, 57.8692, 61.977, 152.513, 126.908, 28.8095, 2.03253, 49.4339, 123.862, 95.0224, 187.414, 144.804, 121.989, 8.417, 27.4728, 102.67, 114.963, 66.8844, 195.361, 110.794, 159.844, 88.4854, 195.886, 192.364, 182.488, 60.7074, 8.56349, 192.712, 58.0523, 64.6565, 5.57878, 41.023, 166.466, 74.2882, 144.713, 87.0876, 22.8584, 30.6223, 37.3302, 112.961, 183.099, 190.973, 91.3785, 19.7394, 24.1768, 153.136, 129.313, 189.257, 199.768, 154.643, 81.0877, 120.798, 170.605, 46.968, 152.123, 142.814, 10.0162, 144.743, 66.2679, 199.67, 84.3776, 149.968, 183.782, 113.321, 8.5696, 151.256, 94.2839, 120.078, 76.4489, 98.0193, 84.7316, 82.2718, 166.326, 149.712, 82.4061, 175.03, 121.415, 100.046, 72.8416, 47.9446, 5.33464, 76.1864, 192.84, 61.8305, 32.5449, 113.022, 98.3978, 0.421155, 173.846, 2.3133, 7.70287, 57.094, 36.7931, 16.9744, 19.0985, 96.585, 21.2531, 110.257, 104.05, 191.723, 149.785, 189.978, 175.097, 35.1085, 173.15, 67.7328, 28.84, 25.898, 150.792, 83.1385, 163.927, 31.4646, 106.284, 3.01523, 73.9647, 24.134, 157.793, 195.636, 15.5583, 192.926, 123.576, 79.9097, 51.2284, 130.222, 2.41096, 49.6597, 167.406, 93.5514, 129.118, 72.7805, 67.3727, 142.247, 4.96231, 165.49, 39.2773, 149.211, 3.18613, 141.527, 99.0509, 87.5942, 149.431, 36.4086, 8.4109, 192.56, 177.221, 6.60421, 167.827, 197.949, 58.4429, 103.781, 184.478, 48.0483, 8.81375, 124.015, 115.213, 84.2921, 168.346, 132.011, 184.552, 106.949, 156.78, 60.3046, 177.618, 111.899, 104.117, 183.532, 108.139, 102.298, 156.047, 67.568, 161.388, 175.341, 54.4267, 193.835, 112.174, 83.9442, 192.755, 58.4429, 17.7862, 73.4947, 146.617, 146.464, 185.998, 193.542, 79.1894, 181.115, 17.9998, 84.5241, 21.6865, 47.9995, 126.389, 39.5154, 28.8339, 194.324, 128.135, 140.904, 9.33256, 103.085, 112.54, 72.1152, 31.3303, 42.3414, 184.222, 4.37635, 102.896, 89.3948, 81.0572, 158.379, 27.485, 30.0058, 67.2018, 41.0169, 134.41, 132.652, 100.992, 157.128, 84.2921, 197.693, 77.9565, 196.173, 149.565, 81.6309, 148.79, 93.9909, 169.909, 96.3103, 112.284, 169.5, 182.733, 83.1507, 83.3399, 136.399, 128.654, 162.34, 35.8959, 189.941, 31.9529, 157.085, 20.9357, 168.578, 56.0381, 92.7946, 59.4317, 190.747, 33.9366, 171.337, 110.312, 166.54, 73.7388, 20.2277, 113.45, 184.46, 39.0759, 16.8706, 107.608, 173.101, 104.593, 87.759, 23.4321, 37.9101, 186.425, 182.702, 151.396, 72.8782, 47.6272, 24.0364, 84.7865, 47.2549, 78.7011, 130.876, 137.663, 167.284, 153.02, 118.278, 37.8307, 112.912, 157.317, 71.4988, 94.821, 149.516, 197.143, 195.215, 42.8236, 114.615, 77.3339, 137.718, 158.708, 79.5862, 57.5152, 190.149, 54.9333, 86.3918, 82.5098, 160.723, 147.569, 159.288, 87.8567, 123.93, 199.243, 191.565, 180.255, 44.7584, 137.095, 107.224, 93.0326, 77.9382, 145.775, 97.7508, 116.648, 142.125, 35.3771, 133.793, 104.099, 169.127, 84.4508, 151.885, 151.457, 28.1381, 157.939, 116.178, 153.276, 124.967, 100.143, 155.162, 33.1065, 76.1986, 2.71615, 173.37, 48.9456, 58.4857, 147.63, 138.096, 173.711, 32.7219, 25.0923, 41.1451, 150.694, 43.2752, 26.5999, 135.508, 24.0364, 192.199, 12.8361, 47.0962, 144.23, 88.4548, 156.688, 161.492, 48.5366, 172.143, 61.1652, 85.6044, 16.48, 113.694, 152.434, 184.454, 100.327, 88.113, 147.71, 192.846, 30.5185, 124.204, 198.303, 102.091, 196.197, 128.751, 107.517, 121.775, 100.601, 185.766, 23.6763, 116.404, 91.6044, 130.705, 8.06299, 117.179, 167.144, 199.683, 77.8832, 108.683, 55.6963, 153.691, 124.253, 90.6156, 171.752, 77.81, 105.148, 14.5085, 46.2661, 55.4582, 113.395, 195.453, 32.1726, 117.563, 119.62, 198.785, 84.8048, 66.8172, 190.228, 1.84332, 17.3711, 176.891, 180.377, 30.6772, 31.4524, 166.631, 101.92, 71.7551, 122.446, 49.2874, 166.417, 58.681, 160.039, 56.7705, 103.055, 133.934, 43.6781, 38.8134, 151.659, 185.565, 133.048, 137.211, 130.631, 58.9312, 153.526, 135.051, 179.943, 43.4034, 176.659, 80.9229, 179.083, 34.0953, 75.6188, 188.141, 40.0342, 168.572, 55.6963, 8.94803, 75.2586, 138.572, 59.743, 83.7672, 40.6262, 176.733, 48.6709, 65.3829, 53.4257, 118.296, 156.89, 118.18, 185.314, 137.852, 122.776, 192.358, 130.284, 161.016, 194.183, 173.144, 147.636, 151.189, 108.86, 105.039, 79.9402, 71.2546, 149.944, 90.994, 46.028, 120.304, 76.4367, 66.6585, 8.93582, 45.3505, 128.66, 49.5987, 18.8787, 128.062, 108.713, 63.4968, 131.236, 46.437, 135.984, 137.394, 199.487, 11.5909, 77.5597, 179.876, 143.211, 179.437, 53.7492, 128.33, 91.0489, 137.37, 18.5125, 173.15, 198.669, 66.0604, 14.9541, 56.9353, 141.716, 176.128, 105.789, 48.7381, 83.8221, 19.8431, 178.79, 193.274, 119.547, 77.9015, 27.2652, 35.1085, 192.34, 21.0883, 105.576, 17.9327, 87.521, 47.0412, 159.514, 158.757, 100.266, 91.9279, 131.84, 197.101, 178.552, 117.185, 142.741, 19.245, 141.111, 71.2485, 145.274, 157.457, 170.452, 126.261, 55.8611, 164.177, 180.773, 189.776, 159.74, 61.2568, 60.9699, 91.4945, 191.449, 64.1865, 192.59, 44.9965, 90.2676, 181.304, 42.787, 184.423, 30.7321, 87.9849, 32.3191, 176.226, 128.062, 75.9301, 78.4875, 105.863, 18.6407, 77.3095, 13.1352, 39.552, 115.5, 164.037, 75.5882, 60.9394, 138.762, 121.946, 80.6421, 182.879, 121.134, 100.052, 61.5192, 134.074, 88.3511, 134.733, 187.86, 20.8808, 14.6733, 148.967, 158.647, 63.9668, 53.3403, 26.545, 70.2231, 10.0589, 42.024, 161.309, 159.239, 18.6468, 83.3338, 81.3257, 20.6, 23.7007, 0.244148, 148.155, 100.497, 170.415, 66.6036, 131.04, 64.2903, 126.884, 12.5675, 56.1357, 39.552, 70.8701, 105.1, 130.717, 133.995, 151.482, 103.409, 142.418, 28.4433, 25.9163, 89.4375, 133.427, 44.8317, 71.1753, 173.345, 149.718, 89.1446, 70.7663, 73.9158, 65.2791, 69.3747, 176.44, 174.12, 81.3074, 188.537, 144.902, 51.4908, 100.443, 112.583, 49.5315, 83.6634, 175.994, 13.422, 67.1163, 128.8, 36.1278, 86.8618, 139.537, 81.0755, 83.1385, 145.555, 110.508, 101.144, 67.5985, 87.0205, 137.303, 121.616, 37.6415, 36.5734, 177.459, 27.5033, 163.152, 165.307, 150.493, 111.728, 34.9681, 183.056, 174.645, 5.53606, 39.1369, 106.375, 78.5607, 84.0236, 6.1037, 101.505, 188.055, 146.556, 134.959, 2.3133, 187.774, 172.283, 192.615, 103.33, 57.9608, 150.56, 188.061, 27.6315, 86.166, 186.462, 121.574, 69.8813, 153.948, 81.2098, 8.92361, 14.2521, 98.5992, 187.689, 180.633, 27.4239, 149.876, 140.849, 56.1052, 26.5267, 41.7005, 63.2527, 192.969, 122.141, 198.914, 45.4848, 11.182, 136.229, 198.932, 101.639, 192.328, 195.135, 108.524, 199.799, 153.789, 197.54, 95.5168, 167.486, 197.986, 157.439, 115.476, 72.1397, 136.595, 22.0832, 73.4886, 76.5954, 189.77, 165.001, 60.8234, 169.543, 160.338, 153.899, 66.689, 170.971, 83.0531, 193.695, 164.928, 12.6896, 148.79, 44.4411, 175.744, 93.759, 103.763, 14.3132, 2.42317, 110.965, 123.765, 27.5887, 43.0677, 164.293, 163.366, 167.589, 77.279, 137.4, 180.139, 94.6501, 27.9, 153.075, 33.8694, 139.14, 197.736, 103.714, 60.8112, 69.3442, 181.256, 56.7766, 122.855, 134.532, 51.326, 135.282, 131.761, 182.824, 20.9418, 73.2505, 48.4085, 54.2436, 179.907, 46.1684, 156.987, 48.6465, 106.577, 176.507, 14.1972, 68.6972, 154.595, 114.103, 73.5374, 37.8124, 14.5695, 91.7264, 184.436, 193.2, 150.835, 55.4827, 166.521, 53.7309, 113.132, 70.2902, 58.8458, 63.0024, 123.356, 0.372326, 44.0809, 107.236, 137.266, 64.7176, 47.7432, 108.28, 181.188, 67.5314, 12.421, 91.5799, 163.768, 103.72, 46.9741, 129.124, 162.651, 139.36, 50.7767, 157.439, 186.236, 67.1651, 12.9154, 70.5405, 10.358, 36.9518, 186.438, 87.9177, 90.0601, 61.1103, 137.523, 197.479, 146.245, 95.0713, 69.7409, 73.5313, 27.4422, 192.779, 23.2612, 69.7775, 46.5102, 137.15, 149.065, 87.2341, 192.37, 145.555, 58.1683, 117.234, 16.6753, 50.2762, 45.8449, 59.3768, 195.618, 154.003, 190.832, 14.5268, 166.552, 160.247, 129.557, 52.7543, 39.7717, 178.863, 105.362, 56.3066, 185.229, 6.48213, 92.7152, 37.5744, 94.2167, 82.0338, 196.435, 132.096, 116.117, 117.13, 125.901, 66.4754, 70.1193, 1.60527, 144.273, 101.376, 131.919, 183.966, 92.7213, 49.4949, 195.459, 126.835, 89.639, 168.523, 109.629, 37.4523, 104.855, 129.112, 190.002, 73.1285, 8.84426, 57.8509, 5.52995, 21.7231, 96.9939, 55.2263, 40.3088, 57.1001, 88.7539, 188.604, 30.2316, 169.597, 1.90435, 160.161, 174.755, 71.4988, 101.767, 49.9161, 23.603, 177.538, 125.401, 133.647, 38.4899, 60.2802, 180.84, 175.878, 29.8349, 180.7, 121.317, 197.845, 68.4774, 93.5453, 194.47, 120.475, 72.4082, 127.903, 166.564, 82.345, 68.4652, 142.582, 121.708, 32.6426, 129.093, 156.645, 136.076, 199.152, 184.014, 54.8418, 27.1371, 31.2082, 106.973, 168.09, 177.856, 17.6885, 77.456, 128.953, 35.9081, 179.949, 17.4078, 2.07526, 87.936, 39.8083, 102.945, 89.7122, 108.707, 62.5324, 151.079, 12.1158, 85.403, 174.786, 81.4173, 63.0879, 16.7669, 157.585, 52.9313, 40.3882, 187.39, 114.304, 75.6188, 151.231, 116.868, 13.6174, 97.2503, 192.749, 184.075, 107.486, 165.081, 7.44652, 126.341, 24.0974, 62.5752, 13.7089, 13.6845, 135.868, 21.131, 11.2308, 98.0499, 2.38655, 106.772, 61.4887, 137.498, 88.406, 81.8995, 43.2325, 195.434, 156.792, 141.99, 82.9432, 114.829, 136.485, 127.317, 60.2863, 67.4398, 65.511, 9.67437, 188.617, 178.045, 108.902, 54.0971, 70.3879, 23.603, 51.6923, 55.5742, 27.7108, 7.55638, 66.8844, 50.7706, 107.724, 8.97855, 158.403, 104.74, 163.128, 106.833, 79.0735, 172.649, 7.62352, 179.174, 123.612, 36.494, 46.4064, 177.38, 196.551, 116.837, 181.616, 75.3441, 155.229, 159.49, 192.798, 93.7407, 27.7841, 197.253, 192.383, 199.2, 106.082, 186.645, 156.175, 78.5302, 113.706, 119.602, 128.788, 187.982, 48.9212, 10.1993, 104.129, 77.3949, 102.646, 156.389, 180.871, 189.489, 51.4115, 76.6808, 192.901, 134.025, 125.352, 121.909, 14.8198, 37.0617, 151.146, 161.913, 142.405, 165.166, 170.006, 147.27, 148.033, 24.5979, 130.638, 146.013, 11.2186, 135.6, 57.2039, 55.3545, 124.046, 81.5332, 92.3185, 188.403, 171.624, 33.1614, 14.9724, 135.862, 87.5759, 147.85, 33.5459, 102.786, 84.5241, 43.7513, 53.7553, 120.273, 159.526, 149.077, 21.6071, 132.188, 74.5445, 126.286, 141.417, 156.883, 11.951, 171.294, 168.706, 123.917, 104.953, 187.738, 178.24, 185.766, 43.0921, 36.1278, 6.94601, 104.563, 152.702, 58.1317, 171.929, 113.804, 152.361, 75.7469, 63.3869, 55.0432, 183.398, 65.5416, 12.6225, 189.666, 115.329, 107.871, 34.9925, 85.5556, 126.341, 11.0843, 47.8713, 178.832, 158.751, 10.7669, 163.652, 10.5838, 49.4827, 143.931, 57.6556, 138.572, 135.551, 98.2452, 11.1637, 123.936, 134.477, 136.277, 29.3588, 192.944, 99.826, 94.6684, 34.2784, 173.693, 51.619, 58.388, 132.81, 107.816, 81.8384, 109.934, 159.642, 137.236, 134.886, 153.569, 95.2422, 65.8467, 63.0329, 14.5024, 158.831, 196.265, 156.926, 13.1596, 176.33, 41.4991, 151.024, 14.3681, 23.3345, 111.74, 104.068, 151.585, 143.138, 176.019, 154.582, 26.0872, 139.592, 198.956, 151.305, 181.268, 45.1796, 152.464, 158.672, 51.7472, 63.9241, 102.475, 37.2936, 187.701, 189.386, 170.672, 99.4171, 153.99, 97.6104, 171.178, 39.3811, 104.428, 115.104, 122.294, 139.116, 30.9885, 61.8183, 33.2163, 148.576, 88.0337, 169.109, 165.77, 41.2366, 191.504, 72.6768, 171.075, 44.557, 93.1547, 80.5872, 101.334, 145.433, 129.795, 116.196, 96.4995, 70.6748, 47.4685, 136.155, 146.025, 142.503, 100.699, 23.3406, 125.559, 8.96023, 26.6976, 154.857, 108.676, 49.44, 94.5891, 44.9721, 153.606, 55.1103, 29.6213, 63.1367, 164.916, 184.411, 12.3844, 26.8929, 30.2805, 193.5, 153.056, 69.8874, 32.1665, 141.728, 40.5591, 184.826, 134.55, 172.149, 166.869, 41.4136, 144.652, 166.912, 26.1238, 79.3847, 149.651, 77.7795, 88.9676, 76.4306, 28.3883, 131.614, 51.6495, 191.162, 38.1237, 110.788, 160.326, 81.0877, 76.2047, 108.701, 10.2298, 95.6084, 16.8157, 160.222, 19.9347, 143.956, 122.697, 147.179, 180.535, 38.1481, 131.858, 178.808, 79.8669, 111.02, 19.2816, 47.2365, 1.75787, 109.11, 33.6802, 157.945, 100.217, 160.698, 9.17386, 135.24, 48.6526, 18.7262, 152.025, 102.713, 164.129, 178.814, 102.365, 38.7158, 6.51265, 72.1763, 70.9067, 6.84225, 52.7909, 133.634, 50.6607, 61.3788, 46.1928, 144.554, 103.659, 27.7657, 20.4596, 50.8316, 122.66, 158.098, 199.072, 55.8611, 67.8915, 61.5619, 169.427, 188.507, 78.75, 109.922, 132.09, 146.049, 60.5304, 117.991, 25.0069, 12.1769, 146.135, 99.0753, 122.733, 49.9588, 32.7891, 95.4802, 110.605, 169.793, 193.793, 83.9503, 34.022, 72.3777, 165.184, 156.145, 122.636, 66.7074, 107.505, 174.108, 89.5535, 163.012, 42.5062, 100.742, 105.582, 40.7239, 164.867, 5.2797, 48.439, 91.4762, 28.8827, 143.736, 26.9662, 137.04, 10.242, 114.383, 180.352, 22.309, 42.7015, 88.2351, 152.355, 83.285, 29.8166, 114.493, 40.5103, 89.993, 131.669, 101.035, 20.3681, 64.5161, 126.56, 69.863, 44.6852, 173.913, 47.7737, 87.5942, 17.6702, 98.2086, 95.2422, 70.9189, 44.1847, 73.3665, 22.309, 67.0553, 153.978, 9.25321, 197.43, 9.24711, 100.87, 82.6014, 140.88, 147.215, 37.0312, 4.96841, 136.485, 152.629, 62.1418, 165.758, 118.351, 99.5575, 47.6455, 50.1053, 32.5571, 119.987, 135.96, 131.925, 73.9158, 134.233, 114.682, 41.8165, 197.626, 56.4409, 65.8162, 10.0833, 106.052, 75.0572, 90.0174, 184.686, 110.428, 8.27662, 105.771, 24.1707, 67.5436, 134.538, 123.618, 165.722, 191.516, 97.9522, 195.404, 168.029, 140.458, 88.8211, 38.4594, 129.344, 178.24, 99.0814, 134.855, 180.407, 11.1637, 52.7604, 105.588, 52.4064, 99.7406, 11.4689, 159.819, 119.993, 45.3383, 54.8601, 19.0863, 119.193, 51.9669, 36.0851, 116.532, 140.403, 62.9414, 12.299, 71.0959, 96.3775, 172.649, 150.078, 36.7626, 65.157, 113.236, 105.936, 126.786, 108.084, 16.3213, 81.753, 112.857, 162.719, 49.7696, 155.29, 199.384, 10.8463, 129.331, 85.6044, 72.6707, 121.445, 105.411, 193.567, 25.1595, 168.633, 24.781, 117.283, 154.643, 24.6406, 67.3055, 42.616, 38.4472, 27.8085, 157.622, 8.61843, 82.7235, 125.517, 51.9059, 88.0032, 146.605, 92.1842, 99.5636, 42.6222, 41.4441, 22.2175, 79.9524, 53.9262, 105.045, 97.9888, 114.121, 128.459, 50.8621, 32.8013, 51.4908, 178.277, 92.2269, 188.867, 131.62, 176.476, 141.581, 170.251, 120.866, 187.609, 84.6644, 90.8719, 124.168, 73.1834, 129.765, 117.46, 71.7063, 72.1397, 31.9102, 87.5088, 80.6726, 56.2883, 192.114, 2.26447, 91.9523, 60.7868, 31.5622, 194.696, 181.854, 89.639, 40.8704, 172.423, 148.833, 196.118, 104.27, 135.746, 175.201, 38.2641, 98.8433, 170.586, 163.866, 109.366, 168.731, 116.428, 150.017, 43.379, 74.3675, 109.348, 187.884, 195.459, 16.6814, 199.921, 112.406, 97.3601, 136.338, 46.2294, 176.006, 187.982, 87.9177, 81.814, 160.259, 18.7506, 89.1873, 158.666, 162.645, 134.184, 45.2712, 65.1387, 58.2598, 27.2713, 147.905, 119.004, 36.5246, 170.013, 149.626, 35.3343, 12.9093, 118.821, 113.016, 69.5334, 115.653, 95.0591, 34.9071, 35.2672, 47.4807, 40.4553, 145.604, 197.461, 122.336, 147.606, 83.0958, 40.2234, 177.074, 65.4134, 142.296, 109.891, 158.184, 179.595, 199.219, 132.914, 183.544, 168.108, 110.251, 8.87478, 79.0918, 31.6477, 186.34, 78.457, 190.582, 199.115, 142.198, 66.3839, 70.2597, 108.054, 120.573, 99.4232, 38.6486, 60.1764, 191.406, 199.316, 21.1921, 151.781, 126.084, 50.7706, 60.0238, 79.2322, 46.6689, 44.4533, 100.674, 19.4525, 150.09, 194.91, 53.7675, 185.376, 107.37, 133.555, 131.791, 56.386, 137.846, 176.653, 60.7501, 83.8588, 158.397, 68.2577, 166.942, 71.5537, 7.64794, 198.236, 138.633, 31.135, 145.213, 95.1506, 62.801, 112.613, 25.8614, 194.324, 136.857, 9.81475, 148.808, 80.7215, 55.3484, 40.8765, 181.878, 6.99484, 71.688, 103.677, 160.741, 86.3979, 134.867, 106.339, 163.14, 160.772, 101.175, 72.6768, 58.095, 143.516, 20.9235, 122.672, 130.137, 37.5622, 100.162, 127.946, 178.521, 28.3334, 137.394, 42.2132, 91.4151, 51.7289, 35.2977, 106.772, 95.1872, 37.0495, 187.451, 130.967, 78.5607, 25.0313, 140.025, 47.6333, 191.852, 160.772, 128.544, 143.614, 121.201, 161.681, 89.5718, 165.178, 37.7758, 60.506, 147.447, 98.5137, 22.0099, 23.2673, 175.988, 25.2022, 162.804, 42.2254, 35.2123, 52.6872, 131.73, 103.854, 83.0836, 119.712, 32.2947, 31.8003, 162.2, 76.8334, 124.125, 4.87686, 61.5131, 41.8531, 22.1809, 150.023, 53.0595, 30.3171, 171.014, 10.2359, 103.684, 60.9638, 193.933, 87.6003, 161.742, 136.186, 117.264, 61.2201, 118.827, 97.5372, 69.5517, 80.1843, 46.4858, 156.151, 55.7878, 131.559, 107.956, 98.2452, 6.61641, 83.3277, 144.847, 165.593, 102.554, 2.39265, 138.89, 10.9439, 173.229, 147.52, 148.65, 128.568, 171.569, 37.2997, 121.244, 159.648, 123.038, 193.158, 179.046, 72.805, 149.876, 157.268, 181.426, 19.5379, 170.263, 185.571, 161.455, 64.7053, 72.5791, 15.6987, 37.6354, 191.986, 164.635, 110.001, 0.61037, 184.344, 97.8362, 173.443, 174.889, 179.516, 66.4571, 146.623, 155.553, 100.461, 81.8384, 73.7327, 19.9774, 160.839, 121.586, 45.5275, 14.5817, 45.4726, 153.825, 17.2674, 116.507, 165.825, 36.3903, 101.73, 70.3452, 190.466, 91.0794, 137.144, 99.8688, 50.5325, 152.831, 130.168, 94.7417, 20.2033, 93.9543, 173.87, 72.2434, 72.1702, 23.3711, 40.6873, 113.993, 108.115, 160.814, 50.3983, 177.636, 64.1438, 186.053, 13.0741, 183.612, 91.2992, 63.1001, 47.0901, 176.824, 11.005, 139.451, 72.0359, 163.396, 45.5397, 120.286, 176.739, 22.3762, 23.4993, 117.692, 62.1845, 92.465, 60.2802, 174.889, 159.545, 33.6924, 113.529, 65.4378, 23.072, 164.214, 13.6357, 167.486, 198.144, 71.9382, 169.176, 12.9215, 164.165, 101.34, 147.404, 111.686, 152.586, 115.091, 129.691, 50.9049, 39.845, 130.839, 42.0362, 69.161, 115.348, 32.9112, 160.503, 105.698, 124.43, 159.014, 191.681, 124.625, 186.328, 161.974, 28.193, 114.158, 187.146, 146.44, 179.138, 160.826, 47.8347, 80.6299, 75.5455, 198.694, 67.1468, 83.0103, 50.8805, 33.5704, 139.616, 184.057, 196.49, 42.2498, 48.5122, 162.914, 92.0682, 146.715, 33.8328, 59.1937, 11.1332, 63.3381, 35.2428, 159.624, 1.51372, 27.1493, 10.0711, 59.8041, 40.7727, 191.009, 126.255, 0.915555, 66.7196, 99.826, 30.3293, 81.4356, 82.8578, 39.8328, 190.393, 143.87, 138.853, 62.7338, 114.042, 136.442, 186.01, 84.9696, 41.4075, 149.852, 7.73339, 61.916, 113.059, 128.959, 25.0252, 37.0983, 12.9948, 59.5294, 176.897, 66.451, 86.642, 29.9326, 75.8812, 17.4627, 133.769, 167.876, 38.5022, 43.4706, 76.4916, 52.1012, 190.728, 132.835, 102.243, 199.139, 48.4146, 133.164, 81.7225, 24.9947, 172.857, 50.5448, 61.8793, 81.8873, 101.163, 23.7495, 33.8694, 169.951, 151.775, 173.467, 176.049, 123.972, 104.929, 126.505, 171.899, 75.0938, 158.129, 15.0212, 3.03354, 187.548, 156.597, 150.749, 17.1758, 70.3513, 98.7518, 174.181, 71.0715, 64.0522, 116.697, 78.8415, 148.521, 2.44148, 131.651, 110.459, 53.2914, 49.2508, 184.448, 141.209, 177.142, 9.888, 40.9864, 118.918, 142.747, 118.174, 72.7256, 191.302, 121.696, 98.0132, 12.1158, 105.161, 27.43, 100.162, 183.099, 80.6299, 183.245, 113.919, 6.54927, 160.356, 65.2364, 53.853, 180.792, 33.7474, 120.286, 121.525, 136.991, 136.198, 58.9984, 42.1827, 169.701, 23.2612, 23.719, 176.525, 42.2803, 19.9347, 31.4646, 107.376, 67.3971, 144.823, 91.8912, 101.431, 62.4592, 109.372, 0.299081, 45.967, 180.132, 192.114, 5.55437, 60.5487, 13.7394, 187.848, 153.099, 183.776, 177.813, 31.8796, 102.884, 134.965, 163.488, 99.2462, 22.3701, 150.92, 69.0268, 178.918, 154.479, 97.5799, 5.25529, 123.014, 60.5548, 56.8133, 176.049, 171.05, 138.359, 188.086, 33.7657, 149.132, 15.0029, 65.1875, 109.793, 144.176, 14.9663, 87.9666, 119.913, 53.1266, 103.006, 169.042, 144.871, 7.20237, 102.176, 95.5107, 85.757, 71.0837, 186.761, 165.685, 194.555, 17.1331, 50.2579, 100.797, 79.5312, 174.633, 148.375, 31.7209, 185.791, 153.978, 16.9378, 160.704, 150.652, 45.9243, 64.9129, 98.0621, 34.1929, 95.7976, 42.3353, 73.9097, 24.6895, 52.6933, 183.331, 24.8909, 19.068, 193.725, 24.012, 155.889, 78.4021, 23.2063, 95.5168, 176.72, 158.373, 46.9131, 91.934, 13.8981, 114.713, 21.2653, 113.291, 197.333, 183.734, 77.5781, 183.508, 122.025, 137.718, 121.244, 43.3729, 184.82, 115.458, 105.972, 18.5308, 196.118, 127.677, 183.032, 162.426, 66.567, 37.5256, 125.413, 74.2637, 168.834, 16.0344, 27.0882, 192.022, 174.261, 72.5974, 77.2301, 160.363, 164.635, 60.4083, 9.41191, 73.6534, 18.8726, 156.981, 25.8431, 193.909, 74.7765, 19.5685, 196.564, 143.754, 65.7002, 15.6743, 133.689, 174.407, 132.31, 128.483, 140.617, 108.652, 109.293, 137.352, 36.2499, 158.788, 87.0754, 73.2688, 54.2192, 19.6112, 48.5366, 149.339, 17.0782, 36.3964, 180.731, 166.228, 185.559, 47.8286, 61.5925, 192.651, 182.757, 104.013, 121.702, 120.829, 193.371, 59.4012, 180.938, 96.9695, 37.9101, 40.8155, 60.2313, 26.7159, 121.934, 190.368, 68.3126, 155.651, 60.2313, 83.4376, 163.366, 124.711, 176.08, 21.662, 189.789, 100.711, 75.4357, 0.897244, 189.923, 181.787, 96.176, 154.18, 190.722, 77.8527, 32.3008, 69.6738, 65.5293, 66.097, 133.152, 183.697, 70.7541, 21.4423, 177.648, 48.6282, 57.6189, 121.995, 23.4687, 75.9301, 158.855, 29.5236, 31.3364, 154.076, 150.42, 144.963, 164.153, 141.533, 88.2473, 138.816, 161.29, 34.7301, 19.9347, 156.682, 187.762, 11.0477, 171.484, 99.6857, 158.464, 146.757, 8.99075, 179.992, 137.394, 9.97345, 175.408, 151.207, 11.8595, 128.703, 102.206, 13.9103, 41.7127, 133.708, 96.1455, 196.1, 83.5475, 176.568, 100.98, 8.63674, 89.9319, 22.5715, 134.147, 175.762, 83.6207, 18.7506, 198.523, 40.5225, 6.78121, 136.821, 69.1244, 45.2712, 45.2223, 91.11, 154.839, 87.1059, 27.5704, 131.614, 35.1085, 148.369, 99.5514, 111.887, 71.7185, 2.08747, 73.1468, 32.0505, 95.4375, 192.474, 50.8499, 56.7522, 167.779, 90.3531, 124.888, 8.14234, 17.7129, 23.6641, 151.415, 133.824, 153.404, 82.9371, 199.744, 149.834, 48.8723, 1.70904, 76.5648, 158.605, 5.78021, 112.064, 125.187, 41.963, 90.1151, 175.042, 99.6979, 129.051, 143.907, 86.1354, 114.249, 139.323, 71.9504, 55.7573, 67.2323, 0.512711, 3.55235, 64.7053, 140.519, 141.612, 129.203, 194.635, 173.656, 60.7929, 12.3112, 33.5154, 113.114, 168.45, 175.964, 156.12, 136.479, 105.1, 164.568, 79.4702, 51.6984, 134.385, 80.636, 93.3317, 171.282, 11.0477, 196.484, 165.654, 61.507, 123.881, 40.9253, 123.063, 166.68, 113.706, 161.04, 106.314, 108.298, 166.332, 158.831, 100.778, 59.328, 180.541, 2.99692, 141.881, 193.384, 11.0355, 15.6316, 195.605, 8.94803, 186.444, 133.934, 113.938, 128.617, 102.237, 46.9253, 65.3523, 173.296, 88.7417, 7.19626, 40.26, 111.411, 154.088, 117.6, 31.4951, 83.1629, 2.17292, 92.9655, 89.7855, 99.7162, 125.657, 96.8352, 54.1948, 17.42, 173.901, 53.1816, 157.628, 187.036, 195.953, 33.1126, 107.273, 177.953, 158.507, 116.605, 40.0647, 177.416, 138.884, 129.209, 153.209, 64.7114, 192.993, 53.7858, 9.5584, 160.442, 98.3001, 117.203, 118.174, 50.6851, 171.795, 152.068, 189.734, 86.2575, 162.963, 33.4544, 67.0125, 156.914, 21.7475, 190.606, 130.381, 140.556, 141.551, 193.646, 178.234, 27.4789, 77.1386, 15.3203, 182.476, 119.572, 137.681, 167.724, 119.01, 104.526, 54.6525, 113.974, 73.1651, 5.07218, 124.876, 154.967, 20.1971, 3.21665, 28.4127, 191.089, 80.7581, 139.409, 121.592, 158.922, 51.326, 86.7885, 4.944, 142.991, 9.7354, 0.427259, 166.198, 162.45, 195.599, 41.5113, 98.2086, 32.8196, 165.77, 176.58, 91.9828, 102.701, 24.8665, 51.7106, 104.593, 65.4988, 130.68, 141.807, 102.176, 46.1257, 74.6788, 174.749, 77.8954, 162.749, 137.474, 30.6406, 144.914, 163.475, 19.2511, 116.037, 104.343, 138.09, 45.3139, 13.1413, 88.2412, 104.776, 46.4736, 4.03455, 77.5719, 71.8284, 114.56, 69.9118, 95.1567, 174.383, 32.8867, 165.264, 73.6656, 86.0927, 181.726, 167.119, 198.566, 105.924, 118.735, 99.2645, 126.102, 119.791, 34.4859, 31.4524, 76.278, 37.3363, 82.9005, 53.6393, 0.799585, 103.623, 186.724, 125.175, 26.0567, 85.8486, 117.972, 199.042, 171.093, 135.093, 53.4501, 21.07, 140.562, 122.99, 5.18204, 4.15662, 192.73, 53.4989, 125.895, 177.496, 82.3939, 149.583, 89.4559, 10.7425, 78.8903, 14.948, 146.416, 113.327, 59.0533, 70.748, 54.3779, 8.5757, 75.1793, 67.3238, 83.5597, 183.013, 197.998, 70.9067, 97.2015, 10.4984, 34.6141, 125.333, 107.059, 151.531, 121.555, 188.769, 103.867, 27.3873, 192.645, 145.445, 162.804, 169.414, 122.025, 92.349, 23.9692, 43.1776, 142.125, 100.955, 147.856, 147.74, 136.601, 53.9506, 45.79, 189.843, 8.34376, 156.944, 85.4457, 133.14, 122.41, 125.187, 171.276, 122.404, 24.012, 45.2834, 99.9908, 82.7723, 31.489, 111.948, 191.046, 193.939, 104.27, 39.6619, 192.071, 39.3628, 185.821, 149.449, 59.3829, 30.549, 21.6498, 130.546, 13.422, 106.186, 52.2721, 77.1691, 69.2526, 106.412, 181.152, 0.195318, 41.9874, 22.0344, 17.6702, 13.9103, 27.2958, 118.735, 195.148, 197.699, 156.859, 136.29, 163.744, 132.768, 28.8156, 162.78, 58.4613, 113.895, 13.2878, 183.837, 42.1339, 36.1156, 32.7952, 106.839, 123.35, 156.456, 17.7313, 60.1398, 49.4339, 130.644, 36.8725, 167.278, 74.453, 124.833, 102.799, 196.197, 2.13019, 2.32551, 52.382, 83.4803, 103.867, 185.29, 102.261, 133.555, 111.655, 191.455, 79.3115, 92.9044, 86.0927, 45.8632, 126.084, 99.3072, 159.337, 167.589, 81.3257, 106.034, 153.056, 131.419, 135.392, 181.097, 71.4377, 104.605, 160.765, 81.4112, 69.1122, 158.58, 117.478, 102.097, 94.1557, 55.6658, 133.634, 0.622578, 6.03046, 110.801, 60.8234, 196.063, 168.59, 128.245, 104.624, 43.2997, 118.009, 13.3122, 130.058, 182.263, 105.771, 59.096, 42.0728, 162.12, 116.648, 108.164, 41.0047, 140.526, 161.65, 189.288, 153.026, 57.8082, 45.9975, 29.1696, 35.6456, 86.227, 12.0975, 39.1614, 58.1622, 154.149, 57.5091, 176.971, 57.1368, 145.293, 127.915, 83.3766, 166.863, 172.948, 185.07, 93.9787, 124.821, 129.185, 149.809, 125.883, 52.1622, 109.079, 119.98, 39.4299, 109.183, 109.922, 63.4236, 74.9229, 25.7698, 5.45061, 185.961, 164.342, 106.906, 195.074, 174.322, 118.046, 113.083, 128.037, 114.158, 86.52, 15.8269, 102.731, 82.9798, 97.0855, 28.1442, 140.837, 18.1402, 65.5904, 109.977, 136.564, 59.8041, 83.1751, 148.692, 80.8985, 168.908, 177.96, 14.5329, 160.772, 20.1178, 151.756, 91.1161, 133.329, 169.86, 91.8241, 80.8618, 175.109, 99.1791, 157.555, 85.4946, 6.27461, 131.834, 42.201, 161.498, 89.8587, 27.1493, 109.598, 150.578, 105.942, 48.0789, 5.88397, 185.144, 100.882, 104.123, 146.214, 68.7704, 111.557, 169.292, 76.4733, 131.675, 64.5528, 175.109, 110.013, 161.168, 150.414, 178.552, 39.3567, 7.13523, 72.8233, 5.03555, 122.922, 62.5202, 172.857, 166.393, 146.739, 18.7567, 28.1625, 53.7797, 161.156, 96.2676, 127.25, 5.74358, 92.5932, 149.925, 18.6163, 51.1124, 30.2194, 49.5071, 135.044, 126.994, 187.713, 135.203, 92.758, 171.734, 139.933, 148.027, 65.7002, 92.9044, 134.678, 35.3282, 34.7056, 10.2542, 79.3847, 31.2571, 111.747, 173.699, 2.86264, 12.2074, 139.134, 156.243, 191.992, 127.329, 163.756, 122.001, 3.44859, 103.934, 186.267, 169.146, 150.444, 28.0221, 185.339, 147.868, 64.4063, 199.738, 16.8584, 153.362, 131.742, 63.8081, 188.165, 93.7712, 5.50554, 15.0517, 107.236, 133.195, 192.175, 78.8232, 6.93991, 9.60723, 113.315, 66.4754, 144.249, 27.8756, 42.3353, 5.85955, 74.4285, 127.702, 116.19, 46.7727, 0.299081, 26.5694, 22.1076, 41.4136, 196.435, 37.7758, 159.301, 199.884, 108.353, 163.347, 75.6798, 20.6244, 126.798, 62.0136, 179.595, 95.9197, 36.8969, 92.2025, 126.53, 192.322, 142.43, 29.9814, 80.8252, 6.15253, 64.9922, 62.0563, 52.4247, 193.988, 18.2623, 53.7004, 108.554, 78.0602, 157.219, 91.757, 60.4511, 26.7953, 2.3011, 102.585, 25.1167, 110.251, 138.719, 145.061, 61.9831, 30.5857, 190.588, 178.491, 84.6644, 127.531, 70.1804, 105.649, 47.676, 58.858, 102.182, 90.6888, 8.13013, 155.87, 22.8462, 152.141, 62.4958, 53.8041, 54.2436, 30.8664, 158.269, 197.406, 53.4867, 133.055, 180.718, 21.2775, 136.943, 148.149, 114.597, 5.45061, 179.443, 141.85, 80.8802, 5.79241, 188.507, 124.064, 48.4512, 59.4989, 153.569, 91.8912, 20.8502, 65.5538, 170.202, 147.575, 97.9705, 140.208, 39.3017, 91.4762, 12.7812, 47.4319, 141.356, 90.0601, 143.071, 104.404, 185.607, 18.8238, 198.126, 161.968, 34.3272, 80.3186, 180.474, 60.0909, 1.99591, 97.0733, 22.6081, 158.367, 29.5419, 79.5862, 98.703, 141.441, 133.543, 69.2831, 11.298, 99.5148, 130.454, 56.502, 53.0717, 83.7977, 35.8348, 173.437, 143.522, 136.979, 138.572, 162.133, 40.6934, 198.688, 150.053, 117.435, 27.9122, 85.9706, 28.4433, 104.971, 75.4784, 148.784, 77.8832, 54.1032, 77.4071, 0.402844, 122.684, 125.925, 163.14, 122.959, 156.584, 156.194, 84.2433, 147.85, 66.9332, 173.333, 76.1681, 11.2369, 33.9427, 191.375, 194.94, 184.545, 4.88296, 77.9443, 128.874, 179.846, 43.3119, 97.2381, 85.3175, 130.406, 153.282, 127.952, 152.355, 46.1318, 178.582, 156.291, 135.508, 144.023, 176.922, 43.9344, 69.4967, 53.6576, 69.1794, 170.977, 4.91958, 97.6592, 163.622, 153.929, 69.9973, 47.1511, 155.101, 38.6486, 7.26951, 152.245, 199.286, 168.365, 185.345, 47.4929, 61.8671, 126.591, 85.5739, 132.377, 142.821, 155.119, 63.8203, 26.7464, 175.567, 46.9924, 187.14, 101.334, 1.22074, 114.475, 145.512, 83.1629, 17.8167, 31.9895, 38.5632, 136.064, 104.508, 154.143, 37.2936, 16.126, 20.7892, 108.621, 120.762, 102.188, 80.7398, 64.2415, 87.8384, 101.242, 102.206, 73.8548, 43.9161, 28.9865, 31.0434, 92.3429, 162.627, 65.2425, 40.3333, 69.5761, 122.544, 84.4386, 104.587, 118.406, 18.9215, 61.0614, 48.9761, 157.604, 150.42, 150.169, 73.5679, 29.8288, 80.7886, 42.7931, 152.507, 94.6562, 175.64, 196.484, 108.249, 96.4446, 14.0934, 140.703, 160.814, 107.395, 75.5821, 57.3565, 151.946, 145.476, 101.498, 36.2255, 44.3617, 67.3971, 131.95, 5.45061, 95.9868, 136.589, 17.4566, 136.985, 109.146, 66.158, 5.70086, 127.079, 154.796, 136.869, 3.21665, 151.75, 171.722, 190.1, 112.131, 97.2381, 40.791, 95.822, 41.8409, 65.4866, 134.172, 95.5596, 195.807, 154.595, 139.494, 20.3742, 33.1736, 114.31, 159.142, 183.813, 105.588, 76.4794, 84.4203, 149.883, 168.853, 160.533, 44.8073, 176.031, 169.665, 153.917, 132.163, 131.101, 107.321, 67.6107, 199.927, 88.3206, 170.482, 166.57, 137.358, 193.243, 148.827, 75.1915, 18.7567, 6.72018, 119.81, 157.805, 127.683, 122.91, 10.3824, 174.102, 56.1419, 62.1784, 33.7046, 9.63775, 78.8598, 39.1369, 0.842311, 81.3074, 2.44758, 5.56658, 107.529, 17.774, 89.6695, 168.395, 195.831, 14.9785, 58.6016, 182.684, 90.9207, 66.5792, 156.926, 19.5563, 12.7628, 69.4418, 111.069, 109.232, 62.8376, 4.08948, 150.499, 158.928, 185.87, 190.204, 106.864, 45.8632, 6.52486, 17.0293, 188.299, 185.229, 141.166, 148.93, 28.5531, 173.138, 184.533, 5.7741, 11.2857, 104.605, 171.477, 118.467, 69.2404, 166.533, 113.688, 159.38, 158.477, 105.631, 11.3956, 47.8347, 7.5808, 171.453, 140.129, 130.576, 141.917, 134.294, 12.3539, 37.2265, 182.788, 178.222, 14.2888, 10.7669, 21.7048, 69.0023, 117.551, 112.998, 69.6005, 197.095, 65.9078, 105.02, 181.011, 95.5962, 36.9884, 2.68563, 53.3158, 33.2347, 179.009, 107.083, 100.998, 82.3267, 22.4677, 109.989, 4.7853, 61.7328, 166.906, 74.3614, 135.929, 154.869, 42.5855, 89.7916, 55.3911, 2.61238, 147.233, 2.95419, 88.0886, 71.3279, 157.738, 23.133, 172.082, 95.7976, 116.898, 2.61849, 182.812, 197.223, 95.114, 192.248, 132.322, 76.1498, 127.122, 71.7917, 43.4767, 80.4956, 23.7007, 55.5071, 99.1607, 148.454, 58.4124, 72.5608, 66.5181, 26.5084, 129.289, 22.4921, 196.173, 162.401, 195.581, 105.716, 196.417, 73.0552, 189.459, 28.9377, 82.4732, 187.616, 2.72835, 85.6899, 100.681, 165.343, 148.582, 111.753, 127.738, 88.6441, 98.2635, 115.616, 144.896, 130.998, 92.8373, 184.008, 187.194, 99.9542, 29.8776, 62.2761, 167.675, 55.0737, 25.1961, 9.95514, 176.598, 0.933866, 49.5743, 131.144, 55.6719, 55.0493, 40.437, 180.029, 47.261, 90.8963, 88.638, 41.3343, 118.613, 55.8184, 100.119, 85.8119, 161.052, 164.068, 132.664, 0.671407, 64.2415, 164.702, 7.60521, 95.0835, 65.3645, 84.6034, 196.545, 80.6299, 173.37, 46.4309, 183.056, 126.78, 68.16, 161.583, 99.4476, 191.357, 74.8497, 98.1719, 38.4228, 26.0018, 12.9337, 124.003, 39.8572, 56.6668, 120.573, 24.5125, 152.477, 139.048, 53.7431, 95.7793, 170.568, 189.068, 160.497, 12.4516, 45.7411, 88.3572, 30.1645, 170.757, 173.632, 64.7176, 117.6, 106.43, 155.589, 147.569, 42.3475, 187.701, 35.4259, 98.4527, 162.926, 183.282, 11.1454, 16.4739, 25.9774, 105.295, 21.6254, 18.7872, 90.5606, 127.28, 8.36818, 196.057, 127.299, 177.148, 52.1928, 42.9029, 92.6359, 5.12101, 184.985, 163.439, 78.0725, 126.78, 76.3817, 51.6495, 134.342, 40.9253, 195.532, 146.947, 170.647, 122.648, 47.7615, 164.562, 128.886, 191.229, 86.343, 101.059, 32.8745, 153.13, 99.1791, 114.817, 112.552, 119.98, 70.5405, 81.4478, 3.19834, 1.51372, 117.789, 24.2988, 19.66, 173.571, 103.061, 19.9225, 105.576, 21.0211, 120.042, 178.552, 57.5762, 155.797, 55.6169, 99.5087, 24.5247, 109.787, 26.6732, 1.77007, 81.7164, 102.878, 91.7386, 129.362, 80.1111, 85.873, 37.7392, 31.8552, 170.537, 58.4735, 41.731, 119.834, 2.92978, 62.2578, 7.57469, 95.3581, 0.543229, 48.6404, 177.715, 57.4419, 109.159, 21.8513, 19.8187, 82.5098, 21.3019, 56.8804, 188.592, 79.873, 75.4845, 192.48, 162.407, 122.202, 30.4025, 199.097, 155.077, 50.9903, 114.164, 21.8757, 102.561, 157.018, 148.497, 98.6236, 124.668, 135.832, 147.99, 152.733, 61.8061, 118.192, 194.281, 61.214, 86.6665, 175.591, 185.308, 197.626, 190.979, 100.65, 59.4928, 50.0504, 147.179, 62.6484, 95.0041, 114.072, 145.189, 137.968, 83.9259, 60.7318, 104.086, 115.555, 181.524, 179.51, 58.8519, 177.416, 30.4147, 96.2798, 22.0832, 54.0361, 159.844, 166.295, 120.701, 54.9089, 59.8407, 142.137, 7.66625, 173.98, 130.497, 116.703, 148.729, 70.0705, 106.137, 141.166, 184.112, 122.44, 154.564, 117.936, 165.252, 161.168, 51.2711, 168.651, 66.4998, 72.6707, 28.6691, 77.1203, 171.874, 80.1477, 100.278, 7.30613, 136.723, 99.0692, 59.9811, 96.2981, 124.497, 167.626, 156.224, 122.269, 18.4393, 152.214, 41.2183, 47.438, 108.316, 127.897, 195.221, 36.0729, 52.1317, 33.8633, 25.6539, 147.429, 112.833, 3.91858, 41.4258, 103.708, 70.0644, 171.941, 102.091, 133.073, 6.84225, 152.33, 142.07, 87.5698, 171.306, 195.532, 169.897, 36.1095, 113.425, 18.4393, 142.399, 132.469, 96.7193, 109.915, 113.962, 158.202, 184.979, 111.222, 60.7135, 140.648, 3.9613, 71.5232, 179.76, 45.9059, 178.173, 178.326, 63.3747, 146.77, 42.5001, 162.969, 148.271, 65.7308, 28.9987, 181.079, 39.7717, 92.9167, 128.581, 168.041, 111.057, 179.095, 27.7291, 69.4662, 177.361, 3.59508, 120.249, 146.684, 45.204, 117.338, 193.945, 157.164, 136.619, 160.741, 192.358, 10.0223, 188.36, 156.731, 51.4969, 96.4995, 26.0933, 78.6889, 11.5482, 95.3764, 13.6906, 93.4477, 3.57677, 53.4867, 177.38, 79.2199, 111.27, 141.197, 4.95621, 18.598, 107.639, 82.815, 120.09, 66.5181, 66.7501, 62.1235, 31.428, 93.2218, 50.441, 102.823, 156.914, 102.213, 70.0278, 123.991, 122.935, 123.258, 123.051, 70.5466, 138.578, 180.328, 104.941, 118.766, 44.8439, 134.581, 195.813, 156.682, 38.8562, 2.32551, 122.111, 139.616, 73.0857, 147.99, 93.3866, 120.597, 129.569, 152.092, 13.9653, 155.864, 169.127, 154.204, 169.72, 85.3236, 155.583, 70.9189, 189.734, 188.018, 154.979, 88.8516, 83.0531, 112.583, 109.806, 32.5449, 150.42, 82.7479, 88.2107, 7.49535, 170.94, 177.764, 8.20338, 118.107, 63.8447, 87.8994, 36.9213, 60.6281, 25.2022, 144.914, 64.7908, 191.174, 139.634, 27.4911, 54.7746, 68.6422, 67.0003, 141.765, 66.6036, 72.7317, 4.17493, 76.7174, 155.974, 96.8657, 121.751, 134.715, 148.766, 116.196, 75.7042, 136.937, 90.2676, 30.549, 194.189, 103.842, 169.408, 182.409, 139.201, 10.6693, 117.948, 149.87, 43.3363, 17.8777, 84.7316, 14.6123, 121.476, 176.635, 184.869, 104.898, 157.524, 9.25321, 123.978, 13.4953, 66.1214, 164.708, 97.4029, 90.6705, 129.374, 149.052, 164.434, 12.1464, 108.542, 130.931, 97.5066, 22.1015, 30.7932, 22.0222, 106.442, 51.1002, 50.911, 32.5266, 37.6476, 109.067, 188.012, 101.608, 48.3718, 153.288, 137.236, 2.88705, 155.876, 31.3364, 156.529, 124.912, 61.6718, 99.0753, 28.2907, 126.585, 50.7523, 113.614, 96.1516, 67.6962, 165.807, 1.25126, 176.659, 14.3437, 56.4104, 100.516, 145.994, 119.425, 103.293, 80.0378, 1.71514, 178.088, 17.5115, 131.596, 199.463, 58.2049, 114.713, 38.0444, 64.6443, 5.87176, 64.2903, 116.312, 79.2383, 25.8309, 113.675, 35.3343, 43.4156, 136.973, 187.506, 109.055, 112.326, 102.768, 61.8488, 64.3818, 107.791, 36.7931, 13.4587, 84.1395, 105.026, 83.1507, 143.95, 125.077, 94.351, 171.947, 179.028, 110.801, 117.917, 49.7757, 142.723, 199.75, 81.4417, 185.192, 106.601, 128.184, 40.8887, 149.138, 13.9775, 28.1259, 164.238, 70.4672, 179.144, 120.365, 168.133, 166.375, 61.568, 82.3817, 29.6823, 147.887, 115.146, 91.348, 173.119, 100.308, 112.394, 142.088, 131.541, 38.728, 143.669, 40.0464, 83.6268, 138.444, 91.3907, 69.2587, 65.3523, 40.4492, 179.29, 135.307, 56.7278, 31.9285, 196.564, 183.319, 143.48, 101.682, 197.156, 90.8902, 153.801, 70.6198, 73.9341, 36.1095, 170.055, 197.223, 7.94702, 140.489, 115.506, 58.0157, 192.608, 186.92, 16.0222, 103.037, 32.9173, 12.6896, 136.814, 86.3002, 19.7027, 177.435, 194.128, 171.27, 78.8293, 106.052, 41.9752, 111.325, 195.41, 106.76, 33.8878, 105.234, 113.022, 63.9973, 59.566, 127.616, 107.944, 6.43941, 56.6485, 134.416, 38.3557, 149.919, 197.43, 143.376, 68.5812, 15.4302, 150.542, 189.483, 68.2699, 22.1015, 17.7129, 155.461, 86.6482, 155.126, 145.476, 77.2485, 101.718, 108.768, 11.8717, 124.424, 44.5021, 104.312, 188.195, 55.7207, 98.2269, 183.05, 175.665, 149.474, 72.5669, 174.224, 117.197, 9.0762, 52.852, 138.011, 184.112, 43.4889, 124.296, 83.4925, 37.8124, 22.663, 171.203, 161.632, 166.564, 108.042, 151.201, 133.207, 97.1526, 158, 44.4349, 111.82, 81.6553, 108.341, 77.8283, 181.841, 59.1266, 27.7596, 24.2561, 91.3968, 92.3246, 48.2986, 176.794, 20.3131, 79.4336, 170.824, 197.919, 23.1086, 140.794, 145.488, 43.733, 146.959, 80.5872, 27.2103, 163.207, 24.6345, 123.6, 6.14032, 172.35, 142.528, 126.89, 113.132, 3.4669, 183.563, 66.5548, 0.183111, 129.374, 45.1979, 199.426, 195.965, 62.5996, 132.402, 99.7833, 36.2072, 129.612, 125.578, 171.099, 69.2526, 120.035, 23.5847, 32.7097, 136.46, 10.8768, 61.6779, 181.597, 127.72, 165.758, 104.483, 129.051, 11.8778, 9.0762, 113.95, 12.4088, 142.259, 175.298, 92.3612, 81.8018, 37.0678, 133.122, 100.253, 61.8244, 34.9254, 84.7316, 149.425, 188.287, 25.2998, 140.001, 170.879, 119.919, 53.975, 165.697, 57.796, 125.101, 3.52184, 109.726, 183.526, 143.571, 148.802, 0.341807, 194.525, 123.148, 187.365, 20.3681, 78.3349, 31.4585, 144.981, 56.4043, 106.571, 142.637, 156.096, 123.057, 52.852, 199.06, 119.279, 62.1479, 85.8242, 48.0483, 27.6925, 116.947, 137.065, 153.63, 174.609, 181.378, 144.096, 183.19, 33.7901, 124.247, 168.352, 69.2465, 15.0578, 188.458, 188.122, 146.141, 94.6928, 65.212, 166.54, 161.773, 120.92, 43.2814, 194.555, 164.293, 194.385, 33.1614, 108.872, 63.8569, 123.612, 66.3228, 184.96, 69.3197, 155.498, 69.4296, 105.002, 199.969, 180.749, 113.565, 0.848415, 72.2129, 14.3681, 138.432, 172.021, 161.357, 151.122, 151.567, 78.634, 138.249, 195.99, 102.591, 9.42412, 81.0694, 169.677, 32.4107, 24.659, 161.998, 89.0225, 97.4212, 174.157, 8.95413, 22.5349, 68.746, 190.197, 142.332, 20.8136, 106.449, 55.5437, 86.6665, 122.019, 8.7466, 46.4125, 157.414, 32.3618, 136.259, 50.38, 122.636, 81.576, 11.1637, 159.429, 139.72, 53.3525, 191.491, 137.852, 167.101, 90.8597, 20.2704, 165.27, 50.9964, 77.1447, 49.8184, 54.561, 87.0998, 90.6766, 140.898, 144.884, 57.3077, 135.759, 36.9701, 69.5639, 141.465, 148.021, 13.4098, 199.145, 104.019, 74.8863, 1.72124, 159.307, 155.821, 31.0434, 188.745, 23.8472, 90.7682, 128.904, 90.8902, 182.672, 97.5921, 184.082, 103.354, 84.7194, 46.5163, 95.8525, 124.339, 55.9648, 51.0086, 102.628, 54.2619, 192.389, 104.501, 112.436, 169.994, 127.909, 192.09, 108.365, 174.041, 77.4071, 77.2607, 29.0475, 118.802, 154.649, 163.64, 124.131, 5.7741, 165.27, 31.6477, 184.747, 59.7736, 74.752, 50.2823, 48.9456, 78.1396, 151.384, 63.7898, 182.25, 54.9699, 184.216, 153.429, 133.006, 80.3308, 176.061, 74.1295, 62.9597, 114.487, 111.441, 166.277, 152.666, 37.1044, 199.561, 142.387, 81.9544, 171.477, 192.273, 148.833, 185.455, 83.7489, 177.044, 102.585, 82.9798, 57.1306, 44.9477, 102.902, 185.083, 41.1145, 33.607, 118.064, 73.4458, 75.692, 145.17, 147.081, 156.42, 190.588, 39.2651, 56.4287, 199.445, 179.455, 1.01321, 91.7631, 130.522, 49.1043, 183.959, 64.6138, 120.725, 143.742, 128.025, 14.1301, 139.61, 144.151, 152.532, 143.815, 185.73, 141.948, 24.1218, 33.1858, 192.071, 188.763, 99.7711, 124.558, 6.54927, 120.493, 144.694, 84.7133, 47.4685, 56.5081, 25.367, 41.6456, 33.9793, 173.864, 77.5048, 96.2798, 191.552, 156.523, 148.967, 109.183, 132.456, 119.572, 117.777, 162.279, 11.2125, 8.45973, 85.0856, 95.5718, 155.095, 73.1956, 43.1349, 118.876, 169.543, 17.5604, 18.2134, 98.5137, 118.686, 23.3528, 88.638, 119.053, 180.786, 49.5804, 77.7245, 120.005, 178.698, 6.70186, 121.757, 7.56859, 185.363, 5.82904, 112.937, 41.3343, 4.25428, 82.9615, 17.3833, 95.999, 124.876, 198.175, 68.0807, 198.096, 153.185, 109.879, 32.844, 137.7, 117.093, 124.009, 157.311, 155.168, 46.9436, 118.259, 182.873, 102.835, 30.9519, 41.2915, 39.6924, 81.7408, 145.695, 86.9655, 14.6306, 153.362, 61.5619, 143.529, 194.238, 126.298, 123.252, 117.881, 182.708, 18.0242, 105.21, 134.764, 94.8637, 191.607, 42.1094, 10.9622, 24.1035, 167.394, 134.342, 4.57778, 185.406, 109.751, 128.446, 39.198, 133.25, 188.794, 172.594, 3.1251, 54.6281, 17.0904, 147.081, 123.624, 136.998, 161.388, 69.7592, 16.0894, 11.2186, 129.307, 176.537, 8.32545, 69.6066, 31.0984, 76.8151, 139.714, 58.7909, 116.349, 183.99, 35.4381, 147.783, 185.687, 20.893, 55.7573, 70.8701, 77.4926, 0.286874, 120.151, 145.634, 29.664, 80.3186, 6.56758, 165.899, 116.288, 94.9736, 125.669, 169.701, 159.38, 159.575, 170.013, 82.2535, 115.592, 151.225, 38.2763, 33.9366, 137.956, 64.3086, 34.5103, 153.24, 112.558, 40.9192, 142.302, 34.7301, 70.3146, 76.3634, 174.035, 82.0582, 73.6351, 199.603, 195.099, 125.877, 178.185, 128.733, 23.0964, 116.855, 69.5639, 1.47099, 2.08136, 93.6064, 175.079, 180.602, 51.7655, 25.6905, 50.3983, 134.428, 191.723, 131.565, 41.316, 192.297, 140.641, 71.5476, 20.4657, 175.018, 190.521, 15.8086, 199.017, 100.351, 155.889, 187.329, 13.4098, 163.475, 198.035, 79.8486, 87.0815, 192.45, 124.204, 150.865, 67.159, 178.698, 161.858, 189.581, 136.222, 82.4488, 7.83715, 159.893, 198.956, 113.254, 152.641, 118.339, 143.937, 85.3908, 79.5923, 7.0864, 180.309, 48.3901, 139.085, 121.885, 49.5254, 192.474, 97.8057, 55.0432, 112.052, 29.0231, 162.932, 113.401, 109.091, 46.0707, 81.3684, 11.2735, 199.31, 38.7402, 113.962, 38.4716, 70.5283, 114.743, 84.7194, 103.214, 126.493, 174.81, 105.02, 50.1297, 148.534, 68.1967, 172.845, 53.2121, 32.313, 23.1452, 46.9802, 83.8099, 199.579, 152.336, 196.271, 53.56, 84.7987, 33.3628, 23.78, 32.3862, 96.9573, 164.22, 166.076, 141.459, 56.2395, 93.7346, 67.391, 121.146, 105.753, 27.076, 28.8888, 174.023, 98.2513, 106.204, 132.841, 82.7906, 57.8387, 165.331, 41.8775, 3.2899, 116.727, 161.394, 73.1956, 132.45, 126.902, 79.5434, 147.765, 51.2467, 146.879, 113.596, 14.5695, 142.686, 3.39976, 121.488, 149.815, 84.7682, 128.19, 155.773, 131.132, 18.4576, 69.6127, 69.0695, 86.8313, 190.667, 79.458, 194.555, 20.0873, 72.1824, 174.609, 101.389, 8.18506, 150.676, 73.7754, 25.2449, 76.7052, 198.663, 9.2349, 173.724, 148.778, 16.4434, 13.5197, 106.833, 69.277, 50.1358, 81.167, 192.499, 104.062, 84.5607, 5.38957, 136.967, 114.164, 11.2735, 89.8526, 190.326, 5.02945, 71.511, 135.356, 26.9539, 50.7523, 174.163, 101.553, 100.998, 93.2463, 88.7234, 43.6903, 76.809, 16.4983, 113.297, 124.314, 134.465, 95.7915, 17.5298, 99.9786, 150.188, 21.8879, 60.6098, 82.9859, 151.854, 0.305185, 84.2067, 178.088, 49.4217, 125.468, 80.4529, 194.208, 40.4553, 29.9753, 34.9193, 31.1716, 90.5362, 8.81375, 44.5875, 141.423, 88.7539, 18.543, 110.801, 66.7135, 78.1945, 151.567, 7.22068, 49.8672, 161.388, 185.29, 22.8523, 121.47, 80.7642, 6.39058, 179.968, 61.5864, 74.16, 110.031, 127.055, 36.494, 5.26139, 174.035, 106.845, 110.404, 169.073, 108.151, 199.255, 32.1482, 172.393, 178.259, 39.3323, 96.9451, 89.2727, 5.56658, 10.2542, 83.0775, 91.5128, 40.8765, 111.301, 63.6372, 154.094, 113.755, 100.784, 93.234, 35.67, 56.7705, 114.054, 169.713, 134.037, 4.71816, 156.462, 70.864, 16.5838, 168.847, 71.3157, 66.1763, 25.7759, 86.0378, 182.867, 103.513, 159.63, 143.065, 14.2338, 70.8518, 27.1554, 175.805, 80.459, 133.018, 78.8659, 143.437, 156.383, 143.358, 112.204, 77.6757, 19.66, 130.509, 52.0219, 107.004, 68.7399, 60.0909, 6.08539, 155.864, 15.7964, 71.4255, 195.508, 118.888, 54.7258, 189.27, 160.192, 110.117, 104.111, 110.501, 39.3689, 187.225, 125.175, 65.8895, 127.03, 150.725, 114.768, 156.761, 102.371, 18.2073, 173.339, 166.753, 135.319, 173.54, 47.8652, 48.5244, 66.9271, 191.089, 149.889, 85.464, 64.9251, 134.941, 48.4146, 128.416, 154.125, 133.14, 37.9345, 107.407, 21.9062, 26.8746, 181.359, 18.3477, 98.7152, 94.7172, 27.9916, 184.783, 113.486, 51.8204, 6.53706, 194.549, 124.137, 124.186, 93.0143, 111.57, 23.0415, 197.821, 153.746, 162.517, 117.496, 42.9884, 163.353, 98.5565, 185.162, 13.4342, 97.6775, 32.0627, 4.55336, 20.5878, 101.798, 148.332, 160.552, 182.806, 132.963, 92.2941, 124.778, 149.528, 78.5607, 30.2561, 11.1393, 100.583, 149.132, 186.615, 6.2685, 20.3558, 146.275, 55.5315, 75.0023, 47.0046, 5.66424, 87.7163, 140.831, 96.7009, 14.9846, 52.1073, 141.392, 162.835, 106.943, 72.6035, 158.922, 90.2249, 27.1737, 139.769, 25.721, 24.3721, 177.282, 97.5616, 4.66323, 135.643, 132.737, 105.069, 144.603, 18.4881, 35.9081, 173.193, 110.886, 165.703, 63.9851, 176.379, 90.7926, 117.588, 171.978, 139.366, 174.047, 86.2148, 110.666, 198.859, 8.23389, 54.6159, 119.572, 40.2722, 129.307, 151.03, 94.9309, 9.45463, 167.04, 98.0804, 32.6731, 99.3988, 125.492, 137.046, 146.379, 11.9327 };
						static int n[8192] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8, 6, 9, 2, 4, 2, 11, 10, 2, 5, 5, 4, 3, 10, 9, 7, 8, 9, 3, 9, 11, 6, 11, 4, 8, 9, 4, 7, 9, 2, 7, 3, 8, 11, 3, 10, 6, 5, 9, 3, 7, 6, 7, 4, 7, 5, 3, 6, 11, 10, 7, 2, 4, 2, 8, 8, 6, 3, 10, 6, 8, 4, 2, 4, 11, 5, 10, 2, 9, 5, 7, 3, 2, 9, 4, 8, 4, 11, 2, 6, 5, 11, 10, 2, 7, 7, 11, 3, 8, 4, 5, 4, 8, 5, 5, 10, 3, 4, 6, 9, 2, 6, 3, 9, 7, 2, 9, 2, 6, 7, 2, 7, 4, 11, 2, 6, 7, 9, 5, 7, 5, 10, 10, 5, 7, 6, 2, 4, 7, 3, 6, 5, 3, 3, 8, 2, 7, 11, 3, 2, 5, 7, 9, 8, 8, 2, 4, 6, 3, 4, 6, 11, 5, 6, 6, 8, 11, 5, 4, 6, 2, 8, 7, 8, 2, 9, 6, 7, 4, 3, 8, 8, 5, 3, 7, 2, 4, 5, 6, 5, 6, 11, 9, 9, 4, 10, 5, 3, 2, 5, 6, 7, 3, 5, 2, 9, 10, 3, 8, 10, 5, 6, 5, 11, 2, 8, 2, 2, 6, 10, 6, 10, 9, 6, 8, 5, 4, 11, 5, 5, 11, 3, 6, 2, 11, 11, 3, 10, 11, 6, 6, 4, 7, 8, 11, 6, 3, 4, 5, 10, 5, 10, 2, 10, 5, 9, 4, 10, 2, 5, 6, 8, 11, 3, 7, 7, 7, 2, 5, 8, 9, 8, 9, 8, 9, 6, 5, 2, 7, 7, 10, 5, 7, 5, 10, 9, 11, 7, 2, 5, 3, 2, 5, 4, 7, 4, 5, 4, 3, 5, 6, 10, 10, 2, 6, 11, 11, 9, 4, 10, 5, 8, 6, 11, 6, 10, 7, 11, 2, 2, 9, 5, 8, 9, 8, 3, 4, 8, 2, 3, 4, 8, 9, 10, 3, 7, 4, 8, 7, 7, 6, 2, 8, 3, 5, 4, 2, 5, 7, 4, 8, 8, 10, 5, 7, 5, 3, 2, 11, 7, 11, 2, 6, 8, 2, 11, 6, 8, 3, 5, 8, 7, 5, 5, 10, 5, 6, 6, 11, 4, 5, 8, 9, 10, 10, 9, 11, 11, 4, 6, 7, 3, 2, 8, 4, 4, 3, 10, 2, 4, 6, 11, 11, 10, 5, 8, 6, 10, 8, 5, 6, 11, 2, 9, 6, 3, 9, 7, 4, 5, 4, 8, 2, 7, 8, 3, 10, 4, 11, 7, 11, 9, 10, 7, 2, 4, 10, 4, 3, 2, 6, 11, 7, 8, 11, 10, 4, 11, 4, 7, 2, 6, 7, 11, 9, 5, 5, 10, 2, 6, 2, 5, 9, 5, 8, 9, 11, 10, 3, 6, 9, 7, 10, 8, 10, 9, 4, 3, 6, 11, 11, 5, 10, 7, 5, 9, 3, 6, 6, 7, 5, 10, 11, 10, 11, 7, 6, 4, 4, 8, 6, 3, 4, 10, 3, 10, 9, 9, 10, 9, 9, 6, 5, 6, 8, 5, 9, 4, 9, 6, 4, 3, 9, 4, 3, 10, 4, 6, 2, 3, 8, 9, 8, 5, 9, 8, 11, 7, 2, 2, 9, 3, 11, 6, 2, 10, 3, 3, 9, 6, 3, 7, 10, 11, 2, 9, 2, 7, 7, 7, 2, 7, 11, 4, 11, 5, 7, 5, 6, 9, 5, 3, 8, 3, 2, 4, 4, 8, 10, 2, 6, 9, 5, 5, 6, 5, 8, 3, 11, 4, 11, 3, 3, 5, 11, 4, 2, 9, 2, 11, 4, 2, 11, 6, 6, 8, 2, 8, 4, 7, 9, 2, 6, 3, 5, 2, 9, 5, 8, 8, 2, 8, 6, 9, 7, 3, 11, 10, 10, 6, 11, 9, 3, 10, 9, 7, 9, 3, 6, 5, 3, 2, 5, 6, 7, 5, 4, 4, 2, 9, 9, 4, 4, 9, 8, 9, 11, 5, 2, 4, 11, 7, 11, 10, 7, 6, 5, 3, 11, 3, 6, 2, 3, 6, 5, 6, 6, 11, 6, 4, 9, 5, 6, 2, 10, 10, 9, 10, 4, 3, 11, 9, 2, 7, 11, 8, 4, 11, 10, 7, 9, 8, 8, 6, 2, 10, 6, 3, 9, 5, 7, 10, 10, 5, 9, 3, 7, 5, 9, 5, 11, 7, 11, 6, 7, 4, 3, 5, 4, 11, 3, 8, 4, 8, 10, 7, 11, 3, 6, 11, 10, 6, 2, 5, 8, 11, 4, 8, 3, 4, 3, 10, 10, 4, 11, 10, 11, 7, 8, 4, 5, 4, 11, 2, 10, 9, 11, 11, 6, 10, 8, 2, 3, 5, 9, 6, 2, 4, 5, 9, 7, 6, 3, 11, 4, 10, 9, 2, 7, 9, 9, 8, 4, 11, 3, 5, 9, 8, 2, 8, 5, 7, 11, 10, 10, 8, 9, 9, 2, 11, 3, 8, 3, 6, 9, 5, 9, 8, 7, 6, 8, 6, 4, 3, 6, 5, 10, 5, 2, 4, 4, 5, 2, 5, 3, 9, 3, 11, 2, 10, 5, 6, 4, 8, 8, 9, 4, 7, 5, 8, 5, 6, 4, 6, 8, 10, 9, 5, 5, 10, 7, 2, 10, 3, 5, 5, 5, 11, 3, 3, 11, 6, 11, 4, 2, 11, 6, 7, 3, 6, 7, 10, 8, 6, 5, 11, 10, 6, 5, 7, 11, 7, 5, 6, 7, 10, 6, 3, 8, 2, 11, 9, 10, 5, 2, 4, 5, 10, 10, 10, 2, 6, 5, 10, 11, 11, 3, 5, 10, 3, 9, 7, 6, 4, 3, 6, 11, 8, 9, 5, 7, 5, 5, 2, 6, 2, 8, 7, 11, 9, 3, 3, 7, 8, 9, 3, 2, 4, 4, 4, 10, 9, 11, 8, 4, 10, 8, 3, 10, 4, 6, 5, 5, 6, 2, 2, 6, 7, 10, 7, 3, 11, 7, 5, 9, 6, 8, 10, 6, 11, 7, 9, 4, 7, 6, 8, 6, 4, 7, 10, 4, 2, 9, 4, 2, 10, 11, 10, 8, 6, 4, 10, 10, 11, 11, 7, 8, 2, 11, 2, 7, 8, 8, 3, 5, 4, 2, 8, 11, 4, 4, 5, 5, 9, 6, 11, 11, 3, 7, 9, 4, 10, 11, 7, 8, 2, 6, 10, 6, 11, 5, 7, 3, 9, 2, 9, 10, 5, 10, 3, 2, 11, 3, 8, 5, 9, 10, 2, 7, 9, 6, 7, 5, 4, 4, 9, 10, 9, 10, 3, 6, 4, 7, 10, 10, 11, 6, 8, 8, 8, 7, 6, 11, 4, 9, 2, 9, 11, 6, 2, 8, 5, 2, 3, 5, 10, 2, 3, 4, 7, 11, 3, 5, 7, 11, 5, 10, 10, 2, 9, 7, 6, 3, 10, 5, 3, 2, 11, 4, 9, 5, 4, 8, 8, 8, 10, 10, 6, 6, 6, 8, 11, 7, 3, 8, 4, 5, 2, 11, 2, 3, 7, 8, 11, 2, 3, 8, 8, 6, 5, 6, 2, 9, 7, 11, 4, 7, 2, 6, 2, 5, 9, 3, 7, 3, 7, 6, 10, 8, 2, 10, 5, 2, 7, 5, 5, 4, 4, 11, 11, 9, 4, 5, 6, 9, 10, 4, 9, 10, 8, 2, 6, 7, 5, 7, 11, 3, 4, 6, 8, 6, 11, 6, 5, 8, 11, 11, 10, 10, 7, 10, 11, 2, 2, 2, 10, 6, 8, 3, 3, 8, 9, 7, 2, 8, 3, 7, 8, 3, 2, 2, 7, 9, 4, 10, 10, 4, 9, 3, 10, 7, 5, 11, 11, 4, 5, 9, 10, 5, 5, 9, 11, 10, 8, 4, 6, 5, 9, 4, 5, 5, 5, 6, 8, 10, 3, 5, 8, 7, 7, 7, 6, 5, 10, 2, 2, 11, 5, 11, 7, 8, 3, 11, 3, 5, 2, 4, 2, 2, 8, 8, 10, 2, 10, 3, 10, 6, 9, 3, 7, 5, 3, 2, 4, 9, 8, 11, 6, 2, 11, 2, 6, 9, 11, 2, 5, 11, 9, 3, 10, 6, 3, 2, 8, 4, 5, 10, 8, 8, 9, 3, 5, 7, 8, 9, 6, 9, 8, 11, 5, 10, 11, 10, 8, 7, 5, 6, 8, 4, 3, 8, 2, 6, 3, 10, 7, 2, 11, 3, 9, 3, 11, 9, 11, 8, 4, 9, 7, 6, 3, 2, 2, 2, 2, 3, 2, 3, 11, 8, 9, 11, 9, 5, 7, 8, 7, 8, 6, 5, 3, 3, 9, 10, 6, 11, 4, 4, 9, 7, 10, 7, 4, 11, 9, 11, 4, 2, 7, 5, 11, 5, 8, 6, 2, 4, 2, 11, 11, 3, 2, 3, 8, 10, 7, 2, 8, 9, 4, 9, 2, 7, 3, 4, 3, 10, 10, 10, 2, 8, 3, 3, 6, 6, 5, 4, 6, 6, 4, 7, 8, 4, 4, 11, 7, 8, 8, 7, 6, 5, 9, 6, 3, 6, 6, 11, 6, 11, 8, 2, 11, 4, 11, 3, 5, 10, 8, 7, 2, 3, 11, 4, 10, 11, 9, 8, 11, 9, 2, 7, 6, 11, 11, 9, 9, 2, 10, 7, 9, 8, 7, 7, 11, 6, 9, 2, 2, 10, 9, 7, 6, 10, 11, 3, 6, 8, 8, 8, 6, 10, 5, 7, 4, 5, 8, 9, 7, 5, 5, 6, 4, 4, 2, 5, 3, 10, 11, 3, 3, 7, 8, 6, 9, 11, 11, 8, 4, 6, 8, 6, 2, 8, 7, 8, 3, 9, 3, 7, 7, 6, 2, 5, 4, 9, 6, 6, 4, 3, 4, 4, 9, 9, 6, 4, 11, 5, 2, 9, 7, 2, 11, 6, 8, 2, 5, 4, 3, 6, 5, 2, 2, 9, 3, 4, 7, 9, 4, 8, 9, 6, 4, 6, 10, 8, 3, 10, 8, 9, 10, 10, 11, 11, 6, 11, 10, 11, 10, 2, 2, 11, 7, 3, 9, 7, 8, 9, 10, 7, 2, 8, 11, 10, 8, 3, 2, 7, 11, 3, 6, 5, 7, 7, 3, 4, 2, 2, 4, 9, 9, 5, 8, 9, 9, 9, 5, 7, 11, 10, 7, 10, 4, 3, 11, 7, 2, 4, 2, 11, 2, 10, 6, 3, 11, 11, 4, 7, 11, 2, 9, 7, 4, 8, 4, 9, 3, 5, 4, 7, 2, 10, 10, 6, 7, 8, 7, 11, 5, 4, 11, 4, 7, 7, 10, 8, 2, 10, 9, 5, 4, 8, 9, 3, 3, 2, 2, 10, 2, 9, 8, 5, 6, 5, 10, 6, 6, 8, 6, 8, 3, 8, 2, 6, 9, 10, 6, 4, 10, 10, 3, 8, 11, 2, 8, 11, 9, 2, 2, 2, 8, 5, 6, 2, 3, 9, 4, 8, 8, 6, 2, 6, 5, 9, 2, 4, 2, 4, 11, 7, 7, 8, 6, 3, 4, 10, 5, 10, 8, 5, 11, 8, 4, 7, 8, 11, 9, 6, 9, 9, 10, 5, 7, 3, 7, 2, 8, 11, 6, 2, 7, 6, 11, 9, 9, 4, 4, 10, 11, 10, 6, 4, 4, 3, 3, 9, 6, 5, 4, 6, 7, 9, 7, 7, 11, 11, 11, 5, 5, 7, 7, 8, 8, 11, 7, 6, 10, 11, 5, 8, 9, 2, 10, 5, 6, 10, 8, 2, 7, 9, 6, 3, 10, 9, 8, 2, 2, 10, 3, 4, 3, 7, 7, 2, 2, 2, 2, 4, 3, 10, 8, 10, 6, 10, 4, 9, 6, 9, 3, 9, 8, 2, 5, 5, 8, 11, 10, 11, 3, 6, 5, 4, 4, 5, 10, 6, 7, 3, 11, 3, 11, 5, 5, 9, 11, 6, 9, 10, 4, 6, 2, 9, 10, 11, 9, 2, 7, 8, 11, 2, 2, 2, 2, 8, 5, 11, 10, 9, 8, 7, 4, 8, 6, 6, 11, 10, 6, 8, 7, 8, 6, 4, 9, 3, 9, 6, 9, 3, 2, 9, 9, 3, 5, 5, 9, 11, 11, 10, 3, 11, 9, 11, 6, 6, 6, 9, 8, 5, 7, 2, 10, 4, 6, 11, 10, 8, 2, 9, 4, 5, 6, 7, 3, 2, 5, 5, 11, 4, 6, 5, 2, 10, 9, 11, 6, 8, 8, 7, 10, 6, 11, 9, 3, 6, 7, 5, 9, 11, 6, 4, 6, 3, 9, 4, 11, 3, 5, 10, 3, 9, 10, 4, 4, 2, 9, 8, 11, 2, 6, 10, 10, 2, 9, 8, 5, 4, 10, 6, 11, 11, 7, 5, 3, 6, 9, 8, 2, 4, 3, 3, 3, 6, 4, 5, 2, 5, 3, 11, 10, 6, 2, 3, 4, 4, 4, 2, 4, 3, 10, 5, 2, 2, 8, 8, 8, 6, 2, 3, 6, 6, 9, 6, 3, 5, 2, 8, 4, 3, 7, 3, 11, 6, 6, 9, 6, 11, 3, 4, 10, 2, 5, 7, 8, 9, 8, 11, 11, 8, 3, 10, 3, 5, 5, 3, 3, 2, 11, 3, 5, 10, 2, 7, 4, 10, 6, 8, 10, 10, 4, 10, 10, 5, 8, 3, 5, 7, 8, 5, 5, 3, 3, 6, 3, 3, 7, 2, 7, 8, 3, 5, 2, 10, 10, 9, 6, 10, 7, 5, 2, 8, 10, 7, 5, 8, 10, 3, 10, 7, 11, 9, 10, 6, 11, 6, 5, 10, 2, 8, 4, 7, 7, 6, 10, 5, 6, 5, 10, 2, 3, 2, 7, 11, 5, 3, 2, 2, 10, 2, 9, 2, 6, 6, 11, 4, 5, 10, 2, 7, 3, 3, 10, 7, 11, 5, 8, 7, 8, 4, 2, 6, 6, 9, 2, 4, 6, 8, 5, 3, 5, 11, 9, 3, 10, 11, 7, 10, 10, 5, 3, 4, 2, 6, 3, 11, 2, 6, 3, 7, 6, 6, 5, 8, 3, 11, 11, 11, 5, 4, 11, 9, 10, 9, 9, 10, 6, 10, 4, 2, 8, 6, 10, 10, 7, 11, 11, 3, 3, 11, 2, 3, 6, 3, 8, 6, 2, 10, 3, 6, 3, 5, 2, 5, 7, 11, 6, 2, 9, 9, 3, 8, 11, 2, 5, 11, 7, 10, 3, 6, 6, 9, 3, 5, 2, 6, 4, 11, 8, 4, 3, 11, 8, 3, 8, 7, 6, 8, 8, 8, 7, 7, 9, 4, 7, 10, 9, 6, 5, 9, 5, 3, 9, 4, 4, 10, 4, 2, 10, 2, 2, 8, 9, 6, 7, 7, 11, 8, 7, 8, 3, 4, 2, 6, 4, 10, 2, 4, 5, 8, 5, 6, 9, 2, 10, 7, 5, 6, 5, 5, 2, 9, 6, 5, 8, 2, 2, 4, 2, 2, 4, 6, 10, 7, 5, 6, 4, 6, 11, 3, 6, 4, 6, 9, 11, 7, 4, 7, 3, 7, 10, 5, 8, 7, 7, 3, 8, 8, 8, 2, 4, 9, 2, 3, 8, 3, 4, 2, 11, 11, 5, 4, 3, 8, 9, 8, 4, 4, 4, 7, 8, 10, 5, 10, 3, 4, 2, 6, 6, 4, 2, 4, 6, 2, 2, 6, 7, 5, 8, 8, 3, 5, 5, 11, 11, 2, 6, 5, 5, 3, 9, 9, 10, 7, 2, 11, 6, 3, 3, 6, 2, 5, 4, 2, 4, 10, 4, 11, 6, 7, 9, 11, 2, 3, 8, 3, 3, 8, 5, 3, 5, 10, 8, 11, 8, 2, 4, 11, 4, 2, 3, 10, 3, 10, 6, 9, 9, 4, 4, 7, 6, 3, 11, 10, 5, 5, 9, 5, 3, 7, 4, 8, 4, 2, 8, 6, 3, 7, 7, 3, 6, 11, 10, 4, 9, 4, 5, 11, 5, 11, 9, 3, 5, 3, 11, 4, 6, 2, 11, 2, 4, 10, 6, 5, 4, 4, 11, 5, 4, 4, 11, 3, 8, 6, 3, 8, 9, 10, 8, 7, 3, 9, 9, 11, 6, 8, 2, 4, 4, 3, 7, 2, 2, 5, 5, 2, 4, 11, 11, 5, 4, 2, 6, 9, 11, 5, 5, 10, 2, 4, 7, 3, 10, 2, 8, 4, 8, 8, 5, 4, 2, 9, 3, 2, 11, 2, 9, 10, 8, 6, 7, 11, 2, 11, 7, 8, 2, 3, 10, 4, 9, 7, 6, 11, 7, 10, 2, 7, 4, 2, 6, 8, 8, 5, 8, 3, 5, 7, 9, 9, 5, 2, 5, 2, 10, 9, 3, 2, 2, 6, 8, 11, 10, 3, 2, 3, 11, 4, 8, 5, 5, 8, 5, 2, 9, 10, 6, 5, 11, 7, 8, 11, 9, 9, 2, 6, 10, 3, 3, 2, 9, 5, 7, 9, 4, 10, 4, 2, 4, 10, 10, 7, 2, 2, 3, 3, 2, 7, 9, 3, 4, 7, 11, 2, 10, 10, 9, 5, 4, 11, 10, 11, 5, 7, 5, 10, 4, 11, 6, 3, 4, 7, 10, 11, 7, 11, 5, 11, 4, 5, 6, 10, 10, 2, 9, 7, 3, 3, 9, 11, 4, 7, 9, 5, 7, 10, 4, 7, 10, 3, 11, 3, 3, 2, 8, 10, 4, 10, 7, 6, 4, 4, 7, 4, 6, 6, 2, 3, 9, 8, 9, 9, 3, 7, 11, 10, 9, 9, 7, 4, 3, 5, 10, 10, 9, 11, 11, 3, 5, 3, 10, 2, 9, 10, 6, 11, 7, 10, 5, 9, 3, 11, 6, 4, 3, 3, 6, 3, 9, 10, 3, 7, 6, 4, 11, 3, 6, 8, 5, 7, 3, 3, 7, 10, 9, 4, 8, 2, 11, 7, 5, 6, 7, 2, 5, 4, 8, 9, 8, 5, 10, 6, 10, 5, 11, 7, 10, 3, 2, 8, 3, 6, 11, 2, 3, 8, 10, 8, 3, 6, 6, 8, 3, 6, 6, 3, 7, 5, 2, 6, 5, 7, 11, 2, 3, 6, 9, 8, 2, 5, 2, 2, 6, 9, 3, 2, 7, 2, 4, 10, 11, 7, 8, 2, 11, 2, 2, 3, 11, 2, 9, 6, 6, 2, 7, 8, 5, 11, 4, 2, 7, 11, 4, 5, 2, 11, 4, 3, 11, 10, 8, 3, 9, 9, 5, 9, 6, 10, 3, 3, 7, 3, 2, 8, 4, 3, 6, 8, 7, 2, 8, 2, 7, 9, 4, 5, 8, 6, 8, 5, 7, 3, 4, 11, 2, 7, 7, 6, 5, 6, 11, 9, 10, 4, 6, 11, 5, 7, 5, 9, 7, 4, 5, 2, 8, 5, 10, 6, 9, 3, 8, 6, 5, 2, 10, 6, 11, 8, 10, 10, 11, 9, 3, 10, 2, 8, 6, 3, 8, 11, 3, 5, 3, 11, 9, 11, 7, 9, 10, 3, 7, 8, 11, 3, 2, 10, 8, 11, 9, 2, 5, 8, 7, 8, 11, 11, 4, 4, 7, 2, 2, 4, 5, 10, 8, 11, 3, 2, 4, 10, 2, 8, 11, 3, 11, 4, 11, 3, 7, 4, 2, 8, 9, 4, 10, 5, 5, 8, 7, 7, 5, 3, 3, 8, 3, 6, 5, 2, 2, 4, 3, 3, 3, 5, 10, 7, 4, 4, 9, 6, 9, 7, 7, 9, 3, 3, 11, 3, 8, 5, 6, 10, 5, 2, 7, 3, 2, 5, 4, 6, 11, 6, 11, 9, 4, 4, 10, 6, 7, 8, 3, 8, 5, 7, 8, 6, 7, 8, 2, 6, 9, 6, 6, 2, 3, 4, 3, 3, 4, 8, 2, 11, 4, 4, 6, 5, 2, 8, 3, 2, 4, 9, 5, 8, 7, 4, 6, 8, 10, 11, 8, 11, 11, 3, 10, 8, 6, 10, 5, 5, 6, 4, 5, 4, 6, 7, 10, 8, 8, 3, 11, 3, 4, 11, 7, 7, 6, 2, 11, 7, 2, 5, 8, 5, 5, 9, 7, 10, 10, 4, 6, 4, 10, 2, 6, 7, 3, 10, 8, 5, 10, 11, 4, 5, 2, 5, 7, 8, 5, 9, 11, 5, 5, 8, 3, 8, 5, 2, 6, 6, 3, 8, 6, 9, 2, 2, 7, 8, 11, 9, 7, 8, 10, 6, 7, 9, 5, 3, 4, 4, 6, 8, 10, 9, 9, 2, 6, 9, 11, 4, 2, 2, 11, 2, 6, 10, 6, 7, 6, 4, 6, 6, 8, 7, 5, 9, 7, 4, 2, 4, 10, 2, 3, 6, 11, 2, 6, 4, 5, 3, 8, 2, 9, 9, 3, 5, 8, 8, 10, 7, 11, 7, 5, 6, 4, 5, 9, 2, 4, 4, 10, 8, 11, 9, 6, 4, 5, 5, 7, 2, 9, 2, 8, 7, 9, 6, 6, 6, 9, 8, 7, 4, 6, 4, 3, 11, 10, 8, 10, 4, 11, 2, 7, 3, 5, 3, 3, 4, 10, 5, 4, 3, 10, 2, 6, 2, 11, 6, 9, 4, 9, 2, 5, 6, 3, 2, 9, 11, 7, 2, 3, 4, 4, 5, 2, 6, 11, 6, 3, 8, 4, 5, 10, 2, 3, 11, 4, 6, 11, 2, 10, 4, 9, 8, 9, 11, 3, 11, 10, 8, 4, 4, 4, 9, 10, 8, 3, 4, 4, 9, 5, 7, 9, 5, 7, 5, 7, 6, 8, 2, 6, 11, 7, 2, 6, 2, 8, 7, 4, 10, 3, 10, 6, 4, 10, 9, 2, 2, 9, 11, 10, 5, 8, 7, 9, 11, 8, 5, 2, 8, 10, 6, 6, 8, 11, 4, 5, 6, 6, 10, 11, 4, 6, 6, 11, 3, 10, 7, 3, 4, 10, 3, 8, 10, 2, 11, 2, 7, 5, 6, 2, 8, 8, 9, 4, 4, 11, 4, 8, 8, 2, 5, 9, 4, 8, 4, 9, 3, 8, 11, 11, 9, 5, 3, 9, 10, 8, 3, 7, 3, 3, 9, 2, 11, 3, 5, 6, 6, 6, 4, 9, 3, 4, 8, 9, 9, 8, 9, 2, 8, 6, 2, 4, 8, 5, 3, 10, 4, 4, 2, 9, 4, 6, 5, 4, 10, 5, 4, 10, 8, 7, 6, 3, 3, 10, 10, 11, 2, 4, 9, 6, 5, 7, 6, 7, 8, 9, 6, 7, 11, 3, 11, 11, 4, 7, 5, 7, 3, 7, 8, 10, 11, 4, 3, 10, 4, 6, 7, 10, 5, 5, 6, 9, 10, 4, 3, 3, 3, 8, 11, 10, 3, 5, 7, 4, 11, 11, 8, 8, 7, 5, 5, 6, 3, 7, 4, 10, 4, 5, 7, 3, 5, 2, 6, 4, 2, 3, 8, 2, 4, 3, 9, 3, 3, 11, 2, 7, 3, 2, 2, 3, 2, 6, 4, 9, 7, 9, 8, 9, 2, 11, 5, 2, 7, 4, 10, 2, 2, 5, 8, 2, 8, 6, 8, 2, 4, 2, 10, 6, 10, 6, 5, 4, 8, 7, 2, 6, 3, 10, 3, 5, 6, 7, 6, 8, 11, 11, 2, 3, 10, 6, 5, 11, 8, 7, 6, 6, 4, 2, 2, 7, 3, 8, 2, 3, 10, 4, 7, 2, 9, 11, 8, 7, 10, 10, 3, 9, 6, 3, 11, 5, 2, 10, 5, 2, 5, 9, 11, 6, 10, 11, 10, 2, 6, 4, 10, 7, 10, 5, 2, 10, 8, 6, 9, 8, 7, 3, 10, 11, 3, 3, 8, 10, 11, 2, 9, 7, 5, 4, 8, 7, 10, 3, 2, 10, 2, 9, 2, 5, 4, 7, 5, 11, 10, 6, 10, 5, 11, 2, 9, 9, 5, 10, 11, 5, 5, 7, 11, 6, 2, 4, 2, 6, 7, 9, 9, 9, 10, 11, 3, 8, 11, 9, 7, 4, 11, 4, 8, 5, 9, 11, 11, 4, 8, 7, 10, 6, 11, 3, 9, 6, 6, 3, 8, 3, 11, 5, 7, 6, 6, 4, 6, 11, 9, 8, 4, 5, 5, 3, 5, 11, 9, 8, 5, 5, 5, 4, 11, 6, 10, 11, 6, 3, 2, 2, 9, 11, 7, 11, 6, 4, 10, 8, 11, 10, 8, 3, 4, 8, 2, 6, 4, 7, 11, 7, 2, 11, 7, 5, 10, 8, 2, 2, 7, 9, 5, 6, 6, 5, 10, 4, 10, 3, 11, 8, 10, 7, 10, 2, 11, 5, 8, 6, 7, 9, 6, 7, 8, 6, 11, 11, 2, 8, 10, 9, 4, 7, 4, 9, 2, 5, 7, 6, 6, 4, 5, 3, 2, 5, 10, 7, 3, 7, 5, 9, 5, 9, 7, 11, 3, 4, 9, 5, 3, 6, 9, 2, 2, 4, 4, 3, 7, 2, 7, 3, 5, 4, 2, 10, 6, 8, 2, 3, 2, 10, 4, 7, 8, 8, 8, 10, 11, 6, 2, 11, 6, 4, 5, 5, 7, 7, 9, 5, 10, 9, 10, 7, 5, 6, 2, 7, 11, 6, 2, 5, 2, 11, 9, 5, 5, 11, 5, 9, 10, 9, 6, 11, 2, 7, 5, 8, 5, 11, 8, 8, 9, 3, 7, 6, 5, 4, 8, 3, 4, 10, 11, 8, 5, 3, 5, 8, 3, 8, 5, 6, 11, 11, 4, 10, 4, 2, 8, 7, 10, 7, 7, 7, 3, 5, 2, 4, 8, 9, 2, 6, 10, 4, 6, 6, 3, 6, 9, 11, 10, 10, 8, 9, 11, 7, 5, 10, 2, 11, 5, 11, 4, 7, 8, 8, 7, 7, 9, 7, 4, 7, 11, 7, 4, 6, 10, 7, 4, 10, 8, 11, 3, 10, 3, 8, 9, 7, 6, 7, 6, 7, 9, 7, 2, 7, 10, 6, 8, 4, 5, 9, 3, 6, 11, 3, 2, 4, 6, 5, 11, 2, 4, 11, 6, 6, 4, 8, 11, 3, 9, 5, 5, 6, 2, 6, 3, 3, 8, 7, 11, 4, 8, 10, 4, 10, 11, 6, 5, 3, 7, 9, 9, 6, 5, 9, 3, 6, 4, 2, 4, 11, 3, 9, 6, 7, 10, 5, 10, 10, 6, 8, 9, 9, 7, 3, 4, 9, 10, 4, 8, 6, 11, 2, 8, 4, 8, 11, 9, 4, 7, 6, 10, 8, 5, 4, 6, 2, 11, 5, 10, 7, 10, 5, 4, 3, 4, 7, 10, 4, 11, 9, 10, 3, 5, 5, 3, 7, 3, 3, 5, 4, 11, 2, 7, 7, 6, 5, 2, 5, 2, 3, 4, 4, 8, 4, 10, 11, 10, 8, 9, 11, 6, 11, 2, 5, 8, 11, 11, 3, 10, 3, 3, 5, 5, 7, 3, 9, 5, 9, 8, 5, 4, 2, 7, 9, 9, 3, 7, 4, 10, 5, 4, 7, 10, 10, 4, 4, 11, 2, 6, 7, 3, 8, 3, 7, 8, 4, 5, 3, 8, 2, 8, 11, 9, 10, 8, 5, 4, 10, 5, 3, 10, 4, 11, 3, 9, 9, 5, 10, 7, 2, 2, 3, 4, 5, 5, 4, 11, 11, 10, 6, 6, 10, 6, 7, 8, 9, 2, 6, 6, 11, 4, 4, 7, 5, 2, 10, 2, 9, 2, 9, 8, 2, 11, 3, 2, 3, 9, 3, 8, 2, 11, 2, 9, 9, 3, 8, 8, 8, 4, 11, 7, 6, 10, 5, 10, 4, 10, 8, 6, 2, 2, 6, 2, 8, 7, 5, 11, 9, 10, 2, 4, 5, 5, 5, 2, 8, 2, 4, 4, 11, 7, 10, 5, 3, 3, 6, 9, 3, 4, 2, 4, 10, 11, 5, 4, 2, 8, 8, 2, 3, 7, 5, 4, 3, 2, 3, 6, 3, 11, 5, 4, 7, 7, 7, 5, 10, 8, 11, 7, 8, 7, 7, 7, 6, 3, 4, 4, 10, 7, 5, 3, 2, 5, 6, 6, 7, 8, 6, 9, 2, 11, 6, 8, 10, 2, 6, 8, 7, 10, 6, 6, 11, 7, 4, 11, 2, 3, 4, 11, 4, 11, 9, 9, 7, 10, 2, 11, 10, 11, 10, 7, 2, 10, 5, 6, 9, 6, 10, 2, 2, 4, 10, 2, 9, 3, 4, 10, 11, 3, 9, 8, 2, 10, 2, 10, 7, 4, 4, 7, 3, 3, 7, 10, 4, 6, 11, 11, 10, 8, 8, 10, 6, 4, 6, 2, 5, 11, 3, 2, 8, 5, 4, 8, 6, 9, 10, 5, 2, 11, 6, 9, 11, 3, 9, 7, 4, 5, 8, 8, 3, 8, 6, 4, 8, 11, 7, 3, 6, 10, 8, 4, 6, 11, 9, 4, 3, 7, 11, 7, 11, 4, 3, 10, 4, 11, 7, 2, 2, 3, 10, 11, 3, 3, 10, 2, 9, 9, 2, 6, 6, 2, 2, 9, 9, 11, 7, 5, 8, 6, 8, 7, 6, 3, 5, 5, 7, 2, 6, 6, 8, 4, 6, 8, 11, 3, 3, 6, 9, 6, 11, 2, 3, 6, 5, 6, 11, 4, 2, 8, 8, 3, 4, 3, 6, 9, 4, 2, 3, 6, 3, 7, 10, 2, 8, 9, 4, 11, 5, 8, 10, 4, 7, 4, 11, 2, 7, 2, 8, 8, 11, 2, 8, 11, 7, 7, 4, 10, 3, 9, 7, 5, 8, 10, 9, 10, 9, 2, 2, 5, 5, 4, 11, 11, 6, 6, 9, 6, 4, 8, 4, 10, 5, 9, 7, 2, 4, 11, 10, 6, 11, 7, 2, 7, 5, 4, 5, 10, 6, 10, 4, 8, 9, 9, 6, 5, 7, 5, 11, 6, 11, 4, 5, 7, 8, 2, 8, 8, 3, 2, 6, 4, 8, 7, 3, 4, 9, 7, 9, 3, 10, 7, 10, 6, 11, 3, 6, 2, 2, 7, 7, 7, 10, 5, 5, 5, 5, 3, 3, 10, 4, 2, 4, 10, 6, 4, 5, 10, 9, 3, 10, 9, 2, 7, 8, 9, 3, 2, 3, 10, 5, 3, 3, 6, 10, 3, 3, 8, 5, 10, 5, 6, 9, 2, 6, 10, 2, 11, 3, 6, 7, 3, 2, 6, 8, 7, 2, 10, 5, 10, 9, 2, 3, 2, 7, 4, 11, 8, 3, 9, 10, 10, 6, 7, 7, 9, 6, 8, 4, 5, 2, 5, 7, 3, 4, 4, 6, 6, 9, 10, 5, 6, 7, 4, 6, 8, 7, 7, 8, 4, 2, 2, 5, 2, 11, 9, 3, 6, 6, 8, 4, 5, 9, 4, 6, 8, 8, 9, 6, 2, 2, 9, 7, 7, 11, 7, 8, 3, 2, 4, 4, 4, 7, 7, 7, 5, 11, 2, 4, 2, 9, 2, 5, 9, 9, 5, 3, 6, 8, 7, 4, 8, 10, 4, 5, 7, 3, 6, 3, 4, 10, 10, 10, 5, 6, 6, 6, 3, 3, 7, 9, 4, 8, 4, 6, 10, 9, 4, 3, 6, 5, 4, 4, 5, 7, 6, 10, 4, 8, 10, 11, 8, 11, 11, 7, 3, 5, 9, 5, 9, 8, 4, 10, 5, 9, 3, 6, 10, 5, 10, 10, 9, 11, 4, 7, 9, 3, 10, 10, 5, 9, 10, 3, 2, 4, 11, 10, 9, 4, 4, 10, 7, 2, 4, 3, 3, 4, 9, 8, 3, 6, 8, 3, 7, 10, 11, 5, 6, 2, 4, 2, 2, 2, 6, 3, 7, 9, 4, 5, 2, 9, 10, 9, 5, 10, 2, 5, 6, 9, 2, 7, 5, 7, 6, 2, 5, 6, 6, 3, 2, 4, 10, 2, 6, 9, 7, 7, 5, 4, 11, 4, 6, 10, 3, 4, 2, 7, 8, 9, 6, 8, 9, 11, 4, 7, 5, 3, 7, 11, 3, 8, 8, 4, 6, 10, 11, 8, 9, 6, 9, 3, 6, 8, 2, 5, 10, 8, 6, 7, 6, 6, 2, 6, 7, 2, 8, 3, 2, 11, 11, 10, 11, 11, 3, 3, 2, 7, 10, 2, 10, 3, 3, 2, 6, 5, 8, 4, 5, 10, 9, 8, 3, 4, 9, 9, 3, 4, 9, 5, 6, 7, 5, 8, 9, 6, 10, 6, 4, 10, 2, 7, 5, 2, 5, 7, 5, 10, 9, 5, 4, 9, 5, 4, 3, 11, 9, 3, 3, 4, 7, 9, 3, 3, 10, 7, 8, 9, 2, 6, 9, 6, 3, 10, 9, 11, 11, 5, 8, 9, 5, 9, 9, 7, 7, 3, 9, 11, 3, 3, 9, 11, 8, 7, 9, 4, 6, 5, 4, 8, 6, 8, 2, 8, 5, 6, 5, 3, 2, 11, 5, 2, 3, 6, 2, 10, 3, 10, 4, 7, 4, 6, 6, 7, 6, 5, 8, 8, 8, 5, 4, 6, 7, 4, 9, 6, 5, 2, 8, 10, 8, 4, 11, 8, 9, 5, 5, 7, 9, 4, 11, 8, 5, 9, 6, 8, 8, 5, 5, 6, 9, 4, 5, 8, 5, 11, 2, 11, 11, 5, 8, 9, 3, 9, 4, 7, 3, 8, 9, 7, 5, 9, 7, 3, 5, 9, 11, 2, 3, 6, 2, 2, 8, 6, 11, 11, 9, 2, 4, 10, 3, 2, 6, 10, 9, 5, 7, 11, 11, 6, 3, 6, 2, 7, 6, 7, 8, 2, 10, 10, 10, 9, 2, 3, 10, 9, 6, 4, 11, 4, 7, 5, 9, 11, 4, 4, 4, 9, 6, 5, 7, 10, 5, 10, 2, 2, 3, 5, 6, 7, 4, 5, 11, 2, 4, 6, 8, 8, 4, 2, 4, 11, 3, 2, 5, 9, 9, 8, 3, 10, 2, 8, 2, 11, 4, 8, 8, 5, 8, 7, 7, 2, 2, 4, 8, 9, 2, 7, 9, 2, 7, 11, 2, 11, 9, 9, 2, 5, 10, 9, 7, 5, 10, 11, 3, 2, 5, 4, 8, 2, 8, 3, 6, 5, 11, 6, 8, 9, 11, 7, 10, 5, 6, 8, 3, 8, 8, 5, 8, 6, 5, 7, 5, 4, 7, 8, 2, 5, 5, 11, 6, 5, 8, 9, 5, 4, 7, 3, 5, 5, 2, 7, 10, 7, 11, 6, 4, 10, 4, 5, 8, 2, 9, 6, 10, 5, 8, 4, 11, 2, 9, 7, 3, 9, 4, 2, 11, 8, 7, 10, 7, 6, 10, 5, 10, 4, 11, 9, 4, 2, 6, 4, 3, 11, 8, 3, 5, 10, 8, 2, 5, 6, 11, 5, 9, 8, 10, 3, 8, 7, 6, 5, 5, 7, 5, 6, 4, 5, 6, 4, 4, 3, 10, 4, 10, 3, 7, 6, 5, 5, 9, 10, 8, 10, 3, 7, 4, 8, 7, 2, 8, 9, 8, 9, 8, 9, 8, 5, 3, 8, 3, 11, 7, 9, 2, 3, 4, 8, 9, 8, 6, 3, 6, 7, 9, 8, 7, 9, 3, 2, 11, 9, 8, 6, 5, 4, 10, 5, 7, 3, 5, 11, 7, 3, 7, 10, 4, 9, 6, 6, 6, 3, 8, 2, 7, 8, 5, 4, 5, 2, 10, 7, 10, 10, 3, 6, 10, 8, 3, 7, 3, 9, 4, 4, 6, 6, 10, 11, 2, 7, 3, 6, 11, 9, 10, 5, 6, 5, 8, 11, 7, 6, 11, 8, 6, 7, 2, 11, 4, 8, 5, 10, 11, 11, 2, 6, 3, 7, 2, 3, 6, 9, 9, 10, 4, 5, 2, 11, 10, 2, 7, 5, 2, 7, 5, 2, 8, 4, 4, 11, 7, 7, 11, 6, 11, 3, 7, 7, 2, 9, 2, 2, 10, 7, 4, 8, 4, 7, 9, 2, 10, 5, 9, 9, 8, 2, 2, 4, 5, 10, 2, 7, 9, 9, 11, 10, 5, 8, 2, 6, 6, 5, 2, 10, 2, 9, 6, 11, 2, 8, 4, 9, 2, 11, 7, 9, 3, 2, 2, 2, 4, 11, 6, 10, 8, 2, 5, 5, 10, 7, 8, 3, 8, 2, 2, 4, 3, 10, 8, 2, 3, 9, 3, 6, 4, 7, 8, 11, 3, 11, 7, 11, 2, 3, 3, 3, 11, 5, 4, 8, 9, 8, 3, 10, 7, 10, 6, 6, 11, 6, 11, 2, 6, 11, 5, 2, 10, 2, 9, 10, 3, 3, 3, 11, 3, 10, 8, 7, 5, 3, 2, 11, 7, 8, 10, 6, 4, 11, 10, 2, 5, 8, 2, 3, 6, 8, 8, 3, 8, 8, 2, 11, 9, 9, 5, 7, 5, 2, 9, 5, 5, 11, 5, 2, 2, 9, 10, 4, 8, 11, 9, 7, 9, 6, 3, 7, 9, 8, 4, 9, 9, 3, 6, 2, 3, 9, 11, 9, 8, 7, 4, 9, 6, 7, 8, 3, 10, 5, 11, 4, 7, 7, 2, 7, 2, 7, 4, 8, 9, 10, 3, 4, 9, 2, 6, 9, 8, 9, 9, 4, 8, 3, 5, 2, 11, 3, 9, 3, 6, 5, 5, 9, 8, 2, 9, 7, 6, 7, 2, 8, 8, 8, 11, 11, 7, 9, 3, 8, 6, 5, 9, 11, 5, 2, 3, 7, 5, 3, 9, 5, 4, 6, 2, 9, 8, 7, 11, 5, 8, 9, 11, 2, 10, 7, 2, 6, 7, 5, 7, 8, 8, 8, 5, 7, 2, 8, 4, 8, 7, 5, 8, 9, 8, 4, 10, 9, 2, 11, 10, 5, 8, 4, 9, 11, 5, 6, 4, 10, 7, 9, 2, 5, 4, 11, 9, 5, 6, 11, 11, 10, 9, 8, 9, 9, 9, 11, 2, 10, 4, 11, 2, 6, 11, 10, 3, 2, 3, 5, 5, 10, 9, 9, 10, 8, 3, 10, 6, 10, 9, 7, 2, 5, 11, 5, 7, 4, 10, 9, 7, 6, 7, 2, 2, 4, 5, 5, 6, 7, 6, 10, 5, 8, 8, 9, 8, 4, 11, 3, 4, 6, 6, 3, 5, 9, 2, 11, 7, 9, 9, 6, 2, 9, 5, 6, 7, 7, 3, 2, 6, 3, 10, 7, 6, 2, 5, 11, 2, 2, 8, 11, 11, 10, 9, 11, 11, 6, 10, 5, 7, 5, 9, 8, 9, 4, 9, 4, 7, 11, 2, 4, 10, 2, 3, 6, 11, 5, 3, 7, 3, 11, 8, 9, 6, 4, 9, 3, 10, 11, 3, 9, 5, 6, 5, 11, 10, 4, 8, 4, 2, 7, 3, 10, 10, 2, 2, 9, 5, 4, 2, 4, 8, 11, 7, 4, 2, 8, 5, 8, 8, 11, 5, 5, 3, 4, 7, 6, 8 };
						int an[8192];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
				}
			}
		}
	}
	system("pause");
	outputFile.close();
}

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type) {

	/*
	This subroutine determines how many additional runs each
	design will should have for next iteration of simulation.
	s_mean[i]: sample mean of design i, i=0,1,..,ND-1
	s_var[i]: sample variance of design i, i=0,1,..,ND-1
	nd: the number of designs
	n[i]: number of simulation replications of design i,
	i=0,1,..,ND-1
	add_budget: the additional simulation budget
	an[i]: additional number of simulation replications assigned
	to design i, i=0,1,..,ND-1
	type: type of optimazition problem. type=1, MIN problem;
	type=2, MAX problem
	*/
	int i;
	int b, s;
	int t_budget, t1_budget;
	int morerun[8192], more_alloc; /* 1:Yes; 0:No */
	float t_s_mean[8192];
	float ratio[8192]; /* Ni/Ns */
	float ratio_s, temp;
	if (type == 1) { /*MIN problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = s_mean[i];
	}
	else { /*MAX problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = (-1)*s_mean[i];
	}
	t_budget = add_budget;
	for (i = 0; i < nd; i++) t_budget += n[i];
	b = best(t_s_mean, nd);
	s = second_best(t_s_mean, nd, b);
	ratio[s] = 1.0;
	for (i = 0; i < nd; i++)
		if (i != s && i != b) {
			temp = (t_s_mean[b] - t_s_mean[s]) / (t_s_mean[b] - t_s_mean[i]);
			ratio[i] = temp*temp*s_var[i] / s_var[s];
		} /* calculate ratio of Ni/Ns*/
	temp = 0;
	for (i = 0; i < nd; i++) if (i != b) temp += (ratio[i] * ratio[i] / s_var[i]);
	ratio[b] = sqrt(s_var[b] * temp); /* calculate Nb */
	for (i = 0; i < nd; i++) morerun[i] = 1;
	t1_budget = t_budget;
	do{
		more_alloc = 0;
		ratio_s = 0.0;
		for (i = 0; i < nd; i++) if (morerun[i]) ratio_s += ratio[i];
		for (i = 0; i < nd; i++) if (morerun[i])
		{
			an[i] = (int)(t1_budget / ratio_s*ratio[i]);
			/* disable those design which have been run too much */
			if (an[i] < n[i])
			{
				an[i] = n[i];
				morerun[i] = 0;
				more_alloc = 1;
			}
		}
		if (more_alloc)
		{
			t1_budget = t_budget;
			for (i = 0; i < nd; i++) if (!morerun[i]) t1_budget -= an[i];
		}
	} while (more_alloc); /* end of WHILE */
	/* calculate the difference */
	t1_budget = an[0];
	for (i = 1; i < nd; i++) t1_budget += an[i];
	an[b] += (t_budget - t1_budget); /* give the difference
									 to design b */
	for (i = 0; i < nd; i++) an[i] -= n[i];
}

int best(float t_s_mean[], int nd) {
	/*This function determines the best design based on current
	simulation results */
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs */

	int i, min_index;
	min_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[min_index]) {
			min_index = i;
		}
	}
	return min_index;
}

int second_best(float t_s_mean[], int nd, int b) {
	/*This function determines the second best design based on
	current simulation results*/
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs.
	b: current best design determined by function
	best() */
	int i, second_index;
	if (b == 0) second_index = 1;
	else second_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[second_index] && i != b) {
			second_index = i;
		}
	}
	return second_index;
}