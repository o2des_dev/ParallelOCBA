﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelOCBA
{
    class NonParallelOCBA
    {
        private int _k; // number of designs
        private int _totalBudget;
        private int[] _n; 
        private double[] _sum;
        private double[] _sumSq;
        private double[] _mean;
        private double[] _var;

        private int _bIndex;
        private double _bValue;
        private double[] _snsr; // squared (noise to signal) ratio
        private double[] _snssr; // squared (noise to squared signal) ratio
        private double[] _alpha;
        private double _alphaSum;
        
        public NonParallelOCBA(int nDesign)
        {
            _k = nDesign;
            _totalBudget = 0;
            _n = new int[_k];
            _sum = new double[_k];
            _sumSq = new double[_k];
            _mean = new double[_k];
            _var = new double[_k];

            _bIndex = 0;
            _bValue = double.PositiveInfinity;
            _snsr = new double[_k];
            _snssr = new double[_k];
            _alpha = new double[_k];
        }

        public void Evaluate(Dictionary<int, List<double>> results)
        {
            foreach (var i in results)
            {
                int idx = i.Key;
                _n[idx] += i.Value.Count;
                _sum[idx] += i.Value.Sum();
                _sumSq[idx] += i.Value.Sum(v => v * v);
                _mean[idx] = _sum[idx] / _n[idx];
                _var[idx] = (_sumSq[idx] - (_sum[idx] * _sum[idx]) / _n[idx]) / (_n[idx] - 1);
            }

            Action<int> updateByIdx = idx => {
                double signalSq = Math.Pow(_mean[idx] - _bValue, 2);
                _snsr[idx] = _var[idx] / signalSq;
                _snssr[idx] = _snsr[idx] / signalSq;
                _alpha[idx] = _snsr[idx];
            };

            var b = results.Aggregate((i1, i2) => _mean[i1.Key] < _mean[i2.Key] ? i1 : i2);
            if (_mean[b.Key] < _bValue)
            {
                _bIndex = b.Key;
                _bValue = _mean[_bIndex];
                _bIndex = b.Key;
                for (int idx = 0; idx < _k; idx++) updateByIdx(idx);
            }
            else
            {
                foreach(var i in results) updateByIdx(i.Key);
            }

            _alpha[_bIndex] = Math.Sqrt(_var[_bIndex] * Enumerable.Range(0, _k).Sum(idx => idx != _bIndex ? _snssr[idx] : 0));
            _totalBudget = _n.Sum();
            _alphaSum = _alpha.Sum();
        }

        public List<int> Alloc(int budget)
        {
            var indices = new List<int>();

            int newBudget = _totalBudget + budget;
            double[] delta = Enumerable.Range(0, _k).Select(idx => _alpha[idx] / _alphaSum * newBudget - _n[idx]).ToArray();
            var sequence = Enumerable.Range(0, _k).OrderByDescending(idx => delta[idx]).ToList();
            while (indices.Count < budget)
            {
                int count = Math.Min((int)Math.Ceiling(delta[sequence.First()] - delta[sequence[1]]), budget - indices.Count);
                indices.AddRange(Enumerable.Repeat(sequence.First(), count));
                delta[sequence.First()] -= count;
                int i = 2;
                while (i < sequence.Count && delta[sequence.First()] < delta[sequence[i]]) i++;
                sequence.Insert(i, sequence.First());
                sequence.RemoveAt(0);
            }

            return indices;
        }
    }
}
