﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelOCBA
{
    class Program
    {
        static void Main(string[] args)
        {
            int nDesigns = 4000000;
            var pocba = new ParallelOCBA(nDesigns);
            var npocba = new NonParallelOCBA(nDesigns);

            var rs = new Random(0);
            var lb = Enumerable.Range(0, nDesigns).Select(i => rs.NextDouble() * 100).ToArray();
            var results = Enumerable.Range(0, nDesigns).ToDictionary(idx => idx, 
                idx => Enumerable.Range(0, 5).Select(i => lb[idx] + rs.NextDouble()).ToList());

            int budget = 1000;

            var stopwatch = Stopwatch.StartNew();
            pocba.Evaluate(results);
            pocba.Alloc(budget);
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.TotalSeconds);

            stopwatch.Restart();
            npocba.Evaluate(results);
            npocba.Alloc(budget);
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.TotalSeconds);
        }

        
    }
}
