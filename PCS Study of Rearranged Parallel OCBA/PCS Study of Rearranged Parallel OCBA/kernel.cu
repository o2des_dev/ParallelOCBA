#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <cuda.h>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include <fstream>
#include <random>
#include <time.h>

/* please put the following definitions at the head of your
program */
/*Define parameters for simulation*/
/*the number of designs. It should be larger than 2.*/
#define ND 16
#define GRIDDIMENSION 1
#define BLOCKDIMENSION 16
#define TYPE 1
/*maximization TYPE = -1, minimization TYPE = 1*/
/*prototypes of functions and subroutines included in this
file*/

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type);
int best(float t_s_mean[], int nd);
int second_best(float t_s_mean[], int nd, int b);

#ifdef _WIN32
#include <Windows.h>
double get_wall_time(){
	LARGE_INTEGER time, freq;
	if (!QueryPerformanceFrequency(&freq)){
		//  Handle error
		return 0;
	}
	if (!QueryPerformanceCounter(&time)){
		//  Handle error
		return 0;
	}
	return (double)time.QuadPart / freq.QuadPart;
}

double get_cpu_time(){
	FILETIME a, b, c, d;
	if (GetProcessTimes(GetCurrentProcess(), &a, &b, &c, &d) != 0){
		//  Returns total user time.
		return
			(double)(d.dwLowDateTime |
			((unsigned long long)d.dwHighDateTime << 32)) * 0.0000001;
	}
	else{
		//  Handle error
		return 0;
	}
}
#endif


__device__ inline void atomicFloatSub(float *address, float val) {
	int i_val = __float_as_int(val);
	int tmp0 = 0;
	int tmp1;

	while ((tmp1 = atomicCAS((int *)address, tmp0, i_val)) != tmp0)	{
		tmp0 = tmp1;
		i_val = __float_as_int(__int_as_float(tmp1) - val);
	}
}



__global__ void parallelOCBA(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < ND) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 2: find maximum
		*best = -FLT_MAX;
		float temp = -FLT_MAX;
		while (index + offset < ND){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		unsigned int i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < ND){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < ND){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rho = d_t[0] / *summation;
		d_t[1] = d_t[0];
		//	__syncthreads();


		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < ND){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];
	}
}


void simulatorEmulator(int an[], int nd, double xToAdd[], double xSquareToAdd[]) {
	//test case: i=0 is best design
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		for (int j = 0; j < an[i]; j++) {
			std::default_random_engine generator;
			std::normal_distribution<double> distribution(-900 + (100 * i), 50);
			temp = distribution(generator);
			xToAdd[i] += temp;
			xSquareToAdd[i] += (temp*temp);
		}
	}
	return;
}

void updateMeanVariance(int nd, int an[], int n[], float mean[], float var[], double xCurrent[], double xSquareCurrent[], double xToAdd[], double xSquareToAdd[]) {
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		xCurrent[i] += xToAdd[i];
		xSquareCurrent[i] += xSquareToAdd[i];
		mean[i] = xCurrent[i] / (n[i] + an[i]);
		var[i] = (xSquareCurrent[i] - (xCurrent[i] * xCurrent[i] / (n[i] + an[i]))) / (n[i] + an[i] - 1);
	}
	memset(xToAdd, 0, nd*sizeof(*xToAdd));
	memset(xToAdd, 0, nd*sizeof(*xSquareToAdd));
	return;
}



void main()
{
	srand(time(NULL));
	std::ofstream outputFile;
	outputFile.open("results.csv");
	int budget[1] = { 100 };
	//int budget[6] = { 10, 100, 1000, 10000, 100000, 1000000 };
	for (int k = 0; k < 1; k++) {
		int ADD_BUDGET = budget[k]; /*the additional simulation
									budget. It should be positive integer.*/
		outputFile << std::endl << "Budget" << "," << budget[k] << std::endl;
		for (int j = 0; j < 1; j++) {
			int i;

			//Rearrangement of Process - see excel for raw data
			float s_mean[ND] = { -891.43, -778.404, -672.865, -465.137, -458.288, -496.536, -210.198, -218.259, -176.049, -261.049, 149.1107, 248.7619, 329.7235, 260.4459, 466.6979, 656.9188 };
			float s_var[ND] = { 30369.75261, 10515.76669, 14897.38777, 12937.74895, 32876.36093, 39461.76848, 33378.26336, 60845.15336, 44810.58394, 51579.91784, 27141.06839, 38102.93198, 88345.47724, 69870.57323, 45359.79586, 28824.56514 };
			int n[ND] = { 8, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
			double xCurrentSequential[ND] = { -7131.44, -4670.42, -3364.32, -2325.69, -1833.15, -3475.75, -1471.39, -1091.29, -1056.3, -783.147, 1043.775, 497.5237, 2637.788, 1302.229, 2333.49, 3941.513 };
			double xSquareCurrentSequential[ND] = { 6569769.66, 3688052.601, 2323324.289, 1133513.436, 938739.3106, 1962607.743, 509553.0126, 481565.0714, 410012.8511, 307599.6811, 318484.4651, 161867.8688, 1488159.223, 618642.5337, 1270474.015, 2733376.813 };
			double xCurrentParallel[ND] = { -7131.44, -4670.42, -3364.32, -2325.69, -1833.15, -3475.75, -1471.39, -1091.29, -1056.3, -783.147, 1043.775, 497.5237, 2637.788, 1302.229, 2333.49, 3941.513 };
			double xSquareCurrentParallel[ND] = { 6569769.66, 3688052.601, 2323324.289, 1133513.436, 938739.3106, 1962607.743, 509553.0126, 481565.0714, 410012.8511, 307599.6811, 318484.4651, 161867.8688, 1488159.223, 618642.5337, 1270474.015, 2733376.813 };

			//test
//			float s_mean[ND] = { -887.845093, -785.764526, -681.309448, -465.138000, -470.739410, -463.777588, -210.198578, -218.257996, -176.050003, -151.566177, 173.962494, 248.761856, 329.723511, 260.445801, 466.697998, 656.918823 };
//			float s_var[ND] = { 3323.552246, 1605.360596, 5473.258301, 12936.660156, 16679.453125, 29090.291016, 33378.156250, 60845.574219, 44810.246094, 48264.648438, 27381.351563, 38102.953125, 88345.507813, 69870.617188, 45359.726563, 28824.539063};
//			int n[ND] = { 65, 34, 12, 5, 7, 10, 7, 5, 6, 5, 6, 2, 8, 5, 5, 6 };

			//Sequential OCBA
			int t = 0;
			double xToAdd[ND] = {0};
			double xSquareToAdd[ND] = {0};
			for (i = 0; i < ND; i++) {
				t += n[i];
			}
			t += ADD_BUDGET;
			int an[ND];

			//Parallel OCBA
			float *h_mean;
			float *d_mean;
			float *h_var;
			float *d_var;
			int *h_an;
			int *d_an;
			int *h_n;
			int *d_n;
			int *h_t;
			int *d_t;
			float *h_best;
			float *d_best;
			int *h_indexOfBest;
			int *d_indexOfBest;
			float *h_gamma;
			float *d_gamma;
			float *h_lamda;
			float *d_lamda;
			float *h_rho;
			float *d_rho;
			int *h_moreRun;
			int *d_moreRun;
			int *d_mutex;
			float *h_summation;
			float *d_summation;

			h_mean = (float*)malloc(ND*sizeof(float));
			h_var = (float*)malloc(ND*sizeof(float));
			h_an = (int*)malloc(ND*sizeof(int));
			h_n = (int*)malloc(ND*sizeof(int));
			h_t = (int*)malloc(ND*sizeof(int));
			h_best = (float*)malloc(sizeof(float));
			h_indexOfBest = (int*)malloc(sizeof(int));
			h_gamma = (float*)malloc(ND*sizeof(float));
			h_lamda = (float*)malloc(ND*sizeof(float));
			h_rho = (float*)malloc(ND*sizeof(float));
			h_moreRun = (int*)malloc(ND*sizeof(int));
			h_summation = (float*)malloc(sizeof(float));
			cudaMalloc((void**)&d_mean, ND*sizeof(float));
			cudaMalloc((void**)&d_var, ND*sizeof(float));
			cudaMalloc((void**)&d_an, ND*sizeof(int));
			cudaMalloc((void**)&d_n, ND*sizeof(int));
			cudaMalloc((void**)&d_t, ND*sizeof(int));
			cudaMalloc((void**)&d_best, sizeof(float));
			cudaMalloc((void**)&d_indexOfBest, sizeof(int));
			cudaMalloc((void**)&d_gamma, ND*sizeof(float));
			cudaMalloc((void**)&d_lamda, ND*sizeof(float));
			cudaMalloc((void**)&d_rho, ND*sizeof(float));
			cudaMalloc((void**)&d_moreRun, ND*sizeof(int));
			cudaMalloc((void**)&d_summation, sizeof(float));
			cudaMalloc((void**)&d_mutex, sizeof(int));
			cudaMemset(d_an, 0, ND*sizeof(int));
			cudaMemset(d_mutex, 0, sizeof(int));
			cudaMemset(d_best, 0, sizeof(float));
			cudaMemset(d_indexOfBest, 0, sizeof(int));
			cudaMemset(d_gamma, 0, ND*sizeof(float));
			cudaMemset(d_lamda, 0, ND*sizeof(float));
			cudaMemset(d_rho, 0, sizeof(float));
			cudaMemset(d_moreRun, 1, ND*sizeof(int));
			cudaMemset(d_summation, 0, sizeof(float));

			for (int i = 0; i < ND; i++){
				h_mean[i] = s_mean[i];
				h_var[i] = s_var[i];
				h_n[i] = n[i];
				h_t[i] = t;
			}

			cudaMemcpy(d_mean, h_mean, ND*sizeof(float), cudaMemcpyHostToDevice);
			cudaMemcpy(d_var, h_var, ND*sizeof(float), cudaMemcpyHostToDevice);
			cudaMemcpy(d_n, h_n, ND*sizeof(int), cudaMemcpyHostToDevice);
			cudaMemcpy(d_t, h_t, ND*sizeof(int), cudaMemcpyHostToDevice);

			dim3 gridSize = GRIDDIMENSION;
			dim3 blockSize = BLOCKDIMENSION;

			for (int iteration = 0; iteration < 2; iteration++) {
				printf("\nSequential iteration %d\n\n", iteration+1);
				ocba(s_mean, s_var, ND, n, ADD_BUDGET, an, TYPE);
				simulatorEmulator(an, ND, xToAdd, xSquareToAdd);
				updateMeanVariance(ND, an, n, s_mean, s_var, xCurrentSequential, xSquareCurrentSequential, xToAdd, xSquareToAdd);
				for (int m = 0; m < ND; m++) {
					n[m] += an[m];
					printf("Additional budget to Design %d is %ld.\n", m, an[m]);
					printf("Design %d mean is %f.\n", m, s_mean[m]);
					printf("Design %d var is %f.\n", m, s_var[m]);
					an[m] = 0;
				}
			}
			for (i = 0; i < ND; i++) {
			}
			
			for (int iteration = 0; iteration < 2; iteration++) {
				printf("\nParallel iteration %d\n\n", iteration + 1);
				parallelOCBA << <gridSize, blockSize >> >(d_mean, d_var, ND, d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
				cudaMemcpy(h_an, d_an, ND*sizeof(float), cudaMemcpyDeviceToHost);
				simulatorEmulator(h_an, ND, xToAdd, xSquareToAdd);
				updateMeanVariance(ND, h_an, h_n, h_mean, h_var, xCurrentParallel, xSquareCurrentParallel, xToAdd, xSquareToAdd);
				t += ADD_BUDGET;

				for (int m = 0; m < ND; m++) {
					h_n[m] += h_an[m];
//					printf("Additional budget to Design %d is %ld.\n", m, h_an[m]);
//					printf("Design %d mean is %f.\n", m, h_mean[m]);
//					printf("Design %d var is %f.\n", m, h_var[m]);
					h_t[m] = t;
					h_an[m] = 0;
				}
				cudaMemcpy(d_mean, h_mean, ND*sizeof(float), cudaMemcpyHostToDevice);
				cudaMemcpy(d_var, h_var, ND*sizeof(float), cudaMemcpyHostToDevice);
				cudaMemcpy(d_n, h_n, ND*sizeof(int), cudaMemcpyHostToDevice);
				cudaMemcpy(d_t, h_t, ND*sizeof(int), cudaMemcpyHostToDevice);
				cudaMemset(d_an, 0, ND*sizeof(int));
				cudaMemset(d_mutex, 0, sizeof(int));
				cudaMemset(d_best, 0.0, sizeof(float));
				cudaMemset(d_indexOfBest, 0, sizeof(int));
				cudaMemset(d_gamma, 0.0, ND*sizeof(float));
				cudaMemset(d_lamda, 0.0, ND*sizeof(float));
				cudaMemset(d_rho, 0.0, sizeof(float));
				cudaMemset(d_moreRun, 1.0, ND*sizeof(int));
				cudaMemset(d_summation, 0.0, sizeof(float));
				//to test
				cudaMemcpy(h_an, d_an, ND*sizeof(int), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_n, d_n, ND*sizeof(int), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_mean, d_mean, ND*sizeof(float), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_var, d_var, ND*sizeof(float), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_t, d_t, ND*sizeof(int), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_gamma, d_gamma, ND*sizeof(float), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_lamda, d_lamda, ND*sizeof(float), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_moreRun, d_moreRun, ND*sizeof(int), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_best, d_best, sizeof(float), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_indexOfBest, d_indexOfBest, sizeof(int), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_rho, d_rho, sizeof(float), cudaMemcpyDeviceToHost);
				cudaMemcpy(h_summation, d_summation, sizeof(float), cudaMemcpyDeviceToHost);
				printf("summation is %f.\n", h_summation);
				printf("rho is %f.\n", h_rho);
				printf("indexOfBest is %d.\n", h_indexOfBest);
				printf("best is %f.\n", h_best);
				for (int m = 0; m < ND; m++) {
					printf("n[%d] is %d.\n", m, h_n[m]);
					printf("an[%d] is %d.\n", m, h_an[m]);
					printf("mean[%d] is %f.\n", m, h_mean[m]);
					printf("var[%d] is %f.\n", m, h_var[m]);
					printf("t[%d] is %d.\n", m, h_t[m]);
					printf("gamma[%d] is %f.\n", m, h_gamma[m]);
					printf("lamda[%d] is %f.\n", m, h_lamda[m]);
					printf("moreRun[%d] is %d.\n", m, h_moreRun[m]);
				}
			}
			
			free(h_mean);
			free(h_var);
			free(h_an);
			cudaFree(d_mean);
			cudaFree(d_var);
			cudaFree(d_an);
		}
	}
	system("pause");
	outputFile.close();
}

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type) {

	/*
	This subroutine determines how many additional runs each
	design will should have for next iteration of simulation.
	s_mean[i]: sample mean of design i, i=0,1,..,ND-1
	s_var[i]: sample variance of design i, i=0,1,..,ND-1
	nd: the number of designs
	n[i]: number of simulation replications of design i,
	i=0,1,..,ND-1
	add_budget: the additional simulation budget
	an[i]: additional number of simulation replications assigned
	to design i, i=0,1,..,ND-1
	type: type of optimazition problem. type=1, MIN problem;
	type=2, MAX problem
	*/
	int i;
	int b, s;
	int t_budget, t1_budget;
	int morerun[ND], more_alloc; /* 1:Yes; 0:No */
	float t_s_mean[ND];
	float ratio[ND]; /* Ni/Ns */
	float ratio_s, temp;
	if (nd > ND) printf("\n !!!!! please stop the program and increase ND");
	if (type == 1) { /*MIN problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = s_mean[i];
	}
	else { /*MAX problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = (-1)*s_mean[i];
	}
	t_budget = add_budget;
	for (i = 0; i < nd; i++) t_budget += n[i];
	b = best(t_s_mean, nd);
	s = second_best(t_s_mean, nd, b);
	ratio[s] = 1.0;
	for (i = 0; i < nd; i++)
		if (i != s && i != b) {
			temp = (t_s_mean[b] - t_s_mean[s]) / (t_s_mean[b] - t_s_mean[i]);
			ratio[i] = temp*temp*s_var[i] / s_var[s];
		} /* calculate ratio of Ni/Ns*/
	temp = 0;
	for (i = 0; i < nd; i++) if (i != b) temp += (ratio[i] * ratio[i] / s_var[i]);
	ratio[b] = sqrt(s_var[b] * temp); /* calculate Nb */
	for (i = 0; i < nd; i++) morerun[i] = 1;
	t1_budget = t_budget;
	do{
		more_alloc = 0;
		ratio_s = 0.0;
		for (i = 0; i < nd; i++) if (morerun[i]) ratio_s += ratio[i];
		for (i = 0; i < nd; i++) if (morerun[i])
		{
			an[i] = (int)(t1_budget / ratio_s*ratio[i]);
			/* disable those design which have been run too much */
			if (an[i] < n[i])
			{
				an[i] = n[i];
				morerun[i] = 0;
				more_alloc = 1;
			}
		}
		if (more_alloc)
		{
			t1_budget = t_budget;
			for (i = 0; i < nd; i++) if (!morerun[i]) t1_budget -= an[i];
		}
	} while (more_alloc); /* end of WHILE */
	/* calculate the difference */
	t1_budget = an[0];
	for (i = 1; i < nd; i++) t1_budget += an[i];
	an[b] += (t_budget - t1_budget); /* give the difference
									 to design b */
	for (i = 0; i < nd; i++) an[i] -= n[i];
}

int best(float t_s_mean[], int nd) {
	/*This function determines the best design based on current
	simulation results */
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs */

	int i, min_index;
	min_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[min_index]) {
			min_index = i;
		}
	}
	return min_index;
}

int second_best(float t_s_mean[], int nd, int b) {
	/*This function determines the second best design based on
	current simulation results*/
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs.
	b: current best design determined by function
	best() */
	int i, second_index;
	if (b == 0) second_index = 1;
	else second_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[second_index] && i != b) {
			second_index = i;
		}
	}
	return second_index;
}


