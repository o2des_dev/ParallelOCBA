#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <cuda.h>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <ctime>
#include <fstream>

#define dimensions 256


__global__ void find_maximum_kernel(float *array, float *max, int *mutex, unsigned int n)
{
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;

	__shared__ float cache[dimensions];


	float temp = -1.0;
	while (index + offset < n){
		temp = fmaxf(temp, array[index + offset]);

		offset += stride;
	}

	cache[threadIdx.x] = temp;

	__syncthreads();


	// reduction
	unsigned int i = blockDim.x / 2;
	while (i != 0){
		if (threadIdx.x < i){
			cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
		}

		__syncthreads();
		i /= 2;
	}

	if (threadIdx.x == 0){
		while (atomicCAS(mutex, 0, 1) != 0);  //lock
		*max = fmaxf(*max, cache[0]);
		atomicExch(mutex, 0);  //unlock
	}
}

int main()
{
	std::ofstream outputFile;
	outputFile.open("results.csv");
	for (int run = 0; run < 20; run++) {
		int number[12] = { 50, 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000, 10000000 };
		for (int k = 0; k < 12; k++) {
			unsigned int N = number[k];
			float *h_array;
			float *d_array;
			float *h_max;
			float *d_max;
			int *d_mutex;


			// allocate memory
			h_array = (float*)malloc(N*sizeof(float));
			h_max = (float*)malloc(sizeof(float));
			cudaMalloc((void**)&d_array, N*sizeof(float));
			cudaMalloc((void**)&d_max, sizeof(float));
			cudaMalloc((void**)&d_mutex, sizeof(int));
			cudaMemset(d_max, 0, sizeof(float));
			cudaMemset(d_mutex, 0, sizeof(float));


			// fill host array with data
			for (unsigned int i = 0; i < N; i++){
				h_array[i] = N*float(rand()) / RAND_MAX;
			}


			// set up timing variables
			float gpu_elapsed_time;
			cudaEvent_t gpu_start, gpu_stop;
			cudaEventCreate(&gpu_start);
			cudaEventCreate(&gpu_stop);

			// copy from host to device
			cudaEventRecord(gpu_start, 0);
			for (unsigned int j = 0; j < 1000; j++){
				cudaMemcpy(d_array, h_array, N*sizeof(float), cudaMemcpyHostToDevice);

				// call kernel
				dim3 gridSize = dimensions;
				dim3 blockSize = dimensions;
				find_maximum_kernel << < gridSize, blockSize >> >(d_array, d_max, d_mutex, N);

				// copy from device to host
				cudaMemcpy(h_max, d_max, sizeof(float), cudaMemcpyDeviceToHost);
			}

			cudaEventRecord(gpu_stop, 0);
			cudaEventSynchronize(gpu_stop);
			cudaEventElapsedTime(&gpu_elapsed_time, gpu_start, gpu_stop);
			cudaEventDestroy(gpu_start);
			cudaEventDestroy(gpu_stop);


			//report results
//			std::cout << "Maximum number found on gpu was: " << *h_max << std::endl;
			std::cout << "The gpu took: " << gpu_elapsed_time << " milli-seconds" << std::endl;
			outputFile << "GPU" << "," << gpu_elapsed_time << "\n";

			// run cpu version
			clock_t cpu_start = clock();
			for (unsigned int j = 0; j < 1000; j++){
				*h_max = -1.0;
				for (unsigned int i = 0; i<N; i++){
					if (h_array[i] > *h_max){
						*h_max = h_array[i];
					}
				}
			}
			clock_t cpu_stop = clock();
			clock_t cpu_elapsed_time = 1000 * (cpu_stop - cpu_start) / CLOCKS_PER_SEC;

//			std::cout << "Maximum number found on cpu was: " << *h_max << std::endl;
			std::cout << "The cpu took: " << cpu_elapsed_time << " milli-seconds" << std::endl;
			outputFile << "CPU" << "," << cpu_elapsed_time << "\n";

			// free memory
			free(h_array);
			free(h_max);
			cudaFree(d_array);
			cudaFree(d_max);
			cudaFree(d_mutex);
		}
	}
	outputFile.close();
	system("pause");
}