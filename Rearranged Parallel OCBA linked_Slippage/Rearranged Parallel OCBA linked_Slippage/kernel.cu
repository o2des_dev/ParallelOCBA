#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <cuda.h>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include <fstream>
#include <random>
#include <time.h>

/* please put the following definitions at the head of your
program */
/*Define parameters for simulation*/
/*the number of designs. It should be larger than 2.*/
#define GRIDDIMENSION 1
#define BLOCKDIMENSION 32
#define TYPE 1
/*maximization TYPE = -1, minimization TYPE = 1*/
/*prototypes of functions and subroutines included in this
file*/

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type);
int best(float t_s_mean[], int nd);
int second_best(float t_s_mean[], int nd, int b);

#ifdef _WIN32
#include <Windows.h>
double get_wall_time(){
	LARGE_INTEGER time, freq;
	if (!QueryPerformanceFrequency(&freq)){
		//  Handle error
		return 0;
	}
	if (!QueryPerformanceCounter(&time)){
		//  Handle error
		return 0;
	}
	return (double)time.QuadPart / freq.QuadPart;
}

double get_cpu_time(){
	FILETIME a, b, c, d;
	if (GetProcessTimes(GetCurrentProcess(), &a, &b, &c, &d) != 0){
		//  Returns total user time.
		return
			(double)(d.dwLowDateTime |
			((unsigned long long)d.dwHighDateTime << 32)) * 0.0000001;
	}
	else{
		//  Handle error
		return 0;
	}
}
#endif


__device__ inline void atomicFloatSub(float *address, float val) {
	int i_val = __float_as_int(val);
	int tmp0 = 0;
	int tmp1;

	while ((tmp1 = atomicCAS((int *)address, tmp0, i_val)) != tmp0)	{
		tmp0 = tmp1;
		i_val = __float_as_int(__int_as_float(tmp1) - val);
	}
}

__global__ void parallelOCBA3(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	float temp = 0;
	unsigned int i = 0;

	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//use previous alpha of best
		if (lamda[index] == 0) {
			lamda[index] = *alphaB;
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//use previous rho
		*rho = *rhoPrevious;
		d_t[1] = d_t[0];
		//	__syncthreads();

		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];
	}
}

__global__ void parallelOCBA_1(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 2: find maximum
		*best = -FLT_MAX;
		float temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		unsigned int i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		*alphaB = *best;

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rhoPrevious = d_t[0] / *summation;
	}
}

__global__ void parallelOCBA2(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, float *alphaB, float *rhoPrevious, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	float temp = 0;
	unsigned int i = 0;

	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//use previous alpha of best
		if (lamda[index] == 0) {
			lamda[index] = *alphaB;
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//use previous rho
		*rho = *rhoPrevious;
		d_t[1] = d_t[0];
		//	__syncthreads();

		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];

		//step 2: find maximum
		*best = -FLT_MAX;
		temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rho = d_t[0] / *summation;
	}
}

__global__ void parallelOCBA1(float *mean, float *var, int nd, int *n, int *d_t, int add_budget, int *an, float *best, int *indexOfBest, float *gamma, float *lamda, float *rho, int *moreRun, int *mutex, float *summation, int type) {
	unsigned int index = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int stride = gridDim.x*blockDim.x;
	unsigned int offset = 0;
	__shared__ float cache[BLOCKDIMENSION];
	__shared__ int more_alloc;

	if (threadIdx.x == 0 && threadIdx.y == 0) {
		more_alloc = 1;
	}
	if (index < nd) {
		if (type == 1) {
			mean[index] = -1 * mean[index];
		}

		//step 2: find maximum
		*best = -FLT_MAX;
		float temp = -FLT_MAX;
		while (index + offset < nd){
			if (!isnan(mean[index + offset])) {
				temp = fmaxf(temp, mean[index + offset]);
			}

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		unsigned int i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = fmaxf(cache[threadIdx.x], cache[threadIdx.x + i]);
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*best = fmaxf(*best, cache[0]);
			//		atomicExch(mutex, 0);  //unlock
		}
		//	__syncthreads();

		if (type == 1) {
			*best = -1 * *best;
			mean[index] = -1 * mean[index];
		}

		//step 3
		if (*best == mean[index]) {
			gamma[index] = 0;
			lamda[index] = 0;
			*indexOfBest = index;
		}
		else {
			gamma[index] = pow(var[index], 1) / pow(*best - mean[index], 2);
			lamda[index] = pow(var[index], 1) / pow(*best - mean[index], 4);
		}
		//	__syncthreads();

		//step 4
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + lamda[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		if (lamda[index] == 0) {
			lamda[index] = sqrtf(var[*indexOfBest] * *summation);
			gamma[index] = lamda[index];
		}
		//	__syncthreads();

		//step 5 finding rho
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + gamma[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}
		*rho = d_t[0] / *summation;
		d_t[1] = d_t[0];
		//	__syncthreads();


		//step 6
		//allocating delta
		gamma[index] *= *rho;

		do {
			more_alloc = 0;
			if (moreRun[index]) {
				if (gamma[index] < n[index]) {
					an[index] = n[index];
					moreRun[index] = 0;
					more_alloc = 1;
					atomicSub(&d_t[0], an[index]);
					atomicFloatSub(summation, (gamma[index] / *rho));
					gamma[index] = 0;
				}
			}
			__syncthreads();
			if (more_alloc) {
				//retrieve gamma[index] from previously
				gamma[index] /= *rho;
				//finding new rho
				*rho = d_t[0] / *summation;
				__syncthreads();

				//Calculate N(l+1)
				gamma[index] *= *rho;
			}

			__syncthreads();
		} while (more_alloc);

		if (gamma[index] != 0) {
			an[index] = gamma[index];
		}

		//allocating excess simulation run to best design
		*summation = 0;
		offset = 0;
		temp = 0;
		while (index + offset < nd){
			temp = temp + an[index + offset];

			offset += stride;
		}
		cache[threadIdx.x] = temp;

		//	__syncthreads();

		// reduction
		i = blockDim.x / 2;
		while (i != 0){
			if (threadIdx.x < i){
				cache[threadIdx.x] = cache[threadIdx.x] + cache[threadIdx.x + i];
			}

			//		__syncthreads();
			i /= 2;
		}

		if (threadIdx.x == 0){
			//		while (atomicCAS(mutex, 0, 1) != 0);  //lock
			*summation = *summation + cache[0];
			//		atomicExch(mutex, 0);  //unlock
		}

		an[*indexOfBest] += d_t[1] - *summation;
		an[index] -= n[index];
	}
}

void simulatorEmulator(int an[], int nd, double xToAdd[], double xSquareToAdd[]) {
	//test case: i=0 is best design
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		for (int j = 0; j < an[i]; j++) {
			std::default_random_engine generator;
			std::normal_distribution<double> distribution(-900 + (100 * i), 50);
			temp = distribution(generator);
			xToAdd[i] += temp;
			xSquareToAdd[i] += (temp*temp);
		}
	}
	return;
}

void updateMeanVariance(int nd, int an[], int n[], float mean[], float var[], double xCurrent[], double xSquareCurrent[], double xToAdd[], double xSquareToAdd[]) {
	double temp = 0;
	for (int i = 0; i < nd; i++) {
		mean[i] = (n[i] * mean[i] + xCurrent[i]) / (n[i] + an[i]);
		xCurrent[i] += xToAdd[i];
		xSquareCurrent[i] += xSquareToAdd[i];
		var[i] = (xSquareCurrent[i] - (xCurrent[i] * xCurrent[i] / (n[i] + an[i]))) / (n[i] + an[i] - 1);
	}
	return;
}



void main()
{
	srand(time(NULL));
	std::ofstream outputFile;
	outputFile.open("results.csv");
	int budget[1] = { 1000 };
	int candidates[10] = { 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192 };
	//int budget[6] = { 10, 100, 1000, 10000, 100000, 1000000 };
	for (int k = 0; k < 1; k++) {
		int ADD_BUDGET = budget[k]; /*the additional simulation
									budget. It should be positive integer.*/
		outputFile << std::endl << "Budget" << "," << budget[k] << std::endl;
		for (int j = 0; j < 50; j++) {
			for (int l = 0; l < 10; l++) {
				outputFile << std::endl << "Candidate" << "," << candidates[l] << std::endl;
				for (int k = 0; k < 4; k++) {
					outputFile << std::endl << "Config" << "," << k << std::endl;

					int i;

					if (l == 0) {
						float s_mean[16] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						float s_var[16] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						int n[16] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[16];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 1) {
						float s_mean[32] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						float s_var[32] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						int n[32] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[32];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 2) {
						float s_mean[64] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						float s_var[64] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						int n[64] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[64];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 3) {
						float s_mean[128] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						float s_var[128] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						int n[128] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[128];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 4) {
						float s_mean[256] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						float s_var[256] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						int n[256] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[256];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 5) {
						float s_mean[512] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						float s_var[512] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						int n[512] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[512];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 6) {
						float s_mean[1024] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						float s_var[1024] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						int n[1024] = { 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6, 12, 6, 5, 5, 4, 7, 7, 5, 6, 3, 6, 2, 8, 5, 5, 6 };
						int an[1024];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 7) {
						float s_mean[2048] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						float s_var[2048] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						int n[2048] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8 };
						int an[2048];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 8) {
						float s_mean[4096] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						float s_var[4096] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						int n[4096] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8, 6, 9, 2, 4, 2, 11, 10, 2, 5, 5, 4, 3, 10, 9, 7, 8, 9, 3, 9, 11, 6, 11, 4, 8, 9, 4, 7, 9, 2, 7, 3, 8, 11, 3, 10, 6, 5, 9, 3, 7, 6, 7, 4, 7, 5, 3, 6, 11, 10, 7, 2, 4, 2, 8, 8, 6, 3, 10, 6, 8, 4, 2, 4, 11, 5, 10, 2, 9, 5, 7, 3, 2, 9, 4, 8, 4, 11, 2, 6, 5, 11, 10, 2, 7, 7, 11, 3, 8, 4, 5, 4, 8, 5, 5, 10, 3, 4, 6, 9, 2, 6, 3, 9, 7, 2, 9, 2, 6, 7, 2, 7, 4, 11, 2, 6, 7, 9, 5, 7, 5, 10, 10, 5, 7, 6, 2, 4, 7, 3, 6, 5, 3, 3, 8, 2, 7, 11, 3, 2, 5, 7, 9, 8, 8, 2, 4, 6, 3, 4, 6, 11, 5, 6, 6, 8, 11, 5, 4, 6, 2, 8, 7, 8, 2, 9, 6, 7, 4, 3, 8, 8, 5, 3, 7, 2, 4, 5, 6, 5, 6, 11, 9, 9, 4, 10, 5, 3, 2, 5, 6, 7, 3, 5, 2, 9, 10, 3, 8, 10, 5, 6, 5, 11, 2, 8, 2, 2, 6, 10, 6, 10, 9, 6, 8, 5, 4, 11, 5, 5, 11, 3, 6, 2, 11, 11, 3, 10, 11, 6, 6, 4, 7, 8, 11, 6, 3, 4, 5, 10, 5, 10, 2, 10, 5, 9, 4, 10, 2, 5, 6, 8, 11, 3, 7, 7, 7, 2, 5, 8, 9, 8, 9, 8, 9, 6, 5, 2, 7, 7, 10, 5, 7, 5, 10, 9, 11, 7, 2, 5, 3, 2, 5, 4, 7, 4, 5, 4, 3, 5, 6, 10, 10, 2, 6, 11, 11, 9, 4, 10, 5, 8, 6, 11, 6, 10, 7, 11, 2, 2, 9, 5, 8, 9, 8, 3, 4, 8, 2, 3, 4, 8, 9, 10, 3, 7, 4, 8, 7, 7, 6, 2, 8, 3, 5, 4, 2, 5, 7, 4, 8, 8, 10, 5, 7, 5, 3, 2, 11, 7, 11, 2, 6, 8, 2, 11, 6, 8, 3, 5, 8, 7, 5, 5, 10, 5, 6, 6, 11, 4, 5, 8, 9, 10, 10, 9, 11, 11, 4, 6, 7, 3, 2, 8, 4, 4, 3, 10, 2, 4, 6, 11, 11, 10, 5, 8, 6, 10, 8, 5, 6, 11, 2, 9, 6, 3, 9, 7, 4, 5, 4, 8, 2, 7, 8, 3, 10, 4, 11, 7, 11, 9, 10, 7, 2, 4, 10, 4, 3, 2, 6, 11, 7, 8, 11, 10, 4, 11, 4, 7, 2, 6, 7, 11, 9, 5, 5, 10, 2, 6, 2, 5, 9, 5, 8, 9, 11, 10, 3, 6, 9, 7, 10, 8, 10, 9, 4, 3, 6, 11, 11, 5, 10, 7, 5, 9, 3, 6, 6, 7, 5, 10, 11, 10, 11, 7, 6, 4, 4, 8, 6, 3, 4, 10, 3, 10, 9, 9, 10, 9, 9, 6, 5, 6, 8, 5, 9, 4, 9, 6, 4, 3, 9, 4, 3, 10, 4, 6, 2, 3, 8, 9, 8, 5, 9, 8, 11, 7, 2, 2, 9, 3, 11, 6, 2, 10, 3, 3, 9, 6, 3, 7, 10, 11, 2, 9, 2, 7, 7, 7, 2, 7, 11, 4, 11, 5, 7, 5, 6, 9, 5, 3, 8, 3, 2, 4, 4, 8, 10, 2, 6, 9, 5, 5, 6, 5, 8, 3, 11, 4, 11, 3, 3, 5, 11, 4, 2, 9, 2, 11, 4, 2, 11, 6, 6, 8, 2, 8, 4, 7, 9, 2, 6, 3, 5, 2, 9, 5, 8, 8, 2, 8, 6, 9, 7, 3, 11, 10, 10, 6, 11, 9, 3, 10, 9, 7, 9, 3, 6, 5, 3, 2, 5, 6, 7, 5, 4, 4, 2, 9, 9, 4, 4, 9, 8, 9, 11, 5, 2, 4, 11, 7, 11, 10, 7, 6, 5, 3, 11, 3, 6, 2, 3, 6, 5, 6, 6, 11, 6, 4, 9, 5, 6, 2, 10, 10, 9, 10, 4, 3, 11, 9, 2, 7, 11, 8, 4, 11, 10, 7, 9, 8, 8, 6, 2, 10, 6, 3, 9, 5, 7, 10, 10, 5, 9, 3, 7, 5, 9, 5, 11, 7, 11, 6, 7, 4, 3, 5, 4, 11, 3, 8, 4, 8, 10, 7, 11, 3, 6, 11, 10, 6, 2, 5, 8, 11, 4, 8, 3, 4, 3, 10, 10, 4, 11, 10, 11, 7, 8, 4, 5, 4, 11, 2, 10, 9, 11, 11, 6, 10, 8, 2, 3, 5, 9, 6, 2, 4, 5, 9, 7, 6, 3, 11, 4, 10, 9, 2, 7, 9, 9, 8, 4, 11, 3, 5, 9, 8, 2, 8, 5, 7, 11, 10, 10, 8, 9, 9, 2, 11, 3, 8, 3, 6, 9, 5, 9, 8, 7, 6, 8, 6, 4, 3, 6, 5, 10, 5, 2, 4, 4, 5, 2, 5, 3, 9, 3, 11, 2, 10, 5, 6, 4, 8, 8, 9, 4, 7, 5, 8, 5, 6, 4, 6, 8, 10, 9, 5, 5, 10, 7, 2, 10, 3, 5, 5, 5, 11, 3, 3, 11, 6, 11, 4, 2, 11, 6, 7, 3, 6, 7, 10, 8, 6, 5, 11, 10, 6, 5, 7, 11, 7, 5, 6, 7, 10, 6, 3, 8, 2, 11, 9, 10, 5, 2, 4, 5, 10, 10, 10, 2, 6, 5, 10, 11, 11, 3, 5, 10, 3, 9, 7, 6, 4, 3, 6, 11, 8, 9, 5, 7, 5, 5, 2, 6, 2, 8, 7, 11, 9, 3, 3, 7, 8, 9, 3, 2, 4, 4, 4, 10, 9, 11, 8, 4, 10, 8, 3, 10, 4, 6, 5, 5, 6, 2, 2, 6, 7, 10, 7, 3, 11, 7, 5, 9, 6, 8, 10, 6, 11, 7, 9, 4, 7, 6, 8, 6, 4, 7, 10, 4, 2, 9, 4, 2, 10, 11, 10, 8, 6, 4, 10, 10, 11, 11, 7, 8, 2, 11, 2, 7, 8, 8, 3, 5, 4, 2, 8, 11, 4, 4, 5, 5, 9, 6, 11, 11, 3, 7, 9, 4, 10, 11, 7, 8, 2, 6, 10, 6, 11, 5, 7, 3, 9, 2, 9, 10, 5, 10, 3, 2, 11, 3, 8, 5, 9, 10, 2, 7, 9, 6, 7, 5, 4, 4, 9, 10, 9, 10, 3, 6, 4, 7, 10, 10, 11, 6, 8, 8, 8, 7, 6, 11, 4, 9, 2, 9, 11, 6, 2, 8, 5, 2, 3, 5, 10, 2, 3, 4, 7, 11, 3, 5, 7, 11, 5, 10, 10, 2, 9, 7, 6, 3, 10, 5, 3, 2, 11, 4, 9, 5, 4, 8, 8, 8, 10, 10, 6, 6, 6, 8, 11, 7, 3, 8, 4, 5, 2, 11, 2, 3, 7, 8, 11, 2, 3, 8, 8, 6, 5, 6, 2, 9, 7, 11, 4, 7, 2, 6, 2, 5, 9, 3, 7, 3, 7, 6, 10, 8, 2, 10, 5, 2, 7, 5, 5, 4, 4, 11, 11, 9, 4, 5, 6, 9, 10, 4, 9, 10, 8, 2, 6, 7, 5, 7, 11, 3, 4, 6, 8, 6, 11, 6, 5, 8, 11, 11, 10, 10, 7, 10, 11, 2, 2, 2, 10, 6, 8, 3, 3, 8, 9, 7, 2, 8, 3, 7, 8, 3, 2, 2, 7, 9, 4, 10, 10, 4, 9, 3, 10, 7, 5, 11, 11, 4, 5, 9, 10, 5, 5, 9, 11, 10, 8, 4, 6, 5, 9, 4, 5, 5, 5, 6, 8, 10, 3, 5, 8, 7, 7, 7, 6, 5, 10, 2, 2, 11, 5, 11, 7, 8, 3, 11, 3, 5, 2, 4, 2, 2, 8, 8, 10, 2, 10, 3, 10, 6, 9, 3, 7, 5, 3, 2, 4, 9, 8, 11, 6, 2, 11, 2, 6, 9, 11, 2, 5, 11, 9, 3, 10, 6, 3, 2, 8, 4, 5, 10, 8, 8, 9, 3, 5, 7, 8, 9, 6, 9, 8, 11, 5, 10, 11, 10, 8, 7, 5, 6, 8, 4, 3, 8, 2, 6, 3, 10, 7, 2, 11, 3, 9, 3, 11, 9, 11, 8, 4, 9, 7, 6, 3, 2, 2, 2, 2, 3, 2, 3, 11, 8, 9, 11, 9, 5, 7, 8, 7, 8, 6, 5, 3, 3, 9, 10, 6, 11, 4, 4, 9, 7, 10, 7, 4, 11, 9, 11, 4, 2, 7, 5, 11, 5, 8, 6, 2, 4, 2, 11, 11, 3, 2, 3, 8, 10, 7, 2, 8, 9, 4, 9, 2, 7, 3, 4, 3, 10, 10, 10, 2, 8, 3, 3, 6, 6, 5, 4, 6, 6, 4, 7, 8, 4, 4, 11, 7, 8, 8, 7, 6, 5, 9, 6, 3, 6, 6, 11, 6, 11, 8, 2, 11, 4, 11, 3, 5, 10, 8, 7, 2, 3, 11, 4, 10, 11, 9, 8, 11, 9, 2, 7, 6, 11, 11, 9, 9, 2, 10, 7, 9, 8, 7, 7, 11, 6, 9, 2, 2, 10, 9, 7, 6, 10, 11, 3, 6, 8, 8, 8, 6, 10, 5, 7, 4, 5, 8, 9, 7, 5, 5, 6, 4, 4, 2, 5, 3, 10, 11, 3, 3, 7, 8, 6, 9, 11, 11, 8, 4, 6, 8, 6, 2, 8, 7, 8, 3, 9, 3, 7, 7, 6, 2, 5, 4, 9, 6, 6, 4, 3, 4, 4, 9, 9, 6, 4, 11, 5, 2, 9, 7, 2, 11, 6, 8, 2, 5, 4, 3, 6, 5, 2, 2, 9, 3, 4, 7, 9, 4, 8, 9, 6, 4, 6, 10, 8, 3, 10, 8, 9, 10, 10, 11, 11, 6, 11, 10, 11, 10, 2, 2, 11, 7, 3, 9, 7, 8, 9, 10, 7, 2, 8, 11, 10, 8, 3, 2, 7, 11, 3, 6, 5, 7, 7, 3, 4, 2, 2, 4, 9, 9, 5, 8, 9, 9, 9, 5, 7, 11, 10, 7, 10, 4, 3, 11, 7, 2, 4, 2, 11, 2, 10, 6, 3, 11, 11, 4, 7, 11, 2, 9, 7, 4, 8, 4, 9, 3, 5, 4, 7, 2, 10, 10, 6, 7, 8, 7, 11, 5, 4, 11, 4, 7, 7, 10, 8, 2, 10, 9, 5, 4, 8, 9, 3, 3, 2, 2, 10, 2, 9, 8, 5, 6, 5, 10, 6, 6, 8, 6, 8, 3, 8, 2, 6, 9, 10, 6, 4, 10, 10, 3, 8, 11, 2, 8, 11, 9, 2, 2, 2, 8, 5, 6, 2, 3, 9, 4, 8, 8, 6, 2, 6, 5, 9, 2, 4, 2, 4, 11, 7, 7, 8, 6, 3, 4, 10, 5, 10, 8, 5, 11, 8, 4, 7, 8, 11, 9, 6, 9, 9, 10, 5, 7, 3, 7, 2, 8, 11, 6, 2, 7, 6, 11, 9, 9, 4, 4, 10, 11, 10, 6, 4, 4, 3, 3, 9, 6, 5, 4, 6, 7, 9, 7, 7, 11, 11, 11, 5, 5, 7, 7, 8, 8, 11, 7, 6, 10, 11, 5, 8, 9, 2, 10, 5, 6, 10, 8, 2, 7, 9, 6, 3, 10, 9, 8, 2, 2, 10, 3, 4, 3, 7, 7, 2, 2, 2, 2, 4, 3, 10, 8, 10, 6, 10, 4, 9, 6, 9, 3, 9, 8, 2, 5, 5, 8, 11, 10, 11, 3, 6, 5, 4, 4, 5, 10, 6, 7, 3, 11, 3, 11, 5, 5, 9, 11, 6, 9, 10, 4, 6, 2, 9, 10, 11, 9, 2, 7, 8, 11, 2, 2, 2, 2, 8, 5, 11, 10, 9, 8, 7, 4, 8, 6, 6, 11, 10, 6, 8, 7, 8, 6, 4, 9, 3, 9, 6, 9, 3, 2, 9, 9, 3, 5, 5, 9, 11, 11, 10, 3, 11, 9, 11, 6, 6, 6, 9, 8, 5, 7, 2, 10, 4, 6, 11, 10, 8, 2, 9, 4, 5, 6, 7, 3, 2, 5, 5, 11, 4, 6, 5, 2, 10, 9, 11, 6, 8, 8, 7, 10, 6, 11, 9, 3, 6, 7, 5, 9, 11, 6, 4, 6, 3, 9, 4, 11, 3, 5, 10, 3, 9, 10, 4, 4, 2, 9, 8, 11, 2, 6, 10, 10, 2, 9, 8, 5, 4, 10, 6, 11, 11, 7, 5, 3, 6, 9, 8, 2, 4, 3, 3, 3, 6, 4, 5, 2, 5, 3, 11, 10, 6, 2, 3 };
						int an[4096];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
					else if (l == 9) {
						static float s_mean[8192] = { 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
						static float s_var[8192] = { 1, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421, 1.41421 };
						static int n[8192] = { 2, 10, 9, 7, 10, 7, 6, 10, 5, 7, 3, 10, 5, 2, 11, 7, 11, 11, 4, 10, 5, 11, 7, 4, 6, 9, 3, 2, 11, 4, 4, 5, 10, 3, 7, 7, 10, 4, 8, 7, 10, 4, 2, 5, 3, 8, 7, 7, 6, 10, 5, 9, 8, 7, 6, 6, 3, 5, 8, 6, 2, 9, 7, 2, 5, 7, 10, 8, 2, 8, 4, 2, 10, 11, 2, 6, 10, 2, 8, 8, 8, 8, 3, 7, 3, 7, 4, 7, 9, 4, 6, 5, 2, 5, 7, 10, 11, 11, 7, 8, 7, 7, 5, 2, 11, 10, 6, 5, 6, 8, 2, 5, 7, 5, 5, 9, 10, 5, 10, 10, 11, 9, 2, 5, 11, 8, 8, 11, 4, 9, 5, 3, 4, 2, 10, 4, 9, 3, 2, 6, 7, 5, 9, 9, 2, 3, 4, 7, 4, 8, 2, 2, 5, 9, 6, 5, 7, 8, 11, 4, 10, 7, 10, 5, 4, 8, 6, 8, 3, 3, 9, 11, 11, 4, 2, 6, 6, 9, 2, 11, 4, 9, 7, 9, 10, 7, 8, 6, 3, 10, 5, 3, 4, 8, 3, 5, 5, 5, 7, 3, 9, 8, 8, 6, 5, 3, 5, 10, 9, 4, 8, 2, 6, 5, 8, 3, 2, 11, 11, 7, 6, 3, 6, 2, 3, 7, 7, 7, 11, 6, 7, 3, 6, 8, 6, 9, 9, 2, 9, 2, 4, 4, 9, 6, 2, 4, 6, 11, 9, 10, 3, 4, 11, 8, 10, 8, 5, 5, 7, 4, 9, 8, 6, 2, 11, 11, 10, 9, 8, 5, 11, 8, 11, 7, 8, 7, 6, 3, 11, 8, 11, 3, 11, 9, 5, 3, 9, 7, 5, 4, 4, 3, 4, 2, 11, 5, 3, 4, 10, 6, 5, 2, 9, 2, 4, 8, 11, 3, 10, 9, 10, 6, 5, 5, 9, 10, 3, 9, 4, 10, 2, 3, 8, 2, 4, 5, 9, 9, 11, 4, 11, 2, 5, 5, 6, 4, 4, 2, 6, 3, 11, 6, 7, 2, 10, 3, 11, 4, 4, 10, 11, 7, 4, 2, 3, 11, 3, 6, 3, 4, 3, 6, 8, 11, 2, 11, 4, 10, 5, 3, 3, 5, 11, 8, 6, 2, 6, 2, 5, 10, 2, 6, 8, 10, 7, 10, 7, 9, 2, 8, 6, 8, 9, 4, 11, 7, 11, 11, 4, 4, 10, 4, 6, 3, 7, 8, 7, 5, 11, 5, 2, 10, 7, 8, 7, 3, 11, 4, 10, 11, 5, 9, 6, 4, 8, 2, 2, 8, 5, 9, 3, 5, 2, 7, 11, 9, 2, 10, 7, 10, 7, 6, 4, 6, 4, 5, 11, 3, 4, 10, 4, 10, 3, 11, 10, 5, 2, 3, 5, 11, 2, 6, 6, 9, 5, 3, 8, 8, 3, 3, 4, 11, 2, 7, 9, 11, 4, 6, 8, 4, 5, 5, 6, 11, 10, 9, 5, 7, 9, 8, 8, 6, 6, 3, 7, 9, 4, 2, 8, 7, 4, 9, 11, 7, 8, 8, 5, 11, 3, 8, 7, 8, 4, 3, 3, 2, 9, 4, 2, 5, 10, 8, 11, 6, 5, 9, 6, 5, 2, 4, 7, 4, 3, 6, 10, 8, 5, 11, 3, 4, 2, 10, 11, 11, 10, 8, 7, 2, 5, 10, 2, 9, 7, 3, 8, 11, 2, 4, 3, 2, 4, 6, 11, 9, 5, 5, 11, 6, 4, 4, 9, 9, 6, 4, 4, 10, 2, 2, 11, 10, 9, 4, 4, 5, 10, 11, 4, 8, 6, 3, 4, 3, 6, 7, 3, 11, 8, 9, 10, 11, 6, 6, 10, 4, 9, 8, 6, 3, 2, 7, 4, 9, 8, 9, 4, 8, 2, 11, 9, 3, 3, 7, 3, 7, 3, 2, 3, 5, 6, 10, 4, 11, 4, 5, 6, 3, 6, 4, 5, 9, 9, 5, 4, 3, 5, 10, 6, 8, 4, 9, 3, 11, 11, 8, 3, 10, 10, 7, 10, 4, 11, 9, 5, 10, 10, 6, 10, 9, 11, 6, 8, 11, 5, 5, 4, 5, 7, 9, 3, 2, 7, 11, 2, 4, 6, 3, 4, 4, 4, 6, 2, 4, 5, 5, 3, 6, 4, 5, 7, 6, 11, 4, 9, 9, 10, 7, 9, 4, 7, 8, 2, 3, 6, 3, 3, 2, 8, 5, 5, 6, 11, 4, 9, 2, 11, 8, 8, 8, 11, 7, 8, 11, 8, 3, 9, 2, 6, 8, 8, 7, 2, 8, 4, 10, 11, 9, 6, 6, 3, 9, 10, 4, 7, 2, 8, 8, 4, 3, 6, 7, 3, 6, 3, 2, 2, 9, 9, 2, 5, 6, 11, 3, 7, 9, 11, 11, 8, 11, 3, 11, 11, 4, 3, 8, 9, 3, 6, 2, 4, 11, 11, 5, 4, 4, 6, 9, 6, 4, 5, 10, 10, 6, 2, 4, 9, 8, 9, 8, 8, 2, 11, 2, 9, 5, 5, 8, 5, 7, 11, 6, 9, 4, 5, 6, 7, 4, 4, 3, 8, 4, 3, 11, 11, 6, 7, 4, 3, 6, 7, 11, 6, 4, 7, 6, 9, 9, 7, 7, 11, 4, 8, 3, 9, 10, 9, 5, 5, 5, 8, 7, 3, 9, 7, 10, 2, 7, 6, 5, 8, 8, 5, 7, 5, 7, 4, 2, 7, 7, 4, 8, 7, 3, 2, 10, 2, 11, 9, 10, 3, 8, 10, 5, 11, 2, 6, 7, 7, 5, 2, 4, 9, 2, 8, 7, 10, 10, 4, 10, 5, 7, 2, 8, 10, 2, 2, 11, 10, 3, 2, 7, 6, 2, 6, 2, 6, 2, 10, 7, 7, 2, 9, 10, 6, 10, 3, 11, 7, 11, 7, 2, 11, 7, 2, 3, 9, 9, 10, 7, 11, 4, 10, 9, 9, 5, 4, 7, 9, 10, 4, 4, 8, 8, 4, 10, 3, 10, 10, 6, 8, 9, 10, 10, 9, 2, 7, 4, 2, 8, 8, 6, 7, 10, 5, 6, 6, 4, 5, 9, 5, 3, 5, 7, 3, 6, 10, 5, 2, 3, 9, 3, 11, 10, 6, 9, 5, 10, 3, 8, 9, 5, 5, 10, 7, 7, 6, 2, 2, 6, 3, 7, 4, 11, 3, 2, 5, 4, 8, 8, 9, 2, 10, 11, 10, 10, 2, 6, 4, 3, 7, 10, 8, 5, 11, 2, 5, 8, 9, 10, 5, 4, 5, 5, 4, 7, 8, 3, 11, 11, 5, 3, 3, 8, 6, 6, 2, 3, 5, 9, 4, 7, 3, 10, 11, 11, 7, 7, 8, 9, 10, 9, 9, 5, 11, 5, 5, 11, 9, 7, 5, 5, 11, 4, 3, 3, 3, 9, 2, 4, 6, 7, 9, 2, 2, 6, 6, 5, 9, 11, 2, 9, 9, 4, 11, 10, 3, 3, 3, 4, 11, 11, 11, 4, 7, 9, 10, 4, 10, 11, 8, 4, 9, 2, 9, 4, 4, 3, 4, 8, 6, 11, 5, 3, 8, 8, 9, 4, 5, 6, 2, 3, 7, 7, 6, 2, 7, 6, 11, 3, 7, 5, 5, 7, 11, 5, 8, 9, 6, 10, 6, 2, 9, 7, 3, 9, 5, 8, 8, 6, 8, 2, 9, 8, 8, 4, 5, 5, 10, 5, 8, 4, 2, 4, 2, 3, 10, 11, 6, 2, 3, 6, 3, 5, 2, 9, 5, 10, 5, 10, 6, 8, 10, 2, 5, 4, 4, 8, 4, 7, 6, 8, 9, 7, 8, 6, 6, 3, 10, 6, 10, 7, 9, 3, 6, 6, 11, 10, 8, 11, 3, 5, 11, 9, 6, 6, 10, 9, 11, 10, 10, 11, 7, 7, 7, 5, 8, 6, 3, 4, 6, 7, 9, 7, 3, 2, 7, 3, 5, 4, 11, 3, 8, 2, 11, 7, 2, 8, 6, 6, 4, 10, 10, 9, 10, 9, 11, 6, 3, 5, 4, 7, 5, 3, 8, 5, 5, 10, 7, 2, 8, 6, 11, 10, 2, 5, 4, 2, 8, 2, 2, 11, 6, 2, 11, 3, 3, 8, 4, 11, 11, 2, 5, 7, 7, 4, 11, 10, 11, 10, 9, 10, 4, 3, 9, 10, 10, 5, 8, 9, 5, 5, 2, 6, 7, 7, 10, 2, 9, 2, 5, 5, 8, 10, 7, 11, 7, 2, 3, 5, 5, 3, 6, 11, 2, 5, 2, 7, 7, 11, 2, 4, 9, 4, 5, 5, 4, 5, 4, 7, 11, 2, 9, 8, 7, 2, 7, 5, 11, 11, 5, 7, 9, 4, 10, 9, 11, 10, 11, 7, 8, 7, 7, 2, 7, 9, 6, 7, 3, 10, 3, 6, 6, 8, 5, 4, 10, 4, 4, 3, 8, 6, 6, 7, 3, 7, 4, 7, 7, 11, 4, 11, 10, 4, 10, 8, 10, 10, 5, 5, 10, 3, 3, 6, 5, 3, 8, 11, 7, 2, 10, 9, 10, 4, 8, 9, 4, 2, 2, 6, 4, 2, 4, 3, 5, 9, 3, 4, 11, 10, 6, 6, 4, 3, 5, 3, 10, 10, 9, 10, 4, 5, 4, 5, 2, 2, 8, 3, 7, 4, 9, 10, 3, 6, 9, 2, 6, 7, 4, 2, 7, 4, 10, 11, 5, 3, 6, 4, 7, 5, 2, 7, 7, 2, 5, 2, 10, 11, 10, 2, 7, 7, 6, 6, 4, 11, 11, 11, 8, 8, 5, 3, 6, 5, 4, 4, 11, 6, 6, 2, 4, 6, 10, 11, 9, 6, 5, 2, 6, 2, 2, 2, 4, 11, 11, 5, 4, 4, 9, 6, 4, 10, 6, 9, 10, 3, 7, 7, 6, 8, 5, 3, 3, 11, 7, 8, 10, 8, 5, 5, 4, 8, 8, 6, 7, 10, 6, 5, 10, 6, 7, 10, 2, 6, 9, 7, 3, 5, 7, 10, 3, 4, 2, 8, 8, 10, 6, 4, 9, 5, 5, 8, 7, 3, 7, 9, 3, 7, 7, 3, 2, 6, 5, 6, 10, 7, 4, 7, 4, 11, 5, 3, 2, 4, 8, 10, 5, 7, 2, 2, 2, 11, 8, 10, 7, 3, 3, 9, 5, 2, 10, 5, 4, 8, 6, 5, 3, 9, 4, 5, 2, 4, 9, 6, 3, 7, 4, 9, 4, 3, 10, 8, 2, 5, 4, 9, 4, 6, 7, 10, 7, 10, 10, 8, 11, 7, 11, 8, 4, 6, 10, 9, 10, 7, 10, 4, 11, 5, 9, 5, 8, 4, 5, 3, 3, 5, 7, 9, 2, 11, 2, 10, 9, 3, 3, 5, 3, 7, 4, 4, 4, 9, 5, 4, 5, 4, 7, 8, 10, 9, 6, 8, 2, 6, 11, 10, 4, 4, 9, 6, 10, 9, 10, 10, 8, 9, 6, 10, 11, 6, 6, 4, 2, 4, 11, 4, 5, 3, 2, 10, 2, 7, 5, 6, 4, 9, 5, 8, 2, 2, 2, 5, 6, 3, 6, 5, 10, 6, 4, 6, 9, 3, 3, 3, 3, 3, 11, 4, 11, 11, 3, 8, 7, 11, 9, 5, 6, 11, 11, 9, 2, 5, 10, 8, 4, 5, 10, 7, 9, 3, 6, 5, 6, 9, 5, 10, 3, 7, 9, 5, 7, 8, 5, 5, 2, 3, 9, 3, 10, 3, 8, 10, 9, 6, 6, 8, 10, 11, 11, 7, 7, 10, 8, 7, 11, 6, 10, 9, 3, 3, 9, 10, 2, 5, 6, 5, 3, 8, 9, 4, 3, 9, 10, 11, 2, 5, 4, 5, 2, 9, 8, 4, 11, 11, 7, 2, 7, 6, 8, 7, 3, 2, 3, 10, 4, 5, 10, 9, 2, 10, 10, 2, 5, 3, 6, 7, 10, 9, 7, 11, 2, 7, 8, 10, 9, 8, 2, 9, 10, 7, 7, 7, 11, 4, 6, 4, 6, 3, 7, 10, 11, 9, 5, 2, 8, 11, 10, 6, 11, 9, 11, 5, 3, 5, 5, 10, 7, 9, 11, 6, 9, 3, 8, 3, 2, 6, 3, 2, 4, 3, 2, 7, 8, 11, 9, 5, 7, 10, 3, 11, 8, 5, 9, 5, 3, 4, 5, 5, 6, 10, 2, 11, 4, 4, 11, 6, 10, 10, 9, 11, 7, 4, 4, 11, 9, 9, 4, 4, 10, 9, 10, 9, 4, 4, 8, 9, 10, 9, 11, 9, 8, 10, 4, 8, 11, 10, 6, 9, 6, 5, 8, 8, 5, 5, 11, 8, 9, 3, 5, 9, 7, 10, 11, 11, 9, 6, 11, 4, 9, 3, 2, 9, 9, 8, 7, 4, 6, 11, 4, 10, 8, 7, 6, 11, 3, 6, 11, 9, 11, 8, 6, 9, 2, 4, 2, 11, 10, 2, 5, 5, 4, 3, 10, 9, 7, 8, 9, 3, 9, 11, 6, 11, 4, 8, 9, 4, 7, 9, 2, 7, 3, 8, 11, 3, 10, 6, 5, 9, 3, 7, 6, 7, 4, 7, 5, 3, 6, 11, 10, 7, 2, 4, 2, 8, 8, 6, 3, 10, 6, 8, 4, 2, 4, 11, 5, 10, 2, 9, 5, 7, 3, 2, 9, 4, 8, 4, 11, 2, 6, 5, 11, 10, 2, 7, 7, 11, 3, 8, 4, 5, 4, 8, 5, 5, 10, 3, 4, 6, 9, 2, 6, 3, 9, 7, 2, 9, 2, 6, 7, 2, 7, 4, 11, 2, 6, 7, 9, 5, 7, 5, 10, 10, 5, 7, 6, 2, 4, 7, 3, 6, 5, 3, 3, 8, 2, 7, 11, 3, 2, 5, 7, 9, 8, 8, 2, 4, 6, 3, 4, 6, 11, 5, 6, 6, 8, 11, 5, 4, 6, 2, 8, 7, 8, 2, 9, 6, 7, 4, 3, 8, 8, 5, 3, 7, 2, 4, 5, 6, 5, 6, 11, 9, 9, 4, 10, 5, 3, 2, 5, 6, 7, 3, 5, 2, 9, 10, 3, 8, 10, 5, 6, 5, 11, 2, 8, 2, 2, 6, 10, 6, 10, 9, 6, 8, 5, 4, 11, 5, 5, 11, 3, 6, 2, 11, 11, 3, 10, 11, 6, 6, 4, 7, 8, 11, 6, 3, 4, 5, 10, 5, 10, 2, 10, 5, 9, 4, 10, 2, 5, 6, 8, 11, 3, 7, 7, 7, 2, 5, 8, 9, 8, 9, 8, 9, 6, 5, 2, 7, 7, 10, 5, 7, 5, 10, 9, 11, 7, 2, 5, 3, 2, 5, 4, 7, 4, 5, 4, 3, 5, 6, 10, 10, 2, 6, 11, 11, 9, 4, 10, 5, 8, 6, 11, 6, 10, 7, 11, 2, 2, 9, 5, 8, 9, 8, 3, 4, 8, 2, 3, 4, 8, 9, 10, 3, 7, 4, 8, 7, 7, 6, 2, 8, 3, 5, 4, 2, 5, 7, 4, 8, 8, 10, 5, 7, 5, 3, 2, 11, 7, 11, 2, 6, 8, 2, 11, 6, 8, 3, 5, 8, 7, 5, 5, 10, 5, 6, 6, 11, 4, 5, 8, 9, 10, 10, 9, 11, 11, 4, 6, 7, 3, 2, 8, 4, 4, 3, 10, 2, 4, 6, 11, 11, 10, 5, 8, 6, 10, 8, 5, 6, 11, 2, 9, 6, 3, 9, 7, 4, 5, 4, 8, 2, 7, 8, 3, 10, 4, 11, 7, 11, 9, 10, 7, 2, 4, 10, 4, 3, 2, 6, 11, 7, 8, 11, 10, 4, 11, 4, 7, 2, 6, 7, 11, 9, 5, 5, 10, 2, 6, 2, 5, 9, 5, 8, 9, 11, 10, 3, 6, 9, 7, 10, 8, 10, 9, 4, 3, 6, 11, 11, 5, 10, 7, 5, 9, 3, 6, 6, 7, 5, 10, 11, 10, 11, 7, 6, 4, 4, 8, 6, 3, 4, 10, 3, 10, 9, 9, 10, 9, 9, 6, 5, 6, 8, 5, 9, 4, 9, 6, 4, 3, 9, 4, 3, 10, 4, 6, 2, 3, 8, 9, 8, 5, 9, 8, 11, 7, 2, 2, 9, 3, 11, 6, 2, 10, 3, 3, 9, 6, 3, 7, 10, 11, 2, 9, 2, 7, 7, 7, 2, 7, 11, 4, 11, 5, 7, 5, 6, 9, 5, 3, 8, 3, 2, 4, 4, 8, 10, 2, 6, 9, 5, 5, 6, 5, 8, 3, 11, 4, 11, 3, 3, 5, 11, 4, 2, 9, 2, 11, 4, 2, 11, 6, 6, 8, 2, 8, 4, 7, 9, 2, 6, 3, 5, 2, 9, 5, 8, 8, 2, 8, 6, 9, 7, 3, 11, 10, 10, 6, 11, 9, 3, 10, 9, 7, 9, 3, 6, 5, 3, 2, 5, 6, 7, 5, 4, 4, 2, 9, 9, 4, 4, 9, 8, 9, 11, 5, 2, 4, 11, 7, 11, 10, 7, 6, 5, 3, 11, 3, 6, 2, 3, 6, 5, 6, 6, 11, 6, 4, 9, 5, 6, 2, 10, 10, 9, 10, 4, 3, 11, 9, 2, 7, 11, 8, 4, 11, 10, 7, 9, 8, 8, 6, 2, 10, 6, 3, 9, 5, 7, 10, 10, 5, 9, 3, 7, 5, 9, 5, 11, 7, 11, 6, 7, 4, 3, 5, 4, 11, 3, 8, 4, 8, 10, 7, 11, 3, 6, 11, 10, 6, 2, 5, 8, 11, 4, 8, 3, 4, 3, 10, 10, 4, 11, 10, 11, 7, 8, 4, 5, 4, 11, 2, 10, 9, 11, 11, 6, 10, 8, 2, 3, 5, 9, 6, 2, 4, 5, 9, 7, 6, 3, 11, 4, 10, 9, 2, 7, 9, 9, 8, 4, 11, 3, 5, 9, 8, 2, 8, 5, 7, 11, 10, 10, 8, 9, 9, 2, 11, 3, 8, 3, 6, 9, 5, 9, 8, 7, 6, 8, 6, 4, 3, 6, 5, 10, 5, 2, 4, 4, 5, 2, 5, 3, 9, 3, 11, 2, 10, 5, 6, 4, 8, 8, 9, 4, 7, 5, 8, 5, 6, 4, 6, 8, 10, 9, 5, 5, 10, 7, 2, 10, 3, 5, 5, 5, 11, 3, 3, 11, 6, 11, 4, 2, 11, 6, 7, 3, 6, 7, 10, 8, 6, 5, 11, 10, 6, 5, 7, 11, 7, 5, 6, 7, 10, 6, 3, 8, 2, 11, 9, 10, 5, 2, 4, 5, 10, 10, 10, 2, 6, 5, 10, 11, 11, 3, 5, 10, 3, 9, 7, 6, 4, 3, 6, 11, 8, 9, 5, 7, 5, 5, 2, 6, 2, 8, 7, 11, 9, 3, 3, 7, 8, 9, 3, 2, 4, 4, 4, 10, 9, 11, 8, 4, 10, 8, 3, 10, 4, 6, 5, 5, 6, 2, 2, 6, 7, 10, 7, 3, 11, 7, 5, 9, 6, 8, 10, 6, 11, 7, 9, 4, 7, 6, 8, 6, 4, 7, 10, 4, 2, 9, 4, 2, 10, 11, 10, 8, 6, 4, 10, 10, 11, 11, 7, 8, 2, 11, 2, 7, 8, 8, 3, 5, 4, 2, 8, 11, 4, 4, 5, 5, 9, 6, 11, 11, 3, 7, 9, 4, 10, 11, 7, 8, 2, 6, 10, 6, 11, 5, 7, 3, 9, 2, 9, 10, 5, 10, 3, 2, 11, 3, 8, 5, 9, 10, 2, 7, 9, 6, 7, 5, 4, 4, 9, 10, 9, 10, 3, 6, 4, 7, 10, 10, 11, 6, 8, 8, 8, 7, 6, 11, 4, 9, 2, 9, 11, 6, 2, 8, 5, 2, 3, 5, 10, 2, 3, 4, 7, 11, 3, 5, 7, 11, 5, 10, 10, 2, 9, 7, 6, 3, 10, 5, 3, 2, 11, 4, 9, 5, 4, 8, 8, 8, 10, 10, 6, 6, 6, 8, 11, 7, 3, 8, 4, 5, 2, 11, 2, 3, 7, 8, 11, 2, 3, 8, 8, 6, 5, 6, 2, 9, 7, 11, 4, 7, 2, 6, 2, 5, 9, 3, 7, 3, 7, 6, 10, 8, 2, 10, 5, 2, 7, 5, 5, 4, 4, 11, 11, 9, 4, 5, 6, 9, 10, 4, 9, 10, 8, 2, 6, 7, 5, 7, 11, 3, 4, 6, 8, 6, 11, 6, 5, 8, 11, 11, 10, 10, 7, 10, 11, 2, 2, 2, 10, 6, 8, 3, 3, 8, 9, 7, 2, 8, 3, 7, 8, 3, 2, 2, 7, 9, 4, 10, 10, 4, 9, 3, 10, 7, 5, 11, 11, 4, 5, 9, 10, 5, 5, 9, 11, 10, 8, 4, 6, 5, 9, 4, 5, 5, 5, 6, 8, 10, 3, 5, 8, 7, 7, 7, 6, 5, 10, 2, 2, 11, 5, 11, 7, 8, 3, 11, 3, 5, 2, 4, 2, 2, 8, 8, 10, 2, 10, 3, 10, 6, 9, 3, 7, 5, 3, 2, 4, 9, 8, 11, 6, 2, 11, 2, 6, 9, 11, 2, 5, 11, 9, 3, 10, 6, 3, 2, 8, 4, 5, 10, 8, 8, 9, 3, 5, 7, 8, 9, 6, 9, 8, 11, 5, 10, 11, 10, 8, 7, 5, 6, 8, 4, 3, 8, 2, 6, 3, 10, 7, 2, 11, 3, 9, 3, 11, 9, 11, 8, 4, 9, 7, 6, 3, 2, 2, 2, 2, 3, 2, 3, 11, 8, 9, 11, 9, 5, 7, 8, 7, 8, 6, 5, 3, 3, 9, 10, 6, 11, 4, 4, 9, 7, 10, 7, 4, 11, 9, 11, 4, 2, 7, 5, 11, 5, 8, 6, 2, 4, 2, 11, 11, 3, 2, 3, 8, 10, 7, 2, 8, 9, 4, 9, 2, 7, 3, 4, 3, 10, 10, 10, 2, 8, 3, 3, 6, 6, 5, 4, 6, 6, 4, 7, 8, 4, 4, 11, 7, 8, 8, 7, 6, 5, 9, 6, 3, 6, 6, 11, 6, 11, 8, 2, 11, 4, 11, 3, 5, 10, 8, 7, 2, 3, 11, 4, 10, 11, 9, 8, 11, 9, 2, 7, 6, 11, 11, 9, 9, 2, 10, 7, 9, 8, 7, 7, 11, 6, 9, 2, 2, 10, 9, 7, 6, 10, 11, 3, 6, 8, 8, 8, 6, 10, 5, 7, 4, 5, 8, 9, 7, 5, 5, 6, 4, 4, 2, 5, 3, 10, 11, 3, 3, 7, 8, 6, 9, 11, 11, 8, 4, 6, 8, 6, 2, 8, 7, 8, 3, 9, 3, 7, 7, 6, 2, 5, 4, 9, 6, 6, 4, 3, 4, 4, 9, 9, 6, 4, 11, 5, 2, 9, 7, 2, 11, 6, 8, 2, 5, 4, 3, 6, 5, 2, 2, 9, 3, 4, 7, 9, 4, 8, 9, 6, 4, 6, 10, 8, 3, 10, 8, 9, 10, 10, 11, 11, 6, 11, 10, 11, 10, 2, 2, 11, 7, 3, 9, 7, 8, 9, 10, 7, 2, 8, 11, 10, 8, 3, 2, 7, 11, 3, 6, 5, 7, 7, 3, 4, 2, 2, 4, 9, 9, 5, 8, 9, 9, 9, 5, 7, 11, 10, 7, 10, 4, 3, 11, 7, 2, 4, 2, 11, 2, 10, 6, 3, 11, 11, 4, 7, 11, 2, 9, 7, 4, 8, 4, 9, 3, 5, 4, 7, 2, 10, 10, 6, 7, 8, 7, 11, 5, 4, 11, 4, 7, 7, 10, 8, 2, 10, 9, 5, 4, 8, 9, 3, 3, 2, 2, 10, 2, 9, 8, 5, 6, 5, 10, 6, 6, 8, 6, 8, 3, 8, 2, 6, 9, 10, 6, 4, 10, 10, 3, 8, 11, 2, 8, 11, 9, 2, 2, 2, 8, 5, 6, 2, 3, 9, 4, 8, 8, 6, 2, 6, 5, 9, 2, 4, 2, 4, 11, 7, 7, 8, 6, 3, 4, 10, 5, 10, 8, 5, 11, 8, 4, 7, 8, 11, 9, 6, 9, 9, 10, 5, 7, 3, 7, 2, 8, 11, 6, 2, 7, 6, 11, 9, 9, 4, 4, 10, 11, 10, 6, 4, 4, 3, 3, 9, 6, 5, 4, 6, 7, 9, 7, 7, 11, 11, 11, 5, 5, 7, 7, 8, 8, 11, 7, 6, 10, 11, 5, 8, 9, 2, 10, 5, 6, 10, 8, 2, 7, 9, 6, 3, 10, 9, 8, 2, 2, 10, 3, 4, 3, 7, 7, 2, 2, 2, 2, 4, 3, 10, 8, 10, 6, 10, 4, 9, 6, 9, 3, 9, 8, 2, 5, 5, 8, 11, 10, 11, 3, 6, 5, 4, 4, 5, 10, 6, 7, 3, 11, 3, 11, 5, 5, 9, 11, 6, 9, 10, 4, 6, 2, 9, 10, 11, 9, 2, 7, 8, 11, 2, 2, 2, 2, 8, 5, 11, 10, 9, 8, 7, 4, 8, 6, 6, 11, 10, 6, 8, 7, 8, 6, 4, 9, 3, 9, 6, 9, 3, 2, 9, 9, 3, 5, 5, 9, 11, 11, 10, 3, 11, 9, 11, 6, 6, 6, 9, 8, 5, 7, 2, 10, 4, 6, 11, 10, 8, 2, 9, 4, 5, 6, 7, 3, 2, 5, 5, 11, 4, 6, 5, 2, 10, 9, 11, 6, 8, 8, 7, 10, 6, 11, 9, 3, 6, 7, 5, 9, 11, 6, 4, 6, 3, 9, 4, 11, 3, 5, 10, 3, 9, 10, 4, 4, 2, 9, 8, 11, 2, 6, 10, 10, 2, 9, 8, 5, 4, 10, 6, 11, 11, 7, 5, 3, 6, 9, 8, 2, 4, 3, 3, 3, 6, 4, 5, 2, 5, 3, 11, 10, 6, 2, 3, 4, 4, 4, 2, 4, 3, 10, 5, 2, 2, 8, 8, 8, 6, 2, 3, 6, 6, 9, 6, 3, 5, 2, 8, 4, 3, 7, 3, 11, 6, 6, 9, 6, 11, 3, 4, 10, 2, 5, 7, 8, 9, 8, 11, 11, 8, 3, 10, 3, 5, 5, 3, 3, 2, 11, 3, 5, 10, 2, 7, 4, 10, 6, 8, 10, 10, 4, 10, 10, 5, 8, 3, 5, 7, 8, 5, 5, 3, 3, 6, 3, 3, 7, 2, 7, 8, 3, 5, 2, 10, 10, 9, 6, 10, 7, 5, 2, 8, 10, 7, 5, 8, 10, 3, 10, 7, 11, 9, 10, 6, 11, 6, 5, 10, 2, 8, 4, 7, 7, 6, 10, 5, 6, 5, 10, 2, 3, 2, 7, 11, 5, 3, 2, 2, 10, 2, 9, 2, 6, 6, 11, 4, 5, 10, 2, 7, 3, 3, 10, 7, 11, 5, 8, 7, 8, 4, 2, 6, 6, 9, 2, 4, 6, 8, 5, 3, 5, 11, 9, 3, 10, 11, 7, 10, 10, 5, 3, 4, 2, 6, 3, 11, 2, 6, 3, 7, 6, 6, 5, 8, 3, 11, 11, 11, 5, 4, 11, 9, 10, 9, 9, 10, 6, 10, 4, 2, 8, 6, 10, 10, 7, 11, 11, 3, 3, 11, 2, 3, 6, 3, 8, 6, 2, 10, 3, 6, 3, 5, 2, 5, 7, 11, 6, 2, 9, 9, 3, 8, 11, 2, 5, 11, 7, 10, 3, 6, 6, 9, 3, 5, 2, 6, 4, 11, 8, 4, 3, 11, 8, 3, 8, 7, 6, 8, 8, 8, 7, 7, 9, 4, 7, 10, 9, 6, 5, 9, 5, 3, 9, 4, 4, 10, 4, 2, 10, 2, 2, 8, 9, 6, 7, 7, 11, 8, 7, 8, 3, 4, 2, 6, 4, 10, 2, 4, 5, 8, 5, 6, 9, 2, 10, 7, 5, 6, 5, 5, 2, 9, 6, 5, 8, 2, 2, 4, 2, 2, 4, 6, 10, 7, 5, 6, 4, 6, 11, 3, 6, 4, 6, 9, 11, 7, 4, 7, 3, 7, 10, 5, 8, 7, 7, 3, 8, 8, 8, 2, 4, 9, 2, 3, 8, 3, 4, 2, 11, 11, 5, 4, 3, 8, 9, 8, 4, 4, 4, 7, 8, 10, 5, 10, 3, 4, 2, 6, 6, 4, 2, 4, 6, 2, 2, 6, 7, 5, 8, 8, 3, 5, 5, 11, 11, 2, 6, 5, 5, 3, 9, 9, 10, 7, 2, 11, 6, 3, 3, 6, 2, 5, 4, 2, 4, 10, 4, 11, 6, 7, 9, 11, 2, 3, 8, 3, 3, 8, 5, 3, 5, 10, 8, 11, 8, 2, 4, 11, 4, 2, 3, 10, 3, 10, 6, 9, 9, 4, 4, 7, 6, 3, 11, 10, 5, 5, 9, 5, 3, 7, 4, 8, 4, 2, 8, 6, 3, 7, 7, 3, 6, 11, 10, 4, 9, 4, 5, 11, 5, 11, 9, 3, 5, 3, 11, 4, 6, 2, 11, 2, 4, 10, 6, 5, 4, 4, 11, 5, 4, 4, 11, 3, 8, 6, 3, 8, 9, 10, 8, 7, 3, 9, 9, 11, 6, 8, 2, 4, 4, 3, 7, 2, 2, 5, 5, 2, 4, 11, 11, 5, 4, 2, 6, 9, 11, 5, 5, 10, 2, 4, 7, 3, 10, 2, 8, 4, 8, 8, 5, 4, 2, 9, 3, 2, 11, 2, 9, 10, 8, 6, 7, 11, 2, 11, 7, 8, 2, 3, 10, 4, 9, 7, 6, 11, 7, 10, 2, 7, 4, 2, 6, 8, 8, 5, 8, 3, 5, 7, 9, 9, 5, 2, 5, 2, 10, 9, 3, 2, 2, 6, 8, 11, 10, 3, 2, 3, 11, 4, 8, 5, 5, 8, 5, 2, 9, 10, 6, 5, 11, 7, 8, 11, 9, 9, 2, 6, 10, 3, 3, 2, 9, 5, 7, 9, 4, 10, 4, 2, 4, 10, 10, 7, 2, 2, 3, 3, 2, 7, 9, 3, 4, 7, 11, 2, 10, 10, 9, 5, 4, 11, 10, 11, 5, 7, 5, 10, 4, 11, 6, 3, 4, 7, 10, 11, 7, 11, 5, 11, 4, 5, 6, 10, 10, 2, 9, 7, 3, 3, 9, 11, 4, 7, 9, 5, 7, 10, 4, 7, 10, 3, 11, 3, 3, 2, 8, 10, 4, 10, 7, 6, 4, 4, 7, 4, 6, 6, 2, 3, 9, 8, 9, 9, 3, 7, 11, 10, 9, 9, 7, 4, 3, 5, 10, 10, 9, 11, 11, 3, 5, 3, 10, 2, 9, 10, 6, 11, 7, 10, 5, 9, 3, 11, 6, 4, 3, 3, 6, 3, 9, 10, 3, 7, 6, 4, 11, 3, 6, 8, 5, 7, 3, 3, 7, 10, 9, 4, 8, 2, 11, 7, 5, 6, 7, 2, 5, 4, 8, 9, 8, 5, 10, 6, 10, 5, 11, 7, 10, 3, 2, 8, 3, 6, 11, 2, 3, 8, 10, 8, 3, 6, 6, 8, 3, 6, 6, 3, 7, 5, 2, 6, 5, 7, 11, 2, 3, 6, 9, 8, 2, 5, 2, 2, 6, 9, 3, 2, 7, 2, 4, 10, 11, 7, 8, 2, 11, 2, 2, 3, 11, 2, 9, 6, 6, 2, 7, 8, 5, 11, 4, 2, 7, 11, 4, 5, 2, 11, 4, 3, 11, 10, 8, 3, 9, 9, 5, 9, 6, 10, 3, 3, 7, 3, 2, 8, 4, 3, 6, 8, 7, 2, 8, 2, 7, 9, 4, 5, 8, 6, 8, 5, 7, 3, 4, 11, 2, 7, 7, 6, 5, 6, 11, 9, 10, 4, 6, 11, 5, 7, 5, 9, 7, 4, 5, 2, 8, 5, 10, 6, 9, 3, 8, 6, 5, 2, 10, 6, 11, 8, 10, 10, 11, 9, 3, 10, 2, 8, 6, 3, 8, 11, 3, 5, 3, 11, 9, 11, 7, 9, 10, 3, 7, 8, 11, 3, 2, 10, 8, 11, 9, 2, 5, 8, 7, 8, 11, 11, 4, 4, 7, 2, 2, 4, 5, 10, 8, 11, 3, 2, 4, 10, 2, 8, 11, 3, 11, 4, 11, 3, 7, 4, 2, 8, 9, 4, 10, 5, 5, 8, 7, 7, 5, 3, 3, 8, 3, 6, 5, 2, 2, 4, 3, 3, 3, 5, 10, 7, 4, 4, 9, 6, 9, 7, 7, 9, 3, 3, 11, 3, 8, 5, 6, 10, 5, 2, 7, 3, 2, 5, 4, 6, 11, 6, 11, 9, 4, 4, 10, 6, 7, 8, 3, 8, 5, 7, 8, 6, 7, 8, 2, 6, 9, 6, 6, 2, 3, 4, 3, 3, 4, 8, 2, 11, 4, 4, 6, 5, 2, 8, 3, 2, 4, 9, 5, 8, 7, 4, 6, 8, 10, 11, 8, 11, 11, 3, 10, 8, 6, 10, 5, 5, 6, 4, 5, 4, 6, 7, 10, 8, 8, 3, 11, 3, 4, 11, 7, 7, 6, 2, 11, 7, 2, 5, 8, 5, 5, 9, 7, 10, 10, 4, 6, 4, 10, 2, 6, 7, 3, 10, 8, 5, 10, 11, 4, 5, 2, 5, 7, 8, 5, 9, 11, 5, 5, 8, 3, 8, 5, 2, 6, 6, 3, 8, 6, 9, 2, 2, 7, 8, 11, 9, 7, 8, 10, 6, 7, 9, 5, 3, 4, 4, 6, 8, 10, 9, 9, 2, 6, 9, 11, 4, 2, 2, 11, 2, 6, 10, 6, 7, 6, 4, 6, 6, 8, 7, 5, 9, 7, 4, 2, 4, 10, 2, 3, 6, 11, 2, 6, 4, 5, 3, 8, 2, 9, 9, 3, 5, 8, 8, 10, 7, 11, 7, 5, 6, 4, 5, 9, 2, 4, 4, 10, 8, 11, 9, 6, 4, 5, 5, 7, 2, 9, 2, 8, 7, 9, 6, 6, 6, 9, 8, 7, 4, 6, 4, 3, 11, 10, 8, 10, 4, 11, 2, 7, 3, 5, 3, 3, 4, 10, 5, 4, 3, 10, 2, 6, 2, 11, 6, 9, 4, 9, 2, 5, 6, 3, 2, 9, 11, 7, 2, 3, 4, 4, 5, 2, 6, 11, 6, 3, 8, 4, 5, 10, 2, 3, 11, 4, 6, 11, 2, 10, 4, 9, 8, 9, 11, 3, 11, 10, 8, 4, 4, 4, 9, 10, 8, 3, 4, 4, 9, 5, 7, 9, 5, 7, 5, 7, 6, 8, 2, 6, 11, 7, 2, 6, 2, 8, 7, 4, 10, 3, 10, 6, 4, 10, 9, 2, 2, 9, 11, 10, 5, 8, 7, 9, 11, 8, 5, 2, 8, 10, 6, 6, 8, 11, 4, 5, 6, 6, 10, 11, 4, 6, 6, 11, 3, 10, 7, 3, 4, 10, 3, 8, 10, 2, 11, 2, 7, 5, 6, 2, 8, 8, 9, 4, 4, 11, 4, 8, 8, 2, 5, 9, 4, 8, 4, 9, 3, 8, 11, 11, 9, 5, 3, 9, 10, 8, 3, 7, 3, 3, 9, 2, 11, 3, 5, 6, 6, 6, 4, 9, 3, 4, 8, 9, 9, 8, 9, 2, 8, 6, 2, 4, 8, 5, 3, 10, 4, 4, 2, 9, 4, 6, 5, 4, 10, 5, 4, 10, 8, 7, 6, 3, 3, 10, 10, 11, 2, 4, 9, 6, 5, 7, 6, 7, 8, 9, 6, 7, 11, 3, 11, 11, 4, 7, 5, 7, 3, 7, 8, 10, 11, 4, 3, 10, 4, 6, 7, 10, 5, 5, 6, 9, 10, 4, 3, 3, 3, 8, 11, 10, 3, 5, 7, 4, 11, 11, 8, 8, 7, 5, 5, 6, 3, 7, 4, 10, 4, 5, 7, 3, 5, 2, 6, 4, 2, 3, 8, 2, 4, 3, 9, 3, 3, 11, 2, 7, 3, 2, 2, 3, 2, 6, 4, 9, 7, 9, 8, 9, 2, 11, 5, 2, 7, 4, 10, 2, 2, 5, 8, 2, 8, 6, 8, 2, 4, 2, 10, 6, 10, 6, 5, 4, 8, 7, 2, 6, 3, 10, 3, 5, 6, 7, 6, 8, 11, 11, 2, 3, 10, 6, 5, 11, 8, 7, 6, 6, 4, 2, 2, 7, 3, 8, 2, 3, 10, 4, 7, 2, 9, 11, 8, 7, 10, 10, 3, 9, 6, 3, 11, 5, 2, 10, 5, 2, 5, 9, 11, 6, 10, 11, 10, 2, 6, 4, 10, 7, 10, 5, 2, 10, 8, 6, 9, 8, 7, 3, 10, 11, 3, 3, 8, 10, 11, 2, 9, 7, 5, 4, 8, 7, 10, 3, 2, 10, 2, 9, 2, 5, 4, 7, 5, 11, 10, 6, 10, 5, 11, 2, 9, 9, 5, 10, 11, 5, 5, 7, 11, 6, 2, 4, 2, 6, 7, 9, 9, 9, 10, 11, 3, 8, 11, 9, 7, 4, 11, 4, 8, 5, 9, 11, 11, 4, 8, 7, 10, 6, 11, 3, 9, 6, 6, 3, 8, 3, 11, 5, 7, 6, 6, 4, 6, 11, 9, 8, 4, 5, 5, 3, 5, 11, 9, 8, 5, 5, 5, 4, 11, 6, 10, 11, 6, 3, 2, 2, 9, 11, 7, 11, 6, 4, 10, 8, 11, 10, 8, 3, 4, 8, 2, 6, 4, 7, 11, 7, 2, 11, 7, 5, 10, 8, 2, 2, 7, 9, 5, 6, 6, 5, 10, 4, 10, 3, 11, 8, 10, 7, 10, 2, 11, 5, 8, 6, 7, 9, 6, 7, 8, 6, 11, 11, 2, 8, 10, 9, 4, 7, 4, 9, 2, 5, 7, 6, 6, 4, 5, 3, 2, 5, 10, 7, 3, 7, 5, 9, 5, 9, 7, 11, 3, 4, 9, 5, 3, 6, 9, 2, 2, 4, 4, 3, 7, 2, 7, 3, 5, 4, 2, 10, 6, 8, 2, 3, 2, 10, 4, 7, 8, 8, 8, 10, 11, 6, 2, 11, 6, 4, 5, 5, 7, 7, 9, 5, 10, 9, 10, 7, 5, 6, 2, 7, 11, 6, 2, 5, 2, 11, 9, 5, 5, 11, 5, 9, 10, 9, 6, 11, 2, 7, 5, 8, 5, 11, 8, 8, 9, 3, 7, 6, 5, 4, 8, 3, 4, 10, 11, 8, 5, 3, 5, 8, 3, 8, 5, 6, 11, 11, 4, 10, 4, 2, 8, 7, 10, 7, 7, 7, 3, 5, 2, 4, 8, 9, 2, 6, 10, 4, 6, 6, 3, 6, 9, 11, 10, 10, 8, 9, 11, 7, 5, 10, 2, 11, 5, 11, 4, 7, 8, 8, 7, 7, 9, 7, 4, 7, 11, 7, 4, 6, 10, 7, 4, 10, 8, 11, 3, 10, 3, 8, 9, 7, 6, 7, 6, 7, 9, 7, 2, 7, 10, 6, 8, 4, 5, 9, 3, 6, 11, 3, 2, 4, 6, 5, 11, 2, 4, 11, 6, 6, 4, 8, 11, 3, 9, 5, 5, 6, 2, 6, 3, 3, 8, 7, 11, 4, 8, 10, 4, 10, 11, 6, 5, 3, 7, 9, 9, 6, 5, 9, 3, 6, 4, 2, 4, 11, 3, 9, 6, 7, 10, 5, 10, 10, 6, 8, 9, 9, 7, 3, 4, 9, 10, 4, 8, 6, 11, 2, 8, 4, 8, 11, 9, 4, 7, 6, 10, 8, 5, 4, 6, 2, 11, 5, 10, 7, 10, 5, 4, 3, 4, 7, 10, 4, 11, 9, 10, 3, 5, 5, 3, 7, 3, 3, 5, 4, 11, 2, 7, 7, 6, 5, 2, 5, 2, 3, 4, 4, 8, 4, 10, 11, 10, 8, 9, 11, 6, 11, 2, 5, 8, 11, 11, 3, 10, 3, 3, 5, 5, 7, 3, 9, 5, 9, 8, 5, 4, 2, 7, 9, 9, 3, 7, 4, 10, 5, 4, 7, 10, 10, 4, 4, 11, 2, 6, 7, 3, 8, 3, 7, 8, 4, 5, 3, 8, 2, 8, 11, 9, 10, 8, 5, 4, 10, 5, 3, 10, 4, 11, 3, 9, 9, 5, 10, 7, 2, 2, 3, 4, 5, 5, 4, 11, 11, 10, 6, 6, 10, 6, 7, 8, 9, 2, 6, 6, 11, 4, 4, 7, 5, 2, 10, 2, 9, 2, 9, 8, 2, 11, 3, 2, 3, 9, 3, 8, 2, 11, 2, 9, 9, 3, 8, 8, 8, 4, 11, 7, 6, 10, 5, 10, 4, 10, 8, 6, 2, 2, 6, 2, 8, 7, 5, 11, 9, 10, 2, 4, 5, 5, 5, 2, 8, 2, 4, 4, 11, 7, 10, 5, 3, 3, 6, 9, 3, 4, 2, 4, 10, 11, 5, 4, 2, 8, 8, 2, 3, 7, 5, 4, 3, 2, 3, 6, 3, 11, 5, 4, 7, 7, 7, 5, 10, 8, 11, 7, 8, 7, 7, 7, 6, 3, 4, 4, 10, 7, 5, 3, 2, 5, 6, 6, 7, 8, 6, 9, 2, 11, 6, 8, 10, 2, 6, 8, 7, 10, 6, 6, 11, 7, 4, 11, 2, 3, 4, 11, 4, 11, 9, 9, 7, 10, 2, 11, 10, 11, 10, 7, 2, 10, 5, 6, 9, 6, 10, 2, 2, 4, 10, 2, 9, 3, 4, 10, 11, 3, 9, 8, 2, 10, 2, 10, 7, 4, 4, 7, 3, 3, 7, 10, 4, 6, 11, 11, 10, 8, 8, 10, 6, 4, 6, 2, 5, 11, 3, 2, 8, 5, 4, 8, 6, 9, 10, 5, 2, 11, 6, 9, 11, 3, 9, 7, 4, 5, 8, 8, 3, 8, 6, 4, 8, 11, 7, 3, 6, 10, 8, 4, 6, 11, 9, 4, 3, 7, 11, 7, 11, 4, 3, 10, 4, 11, 7, 2, 2, 3, 10, 11, 3, 3, 10, 2, 9, 9, 2, 6, 6, 2, 2, 9, 9, 11, 7, 5, 8, 6, 8, 7, 6, 3, 5, 5, 7, 2, 6, 6, 8, 4, 6, 8, 11, 3, 3, 6, 9, 6, 11, 2, 3, 6, 5, 6, 11, 4, 2, 8, 8, 3, 4, 3, 6, 9, 4, 2, 3, 6, 3, 7, 10, 2, 8, 9, 4, 11, 5, 8, 10, 4, 7, 4, 11, 2, 7, 2, 8, 8, 11, 2, 8, 11, 7, 7, 4, 10, 3, 9, 7, 5, 8, 10, 9, 10, 9, 2, 2, 5, 5, 4, 11, 11, 6, 6, 9, 6, 4, 8, 4, 10, 5, 9, 7, 2, 4, 11, 10, 6, 11, 7, 2, 7, 5, 4, 5, 10, 6, 10, 4, 8, 9, 9, 6, 5, 7, 5, 11, 6, 11, 4, 5, 7, 8, 2, 8, 8, 3, 2, 6, 4, 8, 7, 3, 4, 9, 7, 9, 3, 10, 7, 10, 6, 11, 3, 6, 2, 2, 7, 7, 7, 10, 5, 5, 5, 5, 3, 3, 10, 4, 2, 4, 10, 6, 4, 5, 10, 9, 3, 10, 9, 2, 7, 8, 9, 3, 2, 3, 10, 5, 3, 3, 6, 10, 3, 3, 8, 5, 10, 5, 6, 9, 2, 6, 10, 2, 11, 3, 6, 7, 3, 2, 6, 8, 7, 2, 10, 5, 10, 9, 2, 3, 2, 7, 4, 11, 8, 3, 9, 10, 10, 6, 7, 7, 9, 6, 8, 4, 5, 2, 5, 7, 3, 4, 4, 6, 6, 9, 10, 5, 6, 7, 4, 6, 8, 7, 7, 8, 4, 2, 2, 5, 2, 11, 9, 3, 6, 6, 8, 4, 5, 9, 4, 6, 8, 8, 9, 6, 2, 2, 9, 7, 7, 11, 7, 8, 3, 2, 4, 4, 4, 7, 7, 7, 5, 11, 2, 4, 2, 9, 2, 5, 9, 9, 5, 3, 6, 8, 7, 4, 8, 10, 4, 5, 7, 3, 6, 3, 4, 10, 10, 10, 5, 6, 6, 6, 3, 3, 7, 9, 4, 8, 4, 6, 10, 9, 4, 3, 6, 5, 4, 4, 5, 7, 6, 10, 4, 8, 10, 11, 8, 11, 11, 7, 3, 5, 9, 5, 9, 8, 4, 10, 5, 9, 3, 6, 10, 5, 10, 10, 9, 11, 4, 7, 9, 3, 10, 10, 5, 9, 10, 3, 2, 4, 11, 10, 9, 4, 4, 10, 7, 2, 4, 3, 3, 4, 9, 8, 3, 6, 8, 3, 7, 10, 11, 5, 6, 2, 4, 2, 2, 2, 6, 3, 7, 9, 4, 5, 2, 9, 10, 9, 5, 10, 2, 5, 6, 9, 2, 7, 5, 7, 6, 2, 5, 6, 6, 3, 2, 4, 10, 2, 6, 9, 7, 7, 5, 4, 11, 4, 6, 10, 3, 4, 2, 7, 8, 9, 6, 8, 9, 11, 4, 7, 5, 3, 7, 11, 3, 8, 8, 4, 6, 10, 11, 8, 9, 6, 9, 3, 6, 8, 2, 5, 10, 8, 6, 7, 6, 6, 2, 6, 7, 2, 8, 3, 2, 11, 11, 10, 11, 11, 3, 3, 2, 7, 10, 2, 10, 3, 3, 2, 6, 5, 8, 4, 5, 10, 9, 8, 3, 4, 9, 9, 3, 4, 9, 5, 6, 7, 5, 8, 9, 6, 10, 6, 4, 10, 2, 7, 5, 2, 5, 7, 5, 10, 9, 5, 4, 9, 5, 4, 3, 11, 9, 3, 3, 4, 7, 9, 3, 3, 10, 7, 8, 9, 2, 6, 9, 6, 3, 10, 9, 11, 11, 5, 8, 9, 5, 9, 9, 7, 7, 3, 9, 11, 3, 3, 9, 11, 8, 7, 9, 4, 6, 5, 4, 8, 6, 8, 2, 8, 5, 6, 5, 3, 2, 11, 5, 2, 3, 6, 2, 10, 3, 10, 4, 7, 4, 6, 6, 7, 6, 5, 8, 8, 8, 5, 4, 6, 7, 4, 9, 6, 5, 2, 8, 10, 8, 4, 11, 8, 9, 5, 5, 7, 9, 4, 11, 8, 5, 9, 6, 8, 8, 5, 5, 6, 9, 4, 5, 8, 5, 11, 2, 11, 11, 5, 8, 9, 3, 9, 4, 7, 3, 8, 9, 7, 5, 9, 7, 3, 5, 9, 11, 2, 3, 6, 2, 2, 8, 6, 11, 11, 9, 2, 4, 10, 3, 2, 6, 10, 9, 5, 7, 11, 11, 6, 3, 6, 2, 7, 6, 7, 8, 2, 10, 10, 10, 9, 2, 3, 10, 9, 6, 4, 11, 4, 7, 5, 9, 11, 4, 4, 4, 9, 6, 5, 7, 10, 5, 10, 2, 2, 3, 5, 6, 7, 4, 5, 11, 2, 4, 6, 8, 8, 4, 2, 4, 11, 3, 2, 5, 9, 9, 8, 3, 10, 2, 8, 2, 11, 4, 8, 8, 5, 8, 7, 7, 2, 2, 4, 8, 9, 2, 7, 9, 2, 7, 11, 2, 11, 9, 9, 2, 5, 10, 9, 7, 5, 10, 11, 3, 2, 5, 4, 8, 2, 8, 3, 6, 5, 11, 6, 8, 9, 11, 7, 10, 5, 6, 8, 3, 8, 8, 5, 8, 6, 5, 7, 5, 4, 7, 8, 2, 5, 5, 11, 6, 5, 8, 9, 5, 4, 7, 3, 5, 5, 2, 7, 10, 7, 11, 6, 4, 10, 4, 5, 8, 2, 9, 6, 10, 5, 8, 4, 11, 2, 9, 7, 3, 9, 4, 2, 11, 8, 7, 10, 7, 6, 10, 5, 10, 4, 11, 9, 4, 2, 6, 4, 3, 11, 8, 3, 5, 10, 8, 2, 5, 6, 11, 5, 9, 8, 10, 3, 8, 7, 6, 5, 5, 7, 5, 6, 4, 5, 6, 4, 4, 3, 10, 4, 10, 3, 7, 6, 5, 5, 9, 10, 8, 10, 3, 7, 4, 8, 7, 2, 8, 9, 8, 9, 8, 9, 8, 5, 3, 8, 3, 11, 7, 9, 2, 3, 4, 8, 9, 8, 6, 3, 6, 7, 9, 8, 7, 9, 3, 2, 11, 9, 8, 6, 5, 4, 10, 5, 7, 3, 5, 11, 7, 3, 7, 10, 4, 9, 6, 6, 6, 3, 8, 2, 7, 8, 5, 4, 5, 2, 10, 7, 10, 10, 3, 6, 10, 8, 3, 7, 3, 9, 4, 4, 6, 6, 10, 11, 2, 7, 3, 6, 11, 9, 10, 5, 6, 5, 8, 11, 7, 6, 11, 8, 6, 7, 2, 11, 4, 8, 5, 10, 11, 11, 2, 6, 3, 7, 2, 3, 6, 9, 9, 10, 4, 5, 2, 11, 10, 2, 7, 5, 2, 7, 5, 2, 8, 4, 4, 11, 7, 7, 11, 6, 11, 3, 7, 7, 2, 9, 2, 2, 10, 7, 4, 8, 4, 7, 9, 2, 10, 5, 9, 9, 8, 2, 2, 4, 5, 10, 2, 7, 9, 9, 11, 10, 5, 8, 2, 6, 6, 5, 2, 10, 2, 9, 6, 11, 2, 8, 4, 9, 2, 11, 7, 9, 3, 2, 2, 2, 4, 11, 6, 10, 8, 2, 5, 5, 10, 7, 8, 3, 8, 2, 2, 4, 3, 10, 8, 2, 3, 9, 3, 6, 4, 7, 8, 11, 3, 11, 7, 11, 2, 3, 3, 3, 11, 5, 4, 8, 9, 8, 3, 10, 7, 10, 6, 6, 11, 6, 11, 2, 6, 11, 5, 2, 10, 2, 9, 10, 3, 3, 3, 11, 3, 10, 8, 7, 5, 3, 2, 11, 7, 8, 10, 6, 4, 11, 10, 2, 5, 8, 2, 3, 6, 8, 8, 3, 8, 8, 2, 11, 9, 9, 5, 7, 5, 2, 9, 5, 5, 11, 5, 2, 2, 9, 10, 4, 8, 11, 9, 7, 9, 6, 3, 7, 9, 8, 4, 9, 9, 3, 6, 2, 3, 9, 11, 9, 8, 7, 4, 9, 6, 7, 8, 3, 10, 5, 11, 4, 7, 7, 2, 7, 2, 7, 4, 8, 9, 10, 3, 4, 9, 2, 6, 9, 8, 9, 9, 4, 8, 3, 5, 2, 11, 3, 9, 3, 6, 5, 5, 9, 8, 2, 9, 7, 6, 7, 2, 8, 8, 8, 11, 11, 7, 9, 3, 8, 6, 5, 9, 11, 5, 2, 3, 7, 5, 3, 9, 5, 4, 6, 2, 9, 8, 7, 11, 5, 8, 9, 11, 2, 10, 7, 2, 6, 7, 5, 7, 8, 8, 8, 5, 7, 2, 8, 4, 8, 7, 5, 8, 9, 8, 4, 10, 9, 2, 11, 10, 5, 8, 4, 9, 11, 5, 6, 4, 10, 7, 9, 2, 5, 4, 11, 9, 5, 6, 11, 11, 10, 9, 8, 9, 9, 9, 11, 2, 10, 4, 11, 2, 6, 11, 10, 3, 2, 3, 5, 5, 10, 9, 9, 10, 8, 3, 10, 6, 10, 9, 7, 2, 5, 11, 5, 7, 4, 10, 9, 7, 6, 7, 2, 2, 4, 5, 5, 6, 7, 6, 10, 5, 8, 8, 9, 8, 4, 11, 3, 4, 6, 6, 3, 5, 9, 2, 11, 7, 9, 9, 6, 2, 9, 5, 6, 7, 7, 3, 2, 6, 3, 10, 7, 6, 2, 5, 11, 2, 2, 8, 11, 11, 10, 9, 11, 11, 6, 10, 5, 7, 5, 9, 8, 9, 4, 9, 4, 7, 11, 2, 4, 10, 2, 3, 6, 11, 5, 3, 7, 3, 11, 8, 9, 6, 4, 9, 3, 10, 11, 3, 9, 5, 6, 5, 11, 10, 4, 8, 4, 2, 7, 3, 10, 10, 2, 2, 9, 5, 4, 2, 4, 8, 11, 7, 4, 2, 8, 5, 8, 8, 11, 5, 5, 3, 4, 7, 6, 8 };
						int an[8192];

						int t = 0;
						for (i = 0; i < candidates[l]; i++) {
							t += n[i];
						}
						t += ADD_BUDGET;
						double wallTime;

						//Parallel OCBA
						float *h_mean;
						float *d_mean;
						float *h_var;
						float *d_var;
						int *h_an;
						int *d_an;
						int *h_n;
						int *d_n;
						int *h_t;
						int *d_t;
						float *h_best;
						float *d_best;
						int *h_indexOfBest;
						int *d_indexOfBest;
						float *h_gamma;
						float *d_gamma;
						float *h_lamda;
						float *d_lamda;
						float *h_rho;
						float *d_rho;
						int *h_moreRun;
						int *d_moreRun;
						int *d_mutex;
						float *h_summation;
						float *d_summation;
						float *h_alphaB;
						float *d_alphaB;
						float *h_rhoPrevious;
						float *d_rhoPrevious;

						h_mean = (float*)malloc(candidates[l] * sizeof(float));
						h_var = (float*)malloc(candidates[l] * sizeof(float));
						h_an = (int*)malloc(candidates[l] * sizeof(int));
						h_n = (int*)malloc(candidates[l] * sizeof(int));
						h_t = (int*)malloc(candidates[l] * sizeof(int));
						h_best = (float*)malloc(sizeof(float));
						h_indexOfBest = (int*)malloc(sizeof(int));
						h_gamma = (float*)malloc(candidates[l] * sizeof(float));
						h_lamda = (float*)malloc(candidates[l] * sizeof(float));
						h_rho = (float*)malloc(candidates[l] * sizeof(float));
						h_moreRun = (int*)malloc(candidates[l] * sizeof(int));
						h_summation = (float*)malloc(sizeof(float));
						h_alphaB = (float*)malloc(sizeof(float));
						h_rhoPrevious = (float*)malloc(sizeof(float));
						cudaMalloc((void**)&d_mean, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_var, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_an, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_n, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_t, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_best, sizeof(float));
						cudaMalloc((void**)&d_indexOfBest, sizeof(int));
						cudaMalloc((void**)&d_gamma, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_lamda, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_rho, candidates[l] * sizeof(float));
						cudaMalloc((void**)&d_moreRun, candidates[l] * sizeof(int));
						cudaMalloc((void**)&d_summation, sizeof(float));
						cudaMalloc((void**)&d_mutex, sizeof(int));
						cudaMalloc((void**)&d_alphaB, sizeof(int));
						cudaMalloc((void**)&d_rhoPrevious, sizeof(int));
						cudaMemset(d_an, 0, candidates[l] * sizeof(int));
						cudaMemset(d_mutex, 0, sizeof(float));
						cudaMemset(d_best, 0, sizeof(float));
						cudaMemset(d_indexOfBest, 0, sizeof(int));
						cudaMemset(d_gamma, 0, candidates[l] * sizeof(float));
						cudaMemset(d_lamda, 0, candidates[l] * sizeof(float));
						cudaMemset(d_rho, 0, sizeof(float));
						cudaMemset(d_moreRun, 1, candidates[l] * sizeof(int));
						cudaMemset(d_summation, 0, sizeof(float));

						for (unsigned int i = 0; i < candidates[l]; i++){
							h_mean[i] = s_mean[i];
							h_var[i] = s_var[i];
							h_n[i] = n[i];
							h_t[i] = t;
						}

						cudaMemcpy(d_mean, h_mean, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_var, h_var, candidates[l] * sizeof(float), cudaMemcpyHostToDevice);
						cudaMemcpy(d_n, h_n, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);
						cudaMemcpy(d_t, h_t, candidates[l] * sizeof(int), cudaMemcpyHostToDevice);

						dim3 gridSize = GRIDDIMENSION;
						dim3 blockSize = BLOCKDIMENSION;


						if (k == 0) {
							wallTime = get_wall_time();
							ocba(s_mean, s_var, candidates[l], n, ADD_BUDGET, an, TYPE);
						}
						else if (k == 1) {
							wallTime = get_wall_time();
							parallelOCBA1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, TYPE);
						}
						else if (k == 2) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA2 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}
						else if (k == 3) {
							parallelOCBA_1 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
							wallTime = get_wall_time();
							parallelOCBA3 << <gridSize, blockSize >> >(d_mean, d_var, candidates[l], d_n, d_t, ADD_BUDGET, d_an, d_best, d_indexOfBest, d_gamma, d_lamda, d_rho, d_moreRun, d_mutex, d_summation, d_alphaB, d_rhoPrevious, TYPE);
						}

						wallTime = get_wall_time() - wallTime;
						outputFile << candidates[l] << "," << wallTime << std::endl;

						cudaMemcpy(h_an, d_an, candidates[l] * sizeof(float), cudaMemcpyDeviceToHost);

						free(h_mean);
						free(h_var);
						free(h_an);
						free(h_alphaB);
						free(h_best);
						free(h_gamma);
						free(h_lamda);
						free(h_indexOfBest);
						free(h_moreRun);
						free(h_rho);
						free(h_rhoPrevious);
						free(h_n);
						free(h_summation);
						free(h_t);

						cudaFree(d_mean);
						cudaFree(d_var);
						cudaFree(d_an);
						cudaFree(d_alphaB);
						cudaFree(d_best);
						cudaFree(d_gamma);
						cudaFree(d_lamda);
						cudaFree(d_indexOfBest);
						cudaFree(d_moreRun);
						cudaFree(d_rho);
						cudaFree(d_rhoPrevious);
						cudaFree(d_n);
						cudaFree(d_summation);
						cudaFree(d_t);
					}
				}
			}
		}
	}
	system("pause");
	outputFile.close();
}

void ocba(float s_mean[], float s_var[], int nd, int n[], int add_budget, int an[], int type) {

	/*
	This subroutine determines how many additional runs each
	design will should have for next iteration of simulation.
	s_mean[i]: sample mean of design i, i=0,1,..,ND-1
	s_var[i]: sample variance of design i, i=0,1,..,ND-1
	nd: the number of designs
	n[i]: number of simulation replications of design i,
	i=0,1,..,ND-1
	add_budget: the additional simulation budget
	an[i]: additional number of simulation replications assigned
	to design i, i=0,1,..,ND-1
	type: type of optimazition problem. type=1, MIN problem;
	type=2, MAX problem
	*/
	int i;
	int b, s;
	int t_budget, t1_budget;
	int morerun[8192], more_alloc; /* 1:Yes; 0:No */
	float t_s_mean[8192];
	float ratio[8192]; /* Ni/Ns */
	float ratio_s, temp;
	if (type == 1) { /*MIN problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = s_mean[i];
	}
	else { /*MAX problem*/
		for (i = 0; i < nd; i++) t_s_mean[i] = (-1)*s_mean[i];
	}
	t_budget = add_budget;
	for (i = 0; i < nd; i++) t_budget += n[i];
	b = best(t_s_mean, nd);
	s = second_best(t_s_mean, nd, b);
	ratio[s] = 1.0;
	for (i = 0; i < nd; i++)
		if (i != s && i != b) {
			temp = (t_s_mean[b] - t_s_mean[s]) / (t_s_mean[b] - t_s_mean[i]);
			ratio[i] = temp*temp*s_var[i] / s_var[s];
		} /* calculate ratio of Ni/Ns*/
	temp = 0;
	for (i = 0; i < nd; i++) if (i != b) temp += (ratio[i] * ratio[i] / s_var[i]);
	ratio[b] = sqrt(s_var[b] * temp); /* calculate Nb */
	for (i = 0; i < nd; i++) morerun[i] = 1;
	t1_budget = t_budget;
	do{
		more_alloc = 0;
		ratio_s = 0.0;
		for (i = 0; i < nd; i++) if (morerun[i]) ratio_s += ratio[i];
		for (i = 0; i < nd; i++) if (morerun[i])
		{
			an[i] = (int)(t1_budget / ratio_s*ratio[i]);
			/* disable those design which have been run too much */
			if (an[i] < n[i])
			{
				an[i] = n[i];
				morerun[i] = 0;
				more_alloc = 1;
			}
		}
		if (more_alloc)
		{
			t1_budget = t_budget;
			for (i = 0; i < nd; i++) if (!morerun[i]) t1_budget -= an[i];
		}
	} while (more_alloc); /* end of WHILE */
	/* calculate the difference */
	t1_budget = an[0];
	for (i = 1; i < nd; i++) t1_budget += an[i];
	an[b] += (t_budget - t1_budget); /* give the difference
									 to design b */
	for (i = 0; i < nd; i++) an[i] -= n[i];
}

int best(float t_s_mean[], int nd) {
	/*This function determines the best design based on current
	simulation results */
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs */

	int i, min_index;
	min_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[min_index]) {
			min_index = i;
		}
	}
	return min_index;
}

int second_best(float t_s_mean[], int nd, int b) {
	/*This function determines the second best design based on
	current simulation results*/
	/* t_s_mean[i]: temporary array for sample mean of design i,
	i=0,1,..,ND-1
	nd: the number of designs.
	b: current best design determined by function
	best() */
	int i, second_index;
	if (b == 0) second_index = 1;
	else second_index = 0;
	for (i = 0; i < nd; i++) {
		if (t_s_mean[i] < t_s_mean[second_index] && i != b) {
			second_index = i;
		}
	}
	return second_index;
}